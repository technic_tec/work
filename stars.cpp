#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 200
#define INF 99999999

struct Star {
    int x, y, w;
} p[N];
int n, dx, dy, wm, ws;

int comp(const void *a, const void *b)
{
    struct Star *pa = (struct Star *)a;
    struct Star *pb = (struct Star *)b;
    // dx*y - dy*x = c
    long long ca = (long long)dx * (long long)pa->y - (long long)dy * (long long)pa->x;
    long long cb = (long long)dx * (long long)pb->y - (long long)dy * (long long)pb->x;
    return (ca > cb ? 1 : ((ca < cb) ? -1 : 0));
}
int main()
{
    scanf("%d", &n);
    ws = 0;
    for(int i = 0; i < n; i++) {
        scanf("%d%d%d", &p[i].x, &p[i].y, &p[i].w);
        ws += p[i].w;
    }
    wm = 0;
    for(int i = 0; i < n; i++)
        for(int j = i+1; j < n; j++) {
            dx = p[j].x - p[i].x; dy = p[j].y - p[i].y;
            qsort(p, n, sizeof(struct Star), comp);
            int k, s;
            for(k = 0, s = 0; k < n && 2*(s+p[k].w) <= ws; s += p[k].w, k++);
            if(s > wm) wm = s;
            if(ws-s-p[k].w > wm) wm = ws - s - p[k].w;
            if(k > 0 && (p[k-1].y-p[k].y)*dx == (p[k-1].x-p[k].x)*dy) {
                int u = k-1, v = k;
                s -= p[u].w;
                while(u > 0 && (p[u-1].y-p[u].y)*dx == (p[u-1].x-p[u].x)*dy) {
                    --u;
                    s -= p[u].w;
                }
                while(v + 1 < n && (p[v+1].y-p[v].y)*dx == (p[v+1].x-p[v].x)*dy)
                    ++v;
                while(v >= u && 2*(s+p[v].w) <= ws)
                    s += p[v--].w;
                if(s > wm) wm = s;
                if(ws-s-p[v].w > wm) wm = ws - s - p[v].w;
            }
        }
    printf("%d\n", wm);
    return 0;
}
