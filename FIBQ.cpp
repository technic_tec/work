/*
 * =====================================================================================
 *
 *       Filename:  ADDMUL.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月05日 13时26分11秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define P 1000000007

class Fib {
  public:
    int a, b, c;
    Fib(int _a = 0, int _b = 1, int _c = 1) : a(_a), b(_b), c(_c) {}
    Fib operator+(const Fib &o) const {
      return Fib(a+o.a, b+o.b, c+o.c);
    }
    Fib operator*(const Fib &o) const {
      // a b * oa ob = a*oa+b*ob a*ob+b*oc
      // b c   ob oc   b*oa+c*ob b*ob+c*oc
      int a1 = (int)(((long long)a*(long long)o.a + (long long)b * (long long)o.b) % P);
      int b1 = (int)(((long long)a*(long long)o.b + (long long)b * (long long)o.c) % P);
      int b2 = (int)(((long long)b*(long long)o.a + (long long)c * (long long)o.b) % P);
      int c1 = (int)(((long long)b*(long long)o.b + (long long)c * (long long)o.c) % P);
      assert(b1 == b2);
      return Fib(a1, b1, c1);
    }
    Fib operator^(int k) const {
      Fib r = Fib(1, 0, 1);
      Fib x = *this;
      while(k) {
        if(k & 1)
          r = r * x;
        x = x * x;
        k >>= 1;
      }
      return r;
    }
};

class SegmentTree {
  public:
    struct Node {
        int l, r;
        Fib v;
    };
    SegmentTree(const vector<int> &v)
      : m_sz(v.size()), m_nodes(2*v.size())
    {
      int m = m_sz;
      while(m & (m-1))
        m += (m&(-m));
      m_base = m;
      for(int j = 0; j < m_sz; j++) {
        int i = m + j;
        if(i >= 2*m_sz)
          i -= m_sz;
        m_nodes[i].l = m_nodes[i].r = j+1;
        m_nodes[i].v = (Fib()^v[j]) + Fib(1, 0, 1);
      }
      for(int i = m_sz-1; i > 0; i--) {
        //assert(m_nodes[2*i].l <= m_nodes[2*i].r && m_nodes[2*i].r == m_nodes[2*i+1].l-1 && m_nodes[2*i+1].l <= m_nodes[2*i+1].r);
        m_nodes[i].l = m_nodes[2*i].l;
        m_nodes[i].r = m_nodes[2*i+1].r;
        m_nodes[i].v = m_nodes[2*i].v * m_nodes[2*i+1].v;
      }
    }
    void update(int x, int y)
    {
      int k = m_base + x - 1;
      if(k >= 2*m_sz)
        k -= m_sz;
      assert(m_nodes[k].l == x && m_nodes[k].r == x);
      m_nodes[k].v = (Fib()^y) + Fib(1, 0, 1);
      for(k >>= 1; k > 0; k >>= 1) {
        m_nodes[k].v = m_nodes[2*k].v * m_nodes[2*k+1].v;
      }
    }
    int query(int l, int r)
    {
      Fib res(1, 0, 1);
      int i = 1;
      assert(l >= m_nodes[1].l && r <= m_nodes[1].r && l <= r);
      while(m_nodes[i].l < m_nodes[i].r) {
        if(r <= m_nodes[2*i].r)
          i = 2*i;
        else if(l >= m_nodes[2*i+1].l)
          i = 2*i+1;
        else
          break;
      }
      if(l <= m_nodes[i].l && r >= m_nodes[i].r) {
        res = res * m_nodes[i].v;
      } else {
        int j = 2*i;
        while(1) {
          if(l <= m_nodes[j].l) {
            res = res * m_nodes[j].v;
            break;
          } else if(l <= m_nodes[2*j].r) {
            res = res * m_nodes[2*j+1].v;
            j = 2*j;
          } else {
            j = 2*j+1;
          }
        }
        j = 2*i+1;
        while(1) {
          if(r >= m_nodes[j].r) {
            res = res * m_nodes[j].v;
            break;
          } else if(r >= m_nodes[2*j+1].l) {
            res = res * m_nodes[2*j].v;
            j = 2*j+1;
          } else {
            j = 2*j;
          }
        }
      }
      return res.b;
    }
  private:
    int m_sz, m_base;
    vector<Node> m_nodes;
};

int main()
{
  int n, q;
  vector<int> v;
  scanf("%d%d", &n, &q);
  for(int i = 0; i < n; i++) {
    int x;
    scanf("%d", &x);
    v.push_back(x);
  }
  SegmentTree st(v);
  while(q--) {
    char type[5];
    int x, y;
    scanf("%s%d%d", type, &x, &y);
    switch(type[0]) {
      case 'C':
        st.update(x, y);
        break;
      case 'Q':
        printf("%d\n", st.query(x, y));
        break;
      default:
        assert(0);
    }
  }
  return 0;
}
