/*
 * =====================================================================================
 *
 *       Filename:  PalindromeTree.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年08月06日 19时32分54秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009

class PalindromeTree {
  struct Node {
    char c;
    int len;
    Node *child;
    Node *sib;
    Node *sflink;
    Node(int _len, char _c = -1) : len(_len), c(_c), child(NULL), sib(NULL), sflink(NULL) {}
    inline Node *find_child(int _c, bool exact = true) {
      if(child == NULL || child->c > _c)
        return NULL;
      for(Node *p = child; p; p = p->sib)
        if(p->sib == NULL || p->sib->c > _c)
          return (!exact || p->c == _c ? p : NULL);
      return NULL;
    }
    inline void add_child(Node *ch, Node *pre = NULL) {
      Node *p = pre?pre:child;
      if(p == NULL || p->c > ch->c) {
          ch->sib = child;
          child = ch;
      } else {
        while(p->sib && p->sib->c <= ch->c)
          p = p->sib;
        ch->sib = p->sib;
        p->sib = ch;
      }
    }
  };
  public:
    PalindromeTree(const char *s = ""), m_re(0), m_ro(-1), m_s("") {
      m_re.sflink = &m_ro;
      m_act = &m_re;
      while(*s)
        add_char(*s++);
    }
  private:
    // return if new palindrome suffix is encountered
    inline bool add_char(char c) {
      m_s += c;
      Node *p = m_act;
      while(p->len >= 0 && m_s[m_s.length()-p->len-1] != c)
        p = p->sflink;
      Node *q = p->find_child(c, false);
      if(q && q->c == c) {
        m_act = q;
        return false;
      } else {
        m_nodes.push_back(Node(p->len+2, c));
        Node *x = m_nodes.back();
        p->add_child(x, q);
        Node *y = m_act->;
        m_act = x;
        return true;
      }
    }
    Node m_re, m_ro;
    Node *m_act;
    vector<Node> m_nodes;
    string m_s;
};

int main()
{
  return 0;
}


