from random import seed, randint, random, uniform
seed()

lst = list()
while len(lst) == 0:
    L = randint(10**4, 10**5)
    p = uniform(0.1, 0.9)
    for x in range(L):
        if random() < p:
            lst.append(x)
print len(lst)
print ' '.join(map(str, lst))


