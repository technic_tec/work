/*
 * =====================================================================================
 *
 *       Filename:  CAOS.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年09月10日 16时55分18秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 510

int r, c;
char b[N][N];
bool isp[N];
int pc[N], cl[N][N], cr[N][N], cu[N][N], cd[N][N];

int main()
{
    int t;
    scanf("%d", &t);
    std::fill(isp, isp+N, true);
    isp[0] = isp[1] = false;
    pc[0] = pc[1] = false;
    for(int i = 2; i < N; i++) {
        if(!isp[i]) {
            pc[i] = pc[i-1];
            continue;
        }
        pc[i] = pc[i-1] + 1;
        for(int j = i*i; j < N; j += i)
            isp[j] = false;
    }
    while(t--) {
        scanf("%d%d", &r, &c);
        for(int i = 0; i < r; i++)
            scanf("%s", b[i]);
        for(int i = 0; i < r; i++)
            for(int j = 0; j < c; j++)
                if(b[i][j] == '#')
                    cl[i][j] = cu[i][j] = -1;
                else {
                    cl[i][j] = (j == 0 ? 0 : cl[i][j-1] + 1);
                    cu[i][j] = (i == 0 ? 0 : cu[i-1][j] + 1);
                }
        for(int i = r-1; i >= 0; i--)
            for(int j = c-1; j >= 0; j--)
                if(b[i][j] == '#')
                    cr[i][j] = cd[i][j] = -1;
                else {
                    cr[i][j] = (j == c-1 ? 0 : cr[i][j+1] + 1);
                    cd[i][j] = (i == r-1 ? 0 : cd[i+1][j] + 1);
                }
        int res = 0;
        for(int i = 0; i < r; i++)
            for(int j = 0; j < c; j++) {
                if(b[i][j] == '#') continue;
                int cnt = std::min(std::min(cl[i][j], cr[i][j]), std::min(cu[i][j], cd[i][j]));
                //if(cnt >= 2)
                //    ++res;
                res += pc[cnt];
            }
        printf("%d\n", res);
    }
    return 0;
}

