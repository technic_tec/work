/*
* PPP07D-judge, Adrian Kosowski, 15.12.2007
* http://www.spoj.pl/PP2007/problems/PPP07D/
* 
* THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
* INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
* OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
* OF SUCH DAMAGE.
* 
* PROJEKT PP2007 - dzialania dozwolone:
* - wykorzystanie sedziego do testowania programow
* - wprowadzanie zmian i udostepnianie zmodyfikowanych wersji sedziego
* - wzorowanie sie na niektorych fragmentach sedziego (na wlasna odpowiedzialnosc)
* 
* PROJEKT PP2007 - dzialania niedozwolone:
* - bezposrednie uzycie fragmentow kodu sedziego we wlasnych programach
* - wykorzystanie ewentualnych niezgodnosci pomiedzy dzialaniem sedziego a specyfikacja
*   zadania do uzyskania lepszego wyniku z projektu
* 
* Ewentualne pytania prosze kierowac na forum
* http://www.spoj.pl/forum-gut/viewforum.php?f=20
*/

#include <cstdio>
#include <cctype>
#include <unistd.h>
#include <list>
#include <set>
#include <cstring>
#include <string>
#include <sstream>

using namespace std;

/* Funkcja pomocnicza konwersji typow wspierajacych operacje strumieniowe
   do typu string */
template <class T>
inline string to_string (const T& t)
{
	stringstream ss;
	ss << t;
	return ss.str();
}

/*
"Modul 1": Obsluga logiki gry
*/

typedef pair<int,int> point;
typedef list<string> textlines;

/* Struktura opisujaca jeden z kierunkow N, S, E, W */
struct direction
{
	point value;

	friend istream& operator >> (istream& is, direction& d)
	{
		string s;
		is >> s;
		if (s=="north") d.value = point(0,-1);
		else if (s=="south") d.value = point(0,1);
		else if (s=="east") d.value = point(1,0);
		else if (s=="west") d.value = point(-1,0);
		else is.setstate(is.failbit);
		return is;
	}
};

/* Klasa logiki labiryntu */
class maze
{
private:
	set<point> walls; // zbior punktow stanowiacych elementy scian
	point ex, loc; // wyjscie, polozenie gracza

	/* Pobranie wspolrzednych najmniejszego prostokata H-V zawierajacego labirynt:
	   (lewy gorny rog, prawy dolny rog) */
	pair<point,point> get_bounds()
	{
		if(!walls.size()) return pair<point,point>();
		point tl = *walls.begin(), br = *walls.begin();
		for (set<point>::iterator it = walls.begin(); it!=walls.end(); it++)
		{
			tl.first  = min (tl.first, it->first);
			tl.second = min (tl.second, it->second);
			br.first  = max (br.first, it->first);
			br.second = max (br.second, it->second);
		}
		return pair<point,point>(tl,br);
	}

	/* Pobranie reprezentacji tekstowej fragmentu labiryntu w prostokacie H-V:
	   tl - lewy gorny rog, br - prawy dolny rog */	
	textlines get_view(point tl, point br)
	{
		textlines view;
		for (int y = tl.second; y<= br.second; y++)
		{
			string s;
			for (int x = tl.first; x<= br.first; x++)
			{
				if (point(x,y)==loc) s.push_back('O');
				else if (point(x,y)==ex) s.push_back('E');
				else if (walls.count(point(x,y))) s.push_back('X');
				else s.push_back('.');
			}
			view.push_back(s);
		}
		return view;
	}

public:
	/* Konstruktor przepisujacy argumenty, wartosci domyslne puste lub zerowe */
	maze(point ex=point(), point loc=point(), set<point> walls = set<point>())
	{
		this->ex = ex;
		this->loc = loc;
		this->walls = walls;
	};

	/* Wypelnienie podanego prostokata H-V w calosci scianami
	   tl - lewy gorny rog, br - prawy dolny rog */
	void set_range (point tl, point br)
	{
		for (int y = tl.second; y<= br.second; y++)
			for (int x = tl.first; x<= br.first; x++)
				walls.insert(point(x,y));
	}

	/* Wypelnienie podanego prostokata H-V w calosci pustymi polami
	   tl - lewy gorny rog, br - prawy dolny rog */
	void clear_range (point tl, point br)
	{
		for (int y = tl.second; y<= br.second; y++)
			for (int x = tl.first; x<= br.first; x++)
				walls.erase(point(x,y));
	}

	/* Pobranie pola powierzchni najmniejszego prostokata H-V zawierajacego labirynt */
	unsigned long long area()
	{
		pair<point,point> bounds = get_bounds();
		return (unsigned long long) (bounds.second.first -  bounds.first.first + 1) *
			   (unsigned long long) (bounds.second.second - bounds.first.second + 1);
	}

	/* Pobranie reprezentacji tekstowej calego labiryntu
	   (w najmniejszym otaczajacym prostokacie H-V) */
	operator textlines()
	{
		pair<point,point> bounds = get_bounds();
		return get_view (bounds.first, bounds.second);
	}

	/* Przesuniecie gracza w kierunku dir o odleglosc dist;
	   wartosc zwracana true <-> operacja zakonczona pomyslnie */
	bool walk(direction dir, int dist)
	{
		if (dir.value==point()) return false;
		point nloc;
		for (int i=1; i<=dist; i++)
		{
			nloc = point (loc.first+i*dir.value.first, loc.second+i*dir.value.second);
			if (walls.count(nloc) && nloc!=ex)
				return false;
		}
		loc = nloc;
		return true;
	}

	/* Pobranie reprezentacji tekstowej labiryntu w promieniu radius (metryka max(Dx,Dy)) wokol gracza.
	   Opcjonalnie, zasieg widocznosci w kierunku dir moze byc wiekszy i rowny dist*/
	textlines get_local_view(direction dir=direction(), int dist=1, int radius=1)
	{
		int i;
		for (i=1; i<dist; i++)
			if (walls.count(point(loc.first+i*dir.value.first, loc.second+i*dir.value.second)))
				break;
		point locd = point(loc.first + (i-radius)*dir.value.first, loc.second+ (i-radius)*dir.value.second);
		point tl = point (min(loc.first-radius, locd.first-radius), min(loc.second-radius, locd.second-radius));
		point br = point (max(loc.first+radius, locd.first+radius), max(loc.second+radius, locd.second+radius));
		return get_view (tl, br);
	}

	/* Usuniecie scian labiryntu w promieniu radius (metryka max(Dx,Dy)) wokol gracza */
	void clear_local_range (int radius=1)
	{
		clear_range(point(loc.first-radius,loc.second-radius),point(loc.first+radius,loc.second+radius));
	}

	/* Sprawdzenie, czy gracz stoi w punkcie reprezentujacym wyjscie */
	bool has_reached_exit()
	{
		return loc == ex;
	}
};

/* Klasa okreslajaca zasoby pozostajace do dyspozycji gracza */
class resources
{
private:
	int dynamite, oxygen, moves; // pozostaly dynamit, tlen, liczba zapytan do systemu

	/* Sprawdzenie, czy stan zasobow jest nieujemny (nie wyczerpano zasobow) */
	bool state_ok()
	{
		return dynamite>=0 && oxygen>=0 && moves>=0;
	}

public:
	/* Konstruktor; wartosci domyslne wedlug specyfikacji w tresci zadania */
	resources (int dynamite=0, int oxygen=10000000, int moves=10000)
	{
		this->dynamite=dynamite;
		this->oxygen=oxygen;
		this->moves=moves;
	}

	/* Pobranie opisu dot. ilosci pozostalego dynamitu */
	string get_dynamite_string()
	{
		return to_string(dynamite) + (dynamite==1?" stick":" sticks")+" of dynamite";
	}

	/* Pobranie opisu dot. ilosci pozostalego tlenu */
	string get_oxygen_string()
	{
		return to_string(oxygen) + (oxygen==1?" unit":" units")+" of oxygen";
	}

	/* Pobranie opisu dot. ilosci pozostalych komunikatow */
	string get_moves_string()
	{
		return to_string(moves) + (moves==1?" move":" moves");
	}

	/* Ksiegowanie kosztow przejscia w labiryncie odleglosci dist;
	   wartosc zwracana falsz <-> przekroczenie limitu zasobow */
	bool walk (int dist)
	{
		moves--;
		oxygen -= 10*dist;
		return state_ok();
	}

	/* Ksiegowanie kosztow uzyskania widoku okolicy view;
	   wartosc zwracana falsz <-> przekroczenie limitu zasobow */
	bool look (const textlines & view)
	{
		moves--;
		oxygen -= view.size()*view.begin()->size();
		return state_ok();
	}

	/* Ksiegowanie kosztow uzycia laski dynamitu;
	   wartosc zwracana falsz <-> przekroczenie limitu zasobow */
	bool use_dynamite()
	{
		moves--;
		dynamite--;
		oxygen -= 1000;
		return state_ok();
	}

	/* Ograniczenie podanego zasiegu widocznosci dist; wartosc zwracana jest w razie potrzeby przycinana do
	   pewnego gornego ograniczenie, ktorego osiagniecie oznaczaloby przekroczenie limitu zasobow */
	int trim_viewrange (int dist, int radius=1)
	{
		return min(dist, oxygen/(2*radius+1));
	}
	
	/* Uzyskanie reprezentacji tekstowej stanu zasobow */
	operator string()
	{
		if (state_ok())
			return get_dynamite_string()+", "+get_oxygen_string()+", and "+get_moves_string()+" remaining.";
		string s = "depleted:";
		if (dynamite < 0) s+=" dynamite";
		if (oxygen < 0) s+=" oxygen";
		if (moves < 0) s+=" moves";
		s+=".";
		return s;
	}
};

/* Funkcja realizujaca logike rozgrywki. Parametry: mz - labirynt, rs - stan zasobow, read_f - wskaznik do funkcji
   odbierajacej dane (string) od klienta, write_f - wskaznik do funkcji piszacej dane (textlines) do klienta.
   Wartosc zwracana true <-> rozgrywka zakonczona zwyciestwem. */
bool perform_gameplay (maze &mz, resources &rs, string(*read_f)(), void(*write_f)(const textlines&))
{
	textlines g_msg; 
	g_msg.push_back("You wake up with a headache, completely lost in a dark maze.");
	g_msg.push_back("In your pockets you find a torch and " + rs.get_dynamite_string() + ".");
	// Komunikat powitalny
	write_f(g_msg); 
	while (true)
	{
		// Odbior polecenia
		string input_s = read_f();
		stringstream input;
		input.str(input_s);
		string keyword;
		input >> keyword;
		// Logika przetwarzania polecenia "walk dir dist"
		if (keyword == "walk")
		{
			direction dir;
			int dist = 0;
			input >> dir >> dist;
			if (dist <=0 || input.bad())
			{
				write_f(textlines(1,"STOP. Invalid walk command."));
				return false;
			}
			if (! rs.walk(dist))
			{
				write_f(textlines(1,"STOP. Resources " + string(rs)));
				return false;
			}
			if (!mz.walk(dir, dist))
			{
				write_f(textlines(1,"STOP. You have bumped into a wall."));
				return false;
			}
			if (mz.has_reached_exit())
			{
				write_f(textlines(1,"Against all odds, you have found the exit. Congratulations!"));
				return true;
			}
			write_f(textlines(1,"You are still lost in a dark maze."));
		}
		// Logika przetwarzania polece� "look around" i "look dir dist"
		else if (keyword == "look")
		{
			textlines view;
			if (input_s=="look around")
				view = mz.get_local_view();
			else
			{
				direction dir;
				int dist = 0;
				input >> dir >> dist;
				if (dist <=0 || input.bad())
				{
					write_f(textlines(1,"STOP. Invalid look command."));
					return false;
				}
				dist = rs.trim_viewrange(dist);
				view = mz.get_local_view(dir, dist);
			}
			if (! rs.look(view))
			{
				write_f(textlines(1,"STOP. Resources " + string(rs)));
				return false;
			}
			view.push_front("The maze around you looks like this:");
			write_f(view);
		}
		// Logika przetwarzania polecenia "use dynamite"
		else if (input_s=="use dynamite")
		{
			if (! rs.use_dynamite())
			{
				write_f(textlines(1,"STOP. Resources " + string(rs)));
				return false;
			}
			mz.clear_local_range();
			textlines d_msg; 
			d_msg.push_back("You light up a stick of dynamite.");
			d_msg.push_back("The explosion knocks you to the ground, but amazingly you survive.");
			write_f(d_msg);
		}
#ifndef ONLINE_JUDGE
		// Logika przetwarzania polecen diagnostycznych "show maze" i "show resources", niedostepnych
		// w systemie sprawdzajacym
		else if (input_s=="show maze")
		{
			unsigned long long area = mz.area();
			textlines m_msg;
			if (area < (1<<20))
				m_msg = mz;			
			m_msg.push_front ("Maze area: "+to_string(area));
			write_f(m_msg);
		}
		else if (input_s=="show resources")
			write_f(textlines(1,"Current resources: "+string(rs)));
#endif
		else
		{
			write_f(textlines(1,"STOP. Invalid command."));
			return false;
		}
	}
	return true;	
}

/*
"Modul 2": Realizacja komunikacji na poziomie systemowym.
*/

/* Realizacja funkcji do odbioru komunikatow od programu testowanego. Kopia komunikatu jest logowana na stderr */
string read_stdin_echoed()
{
	const int BF_SIZE = 128;
	char c[BF_SIZE];
	if (!fgets (c, BF_SIZE, stdin)) return string();
	for (int k = strlen(c)-1; k>=0 && isspace(c[k]); k--)
		c[k]=0;
	fprintf (stderr, "< %s\n", c);
	return string(c);
}

/* Realizacja funkcji do wysy�ania komunikatow do programu testowanego. Kopia komunikatu jest logowana na stderr */
void write_stdout_echoed (const textlines & vs)
{
	for (textlines::const_iterator it = vs.begin(); it!=vs.end(); it++)
	{
		fprintf (stderr, "> %s\n", it->c_str());
		puts(it->c_str());
	}
	fflush(stdout);
}

/* Uruchomienie programu exec_path w oddzielnym procesie i sprzegniecie (na krzyz)
   standardowego wejscia/wyjscia biezacego procesu ze standardowym wyjsciem/wejsciem nowego procesu.
   Wartosc zwracana false oznacza wykryte niepowodzenie; niektore bledy nie sa sygnalizowane. */
bool redirect_stdin_stdout(char* exec_path)
{
	int pipes[2][2];
	//   judge.stdout = pipes[1][1] -> pipes[1][0] = program.stdin
	// program.stdout = pipes[0][1] -> pipes[0][0] = judge.stdin
	for (int i=0; i<2; i++)
		if (pipe(pipes[i])==-1) return false;
	int fres = fork();
	if(fres==-1) return false;
	for (int j=0; j<2; j++) // j=0: input, j=1: output
	{
		close(j);
		dup2 (pipes[(!fres)^j][j],j);
		close(pipes[0][j]);
		close(pipes[1][j]);
	}
	if (!fres)
		if (execlp(exec_path, exec_path, NULL)==-1)
			return false;
	return true;
}

int main(int argc, char **argv)
{
	// Minimalistyczne sprawdzenie poprawnosci wywolania
	if (argc<2 || argc>3)
	{
		fprintf (stderr, "Usage: PPP07D-judge input_file [tested_program]\n");
		return 0;
	}
	// Uruchomienie testowanego programu
	if (argc==3 && !redirect_stdin_stdout(argv[2]))
		perror ("Could not establish communication with tested program");
	// Otwarcie pliku z mapa
	FILE *map_data = fopen (argv[1], "rt");
	if (!map_data)
		perror ("Could not read map data");

	int t; // Przetwarzanie przypadkow testowych
	fscanf (map_data, "%d", &t);
	write_stdout_echoed(textlines(1,to_string(t)));
	int score = 0;
	for (int ti=1; ti<=t; ti++)
	{
		fprintf (stderr, "*** Test #%d ***\n", ti);
		// Wczytywanie danych mapy
		int k, dn;
		point ex, loc;
		fscanf (map_data, "%d%d%d%d%d%d", &k, &(ex.first), &(ex.second), &(loc.first), &(loc.second), &dn);
		maze mz(ex, loc);
		resources rs(dn);
		while (k--)
		{
			point tl, br;
			fscanf (map_data, "%d%d%d%d", &(tl.first), &(tl.second), &(br.first), &(br.second));
			mz.set_range(tl, br);
		}
		// Uruchomienie logiki gry
		bool result = perform_gameplay(mz, rs, read_stdin_echoed, write_stdout_echoed);
		fprintf(stderr, "*** Final resources: %s\n\n\n", string(rs).c_str());
		if (result) score++;
	}
	fprintf(stderr, "*** Overall score: %d/%d ***\n", score, t);
}

