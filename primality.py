from fractions import gcd
# if n < 1,373,653, it is enough to test a = 2 and 3;
# if n < 9,080,191, it is enough to test a = 31 and 73;
# if n < 4,759,123,141, it is enough to test a = 2, 7, and 61;
# if n < 1,122,004,669,633, it is enough to test a = 2, 13, 23, and 1662803;
# if n < 2,152,302,898,747, it is enough to test a = 2, 3, 5, 7, and 11;
# if n < 3,474,749,660,383, it is enough to test a = 2, 3, 5, 7, 11, and 13;
# if n < 341,550,071,728,321, it is enough to test a = 2, 3, 5, 7, 11, 13, and 17.
# if n < 3,825,123,056,546,413,051, it is enough to test a = 2, 3, 5, 7, 11, 13, 17, 19, and 23.
def power_mod(n, k, p):
    r = 1
    while k > 0:
        if k & 1:
            r = r * n % p
        n = n * n % p
        k >>= 1
    return r

def witness(n, p):
    t, k = n-1, 0
    while (t & 1) == 0:
        t >>= 1
        k += 1
    r = power_mod(p, t, n)
    if r == 1 or r == n-1:
        return False
    for _ in range(k):
        r = r * r % n
        if r == n-1:
            return False
        elif r == 1:
            return True
    return True

def is_prime(n):
    if n in [0, 1]:
        return False
    for p in [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]:
        if n == p:
            return True
        elif n % p == 0:
            return False
    if n < 1373653L:
        l = [2, 3]
    elif n < 9080191L:
        l = [31, 73]
    elif n < 4759123141L:
        l = [2, 7, 61]
    elif n < 1122004669633L:
        l = [2, 13, 23, 1662803]
    elif n < 2152302898747L:
        l = [2, 3, 5, 7, 11]
    elif n < 3474749660383L:
        l = [2, 3, 5, 7, 11, 13]
    elif n < 341550071728321L:
        l = [2, 3, 5, 7, 11, 13, 17]
    elif n < 3825123056546413051L:
        l = [2, 3, 5, 7, 11, 13, 17, 19, 23]
    else:
        raise ValueError
    for p in l:
        if witness(n, p):
            return False
    return True

def prev_prime(n):
    if n > 3 and (n & 1)==0:
        n -= 1
    while n > 0:
        if is_prime(n):
            return n
        n -= 2

def pollard_rho(n):
    x, y, d, c = 2, 2, 1, 1
    while True:
        while d == 1:
            x = (x*x+c)%n
            y = (y*y+c)%n
            y = (y*y+c)%n
            d = gcd(abs(x - y),  n)
        if d == n:
            x, y, d, c = 2, 2, 1, c+2
        else:
            return d

def factor(n):
    k = 0
    while (n & 1) == 0:
        n >>= 1
        k += 1
    if n == 1:
        return [2]*k
    elif is_prime(n):
        return [2]*k + [n]
    else:
        x = pollard_rho(n)
        return [2]*k + factor(x)+factor(n/x)

def euler_phi(n):
    f = sorted(factor(n))
    i = 0
    r = 1
    while i < n:
        r *= f[i]-1
        while i+1 < n and f[i+1] == f[i]:
            i += 1
            r *= f[i]
        i += 1
    return r

def main():
    t = int(raw_input())
    for _ in range(t):
        n = long(raw_input())
        print prev_prime(n)
if __name__ == '__main__':
    main()
