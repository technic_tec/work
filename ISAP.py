# -*- coding: utf8 -*-

##algorithm Improved-Shortest-Augmenting-Path
##1 f <-- 0
##2 从终点 t 开始进行一遍反向 BFS 求得所有顶点的起始距离标号 d(i)
##3 i <-- s
##4 while d(s) < n do
##5     if i = t then    // 找到增广路
##6         Augment
##7         i <-- s      // 从源点 s 开始下次寻找
##8     if Gf 包含从 i 出发的一条允许弧 (i,j)
##9         then Advance(i)
##10         else Retreat(i)    // 没有从 i 出发的允许弧则回退
##11 return f

##procedure Advance(i)
##1 设 (i,j) 为从 i 出发的一条允许弧
##2 pi(j) <-- i    // 保存一条反向路径，为回退时准备
##3 i <-- j        // 前进一步，使 j 成为当前结点

##procedure Retreat(i)
##1 d(i) <-- 1 + min{d(j):(i,j)属于残量网络Gf}    // 称为重标号的操作
##2 if i != s3     then i <-- pi(i)    // 回退一步

##procedure Augment
##1 pi 中记录为当前找到的增广路 P
##2 delta <-- min{rij:(i,j)属于P}
##3 沿路径 P 增广 delta 的流量
##4 更新残量网络 Gf

##GAP 优化:
##由于从 s 到 t 的一条最短路径的顶点距离标号单调递减，且相邻顶点标号差严格等于1，
##因此可以预见如果在当前网络中距离标号为 k (0 <= k < n) 的顶点数为 0，
##那么可以知道一定不存在一条从 s 到 t 的增广路径，此时可直接跳出主循环。