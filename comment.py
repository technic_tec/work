
# Enter your code here. Read input from STDIN. Print output to STDOUT
import re,sys
mlc = False
ans = ''
for l in sys.stdin.readlines():
    if mlc:
        #inside multi line comment
        k = l.find("*/")
        if k < 0:
            ans += l.strip() + "\n"
        else:
            ans += l[:k+2].strip()
            mlc = False
    else:
        i = l.find("//")
        j = l.find("/*")
        if i >= 0 and (j < 0 or j > i):
            #single line comment
            ans += l[i:]+'\n'
        elif j >= 0 and (i < 0 or i > j):
            #multi line comment
            k = l.find("*/", j+2)
            if k < 0:
                ans += l[j:] + "\n"
                mlc = True
            else:
                ans += l[j:k+2]
print ans
