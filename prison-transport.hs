import Control.Monad
import Data.Graph
import Data.List
import Data.Array
import GHC.Float

treeSize :: Tree Vertex -> Int
treeSize (Node x s) = 1 + (sum (map treeSize s))

main :: IO()
main = do
  s <- getLine
  let n = read s
  s <- getLine
  let m = read s
  es <- replicateM m getLine
  let e = map ((\x->(x!!0, x!!1)).(map read.words)) es
      g = buildG (1, n) e
      c = components g
  putStrLn $ show $ sum $ map (\x->ceiling (sqrt (int2Double (treeSize x)))) c
