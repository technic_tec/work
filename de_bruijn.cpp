#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 1000
#define M 1000100
int a[N], sl;
char dbs[6][M];

void db(int t, int p, int n, int k, char seq[])
{
    if (t > n) {
        if(n % p == 0)
            for (int j = 1; j <= p; j++)
                seq[sl++] = '0'+a[j];
    } else {
        a[t] = a[t - p];
        db(t + 1, p, n, k, seq);
        for (int j = a[t - p] + 1; j < k; j++) {
            a[t] = j;
            db(t + 1, t, n, k, seq);
        }
    }
}
void de_bruijn(int k, int n, char dbs[])
{
    // De Bruijn Sequence for alphabet size k 
    // and subsequences of length n.
    memset(a, 0, sizeof(a));
    sl = 0;
    db(1, 1, n, k, dbs);
    for(int i = 0; i < n-1; i++)
        dbs[sl++] = dbs[i];
    dbs[sl++] = 0;
}

int main()
{
    for (int i = 1; i <= 6; i++)
        de_bruijn(10, i, dbs[i-1]);

    int n;
    while(scanf("%d", &n) == 1 && n > 0) {
        printf("%s\n", dbs[n-1]);
    }
    return 0;
}
