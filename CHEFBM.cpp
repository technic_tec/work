#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 100000

int n, m, p;

struct Inc {
    int x, y;
    bool operator<(const Inc &o) const {
        return (x < o.x || (x == o.x && y < o.y));
    }
} op[N];

int main()
{
    scanf("%d%d%d", &n, &m, &p);
    for(int i = 0; i < p; i++)
        scanf("%d%d", &op[i].x, &op[i].y);
    std::sort(op, op+p);
    int j = 0;
    for(int i = 1; i <= n; i++) {
        int d = m-1;
        int lv = 0, li = -1;
        while(j < p && op[j].x == i) {
            int c = 1;
            while(j+1 < p && op[j+1].x == i && op[j+1].y == op[j].y) {
                ++j; ++c;
            }
            if((op[j].y - li == 1 && c < lv-1) || (op[j].y - li > 1 && li > 0 && lv > 1)) {
                d = -1;
                while(j < p && op[j].x == i)
                    ++j;
                break;
            }
            if(op[j].y == 1)
                d -= c;
            if(op[j].y == m)
                d += c;
            lv = c; li = op[j].y;
            ++j;
        }
        if(d >= 0 && li > 0 && li < m && lv > 1)
            d = -1;
        printf("%d\n", d);
    }
    return 0;
}

