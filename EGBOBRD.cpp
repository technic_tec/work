/*
 * =====================================================================================
 *
 *       Filename:  EGBOBRD.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月05日 18时37分24秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009

int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    int n;
    long long k, s;
    scanf("%d%lld", &n, &k);
    s = 0;
    for(int i = 0; i < n; i++) {
      int x;
      scanf("%d", &x);
      s += x;
      if(s % k) ++s;
    }
    printf("%lld\n", (s+k-1)/k);
  }
  return 0;
}


