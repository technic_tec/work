/*
 * =====================================================================================
 *
 *       Filename:  CHEFPC.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016/03/10 21:34:58
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), technic.tec@gmail.com
 *   Organization:  Verisilicon, Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;
#define N 100000000
#define P 1000000007

const double pi = acos(-1.0);
const double eps = 1e-8;
class Seg;
class Arc;
class Edge {
  friend class Poly;
  public:
  Edge(double sx, double sy, double tx, double ty)
  : m_sx(sx), m_sy(sy), m_tx(tx), m_ty(ty) {}
  virtual vector<pair<double,double> > intersect(Seg *seg) = 0;
  virtual vector<pair<double,double> > intersect(Arc *arc) = 0;
  virtual pair<double,double> get(double t) = 0;
  virtual double lookup(pair<double,double> p) = 0;
  virtual double area() const = 0;
  public:
  double m_sx, m_sy, m_tx, m_ty;
};
class Seg : public Edge {
  friend class Poly;
  public:
  Seg(double sx, double sy, double tx, double ty) : Edge(sx,sy,tx,ty) {}
  virtual vector<pair<double,double> > intersect(Seg *seg) {
    vector<pair<double,double> > res;
    //  u*(m_tx-m_sx) - v*(seg->m_tx-seg->m_sx) = seg->m_sx - m_sx
    //  u*(m_ty-m_sy) - v*(seg->m_ty-seg->m_sy) = seg->m_sy - m_sy
    double q = (m_tx-m_sx)*(seg->m_ty-seg->m_sy)-(m_ty-m_sy)*(seg->m_tx-seg->m_sx);
    if(fabs(q) < eps)
      return res;
    double u = ((seg->m_sx - m_sx)*(seg->m_ty-seg->m_sy)-(seg->m_sy - m_sy)*(seg->m_tx-seg->m_sx))/q;
    double v = ((m_ty-m_sy)*(seg->m_sx - m_sx)-(m_tx-m_sx)*(seg->m_sy - m_sy))/q;
    if(u > -eps && u < 1.0+eps && v > -eps && v < 1.0+eps)
      res.push_back(make_pair(u,v));
    return res;
  }
  virtual vector<pair<double,double> > intersect(Arc *arc);
  virtual pair<double,double> get(double t) {
    return make_pair((1.0-t)*m_sx + t*m_tx, (1.0-t)*m_sy + t*m_ty);
  }
  virtual double lookup(pair<double,double> p) {
    return (p.first-m_sx)/(m_tx-m_sx);
  }
  virtual double area() const {
    //return (m_sy+m_ty)*(m_tx-m_sx)/2.0;
    return (m_sx*m_ty-m_sy*m_tx)/2.0;
  }
  private:
};
class Arc : public Edge {
  friend class Poly;
  public:
  Arc(double ox, double oy, double r, double t1, double t2)
  : m_ox(ox), m_oy(oy), m_r(r), m_t1(t1), m_t2(t2),
    Edge(ox+r*cos(t1),oy+r*sin(t1),ox+r*cos(t2),oy+r*sin(t2)) {}
  virtual vector<pair<double,double> > intersect(Seg *seg){
    vector<pair<double,double> > res;
    // (seg->m_sx-m_ox + t*(seg->m_tx-seg->m_sx))^2 + (seg->m_sy-m_oy + t*(seg->m_ty-seg->m_sy))^2 = m_r^2;
    double A = (seg->m_tx-seg->m_sx)*(seg->m_tx-seg->m_sx) + (seg->m_ty-seg->m_sy)*(seg->m_ty-seg->m_sy);
    double B = (seg->m_tx-seg->m_sx)*(seg->m_sx-m_ox) + (seg->m_ty-seg->m_sy)*(seg->m_sy-m_oy);
    double C = (seg->m_sx-m_ox)*(seg->m_sx-m_ox) + (seg->m_sy-m_oy)*(seg->m_sy-m_oy) - m_r*m_r;
    double D = B*B-A*C;
    if(D < eps) return res;
    D = sqrt(D);
    double t1 = -B-D, t2 = -B+D;
    if(t1 >= -eps && t1 <= 1.0+eps) {
      double u1 = lookup(seg->get(t1));
      if((u1 >= m_t1-eps && u1 <= m_t2+eps) || u1+2*pi <= m_t2 + eps)
        res.push_back(make_pair(u1, t1));
    }
    if(t2 >= -eps && t2 <= 1.0+eps) {
      double u2 = lookup(seg->get(t2));
      if((u2 >= m_t1-eps && u2 <= m_t2+eps) || u2+2*pi <= m_t2 + eps)
        res.push_back(make_pair(u2, t2));
    }
    return res;
  }
  virtual vector<pair<double,double> > intersect(Arc *arc) {
    vector<pair<double,double> > res;
    double d = (m_ox - arc->m_ox)*(m_ox - arc->m_ox) + (m_oy - arc->m_oy) * (m_oy - arc->m_oy);
    if(d >= (m_r + arc->m_r)*(m_r + arc->m_r) - eps)
      return res;
    return res;
  }
  virtual pair<double,double> get(double t) {
    return make_pair(m_ox+m_r*cos(t), m_oy+m_r*sin(t));
  }
  virtual double lookup(pair<double,double> p) {
    double t = atan2(p.second - m_oy, p.first - m_ox);
    return (t<-eps ? t+2*pi : t);
  }
  virtual double area() const {
    //return m_oy*m_r*(cos(m_t2)-cos(m_t1))+m_r*m_r*((sin(2*m_t2)-sin(2*m_t1))/4-(m_t2-m_t1)/2);
    return m_r*(m_ox*(sin(m_t2)-sin(m_t1))-m_oy*(cos(m_t2)-cos(m_t1))+m_r*(m_t2-m_t1))/2.0;
  }
  private:
  double m_ox, m_oy, m_r, m_t1, m_t2;
};
vector<pair<double,double> > Seg::intersect(Arc *arc) {
  vector<pair<double,double> > res = arc->intersect(this);
  for(int i = 0; i < res.size(); i++)
    res[i] = make_pair(res[i].second,res[i].first);
  return res;
}
class Poly {
  public:
  Poly(int n = 3) {
    m_e.reserve(n);
  }
  virtual ~Poly() {
    while(!m_e.empty()) {
      Edge *e = m_e.back();
      m_e.pop_back();
      delete e;
    }
  }
  void addVertex(double x, double y) {
    if(m_e.size() == 0) {
      Seg *s = new Seg(x,y,x,y);
      m_e.push_back(s);
    } else {
      Seg *s = dynamic_cast<Seg *>(m_e.back());
      if(fabs((s->m_tx-s->m_sx)*(y-s->m_ty)-(s->m_ty-s->m_sy)*(x-s->m_tx))<=eps) {
        s->m_tx = x; s->m_ty = y;
      } else {
        m_e.push_back(new Seg(s->m_tx,s->m_ty,x,y));
      }
    }
  }
  void finalize() {
    Seg *ex = dynamic_cast<Seg *>(m_e.front());
    addVertex(ex->m_sx,ex->m_sy);
    Seg *ey = dynamic_cast<Seg *>(m_e.back());
    if(fabs((ey->m_tx-ey->m_sx)*(ex->m_ty-ex->m_sy)-(ey->m_ty-ey->m_sy)*(ex->m_tx-ex->m_sx))<=eps) {
      ex->m_sx = ey->m_sx; ex->m_sy = ey->m_sy;
      m_e.pop_back();
      delete ey;
    }
    printf("area: %lf\n", area());
  }
  vector<Poly *> cut(Arc a) const {
    vector<Poly *> ps;
    return ps;
  }
  double area() const {
    double res = 0.0;
    for(int i = 0; i < m_e.size(); i++)
      res += m_e[i]->area();
    return res;
  }
  private:
  vector<Edge *> m_e;
};
int main()
{
	int m;
	scanf("%d",  &m);
  vector<Arc> as;
	for(int i = 0; i < m; i++) {
    int x, y, r;
		scanf("%d%d%d", &x, &y, &r);
    as.push_back(Arc(x,y,r,0,2*pi));
	}
  int n;
	scanf("%d", &n);
  Poly *ply = new Poly(n);
	for(int i = 0; i < n; i++) {
		int x, y;
		scanf("%d%d", &x, &y);
    ply->addVertex(x,y);
	}
  ply->finalize();
  vector<Poly *> ps;
  ps.push_back(ply);
  for(int i = 0; i < m; i++) {
    vector<Poly *> nps;
    for(int j = 0; j < ps.size(); j++) {
      vector<Poly *> rps = ps[j]->cut(as[i]);
      nps.insert(nps.end(), rps.begin(), rps.end());
    }
    ps.swap(nps);
  }
  double res = 0.0;
  while(!ps.empty()) {
    Poly *px = ps.back();
    ps.pop_back();
    res += px->area();
    delete px;
  }
  printf("%lf\n", res);
  return 0;
}