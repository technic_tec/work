/*
 * =====================================================================================
 *
 *       Filename:  RIVPILE.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  07/14/13 19:21:34
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 260

struct Pile {
    int x, y;
} pile[N];
struct Disk {
    int r, c;
} disk[N];
int n, m, w;
int d[N*N];

int comp(const void *a, const void *b)
{
    if(((Disk*)a)->r != ((Disk*)b)->r)
        return ((Disk*)a)->r - ((Disk*)b)->r;
    else
        return ((Disk*)b)->c - ((Disk*)a)->c;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d%d", &n, &m, &w);
        for(int i = 0; i < n; i++)
            scanf("%d%d", pile[i].x, &pile[i].y);
        for(int i = 0; i < m; i++)
            scanf("%d%d", &disk[i].r, &disk[i].c);
        qsort(disk, n, sizeof(Disk), comp);
        int cc = 0;
        for(int i = 1; i < m; i++) {
            while(i-cc > 0 && disk[i].c <= disk[i-1-cc].c)
                ++cc;
            disk[i-cc] = disk[i];
        }
        m -= cc;
        heap_init();
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                if(pile[i].y <= disk[j].r)
                    d[i*m+j] = INF;
                else {
                    d[i*m+j] = disk[j].c;
                    heap_push(i*m+j);
                }
    }
    return 0;
}

