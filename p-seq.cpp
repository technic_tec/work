/*
 * =====================================================================================
 *
 *       Filename:  p-seq.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年04月24日 09时57分39秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define P 1000000007
#define N 100000

int n, p, m, c[N], v[2][N][2];
int main()
{
    scanf("%d%d", &n, &p);
    m = 0;
    for(int i = 1; i*i <= p; i++) {
        v[0][m][0] = v[0][m][1] = 1;
        c[m++] = p/i;
    }
    for(int i = 1; i <= n; i++) {
        int i1 = (i & 1);
        v[i1][0][1] = v[i1^1][0][0];
        for(int j = 1; j < m; j++) {
            v[i1][j][1] = v[i1][j-1][1] + v[i1^1][j][0];
            if(v[i1][j][1] >= P)
                v[i1][j][1] -= P;
        }
        v[i1][m-1][0] = (int)(((long long)v[i1][m-1][1] + (long long)(c[m-1] - m) * (long long)v[i1^1][m-1][1]) % P);
        for(int j = m-2; j >= 0; j--)
            v[i1][j][0] = (int)(((long long)v[i1][j+1][0] + (long long)(c[j]-c[j+1]) * (long long)v[i1^1][j][1]) % P);
    }
    printf("%d\n", v[n&1][0][0]);
    return 0;
}

