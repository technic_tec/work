/*
 * =====================================================================================
 *
 *       Filename:  ural2047.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月27日 15时26分50秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <numeric>
using namespace std;

const int N = 100000;
const int vmax = 200;
const int smax = 1600000;
vector<int> fac(smax, 0);
vector<int> cf(smax, 0);
vector<int> nf(smax, 0);
vector<int> c;
vector<int> v;
int ml = 0;
int nmax = 100000;

bool search(int n, int s)
{
  if(n > ml)
    ml = n;
  if(n == nmax)
    return true;
  for(int i = 2; i <= vmax && s+i < smax; i++)
    if(nf[s+i] == i) {
      v.push_back(i);
      if(search(n+1, s+i))
        return true;
      v.pop_back();
    }
  return false;
}

int main()
{
  fac[1] = nf[1] = 1; cf[1] = 0;
  for(int i = 2; i < smax; i++)
    if(!fac[i]) {
      fac[i] = i;
      nf[i] = 2;
      cf[i] = 1;
      if(i < 32768)
        for(int j = i*i; j < smax; j += i)
          if(!fac[j]) fac[j] = i;
    } else {
      int x = fac[i];
      int j = i/x;
      if(fac[j] != x) {
        nf[i] = nf[j] * 2;
        cf[i] = 1;
      } else {
        cf[i] = cf[j] + 1;
        nf[i] = nf[j] / cf[i] * (cf[i]+1);
      }
    }
  scanf("%d", &nmax);
  c.clear();
  c.push_back(0);
  int imax = 0;
  for(int i = 1; i < smax; i++) {
    c.push_back(nf[i] <= i ? c[i-nf[i]]+1 : 0);
    if(c[i] == nmax) {
      imax = i;
      break;
    }
  }
  assert(imax > 0);
  v.clear();
  while(imax > 0) {
    v.push_back(nf[imax]);
    imax -= nf[imax];
  }
  assert(v.size() == nmax);
  for(int i = nmax-1; i >= 0; i--)
    printf("%d%c", v[i], i>0?' ':'\n');
  return 0;
}
