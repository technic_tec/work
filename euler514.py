from fractions import gcd

def gen_coprime(N, l = (0, 1), r = (1, 0)):
  m = (l[0]+r[0], l[1]+r[1])
  if max(m) > N:
    return
  for x in gen_coprime(N, l, m):
    yield x
  #print m
  yield m
  for x in gen_coprime(N, m, r):
    yield x

def E(n, ans=None):
  c = 0
  for i, j in gen_coprime(n):
    c += (n+1-i)*(n+1-j)
    if 2*max(i, j) <= n+1:
      c -= (n+1-2*i)*(n+1-2*j)
  if 1 <= n:
    c += n*(n+1)
  if 2 <= n:
    c -= (n+1)*(n-1)
  c *= 2
  print '%d: %d %d' % (n, len(list(gen_coprime(n))), c)
  return c
      

E(1, 0.18750)
E(2, 0.94335)
E(10, 55.03013)
E(100)
