/*
 * =====================================================================================
 *
 *       Filename:  POTATOES.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年04月14日 10时48分00秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 3000

int np[N];
bool isp[N];

int main()
{
    memset(isp, true, sizeof(isp));
    isp[0] = isp[1] = false;
    for(int i = 2; i < N; i++) {
        if(!isp[i]) continue;
        for(int j = i*i; j < N; j += i)
            isp[j] = false;
    }
    np[N-1] = N;
    for(int i = N-2; i >= 0; i--)
        if(isp[i+1])
            np[i] = i+1;
        else
            np[i] = np[i+1];
    int t, x, y;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d", &x, &y);
        printf("%d\n", np[x+y]-x-y);
    }
    return 0;
}

