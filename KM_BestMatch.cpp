/*
 * =====================================================================================
 *
 *       Filename:  KM_BestMatch.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月31日 17时56分55秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009

int KM_Min(int n, vector<vector<int> > &w)
{
  // cl[i] + cr[j] <= w[i][j]
  // slack[j] = min(w[i][j]-cl[i]-cr[j], i in L^T) for j in R\T
  vector<int> u(n, -1);
  vector<int> v(n, -1);
  for(int k = 0; k < n; k++) {
    int x = 0;
    while(u[x] >= 0)
      ++x;
    while(!dfs(x))
      relabel();
    aug();
  }
}

int main()
{
  return 0;
}


