#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <deque>
#include <vector>
using namespace std;

#define INF 99999999

class Dinic {
    public:
        Dinic(int _nu)
        {
            nu = _nu;
            adj.clear();
            for(int i = 0; i < nu; i++)
                adj.push_back(-1);
            dst.clear();
            nxt.clear();
			flow.clear();
			cap.clear();
        }

        int add_edge(int _s, int _t, int _c = 1)
        {
			assert(_s < nu && _t < nu);
            int ne = dst.size();
            dst.push_back(_t);
            nxt.push_back(adj[_s]);
			flow.push_back(0);
			cap.push_back(_c);
            adj[_s] = ne;
            ne = dst.size();
            dst.push_back(_s);
            nxt.push_back(adj[_t]);
			flow.push_back(0);
			cap.push_back(0);
            adj[_t] = ne;
        }

        int maxflow(int s, int t)
        {
			int res = 0;
            int matching = 0;
            while (BFS(s, t))
                res += DFS(s, t);
            return res;
        }
		
		vector<pair<int,int> > get_flow(int v) {
			vector<pair<int,int> > fdest; 
			for(int e = adj[v]; e >= 0; e = nxt[e]) {
				if(flow[e] > 0) {
					fdest.push_back(make_pair(dst[e], flow[e]));
				}
			}
			return fdest;
		}

    private:
        bool BFS(int s, int t)
        {
            deque<int> Q;
            Q.clear();
            Dist.clear();
			for(int i = 0; i < nu; i++)
				Dist.push_back(i==s ? 0 : INF);
			Q.push_back(s);
            while (!Q.empty()) {
                int v = Q.front();
                Q.pop_front();
                if(Dist[v] < Dist[t])
                    for(int e = adj[v]; e >= 0; e = nxt[e]) {
                        int u = dst[e];
                        if(Dist[u] == INF && flow[e] < cap[e]) {
                            //printf("BFS G1[%d]-->G2[%d]==>G1[%d]\n", v, u, Pair_G2[u]);
                            Dist[u] = Dist[v] + 1;
                            Q.push_back(u);
                        }
                    }
            }
            return (Dist[t] != INF);
        }

        int DFS(int v, int t, int fl = INF)
        {
			if(fl == 0 || v == t)
				return fl;
			int fl0 = fl;
            for(int e = adj[v]; e >= 0; e = nxt[e]) {
                int u = dst[e];
                if (Dist[u] == Dist[v] + 1 && flow[e] < cap[e]) {
					int df = DFS(u, t, min(fl,cap[e]-flow[e]));
					flow[e] += df;
					flow[e^1] -= df;
					fl -= df;
                }
            }
            Dist[v] = INF;
            return fl0 - fl;
        }

        int nu, NIL;
        vector<int> adj, dst, nxt, flow, cap;
        vector<int> Dist;
};

int main()
{
	int n;
	scanf("%d", &n);
	const int s = 6*n*n;
	const int t = s+1;
    Dinic hk(t+1);
	for(int f = 0; f < 3; f++)
		for(int x = 0; x < n; x++)
			for(int y = 0; y < n; y++) {
				int v = (f*n+x)*n+y;
				hk.add_edge(2*v,2*v+1);
				if(x > 0) {
					int w = v-n;
					hk.add_edge(2*v+1, 2*w);
					hk.add_edge(2*w+1, 2*v);
				} else if(f != 0) {
					int w = (f == 1 ? (n-1)*n + y : (n-y)*n-1);
					hk.add_edge(2*v+1, 2*w);
					hk.add_edge(2*w+1, 2*v);
				}
				if(y > 0) {
					int w = v-1;
					hk.add_edge(2*v+1, 2*w);
					hk.add_edge(2*w+1, 2*v);
				} else if(f == 2) {
					int w = v - n*(n-1) - 1;
					hk.add_edge(2*v+1, 2*w);
					hk.add_edge(2*w+1, 2*v);
				}
			}
	int f, x, y;
	char c[5], cl[3][5][5];
	int id[26][2],ncolor = 0;
	memset(id, -1, sizeof(id));
	while(scanf("%d%d%d%s", &f, &x, &y, c) == 4) {
		if(c[0] >= 'a' && c[0] <= 'z')
			c[0] -= 32;
		assert(0 <= f && f <= 2 && 0 <= x && x < n && 0 <= y && y < n && c[0] >= 'A' && c[0] <= 'Z' && c[1] == 0);
		int px = (f*n+x)*n+y;
		if(id[c[0]-'A'][0] < 0) {
			id[c[0]-'A'][0] = px;
			hk.add_edge(s, 2*px);
			++ncolor;
		} else {
			assert(id[c[0]-'A'][1] < 0);
			id[c[0]-'A'][1] = px;
			hk.add_edge(2*px+1, t);
		}
	}
    assert(hk.maxflow(s,t) == ncolor);
	
	vector<pair<int,int> > src = hk.get_flow(s);
	assert(src.size() == ncolor);
	
	char cube[3][5][5];
	memset(cube, ' ', sizeof(cube));
	for(int i = 0; i < ncolor; i++) {
		int v = src[i].first;
		assert(src[i].second == 1);
		while(v != t) {
			if(v & 1) {
				int f = v/2;
				int y = f % n;
				f /= n;
				int x = f % n;
				f /= n;
				printf("(%d,%d,%d) ", f, x, y);
			}
			vector<pair<int,int> > res = hk.get_flow(v);
			assert(res.size() <= 1);
			v = res[0].first;
			assert(res[0].second == 1);
		}
		printf("\n");
	}
    return 0;
}
