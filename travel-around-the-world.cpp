/*
 * =====================================================================================
 *
 *       Filename:  travel-around-the-world.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年05月09日 14时07分26秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <algorithm>

#define N 100000

int n, a[N], b[N];
long long c, r[N];

int count_start(int n, int a[], int b[], long long c) {
    int cnt = 0;
    if(b[n-1] > c)
        return 0;
#if 0
    printf("count_start(%d,%lld,\n", n, c);
    for(int i = 0; i < n; i++)
        printf("%d%c", a[i], (i<n-1?',':'\n'));
    for(int i = 0; i < n; i++)
        printf("%d%s", b[i], (i<n-1?",":")\n"));
#endif
    r[n-1] = std::max(0LL, b[n-1] - std::min((long long)a[n-1], c));
    for(int i = n-2; i >= 0; i--) {
        if(r[i+1] + b[i] > c)
            return 0;
        r[i] = std::max(0LL, r[i+1] + b[i] - std::min((long long)a[i], c));
    }
    if(r[0] + b[n-1] > c)
        return 0;
#if 0
    for(int i = 0; i < n; i++)
        printf("%lld%c", r[i], (i<n-1?',':'\n'));
#endif
    r[n-1] = std::max(0LL, r[0] + b[n-1] - std::min((long long)a[n-1], c));
    if(r[n-1] == 0)
        ++cnt;
    for(int i = n-2; i >= 0; i--) {
        if(r[i+1] + b[i] > c)
            break;
        r[i] = std::max(0LL, r[i+1] + b[i] - std::min((long long)a[i], c));
        if(r[i] == 0)
            ++cnt;
    }
#if 0
    for(int i = 0; i < n; i++)
        printf("%lld%c", r[i], (i<n-1?',':'\n'));
#endif
    return cnt;
}

void test(int n, time_t seed = 0) {
    const int P = 1000000000;
    if(seed == 0)
        seed = time(NULL);
    srand(seed);
    for(int i = 0; i < n; i++)
        a[i] = rand() % P;
    for(int i = 0; i < n; i++)
        b[i] = rand() % P;
    c = rand();
    int res = 0;
    for(int i = 0; i < n; i++) {
        long long r = 0;
        bool valid = true;
        for(int j = i, k = i; j < i+n; j++, k++) {
            if(k == n) k = 0;
            r = std::min(c, r+a[k]);
            r -= b[k];
            if(r < 0) {
                valid = false;
                break;
            }
        }
        if(valid) res++;
    }
    int res1 = count_start(n, a, b, c);
    if(res != res1) {
        printf("test(n=%d, seed=%u) failed: exp %d got %d\n", n, seed, res, res1);
    }
    assert(res == res1);
}
int main()
{
#if 0
    test(10);
    test(100);
    test(1000);
    test(5000);
#else
    scanf("%d%lld", &n, &c);
    for(int i = 0; i < n; i++)
        scanf("%d", &a[i]);
    for(int i = 0; i < n; i++)
        scanf("%d", &b[i]);
    printf("%d\n", count_start(n, a, b, c));
#endif
    return 0;
}

