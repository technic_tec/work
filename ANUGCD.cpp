/*
 * =====================================================================================
 *
 *       Filename:  ANUGCD.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年03月08日 18时37分06秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <vector>
#include <algorithm>
using namespace std;

#define N 100100

struct Node {
    int l, r;
    int vm, c;
};
class BT {
    public:
        BT(vector<pair<int, int> > mult) {
            m_sz = mult.size();
            m_nodes = new Node[2*m_sz];
            // n --> b-1 -> b --> 2*n-1
            int b = 2*m_sz-1;
            while(b & (b-1))
                b &= (b-1);
            for(int i = b; i < 2*m_sz; i++) {
                m_nodes[i].l = m_nodes[i].r = mult[i-b].first;
                m_nodes[i].vm = mult[i-b].second;
                m_nodes[i].c = 1;
            }
            for(int i = m_sz; i < b; i++) {
                m_nodes[i].l = m_nodes[i].r = mult[m_sz-b+i].first;
                m_nodes[i].vm = mult[m_sz-b+i].second;
                m_nodes[i].c = 1;
            }
            for(int i = m_sz-1; i > 0; i--) {
                assert(m_nodes[2*i].r < m_nodes[2*i+1].l);
                m_nodes[i].l = m_nodes[2*i].l;
                m_nodes[i].r = m_nodes[2*i+1].r;
                if(m_nodes[2*i].vm < m_nodes[2*i+1].vm) {
                    m_nodes[i].vm = m_nodes[2*i+1].vm;
                    m_nodes[i].c  = m_nodes[2*i+1].c;
                }
                else if(m_nodes[2*i].vm > m_nodes[2*i+1].vm) {
                    m_nodes[i].vm = m_nodes[2*i].vm;
                    m_nodes[i].c  = m_nodes[2*i].c;
                }
                else {
                    m_nodes[i].vm = m_nodes[2*i].vm;
                    m_nodes[i].c  = m_nodes[2*i].c + m_nodes[2*i+1].c;
                }
            }
            //printf("%d: ", m_sz);
            //for(int i = 1; i < 2*m_sz; i++)
            //    printf("(%d,%d,%d,%d)%c", m_nodes[i].l, m_nodes[i].r, m_nodes[i].vm, m_nodes[i].c, (i<2*m_sz-1?' ':'\n'));
        }
        ~BT() {
            if(m_nodes) {
                delete[] m_nodes;
                m_nodes = NULL;
            }
        }
        pair<int, int> maxcount(int l, int r) {
            int v = 1, vc = -1, vm = -1;
            while(m_nodes[v].l < m_nodes[v].r) {
                if (l > m_nodes[v].r || r < m_nodes[v].l)
                    return make_pair(-1, -1);
                else if(r < m_nodes[2*v+1].l)
                    v = 2*v;
                else if(l > m_nodes[2*v].r)
                    v = 2*v+1;
                else
                    break;
            }
            if (l > m_nodes[v].r || r < m_nodes[v].l)
                return make_pair(-1, -1);
            if(l <= m_nodes[v].l && r >= m_nodes[v].r)
                return make_pair(m_nodes[v].vm, m_nodes[v].c);
            int u = 2*v;
            while(1) {
                assert(l <= m_nodes[u].r);
                if(l <= m_nodes[u].l) {
                    if(m_nodes[u].vm > vm) {
                        vm = m_nodes[u].vm;
                        vc = m_nodes[u].c;
                    } else if(m_nodes[u].vm == vm)
                        vc += m_nodes[u].c;
                    break;
                } else if(l <= m_nodes[2*u].r) {
                    if(m_nodes[2*u+1].vm > vm) {
                        vm = m_nodes[2*u+1].vm;
                        vc = m_nodes[2*u+1].c;
                    } else if(m_nodes[2*u+1].vm == vm)
                        vc += m_nodes[2*u+1].c;
                    u = 2*u;
                } else {
                    u = 2*u+1;
                }
            }
            u = 2*v+1;
            while(1) {
                assert(r >= m_nodes[u].l);
                if(r >= m_nodes[u].r) {
                    if(m_nodes[u].vm > vm) {
                        vm = m_nodes[u].vm;
                        vc = m_nodes[u].c;
                    } else if(m_nodes[u].vm == vm)
                        vc += m_nodes[u].c;
                    break;
                } else if(r >= m_nodes[2*u+1].l) {
                    if(m_nodes[2*u].vm > vm) {
                        vm = m_nodes[2*u].vm;
                        vc = m_nodes[2*u].c;
                    } else if(m_nodes[2*u].vm == vm)
                        vc += m_nodes[2*u].c;
                    u = 2*u+1;
                } else {
                    u = 2*u;
                }
            }
            return make_pair(vm, vc);
        }
    private:
        int m_sz;
        Node *m_nodes;
};

int n, m;
int fac[N];
vector<pair<int, int> > mult[N];
BT *bt[N];

int main()
{
    memset(fac, 0, sizeof(fac));
    fac[0] = 0; fac[1] = 1;
    for(int i = 2; i < N; i++) {
        if(fac[i]) continue;
        fac[i] = i;
        if(i < 32768)
            for(int j = i*i; j < N; j += i)
                if(!fac[j])
                    fac[j] = i;
    }
    scanf("%d%d", &n, &m);
    for(int i = 1; i <= n; i++) {
        int x;
        scanf("%d", &x);
        pair<int,int> val = make_pair(i, x);
        while(x > 1) {
            int y = fac[x];
            mult[y].push_back(val);
            while(fac[x] == y)
                x /= y;
        }
    }
    bt[0] = bt[1] = NULL;
    for(int i = 2; i < N; i++)
        if(fac[i] == i && mult[i].size() > 0) {
            bt[i] = new BT(mult[i]);
        } else {
            bt[i] = NULL;
        }
    while(m--) {
        int g, x, y;
        scanf("%d%d%d", &g, &x, &y);
        int vm = -1, vc = -1;
        while(g > 1) {
            int gf = fac[g];
            if(bt[gf]) {
                pair<int, int> mc = bt[gf]->maxcount(x, y);
                if(mc.first > vm) {
                    vm = mc.first;
                    vc = mc.second;
                }
            }
            while(fac[g] == gf)
                g /= gf;
        }
        printf("%d %d\n", vm, vc);
    }
    for(int i = 0; i < N; i++)
        if(bt[i]) {
            delete bt[i];
            bt[i] = NULL;
        }
    return 0;
}

