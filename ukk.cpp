/*
 * =====================================================================================
 *
 *       Filename:  ukk.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年04月15日 10时57分02秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define N 100000
#define INF 99999999

class UKK {
public:
    class State;
    class Transition {
    public:
        State *s;
        int k, p;
        Transition(State *_s = NULL, int _k = 0, int _p = -1) {
            s = _s; k = _k; p = _p;
        }
        ~Transition() {
            if(s)
                delete s;
            s = NULL;
            k = 0; p = -1;
        }
    };
    class State {
    public:
        Transition *tr[26];
        State *sfl;   // suffix link
        State *rsl;   // reverse suffix link
        State() {
            sfl = rsl = NULL;
            memset(tr, 0, sizeof(tr));
        }
        ~State() {
            for(int i = 0; i < 26; i++)
                if(tr[i]) {
                    delete tr[i];
                    tr[i] = NULL;
                }
            sfl = rsl = NULL;
        }
    };
    UKK(int sz = 100) {
        m_root = new State();
        m_EMPTY= new State();
        m_buf = new char[sz+26];
        m_bufsz = sz;
        m_str = m_buf + 26;
        for(int i = 1; i <= 26; i++) {
            m_str[-i] = 'a'+i-1;
            m_EMPTY->tr[i-1] = new Transition(m_root, -i, -i);
        }
        m_str[0] = 0;
        m_len = 0;
        m_root->sfl = m_EMPTY;
        m_EMPTY->rsl = m_root;
        m_active_state = m_root;
        m_active_k = 0;
    }
    void construct(char *s)
    {
        while(*s)
            update(m_active_state, m_active_k, *s++);
    }
    virtual ~UKK()
    {
        for(int i = 0; i < 26; i++)
            m_EMPTY->tr[i]->s = NULL;
        delete m_root;
        delete m_EMPTY;
        delete[] m_buf;
        m_root = m_EMPTY = NULL;
        m_buf = m_str = NULL;
        m_bufsz = m_len = 0;
    }
    // (s, (k, p))--canonic pair of active point, t--new appended char, snew--split point
    bool test_split(State *s, int k, int p, char t, State* &snew)
    {
        snew = s;
        if(k<=p) {
            Transition *tr = s->tr[m_str[k]-'a'];
            if(t == m_str[tr->k+p-k+1])
               return true;
            else {
                State *r = new State;
                r->tr[m_str[tr->k+p-k+1]-'a'] = new Transition(tr->s, tr->k+p-k+1, tr->p);
                tr->s = r;
                tr->p = tr->k+p-k;
                snew = r;
                return false;
            }
        } else {
            if(s->tr[t-'a'] == NULL)
                return false;
            else
                return true;
        }
    }
    void canonize(State* &s, int &k, int p)
    {
        if(p < k)
            return;
        else {
            Transition *tr = s->tr[m_str[k]-'a'];
            while (tr->p - tr->k <= p - k) {
                k += tr->p - tr->k + 1;
                s = tr->s;
                if(k <= p)
                    tr = s->tr[m_str[k]-'a'];
            }
        }
    }
    void update(State* &s, int &k, char ti)
    {
        int i = m_len;
        m_str[m_len++] = ti;
        m_str[m_len] = 0;
        State *r;
        State *oldr = m_root;
        bool end_point = test_split(s, k, i-1, m_str[i], r);
        while(!end_point) {
            r->tr[m_str[i]-'a'] = new Transition(NULL, i, INF);
            if(oldr != m_root) {
                oldr->sfl = r;
                r->rsl = oldr;
            }
            oldr = r;
            s = s->sfl;
            canonize(s, k, i-1);
            end_point = test_split(s, k, i-1, m_str[i], r);
        }
        if(oldr != m_root) {
            oldr->sfl = r;
            r->rsl = oldr;
        }
        canonize(s, k, i);
        assert(s->sfl);
    }
    void revert(State* &s, int &k);
private:
    State *m_root, *m_EMPTY;
    char *m_buf;
    int m_bufsz;
    char *m_str;
    int m_len;
    // Active point
    State *m_active_state;
    int m_active_k;
};
int main()
{
    UKK tree(100);
    tree.construct("misissipi");
    return 0;
}

