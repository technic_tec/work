/*
 * =====================================================================================
 *
 *       Filename:  CLOWAY.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年08月16日 16时56分08秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009

int main()
{
  vector<Graph> gs;
  vector<Query> qr;
  int t, q;

  scanf("%d", &t);
  for(int i = 0; i < t; i++) {
    int n, m;
    scanf("%d%d", &n, &m);
    gs.push_back(Graph(n));
    Graph &g = gs.back();
    for(int i = 0; i < m; i++) {
      int x, y;
      scanf("%d%d", &x, &y);
      g.add_edge(x, y);
    }
    g.init(); // characteristic polynomial
  }
  scanf("%d", &q);
  for(int i = 0; i < q; i++) {
    int l, r, k;
    scanf("%d%d%d", &l, &r, &k);
    qr.push_back(Query(qr.size(), l, r, k));
  }
  return 0;
}


