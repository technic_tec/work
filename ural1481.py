from itertools import groupby, permutations
def weigh(l, r):
  vl = sorted(l)
  vr = sorted(r)
  if len(vl) > len(vr):
    for x, y in zip(vl[len(vl)-len(vr):], vr):
      if x < y:
        return 'U'
    return 'L'
  elif len(vl) < len(vr):
    for x, y in zip(vl, vr[len(vr)-len(vl):]):
      if x > y:
        return 'U'
    return 'R'
  else:
    res = 'E'
    for x, y in zip(vl, vr):
      if x < y:
        if res == 'L':
          return 'U'
        else:
          res = 'R'
      elif x > y:
        if res == 'R':
          return 'U'
        else:
          res = 'L'
    return res

def brutalforce(n):
  sol = dict()
  for p in permutations(range(1, n+1)):
    if len(sol) == (1<<n):
      break
    for sc in range(1<<n):
      l, r = [], []
      res = ''
      m = bin(sc)[2:].zfill(n)
      for w, x in zip(p, m):
        if x == '0':
          l.append(w)
        else:
          r.append(w)
        res += weigh(l, r)
        if res[-1] == 'U':
          break
      if len(res) == n and res[-1] != 'U' and res not in sol:
        sol[res] = (p, m)
        print res, ':', sol[res]
        if len(sol) == (1<<n):
          break
  print sol

n = int(raw_input())
w = map(int, raw_input().split())
s = raw_input().strip()

w.sort()
sc = [None] * n
for i in range(n-1, -1, -1):
  if i == 0 or s[i] != s[i-1]:
    sc[i] = (w.pop(), s[i])
fst = (len(w) & 1)
f, o = ('L', 'R') if s[0] == 'L' else ('R', 'L')
for i in range(n):
  if sc[i] is None:
    v = w.pop()
    p = (f if (len(w) & 1) == fst else o)
    sc[i] = (v, p)
#l, r = [], []
for x, y in sc:
  #if y == 'L':
  #  l.append(x)
  #else:
  #  r.append(x)
  #if weigh(l, r) != s[len(l)+len(r)-1]:
  #  print 'ERROR', l, r, s[len(l)+len(r)-1]
  print x, y
