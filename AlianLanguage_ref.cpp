#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 3030
#define P 1000000007

int s[N][N];
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        memset(s, -1, sizeof(s));
        s[1][n] = 0;
        for(int j = n-1; j >= 0; j--)
            s[1][j] = s[1][j+1] + (2*(j+1) > n ? 1 : 0);
        for(int i = 2; i <= m; i++) {
            s[i][n] = 0;
            for(int j = n-1; j >= 0; j--) {
                s[i][j] = s[i][j+1] + (2*(j+1)>n ? s[i-1][0] : s[i-1][2*(j+1)-1]);
                if(s[i][j] >= P)
                    s[i][j] -= P;
            }
        }
        printf("%d\n", s[m][0]);
    }
    return 0;
}
