#Task 1: () -> RIGHT,  )( -> WRONG
def p1():
    s=['RIGHT)->)RIGHT',
       'RIGHT(->(RIGHT',
       '()->RIGHT',
       ')->WRONG',
       '(->WRONG',
       'WRONGWRONG->WRONG',
       'WRONGRIGHT->WRONG',
       'RIGHTRIGHT->RIGHT']
    print len(s)
    for l in s:
        print l

#Task 2: [integer1]+[integer2]=? -> [integer1]+[integer2]=[sum]
def p2():
    s = list()
    for x in range(10):
        s.append('|%d->%d|' % (x, x))
    s.append('|*=->*=|')
    for x in range(10):
        s.append('%d+->*+%s' % (x, '|'*x))
        s.append('%d=->*=%s' % (x, '|'*x))
    for x in range(10):
        s.append('=%s?->=?%d', '|'*x, x)
        s.append('=%s?->=!%d', '|'*(10+x), x)
    for x in range(9):
        s.append('=%s!->=?%d', '|'*x, str(x+1))
    for x in range(9, 19):
        s.append('=%s!->=!%d', '|'*x, str(x+1-10))
    s.append('*=->#=')
    s.append('*+->+')
    s.append('#=->=')
#   s = list()
#   d = '0123456789'
#   s.append('=?->?#=')
#   s.append('+?->?+')
#   for x in d:
#       s.append('%s?->?%s%s' % (x, x, chr(ord(x)+49)))
#   s.append('?->')
#   for x in d:
#       for y in d:
#           s.append('%s%s->%s%s' % (chr(ord(x)+49), y, y, chr(ord(x)+49)))
#   for x in d:
#       for y in d:
#           s.append('%s%s->%s%s' % (chr(ord(x)+17), y, y, chr(ord(x)+17)))
#   for x in d:
#       for y in d:
#           s.append('%s%s->%s%s' % (chr(ord(x)+17), chr(ord(y)+49), chr(ord(y)+49), chr(ord(x)+17)))
#   for x in d:
#       for y in d:
#           if int(x)+int(y) < 10:
#               s.append('%s%s#=->#=%s' % (chr(ord(x)+49), chr(ord(y)+17), str(int(x)+int(y))))
#           else:
#               s.append('%s%s#=->$=%s' % (chr(ord(x)+49), chr(ord(y)+17), str(int(x)+int(y)-10)))
#           if int(x)+int(y)+1 < 10:
#               s.append('%s%s$=->#=%s' % (chr(ord(x)+49), chr(ord(y)+17), str(int(x)+int(y)+1)))
#           else:
#               s.append('%s%s$=->$=%s' % (chr(ord(x)+49), chr(ord(y)+17), str(int(x)+int(y)+1-10)))
#   for x in d:
#       s.append('%s+->+%s' % (chr(ord(x)+49), chr(ord(x)+17)))
#   for x in d:
#       s.append('%s#=->#=%s' % (chr(ord(x)+49), str(x)))
#       s.append('%s#=->#=%s' % (chr(ord(x)+17), str(x)))
#       if int(x)+1 < 10:
#           s.append('%s$=->#=%s' % (chr(ord(x)+49), str(int(x)+1)))
#           s.append('%s$=->#=%s' % (chr(ord(x)+17), str(int(x)+1)))
#       else:
#           s.append('%s$=->$=%s' % (chr(ord(x)+49), str(int(x)+1-10)))
#           s.append('%s$=->$=%s' % (chr(ord(x)+17), str(int(x)+1-10)))
#   s.append('#=->=')
#   s.append('$=->=1')
#   print len(s)
#   for l in s:
#       print l

#Task 3: ABBCD? -> DCBBA
def p3():
    s = list()
    w='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for x in w:
        for y in w:
            s.append('%s!%s->%s%s!' % (x, y, y, x))
    for x in w:
        s.append('%s?->?%s!' % (x, x))
    s.append('!->')
    s.append('?->')
    print len(s)
    for l in s:
        print l

#Task 4: 110 -> "zzzzzz"
def p4():
    s = ['1->0z',
         'z0->0zz',
         '0->']
    print len(s)
    for l in s:
        print l

#Task 5: DFAAS? -> AADFS
def p5():
    s = list()
    w='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for x in w:
        for y in w:
            if ord(x) > ord(y):
                s.append('%s!%s->%s%s!' % (x, y, y, x))
            else:
                s.append('%s!%s->%s%s' % (x, y, x, y))
    for x in w:
        s.append('%s?->?%s!' % (x, x))
    s.append('!->')
    s.append('?->')
    print len(s)
    for l in s:
        print l

#Task 6: 30_42=? -> 6
def p6():
    s = list()
    d = '0123456789'
    s.append('=?->?#=!')
    s.append('_?->?_')
    for x in d:
        s.append('%s?->?%s%s' % (x, x, chr(ord(x)+49)))
    s.append('?->')
    for x in d:
        for y in d:
            s.append('%s%s->%s%s' % (chr(ord(x)+49), y, y, chr(ord(x)+49)))
    for x in d:
        for y in d:
            s.append('%s%s->%s%s' % (chr(ord(x)+17), y, y, chr(ord(x)+17)))
    for x in d:
        for y in d:
            s.append('%s%s->%s%s' % (chr(ord(x)+17), chr(ord(y)+49), chr(ord(y)+49), chr(ord(x)+17)))
    for x in d:
        for y in d:
            if int(y)-int(x) >= 0:
                s.append('%s%s#=->#=%s' % (chr(ord(x)+49), chr(ord(y)+17), str(int(y)-int(x))))
            else:
                s.append('%s%s#=->$=%s' % (chr(ord(x)+49), chr(ord(y)+17), str(int(y)-int(x)+10)))
            if int(y)-int(x)-1 >= 0:
                s.append('%s%s$=->#=%s' % (chr(ord(x)+49), chr(ord(y)+17), str(int(y)-int(x)-1)))
            else:
                s.append('%s%s$=->$=%s' % (chr(ord(x)+49), chr(ord(y)+17), str(int(y)-int(x)-1+10)))
    for x in d:
        s.append('%s_->_%s' % (chr(ord(x)+49), chr(ord(x)+17)))
    for x in d:
        if int(x) == 0:
            s.append('%s#=->#=%s' % (chr(ord(x)+49), str(-int(x))))
        else:
            s.append('%s#=->$=%s' % (chr(ord(x)+49), str(10-int(x))))
        s.append('%s#=->#=%s' % (chr(ord(x)+17), x))
        s.append('%s$=->$=%s' % (chr(ord(x)+49), str(10-int(x)-1)))
        if int(x)-1 >= 0:
            s.append('%s$=->#=%s' % (chr(ord(x)+17), str(int(x)-1)))
        else:
            s.append('%s$=->$=%s' % (chr(ord(x)+17), str(int(x)-1+10)))
    s.append('#=->,=')
    s.append('$=->.=~')
    for x in d:
        s.append('~%s->%s~' % (x, str(9-int(x))))
    s.append('~!->^!')
    for x in d:
        if x != '9':
            s.append('%s^->%s' % (x, str(int(x)+1)))
    s.append('9^->^0')
    for x in d:
        s.append('%s,->,%s' % (x, x))
    s.append('_,->.')
    for x in d:
        s.append('%s.->.' % x)
    s.append('_.->')
    s.append('.->')
    s.append('=0->=')
    s.append('=!->')
    s.append('=->_')
    s.append('!->=?')
    print len(s)
    for l in s:
        print l

p1()
p2()
p3()
p4()
p5()
p6()
