import sys
def valid(s):
  for i in range(len(s)):
    for j in range(i-2, -1, -2):
      if s[i] == s[j] and s[i] == s[(i+j)/2]:
        return False
  return True

def gen(n=12, s='x'):
  if not valid(s):
    return
  if len(s) == n:
    yield s
    return
  for x in gen(n, s+'x'):
    yield x
  for x in gen(n, s+'y'):
    yield x
  if 'y' in s:
    for x in gen(n, s+'z'):
      yield x
for i in range(1, 50):
  sys.stderr.write(str(i)+"\n")
  noSol = True
  for x in gen(i):
    print x
    noSol = False
  if noSol:
    break
