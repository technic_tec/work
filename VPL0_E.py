from itertools import groupby
def look_and_say(s):
    return ''.join(str(len(list(w)))+k for k, w in groupby(s))
s = ['1']
t = int(raw_input())
for _ in range(1, t+1):
    n = int(raw_input())
    while len(s) <= n:
        s.append(look_and_say(s[-1]))
    print 'Scenario #%d: %s' % (_, s[n])
