-- Enter your code here. Read input from STDIN. Print output to STDOUT
import Data.Array

data Tree = Branch Int Int Tree Tree | Leaf
  deriving Show

buildTree :: Array Int (Int,Int) -> Int -> Int -> Tree
buildTree a (-1) depth = Leaf
buildTree a key depth = Branch key depth (buildTree a l (depth+1)) (buildTree a r (depth+1))
  where (l,r) = a!key
  
inorder :: Tree -> [Int]
inorder Leaf = []
inorder (Branch key depth l r) = (inorder l) ++ [key] ++ (inorder r)

swp :: Tree -> Int -> Tree
swp Leaf k = Leaf
swp (Branch key depth l r) k | depth `mod` k == 0 = Branch key depth r1 l1
                             | otherwise = Branch key depth l1 r1
    where l1 = swp l k
          r1 = swp r k

readTree :: Int -> [(Int,Int)] -> IO([(Int,Int)])
readTree 0 tr = return tr
readTree n tr = do
  s <- getLine
  let [l,r] = map read (words s)
  readTree (n-1) (tr ++ [(l,r)])

process :: Tree -> Int -> IO()
process tr 0 = return()
process tr n = do
  s <- getLine
  let k = read s
  let tr1 = swp tr k
  putStrLn $ unwords $ map show (inorder tr1)
  process tr1 (n-1)

main :: IO()
main = do
  s <- getLine
  let n = read s
  ls <- readTree n []
  let tr = buildTree (array (1,n) (zip [1..n] ls)) 1 1
  s <- getLine
  let q = read s
  process tr q

