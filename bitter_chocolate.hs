import Data.Array
import Data.Maybe

bitter :: Array (Int, Int, Int) (Maybe Bool)
bitter = array ((0, 0, 0), (25, 25, 25)) [((x, y, z), win x y z) | x<-[0..25], y<-[0..25], z<-[0..25]]
  where win 0 0 0 = Just True
        win x y z | x <= y && y <= z = Just $ not $ and ([fromJust (bitter!(i, y, z))|i<-[0..x-1]] ++ [fromJust (bitter!(min x i, i, z))|i<-[0..y-1]] ++ [fromJust (bitter!(min x i, min y i, i))|i<-[0..z-1]])
		          | otherwise = Nothing

bitterResult :: Int->Int->Int->String
bitterResult r1 r2 r3 | fromJust (bitter!(r3,r2,r1)) = "WIN"
                      | otherwise = "LOSE"

process :: Int->IO()
process 0 = return()
process n = do
  s <- getLine
  let [r1, r2, r3] = map read $ words s
  putStrLn $ bitterResult r1 r2 r3
  process (n-1)

main :: IO()
main = do
  s <- getLine
  let n = read s
  process n
