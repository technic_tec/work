#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100100
#define K 6

int fac[N], fc[N], pc[K+1][N];

int main()
{
    int t;
    memset(fac, 0, sizeof(fac));
    fac[1] = 1; fc[0] = fc[1] = 0;
    for(int i = 2; i < N; i++)
        if(fac[i]) {
            int j = i/fac[i];
            if(fac[j] == fac[i])
                fc[i] = fc[j];
            else
                fc[i] = fc[j]+1;
        } else {
            fac[i] = i;
            fc[i] = 1;
            if(i < 32768)
                for(int j = i*i; j < N; j+=i)
                    if(!fac[j])
                        fac[j] = i;
        }
    for(int i = 0; i <= K; i++)
        pc[i][0] = 0;
    for(int i = 1; i < N; i++) {
        for(int j = 0; j <= K; j++)
            pc[j][i] = pc[j][i-1];
        pc[fc[i]][i]++;
    }
    scanf("%d", &t);
    while(t--) {
        int a, b, k;
        scanf("%d%d%d", &a, &b, &k);
        printf("%d\n", pc[k][b]-pc[k][a-1]);
    }
    return 0;
}

