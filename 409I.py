import re

class Var(object):
    def __init__(self, s):
        self.name = s
        self.ls, self.gs = set(), set()
        self.vmin, self.vmax = 0, 9
    def __repr__(self):
        return '%s(%d:%d)' % (self.name, self.vmin, self.vmax)

class Cond(object):
    def __init__(self, s, _vars):
        s = s.strip()
        i, j = 0, len(s)-1
        while i <= j and s[i] == '_':
            i += 1
        while i <= j and s[j] == '_':
            j -= 1
        if i > j or s[i:j+1].strip() not in ['<', '>']:
            print s[i:j+1]
            raise ValueError
        if s[i:j+1].strip() == '<':
            self.va, self.vb = _vars[s[:i]], _vars[s[j+1:]]
        else:
            self.vb, self.va = _vars[s[:i]], _vars[s[j+1:]]
    def __repr__(self):
        return '[%s < %s]' % (self.va, self.vb)

def topo_sort(cs):
    res = []
    for c in cs:
        c.va.gs.add(c.vb)
        c.vb.ls.add(c.va)
    hp = set([x.va for x in cs]) - set([x.vb for x in cs])
    while len(hp) > 0:
        x = hp.pop()
        res.append(x)
        for y in x.gs:
            y.ls.remove(x)
            if len(y.ls) == 0:
                hp.add(y)
        x.gs.clear()
    for c in cs:
        if len(c.vb.ls) > 0:
            return None
    for c in cs:
        c.va.gs.add(c.vb)
        c.vb.ls.add(c.va)
    for x in res:
        if len(x.ls) > 0:
            x.vmin = max([y.vmin for y in x.ls]) + 1
        else:
            x.vmin = 0
    for x in res[::-1]:
        if len(x.gs) > 0:
            x.vmax = min([y.vmax for y in x.gs]) - 1
        else:
            y.vmax = 9
    for x in res:
        if x.vmin < 0 or x.vmax > 9 or x.vmin > x.vmax:
            return None
    return res
    
s= raw_input()
jaw, stomach = s.split(':-')
if stomach[-1] == '.':
    stomach = stomach[:-1]
varl = list()
_vars = dict()
for x in re.findall(r'_+', jaw):
    if x not in _vars:
        _vars[x] = Var(x)
    varl.append(_vars[x])

conds = [Cond(c, _vars) for c in stomach.split(',')]
sc = topo_sort(conds)
#print _vars
#print conds
if sc is None:
    print 'false'
else:
    o = ''.join([str(x.vmin) for x in varl])
    print o
