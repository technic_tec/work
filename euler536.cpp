/*
 * =====================================================================================
 *
 *       Filename:  536.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年12月31日 17时09分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000010
vector<int> prm;

void prep()
{
  vector<bool> isp(N, true);
  isp[0] = isp[1] = false;
  prm.clear();
  for(int i = 2; i < N; i++) {
    if(!isp[i]) continue;
    prm.push_back(i);
    if(i < 32768)
      for(int j = i*i; j < N; j+=i)
        isp[j] = false;
  }
}

long long search(long long n, long long m, long long k, long long b, int pi = 1)
{
  long long s = 0;
  if(m == b)
    s += m;
  for(;pi < N && (long long)prm[pi]*(long long)(prm[pi]-4) <= n && m*prm[pi] <= n; pi++) {
    long long m1 = m*prm[pi];
    long long k1 = (long long)prm[pi]*(long long)(prm[pi]-1);
    long long b1 = (long long)prm[pi]*(long long)(prm[pi]-4);
    // n=k*x+b=k1*y+b1
    // k/d*x-k1/d*y=(b1-b)/d
    long long x, y;
    long long d = ext_gcd(k, k1, x, y);
    if((b1-b)%d) continue;
    if(b1 <= n)
      s += search(n, m1, k1, b1, pi+1);
  }
  return s;
}
// p^2|m => p^k != p (mod m) for k > 1 => m is squarefree
// a^(m+4)==a, (a, m)==1 => a^(m+3)==1 => Carmichael(m)|(m+3) 
// 1) even m => odd m+3 => odd Carmichael(m) => m=2
// 2) p|m => p-1|(m+3) => m=p*(k*(p-1)+p-4)
long long euler536(long long n)
{
  long long s = 2;
  s += search(n, 1, 2, 1);
  printf("%lld: %lld\n", n, s);
  return s;
}

int main()
{
  prep();
  euler536(100);
  euler536(1000000);
  euler536(1000000000000LL);
  return 0;
}


