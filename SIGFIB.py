from fractions import Fraction
from random import seed,randint
def fib(n):
    a0,a1 = 0,1
    if n >= 0:
        while n>0:
            a0,a1 = a1,a0+a1
            n -= 1
    else:
        while n < 0:
            a0,a1 = a1-a0, a0
            n += 1
    return a0

print map(fib, range(50))

# fib(x)*fib(y)*fib(z) = (fib(x+y+z) - fib(y+z-x)*(-1)^x - fib(z+x-y)*(-1)^y - fib(x+y-z)*(-1)^z)/5
# x+y+z=n => fib(x)*fib(y)*fib(z) = (fib(n) - fib(n-2x)*(-1)^x - fib(n-2y)*(-1)^y - fib(n-2z)*(-1)^z)/5
# sum(x*y*z*fib(x)*fib(y)*fib(z)) = (fib(n)*sum(x*y*z) - 3*sum((-1)^x*x*y*z*fib(n-2*x)))/5
seed()
for a in range(1,15):
    for b in range(1,15):
        for c in range(1,15):
            print '%2d %2d %2d: %d' % (a,b,c,g(a,b,c))

# (a^x-b^x)*(a^y-b^y)*(a^z-b^z)/5^(3/2)