#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100100

class SuffixArray {
    public:
        SuffixArray(int v[], int len) {
            m_v = v;
            m_len = len;
            m_sa = new int[m_len+1];
            m_rank = new int[m_len+1];
#if 1
            m_iv = new int[m_len+1];
            for(int i = 0; i < m_len; i++)
                m_iv[i] = (m_v[i] - 'a'+1);
            m_iv[m_len] = 0;
            SA_IS(m_iv, m_len+1, 27, m_sa);
            assert(m_sa[0] == m_len);
            for(int i = 1; i <= m_len; i++) {
                assert(m_sa[i] >= 0 && m_sa[i] < m_len);
                m_sa[i-1] = m_sa[i];
                m_rank[m_sa[i]] = i;
            }
            compute_lcp();
        }

        void SA_IS(int S[], int len, int vmax, int SA[]) {
            // S is the input string;
            // SA is the output suffix array of S;
            assert(len > 0 && S[len-1] == 0);
            // 1. Scan S once to classify all the characters as L- or S-type into t;
            bool *t = new bool[len];
            t[len-1] = true;
            for(int i = len-2; i >= 0; i--)
                t[i] = (S[i]<S[i+1] || (S[i]==S[i+1] && t[i+1]));
            // 2. Scan t to find all the LMS-substrings in S into P1;
            int *P1 = new int[len];
            int *iP1 = new int[len];
            int *B = new int[vmax];
            int *nxtB = new int[len];
            int len1 = 0;
            iP1[0] = -1;
            fill(B, B+vmax, -1);
            for(int i = 1; i < len; i++)
                if(!t[i-1] && t[i]) {
                    iP1[i] = len1;
                    nxtB[len1] = B[S[i]];
                    B[S[i]] = len1;
                    P1[len1++] = i;
                } else
                    iP1[i] = -1;
            // 3. Induced sort all the LMS-substrings
            int *SA1 = new int[len1];
            for(int i = 0, k = 0; i < vmax; i++)
                for(int j = B[i]; j >= 0; j = nxtB[j])
                    SA1[k++] = j;
            delete[] B;
            delete[] nxtB;
            B = nxtB = NULL;
            induce(SA, len, SA1, len1, P1, S, vmax, t);
            int *S1 = new int[len1];
            int j = 0, vm1 = -1;
            for(int i = 0, lst = -1; i < len; i++) {
                assert(SA[i] >= 0 && SA[i] < len);
                if(iP1[SA[i]] >= 0) {
                    if(lst < 0 || !eq(lst, iP1[SA[i]], P1, len1, S))
                        ++vm1;
                    S1[iP1[SA[i]]] = vm1;
                    lst = iP1[SA[i]];
                }
            }
            ++vm1;
            delete[] iP1;
            iP1 = NULL;
            // 4. Directly compute SA1 from S1 if all unique; recusively otherwise.
            if(vm1 < len1) {
                SA_IS(S1, len1, vm1, SA1);
            } else {
                int *B = new int[vm1];
                int *nxtB = new int[len1];
                fill(B, B+vm1, -1);
                for(int i = 0; i < len1; i++) {
                    nxtB[i] = B[S1[i]];
                    B[S1[i]] = i;
                }
                for(int i = 0, k = 0; i < vm1; i++)
                    for(int j = B[i]; j >= 0; j = nxtB[j])
                        SA1[k++] = j;
                delete[] B;
                delete[] nxtB;
                B = nxtB = NULL;
            }
            delete[] S1;
            S1 = NULL;
            // 6. Induce SA from SA1.
            induce(SA, len, SA1, len1, P1, S, vmax, t);
            delete[] P1;
            delete[] t;
            delete[] SA1;
            P1 = NULL;
            t = NULL;
            SA1 = NULL;
        }

        // LMS (length len1) order in SA1, pointer in P
        // Result in SA1
        inline void induce(int SA[], int len, int SA1[], int len1, int P[], int S[], int vmax, bool t[])
        {
            int (*Bp)[2] = new int[vmax][2];
            fill((int *)Bp, (int *)(Bp+vmax), 0);
            for(int i = 0; i < len; i++)
                Bp[S[i]][1]++;
            Bp[0][1]--;
            for(int i = 1; i < vmax; i++) {
                Bp[i][0] += Bp[i-1][1]+1;
                Bp[i][1] += Bp[i-1][1];
            }
            assert(Bp[vmax-1][1] == len-1);
            fill(SA, SA+len, -1);
            // 1. Find the end of each S-type bucket;
            //    put all the items of SA1 into their corresponding S-type buckets in SA,
            //    with their relative orders unchanged as that in SA1;
            for(int i = len1-1; i >= 0; i--)
                SA[Bp[S[P[SA1[i]]]][1]--] = P[SA1[i]];
            for(int i = 0; i < vmax-1; i++)
                Bp[i][1] = Bp[i+1][0]-1;
            Bp[vmax-1][1] = len-1;
            // 2. Find the head of each L-type bucket in SA;
            //    scan SA from head to end, for each item SA[i], 
            //    if S[SA[i]-1] is L-type, put SA[i]-1 to the current head of the L-type bucket for S[SA[i]-1] and forward the current head one item to the right.
            for(int i = 0; i < len; i++) 
                if(SA[i] > 0 && !t[SA[i]-1])
                    SA[Bp[S[SA[i]-1]][0]++] = SA[i]-1;
            // 3. Find the end of each S-type bucket in SA;
            //    scan SA from end to head, for each item SA[i], 
            //    if S[SA[i]-1] is S-type, put SA[i]-1 to the current end of the S-type bucket for S[SA[i]-1] and forward the current end one item to the left.
            for(int i = len-1; i >= 0; i--)
                if(SA[i] > 0 && t[SA[i]-1])
                    SA[Bp[S[SA[i]-1]][1]--] = SA[i]-1;
            delete[] Bp;
        }

        ~SuffixArray() {
            if(m_sa) {
                delete[] m_sa;
                m_sa = NULL;
            }
            if(m_lcp) {
                delete[] m_lcp;
                m_lcp = NULL;
            }
        }
        void compute_lcp()
        {
            m_lcp = new int[m_len];
            int j;
            int x = m_rank[0]-1;
            if(x == 0)
                m_lcp[x] = 0;
            else {
                for(j = 0; m_sa[x]+j < m_len && m_sa[x-1]+j < m_len && m_v[m_sa[x]+j] == m_v[m_sa[x-1]+j]; j++);
                m_lcp[x] = j;
            }
            for(int i = 1; i < m_len; i++) {
                x = m_rank[i]-1;
                if(x == 0 || m_v[m_sa[x]] != m_v[m_sa[x-1]])
                    m_lcp[x] = 0;
                else {
                    for(j = std::max(m_lcp[m_rank[m_sa[x]-1]-1]-1, 0); m_sa[x]+j < m_len && m_sa[x-1]+j < m_len && m_v[m_sa[x]+j] == m_v[m_sa[x-1]+j]; j++);
                    m_lcp[x] = j;
                }
            }
            /* printf("lcp: ");
            for(int i = 0; i < m_len; i++)
                printf("%d%c", m_lcp[i], i<m_len-1?' ':'\n'); */
        }
        int SubstringCount();
    private:
        int *m_v, m_len;
        int *m_sa;
        int *m_lcp;
}
int n, v[N];
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &n);
        for(int i = 0; i < n; i++)
            scanf("%d", &v[i]);
        int vm = 1;
        for(int i = 1; i < n; i++) {
            v[i] -= v[i-1];
            if(v[i] < vm) vm = v[i];
        }
        if(vm < 1)
            for(int i = 1; i < n; i++)
                v[i] += 1-vm;
        v[n] = 0;
        SuffixArray sa(v+1, n);
        printf("%d\n", sa.SubstringCount());
    }
    return 0;
}

