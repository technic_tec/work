/*
 * =====================================================================================
 *
 *       Filename:  STETSKLX.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年08月13日 13时30分46秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <deque>
using namespace std;

#define INF 1000000000

enum ST {
  UNVISITED,
  VISITING,
  VISITED,
};

struct Edge;

struct Node {
  ST state;
  Node *pa;
  int pa_dis;
  vector<int> md;
  Edge *adj;
  Node() : state(UNVISITED), pa(NULL), pa_dis(0), adj(NULL) {}
};

struct Edge {
  int len;
  Node *dst;
  Edge *nxt;
  Edge(int w = 0) : dst(NULL), nxt(NULL), len(w) {}
  Edge(Node *px, Node *py, int w) : dst(py), nxt(px->adj), len(w) {}
};

vector<Node> nodes;
vector<Edge> edges;

inline void init(int n)
{
  nodes.clear();
  edges.clear();
  nodes.resize(n+1, Node());
  edges.reserve(2*n-2);
}

inline void reset()
{
  for(int i = 0; i < nodes.size(); i++)
    nodes[i].state = UNVISITED;
}

inline void add_edge(int x, int y, int w)
{
  edges.push_back(Edge(&nodes[x], &nodes[y], w));
  nodes[x].adj = &edges.back();
  edges.push_back(Edge(&nodes[y], &nodes[x], w));
  nodes[y].adj = &edges.back();
}

bool median(int ml, int l, int r) {
  vector<Node *> stk;
  reset();
  nodes[1].pa = NULL;
  nodes[1].pa_dis = 0;
  stk.push_back(&nodes[1]);
  while(!stk.empty()) {
    Node *px = stk.back();
    if(px->state == UNVISITED) {
      px->state = VISITING;
      px->md.assign(r+1, -INF);
      px->md[0] = 0;
      for(Edge *e = px->adj; e; e = e->nxt)
        if(e->dst != px->pa) {
          assert(e->dst->state == UNVISITED);
          e->dst->pa = px;
          e->dst->pa_dis = e->len;
          stk.push_back(e->dst);
        }
    } else {
      assert(px->state == VISITING);
      px->state = VISITED;
      stk.pop_back();
      Node *pa = px->pa;
      if(pa) {
        int c = (px->pa_dis <= ml ? 1 : -1);
        // sliding RMQ query on pa->md[j], L<=i+j<=R, j>=0
        deque<int> cmax;
        for(int i = r, j = 0; i > 0; i--, j++) {
          while(!cmax.empty() && cmax.front()+i < l)
            cmax.pop_front();
          while(!cmax.empty() && pa->md[cmax.back()] <= pa->md[j])
            cmax.pop_back();
          cmax.push_back(j);
          if(px->md[i-1]+c + pa->md[cmax.front()] > 0)
            return true;
        }
        // update pa->md[j];
        for(int i = 1; i <= r && px->md[i-1] > -INF; i++)
          if(px->md[i-1]+c > pa->md[i]) {
            pa->md[i] = px->md[i-1]+c;
          }
      }
    }
  }
  return false;
}

int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    int n, l, r;
    scanf("%d%d%d", &n, &l, &r);
    init(n);
    vector<int> v;
    for(int i = 1; i < n; i++) {
      int x, y, w;
      scanf("%d%d%d", &x, &y, &w);
      add_edge(x, y, w);
      v.push_back(w);
    }
    sort(v.begin(), v.end());
    v.resize(unique(v.begin(), v.end())-v.begin());
    int lo = 0, hi = v.size()-1;
    int ans;
    if(!median(v[hi], l, r))
        ans = -1;
    else if(median(v[lo], l, r))
      ans = v[lo];
    else {
      while(hi-lo > 1) {
        int mid = (hi+lo)/2;
        if(median(v[mid], l, r))
          hi = mid;
        else
          lo = mid;
      }
      ans = v[hi];
    }
    printf("%d\n", ans);
  }
  return 0;
}
