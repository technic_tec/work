-- Combinator definision:
--   constant:       K = (\x y. x)
--   substitution:   S = (\x y z. ((x z) (y z)))
--   Identity:       I = (\x. x) = SKK
--   composition:    B = (\x y z. (x (y z))) = S(KS)K
--   parameter flip: C = (\x y z. ((x z) y)) = S(S(K(S(KS)K))S)(KK).
--
-- Lambda expression grammar:
--   charset: A-Za-z0-9_().\
--   EXP = VAR | '(' EXP ' ' EXP ')' | '(' '\' VARLIST '.' EXP ')'
--   VARLIST = VAR VARLIST0 | VARLIST0
--   VARLIST0 = eps | ' ' VAR VARLIST0
--   VAR = [A-Za-z0-9_]+
--
-- Reduction rules:
--   T[x] => x
--   T[(E1 E2)] => (T[E1] T[E2])
--   T[\x.E] => (K T[E]) (if x does not occur free in E)
--   T[\x.x] => I
--   T[\x.\y.E] => T[\x.T[\y.E]] (if x occurs free in E)
--   T[\x.(E1 E2)] => (S T[\x.E1] T[\x.E2]) (if x occurs free in E1 or E2)
--   T[\x.(E1 E2)] => (C T[\x.E1] T[E2]) (if x is free in E1 but not E2)
--   T[\x.(E1 E2)] => (B T[E1] T[\x.E2]) (if x is free in E2 but not E1)
--   T[\x.(E x)] => T[E]
import Control.Monad
import Data.Maybe
import qualified Data.Set as Set
import Text.ParserCombinators.Parsec(Parser, (<|>), try, letter, char, alphaNum, oneOf, parse, many1)
import qualified Text.ParserCombinators.Parsec.Language as L
import qualified Text.ParserCombinators.Parsec.Token as T
import Text.ParserCombinators.Parsec.Expr

data Exp = Atom String | Lambda Exp Exp | Compound Exp Exp
    deriving (Eq, Show)

def = L.emptyDef{ L.identStart = alphaNum <|> (char '_')
		,  L.identLetter = alphaNum <|> (char '_')
		,  L.opStart = oneOf ".\\"
		,  L.opLetter = oneOf ".\\"
        ,  L.reservedOpNames = [".","\\"]
}
T.TokenParser{ T.parens = parens
		,  T.braces = braces
		,  T.whiteSpace = whiteSpace
		,  T.integer = integer
		,  T.identifier = identifier
		,  T.reservedOp = reservedOp
		,  T.reserved = reserved
		,  T.semiSep1 = semiSep1
} = T.makeTokenParser def

parseVar :: Parser Exp
parseVar = liftM Atom identifier

parseVarList :: Parser [Exp]
parseVarList = many1 parseVar

parseLambda :: Parser Exp
parseLambda = do
    reservedOp "\\"
    v <- parseVarList
    reservedOp "."
    e <- parseExp
    return $ foldr (\v e -> Lambda v e) e v

parseCompound :: Parser Exp
parseCompound = do
    e1 <- parseExp
    e2 <- parseExp
    return (Compound e1 e2)

parseExp :: Parser Exp
parseExp = whiteSpace >> (try (parens parseLambda) <|> try (parens parseCompound) <|> parseVar)

freeVal :: Exp -> Exp -> Bool
freeVal (Atom x) (Atom y) = (x == y)
freeVal (Atom x) (Compound e1 e2) = (freeVal (Atom x) e1) || (freeVal (Atom x) e2)
freeVal (Atom x) (Lambda (Atom y) e) = (y /= x) && (freeVal (Atom x) e)

reduceExp :: Exp -> Exp
reduceExp (Atom x) = Atom x
reduceExp (Compound e1 e2) = Compound (reduceExp e1) (reduceExp e2)
reduceExp (Lambda (Atom x) (Lambda (Atom y) e)) = reduceExp (Lambda (Atom x) (reduceExp (Lambda (Atom y) e)))
reduceExp (Lambda (Atom x) (Atom y)) | x == y = Atom "I"
                                     | otherwise = Compound (Atom "K") (Atom y)
reduceExp (Lambda (Atom x) (Compound e1 e2)) | not t1 && not t2 = Compound (Atom "K") (reduceExp (Compound e1 e2))
                                             | t1 && not t2 = Compound (Compound (Atom "C") (reduceExp (Lambda (Atom x) e1))) (reduceExp e2)
                                             | not t1 && t2 && e2==Atom x = reduceExp e1
                                             | not t1 && t2 && e2/=Atom x = Compound (Compound (Atom "B") (reduceExp e1)) (reduceExp (Lambda (Atom x) e2))
                                             | otherwise = Compound (Compound (Atom "S") (reduceExp (Lambda (Atom x) e1))) (reduceExp (Lambda (Atom x) e2))
    where t1 = freeVal (Atom x) e1
          t2 = freeVal (Atom x) e2

simplify :: Exp -> String
simplify (Atom x) = x
simplify (Compound e1 (Atom x)) = (simplify e1)++x
simplify (Compound e1 e2) = (simplify e1)++"("++(simplify e2)++")"
simplify (Lambda e1 e2) = error "unexpected lambda expression in reduced form"

process :: Int->IO()
process 0 = return()
process n = do
    s <- getLine
    case parse parseExp "LambdaExpParser" s of
        Left err -> print err
        Right exp -> putStrLn $ simplify $ reduceExp exp
    process (n-1)

main = do
    s <- getLine
    let n = read s
    process n
