#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N      100100
#define M     1000100
#define INF 100000000

int n, m, c[N], adj[N], dst[2*M], nxt[2*M];
bool v[N];

int stk[N], top;
int dfs(int x)
{
    int r = INF;
    top = -1;
    stk[++top] = x;
    v[x] = true;
    while(top >= 0) {
        x = stk[top--];
        if(c[x] >= 0 && c[x] < r)
            r = c[x];
        for(int e = adj[x]; e >= 0; e = nxt[e]) {
            int y = dst[e];
            if(!v[y]) {
                stk[++top] = y;
                v[y] = true;
            }
        }
    }
    return (r==INF?-1:r);
}

int GFA()
{
    memset(v, false, sizeof(v));
    int cnt = 0, cs = 0, cm = INF;
    for(int i = 1; i <= n; i++)
        if(!v[i]) {
            int cc = dfs(i);
            if(cc < 0)
                return -1;
            ++cnt;
            cs += cc;
            if(cc < cm) cm = cc;
        }
    return cm*(cnt-1) + (cs-cm);
}

int main()
{
    scanf("%d%d", &n, &m);

    memset(adj, -1, sizeof(adj));
    for(int i = 0; i < m; i++) {
        int x, y;
        scanf("%d%d", &x, &y);
        dst[2*i] = y; nxt[2*i] = adj[x]; adj[x] = 2*i;
        dst[2*i+1] = x; nxt[2*i+1] = adj[y]; adj[y] = 2*i+1;
    }
    for(int i = 1; i <= n; i++)
        scanf("%d", &c[i]);
    printf("%d\n", GFA());
    return 0;
}

