#!/usr/bin/python
import sys, re

dirs = [(-1, 0, 'north'), (0, -1, 'west'), (1, 0, 'south'), (0, 1, 'east')]
step = 10000000

class Maze(object):

    def __init__(self):
        self.ndynamics = 0
        self.done = False
        self.x, self.y, self.d = 0, 0, 0

    def start(self):
        #You wake up with a headache,  completely lost in a dark maze.
        #In your pockets you find a torch and n stick[s] of dynamite.
        s = sys.stdin.readline().strip();
        s = sys.stdin.readline().strip();
        m = re.search(r'([0-9]) sticks?', s)
        self.ndynamics = int(m.group(1))
        self.x, self.y, self.d = 0, 0, 0

    def extract_wall(self, mp, d):
        if d == 0:
            ret = ''.join([x[-1] for x in mp[-2::-1]])
        elif d == 1:
            ret = mp[0][-2::-1]
        elif d == 2:
            ret = ''.join([x[0] for x in mp[1:]])
        elif d == 3:
            ret = mp[-1][1:]
        else:
            raise Exception
        #sys.stderr.write('extract_wall: '+ret+'\n')
        return ret

    def cal_steps(self, mp, d):
        if d == 0:
            r = ''.join([x[1] for x in mp[-2::-1]])
        elif d == 1:
            r = mp[1][-2::-1]
        elif d == 2:
            r = ''.join([x[1] for x in mp[1:]])
        elif d == 3:
            r = mp[1][1:]
        else:
            raise Exception
        #sys.stderr.write('cal_steps: '+r+'\n')
        if r.find('X') >= 0:
            return r.index('X')-1
        else:
            return len(r)-1

    def walk(self, d, n):
        print 'walk %s %d' % (dirs[d][2], n)
        sys.stdout.flush()
        #You are still lost in a dark maze.
        #Against all odds,  you have found the exit. Congratulations!
        s = raw_input()
        self.x += n*dirs[d][0]
        self.y += n*dirs[d][1]
        if s.find('Congratulations') >= 0:
            self.done = True
            raise ValueError
        if s.find('STOP') >= 0:
            raise ValueError

    def walkto(self, px):
        x, y = px
        if self.x == x:
            if y < self.y:
                d, n = 1, self.y-y
            else:
                d, n = 3, y-self.y
        elif self.y == y:
            if x < self.x:
                d, n = 0, self.x-x
            else:
                d, n = 2, x-self.x
        else:
            raise Exception
        self.walk(d, n)
        self.d = d

    def look(self, d = None, n = -1):
        if n < 0:
            cmd = 'look around'
            d, n = 'north', 1
        else:
            cmd = 'look %s %d' % (d, n)
        print cmd
        sys.stdout.flush()
        #The maze around you looks like this:
        s = raw_input()
        if s.find('STOP') >= 0:
            raise ValueError
        ret = [raw_input().strip()]
        while ret[-1].find('O') < 0:
            ret.append(raw_input().strip())
        if d == 'south':
            while len(ret) < n+2 and ret[-1][1] != 'X' and ret[-1][1] != 'E':
                ret.append(raw_input().strip())
        else:
            ret.append(raw_input().strip())
        #sys.stderr.write('look: '+str(ret)+'\n')
        return ret

    def use_dynamic(self):
        print 'use dynamite'
        sys.stdout.flush()
        #You light up a stick of dynamite.
        #The explosion knocks you to the ground,  but amazingly you survive.
        s = raw_input()
        if s.find('STOP') >= 0:
            raise ValueError
        raw_input()

    def ExploreMaze(self):
        self.start()
        while not self.done:
            # go forward
            self.d = 0
            mp = self.look(dirs[self.d][2], step)
            while(mp[0][1] != 'X'):
                self.walk(self.d, step)
                if self.done:
                    return
                mp = self.look(dirs[self.d][2], step)
            if len(mp) > 3:
                self.walk(self.d, len(mp)-3)
            # explore
            path = list()
            while (not self.done) and ((self.x, self.y) not in path[:-1] or self.d != 1):
                if mp[1+dirs[self.d][0]][1+dirs[self.d][1]] == 'X':
                    if (self.x, self.y) not in path:
                        path.append((self.x, self.y))
                    self.d = (self.d+1)&3
                    continue
                mp = self.look(dirs[self.d][2], step)
                wall = self.extract_wall(mp, self.d)
                if wall.find('E') >= 0:
                    i = wall.index('E')
                    if i > 0:
                        self.walk(self.d, i)
                    self.d = (self.d-1)&3
                    self.walk(self.d, 1)
                elif wall.find('.', 1) >= 0 and wall.index('.', 1) < len(wall)-1:
                    i = wall.index('.', 1)
                    if len(path) > 1 and self.d == 1:
                        if self.d & 1: # E/W
                            if self.x == path[0][0] and (self.y - path[0][1])*(self.y + i*dirs[self.d][1] - path[0][1]) <= 0:
                                i = abs(path[0][1] - self.y)
                                self.walk(self.d, i)
                                break
                        else:          # N/S
                            if self.y == path[0][1] and (self.x - path[0][0])*(self.x + i*dirs[self.d][0] - path[0][0]) <= 0:
                                i = abs(path[0][0] - self.x)
                                self.walk(self.d, i)
                                break
                    self.walk(self.d, i)
                    path.append((self.x, self.y))
                    self.d = ((self.d-1)&3)
                    self.walk(self.d, 1)
                    mp = self.look()
                else:
                    i = self.cal_steps(mp, self.d)
                    if len(path) > 1 and self.d == 1:
                        if self.d & 1: # E/W
                            if self.x == path[0][0] and (self.y - path[0][1])*(self.y + i*dirs[self.d][1] - path[0][1]) <= 0:
                                i = abs(path[0][1] - self.y)
                        else:          # N/S
                            if self.y == path[0][1] and (self.x - path[0][0])*(self.x + i*dirs[self.d][0] - path[0][0]) <= 0:
                                i = abs(path[0][0] - self.x)
                    #sys.stderr.write('walk %d %d\n' % (self.d, i))
                    self.walk(self.d, i)
                    mp = self.look()

            # back to northest
            xl = [x for x, y in path]
            xm = min(xl)
            i, j = xl.index(xm), xl[::-1].index(xm)
            if i+1 <= j:
                for p in path[1:i+1]:
                    self.walkto(p)
            else:
                for p in path[-1:-j-2:-1]:
                    self.walkto(p)
                self.d ^= 2
                i = j

            # if inner loop, use dynamic
            self.d = 0
            mp = self.look()
            if mp[0][1] == 'X' and (self.d == 1 or (self.d != 3 and mp[1+dirs[(self.d-1)&3][0]][1+dirs[(self.d-1)&3][1]]=='X')):
                self.use_dynamic()

t = int(sys.stdin.readline())
for _ in range(t):
    mz = Maze()
    try:
        mz.ExploreMaze()
    except ValueError:
        pass
