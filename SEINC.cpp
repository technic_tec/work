#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>

#define   N 100100
#define INF 2002000

int n, a[N], vm[N];
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &n);
        for(int i = 1; i <= n; i++)
            scanf("%d", &a[i]);
        a[0] = 0;
        for(int i = 1; i <= n; i++) {
            int x;
            scanf("%d", &x);
            a[i] = (x-a[i]) & 3;
        }
        a[n+1] = 0;

        int res = 0, d = 0;
        const int cl[] = {0, 1, 2, 1};
        for(int i = 0; i <= n; i++) {
            a[i] = (a[i+1]-a[i])&3;
            res += cl[a[i]];
        }

        vm[0] = (a[0] == 3 ? (-1):a[0]);
        for(int i = 1; i <= n; i++)
            vm[i] = vm[i-1] + (a[i]==3 ? -1 : a[i]);
        for(int i = n-1; i >= 0; i--)
            vm[i] = (vm[i] < vm[i+1] ? vm[i] : vm[i+1]);
        if(vm[0] < 0) {
            d += (vm[0]&3) - vm[0];
            res += 2*(d / 4);
        }
        for(int i = 0; i <= n; i++)
            if(a[i] == 2 && vm[i] + d >= 4)
                d -= 4;
        res += 2*(vm[n]+d)/4;
        printf("%d\n", res/2);
    }
    return 0;
}

