/*
 * =====================================================================================
 *
 *       Filename:  NAME2.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年05月06日 15时22分18秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000
char sa[N], sb[N];

bool is_subseq(const char *sa, const char *sb)
{
    int la = strlen(sa), lb = strlen(sb);
    if(la < lb) {
        int lt = la; la = lb; lb = lt;
        const char *st = sa; sa = sb; sb = st;
    }
    int i = 0;
    for(int j = 0; j < la; j++)
        if(i < lb && sa[j] == sb[i])
            ++i;
    return i >= lb;
}
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%s%s", sa, sb);
        if(is_subseq(sa, sb))
            printf("YES\n");
        else
            printf("NO\n");
    }
    return 0;
}

