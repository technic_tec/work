#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
using namespace std;
#define K 100
#define N 100100

inline int pdac(long long n, int p, int ni[])
{
    int lni = 0;
    while(n) {
        ni[lni++] = n % p;
        n /= p;
    }
    assert(lni < K);
    return lni;
}

int inv[N];
int subseq(long long n, long long k, int p)
{
    if(n < k)
        return 0;
    int ni[K], ki[K];
    int lni = pdac(n,p,ni);
    int lki = pdac(k,p,ki);
    while(lki < lni)
        ki[lki++] = 0;
    int s = 0, c = 1;
    inv[0] = 0; inv[1] = 1;
    for(int i = 2; i < p; i++) {
        inv[i] = (-(p/i)*inv[p%i]) % p;
        if(inv[i] < 0)
            inv[i] += p;
    }
    for(int i = lni-1; i >= 0; i--) {
        // snk * sk-1 * sk-2 * ... * s0 + cnk * snk-1 * sk-2 * ... * s0
        // cni  = C(ni,ki)
        // sni = C(0,ki)+C(1,ki)+...+C(ni-1,ki)
        // si  = C(0,ki)+C(1,ki)+...+C(p-1,ki)
        int si = 0, sni, cni, cx = 1;
        for(int j = ki[i]; j < ni[i]; ++j) {
            si += cx;
            if(si >= p) si -= p;
            cx = (int)((long long)cx*(long long)(j+1)%p*(long long)inv[j-ki[i]+1]%p);
        }
        sni = si;
        cni = (ki[i]<=ni[i]?cx:0);
        for(int j = max(ni[i],ki[i]); j < p; ++j) {
            si += cx;
            if(si >= p) si -= p;
            cx = (int)((long long)cx*(long long)(j+1)%p*(long long)inv[j-ki[i]+1]%p);
        }
        //printf("%d(%d:%d): si=%d, sni=%d, cni=%d\n", i, ni[i], ki[i], si, sni, cni);
        s = (int)(((long long)s * (long long)si + (long long)c * (long long)sni) % p);
        c = (int)((long long)c * (long long)cni % p);
    }
    s += c;
    if(s >= p) s -= p;
    return s;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        long long n, k;
        int p;
        scanf("%lld%lld%d", &n, &k, &p);
        printf("%d\n", subseq(n,k,p));
    }
    return 0;
}
