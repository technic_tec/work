from random import randint, seed

seed()
n = randint(4, 10)
l = []
fi = open('meeting-point.in', 'w')
fo = open('meeting-point.out', 'w')
fi.write('%d\n' % n)
for i in range(n):
    x = randint(1, 100)
    y = randint(1, 100)
    l.append((x, y))
    fi.write('%d %d\n' % (x, y))
dm = None
for x, y in l:
    dx = sum(max(abs(x-u), abs(y-v)) for (u, v) in l)
    if dm is None or dx < dm:
        dm = dx
fo.write('%d\n' % dm)
fi.close()
fo.close()
