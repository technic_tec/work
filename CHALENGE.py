from random import seed,randint,sample
import sys

def randstr():
    n = randint(8, 12)
    s = ''.join([chr(96+randint(1, 26)) for y in range(n)])
    return s

seed()
t, n, h = map(int, sys.stdin.readline().split())
ans = [randstr() for x in range(n)]
sc0 = -1
for _ in range(t):
    i = randint(0, n-1)
    _s = ans[i]
    ans[i] = randstr()
    print '\n'.join(ans)
    sys.stdout.flush()
    sc = int(sys.stdin.readline())
    if sc > sc0:
        sc0 = sc
    else:
        ans[i] = _s
