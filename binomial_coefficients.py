t = int(raw_input())
for _ in range(t):
    n, p = map(long, raw_input().split())
    n0 = n
    cnt = 1
    while n > 0:
        d = n % p
        cnt *= d+1
        n /= p
    print n0+1-cnt
