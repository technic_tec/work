#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 3100

class Bitmap {
    public:
        Bitmap(int sz = 200) {
            m_sz = ((sz+31)>>5);
            m_b = new int[m_sz];
            for(int i = 0; i < m_sz; i++)
                m_b[i] = 0;
        }
        ~Bitmap() {
            delete[] m_b;
            m_b = NULL;
        }
        bool nonZero()
        {
            for(int i = 0; i < m_sz; i++)
                if(m_b[i])
                    return true;
            return false;
        }
        void set(int x)
        {
            m_b[x>>5] |= (1<<(x&31));
        }
        int bitcount()
        {
            if(!nonZero())
                return 0;
            int bc[] = {0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4};
            int c = 0;
            for(int i = 0; i < m_sz; i++)
                c += bc[(m_b[i]>>28)&0xf] + bc[(m_b[i]>>24)&0xf] + bc[(m_b[i]>>20)&0xf] + bc[(m_b[i]>>16)&0xf] + bc[(m_b[i]>>12)&0xf] + bc[(m_b[i]>>8)&0xf] + bc[(m_b[i]>>4)&0xf] + bc[m_b[i]&0xf];
            return c;
        }
        Bitmap operator<<(int k)
        {
            Bitmap r;
            int off = (k >> 5);
            k &= 31;
            if(k == 0) {
                for(int i = off; i < m_sz; i++)
                    r.m_b[i-off] = m_b[i];
                for(int i = m_sz-off; i < m_sz; i++)
                    r.m_b[i] = 0;
            } else if(off < m_sz) {
                for(int i = off; i < m_sz-1; i++)
                    r.m_b[i-off] = ((m_b[i]<<k)|((m_b[i+1]>>(32-k))&((1<<k)-1)));
                r.m_b[m_sz-1-off] = (m_b[m_sz-1]<<k);
                for(int i = m_sz-off; i < m_sz; i++)
                    r.m_b[i] = 0;
            } else {
                for(int i = 0; i < m_sz; i++)
                    r.m_b[i] = 0;
            }
            return r;
        }
        Bitmap operator&(Bitmap &o)
        {
            Bitmap r;
            for(int i = 0; i < r.m_sz; i++)
                r.m_b[i] = (m_b[i] & o.m_b[i]);
            return r;
        }
    private:
        int m_sz;
        int *m_b;
};

int m, n;
char b[N][N];
Bitmap star[26][2*N];

int main()
{
    scanf("%d%d", &m, &n);
    for(int i = 0; i < m; i++)
        scanf("%s", &b[i]);
    for(int i = 0; i < m; i++)
        for(int j = 0; j < n; j++)
            if(b[i][j] != '.')
                star[b[i][j]-'a'][i+j].set(j);
    long long c = 0;
    for(int i = 0; i < 26; i++)
        for(int j = 0; j <= m+n-2; j++)
            if(star[i][j].nonZero())
                for(int k = j-2; k >= 0 && k >= j-2*n; k-=2) {
                    int cc = ((star[i][k]<<((j-k)/2))&star[i][j]).bitcount();
                    c += (long long)cc*(long long)(cc-1)/2;
                }
    printf("%lld\n", c);
    return 0;
}

