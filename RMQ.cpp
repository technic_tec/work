#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

class RMQ01 {
public:
	RMQ01(int n, int v[]) {
        m_val = new int[n];
        memcpy(m_val, v, n*sizeof(int));
		m_sz = n;
        m_blksz = 0;
        for(int i = n; i; i>>=1)
            m_blksz++;
        m_blksz = ((m_blksz+1)>>1);
        m_blkno = (n+m_blksz-1)/m_blksz;
        m_blksz = (n+m_blkno-1)/m_blkno;
        m_blkmin = new int*[1<<(m_blksz-1)];

        m_dep = 0;
        for(j = 1; j < m_blkno; j<<=1, m_dep++);

        m_type = new int[m_blkno];
        m_min = new int[m_dep*m_blkno];
        for(int j = 0; j < m_blkno; j++) {
            int tp = 0;
            for(int k = j*m_blksz+1; k < j*m_blksz+m_blksz; j++)
                if(k < n && v[k] < v[k-1])
                    tp = (tp << 1) + 1;
                else
                    tp = (tp << 1);
            m_type[j] = tp;
            if(m_blkmin[tp] == NULL) {
                m_blkmin[tp] = new int[blksz*blksz];
            }
            m_min[0][j] = m_blkmin[blksz-1];
        }
	}
    RMQ01() {
        if(m_val)
            delete[] m_val;
        if(m_blkmin) {
            for(int i = 0; i < (1<<(m_blksz-1)); i++)
                if(m_blkmin[i]) {
                    delete[] m_blkmin[i];
                    m_blkmin[i] = NULL;
                }
            delete[] m_blkmin;
            m_blkmin = NULL;
        }
        m_val = NULL;
    }
private:
    int *m_val;
	int m_sz;
	int m_blksz;
    int m_blkno;

    int **m_blkmin;

    int m_dep;
    int *m_min;
    int *m_type;
};

class LCA {
};

class RMQ {
};
