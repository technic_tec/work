#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 20000
#define M 5000

int n, a[N], buc[N], nxt[N], ia[N];
int main()
{
    scanf("%d", &n);
    memset(buc, -1, sizeof(buc));
    for(int i = 0; i < n; i++)
        scanf("%d", &a[i]);
    for(int i = n-1; i >= 0; i--) {
        nxt[i] = buc[a[i]];
        buc[a[i]] = i;
    }
    int id = 0;
    for(int i = 0; i <= M; i++)
        if(buc[i] >= 0) {
            ++id;
            while(buc[i] >= 0) {
                ia[buc[i]] = id;
                buc[i] = nxt[buc[i]];
            }
        }
    return 0;
}

