class Node(object):
    """
    Binary Search Tree Node
    """
    def __init__(self,  data):
        """
        Node constructor
        """
        self.left = None
        self.right = None
        self.data = data

    def __str__(self):
        return "%s" % self.data 

class BinarySearchTree(object):
    """
    Binary Search Tree: left and right child and data of any object
    """
    def __init__(self):
        """
        Binary Search Tree constructor
        """
        self.root = None

    def insert(self,  data):
        """
        Insert new Node with data
        @param data: object to insert
        """
        if self.root is None:
            self.root = Node(data)
        else:
            node = self.root
            while True:
                if data < node.data:
                    if node.left is None:
                        node.left = Node(data)
                        break
                    else:
                        node = node.left
                elif data > node.data:
                    if node.right is None:
                        node.right = Node(data)
                        break
                    else:
                        node = node.right
                else:
                    break
    
    def find(self,  data):
        """
        Find node containg data
        @param data: object to find
        @return node if found None if not
        """
        if self.root is None:
            return None
        node = self.root
         
        while True:
            if data == node.data:
                return node
            elif data < node.data:
                if node.left is None:
                    return None
                else:
                    node = node.left
                else:
                    if node.right is None:
                        return None
                    else:
                        node = node.right

    def delete(self,  data):
        """
        Delete data
        @param data: content to delete
        """
        pass

    def printf(self,  type='in-order'):
        """
        Print tree content
        """
        pass 
