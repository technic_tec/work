import re

#Forbidden pattern
#.xxx
#cxxx
#.xxc
#.xx.*c
#.x.x*c
pats = ['.xxx', 'cxxx', '.xxc']
pre_pats = ['.xx.', '.x.x']
def is_forbidden(s):
  for p in pats:
    if p in s:
      return True
  sp = s[:s.index('c')]
  for p in pre_pats:
    if p in sp:
      return True
  return False

# Pattern simplification:
# ^xx -> x
# x..x..x -> x..x
re1 = re.compile(r'^x+')
re2 = re.compile(r'x(\.\.x)+')
def simplify(s):
  s = re1.sub('x', s)
  s = re2.sub('x..x', s)
  return s

tr = dict()
e = dict()
def explore(s):
  if s in tr:
    return
  tr[s] = set()
  ci = s.index('c')
  for d in [-3, -2, -1, 1, 2, 3]:
    if ci + d >= len(s) or (ci + d >= 0 and s[ci+d] == '.'):
      sx = s.replace('c', 'x')
      while(len(sx) <= ci+d):
        sx += '.'
      sx = sx[:ci+d]+'c'+sx[ci+d+1:]
      sx = simplify(sx)
      if not is_forbidden(sx):
        tr[s].add(sx)
        e[(s, d)] = sx
        explore(sx)
  #print s, list(tr[s])

def trial():
  explore('xc')
  dead = [x for x in tr if len(tr[x]) == 0]
  while len(dead) > 0:
    x = dead.pop()
    for y in tr:
      if x in tr[y]:
        tr[y].remove(x)
        for d in [-3, -2, -1, 1, 2, 3]:
          if (y, d) in e and e[(y, d)] == x:
            del e[(y, d)]
        if(len(tr[y]) == 0):
          dead.append(y)
    del tr[x]
  for x in tr:
    if len(tr[x]) > 1:
      for d in [-3, -2, -1, 1, 2, 3]:
        if (x, d) in e:
          y = e[(x, d)]
          print '%s[%d] = %s' % (x, d, y),
          while len(tr[y]) == 1:
            y = list(tr[y])[0]
            print '-> %s' % y,
          print ''
      print x, list(tr[x])

# h0[n] = h0[n-1] + h1[n-2] + h2[n-3]
# h1[n] = h3[n+1] + h0[n-2] + h3[n-1] + h5[n-3]
# h2[n] = h4[n+2] + h0[n-1] + h0[n-2] + h3[n-1] + h5[n-3]
# h3[n] = h0[n-2] + h1[n-3]
# h4[n] = h3[n-1] + h0[n-4]
# h5[n] = h3[n+1] + h0[n-1] + h0[n-2] + h3[n-1] + h5[n-3]

class Euler490Ref(object):
  def __init__(self):
    self.h0, self.h1, self.h2, self.h3, self.h4, self.h5 = [0, 1], [0], [0], [0], [0], [0]
  def get_h0(self, n):
    if n <= 0:
      return 0
    while len(self.h0) <= n:
      self.h0.append(-1)
    if self.h0[n] < 0:
      self.h0[n] = self.get_h0(n-1) + self.get_h1(n-2) + self.get_h2(n-3)
    return self.h0[n]
  def get_h1(self, n):
    if n <= 0:
      return 0
    while len(self.h1) <= n:
      self.h1.append(-1)
    if self.h1[n] < 0:
      self.h1[n] = self.get_h3(n+1) + self.get_h0(n-2) + self.get_h3(n-1) + self.get_h5(n-3)
    return self.h1[n]
  def get_h2(self, n):
    if n <= 0:
      return 0
    while len(self.h2) <= n:
      self.h2.append(-1)
    if self.h2[n] < 0:
      self.h2[n] = self.get_h4(n+2) + self.get_h0(n-1) + self.get_h0(n-2) + self.get_h3(n-1) + self.get_h5(n-3)
    return self.h2[n]
  def get_h3(self, n):
    if n <= 0:
      return 0
    while len(self.h3) <= n:
      self.h3.append(-1)
    if self.h3[n] < 0:
      self.h3[n] = self.get_h0(n-2) + self.get_h1(n-3)
    return self.h3[n]
  def get_h4(self, n):
    if n <= 0:
      return 0
    while len(self.h4) <= n:
      self.h4.append(-1)
    if self.h4[n] < 0:
      self.h4[n] = self.get_h3(n-1) + self.get_h0(n-4)
    return self.h4[n]
  def get_h5(self, n):
    if n <= 0:
      return 0
    while len(self.h5) <= n:
      self.h5.append(-1)
    if self.h5[n] < 0:
      self.h5[n] = self.get_h3(n+1) + self.get_h0(n-1) + self.get_h0(n-2) + self.get_h3(n-1) + self.get_h5(n-3)
    return self.h5[n]

  def run(self, n):
    res = self.get_h0(n)
    print '%d: %d' % (n, res)
    return res

class Euler490(object):
  def __init__(self):
    self.h0, self.h5 = [0, 1], [0]
    return
  def get_h0(self, n):
    if n <= 0:
      return 0
    while len(self.h0) <= n:
      self.h0.append(-1)
    if self.h0[n] < 0:
      self.h0[n] = self.get_h0(n-1) - self.get_h0(n-3) + self.get_h0(n-5) + self.get_h5(n-2) + self.get_h5(n-3)
    return self.h0[n]
  def get_h5(self, n):
    if n <= 0:
      return 0
    while len(self.h5) <= n:
      self.h5.append(-1)
    if self.h5[n] < 0:
      self.h5[n] = 2*self.get_h0(n-1) + self.get_h0(n-2) - self.get_h0(n-5) + self.get_h5(n-2) + self.get_h5(n-3) + self.get_h5(n-4)
    return self.h5[n]

  def run(self, n):
    res = self.get_h0(n)
    print '%d: %d' % (n, res)
    return res

ref = Euler490Ref()
cur = Euler490()
fs = map(ref.run, range(100))
print sum(fs[n]**3 for n in range(1, 11))
print sum(fs[n]**3 for n in range(1, 21))
fsr = fs
fs = map(cur.run, range(100))
print sum(fs[n]**3 for n in range(1, 11))
print sum(fs[n]**3 for n in range(1, 21))
assert fs == fsr
