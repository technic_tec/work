import Data.Array

perm_run :: Integer->(Integer->Integer)
perm_run 1 s | s==1 = 1
             | otherwise = 0
perm_run 2 s | s==1 = 2
             | otherwise = 0
perm_run n 1 = 2
perm_run n 2 = 2^n-4
perm_run n s = s*(perm_run (n-1) s) + 2*(perm_run (n-1) (s-1)) + (n-s)*(perm_run (n-1) (s-2))

perm_prt :: Integer->String
perm_prt 0 = ""
perm_prt n = (perm_prt (n-1)) ++ (perm_ln n 1)
  where perm_ln n s | s == n = (show (perm_run n s)) ++ "\n"
                    | otherwise = (show (perm_run n s)) ++ " " ++ (perm_ln n (s+1))

perm_sr :: Integer->(Integer->(Array (Integer,Integer) Integer -> (Integer, Integer)))
perm_sr n k c = ((div p d), (div q d))
  where fac 0 = 1
        fac n = n*(fac (n-1))
        --p = sum [c!(n, s)*(s-1)^k | s <- [1..n]]
        p = sum [c!(n, s)*(s-1)^k | s <- [1..n]]
        q = fac n
        d = gcd p q

frac_show :: (Integer, Integer)->String
frac_show (a, b) = (show a) ++ "/" ++ (show b)
--frac_show (a, b) = (show b)

iN = 90
c = array ((1, 1), (iN, iN))
          ([((1, 1),  1)] ++
           [((1, j),  0) | j <- [2..iN]] ++
           [((2, 1),  2)] ++
           [((2, j),  0) | j <- [2..iN]] ++
           [((i, 1),  2) | i <- [3..iN]] ++
           [((i, 2),  2^i-4) | i <- [3..iN]] ++
           [((i, j),  j * c!(i-1, j) + 2*c!(i-1, j-1) + (i-j)*c!(i-1, j-2)) | i <- [3..iN],  j <- [3..iN]])

showL :: [String] -> String
showL ls = "[ " ++ (_showL ls) ++ " ]"
  where _showL [] = ""
        _showL (c:[]) = c
        _showL (c:cs) = c ++ ", " ++ (_showL cs)

main :: IO()
main = do
--  putStrLn (showL [frac_show (perm_sr (2*k) k c) | k<-[1..5]])
--  putStrLn (showL [frac_show (perm_sr n 0 c) | n<-[2..iN]])
--  putStrLn (showL [frac_show (perm_sr n 1 c) | n<-[2..iN]])
--  putStrLn (showL [frac_show (perm_sr n 2 c) | n<-[2..iN]])
--  putStrLn (showL [frac_show (perm_sr n 3 c) | n<-[2..iN]])
--  putStrLn (showL [frac_show (perm_sr n 4 c) | n<-[2..iN]])
--  putStrLn (showL [frac_show (perm_sr n 5 c) | n<-[2..iN]])
    s <- getLine
    v <- return (map read (words s))
    putStrLn (frac_show (perm_sr (v!!1) (v!!0) c))
    
