/*
 * =====================================================================================
 *
 *       Filename:  JUNONGF.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年05月06日 16时14分09秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 1000
#define P 1000000007

int n, m0, m1, p[N], q[N];
long long v;

int power_mod(long long v, int k, int pp)
{
    int r = 1;
    int iv = (int)(v % pp);
    if(iv == 0)
        return 0;
    while(k) {
        if(k & 1)
            r = (int)((long long)r * (long long)iv % pp);
        iv = (int)((long long)iv * (long long)iv % pp);
        k >>= 1;
    }
    return r;
}
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        int p0, p1, a, b, c;
        scanf("%lld%d", &v, &n);
        scanf("%d%d%d%d%d%d", &p0, &p1, &a, &b, &c, &m0);
        p[0] = p0; p[1] = p1;
        for(int i = 2; i < n; i++)
            p[i] = (int)(((long long)a*(long long)a%m0*(long long)p[i-1] + (long long)b*(long long)p[i-2]%m0 + (long long)c) % m0);
        scanf("%d%d%d%d%d%d", &p0, &p1, &a, &b, &c, &m1);
        q[0] = p0; q[1] = p1;
        for(int i = 2; i < n; i++)
            q[i] = (int)(((long long)a*(long long)a%m1*(long long)q[i-1] + (long long)b*(long long)q[i-2]%m1 + (long long)c) % m1);
        int r = 1;
        for(int i = 0; i < n; i++)
            r = (int)((long long)r * ((long long)p[i]*(long long)m1 % (P-1) + (long long)q[i]) % (P-1));
        printf("%d\n", power_mod(v, r, P));
    }
    return 0;
}

