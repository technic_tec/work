/*
 * =====================================================================================
 *
 *       Filename:  primality.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年11月04日 15时38分43秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define N 1001000
#define M 0x7fffffffffffULL

typedef unsigned long long ULL;

int fac[N], sqrdiv[N], prm[N], np;

void prep()
{
    memset(fac, 0, sizeof(fac));
    np = 0;
    fac[1] = 1; sqrdiv[1] = 1;
    for(int i = 2; i < N; i++) {
        if(fac[i]) {
            int x = fac[i], c = 0, y = i;
            while(fac[y] == x) {
                y /= x; ++c;
            }
            sqrdiv[i] = sqrdiv[y]*(2*c+1);
        } else {
            prm[np++] = i;
            fac[i] = i; sqrdiv[i] = 3;
            if(i < 32768)
                for(int j = i*i; j < N; j += i)
                    if(!fac[j])
                        fac[j] = i;
        }
    }
}

bool isp[N];
int main()
{
    int t;
    prep();
    scanf("%d", &t);
    while(t--) {
        ULL L, R;
        scanf("%llu%llu", &L, &R);
        memset(isp, true, sizeof(isp));
        if(L == 0)
            isp[0] = isp[1] = false;
        else if(L == 1)
            isp[0] = false;
        for(int i = 0; i < np; i++) {
            ULL j = (L+prm[i]-1LL)/prm[i]*(ULL)prm[i];
            if(j < (ULL)prm[i]*(ULL)prm[i])
                j = (ULL)prm[i]*(ULL)prm[i];
            for(; j <= R; j += prm[i])
                isp[j-L] = false;
        }

        ULL c = 0;
        ULL x = 0;
        while(x*x < L) ++x;
        for(ULL i = L; i <= R; i++) {
            if(isp[i-L])
                ++c;
            else if(i == x*x) {
                if(sqrdiv[x] > 1 && fac[sqrdiv[x]] == sqrdiv[x])
                    ++c;
                ++x;
            }
        }
        printf("%llu\n", c);
    }
    return 0;
}
