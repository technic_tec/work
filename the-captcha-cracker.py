import sys
from itertools import groupby

def process(f):
    n, m = map(int, f.readline().split())
    data = [0]*m
    for i, c in enumerate(f.read().split()):
        x, y = i/m, i%m
        r, g, b = map(int, c.split(','))
        if max(r, g, b) < 50:
            data[y] |= (1<<x);
    letters = list()
    cx = list()
    for x in data:
        if x != 0:
            cx.append(x)
        elif len(cx) > 0:
            letters.append(tuple(cx))
            cx = list()
    return letters

def do_train(letters, answer, d):
    for l, c in zip(letters, answer):
        d.setdefault(c, set()).add(l)

def key(l):
    return (l[0], l[5], l[-1])

def train():
    d = dict()
    for i in range(25):
        fi = open('sampleCaptchas/input/input%02d.txt' % i)
        fo = open('sampleCaptchas/output/output%02d.txt' % i)
        letters = process(fi)
        ans = fo.read().strip()
        fi.close()
        fo.close()
        do_train(letters, ans, d)
    for c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789':
        assert (c in d and len(d[c]) == 1)
    dr = dict()
    for c, v in d.iteritems():
        v = list(v)[0]
        assert(key(v) not in dr)
        dr[key(v)] = c
    print len(dr), dr

#train()
cm = dict({(522240,  1572864,  522240): 'U',  (2095104,  34816,  2048): 'F',  (516096,  1677312,  393216): '6',  (2095104,  917504,  2095104): 'N',  (2095104,  1048576,  1048576): 'L',  (325632,  1624064,  458752): '5',  (401408,  1693696,  401408): '8',  (245760,  798720,  245760): '0',  (2095104,  786432,  2095104): 'W',  (516096,  1116160,  987136): 'G',  (2095104,  1579008,  516096): 'D',  (2095104,  428032,  1601536): 'R',  (196608,  2095104,  131072): '4',  (14336,  507904,  14336): 'V',  (516096,  1050624,  798720): 'C',  (6144,  24576,  6144): 'Y',  (2095104,  34816,  28672): 'P',  (516096,  1579008,  516096): 'O',  (2095104,  32768,  2095104): 'H',  (528384,  1693696,  401408): '3',  (552960,  1083392,  987136): 'S',  (2095104,  12288,  2095104): 'M',  (1056768,  1153024,  1073152): '2',  (1050624,  1050624,  1050624): 'I',  (1837056,  1079296,  1062912): 'Z',  (2095104,  798720,  1050624): 'K',  (1579008,  417792,  1579008): 'X',  (1574912,  51200,  14336): '7',  (2048,  2048,  2048): 'T',  (2095104,  1693696,  401408): 'B',  (24576,  1611776,  516096): '9',  (2095104,  1083392,  1050624): 'E',  (516096,  1841152,  1564672): 'Q',  (2080768,  143360,  2080768): 'A',  (786432,  522240,  522240): 'J',  (1056768,  1048576,  1048576): '1'})
letters = process(sys.stdin)
print ''.join([cm[key(l)] for l in letters])
