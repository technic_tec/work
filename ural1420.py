ar, ai = map(int, raw_input().split())
br, bi = map(int, raw_input().split())
u, v, w = ar*br+ai*bi, br*ai-ar*bi, br**2+bi**2
if w == 0:
  print 0
else:
  c = 0
  xl, xh = (u+w-1)/w-1, u/w+1
  yl, yh = (v+w-1)/w-1, v/w+1
  #assert abs(w*xl-u) <= w and abs(w*xh-u) <= w and abs(w*(xl-1)-u) > w and abs(w*(xh+1)-u) > w
  #assert abs(w*yl-v) <= w and abs(w*yh-v) <= w and abs(w*(yl-1)-v) > w and abs(w*(yh+1)-v) > w
  for x in range(xl, xh+1):
    for y in range(yl, yh+1):
      if (w*x-u)**2+(w*y-v)**2 < w**2:
        #assert (ar-br*x+bi*y)**2 + (ai-br*y-bi*x)**2 < br**2 + bi**2
        c += 1
  print c
