n = int(raw_input())

c = [0] * n
candidate = []

def search(i, js = 0):
  if len(candidate) == (n-1)/2:
    return True
  while 3*i <= n and c[i] == 3:
    i += 1
    js = 0
  if 3*i > n:
    return False
  if js == 0:
    js = 2*i
  for j in range(js, n-i+1):
    valid = True
    l = [i, j, n-j, i+n-j, n-i, j-i]
    for x in l:
      c[x] += 1
      if c[x] > 3:
        valid = False
    if valid:
      candidate.append((i, j))
      if search(i, j+1):
        return True
      candidate.pop()
    for x in l:
      c[x] -= 1
  return search(i+1)

#search(1)
#print c, candidate
candidate = [((n-1)/2, (n+1)/2)] + [(i, 2*i+1) for i in range(1, (n-1)/2)]
e = []
for i, j in candidate:
  for x in [i, j, n-i, n-j, j-i, n-j+i]:
    c[x] += 1
  for s in range(n):
    x, y, z = sorted((s, (s+i)%n, (s+j)%n))
    e.append((x, y, z))
print c
for x, y, z in sorted(e):
  print x+1, y+1, z+1
