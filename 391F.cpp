#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 4000000

int n, k;
long long v[N], d[N];
int main()
{
    scanf("%d%d", &n, &k);
    for(int i = 0; i < n; i++)
        scanf("%I64d", &v[i]);
    int j = 0;
    for(int i = 0; i < n; i++) {
        while(i+1 < n && (((j&1) && v[i]<=v[i+1]) || (!(j&1) && v[i]>=v[i+1])))
            ++i;
        v[j++] = v[i];
    }
    n = (j & ~1);
    long long s = 0;
    for(int i = 0; i < n; i += 2) {
        s += v[i+1] - v[i];
        if(i + 2 < n)
            d[i/2] = v[i+1] - v[i+2];
    }
    if(n > 2*k) {
        std::sort(d, d+n/2-1);
        for(int i = 0; i < n/2-k; i++)
            s -= d[i];
    }
    printf("%I64d\n", s);
    return 0;
}

