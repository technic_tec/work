import re
n = int(raw_input())
cnt = 0
for _ in range(n):
    s = raw_input()
    a,b,c = re.split('[+=]', s)
    bs0 = int(max(a+b+c),16) + 1
    for bs in range(bs0,17):
        if int(a,bs) + int(b,bs) == int(c,bs):
            cnt += 1
            break
print cnt