/*
 * =====================================================================================
 *
 *       Filename:  ural1486.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年08月05日 17时19分06秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 510

class BlockRank {
  public:
    BlockRank(int n, int m, int sz = 1)
      : m_h(n), m_w(m), m_sz(sz), m_vmax(-1)
        , m_v1(n, vector<int>(m, -1))
        , m_v2(n, vector<int>(m, -1))
        , m_pl(&m_v1), m_pc(&m_v2) {};

    inline void set(int x, int y, int v) {
#ifndef ONLINE_JUDGE
      assert(x >= 0 && x < m_h && y >= 0 && y < m_w && v >= 0);
#endif
      (*m_pc)[x][y] = v;
      if(v > m_vmax) m_vmax = v;
    }

    inline int get_size() {
      return m_sz;
    }

    inline bool get_dup(int &x1, int &y1, int &x2, int &y2) {
      vector<int> pv(m_vmax+1, -1);
      for(int i = 0; i <= m_h-m_sz; i++)
        for(int j = 0; j <= m_w-m_sz; j++) {
          int vx = pv[(*m_pc)[i][j]];
          if(vx >= 0) {
            x1 = vx >> 10; y1 = vx & 0x3ff;
            x2 = i; y2 = j;
            return true;
          } else
            pv[(*m_pc)[i][j]] = ((i<<10)|j);
        }
      return false;
    }

    bool expand(int nsz) {
#ifndef ONLINE_JUDGE
      assert(nsz > m_sz && nsz <= (m_sz << 1));
#endif
      vector<int> pv(m_vmax+1, -1);
      vector<vector<int> > pnxt(m_h-nsz+1, vector<int>(m_w-nsz+1, -1));
      vector<int> sa;
      sa.reserve((m_h-nsz+1)*(m_w-nsz+1));
      for(int i = m_h-nsz; i >= 0; i--)
        for(int j = m_w-nsz; j >= 0; j--) {
          int x = (*m_pc)[i+nsz-m_sz][j+nsz-m_sz];
          pnxt[i][j] = pv[x];
          pv[x] = (i<<10|j);
        }
      for(int i = 0; i <= m_vmax; i++)
        while(pv[i] >= 0) {
          sa.push_back(pv[i]);
          int x = pv[i]>>10, y = pv[i]&0x3ff;
          pv[i] = pnxt[x][y];
          pnxt[x][y] = -1;
        }
      while(!sa.empty()) {
        int i = sa.back()>>10, j = sa.back()&0x3ff;
        sa.pop_back();
        int x = (*m_pc)[i+nsz-m_sz][j];
        pnxt[i][j] = pv[x];
        pv[x] = (i<<10|j);
      }
      for(int i = 0; i <= m_vmax; i++)
        while(pv[i] >= 0) {
          sa.push_back(pv[i]);
          int x = pv[i]>>10, y = pv[i]&0x3ff;
          pv[i] = pnxt[x][y];
          pnxt[x][y] = -1;
        }
      while(!sa.empty()) {
        int i = sa.back()>>10, j = sa.back()&0x3ff;
        sa.pop_back();
        int x = (*m_pc)[i][j+nsz-m_sz];
        pnxt[i][j] = pv[x];
        pv[x] = (i<<10|j);
      }
      for(int i = 0; i <= m_vmax; i++)
        while(pv[i] >= 0) {
          sa.push_back(pv[i]);
          int x = pv[i]>>10, y = pv[i]&0x3ff;
          pv[i] = pnxt[x][y];
          pnxt[x][y] = -1;
        }
      while(!sa.empty()) {
        int i = sa.back()>>10, j = sa.back()&0x3ff;
        sa.pop_back();
        int x = (*m_pc)[i][j];
        pnxt[i][j] = pv[x];
        pv[x] = (i<<10|j);
      }
      for(int i = 0; i <= m_vmax; i++)
        while(pv[i] >= 0) {
          sa.push_back(pv[i]);
          int x = pv[i]>>10, y = pv[i]&0x3ff;
          pv[i] = pnxt[x][y];
          pnxt[x][y] = -1;
        }
      int k = 0;
      for(int i = 0; i < sa.size(); i++) {
        int x = sa[i]>>10, y = sa[i]&0x3ff;
        if(i > 0) {
          int x1 = sa[i-1]>>10, y1 = sa[i-1]&0x3ff;
          if((*m_pc)[x][y] != (*m_pc)[x1][y1] || (*m_pc)[x][y+nsz-m_sz] != (*m_pc)[x1][y1+nsz-m_sz] || (*m_pc)[x+nsz-m_sz][y] != (*m_pc)[x1+nsz-m_sz][y1] || (*m_pc)[x+nsz-m_sz][y+nsz-m_sz] != (*m_pc)[x1+nsz-m_sz][y1+nsz-m_sz])
            ++k;
        }
        (*m_pl)[x][y] = k;
      }
      if(k == sa.size()-1)
        return false;
      m_sz = nsz;
      m_vmax = k;
      swap(m_pl, m_pc);
      return true;
    }
  private:
    int m_h, m_w, m_sz;
    int m_vmax;
    vector<vector<int> > m_v1, m_v2, *m_pl, *m_pc;
};

char s[N];
int n, m;

int main()
{
  scanf("%d%d", &n, &m);
  BlockRank rk(n, m);
  for(int i = 0; i < n; i++) {
    scanf("%s", s);
    for(int j = 0; j < m; j++)
      rk.set(i, j, s[j]-'a');
  }
  int x1, y1, x2, y2;
  if(!rk.get_dup(x1, y1, x2, y2)) {
    printf("0\n");
    return 0;
  }
  int k = 1;
  while((k<<1) <= min(m, n) && rk.expand(k<<1))
    k <<= 1;
  int l = k, h = min(k<<1, min(m, n)+1);
  while(h - l > 1) {
    int mid = (h+l)/2;
    if(rk.expand(mid))
      l = mid;
    else
      h = mid;
  }
  bool ret = rk.get_dup(x1, y1, x2, y2);
#ifndef ONLINE_JUDGE
  assert(ret);
#endif
  printf("%d\n%d %d\n%d %d\n", rk.get_size(), x1+1, y1+1, x2+1, y2+1);
  return 0;
}
