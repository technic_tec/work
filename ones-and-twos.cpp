/*
 * =====================================================================================
 *
 *       Filename:  ones-and-twos.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年03月15日 17时20分37秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000


/*
 * n = 2^r1 + 2^r2 + ... + 2^rx + ... + 2^rk
 * suppose ri == i for 1<=i<x and rx > x
 * then n+2 = 2^x + 2^rx + ... + 2^rk
 *
 * 1. x == 1:
 *   (x + r1 + ... + rk) - (r1 + r2 + ... + rx + ... + rk) = 1
 * 2. x == 2:
 *   (x + r2 + ... + rk) - (r1 + r2 + ... + rx + ... + rk) = (2 - 1) = 1
 * 3. x > 2:
 *   (x + rx + ... + rk) - (r1 + r2 + ... + rx + ... + rk) = x - (1+2+...+(x-1)) >= x - (1+(x-1)) = 0
 *
 * n = 2^r1 + 2^r2 + ... + 2^rk (r1 < r2 < ... < rk,  r1 + r2 + ... + rk == B)
 * nxt = 2^r2 + 2^r2 + ... + 2^rk (r1 < r2 < ... < rk,  r1 + r2 + ... + rk == B)
 * gap = nxt - n == 2^r2 - 2^r1 (r2 > r1 > 0)
 *
 * sum(gap) = sum(2^r2) - sum(2^r1)
 * sum(fill) = a*count(gap)
 *
 * sum(gap) - sum(fill) + sum(excess fill)
 */

int main()
{
    return 0;
}

