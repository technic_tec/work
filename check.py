#!/usr/bin/python
import os, sys
from random import *
from subprocess import *

def gen():
    seed()
    t = randint(50, 100)
    inp = '%d\n' % t
    for _ in range(t):
        n, m = randint(100, 2000), randint(100, 2000)
        inp += '%d %d\n' % (n, m)
    return inp

def run(prog, inp):
    p1 = Popen([prog],  stdin=PIPE,  stdout=PIPE)
    output = p1.communicate(inp)[0]
    return output

if len(sys.argv) < 3:
    print '%s <prog> <ref>' % sys.argv[0]
    raise SystemExit

nCase = 1
if len(sys.argv) == 4:
    nCase = int(sys.argv[3])
nfail = 0
for _ in range(nCase):
    inp = gen()
    out = run(sys.argv[1], inp)
    ref = run(sys.argv[2], inp)
    if out != ref:
        nfail += 1
        f = open('fail%03d.in' % nfail, 'w')
        f.write(inp)
        f.close()
        f = open('fail%03d.out' % nfail, 'w')
        f.write(out)
        f.close()
        f = open('fail%03d.ref' % nfail, 'w')
        f.write(ref)
        f.close()
    else:
        f = open('pass%03d.in' % _, 'w')
        f.write(inp)
        f.close()
if nfail > 0:
    print 'Failed %d/%d' % (nfail, nCase)
else:
    print 'Passed %d/%d' % (nCase, nCase)
