#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009

//length of local root ri = local period at position i
//maxima local period in a word = global period
//global period can be computed using KMP
//Critical Factorization Theorem: the period of a word w is always locally detectable in at least one position of w resulting in a corresponding critical factorization.
//critical factorization can be found in p=|w|-l, where l=min(|v|, |v'|), v is the maximal suffix in alphabet order, while v' is maximal suffix in reverse alphabet order.

int maximal_suffix(int n, const char *w)
{
  int i = 0, j = 1, k = 0, p = 1;
  while(j + k < n) {
    if(w[i+k] > w[j+k]) {
      j += k+1;
      k = 0; p = j-i;
    } else if(w[i+k] < w[j+k]) {
      i = j; j++;
      k = 0; p = 1;
    } else if(k != p)
      ++k;
    else {
      j += p;
      k = 0;
    }
  }
  return i;
}

int maximal_suffix_tilde(int n, const char *w)
{
  int i = 0, j = 1, k = 0, p = 1;
  while(j + k < n) {
    if(w[i+k] < w[j+k]) {
      j += k+1;
      k = 0; p = j-i;
    } else if(w[i+k] > w[j+k]) {
      i = j; j++;
      k = 0; p = 1;
    } else if(k != p)
      ++k;
    else {
      j += p;
      k = 0;
    }
  }
  return i;
}

inline int critical_factorization(int n, const char *w)
{
  int r1 = maximal_suffix(n, w);
  int r2 = maximal_suffix_tilde(n, w);
  return max(r1, r2);
}

int global_period(int n, const char *w)
{
  vector<int> t;
  t.push_back(-1);
  t.push_back(0);
  int pos = 2, cnd = 0;
  while(pos <= n) {
    if(w[pos-1] == w[cnd]) {
      cnd++;
      t.push_back(cnd);
      pos++;
    } else if(cnd > 0)
      cnd = t[cnd];
    else {
      t.push_back(0);
      pos++;
    }
  }
  return n - t.back();
}

char w[300100];
int n;
int main()
{
  scanf("%s", w);
  n = strlen(w);
  int p = critical_factorization(n, w);
  int l = global_period(n, w);
  printf("%d %d\n", p?p:(p+l), l);
  return 0;
}
