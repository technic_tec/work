mc = dict

def move0(s,x):
    if x == 9:
      yield ''.join(s)
      return
    elif s[x] != '.':
      for r in move0(s,x+1):
          yield r
      return
    s[x] = '*'
    for r in move0(s,x+1):
      yield r
    s[x] = '.'
    if x % 3 < 2 and s[x+1] == '.':
      s[x],s[x+1] = 'L', 'R'
      for r in move0(s,x+1):
          yield r
      s[x],s[x+1] = '.','.'
    if x < 6:
          s[x],s[x+3] = 'T','B'
          for r in move0(s,x+1):
              yield r
          s[x],s[x+3] = '.','.'

def move(s,x):
    if x == 9:
      yield s
      return
    elif (s >> x) & 1:
      for r in move(s,x+1):
          yield r
      return
    for r in move(s,x+1):
      yield r
    if x % 3 < 2 and ((s >> (x+1)) & 1) == 0:
      for r in move(s | (3<<x),x+1):
          yield r
    if x < 6:
          for r in move(s | (9<<x),x+1):
              yield r

#for i, s in enumerate(move0(['.']*9, 0)):
#    print '%d: ' % i
#    print '\n'.join([s[:3],s[3:6],s[6:],'---'])
cx = [0] * 512              
for s in move(0, 0):
  cx[s] += 1
sc = dict()
for i in range(512):
  sc.setdefault(cx[i], set()).add(i)
for k in sc:
  print k, sc[k], k*len(sc[k])
pls = [0]*512
for i in range(512):
  for j in range(512):
    if (i & j) == j:
      pls[i] += cx[j]
print pls
