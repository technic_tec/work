// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 2014/6/8 4:56:17
// Home Page: http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Darts.java

package com.darts;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;
import com.darts.MersenneTwister;

// Referenced classes of package com.darts:
//            MersenneTwister

public class Darts
{

    public Darts()
    {
        mLastDartScore = -1;
        mCurrentScore = 501;
        mDartCounter = 0;
        mCurrentDartCounter = 0;
        mCurrentRoundDartCounter = 0;
        mMt = new MersenneTwister(seed);
        mMtTraining = new MersenneTwister(seed);
        mMtPrefered = new MersenneTwister(seed);
        if(mDeviation < 0.0D)
        {
            MersenneTwister mMtDeviation = new MersenneTwister(seed);
            if(seed % 5L == 0L)
                mDeviation = 0.01D + mMtDeviation.nextDouble() * 0.089999999999999997D;
            else
                if(seed % 5L == 1L)
                    mDeviation = 0.29999999999999999D + mMtDeviation.nextDouble() * 0.69999999999999996D;
                else
                    mDeviation = 0.10000000000000001D + mMtDeviation.nextDouble() * 0.20000000000000001D;
        }
        prefCnt = 2 + mMtPrefered.next(30) % 4;
        prefRadius = new double[prefCnt];
        prefX = new double[prefCnt];
        prefY = new double[prefCnt];
        prefVar = new double[prefCnt];
        for(int i = 0; i < prefCnt; i++)
        {
            double x = mMtPrefered.nextDouble() * 2D - 1.0D;
            double y = mMtPrefered.nextDouble() * 2D - 1.0D;
            if(x * x + y * y > 1.0D)
            {
                i--;
            } else
            {
                double rad = 0.050000000000000003D + mMtPrefered.nextDouble() / 4D;
                double var = mDeviation * (mMtPrefered.nextDouble() / 2D + 0.25D);
                prefX[i] = x;
                prefY[i] = y;
                prefVar[i] = var;
                prefRadius[i] = rad;
            }
        }

    }

    public double CalcDeviation(double x, double y)
    {
        double res = mDeviation;
        for(int i = 0; i < prefRadius.length; i++)
            if((x - prefX[i]) * (x - prefX[i]) + (y - prefY[i]) * (y - prefY[i]) < prefRadius[i] * prefRadius[i] && res > prefVar[i])
                res = prefVar[i];

        return res;
    }

    public void ThrowCalc(double x, double y)
    {
        double angle = Math.toDegrees(Math.atan2(x, y)) + 180D;
        double dist = Math.sqrt(x * x + y * y);
        mLastDartMulti = 1;
        mLastDartScore = -1;
        if(dist < 0.037352941176470589D)
        {
            mLastDartScore = 25;
            mLastDartMulti = 2;
        } else
            if(dist < 0.093529411764705889D)
                mLastDartScore = 25;
            else
                if(dist > 1.0D)
                    mLastDartScore = 0;
                else
                    if(dist > 0.58235294117647063D && dist < 0.62941176470588234D)
                        mLastDartMulti = 3;
                    else
                        if(dist > 0.95294117647058818D)
                            mLastDartMulti = 2;
        if(mLastDartScore == -1)
        {
            double actd = 9D;
            mLastDartScore = 20;
            int i = 0;
            do
            {
                if(i >= 20)
                    break;
                if(actd > angle)
                {
                    mLastDartScore = mScoreOrder[(20 - i) % 20];
                    break;
                }
                actd += 18D;
                i++;
            } while(true);
        }
    }

    public void ThrowDart(double _x, double _y)
    {
        double var = CalcDeviation(_x, _y);
        double x = _x + mMt.nextGaussian() * var;
        double y = _y + mMt.nextGaussian() * var;
        mLastDartX = x;
        mLastDartY = y;
        mDartCounter++;
        mCurrentDartCounter++;
        mCurrentRoundDartCounter++;
        ThrowCalc(x, y);
        processThrow();
    }

    public void processThrow()
    {
        mCurrentRoundScore += mLastDartScore * mLastDartMulti;
        if(mCurrentScore < mCurrentRoundScore || mCurrentScore == mCurrentRoundScore && mLastDartMulti != 2 || mCurrentScore == mCurrentRoundScore + 1)
        {
            mCurrentRoundScore = 0;
            mDartCounter += 3 - mCurrentRoundDartCounter;
            if(mDartCounter > mTotalDarts)
                mDartCounter = mTotalDarts;
            mCurrentDartCounter += 3 - mCurrentRoundDartCounter;
            mCurrentRoundDartCounter = 0;
        } else
            if(mCurrentScore == mCurrentRoundScore && mLastDartMulti == 2)
            {
                mCurrentScore = 501;
                mCurrentDartCounter = 0;
                mFullScore += mCurrentRoundScore;
                mCurrentRoundDartCounter = 0;
                mCurrentRoundScore = 0;
            } else
                if(mCurrentRoundDartCounter == 3)
                {
                    mCurrentScore = mCurrentScore - mCurrentRoundScore;
                    mFullScore += mCurrentRoundScore;
                    mCurrentRoundDartCounter = 0;
                    mCurrentRoundScore = 0;
                }
    }

    public void runTest()
    {
        BufferedReader reader;
        PrintWriter writer;
        solution = null;
        try
        {
            solution = Runtime.getRuntime().exec(execCommand);
        }
        catch(Exception e)
        {
            System.err.println((new StringBuilder()).append("ERROR: Unable to execute your solution using the provided command: ").append(execCommand).append(".").toString());
            return;
        }
        reader = new BufferedReader(new InputStreamReader(solution.getInputStream()));
        writer = new PrintWriter(solution.getOutputStream());
        String li;
        String sp[];
        int N;
        double _x;
        double _y;
        double var;
        try
        {
            li = reader.readLine();
            sp = li.split(" ");
            if(sp.length != 1)
                return;
        }
        catch(Exception e)
        {
            System.err.println("ERROR: unable to parse your return value.");
            e.printStackTrace();
            return;
        }
        N = Integer.parseInt(sp[0]);
		//System.err.println((new StringBuffer()).append("N: ").append(N).toString());
        if(N > mMaxTraining)
        {
            System.err.println((new StringBuilder()).append("ERROR: Training number should be less than: ").append(mMaxTraining).append(".").toString());
            return;
        }
        while(N-- > 0) {
            try
            {
                li = reader.readLine();
                sp = li.split(" ");
                if(sp.length != 2)
                    return;
            }
            // Misplaced declaration of an exception variable
            catch(Exception e)
            {
                System.err.println("ERROR: unable to parse your return value.");
                e.printStackTrace();
                return;
            }
            _x = Double.parseDouble(sp[0]);
            _y = Double.parseDouble(sp[1]);
            if(_x * _x + _y * _y > mMaxRadius)
            {
                System.err.println((new StringBuilder()).append("You have to throw the dart in the ").append(mMaxRadius).append(" radius.").toString());
                return;
            }
            var = CalcDeviation(_x, _y);
            mLastDartX = _x + mMtTraining.nextGaussian() * var;
            mLastDartY = _y + mMtTraining.nextGaussian() * var;
            ThrowCalc(mLastDartX, mLastDartY);
            writer.println(mLastDartX);
            writer.println(mLastDartY);
            writer.println(mLastDartMulti);
            writer.println(mLastDartScore);
            writer.flush();
			System.err.println((new StringBuffer()).append("Exec[").append(mCurrentRoundDartCounter).append("-").append(mCurrentDartCounter).append("-").append(mDartCounter).append("]: (").append(_x).append(", ").append(_y).append(")->(").append(mLastDartX).append(", ").append(mLastDartY).append(") sc ").append(mLastDartMulti).append("*").append(mLastDartScore).toString());
        }
        while(mDartCounter < mTotalDarts) {
            try
            {
                li = reader.readLine();
                sp = li.split(" ");
                if(sp.length != 2)
                    return;
            }
            catch(Exception e)
            {
                System.err.println("ERROR: unable to parse your return value.");
                e.printStackTrace();
                return;
            }
            _x = Double.parseDouble(sp[0]);
            _y = Double.parseDouble(sp[1]);
            if(_x * _x + _y * _y > mMaxRadius)
            {
                System.err.println((new StringBuilder()).append("You have to throw the dart in the ").append(mMaxRadius).append(" radius.").toString());
                return;
            }
            ThrowDart(_x, _y);
            writer.println(mLastDartX);
            writer.println(mLastDartY);
            writer.println(mLastDartMulti);
            writer.println(mLastDartScore);
            writer.flush();
			System.err.println((new StringBuffer()).append("Test[").append(mCurrentRoundDartCounter).append("-").append(mCurrentDartCounter).append("-").append(mDartCounter).append("]: (").append(_x).append(", ").append(_y).append(")->(").append(mLastDartX).append(", ").append(mLastDartY).append(") sc ").append(mLastDartMulti).append("*").append(mLastDartScore).toString());
        }
        try
        {
            int retvalue = solution.exitValue();
            System.err.println((new StringBuilder()).append("Your program exited with return value: ").append(retvalue).append(".").toString());
        }
        catch(Exception e)
        {
            System.err.println("Your program still running, maybe you will get TLE on the server.");
        }
        return;
    }

    public static void main(String args[])
    {
        mDeviation = -1D;
        for(int i = 0; i < args.length; i++)
        {
            if(args[i].equals("-exec"))
            {
                execCommand = args[++i];
                continue;
            }
            if(args[i].equals("-seed"))
            {
                seed = Long.parseLong(args[++i]);
                continue;
            }
            if(args[i].equals("-deviation"))
            {
                mDeviation = Double.parseDouble(args[++i]);
                continue;
            }
            else
                System.out.println((new StringBuilder()).append("WARNING: unknown argument ").append(args[i]).append(".").toString());
        }

        Darts d = new Darts();
        d.runTest();
        System.out.println(String.format("Score: %d ; Average: %f", new Object[] {
                    Integer.valueOf(d.mFullScore), Double.valueOf(((double)d.mFullScore * 1.0D) / (double)mTotalDarts)
                    }));
    }

    private int mLastDartScore;
    private int mLastDartMulti;
    private double mLastDartX;
    private double mLastDartY;
    private int mCurrentScore;
    private int mCurrentDartCounter;
    private int mFullScore;
    private int mDartCounter;
    private int mCurrentRoundScore;
    private int mCurrentRoundDartCounter;
    private static int mScoreOrder[] = {
        20, 1, 18, 4, 13, 6, 10, 15, 2, 17, 
        3, 19, 7, 16, 8, 11, 14, 9, 12, 5
    };
    private static int mMaxTraining = 0x186a0;
    private static double mMaxRadius = 1.2D;
    private MersenneTwister mMt;
    private MersenneTwister mMtTraining;
    private MersenneTwister mMtPrefered;
    private int prefCnt;
    private double prefRadius[];
    private double prefX[];
    private double prefY[];
    private double prefVar[];
    public static String execCommand = null;
    public static long seed = 1L;
    public static Process solution;
    public static double mDeviation;
    public static int mTotalDarts = 0x1869f;
}
