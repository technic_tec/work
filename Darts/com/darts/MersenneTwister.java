// Decompiled by DJ v3.12.12.98 Copyright 2014 Atanas Neshkov  Date: 2014/6/8 4:56:18
// Home Page: http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   MersenneTwister.java

package com.darts;


public class MersenneTwister
{

    public MersenneTwister()
    {
        this(System.currentTimeMillis());
    }

    public MersenneTwister(long seed)
    {
        setSeed(seed);
    }

    public synchronized void setSeed(long seed)
    {
        __haveNextNextGaussian = false;
        mt = new int[624];
        mag01 = new int[2];
        mag01[0] = 0;
        mag01[1] = 0x9908b0df;
        mt[0] = (int)(seed & -1L);
        mt[0] = (int)seed;
        for(mti = 1; mti < 624; mti++)
            mt[mti] = 0x6c078965 * (mt[mti - 1] ^ mt[mti - 1] >>> 30) + mti;

    }

    protected synchronized int next(int bits)
    {
        int y;
        if(mti >= 624)
        {
            int mt[] = this.mt;
            int mag01[] = this.mag01;
            int kk;
            for(kk = 0; kk < 227; kk++)
            {
                y = mt[kk] & 0x80000000 | mt[kk + 1] & 0x7fffffff;
                mt[kk] = mt[kk + 397] ^ y >>> 1 ^ mag01[y & 1];
            }

            for(; kk < 623; kk++)
            {
                y = mt[kk] & 0x80000000 | mt[kk + 1] & 0x7fffffff;
                mt[kk] = mt[kk + -227] ^ y >>> 1 ^ mag01[y & 1];
            }

            y = mt[623] & 0x80000000 | mt[0] & 0x7fffffff;
            mt[623] = mt[396] ^ y >>> 1 ^ mag01[y & 1];
            mti = 0;
        }
        y = this.mt[mti++];
        y ^= y >>> 11;
        y ^= y << 7 & 0x9d2c5680;
        y ^= y << 15 & 0xefc60000;
        y ^= y >>> 18;
        return y >>> 32 - bits;
    }

    public double nextDouble()
    {
        return (double)(((long)next(26) << 27) + (long)next(27)) / 9007199254740992D;
    }

    public synchronized double nextGaussian()
    {
        if(__haveNextNextGaussian)
        {
            __haveNextNextGaussian = false;
            return __nextNextGaussian;
        }
        double v1;
        double v2;
        double s;
        do
        {
            v1 = 2D * nextDouble() - 1.0D;
            v2 = 2D * nextDouble() - 1.0D;
            s = v1 * v1 + v2 * v2;
        } while(s >= 1.0D || s == 0.0D);
        double multiplier = StrictMath.sqrt((-2D * StrictMath.log(s)) / s);
        __nextNextGaussian = v2 * multiplier;
        __haveNextNextGaussian = true;
        return v1 * multiplier;
    }

    private static final int N = 624;
    private static final int M = 397;
    private static final int MATRIX_A = 0x9908b0df;
    private static final int UPPER_MASK = 0x80000000;
    private static final int LOWER_MASK = 0x7fffffff;
    private static final int TEMPERING_MASK_B = 0x9d2c5680;
    private static final int TEMPERING_MASK_C = 0xefc60000;
    private int mt[];
    private int mti;
    private int mag01[];
    private double __nextNextGaussian;
    private boolean __haveNextNextGaussian;
}