#include <stdio.h>
#include <stdlib.h>
#include <math.h>

const int mScoreOrder[20] = {
    20, 1, 18, 4, 13, 6, 10, 15, 2, 17, 
    3, 19, 7, 16, 8, 11, 14, 9, 12, 5
};
class Darts
{
    public:
    Darts()
    {
        mLastDartScore = -1;
        mCurrentScore = 501;
        mCurrentRoundScore = 0;
        mFullScore = 0;
        mDartCounter = 0;
        mCurrentDartCounter = 0;
        mCurrentRoundDartCounter = 0;
    }

    inline double toDegrees(double rad) {
        const double pi = acos(-1.0);
        double deg = rad/pi*180.0;
        return deg;
    }

#if 0
    void ThrowCalc(double x, double y)
    {
        double angle = toDegrees(atan2(x, y)) + 180.0;
        double dist = sqrt(x * x + y * y);
        mLastDartMulti = 1;
        mLastDartScore = -1;
        if(dist < 0.037352941176470589)
        {
            mLastDartScore = 25;
            mLastDartMulti = 2;
        } else
            if(dist < 0.093529411764705889)
                mLastDartScore = 25;
            else
                if(dist > 1.0)
                    mLastDartScore = 0;
                else
                    if(dist > 0.58235294117647063 && dist < 0.62941176470588234)
                        mLastDartMulti = 3;
                    else
                        if(dist > 0.95294117647058818)
                            mLastDartMulti = 2;
        if(mLastDartScore == -1)
        {
            double actd = 9;
            mLastDartScore = 20;
            int i = 0;
            do
            {
                if(i >= 20)
                    break;
                if(actd > angle)
                {
                    mLastDartScore = mScoreOrder[(20 - i) % 20];
                    break;
                }
                actd += 18;
                i++;
            } while(true);
        }
    }
#endif

    void ThrowDart(double _x, double _y)
    {
        int m, s;
        printf("%.6lf %.6lf\n", _x, _y);
        fflush(stdout);
        scanf("%lf%lf%d%d", &mLastDartX, &mLastDartY, &mLastDartMulti, &mLastDartScore);
        mDartCounter++;
        mCurrentDartCounter++;
        mCurrentRoundDartCounter++;
        processThrow();
    }

    void processThrow()
    {
        mCurrentRoundScore += mLastDartScore * mLastDartMulti;
        if(mCurrentScore < mCurrentRoundScore || mCurrentScore == mCurrentRoundScore && mLastDartMulti != 2 || mCurrentScore == mCurrentRoundScore + 1)
        {
            mCurrentRoundScore = 0;
            mDartCounter += 3 - mCurrentRoundDartCounter;
            if(mDartCounter > mTotalDarts)
                mDartCounter = mTotalDarts;
            mCurrentDartCounter += 3 - mCurrentRoundDartCounter;
            mCurrentRoundDartCounter = 0;
        } else if(mCurrentScore == mCurrentRoundScore && mLastDartMulti == 2)
        {
            mCurrentScore = 501;
            mCurrentDartCounter = 0;
            mFullScore += mCurrentRoundScore;
            mCurrentRoundDartCounter = 0;
            mCurrentRoundScore = 0;
        } else if(mCurrentRoundDartCounter == 3)
        {
            mCurrentScore = mCurrentScore - mCurrentRoundScore;
            mFullScore += mCurrentRoundScore;
            mCurrentRoundDartCounter = 0;
            mCurrentRoundScore = 0;
        }
    }

    void runTest()
    {
        int N = 10;
        double _x = 0.0;
        double _y = 0.0;
        printf("%d\n", N);
        fflush(stdout);
        //FILE *fp = fopen("dbg", "w");
        while(N-- > 0) {
            printf("%.6lf %.6lf\n", _x, _y);
            fflush(stdout);
            scanf("%lf%lf%d%d", &mLastDartX, &mLastDartY, &mLastDartMulti, &mLastDartScore);
            //fprintf(fp, "Exec[%d-%d-%d]: (%.6lf %.6lf) -> (%.6lf, %.6lf) sc %d*%d\n", mCurrentRoundDartCounter, mCurrentDartCounter, mDartCounter, _x, _y, mLastDartX, mLastDartY, mLastDartMulti, mLastDartScore);
        }
        while(mDartCounter < mTotalDarts) {
            ThrowDart(_x, _y);
            //fprintf(fp, "Test[%d-%d-%d]: (%.6lf %.6lf) -> (%.6lf, %.6lf) sc %d*%d\n", mCurrentRoundDartCounter, mCurrentDartCounter, mDartCounter, _x, _y, mLastDartX, mLastDartY, mLastDartMulti, mLastDartScore);
        }
        //fclose(fp);
    }

    private:
    int mLastDartScore;
    int mLastDartMulti;
    double mLastDartX;
    double mLastDartY;
    int mCurrentScore;
    int mCurrentDartCounter;
    int mFullScore;
    int mDartCounter;
    int mCurrentRoundScore;
    int mCurrentRoundDartCounter;
    const static int mMaxTraining = 0x186a0;
    const static double mMaxRadius = 1.2;
    public:
    const static int mTotalDarts = 0x1869f;
};

int main()
{
    Darts dart;
    dart.runTest();
    return 0;
}
