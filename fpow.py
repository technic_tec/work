import sys
def fpow(n,k,p):
    r = 1
    while k > 0:
        if k & 1:
            r *= n
            while r >= p:
                r /= 10
        n *= n
        while n >= p:
            n /= 10
        k >>= 1
    return r

k, p, po = int(sys.argv[1]), int(sys.argv[2]), 9
r = fpow(2, k, 10**p)
print r/10**(p-9)
