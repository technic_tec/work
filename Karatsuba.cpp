/*
 * =====================================================================================
 *
 *       Filename:  Karatsuba.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年08月18日 13时25分03秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <valarray>
using namespace std;

#define P 1000000007

// Karatsuba's algorithm for fast multiplication. O(n^{log_2 3})
// (a*x^m+b)*(c*x^m+d) = z1*x^(2m) + z2*x^m + z3
// z1 = ac
// z3 = bd
// z4 = ad+bc = (a+b)(c+d)-z1-z3
valarray<int> Karatsuba(valarray<int> &a, valarray<int> &b)
{
  valarray<int> res;
  if(a.size() == 1) {
    res.resize(1);
    res[0] = a[0]*b[0];
  } else {
    int n = a.size();
    valarray<int> a1 = a[slice(0, n/2, 1)];
    valarray<int> a2 = a[slice(n/2, n, 1)];
    valarray<int> b1 = b[slice(0, n/2, 1)];
    valarray<int> b2 = b[slice(n/2, n, 1)];
    valarray<int> z1 = Karatsuba(a1, b1);
    valarray<int> z3 = Karatsuba(a2, b2);
    valarray<int> as = a1+a2;
    valarray<int> bs = b1+b2;
    valarray<int> z2 = Karatsuba(as, bs) - z1 - z3;
  }
}
int main()
{
  return 0;
}
