import Control.Monad
import Control.Monad.ST
import Data.Array
import Data.Array.ST

fac :: Int -> Array Int Int
fac n = runSTArray $ do
    arr <- newArray (1, n) 0
    forM_ [2..n] $ \i -> do
        f <- readArray arr i
        when (f == 0) $ do
            writeArray arr i i
            forM_ [i*i, i*i+i..n] $ \j -> do
                f <- readArray arr j;
                when (f == 0) $ writeArray arr j i
    return arr

factor :: Array Int Int->Int->[(Integer, Int)]
factor facs 1 = []
factor facs n = ((fromIntegral f, c):factor facs n')
  where f = facs!n
        (c, n') = count_drop_fac n f
        count_drop_fac n f | facs!n /= f = (0, n)
                           | otherwise = (c'+1, n')
           where (c', n') = count_drop_fac (div n f) f

merge_fac :: [(Integer, Int)] -> [(Integer, Int)] -> [(Integer, Int)]
merge_fac [] fb = fb
merge_fac fa [] = fa
merge_fac ((fx, cx):fa) ((fy, cy):fb) | fx < fy = (fx, cx) : merge_fac fa ((fy, cy):fb)
                                      | fx > fy = (fy, cy) : merge_fac ((fx, cx):fa) fb
                                      | otherwise = (fx, cx+cy) : merge_fac fa fb

iP = 1000000007
gcd_fac :: [(Integer, Int)] -> [(Integer, Int)] -> Integer
gcd_fac [] fb = 1
gcd_fac fa [] = 1
gcd_fac ((fx, cx):fa) ((fy, cy):fb) | fx < fy = gcd_fac fa ((fy, cy):fb)
                                    | fx > fy = gcd_fac ((fx, cx):fa) fb
                                    | otherwise = (fx^(min cx cy))*(gcd_fac fa fb) `mod` iP

main = do
  let facs = fac 10000
  s <- getLine
  s <- getLine
  let va = map read $ words s
  s <- getLine
  s <- getLine
  let vb = map read $ words s
  let x = foldl1 merge_fac $ map (factor facs) va
  let y = foldl1 merge_fac $ map (factor facs) vb
  putStrLn $ show $ gcd_fac x y
