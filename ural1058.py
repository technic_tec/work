def distance(p, q):
  x1, y1 = p
  x2, y2 = q
  return (x1-x2)**2+(y1-y2)**2

def fracdiv(s, t, frac):
  xs, ys = s
  xt, yt = t
  return (xs + (xt-xs)*frac, ys + (yt-ys)*frac)

def area(pts):
  lx, ly = pts[-1]
  s = 0.0
  for x, y in pts:
    s += lx*y-ly*x
    lx, ly = x, y
  return s/2.0

def equal_break(pts):
  s0 = area(pts)/2.0
  i, j, s = 0, 1, 0.0
  while j+1 < len(pts):
    sd = area([pts[i], pts[j], pts[j+1]])
    if s + sd <= s0:
      s += sd
      j += 1
    else:
      break
  pts.append(pts[0])

  s1, t1 = pts[i], fracdiv(pts[j], pts[j+1], (s0-s)/sd)
  lm = distance(s1, t1)
  sd2 = area(pts[i], pts[i+1], pts[j+1])
  while j < len(pts)-1:
    if s + sd - sd2 <= s0:
      s2, t2 = fracdiv(pts[i], pts[i+1], (s+sd-s0)/sd2), pts[j+1]
      j += 1
    else:
      s2, t2 = pts[i+1], fracdiv(pts[j], pts[j+1], 

n = int(raw_input())
pts = []
for _ in range(n):
  pts.append(map(float, raw_input().split()))
print '%.4f' % equal_break(pts)
