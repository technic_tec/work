from fractions import gcd

N = 31623
P = 169
isp = [True] * (N+1)
isp[0] = isp[1] = False
prm = []
for i in range(2, N+1):
  if isp[i]:
    prm.append(i)
    isp[i*i::i] = [False]*(N/i-i+1)
print prm
print len(list(x for x in prm if x < 1000))
for k in range(P):
  for n in range(N):
    for i in range(k):
