import Data.Array
import Data.Ratio
--
--data Fraction = Fraction Integer Integer
--Fraction x y = Fraction (div x d) (div y d)
--  where d = (gcd (abs x) (abs y)) * (signum y)
--
--instance Num Fraction where
--    (+) (Fraction xn xd) (Fraction yn yd) = Fraction (xn*yd+xd*yn) (xd*yd)
--    (-) (Fraction xn xd) (Fraction yn yd) = Fraction (xn*yd-xd*yn) (xd*yd)
--    (*) (Fraction xn xd) (Fraction yn yd) = Fraction (xn*yn) (xd*yd)
--    negate (Fraction num denom) = Fraction (negate num) denom
--    abs (Fraction num denom) = Fraction (abs num) (abs denom)
--    signum (Fraction num denom) = fromInteger ((signum num) * (signum denom))
--    fromInteger x = Fraction x 1
--
--instance Fractional Fraction where
--    recip (Fraction xn xd) = Fraction xd xn
--    fromRational x = Fraction (numerator x) (denominator x)
--
--instance Show Fraction where
--    show (Fraction num denom) = (show num) ++ "/" ++ (show denom)

p :: Integer->(Integer->Rational)
p nn kk = arr!(nn, kk)
    where arr = array ((1, 0), (nn, kk)) ([((1, 0), 0), ((1, 1), 1)] ++ [((1, k), 0) | k<-[2..kk]] ++ [((2, 0), 0), ((2, 1), 1)] ++ [((2, k), 0) | k<-[2..kk]] ++ [((n, 0), 0) | n<-[3..nn]] ++ [((n, k), (arr!(n-1, k))*(1-(k%(n-1))) + (arr!(n-1, k-1))*((k-1)%(n-1))) | n<-[3..nn], k<-[1..kk]])

main :: IO()
main = do
  putStrLn (show (p 2013 1000))
