/*
 * =====================================================================================
 *
 *       Filename:  SEAGM.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年05月05日 10时19分03秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 200

int n, a[N];

struct State {
    int d;
    int ntrans;
    int tr[N];
} st[N];
int m = 0, tid[N], wt[N][N];

int gcd(int x, int y)
{
    if(x < y) {
        int t = x; x = y; y = t;
    }
    while(y) {
        int t = x; x = y; y = t % y;
    }
    return x;
}
void init_trans(int x)
{
    int d = st[x].d;
    for(int i = 0; i < n; i++) {
        int d1 = gcd(d, a[i]);
        if(tid[d1] < 0) {
            st[m].d = d1;
            st[m].ntrans = 0;
            tid[d1] = m++;
            init_trans(tid[d1]);
        }
        int y = tid[d1];
        if(wt[x][y] == 0)
            st[x].tr[st[x].ntrans++] = y;
        ++wt[x][y];
    }
}

bool opt[N][N];
double prob[N][N];

bool try_play(int x, int k)
{
    bool res = false;
    if(prob[x][k] >= -1.0)
        return opt[x][k];
    prob[x][k] = 0.0;
    for(int i = 0; i < st[x].ntrans; i++) {
        int y = st[x].tr[i];
        if(wt[x][y] > (x==y?k:0))
            if(!try_play(y, k+1))
                res = true;
        prob[x][k] += (double)(wt[x][y] - (x==y?k:0)) / (double)(n-k) * (1.0-prob[y][k+1]);
    }
    opt[x][k] = res;
    return res;
}

void play(int n, int a[])
{
    memset(tid, -1, sizeof(tid));
    memset(wt, 0, sizeof(wt));
    st[0].d = 0;
    st[0].ntrans = 0;
    tid[0] = 0;
    m = 1;
    init_trans(0);
#if 0
    for(int i = 0; i < m; i++) {
        for(int j = 0; j < m; j++)
            printf("%d ", wt[i][j]);
        printf(":%d:", st[i].d);
        for(int j = 0; j < st[i].ntrans; j++)
            printf(" %d", st[i].tr[j]);
        printf("\n");
    }
#endif
    for(int i = 0; i < m; i++)
        for(int j = 0; j <= n; j++)
            prob[i][j] = -2.0;
    if(tid[1] >= 0)
        for(int j = 0, i1 = tid[1]; j <= n; j++) {
            prob[i1][j] = 1.0;
            opt[i1][j] = true;
        }
    for(int i = 0; i < m; i++)
        if(st[i].d > 1) {
            prob[i][n] = 0.0;
            opt[i][n] = false;
        }
    bool res = try_play(0, 0);
    printf("%d %.4lf\n", (res?1:0), prob[0][0]);
#if 0
    for(int i = 0; i < m; i++) {
        printf("%2d: ", st[i].d);
        for(int j = 0; j <= n; j++)
            printf("%2d(%.4lf)%c", opt[i][j], prob[i][j], (j<n?' ':'\n'));
    }
#endif
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &n);
        for(int i = 0; i < n; i++)
            scanf("%d", &a[i]);
        play(n, a);
    }
    return 0;
}

