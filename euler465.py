#S = c1+c2+...+cn
#S2 = c1^2+c2^2+...+cn^2
#T = (c1+1)*...*(cn+1)
#f(N) = T^2-1 - 2*S*T + S2
#
#S(m) = sum(phi(k), k, 1, m)
#C(m) = 4*(sum(phi(k), k, 1, (N/m)) - sum(phi(k), k, 1, N/(m+1)))
#     = 4*(S(N/m)-S(N/(m+1)))
#S = sum(m*C(m), m, 1, N) = 2*N*(N+1)
#S2 = sum(m^2*C(m), m, 1, N) = 4*sum([N/k]^2*phi(k), k, 1, N)
#T = prod((m+1)^C(m), m, 1, N)
# S(N) = sum(mu(k)*[N/k]^2, k, 1, N)
# S(N/m) = sum(mu(k)*[N/(km)]^2, k<-[1, 2, .., N/m]))
#        = sum(mu(x/m)*[N/x]^2, x<-[m, 2*m, ..., N/m*m])
# S = sum(4*(m*S(N/m)-m*S(N/(m+1)))
#   = sum(4*(m*S(N/m)-(m+1)*S(N/(m+1)) + S(N/(m+1))))
#   = 4*sum(S(N/m), m, 1, N))
def euler465(N):
    S = 2*N*(N+1)
    S2 = 
    res = 0
    print '%d: %d (%d)' % (N, res, res%(10**9+7))

euler465(1)
euler465(2)
euler465(3)
euler465(343)
euler465(7**13)
