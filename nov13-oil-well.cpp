#include <cmath>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
#define N 3000
#define INF 99999999
struct Point {
    int x,y,d;
    bool operator< (const Point &x) const {
        return d < x.d;
    }
} p[N];
int n;

int total_distance(int l, int r, int b, int t)
{
    int res = 0;
    for(int i = 0; i < n; i++) {
        if(p[i].d)
            res += max(max(abs(p[i].x-l),abs(p[i].x-r)),max(abs(p[i].y-b),abs(p[i].y-t)));
    }
    return res;
}
int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int rr,cc;
    scanf("%d%d",&rr,&cc);
    n = 0;
    for(int i = 0; i < rr; i++) {
        for(int j = 0; j < cc; j++) {
            int x;
            scanf("%d",&x);
            if(x) {
                p[n].x = i; p[n].y = j; 
                p[n++].d = 0;
            }
        }
    }
    for(int i = 0; i < n; i++) {
        for(int j = i+1; j < n; j++) {
            int dx = max(abs(p[i].x-p[j].x),abs(p[i].y-p[j].y));
            p[i].d += dx; p[j].d += dx;
        }
    }
    sort(p, p+n);
    int l,r,t,b;
    int v = 0, res = 0;
    l = r = p[v].x; t = b = p[v].y;
    for(int i = 0; i < n; i++)
        p[i].d = max(abs(p[i].x - p[v].x),abs(p[i].y-p[v].y));
    for(int step = 1; step < n; step++) {
        int xl = -1, xr = -1, xt = -1, xb = -1;
        for(int i = 0; i < n; i++) {
            if(!p[i].d)
                continue;
            if(p[i].x <= l && (xl < 0 || p[i].x > p[xl].x))
                xl = i;
            if(p[i].x >= r && (xr < 0 || p[i].x < p[xr].x))
                xr = i;
            if(p[i].y <= b && (xb < 0 || p[i].y > p[xb].y))
                xb = i;
            if(p[i].y >= t && (xt < 0 || p[i].y < p[xt].y))
                xt = i;
        }
        int cand[] = {xl,xr,xt,xb}, cm = -1, dm = INF;
        for(int k = 0; k < 4; k++) {
            if(cand[k] >= 0) {
                int dk = total_distance(min(l,p[cand[k]].x), max(r,p[cand[k]].x), min(b,p[cand[k]].y), max(t,p[cand[k]].y));
                if(dk < dm) {
                    dm = dk; cm = cand[k];
                }
            }
        }
        v = cm;
        l = min(l,p[v].x);
        r = max(r,p[v].x);
        b = min(b,p[v].y);
        t = max(t,p[v].y);
        res += max(max(abs(p[v].x-l),abs(p[v].x-r)),max(abs(p[v].y-b),abs(p[v].y-t)));
        p[v].d = 0;
    }
    printf("%d\n", res);
    return 0;
}
