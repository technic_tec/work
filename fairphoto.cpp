#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>
#include <map>
using namespace std;

#define N 100000
#define M 256 

int n, m, k;
struct Cow {
    int x;
    int br;
    bool operator<(const Cow &o) const {
        return x < o.x;
    }
} c[N];
int dc[M];

struct Diff {
    int sz;
    int d[8];
    Diff(int _sz) : sz(_sz) {
        fill(d, d+sz, 0);
    }
    bool operator< (const Diff &o) const {
        for(int i = 0; i < sz; i++)
            if(d[i] != o.d[i])
                return d[i] < o.d[i];
        return false;
    }
};

int main()
{
    freopen("fairphoto.in", "r", stdin);
    freopen("fairphoto.out", "w", stdout);
    scanf("%d%d", &n, &k);
    dc[0] = 0;
    for(int i = 1; i < M; i++)
        dc[i] = dc[i&(i-1)] + 1;
    m = 0;
    for(int i = 0; i < n; i++) {
        scanf("%d", &c[i].x, &c[i].br);
        if(c[i].br > m)
            m = c[i].br;
        --c[i].br;
    }
    sort(c, c+n);
    int res = 0;
    for(int i = 0; i < (1<<m); i++) {
        if(dc[i] < k) continue;
        map<Diff, int> mp;
        mp.clear();
        Diff cc(m);
        mp[cc] = -1;
        for(int j = 0; j < n; j++) {
            if((1<<c[j].br) & i) {
                if(c[j].br == 0)
                    for(int u = 0; u < m; u++)
                        cc.d[u]++;
                cc.d[c[j].br]--;
                map<Diff, int>::iterator it = mp.find(cc);
                if(it != mp.end()) {
                    int v = mp[cc];
                    if(j - v > res)
                        res = j-v;
                }
                mp[cc] = j;
            } else {
                mp.clear();
                cc = m;
                mp[cc] = j;
            }
        }
    }
    printf("%d\n", (res==0?-1:res));
    return 0;
}

