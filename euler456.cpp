/*
 * =====================================================================================
 *
 *       Filename:  euler456.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年03月25日 17时51分29秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <algorithm>

#define N 100000
#define P 32323
#define Q 30103

struct Point {
    int x, y;
    inline bool operator<(const Point &o) const {
        if(y > 0 || (y == 0 && x >= 0)) {
            if(o.y > 0 || (o.y == 0 && o.x >= 0)) {
                return (x*o.y - y*o.x > 0);
            } else {
                return true;
            }
        } else {
            if(o.y > 0 || (o.y == 0 && o.x >= 0)) {
                return false;
            } else {
                return (x*o.y - y*o.x > 0);
            }
        }
    }
    inline bool operator==(const Point &o) const {
        return (x*o.y - y*o.x == 0 && x*o.x + y*o.y > 0);
    }
    inline int operator*(const Point &o) const {
        return x*o.y - y*o.x;
    }
    inline int operator%(const Point &o) const {
        return x*o.x + y*o.y;
    }
};

void gen(int n, Point pts[])
{
    int xt = 1248, yt = 8421;
    for(int i = 0; i < n; i++) {
        pts[i].x = xt - P/2;
        pts[i].y = yt - Q/2;
        assert(pts[i].x || pts[i].y);
        xt *= 1248; xt %= P;
        yt *= 8421; yt %= Q;
    }
    std::sort(pts, pts+n);
}

#if 0
/*
 * c(x, y) = |{z|x+180 < z < y+180,  z in degs}|
 *         = |{z|0 <= z < y+180,  z in degs}| - |{z|0 <= z <= x+180,  z in degs}|
 * f(x) = sum(|{z|0 <= z < y+180,  z in degs}|, x < y < x+180, y in degs) - sum(|{z|0 <= z <= x+180,  z in degs}|, x < y < x+180, y in degs)
 *      = sum(|{z|0 <= z < y+180,  z in degs}|, 0 <= y < x+180, y in degs)
 *        - sum(|{z|0 <= z < y+180,  z in degs}|, 0 <= y <= x, y in degs)
 *        - |{z|0 <= z <= x+180,  z in degs}| * |{y | 0 <= y < x+180, y in degs}|
 *        + |{z|0 <= z <= x+180,  z in degs}| * |{y | 0 <= y <= x, y in degs}|
 * sf = sum(f(x), x in degs)
 */
long long f(int n)
{
    long long cnt = 0, cnt1;
    Point *pts = new Point[2*n];
    gen(n, pts);
    memcpy(pts+n, pts, n*sizeof(Point));
    c1 = new int[n]; // |{z | 0 <= z < x+180,  z in degs}|
    c2 = new int[n]; // |{z | 0 <= z <= x+180,  z in degs}|
    c3 = new int[n]; // |{y | 0 <= z <= x, x in degs}|
    s1 = new int[n]; // sum(|{z|0 <= z < y+180,  z in degs}|, 0 <= y < x+180, y in degs)
    s2 = new int[n]; // sum(|{z|0 <= z < y+180,  z in degs}|, 0 <= y <= x, y in degs)
    int si = 0, ti = 0, sj = 0, tj = 0;
    while(si < n && ti < n) {
        for(ti = si; ti+1 < n && pts[ti+1] == pts[ti]; ++ti);
        while(sj < n && (pts[si] * pts[sj] > 0 || (pts[si] * pts[sj] == 0 && pts[si] % pts[sj] > 0)))
            ++sj;
        for(tj = sj; tj+1 < n && pts[tj+1] == pts[tj]; ++tj);
        if(sj < n && pts[si]*pts[sj] == 0)
            cnt += (long long)(ti-si+1)*(long long)(tj-sj+1)*(long long)(n-(ti-si+1)-(tj-sj+1));
        si = ti+1; sj = tj+1;
    }

    // count origin inside triangle
    for(int i = 0; i < n; i++)
        cnt += (long long)(s1[i] - s2[i]) - (long long)c2[i] * (long long)(c1[i] - c3[i]);
    assert(cnt % 3 == 0);
    cnt /= 3;
    cnt1 = cnt;

    // count origin on edge
    si = ti = sj = tj = 0;
    while(si < n && ti < n) {
        for(ti = si; ti+1 < n && pts[ti+1] == pts[ti]; ++ti);
        while(sj < n && (pts[si] * pts[sj] > 0 || (pts[si] * pts[sj] == 0 && pts[si] % pts[sj] > 0)))
            ++sj;
        for(tj = sj; tj+1 < n && pts[tj+1] == pts[tj]; ++tj);
        if(sj < n && pts[si]*pts[sj] == 0)
            cnt += (long long)(ti-si+1)*(long long)(tj-sj+1)*(long long)(n-(ti-si+1)-(tj-sj+1));
        si = ti+1; sj = tj+1;
    }

    delete[] pts;
    pts = NULL;
    delete[] c1;
    delete[] c2;
    delete[] c3;
    delete[] s1;
    delete[] s2;
    c1 = c2 = c3 = s1 = s2 = NULL;
    printf("%d: %lld %lld\n", cnt1, cnt);
    return cnt;
}
#else
long long f(int n)
{
  long long res = 0;
  Point *pts = new Point[2*n];
  gen(n, pts);

  int s1 = 0;
  int t1 = s1;
  while(t1 < n && (pts[t1].y > 0 || (pts[t1].y == 0 && pts[t1].x >= 0)))
    ++t1;
  int ineg = t1;
  int s2 = s1;
  while(s2 < n && pts[s2] == pts[s1])
    ++s2;
  int t2 = t1;
  while(t2 < n && pts[t2] == pts[t1])
    ++t2;
  while(s1 < ineg || t1 < n) {
    int cond = (s1 >= ineg ? 1 : (t1 >= n ? -1 : pts[s1]*pts[t1]));
    if(cond < 0) {
      res += (long long)(t1-s1)*(long long)(t1-s1-1)*(long long)(t1-s1-2)/6 - (long long)(t1-s2)*(long long)(t1-s2-1)*(long long)(t1-s2-2)/6;
    } else if(cond > 0) {
      res += (long long)(t1-s1)*(long long)(t1-s1-1)*(long long)(t1-s1-2)/6 - (long long)(t1-s2)*(long long)(t1-s2-1)*(long long)(t1-s2-2)/6;
    } else {
    }
  }
  printf("%d: %lld\n", n, res);
  return res;
}
#endif

int main()
{
    f(8);
    f(600);
    f(40000);
    f(2000000);
    return 0;
}
