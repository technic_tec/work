/*
 * =====================================================================================
 *
 *       Filename:  SEAPERM.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年04月14日 10时54分28秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 2010

int n, k, s, v[N];
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d%d", &n, &k, &s);
        for(int i = 0; i < n; i++)
            scanf("%d", &v[i]);
        for(int i = 1; i <= n; i++)
            printf("%d%c", i, i<n?' ':'\n');
    }
    return 0;
}

