-- x,  y ∈ Var (variables)
-- n ∈ Num (numerals/integers)
-- opa ∈ Opa (arithmetic operators)
-- oba ::= + | - | * | /
-- opb ∈ Opb (boolean operators)
-- opb ::= and | or
-- opr ∈ Opr (relational operators)
-- opr ::= > | <
-- a ∈ AExp (arithmatic expressions)
-- a ::= x | n | a1 opa a2 | ( a )
-- b ∈ BExp (boolean expressions)
-- b ::= true | false | b1 opb b2 | a1 opr a2 | ( b )
-- S ∈ Stmt (statements)
-- S ::= x := a | S1 ; S2 | if b then { S1 } else { S2 } | while b do { S }
-- Arithmetic Operators: (*,  /) > (+,  -) > (>,  <)
-- Boolean Operators: and > or

import Control.Monad
import Data.Maybe
import qualified Data.Map as Map
import Text.ParserCombinators.Parsec(Parser, (<|>), try, letter, alphaNum, oneOf, parse)
import qualified Text.ParserCombinators.Parsec.Language as L
import qualified Text.ParserCombinators.Parsec.Token as T
import Text.ParserCombinators.Parsec.Expr

data AExp = Var String | Num Integer | OpAdd AExp AExp | OpSub AExp AExp | OpMul AExp AExp | OpDiv AExp AExp
	deriving (Eq, Show)
data BExp = Boolean Bool | OpAnd BExp BExp | OpOr BExp BExp | OpGreater AExp AExp | OpSmaller AExp AExp
	deriving (Eq, Show)
data Stmt = Assign AExp AExp | If BExp [Stmt] [Stmt] | While BExp [Stmt]
	deriving (Eq, Show)

def = L.emptyDef{ L.identStart = letter
		,  L.identLetter = alphaNum
		,  L.opStart = oneOf "+-*/><:="
		,  L.opLetter = oneOf "+-*/><:="
		,  L.reservedOpNames = ["+", "-", "*", "/", "and", "or", ">", "<", ":="]
		,  L.reservedNames = ["true", "false", "if", "then", "else", "while", "do"]
}
T.TokenParser{ T.parens = parens
		,  T.braces = braces
		,  T.whiteSpace = whiteSpace
		,  T.integer = integer
		,  T.identifier = identifier
		,  T.reservedOp = reservedOp
		,  T.reserved = reserved
		,  T.semiSep1 = semiSep1
} = T.makeTokenParser def

opa = [
 [Infix (reservedOp "*" >> return OpMul) AssocLeft, Infix (reservedOp "/" >> return OpDiv) AssocLeft], 
 [Infix (reservedOp "+" >> return OpAdd) AssocLeft, Infix (reservedOp "-" >> return OpSub) AssocLeft]]
opb = [[Infix (reservedOp "and" >> return OpAnd) AssocLeft, Infix (reservedOp "or" >> return OpOr) AssocLeft]]

parseVar :: Parser AExp
parseVar = liftM Var identifier

parseNum :: Parser AExp
parseNum = liftM Num integer

parseAExp :: Parser AExp
parseAExp = buildExpressionParser opa (parseVar <|> parseNum <|> parens parseAExp)

parseBool :: Parser BExp
parseBool = (reserved "true" >> return (Boolean True)) <|> (reserved "false" >> return (Boolean False))

parseCond :: Parser BExp
parseCond = do
	e1 <- parseAExp
	op <- ((reservedOp ">" >> return OpGreater) <|> (reservedOp "<" >> return OpSmaller))
	e2 <- parseAExp
	return (op e1 e2)

parseBExp :: Parser BExp
parseBExp = buildExpressionParser opb (try parseBool <|> parens parseBExp <|> parseCond)

parseAssignment :: Parser Stmt
parseAssignment = do
	var <- parseVar
	reservedOp ":="
	aexp <- parseAExp
	return (Assign var aexp)

parseIf :: Parser Stmt
parseIf = do
	reserved "if"
	cond <- parseBExp
	reserved "then"
	s1 <- braces parseStmt
	reserved "else"
	s2 <- braces parseStmt
	return (If cond s1 s2)

parseWhile :: Parser Stmt
parseWhile = do
	reserved "while"
	cond <- parseBExp
	reserved "do"
	s <- braces parseStmt
	return (While cond s)

parseStmt :: Parser [Stmt]
parseStmt = whiteSpace >> semiSep1 (try parseIf <|> try parseWhile <|> parseAssignment)

evalExp :: AExp -> Map.Map String Integer -> Integer
evalExp (Var x) vars = fromJust (Map.lookup x vars)
evalExp (Num x) vars = x
evalExp (OpAdd e1 e2) vars = (evalExp e1 vars) + (evalExp e2 vars)
evalExp (OpSub e1 e2) vars = (evalExp e1 vars) - (evalExp e2 vars) 
evalExp (OpMul e1 e2) vars = (evalExp e1 vars) * (evalExp e2 vars) 
evalExp (OpDiv e1 e2) vars = (evalExp e1 vars) `div` (evalExp e2 vars) 

evalCond :: BExp -> Map.Map String Integer -> Bool
evalCond (Boolean x) vars = x
evalCond (OpAnd e1 e2) vars = ((evalCond e1 vars) && (evalCond e2 vars))
evalCond (OpOr e1 e2) vars = ((evalCond e1 vars) || (evalCond e2 vars))
evalCond (OpGreater e1 e2) vars = (evalExp e1 vars) > (evalExp e2 vars)
evalCond (OpSmaller e1 e2) vars = (evalExp e1 vars) < (evalExp e2 vars)

printVars :: [(String, Integer)] -> IO()
printVars [] = return()
printVars ((x, v):cs) = putStrLn(x++" "++(show v)) >> printVars cs

runStmt :: Stmt -> Map.Map String Integer -> Map.Map String Integer
runStmt (Assign (Var x) e) vars = Map.insert x (evalExp e vars) vars
runStmt (If cond s1 s2) vars | evalCond cond vars = runStmts s1 vars
                             | otherwise = runStmts s2 vars
runStmt (While cond s) vars | evalCond cond vars = runStmt (While cond s) (runStmts s vars)
                            | otherwise = vars

runStmts :: [Stmt] -> Map.Map String Integer -> Map.Map String Integer
runStmts [] vars = vars
runStmts (c:cs) vars = runStmts cs vars1
  where vars1 = runStmt c vars

process :: String->IO()
process s = case parse parseStmt "whileparser" s of
	Left err -> print err
	Right stmt -> printVars $ Map.toAscList $ runStmts stmt Map.empty

main = getContents >>= process
