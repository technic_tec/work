t = int(raw_input())

for _ in range(t):
    s = raw_input().strip()
    n = len(s)
    if sorted(s[:n/2]) == sorted(s[(n+1)/2:]):
        print 'YES'
    else:
        print 'NO'
