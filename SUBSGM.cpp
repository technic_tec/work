#include <stdio.h>
#include <stdlib.h>

#define N 100100

int n, q, v[N];
class ST {
    public:
        struct Node {
            int il, ir, l, r, vl, vr, cl, cr, cm;
        };
        ST(int n, int v[])
        {
            m_v = new Node[2*n];
            m_sz = 0;
            init(0, n-1, v);
        }
        ~ST() {
            delete[] m_v;
            m_v = NULL;
        }

        void insert(int x, int y, int v = 0)
        {
            if(m_v[v].il == m_v[v].ir) {
                m_v[v].vl = m_v[v].vr = y;
                return;
            }
            int l = m_v[v].l, r = m_v[v].r;
            if(x <= m_v[l].ir)
                insert(x, y, l);
            else
                insert(x, y, r);
            m_v[v].vl = m_v[l].vl; m_v[v].vr = m_v[r].vr;
            m_v[v].cl = (m_v[l].cl == m_v[l].ir-m_v[l].il+1 && m_v[l].vr == m_v[r].vl ? m_v[l].cl + m_v[r].cl : m_v[l].cl);
            m_v[v].cr = (m_v[r].cr == m_v[r].ir-m_v[r].il+1 && m_v[l].vr == m_v[r].vl ? m_v[r].cr + m_v[l].cr : m_v[r].cr);
            m_v[v].cm = (m_v[l].cm > m_v[r].cm ? m_v[l].cm : m_v[r].cm);
            if(m_v[l].vr == m_v[r].vl && m_v[l].cr + m_v[r].cl > m_v[v].cm)
                m_v[v].cm = m_v[l].cr + m_v[r].cl;
        }
        int query() {
            return m_v[0].cm;
        }
    private:
        int init(int il, int ir, int v[]) {
            int x = m_sz++;
            m_v[x].il = il; m_v[x].ir = ir;
            if(il == ir) {
                m_v[x].l = m_v[x].r = -1;
                m_v[x].vl = m_v[x].vr = v[il];
                m_v[x].cl = m_v[x].cr = m_v[x].cm = 1;
            } else {
                int l = m_v[x].l = init(il, (il+ir)/2, v);
                int r = m_v[x].r = init((il+ir)/2+1, ir, v);
                m_v[x].vl = m_v[l].vl;
                m_v[x].vr = m_v[r].vr;
                m_v[x].cl = (m_v[l].cl == m_v[l].ir-m_v[l].il+1 && m_v[l].vr == m_v[r].vl ? m_v[l].cl + m_v[r].cl : m_v[l].cl);
                m_v[x].cr = (m_v[r].cr == m_v[r].ir-m_v[r].il+1 && m_v[l].vr == m_v[r].vl ? m_v[r].cr + m_v[l].cr : m_v[r].cr);
                m_v[x].cm = (m_v[l].cm > m_v[r].cm ? m_v[l].cm : m_v[r].cm);
                if(m_v[l].vr == m_v[r].vl && m_v[l].cr + m_v[r].cl > m_v[x].cm)
                    m_v[x].cm = m_v[l].cr + m_v[r].cl;
            }
            return x;
        }
        Node *m_v;
        int m_sz;
};

int main()
{
    scanf("%d%d", &n, &q);
    for(int i = 0; i < n; i++) {
        scanf("%d", &v[i]);
        v[i] += (n-i);
    }
    ST st(n,v);
    printf("%d\n", st.query());
    while(q--) {
        int x, y;
        scanf("%d%d", &x, &y);
        --x;
        y += n-x;
        st.insert(x,y);
        printf("%d\n", st.query());
    }
    return 0;
}
