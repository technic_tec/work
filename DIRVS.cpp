#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>
#define N 300

int r, c, h[N][N], sx, sy, tx, ty;
int d[N][N];
const int dx[4][2] = { {1, 0}, {0, 1}, {-1, 0}, {0, -1} };
const double epsilon = 1e-6;

bool visible(int xa, int ya, int xb, int yb)
{
    // (xa, ya, ha) -- (xb, yb, hb)
    // x = xa + (xb-xa)*t
    // y = ya + (yb-ya)*t
    // z = za + (zb-za)*t
    int x, y, nx, ny, dx, dy;
    if(xb > xa) dx = 1;
    else if(xb < xa) dx = -1;
    else dx = 0;
    if(yb > ya) dy = 1;
    else if(yb < ya) dy = -1;
    else dy = 0;

    x = xa; y = ya;
    nx = (dx>0 ? xa + dx : xa);
    ny = (dy>0 ? ya + dy : ya);
    while(1) {
        double tx = (xa==xb ? 2.0 : (nx - xa - 0.5)/(double)(xb-xa));
        double ty = (ya==yb ? 2.0 : (ny - ya - 0.5)/(double)(yb-ya));
        if(tx > 1.0+epsilon && ty > 1.0+epsilon)
            break;
        double z = h[xa][ya]+0.5 + (h[xb][yb]-h[xa][ya])*std::min(tx, ty);
        if(z < h[x][y] - epsilon)
            return false;
        if(tx < ty - epsilon) {
            x += dx; nx += dx;
        } else if(tx > ty + epsilon) {
            y += dy; ny += dy;
        } else {
            x += dx; nx += dx;
            y += dy; ny += dy;
        }
        if(z < h[x][y] - epsilon)
            return false;
    }

    return true;
}

int q[N*N][2], hd, tl;

int bfs()
{
    memset(d, -1, sizeof(d));
    d[sx][sy] = 0;
    q[0][0] = sx; q[0][1] = sy;
    hd = 0; tl = 1;
    while (hd < tl && d[tx][ty] < 0) {
        int x = q[hd][0], y = q[hd++][1];
        for(int i = 0; i < 4; i++)
            if(x+dx[i][0]>=0 && x+dx[i][0] < r && y+dx[i][1]>=0 && y+dx[i][1] < c) {
                int x1 = x+dx[i][0], y1 = y+dx[i][1];
                if(d[x1][y1] < 0 && h[x1][y1]-h[x][y] <= 1 && h[x][y]-h[x1][y1] <= 3 && (visible(x1, y1, sx, sy) || visible(x1, y1, tx, ty))) {
                    d[x1][y1] = d[x][y] + 1;
                    q[tl][0] = x1; q[tl++][1] = y1;
                }
            }
    }
    return d[tx][ty];
}
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d", &r, &c);
        for(int i = 0; i < r; i++)
            for(int j = 0; j < c; j++)
                scanf("%d", &h[i][j]);
        scanf("%d%d%d%d", &sx, &sy, &tx, &ty);
        --sx; --sy; --tx; --ty;
        int res = bfs();
        if(res < 0) {
            printf("Mission impossible!\n");
//            for(int i = 0; i < r; i++)
//                for(int j = 0; j < c; j++)
//                    printf("%2d%c", d[i][j], j<c-1?' ':'\n');
        } else
            printf("The shortest path is %d steps long.\n", res);
    }
    return 0;
}

