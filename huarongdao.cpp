#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 200

const int ds[][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
const int dirchg[4][4] = {
    {-1, 0, 4, 3}, 
    {0, -1, 1, 5}, 
    {4, 1, -1, 2}, 
    {3, 5, 2, -1}
};
int n, m, k, q, b[N][N];

struct Pos {
    int pos;
    int dis;
} dis[N][N][4];
int hp[4*N*N], tail;
int dd[N][N][6];
inline void heap_init() {
    tail = 0;
}

bool heap_pop(int &x, int &y, int &d)
{
    if(tail <= 0)
        return false;
    x = (hp[1] & 0xff);
    y = (hp[1] >> 8) & 0xff;
    d = (hp[1] >> 16) & 3;
    int v = hp[tail--];
    int x1 = (v & 0xff), y1 = (v >> 8) & 0xff, d1 = (v>>16) & 3;
    int dis1 = dis[x1][y1][d1].dis;
    int pa = 1, ch = 2;
    while(ch <= tail) {
        if(ch+1 <= tail && dis[hp[ch+1]&0xff][(hp[ch+1]>>8)&0xff][(hp[ch+1]>>16)&3].dis < dis[hp[ch]&0xff][(hp[ch]>>8)&0xff][(hp[ch]>>16)&3].dis)
            ++ch;
        if(dis[hp[ch]&0xff][(hp[ch]>>8)&0xff][(hp[ch]>>16)&3].dis >= dis1)
            break;
        hp[pa] = hp[ch]; dis[hp[ch]&0xff][(hp[ch]>>8)&0xff][(hp[ch]>>16)&3].pos = pa;
    }
    hp[pa] = v; dis[v&0xff][(v>>8)&0xff][(v>>16)&3].pos = pa;
    return true;
}
void heap_push_update(int x, int y, int d) {
    int t;
    if(dis[x][y][d].pos < 0)
        t = ++tail;
    else
        t = dis[x][y][d].pos;
    int v = dis[x][y][d].dis;
    int pa, ch;
    for(pa = (t>>1), ch = t; pa > 0; ch = pa, pa >>= 1) {
        int x1 = (hp[pa] & 0xff), y1 = ((hp[pa]>>8)&0xff), d1 = (hp[pa] >> 16) & 3;
        if(dis[x1][y1][d1].dis <= v)
            break;
        hp[ch] = hp[pa];
        dis[x1][y1][d1].pos = ch;
    } 
    hp[ch] = (x|(y<<8)|(d<<16)); dis[x][y][d].pos = ch;
}

int distance(int sx, int sy, int tx, int ty, int k);
void distance4(int sx, int sy, int k);

int play(int ex, int ey, int sx, int sy, int tx, int ty)
{
    memset(dis, -1, sizeof(dis));
    heap_init();
    for(int d = 0; d < 4; d++) {
        if(sx+ds[d][0] < 0 || sx+ds[d][0] >= n || sy+ds[d][1] < 0 || sy+ds[d][1] >= m || !b[sx+ds[d][0]][sy+ds[d][1]])
            continue;
        b[sx][sy] = 0;
        int d1 = distance(ex, ey, sx+ds[d][0], sy+ds[d][1], k);
        b[sx][sy] = 1;
        dis[sx][sy][d].dis = d1;
        dis[sx][sy][d].pos = 0;
        dis[sx+ds[d][0]][sy+ds[d][1]][d^2].dis = d1+1;
        heap_push_update(sx+ds[d][0], sy+ds[d][1], d^2);
    }
    int x0, y0, d0, dis0;
    while(heap_pop(x0, y0, d0)) {
        dis0 = dis[x0][y0][d0].dis;
        if(x0 == tx && y0 == ty)
            return dis0;
        for(int d = 0; d < 4; d++) {
            if(d == d0 || x0+ds[d][0] < 0 || x0+ds[d][0] >= n || y0+ds[d][1] < 0 || y0+ds[d][1] >= m || !b[x0+ds[d][0]][y0+ds[d][1]])
                continue;
            int dis1 = dd[x0][y0][dirchg[d][d0]];
            if(dis[x0+ds[d][0]][y0+ds[d][1]][d^2].pos < 0 || dis[x0+ds[d][0]][y0+ds[d][1]][d^2].dis > dis0 + dis1 + 1) {
                dis[x0][y0][d].dis = dis0 + dis1;
                dis[x0][y0][d].pos = 0;
                dis[x0+ds[d][0]][y0+ds[d][1]][d^2].dis = dis0 + dis1+1;
                heap_push_update(x0+ds[d][0], y0+ds[d][1], d^2);
            }
        }
    }
    return -1;
}
int main()
{
    scanf("%d%d%d%d", &n, &m, &k, &q);
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++)
            scanf("%d", &b[i][j]);
    memset(dd, -1, sizeof(dd));
    for(int x = 0; x < n; x++)
        for(int y = 0; y < m; y++) 
            if (b[x][y] == 1) {
                b[x][y] = 0;
                distance4(x, y, k);
                b[x][y] = 1;
            }
    while(q--) {
        int ex, ey, sx, sy, tx, ty;
        scanf("%d%d%d%d%d%d", &ex, &ey, &sx, &sy, &tx, &ty);
        --ex; --ey; --sx; --sy; --tx; --ty;
        printf("%d\n", play(ex, ey, sx, sy, tx, ty));
    }
    return 0;
}

