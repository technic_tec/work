# Enter your code here. Read input from STDIN. Print output to STDOUT
t = int(raw_input())
P = 10**9+7
for _ in range(t):
    a,b = raw_input().split()
    a = int(a) % P
    pa = [1]
    for i in xrange(1,10):
        pa.append(pa[-1]*a%P)
    r = 1
    # a^(kn+b) = (a^k)^n*a^b = (a^n)^k*a^b
    for c in b:
        r = r ** 10 * pa[int(c)] % P
    print r
