/*
 * =====================================================================================
 *
 *       Filename:  COLLECT.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年06月09日 19时32分55秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 131072
#define P 3046201

int n, q, c[N+1], sz;

int bit_init(int _n)
{
    while(_n & (_n-1))
        _n += _n&(-_n);
    sz = _n;
    memset(c, 0, sizeof(c));
}

void bit_inc(int x, int v)
{
    while(x <= sz) {
        c[x] += v;
        x += x&(-x);
    }
}

int bit_query(int x)
{
    int r = 0;
    while(x > 0) {
        r += c[x];
        x &= x-1;
    }
    return r;
}

int inv[P], fac[P], rfac[N];

int prep()
{
    inv[0] = 0; inv[1] = 1;
    fac[0] = fac[1] = rfac[0] = rfac[1] = 1;
    for(int i = 2; i < P; i++) {
        inv[i] = (int)(-(long long)(P/i)*(long long)inv[P%i] % P);
        if(inv[i] < 0) inv[i] += P;
        fac[i] = (int)((long long)fac[i-1] * (long long)i % P);
    }
    for(int i = 2; i < N; i++)
        rfac[i] = (int)((long long)rfac[i-1] * (long long)inv[i] % P);
}
int power_mod(int n, int k)
{
    int r = 1;
    while(k) {
        if(k & 1)
            r = (int)((long long)r * (long long)n % P);
        n = (int)((long long)n * (long long)n % P);
        k >>= 1;
    }
    return r;
}

// f(n, k) = C(k, n%k)*n!/((n/k+1)!^(n%k)*(n/k)!^(k-n%k));
int fair_div(int n, int k)
{
    int v = n / k, x = n % k, y = k - x;
    // f(n, k) = k!/(x!*y!)*n!/((v+1)!^x*v!^y);
    return (int)((long long)fac[k]*(long long)fac[n]%P*(long long)rfac[x]%P*(long long)rfac[y]%P*(long long)power_mod(rfac[v+1], x)%P*(long long)power_mod(rfac[v], y)%P);
}
int main()
{
    prep();
    scanf("%d", &n);
    bit_init(n);
    for(int i = 1; i <= n; i++) {
        int x;
        scanf("%d", &x);
        bit_inc(i, x);
    }
    scanf("%d", &q);
    while(q--) {
        char op[20];
        int x, y;
        scanf("%s%d%d", op, &x, &y);
        if(op[0] == 'c') {
            int v = bit_query(x) - bit_query(x-1);
            bit_inc(x, y-v);
        } else {
            int v = bit_query(y) - bit_query(x-1);
            printf("%d\n", fair_div(v, y-x+1));
        }
    }
    return 0;
}

