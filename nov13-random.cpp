/*
 * =====================================================================================
 *
 *       Filename:  nov13-random.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年11月20日 11时09分05秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 1100

int n, a, b, d[N];
double msw[N][N], mrv[N][N], v[N], vt[N];

void matmul(double v[], double m[N][N], int n)
{
    for(int i = 0; i < N; i++) {
        vt[i] = 0;
        for(int j = 0; j < n; j++)
            vt[i] += v[j] * m[j][i];
    }
    for(int i = 0; i < N; i++)
        v[i] = vt[i];
}

int main()
{
    scanf("%d%d%d", &n, &a, &b);
    for(int i = 0; i < n; i++) {
        scanf("%d", &d[i]);
        v[i] = (double)d[i];
    }
    // mat[i][j] = m/(m+n-1) if i==j, 1/(m+n-1) otherwise
    // ie. diag=m/(m+n-1), other=1/(m+n-1)
    // then (mat^k)[i][j] = (1+(n-1)*((m-1)/(m+n-1))^k)/n if i==j, (1-((m-1)/(m+n-1))^k)/n otherwise
    // ie. diag=(1+(n-1)*((m-1)/(m+n-1))^k)/n, other= (1-((m-1)/(m+n-1))^k)/n
    double tot = n*(n-1)/2.0;
    double x = pow((n-3.0)/(n-1.0), a);
    for(int i = 0; i < n; i++) {
        msw[i][i] = (1.0+(n-1)*x)/n;
        mrv[i][i] = (std::min(i+1, n-i) - 1 + i*(i-1)/2.0 + (n-i-1)*(n-i-2)/2.0)/tot;
        for(int j = i+1; j < n; j++) {
            msw[i][j] = msw[j][i] = (1.0-x)/n;
            mrv[i][j] = mrv[j][i] = std::min(i+1, n-j)/tot;
        }
    }
    while(b--)
        matmul(v, mrv, n);
    matmul(v, msw, n);
    double s = 0;
    for(int i = 0; i < n; i++)
        s += v[i] * (i*(n-1-i) + n-1) / (n*(n-1)/2);
    printf("%.6lf\n", s);
    return 0;
}

