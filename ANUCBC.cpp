#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000
#define M 100

int n, m, v[N], c[M];
int f[M][M];

int count(int m, int c[])
{
    for(int i = 1; i <= m; i++)
        for(int j = 0; j < m; j++) {
            // get remainder j using values from 0...i-1
            f[i][j] = 0;
            for(int k = c[i-1]%m, bin = 1; k < m; k++)
                f[i][j] += (1LL<<[c[i-1]/m])*bin[c[i-1]%m][k] * f[i-1][j-k*(i-1)];
        }
    return 0;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        int q;
        scanf("%d%d", &n, &q);
        for(int i = 0; i < n; i++)
            scanf("%d", &v[i]);
        while(q--) {
            scanf("%d", &m);
            memset(c, 0, sizeof(c));
            for(int i = 0; i < n; i++) {
                int rem = v[i] % m;
                if(rem < 0) rem += m;
                c[rem]++;
            }
            printf("%d\n", count(m, c));
        }
    }
    return 0;
}

