t = int(raw_input())
P = 1000000007
for _ in range(t):
    n = int(raw_input())
    r, x = 1, 2
    while n > 0:
        if n & 1:
            r = r * x % P
        x = x * x % P
        n >>= 1
    r -= 1
    if r < 0:
        r += P
    print r
