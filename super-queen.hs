import Data.Array

super_queen :: Int->IO(Int)
super_queen n = super_queen_place [] 0 uc us ud
  where uc = array (0, (n-1)) [(i, False) | i<-[0..(n-1)]]
        us = array (0, 2*(n-1)) [(i, False) | i<-[0..2*(n-1)]]
        ud = array (-(n-1), (n-1)) [(i, False) | i<-[-(n-1)..(n-1)]]
        super_queen_place pls y uc us ud | length pls == n = return 1
                                         | y >= n = return 0
                                         | uc!y || us!(x+y) || ud!(x-y) || (abs (y1-y))<=2 || (abs (y2-y)) <= 2 = do
                                              cc <- super_queen_place pls (y+1) uc us ud
                                              return cc
                                         | otherwise = do
                                              --putStrLn $ show (y:pls)
                                              cc <- super_queen_place (y:pls) 0 (uc//[(y,  True)]) (us//[(x+y,  True)]) (ud//[(x-y,  True)])
                                              cn <- super_queen_place pls (y+1) uc us ud
                                              return (cc + cn)
          where x = length pls
                y1 = if (x > 0) then (head pls) else -10
                y2 = if (x > 1) then (head $ tail pls) else -10


main :: IO()
main = do
  l <- getLine
  let n = read l
  c <- super_queen n
  putStrLn $ show c
