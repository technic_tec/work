#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>
#include <set>
#include <algorithm>
using namespace std;

#define N 200000

class LinkCutTree {
    struct Node {
        // left, right, parent -- splay tree of preferred path (solid edges)
        // path_pa -- path between preferred paths (dash edges)
        struct {  // subtree data
            int inc;  // subtree increament
            int sz;   // subtree size;
            long long sum;  // subtree sum of node val
        };
        struct {  // node data
            int linc;
            int lsz;
            long long lsum;
        };
        Node *left, *right, *parent, *path_pa;
        Node() : left(NULL), right(NULL), parent(NULL), path_pa(NULL), inc(0), sum(0), sz(1), linc(0), lsum(0), lsz(1) {}
        inline Node* & Left() { return left; }
        inline Node* & Right() { return right; }
        inline void add(int x) {
            linc += x;
            lsum += (long long)x*(long long)lsz;
            sum += (long long)x*(long long)lsz;
        }
        inline void push() {
            if(Left()) {
                Left()->inc += inc;
                Left()->sum += (long long)inc*(long long)Left()->sz;
            }
            if(Right()) {
                Right()->inc += inc;
                Right()->sum += (long long)inc*(long long)Right()->sz;
            }
            linc += inc;
            lsum += (long long)inc*(long long)lsz;
            inc = 0;
        }
        inline void synth() {
            assert(inc == 0);
            sum = lsum; sz = lsz;
            if(Left()) {
                sz += Left()->sz;
                sum += Left()->sum;
            }
            if(Right()) {
                sz += Right()->sz;
                sum += Right()->sum;
            }
        }
    };
    public:
        LinkCutTree(int sz){
            m_poolsz = sz;
            m_nodes = new Node[sz];
            m_sz = 0;
        }
        ~LinkCutTree() {
            if(m_nodes) {
                delete[] m_nodes;
                m_nodes = NULL;
            }
        }
        int getRoot(int x) {
            Node *p = &m_nodes[x];
            access(p);
            while(p->Left())
                p = p->Left();
            splay(p);
            return p-m_nodes;
        }
        void cut(int x) {
            // NULL -> x
            Node *p = &m_nodes[x];
            access(p);
            if(p->Left()) {
                p->push();
                p->Left()->parent = NULL;
                p->Left() = NULL;
                p->synth();
            }
        }
        void join(int x, int y)
        {
            // y -> x, x is root of sub tree
            access(&m_nodes[x]);
            access(&m_nodes[y]);
            assert(m_nodes[x].Left() == NULL);
            assert(m_nodes[y].parent == NULL);
            assert(m_nodes[y].path_pa == NULL);
            m_nodes[x].push();
            m_nodes[x].Left() = &m_nodes[y];
            m_nodes[y].parent = &m_nodes[x];
            m_nodes[x].synth();
        }
        void extend(int p, int v) {
            int x = m_sz++;
            assert(x < m_poolsz);
            m_nodes[x].lsum = m_nodes[x].sum = v;
            if(p >= 0)
                join(x, p);
        }
        void add(int x, int v)
        {
            access(&m_nodes[x]);
            m_nodes[x].add(v);
        }
        long long sum(int x) {
            access(&m_nodes[x]);
            assert(m_nodes[x].inc == 0);
            return m_nodes[x].lsum;
        }
        void printAll() const {
            const Node *p = &m_nodes[0];
            while(p->parent)
                p = p->parent;
            print(p);
            printf("total: %lld\n", p->sum);
        }
        void print(const Node *p) const {
            printf("$%d = {{inc = %d,  sz = %d,  sum = %lld},    {linc = %d,  lsz = %d,  lsum = %lld},   left = %c%d,   right = %c%d,  parent = %c%d,  path_pa = %c%d}\n", 
                    p - m_nodes, 
                    p->inc, p->sz, p->sum, 
                    p->linc, p->lsz, p->lsum, 
                    p->left ? '$' : ' ', p->left ? p->left - m_nodes : 0, 
                    p->right ? '$' : ' ', p->right ? p->right - m_nodes : 0, 
                    p->parent ? '$' : ' ', p->parent ? p->parent - m_nodes : 0, 
                    p->path_pa ? '$' : ' ', p->path_pa ? p->path_pa - m_nodes : 0);
            for(int i = 0; i < m_sz; i++)
                if(m_nodes[i].path_pa == p)
                    print(&m_nodes[i]);
            if(p->left) print(p->left);
            if(p->right) print(p->right);
        }
        long long operator[](int x) const {
            long long s = 0;
            long long inc = 0;
            const Node *p = &m_nodes[x];
            while(p) {
                inc += p->inc;
                if(p->parent)
                    p = p->parent;
                else {
                    p = p->path_pa;
                    if(p)
                        inc += p->linc;
                }
            }
            p = &m_nodes[x];
            s += p->lsum + inc*p->lsz;
            for(int i = 0; i < m_sz; i++)
                if(m_nodes[i].path_pa == p)
                    s -= m_nodes[i].sum + (inc + p->linc)*m_nodes[i].sz;
            return s;
        }
    private:
        inline void toSolid(Node *w, Node *v)
        {
            assert(w->Right() == NULL && v->path_pa == w);
            w->Right() = v;
            v->parent = w;
            v->path_pa = NULL;
            w->lsz -= v->sz;
            v->inc += w->linc;
            v->sum += (long long)w->linc*(long long)v->sz;
            w->lsum -= v->sum;
        }
        inline void toDashed(Node *v, Node *u)
        {
            assert(v->Right() == u && u->parent == v);
            u->path_pa = v;
            u->parent = NULL;
            v->Right() = NULL;
            v->lsz += u->sz;
            v->lsum += u->sum;
            u->inc -= v->linc;
            u->sum -= (long long)v->linc*(long long)u->sz;
        }
        void access(Node *v)
        {
            Node *v0 = v;
            splay(v);
            printAll();
            if(v->Right()) {
                Node *u = v->Right();
                v->push();
                toDashed(v, u);
                v->synth();
            }
            while(v->path_pa != NULL) {
                Node *w = v->path_pa;
                splay(w);
                Node *u = w->Right();
                w->push();
                if(u)
                    toDashed(w, u);
                toSolid(w, v);
                w->synth();
                v = w;
            }
            splay(v0);
        }
        void splay(Node *v) {
            while(v->parent && v->parent->parent) {
                Node *p = v->parent;
                Node *g = p->parent;
                g->push();
                p->push();
                v->push();
                if(p == g->Left()) {
                    if(v == p->Left()) {
                        //    g        v
                        //  p      ->    p
                        //v                g
                        p->Left() = v->Right();
                        if(p->Left())
                            p->Left()->parent = p;
                        g->Left() = p->Right();
                        if(g->Left())
                            g->Left()->parent = g;
                        v->Right() = p;
                        p->Right() = g;
                        v->parent = g->parent;
                        g->parent = p;
                        p->parent = v;
                    } else {
                        //    g          v
                        // p      ->   p   g
                        //  v
                        p->Right() = v->Left();
                        if(p->Right())
                            p->Right()->parent = p;
                        g->Left() = v->Right();
                        if(g->Left())
                            g->Left()->parent = g;
                        v->Left() = p;
                        v->Right() = g;
                        v->parent = g->parent;
                        g->parent = p->parent = v;
                    }
                } else {
                    if(v == p->Left()) {
                        //  g                v
                        //     p      ->   g   p
                        //   v
                        p->Left() = v->Right();
                        if(p->Left())
                            p->Left()->parent = p;
                        g->Right() = v->Left();
                        if(g->Right())
                            g->Right()->parent = g;
                        v->Right() = p;
                        v->Left() = g;
                        v->parent = g->parent;
                        g->parent = p->parent = v;
                    } else {
                        //  g                v
                        //    p      ->    p
                        //      v        g
                        p->Right() = v->Left();
                        if(p->Right())
                            p->Right()->parent = p;
                        g->Right() = p->Left();
                        if(g->Right())
                            g->Right()->parent = g;
                        v->Left() = p;
                        p->Left() = g;
                        v->parent = g->parent;
                        g->parent = p;
                        p->parent = v;
                    }
                }
                g->synth();
                p->synth();
                v->synth();
                if(v->parent)
                    if(v->parent->Right() == g)
                        v->parent->Right() = v;
                    else
                        v->parent->Left() = v;
                else {
                    v->path_pa = g->path_pa;
                    g->path_pa = NULL;
                }
            }
            if(v->parent) {
                Node *p = v->parent;
                p->push();
                v->push();
                if(v == p->Left()) {
                    //   p
                    // v
                    p->Left() = v->Right();
                    if(p->Left())
                        p->Left()->parent = p;
                    v->Right() = p;
                    v->parent = p->parent;
                    p->parent = v;
                } else {
                    //   p
                    //     v
                    p->Right() = v->Left();
                    if(p->Right())
                        p->Right()->parent = p;
                    v->Left() = p;
                    v->parent = p->parent;
                    p->parent = v;
                }
                v->path_pa = p->path_pa;
                p->path_pa = NULL;
                p->synth();
                v->synth();
            }
            assert(v->parent == NULL);
        }

        int m_poolsz;
        int m_sz;
        Node *m_nodes;
};
class LinkCutTreeRef {
    struct Node {
        int val;
        Node *parent, *child, *sib;
    };
    public:
        LinkCutTreeRef(int sz){
            m_poolsz = sz;
            m_nodes = new Node[sz];
            m_nodes[0].val = 0;
            m_nodes[0].parent = m_nodes[0].child = m_nodes[0].sib = NULL;
            m_sz = 0;
            m_nodeset.clear();
        }
        ~LinkCutTreeRef() {
            if(m_nodes) {
                delete[] m_nodes;
                m_nodes = NULL;
            }
            m_nodeset.clear();
        }
        int random_node(bool nonzero = false) {
            int m = m_nodeset.size();
            set<int>::iterator it = m_nodeset.begin();
            if(nonzero && m > 1)
                advance(it, rand()%(m-1)+1);
            else
                advance(it, rand()%m);
            return *it;
        }
        void cut(int x) {
            // NULL -> x
            Node *p = &m_nodes[x];
            if(p->parent) {
                Node *q = p->parent;
                if(q->child == p)
                    q->child = p->sib;
                else {
                    Node *pp = q->child;
                    while(pp->sib != p)
                        pp = pp->sib;
                    pp->sib = p->sib;
                }
            }
            p->parent = p->sib = NULL;
            remove_nodes(p);
        }
        void extend(int p, int v) {
            int x = m_sz++;
            assert(x < m_poolsz);
            if(p >= 0) {
                m_nodes[x].val = v;
                m_nodes[x].parent = &m_nodes[p];
                m_nodes[x].sib = m_nodes[p].child;
                m_nodes[p].child = &m_nodes[x];
                m_nodes[x].child = NULL;
            } else {
                m_nodes[x].val = v;
                m_nodes[x].parent = NULL;
                m_nodes[x].sib = NULL;
                m_nodes[x].child = NULL;
            }
            m_nodeset.insert(x);
        }
        void add(int x, int v)
        {
            m_nodes[x].val += v;
            for(Node *p = m_nodes[x].child; p; p = p->sib)
                add(p-m_nodes, v);
        }
        long long sum(int x) const {
            long long s = m_nodes[x].val;
            for(Node *p = m_nodes[x].child; p; p = p->sib)
                s += sum(p-m_nodes);
            return s;
        }
        void printAll() const {
            printf("total: %lld\n", sum(0));
        }
        int operator[](int x) const {
            return m_nodes[x].val;
        }
        bool valid(int x) const {
            return m_nodeset.count(x) > 0;
        }
        int getSize() const {
            return m_sz;
        }
    private:
        void remove_nodes(Node *p)
        {
            m_nodeset.erase(p-m_nodes);
            for(Node *q = p->child; q; q = q->sib)
                remove_nodes(q);
        }
        int m_poolsz;
        int m_sz;
        Node *m_nodes;
        set<int> m_nodeset;
};

int n, m, v[N], p[N];
void test(int n, int m)
{
    const int nn = 1000;
    srand(time(NULL));
    for(int i = 0; i < n; i++) {
        v[i] = rand() % (2*nn+1)-nn;
        printf("%4d%c", v[i], (i<n-1?' ':'\n'));
    }
    p[0] = -1;
    printf("%4d ", p[0]);
    for(int i = 1; i < n; i++) {
        p[i] = rand() % i;
        printf("%4d%c", p[i], (i<n-1?' ':'\n'));
    }
    LinkCutTree lct(n+m);
    LinkCutTreeRef lctr(n+m);
    for(int i = 0; i < n; i++) {
        lct.extend(p[i], v[i]);
        lctr.extend(p[i], v[i]);
    }
    lct.printAll();
    lctr.printAll();
    for(int i = 0; i < lctr.getSize(); i++)
        if(lctr.valid(i))
            assert(lctr[i] == lct[i]);
    for(int i = 0; i < m; i++) {
        unsigned x = rand();
        if(x < 0x20000000U) {
            int px = lctr.random_node();
            int vx = rand() % (2*nn+1)-nn;
            printf("insert %d %d\n", px, vx);
            lct.extend(px, vx);
            lctr.extend(px, vx);
        } else if(x < 0x40000000U) {
            int px = lctr.random_node();
            int vx = rand() % (2*nn+1)-nn;
            printf("add %d %d\n", px, vx);
            lct.add(px, vx);
            lctr.add(px, vx);
        } else if(x < 0x60000000U) {
            int px = lctr.random_node(true);
            if(px == 0) {
                --i;
                continue;
            }
            printf("cut %d\n", px);
            lct.cut(px);
            lctr.cut(px);
        } else {
            int px = lctr.random_node();
            printf("sum %d\n", px);
            long long s1 = lct.sum(px);
            long long s0 = lctr.sum(px);
            if(s1 != s0) {
                printf("sum(%d): exp %lld got %lld\n", px, s0, s1);
                lct.printAll();
            }
            assert(s1 == s0);
        }
        lct.printAll();
        lctr.printAll();
        for(int i = 0; i < lctr.getSize(); i++)
            if(lctr.valid(i)) {
                if(lct[i] != lctr[i])
                    printf("%d: %lld -- %d\n", i, lct[i], lctr[i]);
                assert(lctr[i] == lct[i]);
            }
    }
}
int main()
{
    test(100, 100);
    test(1000, 1000);
#if 0
    n = m = 10;
    v = { 189, 128, 949, 293, -96,  73, 513, -896, 344, 723 };
    p = {  -1,   0,   1,   0,   3,   3,   2,    4,   2,   5 };
    LinkCutTree lct(n+m);
    LinkCutTreeRef lctr(n+m);
    for(int i = 0; i < n; i++) {
        lct.extend(p[i], v[i]);
        lctr.extend(p[i], v[i]);
    }
    long long s1 = lct.sum(2);
    long long s0 = lctr.sum(2);
    printf("sum(2): exp %lld got %lld\n", s0, s1);
    s1 = lct.sum(3);
    s0 = lctr.sum(3);
    printf("sum(3): exp %lld got %lld\n", s0, s1);
    n = 10; m = 100;
    v = {-992, 569, 551, 438,-947,-225,-616, 394, 536,-582};
    p = {-1,   0,   0,   1,   3,   0,   0,   4,   0,   2};
    LinkCutTree lct(n+m);
    LinkCutTreeRef lctr(n+m);
    for(int i = 0; i < n; i++) {
        lct.extend(p[i], v[i]);
        lctr.extend(p[i], v[i]);
    }
    lct.printAll();
    printf("add 7 -116\n");
    lct.add(7, -116);
    lctr.add(7, -116);
    lct.printAll();
    printf("cut 6\n");
    lct.cut(6);
    lctr.cut(6);
    lct.printAll();
    long long s1 = lct.sum(0);
    long long s0 = lctr.sum(0);
    printf("sum(0): exp %lld got %lld\n", s0, s1);
    lct.printAll();
    LinkCutTree lct(4);
    lct.extend(-1, 96);
    lct.printAll();
    lct.add(0, 331);
    lct.printAll();
    lct.extend(0, 433);
    lct.printAll();
    lct.extend(0, -828);
    lct.printAll();
    printf("%lld %lld %lld\n", lct.sum(1), lct.sum(2), lct.sum(0));
    return 0;
    LinkCutTree lct(4);
    LinkCutTreeRef lctr(4);
    lct.extend(-1, 665);
    lctr.extend(-1, 665);
    lct.extend(0, 489);
    lctr.extend(0, 489);
    lct.extend(0, -411);
    lctr.extend(0, -411);
    lct.printAll();
    lctr.printAll();
    lct.add(0, -433);
    lctr.add(0, -433);
    lct.printAll();
    lctr.printAll();
    lct.cut(2);
    lctr.cut(2);
    lct.printAll();
    lctr.printAll();
    return 0;
#endif
}

