/*
 * =====================================================================================
 *
 *       Filename:  SCRAPER.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2012年02月28日 17时59分11秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (), thang@c2micro.com
 *        Company:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <algorithm>

#define N 200
#define MIN(x,y) ((x)<(y)?(x):(y))
#define MAX(x,y) ((x)>(y)?(x):(y))

int f, n, x[N], y[N], a, b, sa, tb;
bool v[N];
int e[N][N], d[N];

int ext_gcd(int a, int b, long long &x, long long &y)
{
  int d;
  if(b == 0) {
    d = a; x = 1; y = 0;
  } else {
    d = ext_gcd(b, a%b, y, x);
    y -= a/b*x;
  }
  return d;
}

bool connected(int xi, int yi, int xj, int yj, int f)
{
  if(yi == yj) return true;
  long long a, b;
  int d = ext_gcd(xi, xj, a, b); // a*xi+b*xj == d
  if((yj - yi) % d) return false;
  int di = xi / d,  dj = xj / d; // a*xi + b*xj == 1,  n = a*xi*d+yi = b*xj*d+yj,  
  a *= (yj-yi)/d;  // a*xi == (yj-yi)/d (mod xj), n = a*xi*d+yi == yj (mod xj*d)
                   // a == a0 (mod xj) => n == a0*xi*d+yi (mod xj*xi*d)
  b *= (yi-yj)/d;

  long long va = a % dj, vb = b % di;
  if(va < 0) va += dj;
  if(vb < 0) vb += di;
  long long ka = (va-a)/dj, kb = (vb-b)/di;
  a += std::max(ka, kb)*dj;
  b += std::max(ka, kb)*di;
  assert(a*xi+yi == b*xj+yj);
  assert(a >= 0 && b >= 0 && (a<dj || b<di));
  long long n = a*xi+yi;
  return (n < f);
}

void dfs(int x)
{
  v[x] = true;
  for(int i = 0; i < d[x]; i++)
    if(!v[e[x][i]]) dfs(e[x][i]);
}

int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    sa = tb = -1;
    scanf("%d%d%d%d", &f, &n, &a, &b);
    for(int i = 0; i < n; i++) {
      scanf("%d%d", &x[i], &y[i]);
      if(a >= y[i] && (a-y[i])%x[i] == 0)
        sa = i;
      if(b >= y[i] && (b-y[i])%x[i] == 0)
        tb = i;
      d[i] = 0;
      v[i] = false;
    }
    //printf("sa %d tb %d\n", sa, tb);
    if(sa < 0 || tb < 0) {
      printf("The furniture cannot be moved.\n");
      continue;
    }
    for(int i = 0; i < n; i++) {
      for(int j = 0; j < i; j++)
        if(connected(x[i], y[i], x[j], y[j], f)) {
          //printf("%d[%d]=%d\n", i, d[i], j);
          e[i][d[i]++] = j;
          //printf("%d[%d]=%d\n", j, d[j], i);
          e[j][d[j]++] = i;
        }
    }
    dfs(sa);
    if(v[tb]) {
      printf("It is possible to move the furniture.\n");
    } else {
      printf("The furniture cannot be moved.\n");
    }
  }
  return 0;
}


