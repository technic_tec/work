#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000
#define P 1000000007

int power_mod(int n, int k)
{
    int r = 1;
    if(n == 0)
        return 0;
    else if(n == 1 || k == 0)
        return 1;
    while(k) {
        if(k & 1)
            r = (int)((long long)r * (long long)n % P);
        n = (int)((long long)n * (long long)n % P);
        k >>= 1;
    }
    return r;
}
int main()
{
    int n, k, r;
    scanf("%d%d", &n, &k);
    if(k == 1)
        r = power_mod(n, n-2);
    else if(k == 2)
        r = (int)((long long)power_mod(2, 2*n-2)*(long long)power_mod(n, n-2)%P*(long long)power_mod(n-1, n)%P);
    else
        r = (int)((long long)power_mod(3, 3*n-2)*(long long)power_mod(n, n-2)%P*(long long)power_mod(n-1, 2*n)%P);
    printf("%d\n", r);
    return 0;
}

