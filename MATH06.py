def perm(n, p = []):
    if len(p) == n:
        yield p
        return
    k = len(p)
    s = sum(p)
    cs = set(p)
    for i in xrange(1, n+1):
        if i not in cs and 2*(s+i)%(k+1) == 0:
            for x in perm(n, p+[i]):
                yield x
for n in range(1, 10):
    for x in perm(n):
        print x
