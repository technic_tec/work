import random
class Ordinal(object):
    def __init__(self, v, r = 0):
        while (v&1)==0 and r > 0:
            v >>= 1
            r -= 1
        self.v, self.r = v, r

    def __repr__(self):
        return '%d / 2^%d' % (self.v, self.r)

    def __add__(self, o):
        if isinstance(o, int):
            o = Ordinal(o)
        if o.r < self.r:
            return Ordinal(self.v+(o.v<<(self.r-o.r)), self.r)
        else:
            return Ordinal(o.v+(self.v<<(o.r-self.r)), o.r)

    def __eq__(self, o):
        return self.r == o.r and self.v == o.v

    def sign(self):
        if self.v > 0:
            return 1
        elif self.v < 0:
            return -1
        else:
            return 0

    def colon(self, o):
        if o == 1:
            # return (1:x)
            if self.v >= 0:
                return Ordinal(self.v+(1L<<self.r), self.r)
            else:
                k = 1-(self.v >> self.r)
                return Ordinal(self.v+(k<<self.r), self.r+(k-1))
        elif o == -1:
            # return (-1:x)
            if self.v <= 0:
                return Ordinal(self.v-(1L<<self.r), self.r)
            else:
                k = 1-((-self.v) >> self.r)
                return Ordinal(self.v-(k<<self.r), self.r+(k-1))
        else:
            raise ValueError

    @staticmethod
    def stick_test(s):
        r1 = Ordinal(0)
        for x in s:
            if r1.r == 0 and r1.v * x >= 0:
                r1 += x
            else:
                r1 += Ordinal(x, r1.r+1)
        r2 = Ordinal(0)
        for x in s[::-1]:
            r2 = r2.colon(x)
        if not (r1 == r2):
            print str(s) + ': exp ' + str(r1) + ',  got ' + str(r2)
            raise ValueError

def HackenbushTree(n, e):
    ve = [list() for _ in range(n+1)]
    for x in e:
        ve[x[0]].append((x[1], x[2]))
        ve[x[1]].append((x[0], x[2]))
    q = [[1, 0, None, None]]
    res = None
    while len(q):
        x, cx, px, v = q.pop()
        if v is None:
            cur = [x, cx, px, Ordinal(0)]
            q.append(cur)
            for y, cy in ve[x]:
                if px is None or y != px[0]:
                    q.append([y, cy, cur, None])
        else:
            if px is None:
                res = v
            else:
                px[3] += v.colon(cx)
    return res.sign()

def main():
    t = int(raw_input())
    for _ in range(t):
        n = int(raw_input())
        e = []
        for _ in range(n-1):
            u, v, c = map(int, raw_input().split())
            e.append((u, v, 1-2*c))
        res = HackenbushTree(n, e)
        if res > 0:
            print 'Chef Chef'
        elif res < 0:
            print 'Ksen Ksen'
        else:
            print 'Ksen Chef'

if __name__ == '__main__':
    main()
