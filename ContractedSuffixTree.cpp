/*
 * =====================================================================================
 *
 *       Filename:  ContractedSuffixTree.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年04月25日 16时51分26秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define N 100000
#define K 27

class ContractedSuffixTree {
    struct Node {
        int pos, depth;
        Node *pa;
        Node *ch[K];
        Node *reach;
        void init(int _pos, Node *_pa = NULL){
            pos = _pos;
            pa = _pa;
            memset(ch, 0, sizeof(ch));
            reach = NULL;
            if(pa == NULL)
                depth = 0;
            else
                depth = pa->depth + 1;
        }
    };
    public:
        ContractedSuffixTree(char *s) {
            m_str = s;
            m_sz = strlen(s);
            m_nodes = new Node[m_sz+1];

            // insert suffix i in H(T) via pa link, and D(T) via ch link
            m_root = &m_nodes[m_sz];
            m_root->init(m_sz);
            Node *px = m_root;
            for(int i = m_sz-1; i >= 0; i--) {
                if(m_root->ch[s[i]&0x1f] == NULL) {
                    Node *p = &m_nodes[i];
                    p->init(i, m_root);
                    m_root->ch[s[i]&0x1f] = p;
                    px = p;
                } else {
                    Node *px1 = px->pa;
                    while(px1->ch[s[i]&0x1f] == NULL) {
                        px = px1; px1 = px1->pa;
                    }
                    px1 = px1->ch[s[i]&0x1f];
                    Node *p = &m_nodes[i];
                    p->init(i, px1);
                    px->ch[s[i]&0x1f] = p;
                    px = p;
                }
            }
            // install maximal reach pointers in H(T)
            m_root->reach = m_root;
            px = m_root;
            for(int i = m_sz-1; i >= 0; i--) {
                while(px->ch[s[i]&0x1f] == NULL)
                    px = px->pa;
                px = px->ch[s[i]&0x1f];
                m_nodes[i].reach = px;
            }
            // discard D(T), restore ch link for H(T)
            for(int i = m_sz; i >= 0; i--) {
                Node *p = &m_nodes[i];
                memset(p->ch, 0, sizeof(p->ch));
                if(p->pa) {
                    assert(p->pa->pos > p->pos);
                    char b = m_str[p->pos + p->pa->depth];
                    p->pa->ch[b&0x1f] = p;
                }
            }
        }
        ~ContractedSuffixTree() {
            if(m_nodes) {
                delete[] m_nodes;
                m_nodes = NULL;
            }
        }
        void update(int pos, char ch);
        long long countDistinctSubstring();
    private:
        char *m_str;
        int m_sz;
        Node *m_root, *m_nodes;
};

int main()
{
    return 0;
}

