#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;
#define N 100000

int main() {
    int n, k;
    scanf("%d%d", &n, &k);
    vector<int> v;
    v.reserve(n);
    for(int i = 0; i < n; i++) {
        int x;
        scanf("%d", &x);
        v.push_back(x);
    }
    int vm = *max_element(v.begin(),v.end());
    vector<vector<int> > rb(vm+1);
    vector<int> idx(vm+1, n);
    int rbx = n;
    long long cnt = 0;
    for(int i = n-1; i >= 0; i--) {
        for(int j = 0; j < rb[v[i]].size(); j++)
            if(idx[rb[v[i]][j]] < rbx)
                rbx = idx[rb[v[i]][j]];
        cnt += rbx-i;
        if(v[i] > k) {
            if(idx[v[i]] >= n) {
                for(int j = k; j <= vm; j += v[i])
                    rb[j].push_back(v[i]);
            }
            idx[v[i]] = i;
        }
    }
    printf("%lld\n", cnt);
    return 0;
}
