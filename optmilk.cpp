/*
 * =====================================================================================
 *
 *       Filename:  optmilk.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年12月20日 09时59分20秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 200200

class Stat {
    private:
        int sz;
        int *vl, *vr, *vn, *vb;
    public:
        Stat(int n, int v[])
        {
            sz = n;
            vl = new int[2*n];
            vr = new int[2*n];
            vn = new int[2*n];
            vb = new int[2*n];
            int j = sz;
            while(j & (j-1))
                j += (j&(-j));
            for(int k = 0; k < sz; k++) {
                int i = j+k-sz*(k>=2*sz-j);
                vl[i] = vr[i] = vn[i] = 0;
                vb[i] = v[k];
            }
            for(int i = sz-1; i > 0; i--) {
                vb[i] = std::max(std::max(vl[2*i]+vb[2*i+1], vl[2*i]+vr[2*i+1]), vb[2*i]+vr[2*i+1]);
                vl[i] = std::max(std::max(vl[2*i]+vl[2*i+1], vl[2*i]+vn[2*i+1]), vb[2*i]+vn[2*i+1]);
                vr[i] = std::max(std::max(vn[2*i]+vb[2*i+1], vn[2*i]+vr[2*i+1]), vr[2*i]+vr[2*i+1]);
                vn[i] = std::max(std::max(vn[2*i]+vl[2*i+1], vn[2*i]+vn[2*i+1]), vr[2*i]+vn[2*i+1]);
            }
        }
        ~Stat()
        {
            if(vl) delete[] vl;
            if(vr) delete[] vr;
            if(vn) delete[] vn;
            if(vb) delete[] vb;
            vl = vr = vn = vb = NULL;
        }
        void update(int k, int x)
        {
            int j = sz;
            while(j & (j-1))
                j += (j&(-j));
            --k;
            int i = j+k-sz*(k>=2*sz-j);
            vb[i] = x;
            i >>= 1;
            while(i > 0) {
                vb[i] = std::max(std::max(vl[2*i]+vb[2*i+1], vl[2*i]+vr[2*i+1]), vb[2*i]+vr[2*i+1]);
                vl[i] = std::max(std::max(vl[2*i]+vl[2*i+1], vl[2*i]+vn[2*i+1]), vb[2*i]+vn[2*i+1]);
                vr[i] = std::max(std::max(vn[2*i]+vb[2*i+1], vn[2*i]+vr[2*i+1]), vr[2*i]+vr[2*i+1]);
                vn[i] = std::max(std::max(vn[2*i]+vl[2*i+1], vn[2*i]+vn[2*i+1]), vr[2*i]+vn[2*i+1]);
                i >>= 1;
            }
        }
        int query()
        {
            return std::max(std::max(vl[1], vr[1]), std::max(vn[1], vb[1]));
        }
};

int n, m, v[N];
int main()
{
    freopen("optmilk.in", "r", stdin);
    freopen("optmilk.out", "w", stdout);
    scanf("%d%d", &n, &m);
    for(int i = 0; i < n; i++)
        scanf("%d", &v[i]);
    Stat st(n, v);
    long long res = 0;
    for(int i = 0; i < m; i++) {
        int k, x;
        scanf("%d%d", &k, &x);
        st.update(k, x);
        res += (long long)st.query();
    }
    printf("%lld\n", res);
    return 0;
}

