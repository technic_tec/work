import random
from pprint import pprint

db = dict()
def play(s, o):
    s = tuple(sorted(s, key=lambda x: (len(x), zip(map(len, x), x))))
    if s + (o, ) not in db:
        op = chr(ord(o)^1)
        res = False
        for k in range(len(s)):
            sx = s[k]
            for i in range(len(sx)):
                if s[k][i] == o:
                    sy = sx[:i]
                    if not play(s[:k]+(sy,)+s[k+1:], op):
                        res = True
        db[s+(o,)] = res
    return db[s+(o,)]

#for i in range(16):
#    for j in range(16,32):
#       sa = bin(i)[2:].zfill(5)
#       sb = bin(j)[2:].zfill(5)
#       play((sa, sb), '0')
#       play((sa, sb), '1')
for i in range(16):
    sa = bin(i)[2:].zfill(5)
    for j in range(16,32):
        sb = bin(j)[2:].zfill(5)
        for k in range(j,32):
            sc = bin(k)[2:].zfill(5)
            play((sa, sb, sc), '0')
            play((sa, sb, sc), '1')
for i in range(8):
    sa = bin(i)[2:].zfill(4)
    for j in range(i,8):
        sb = bin(j)[2:].zfill(4)
        for k in range(j,16):
            sc = bin(k)[2:].zfill(4)
            for l in range(max(k,8),16):
                sd = bin(l)[2:].zfill(4)
                play((sa, sb, sc, sd), '0')
                play((sa, sb, sc, sd), '1')
        
#bits = 16
#for _ in range(10):
#    i = random.randint(2**bits, 2**(bits+1)-1)
#    sa = bin(i)[2:].zfill(bits)
#    j = random.randint(0, 2**bits-1)
#    sb = bin(j)[2:].zfill(bits)
#    play(sa, sb, '0')
#    play(sa, sb, '1')

for w in sorted(db.keys(), key=lambda x: (len(x), zip(map(len, x), x))):
    if w[-1] == '1' and w[:-1]+('0', ) in db:
        if db[w[:-1]+('0', )]:
            if len(w) == 3:
                a, b, o = w
                i = 0
                while i < len(a) and i < len(b) and a[i] != b[i]:
                    i += 1
                assert((a[i:]+b[i:])[0] == '0')
            print w[:-1], 'EVEN'
        elif db[w[:-1]+('1', )]:
            if len(w) == 3:
                a, b, o = w
                i = 0
                while i < len(a) and i < len(b) and a[i] != b[i]:
                    i += 1
                assert((a[i:]+b[i:])[0] == '1')
            print w[:-1], 'ODD'
        else:
            if len(w) == 3:
                a, b, o = w
                i = 0
                while i < len(a) and i < len(b) and a[i] != b[i]:
                    i += 1
                assert((a[i:]+b[i:]) == '')
            print w[:-1], 'DONT PLAY'
