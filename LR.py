class Symbol(object):
  def __init__(self, c):
    self.id = c
    self.terminal = not c.isupper()


class Rule(object):
  def __init__(self, l, r):
    assert isinstance(l, Symbol)
    assert isinstance(r, list)
    assert not l.terminal
    self.l, self.r = l, r

  @classmethod
  def parse(cls, s):
    i = s.index('->')
    l, r = s[:i].strip(), s[i+2:].strip()
    assert len(l)==1
    assert r.count('|') == 0
    return cls(Symbol(l), map(Symbol, r))


class Item(object):
  def __init__(self, rule, pos = 0):
    assert(pos >= 0 and pos <= len(rule.r))
    self.rule, self.pos = rule, pos

  def nextSym(self):
    return (self.rule.r[self.pos] if self.pos < len(self.rule.r) else '')

  def advance(self):
    assert(self.pos < len(self.rule.r))
    return Item(self.rule, self.pos+1)


class ItemSet(object):
  def __init__(self, items):
    self.items = items

  def advance(self, sym):
    return [x.advance() for x in self.items if x.nextSym() == sym]

  def nextSym(self):
    return [x.nextSym() for x in self.items if x.nextSym() != '']


class LR(object):
  def __init__(self, rules = [], start_sym = Symbol('S')):
    self.rules = rules
    self.start_sym = start_sym
    self.closure = dict()

  def add_rule(self, rule):
    self.rules.append(rule)

  def start_from(self, start_sym):
    self.start_sym = start_sym

  def closure(self, sym):
    if sym not in self.closure:
      self.closure[sym] = set()
      for x in self.gen.get(sym, set()):
        self.closure[sym] |= k
  def parse_table(self):
    raise NotImplementedError


class LR0(LR):
  def __init__(self, rules = [], start_sym = Symbol('S')):
    LR.__init__(self, rules, start_sym)

  def parse_table(self):
    gen = dict()
    for r in self.rules:
      gen.setdefault(r.l, set()).add(r)
    rulesets = set()
