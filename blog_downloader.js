// ==UserScript==
// @name         163 Blog Downloader
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Download 163 blog
// @author       tec
// @require      http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js
// @match        http://blog.163.com/*/blog/*
// @match        http://*.blog.163.com/*
// @grant        unsafeWindow
// ==/UserScript==
/* jshint -W097 */
'use strict';

var per_page = 50;
eval("o="+/UD\.host *= *({[^{}]*})/.exec(document.body.innerHTML)[1]);
console.log(o);
var uid = o.userId;
var baseUrl = o.baseUrl;
var userName = o.userName;
var getBlogs = function(user_id, userName, start_pg, per_page) {
    var url = "http://api.blog.163.com/"+userName+"/dwr/call/plaincall/BlogBeanNew.getPosts.dwr"
    var data="callCount=1\n\
scriptSessionId=\${scriptSessionId}187\n\
c0-scriptName=BlogBeanNew\n\
c0-methodName=getBlogs\n\
c0-id=0\n\
c0-param0=number:"+user_id+"\n\
c0-param1=number:"+start_pg+"\n\
c0-param2=number:"+per_page+"\n\
batchId=1";
    var deferred = $.Deferred();
    var req=unsafeWindow.J.cqt('http://api.blog.163.com');
    req.open('POST',url,true);
    req.setRequestHeader('Content-Type', 'text/plain');
    req.onreadystatechange = function(e) {
        if(req.readyState != 4) {
            return;
        }

        if([200,304].indexOf(req.status) === -1) {
            deferred.reject(new Error('Server responded with a status of ' + req.status));
        } else {
            deferred.resolve(e.target.responseText);
        }
    };
    req.send(data);
    return deferred.promise();
};
getBlogs(uid,userName,0,per_page).done(function(data) {
    console.log(data);
}).fail(function(err) {
    console.log(err);
});
//var uid = 35141456;
var text = "";
var process = function(html) {
    var prev_link = $(html).find("#\\24 _newOldBlogLinkTopDiv > .pleft > a");
    var title = $(html).find("h3.title > span.tcnt");
    var content = $(html).find("div.nbw-blog");
    console.log(prev_link);
    console.log(title);
    console.log(content);
    text += "\n"+unescape($(title).text())+"\n";
    divs = $(content).children("div,p");
    divs.each(function(i) {
        var div = divs[i];
        text += "\n"+unescape(unescape($(div).text()))+"\n";
    });
    if($(prev_link).length == 1) {
        console.log("next: " + prev_link[0]["href"]);
        $.get(prev_link[0]["href"]).done(function(html) {
            process(html);
        });
    } else {
        console.log(text);
    }
};
process(document.body.innerHTML);
