#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000
#define P 1000000007
#define DIFFP(x) ((x)<0?(x)+P:(x))

int ds[10][100];
int dc[10][100];
int b10[20];

void prep()
{
    memset(ds, 0, sizeof(ds));
    memset(dc, 0, sizeof(ds));
    dc[0][0] = 1;
    for(int k = 1; k <= 9; k++)
        for(int s = 0; s <= 9*k; s++) {
            int dcx = 0, dsx= 0;
            for(int dx = 0; dx <= 9 && dx <= s; dx++) {
                dcx += dc[k-1][s-dx];
                if(dcx >= P) dcx -= P;
                dsx = (int)(((long long)dsx + (long long)ds[k-1][s-dx] * 10LL + (long long)dc[k-1][s-dx] * (long long)dx) % P);
            }
            dc[k][s] = dcx;
            ds[k][s] = dsx;
        }
    for(int k = 1; k <= 18; k++)
        b10[k] = (int)((long long)b10[k-1]*10LL%P);
}
int k_balance_sum(long long n, int k)
{
    int d = 0, b = 1;
    int digits[20];
    if(n <= 0)
        return 0;
    for(long long i = n; i > 0; i /= 10) {
        digits[d++] = i%10; b *= 10;
    }
    if(d <= k)
        return n-1;
    int s = 0;
    for(int i = 1, b = 1; i < d; i++, b *= 10)
        if(i <= k) {
            s += (11LL*b-1LL)*(9LL*b)/2%P;
            if(s >= P) s -= P;
        } else {
            int k1 = (k<i-k ? k : (i-k));
            for(int j = 0; j <= 9*k1; j++) {
                int dc1 = DIFFP(dc[k1][j] - dc[k1-1][j]), ds1 = (int)((long long)(ds[k1][j] - ds[k1-1][j] + P)*b10[i-k1]%P);
                int dc2 = b10[i-2*k1], ds2 = (int)((long long)b10[i-2*k1]*(long long)(b10[i-2*k1]-1)/2%P*(long long)b10[k1]%P);
                int dc3 = dc[k1][j], ds3 = ds[k1][j];
                s = (int)(((long long)s + ((long long)ds1*(long long)dc2 + (long long)dc1*(long long)ds2)%P*(long long)dc3 + (long long)dc1*(long long)dc2%P*(long long)ds3) % P);
            }
        }
    if(k > d-k) k = d-k;
    for(int i = d-1, si = 0; i >= 0; si += digits[i--]) {
        for(int di = 0; di < digits[i]; di++) {
            int dc1, ds1, dc2, ds2, dc3, ds3;
            if(i > d-k) { // d-k..d-1
                for(int j = 0; j <= 9*(i-(d-k)) && si+j<=9*k; j++) {
                    dc1 = DIFFP(dc[k1][j] - dc[k1-1][j]);
                    ds1 = (int)((long long)(ds[k1][j] - ds[k1-1][j] + P)*b10[i-k1]%P);
                    dc2 = b10[i-2*k1];
                    ds2 = (int)((long long)b10[i-2*k1]*(long long)(b10[i-2*k1]-1)/2%P*(long long)b10[k1]%P);
                    dc3 = dc[k1][j];
                    ds3 = ds[k1][j];
                    s = (int)(((long long)s + ((long long)ds1*(long long)dc2 + (long long)dc1*(long long)ds2)%P*(long long)dc3 + (long long)dc1*(long long)dc2%P*(long long)ds3) % P);
                }
            } else if(i >= k) {  // k..d-k
            } else {       // 0..k-1
            }
        }
    }
    return 0;
}

