/* xk = sum{n=0..N/3-1}(a3n*(w^3)^kn) + w^k*sum{n=0..N/3-1}(a3n+1*(w^3)^kn) + w^2k*sum{n=0..N/3-1}(a3n*(w^3)^kn) 
 *   = sum{n=0..N/3-1}((an + aN/3+n*w^Nk/3 + a2N/3+n*w^2Nk/3)*w^nk)
 *
 * DIT:
 * xk = sum{n=0..N/3-1}(a3n*w^3kn) + w^k*sum{n=0..N/3-1}(a3n+1*w^3kn) + w^2k*sum{n=0..N/3-1}(a3n*w^3kn)
 * xk+N/3 = sum{n=0..N/3-1}(a3n*w^3kn) + w^(k+N/3)*sum{n=0..N/3-1}(a3n+1*w^3kn) + w^(2k+2N/3)*sum{n=0..N/3-1}(a3n*w^3kn)
 * xk+2N/3 = sum{n=0..N/3-1}(a3n*w^3kn) + w^(k+2N/3)*sum{n=0..N/3-1}(a3n+1*w^3kn) + w^(2k+N/3)*sum{n=0..N/3-1}(a3n*w^3kn)
 * DIF:
 * x3k = sum{n=0..N/3-1}((an + aN/3+n + a2N/3+n)*w^3nk)
 * x3k+1 = sum{n=0..N/3-1}((an + aN/3+n*w^(N/3) + a2N/3+n*w^(2N/3))*w^n*w^(3nk))
 * x3k+2 = sum{n=0..N/3-1}((an + aN/3+n*w^(2N/3) + a2N/3+n*w^(N/3))*w^2n*w^(3nk))
 * 
 * f*g(n) = sum(f(k)*g(n-k))
 * 
 * xk = sum{n=0..N-1}(an*w^kn)
 * x0 = sum{n=0..N-1}(an)
 * xk = a0+sum{n=1..N-1}(an*w^kn)
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <numeric>
#include <vector>
using namespace std;

#define LIM 20

template<typename T>
class FFT {
  public:
    FFT(int P, T w) : m_N(P-1), m_w(w), m_P(P) {
      T r(1);
      for(int i = 0; i < m_N; i++) {
        assert(!(i > 0 && r == 1));
        m_ws.push_back(r);
        r *= w;
        r %= P;
      }
      assert(r == 1);
      factor(P-1, m_f);
      divisors(m_f, m_d);
    }
    void ditfft(vector<T> &vs, bool restore = true) {
      int n0 = vs.size();
      int N = expand(vs);
      vs.resize(n0);
    }
    void diffft(vector<T> &vs, bool restore = true) {
      int n0 = vs.size();
      int N = expand(vs);
      vs.resize(n0);
    }
    // DIF2:
    // X[2k  ] = sum(w^2kn*(x[n]     + x[n+N/2]),n,0,N/2-1)
    // X[2k+1] = sum(w^2kn*w^n*(x[n] - x[n+N/2]),n,0,N/2-1)
    // (0,1,2,3,4,5,6,7)->(0,4,2,6,1,5,3,7)
    void FFT(vector<T> &v)
    {
      int w0 = m_w;
      int sz = v.size();
      assert(!(sz & (sz-1)));
      // DIFFFT
      for(int step = sz/2, w = w0; step > 0; step >>= 1, w = (int)((long long)w*(long long)w%m_P))
        for(int i = 0; i < sz; i += 2*step)
          for(int j = i, tw = 1; j < i+step; j++, tw = (int)((long long)tw*(long long)w%m_P)) {
            int x = v[j], y = v[j+step];
            int x1 = x + y;
            int y1 = x - y;
            if(x1 >= m_P) x1 -= m_P;
            if(y1 < 0) y1 += m_P;
            y1 = (int)((long long)y1 * (long long)tw % m_P);
            v[j] = x1; v[j+step] = y1;
          }
    }

    // DIT2:
    // X[k    ] = sum((w^2)^kn*x[2n],n,0,N/2-1) + w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
    // X[k+N/2] = sum((w^2)^kn*x[2n],n,0,N/2-1) - w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
    // (0,4,2,6,1,5,3,7)->(0,1,2,3,4,5,6,7)
    void IFFT(vector<T> &v)
    {
      int w0 = m_w;
      int sz = v.size();
      assert(!(sz & (sz-1)));
      vector<int> wn(1, w0);
      while(wn.back() != m_P-1)
        wn.push_back((int)((long long)wn.back()*(long long)wn.back()%m_P));
      // DITFFT
      for(int step = 1, w = wn.back(); step < sz; step <<= 1, wn.pop_back(), w = wn.back())
        for(int i = 0; i < sz; i += 2*step)
          for(int j = i, tw = 1; j < i+step; j++, tw = (int)((long long)tw*(long long)w%m_P)) {
            int x = v[j], y = v[j+step];
            y = (int)((long long)y * (long long)tw % m_P);
            int x1 = x + y;
            int y1 = x - y;
            if(x1 >= m_P) x1 -= m_P;
            if(y1 < 0) y1 += m_P;
            v[j] = x1; v[j+step] = y1;
          }
      // FFT->IFFT
      int szr = inverse(sz, m_P);
      for(int i = 0; i < sz; i++)
        v[i] = (int)((long long)v[i] * (long long)szr % m_P); 
    }

  private:
    int factor(int n, vector<int> &fs);
    int divisors(vector<int> &fs, vector<int> &ds);
    void expand(vector<T> &vs);

    const int m_N;
    const int m_P;
    T m_w;
    vector<T> m_ws;
    vector<int> m_fs;
    vector<int> m_ds;
};
