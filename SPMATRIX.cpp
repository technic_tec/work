#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 10000000
#define P 1000000007

// c[n, r] is the count of n*n matrix, value range from [0, r] and no missing values.
//
// A[1, x] = 1
// A[n, x] = sum(stirling2(n, k)*A[k, x]*x, {k, 1, n-1})
// c[1, 0] = 1, c[1, r>0] = 0
// c[n, 0] = 0, c[n, r] = sum(stirling2(n, k)*c[k, r-1], {k, 1, n-1}) (0<r<n)
// c[n, n-1] = stirling2(n, n-1)*c[n-1, n-2]
// c[n, n-2] = stirling2(n, n-2)*c[n-2, n-3] + stirling2(n, n-1)*c[n-1, n-3]
//
// stirling2(0, 0)=1, stirling2(n, 0) = stirling2(0, n) = 0
// stirling2(n+1, k) = k*stirling2(n, k) + stirling2(n, k-1)
// stirling2(n+1, n) = n + stirling2(n, n-1) = n + (n-1) + ... + 1 = n(n+1)/2
// stirling2(n+2, n) = n*stirling2(n+1, n) + stirling2(n+1, n-1)

int c[N+10];
int main()
{
    int s1 = 1; // stirling(n, n-1);
    int s2 = 0; // stirling(n, n-2);
    int c1 = 1; // c[n, n-1]
    int c2 = 0; // c[n, n-2]
    c[0] = c[1] = c[2] = 0;
    for(int n = 3; n <= N; n++) {
        // stirling2(n, n-1) = n*(n-1)/2
        // stirling2(n, n-2) = n*(n-1)*(n-2)*(3*n-5)/24
        // c[n-1, n-2] = stirling2(n-1, n-2)*c[n-2, n-3]
        // c[n, n-2] = stirling2(n, n-2)*c[n-2, n-3] + stirling2(n, n-1)*c[n-1, n-3]
        int os1 = s1;
        s2 = (int)(((long long)s2 + (long long)(n-2)*(long long)s1) % P);
        s1 += n-1; if(s1 >= P) s1 -= P;
        c[n] = c2 = (int)(((long long)s2 * (long long)c1 + (long long)s1 * (long long)c2) % P);
        c1 = (int)((long long)os1 * (long long)c1 % P);
    }
    int t;
    scanf("%d", &t);
    while(t--) {
        int n;
        scanf("%d", &n);
        printf("%d\n", c[n]);
    }
    return 0;
}
