l = b = 0.25
c = m = 0.5
r = t = 0.75
def domino(k, x0, y0):
  if k == 0:
    return []
  elif k == 1:
    return [(x0+c,y0+m)]
  elif k == 2:
    return [(x0+r,y0+b), (x0+l,y0+t)]
  elif k == 3:
    return [(x0+r,y0+b), (x0+c,y0+m), (x0+l,y0+t)]
  elif k == 4:
    return [(x0+l,y0+b), (x0+r,y0+b), (x0+l,y0+t), (x0+r,y0+t)]
  elif k == 5:
    return [(x0+l,y0+b), (x0+r,y0+b), (x0+c,y0+m), (x0+l,y0+t), (x0+r,y0+t)]
  elif k == 6:
    return [(x0+l,y0+b), (x0+r,y0+b), (x0+l,y0+m), (x0+r,y0+m), (x0+l,y0+t), (x0+r,y0+t)]
  else:
    raise ValueError

eps = 0.0002
def lookup(px, py, dom):
  for x, y in dom:
    if abs(x-px) < eps and abs(y-py) < eps:
      return True
  return False

def domino_match(p, dom):
  n = len(dom)
  if len(p) != n:
    return False
  if n <= 1:
    return True
  for i in range(n):
    for j in range(n):
      if i != j:
        (xi, yi), (xj, yj), (x0, y0), (x1, y1) = dom[i], dom[j], p[0], p[1]
        # [xj-xi] = [a, b] * [x1-x0] => a*(x1-x0)+b*(y1-y0)=xj-xi
        # [yj-yi]   [-b,a]   [y1-y0]    a*(y1-y0)-b*(x1-x0)=yj-yi
        a = ((xj-xi)*(x1-x0)+(yj-yi)*(y1-y0))/((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0))
        b = ((xj-xi)*(y1-y0)-(yj-yi)*(x1-x0))/((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0))
        valid = ((a**2+b**2 >= 1.0-eps) and (a**2+b**2 <= 10000.0+eps))
        for x, y in p[2:]:
          px = a*(x-x0)+b*(y-y0)+xi
          py = a*(y-y0)-b*(x-x0)+yi
          if not lookup(px, py, dom):
            valid = False
            break
        if valid:
          return True

#fst = True
while True:
  try:
    inp = raw_input()
  except EOFError:
    break
  #if inp.strip() == '':
  #  continue
  #if not fst:
  #  print ""
  n = int(inp)
  pts = []
  for _ in range(n):
    pts.append(map(float, raw_input().split()))
  cnt = 0
  for i in range(max(0, n-6), n/2+1):
    p = domino(i, 0.0, 0.0)+domino(n-i, 0.0, 1.0)
    if domino_match(p, pts):
      print i, n-i
      cnt += 1
  #assert(cnt > 0)
  #if cnt == 0:
  #  print 'ERROR:', n, pts
  #fst = False
  break
