#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
#include <map>
using namespace std;

#define P 1000000007
#define N 500

int main()
{
    vector<vector<int> > rv;
    vector<vector<int> > c;
    vector<int> fact;
    vector<int> vo;
    vo.push_back(0);
    rv.push_back(vo);
    vo.clear();
    vo.push_back(1);
    rv.push_back(vo);
    for(int n = 2; n <= N; n++) {
        vector<int> v;
        int s = 0;
        for(int i = 0; i <= n*(n-1)/2; i++) {
            s += vo[min(i,(n-1)*(n-2)/2)];
            if(s >= P) s -= P;
            if(i-n >= 0) {
                s -= vo[i-n];
                if(s < 0) s += P;
            }
            v.push_back(s);
        }
        rv.push_back(v);
        vo = v;
    }
#if 0
    for(int n = 1; n <= 50; n++)
        for(int i = 0; i <= n*(n-1)/2; i++)
            printf("%d%c", rv[n][i], (i==n*(n-1)/2?'\n':' '));
#endif
    fact.push_back(1);
    for(int i = 1; i <= N; i++)
        fact.push_back((int)((long long)fact.back()*(long long)i%P));
    vo.clear();
    vo.push_back(1);
    c.push_back(vo);
    for(int i = 1; i <= N; i++) {
        vo.clear();
        vo.push_back(1);
        for(int j = 1; j < i; j++)
            vo.push_back((c.back()[j-1] + c.back()[j])%P);
        vo.push_back(1);
        c.push_back(vo);
    }
    int t;
    scanf("%d", &t);
    while(t--) {
        int n, e;
        scanf("%d%d", &n, &e);
        int s = 0;
        for(int i = 0; i <= n; i++) {
            s = (int)((s + (long long)c[n][i]*(long long)c[n][i]%P*(long long)(n+1-i)%P*(long long)rv[i][min(e,i*(i-1)/2)]%P*(long long)fact[n-i]%P*(long long)fact[n-i])%P);
        }
        printf("%d\n", s);
    }
    return 0;
}
