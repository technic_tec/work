#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <algorithm>
// DIT2:
// for k in 0..N/2-1:
//     Xk = sum((w^2)^kn*x[2n],n,0,N/2-1) + w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
// Xk+N/2 = sum((w^2)^kn*x[2n],n,0,N/2-1) - w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
// (0,4,2,6,1,5,3,7)->(0,1,2,3,4,5,6,7)

// DIT3:
// for k in 0..N/3-1:
//      Xk = sum((w^3)^kn*x3n,n,0,N/3-1) + w^k         *sum((w^3)^kn*x3n+1,n,0,N/2-1) + w^2k         *sum((w^3)^kn*x3n+2,n,0,N/2-1)
//  Xk+N/3 = sum((w^3)^kn*x3n,n,0,N/3-1) + w^k*w^(N/3) *sum((w^3)^kn*x3n+1,n,0,N/2-1) + w^2k*w^(N/3) *sum((w^3)^kn*x3n+2,n,0,N/2-1)
// Xk+2N/3 = sum((w^3)^kn*x3n,n,0,N/3-1) + w^k*w^(2N/3)*sum((w^3)^kn*x3n+1,n,0,N/2-1) + w^2k*w^(2N/3)*sum((w^3)^kn*x3n+2,n,0,N/2-1)
// 
// DIF2:
// X[2k  ] = sum(w^2kn*(x[n]     + x[n+N/2]),n,0,N/2-1)
// X[2k+1] = sum(w^2kn*w^n*(x[n] - x[n+N/2]),n,0,N/2-1)
// (0,1,2,3,4,5,6,7)->(0,4,2,6,1,5,3,7)
// 
// DIF3:
// X[3k]   = sum(w^3kn*    (x[n] +          x[n+N/3] +          x[n+2N/3]),n,0,N/3-1)
// X[3k+1] = sum(w^3kn*w^n (x[n] + w^(N/3) *x[n+N/3] + w^(2N/3)*x[n+2N/3]),n,0,N/3-1)
// X[3k+2] = sum(w^3kn*w^2n(x[n] + w^(2N/3)*x[n+N/3] + w^(N/3) *x[n+2N/3]),n,0,N/3-1)

const int nn = 30;
const int p23[nn][5] = {
    //{17, 4, 0, 3, 6},
    {19, 1, 2, 3, 13},
    {10369,  7,  4,  13, 6381},
    {12289,  12,  1,  11, 5586},
    {17497,  3,  7,  5, 6999},
    {18433,  11,  2,  5, 11060},
    {39367,  1,  9,  3, 26245},
    {52489,  3,  8,  7, 14997},
    {65537,  16,  0,  3, 21846},
    {139969,  6,  7,  13, 75368},
    {147457,  14,  2,  10, 103220},
    {209953,  5,  8,  10, 62986},
    {331777,  12,  4,  5, 132711},
    {472393,  3,  10,  5, 283436},
    {629857,  5,  9,  5, 251943},
    {746497,  10,  6,  5, 298599},
    {786433,  18,  1,  10, 235930},
    {839809,  7,  8,  7, 479891},
    {995329,  12,  5,  7, 142190},
    {1179649,  17,  2,  19, 310434},
    {1492993,  11,  6,  7, 853139},
    {1769473,  16,  3,  5, 1061684},
    {1990657,  13,  5,  5, 796263},
    {2654209,  15,  4,  11, 965167},
    {5038849,  8,  9,  29, 2085041},
    {5308417,  16,  4,  5, 2123367},
    {8503057,  4,  12,  5, 3401223},
    {11337409,  6,  11,  7, 1619630},
    {14155777,  19,  3,  7, 2022254},
    {19131877,  2,  14,  5, 7652751},
    {28311553,  20,  3,  5, 16986932}
};

void DitFft23Naive(int N, int v[], int p2, int p3, int w0)
{
    int *vo = new int[N];
    int w1 = 1;
    int p = N+1;
    for(int i = 0; i < N; i++) {
        vo[i] = 0;
        for(int j = N-1; j >= 0; j--)
            vo[i] = (int)(((long long)vo[i] * (long long)w1 + (long long)v[j]) % p);
        w1 = (int)((long long)w1 * (long long)w0 % p);
    }
    for(int i = 0; i < N; i++)
        v[i] = vo[i];
    delete[] vo;
}
void DitFft23Recursive(int p, int N, int v[], int p2, int p3, int w0)
{
    if(p2) {
        // DIT2:
        // for k in 0..N/2-1:
        //     Xk = sum((w^2)^kn*x[2n],n,0,N/2-1) + w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
        // Xk+N/2 = sum((w^2)^kn*x[2n],n,0,N/2-1) - w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
        int *ve = new int[N/2];
        int *vo = new int[N/2];
        for(int i = 0; i < N; i+=2) {
            ve[i/2] = v[i];
            vo[i/2] = v[i+1];
        }
        int ww = (int)((long long)w0 * (long long)w0 % p);
        DitFft23Recursive(p, N/2, ve, p2-1, p3, ww);
        DitFft23Recursive(p, N/2, vo, p2-1, p3, ww);
        for(int k = 0, w1 = 1; k < N/2; k++) {
            v[k] = (int)(((long long)ve[k] + (long long)w1*(long long)vo[k]) % p);
            v[k+N/2] = (int)(((long long)ve[k] - (long long)w1*(long long)vo[k]) % p);
            if(v[k+N/2] < 0) v[k+N/2] += p;
            w1 = (int)((long long)w1 * (long long)w0 % p);
        }
        delete[] ve;
        delete[] vo;
    } else if(p3) {
        // DIT3:
        // for k in 0..N/3-1:
        //      Xk = sum((w^3)^kn*x3n,n,0,N/3-1) + w^k         *sum((w^3)^kn*x3n+1,n,0,N/2-1) + w^2k         *sum((w^3)^kn*x3n+2,n,0,N/2-1)
        //  Xk+N/3 = sum((w^3)^kn*x3n,n,0,N/3-1) + w^k*w^(N/3) *sum((w^3)^kn*x3n+1,n,0,N/2-1) + w^2k*w^(2N/3)*sum((w^3)^kn*x3n+2,n,0,N/2-1)
        // Xk+2N/3 = sum((w^3)^kn*x3n,n,0,N/3-1) + w^k*w^(2N/3)*sum((w^3)^kn*x3n+1,n,0,N/2-1) + w^2k*w^(N/3) *sum((w^3)^kn*x3n+2,n,0,N/2-1)
        int *v1 = new int[N/3];
        int *v2 = new int[N/3];
        int *v3 = new int[N/3];
        for(int i = 0; i < N; i+=3) {
            v1[i/3] = v[i];
            v2[i/3] = v[i+1];
            v3[i/3] = v[i+2];
        }
        int ww = (int)((long long)w0 * (long long)w0 % p * (long long)w0 % p);
        DitFft23Recursive(p, N/3, v1, p2, p3-1, ww);
        DitFft23Recursive(p, N/3, v2, p2, p3-1, ww);
        DitFft23Recursive(p, N/3, v3, p2, p3-1, ww);
        int w3 = 1;
        for(int i = 0; i < N/3; i++)
            w3 = (int)((long long)w3 * (long long)w0 % p);
        int ww3 = (int)((long long)w3 * (long long)w3 % p);
        for(int k = 0, w1 = 1; k < N/3; k++) {
            int ww1 = (int)((long long)w1 * (long long)w1 % p);
            v[k] = (int)(((long long)v1[k] + (long long)w1*(long long)v2[k] + (long long)ww1*(long long)v3[k]) % p);
            v[k+N/3] = (int)(((long long)v1[k] + (long long)w3 * (long long)w1 % p * (long long)v2[k] + (long long)ww3 * (long long)ww1 % p * (long long)v3[k]) % p);
            v[k+2*N/3] = (int)(((long long)v1[k] + (long long)ww3 * (long long)w1 % p * (long long)v2[k] + (long long)w3 * (long long)ww1 % p * (long long)v3[k]) % p);
            w1 = (int)((long long)w1 * (long long)w0 % p);
        }
        delete[] v1;
        delete[] v2;
        delete[] v3;
    } else {
        assert(N == 1);
        return;
    }
}
void DitFft23(int N, int v[], int p2, int p3, int w0)
{
    int step = 1, p = N+1;
    int *w = new int[p2+p3];
    w[0] = w0;
    for(int i = 1; i <= p2; i++)
        w[i] = (int)((long long)w[i-1]*(long long)w[i-1]%p);
    for(int i = p2+1; i < p2+p3; i++)
        w[i] = (int)((long long)w[i-1]*(long long)w[i-1]%p*(long long)w[i-1]%p);

    // DIT3:
    // for k in 0..N/3-1:
    //      Xk = sum((w^3)^kn*x3n,n,0,N/3-1) + w^k         *sum((w^3)^kn*x3n+1,n,0,N/2-1) + w^2k         *sum((w^3)^kn*x3n+2,n,0,N/2-1)
    //  Xk+N/3 = sum((w^3)^kn*x3n,n,0,N/3-1) + w^k*w^(N/3) *sum((w^3)^kn*x3n+1,n,0,N/2-1) + w^2k*w^(2N/3)*sum((w^3)^kn*x3n+2,n,0,N/2-1)
    // Xk+2N/3 = sum((w^3)^kn*x3n,n,0,N/3-1) + w^k*w^(2N/3)*sum((w^3)^kn*x3n+1,n,0,N/2-1) + w^2k*w^(N/3) *sum((w^3)^kn*x3n+2,n,0,N/2-1)
    int w3 = w0;
    for(int i = 0; i < p2; i++)
        w3 = (int)((long long)w3*(long long)w3%p);
    for(int i = 0; i < p3-1; i++)
        w3 = (int)((long long)w3*(long long)w3%p*(long long)w3%p);
    int ww3 = (int)((long long)w3 * (long long)w3 % p);
    while(p3) {
        p3--;
        int wx = w[p2+p3];
        for(int i = 0; i < N; i += 3*step)
            for(int j = i, w1 = 1; j < i+step; j++, w1 = (int)((long long)w1 * (long long)wx % p)) {
                int ww1 = (int)((long long)w1 * (long long)w1 % p);
                int x = v[j], y = v[j+step], z = v[j+2*step];
                v[j       ] = (int)(((long long)x +                 (long long)w1     * (long long)y +                 (long long)ww1     * (long long)z) % p);
                v[j+  step] = (int)(((long long)x + (long long)w3 * (long long)w1 % p * (long long)y + (long long)ww3* (long long)ww1 % p * (long long)z) % p);
                v[j+2*step] = (int)(((long long)x + (long long)ww3* (long long)w1 % p * (long long)y + (long long)w3 * (long long)ww1 % p * (long long)z) % p);
            }
        step *= 3;
    }

    // DIT2:
    // for k in 0..N/2-1:
    //     Xk = sum((w^2)^kn*x[2n],n,0,N/2-1) + w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
    // Xk+N/2 = sum((w^2)^kn*x[2n],n,0,N/2-1) - w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
    while(p2) {
        p2--;
        int wx = w[p2+p3], w1 = 1;
        for(int i = 0; i < N; i += 2*step)
            for(int j = i, w1 = 1; j < i+step; j++, w1 = (int)((long long)w1 * (long long)wx % p)) {
                int x = v[j], y = v[j+step];
                v[j     ] = (int)(((long long)x + (long long)w1 * (long long)y) % p);
                v[j+step] = (int)(((long long)x - (long long)w1 * (long long)y) % p);
                if(v[j+step] < 0) v[j+step] += p;
            }
        step *= 2;
    }
    delete[] w;
}

int bitreverse23(int x, int p2, int p3)
{
    // 33..322..2 -> 22..233..3
    int r = 0;
    while(p2--) {
        int d = x % 2;
        x /= 2;
        r = r * 2 + d;
    }
    while(p3--) {
        int d = x % 3;
        x /= 3;
        r = r * 3 + d;
    }
    assert(!x);
    return r;
}

int bitreverse32(int x, int p2, int p3)
{
    // 22..233..3 -> 33..322..2
    int r = 0;
    while(p3--) {
        int d = x % 3;
        x /= 3;
        r = r * 3 + d;
    }
    while(p2--) {
        int d = x % 2;
        x /= 2;
        r = r * 2 + d;
    }
    assert(!x);
    return r;
}

int power_mod(int n, int k, int p)
{
    int r = 1;
    while(k) {
        if(k & 1)
            r = (int)((long long)r * (long long)n % p);
        n = (int)((long long)n * (long long)n % p);
        k >>= 1;
    }
    return r;
}
bool check()
{
    assert(bitreverse32(bitreverse23(700, 5, 3), 5, 3) == 700);
    assert(bitreverse23(bitreverse32(700, 5, 3), 5, 3) == 700);
    assert(bitreverse32(bitreverse23(903, 4, 4), 4, 4) == 903);
    assert(bitreverse23(bitreverse32(903, 4, 4), 4, 4) == 903);
    for(int i = 0; i < nn; i++) {
        int v = 1, N = p23[i][0], p2 = p23[i][1], p3 = p23[i][2], w = p23[i][3], wr = p23[i][4];
        for(int j = 0; j < p2; j++)
            v *= 2;
        for(int j = 0; j < p3; j++)
            v *= 3;
        assert(v == N-1);
        assert(power_mod(w, N-1, N) == 1);
        if(p2)
            assert(power_mod(w, (N-1)/2, N) != 1);
        if(p3)
            assert(power_mod(w, (N-1)/3, N) != 1);
        assert((long long)w * (long long)wr % N == 1);
    }
    int N = p23[0][0], p2 = p23[0][1], p3 = p23[0][2], w0 = p23[0][3];
    int *v1 = new int[N-1];
    int *v2 = new int[N-1];
    int *v3 = new int[N-1];
    memset(v1, 0, (N-1)*sizeof(int));
    v1[0] = v1[1] = v1[2] = 1;
    for(int i = 0; i < N-1; i++) {
        v2[i] = v1[i];
        v3[i] = v1[bitreverse32(i, p2, p3)];
    }
    DitFft23Naive(N-1, v1, p2, p3, w0);
    DitFft23Recursive(N, N-1, v2, p2, p3, w0);
    DitFft23(N-1, v3, p2, p3, w0);
    printf("ref: ");
    for(int i = 0; i < N-1; i++)
        printf("%d%c", v1[i], (i < N-2 ? ' ' : '\n'));
    printf("rec: ");
    for(int i = 0; i < N-1; i++)
        printf("%d%c", v2[i], (i < N-2 ? ' ' : '\n'));
    printf("got: ");
    for(int i = 0; i < N-1; i++)
        printf("%d%c", v3[i], (i < N-2 ? ' ' : '\n'));
    delete[] v1;
    delete[] v2;
    delete[] v3;
    return true;
}
int main()
{
#if 1
    check();
#else
    int n, s;
    scanf("%d", &n);
    int *v = new int[n];
    s = 0;
    for(int i = 0; i < n; i++) {
        scanf("%d", &v[i]);
        s += v[i];
    }

    if(n <= 5000) {
        int *sv = new int[n*(n+1)/2];
        int m = 0;
        for(int i = 0; i < n; i++) {
            int sx = 0;
            for(int j = i; j < n; j++) {
                sx += v[j];
                sv[m++] = sx;
            }
        }
        std::sort(sv, sv+m);
        int c = 0;
        for(int i = 0; i < m; i++) {
            ++c;
            while(i+1 < m && sv[i+1] == sv[i])
                ++i;
        }
        printf("%d\n", c-1);
        delete[] sv;
    } else {
        int m = 0;
        while(m < nn && p23[m][0] <= 2*s)
            ++m;
        int N = p23[m][0], p2 = p23[m][1], p3 = p23[m][2], w0 = p23[m][3], w0r = p23[m][4];
        int *v1 = new int[N-1];
        int *v2 = new int[N-1];
        memset(v1, 0, (N-1)*sizeof(int));
        memset(v2, 0, (N-1)*sizeof(int));
        v1[0] = 1; v2[bitreverse23(s, p2, p3)] = 1;
        for(int i = 0, sx = 0; i < n; i++) {
            sx += v[i];
            v1[bitreverse23(sx, p2, p3)] = 1; // v[x] <- v[r23(x)] <=> v[r32(y)] <- v[y]
            v2[bitreverse23(s-sx, p2, p3)] = 1;
        }
        DitFft23(N-1, v1, p2, p3, w0);
        DitFft23(N-1, v2, p2, p3, w0);
        for(int i = 0; i < N-1; i++)
            v2[i] = (int)((long long)v1[i] * (long long)v2[i] % N);
        for(int i = 0; i < N-1; i++)
            v1[i] = v2[bitreverse32(i, p2, p3)];
        DitFft23(N-1, v1, p2, p3, w0r);
        for(int i = 0; i < N-1; i++)
            v1[i] = (int)((long long)v1[i] * (long long)(N-1) % N);
        int c = 0;
        for(int i = s+1; i <= 2*s; i++)
            if(v1[i])
                ++c;
        printf("%d\n", c-1);
        delete[] v1;
        delete[] v2;
    }
    delete[] v;
#endif
    return 0;
}
