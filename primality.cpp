/*
 * =====================================================================================
 *
 *       Filename:  primality.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年11月04日 15时38分43秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define N 100000000001ULL
#define M 0x7fffffffffffULL

typedef unsigned long long ULL;

ULL mul_mod(ULL a, ULL b, ULL p)
{
    assert(p <= M);
    a %= p; b %= p;
    ULL c2 = a*((b>>32) & 0xffff)%p;
    ULL c1 = a*((b>>16) & 0xffff)%p;
    ULL c0 = a*(b & 0xffff)%p;
    ULL c = ((((c2<<16)+c1)%p<<16)+c0)%p;
    return c;
}

ULL power_mod(ULL n, ULL k, ULL p)
{
    ULL r = 1;
    while(k) {
        if(k & 1)
            r = mul_mod(r, n, p);
        n = mul_mod(n, n, p);
        k >>= 1;
    }
    return r;
}

ULL gcd(ULL a, ULL b)
{
    while(b) {
        ULL c = a, a = b; b = c % b;
    }
    return a;
}

bool witness(ULL n, ULL p)
{
    if(n == p)
        return false;
    else if(n % p == 0)
        return true;
    ULL t = n-1, k = 0;
    while(!(t & 1)) {
        t >>= 1;
        ++k;
    }
    ULL r = power_mod(p, t, n);
    if(r == 1 || r == n-1)
        return false;
    for(int i = 0; i < k; i++) {
        r = mul_mod(r, r, n);
        if(r == n-1)
            return false;
        else if(r == 1)
            return true;
    }
    return true;
}

bool is_prime(ULL n)
{
    int l[20], m = 0;
    if(n <= 1 || !(n & 1))
        return false;
    if(n < 1373653LL) {
        l[m++] = 2;
        l[m++] = 3;
    } else if(n < 9080191ULL) {
        l[m++] = 31;
        l[m++] = 73;
    } else if(n < 4759123141ULL) {
        l[m++] = 2;
        l[m++] = 7;
        l[m++] = 61;
    } else if(n < 1122004669633ULL) {
        l[m++] = 2;
        l[m++] = 13;
        l[m++] = 23;
        l[m++] = 1662803;
    } else if(n < 2152302898747ULL) {
        l[m++] = 2;
        l[m++] = 3;
        l[m++] = 5;
        l[m++] = 7;
        l[m++] = 11;
    } else if(n < 3474749660383ULL) {
        l[m++] = 2;
        l[m++] = 3;
        l[m++] = 5;
        l[m++] = 7;
        l[m++] = 11;
        l[m++] = 13;
    } else if(n < 341550071728321ULL) {
        l[m++] = 2;
        l[m++] = 3;
        l[m++] = 5;
        l[m++] = 7;
        l[m++] = 11;
        l[m++] = 13;
        l[m++] = 17;
    } else if(n < 3825123056546413051ULL) {
        l[m++] = 2;
        l[m++] = 3;
        l[m++] = 5;
        l[m++] = 7;
        l[m++] = 11;
        l[m++] = 13;
        l[m++] = 17;
        l[m++] = 19;
        l[m++] = 23;
    } else
        assert(0);
    for(int i = 0; i < m; i++)
        if(witness(n, l[i]))
            return false;
    return true;
}

ULL pollard_rho(ULL n)
{
    ULL x = 2, y = 2, d = 1, c = 1;
    while(true) {
        while(d == 1) {
            x = mul_mod(x, x, n); x = (x+c) % n;
            y = mul_mod(y, y, n); y = (y+c) % n;
            y = mul_mod(y, y, n); y = (y+c) % n;
            d = gcd((x>y?x-y:y-x), n);
        }
        if(d == n) {
            x = y = 2; d = 1; c += 2;
        } else
            return d;
    }
}

ULL q[100], hd, tl;
int factor(ULL n, ULL f[])
{
    ULL m = 0;
    hd = tl = 0; q[tl++] = n;
    while(hd < tl) {
        n = q[hd++];
        if(is_prime(n))
            f[m++] = n;
        else {
            ULL x = pollard_rho(n);
            q[tl++] = x;
            q[tl++] = n/x;
        }
    }
    return m;
}

int main()
{
    for(ULL i = N+1; i <= N+1000; i++)
        if(is_prime(i))
            printf("%llu ", i);
    printf("\n");
}
