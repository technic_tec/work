def normalize(s):
    nxt = 'a'
    mp = dict()
    o = ''
    for c in s:
        if c not in mp:
            mp[c] = nxt
            nxt = chr(ord(nxt)+1)
        o += mp[c]
    return o

# split l into a1+a2+...+ak while a1>=a2>=...>=ak and at most 1 odd in [a1, a2, ..., ak]
def palindrome_split(l, st=1):
    if st > l:
        return
    elif st == l:
        yield [l]
        return
    if (st & 1) == 0 or (l & 1):
        for x in palindrome_split(l-st, st + (st&1)):
            yield x + [st]
    for x in palindrome_split(l, st+1):
        yield x

def palindrome_gen(l):
    for x in palindrome_split(l):
        yield ''.join([chr(ch)*c for ch, c in zip(range(97, 97+len(x)), x)])

def perm_gen(s):
    d = set([s])
    q = set([s])
    yield(s)
    while len(q) > 0:
        s = q.pop()
        for i in range(len(s)):
            for j in range(i):
                if s[i] != s[j]:
                    ss = normalize(s[:j] + s[i] + s[j+1:i] + s[j] + s[i+1:])
                    ss = min(ss, normalize(s[::-1]))
                    if ss not in d:
                        d.add(ss)
                        q.add(ss)
                        yield(ss)

for i in range(1, 9):
    for x in palindrome_gen(i):
        print len(list(perm_gen(x))), x
