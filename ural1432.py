def minval(se, so, k, tr):
  ce, co = (k+1)/2, k/2
  if se > 9*ce or so > 9*co or se < tr or so < 0:
    return None
  return None

s = raw_input().strip()
n = len(s)
le = lo = re = ro = 0

def accum(i, x):
  global le, lo, re, ro
  if i < n/2:
    if n & 1:
      le += x
    else:
      lo += x
  else:
    if n & 1:
      re += x
    else:
      ro += x

for i in range(n):
  accum(i, int(s[i]))
i = n
while i >= n/2:
  # LR -> EO: (re, le) = (lo+5, le-5), odd trailing 9s
  # EO -> LR: (re', le') = (lo-5, le+5), odd trailing 0s
  i -= 1
  while(i >= 0 and
  accum(i-1, 1)
