-- Enter your code here. Read input from STDIN. Print output to STDOUT
import Data.Array
ccount :: Int -> ([Int] -> [Int])
ccount vm vl = update vl va
  where va = array (0,vm) [(i,0) | i<-[0..vm]]
        update [] va = elems va
        update (c:cs) va = update cs (va//[(c,(va!c)+1)])

missing :: Int -> ([(Int,Int)] -> [Int])
missing vl [] = []
missing vl ((x,y):cs) | x < y = vl:(missing (vl+1) cs)
                      | otherwise = missing (vl+1) cs
main :: IO()
main = do
  l <- getLine
  let n = read l :: Int
  l <- getLine
  let va0 = map read $ words l
  l <- getLine
  let m = read l :: Int
  l <- getLine
  let vb0 = map read $ words l
  let vl = min (minimum va0) (minimum vb0)
  let vr0 = max (maximum va0) (maximum vb0)
  let va = map (\x->x-vl) va0
  let vb = map (\x->x-vl) vb0
  let vr = vr0 - vl
  let cx = ccount vr va
  let cy = ccount vr vb
  let c = missing vl (zip cx cy)
  putStrLn $ unwords $ map show c
