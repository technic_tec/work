/*
 * =====================================================================================
 *
 *       Filename:  ADIGIT.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年04月14日 10时29分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100100

int n, m, x, c[10], s[N];
char d[N];
int main()
{
    scanf("%d%d%s", &n, &m, d);
    memset(c, 0, sizeof(c));
    for(int i = 0; i < n; i++) {
        int xi = d[i]-'0';
        s[i] = 0;
        for(int j = 0; j < 10; j++)
            s[i] += abs(j-xi)*c[j];
        //printf("%d%c", s[i], (i<n-1?' ':'\n'));
        c[d[i]-'0']++;
    }
    while(m--) {
        scanf("%d", &x);
        printf("%d\n", s[x-1]);
    }
    return 0;
}

