/*
 * =====================================================================================
 *
 *       Filename:  empty.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年11月18日 16时41分03秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 3100000

int n, c[N];

int main()
{
    int k;
    freopen("empty.in", "r", stdin);
    freopen("empty.out", "w", stdout);
    scanf("%d%d", &n, &k);
    memset(c, 0, sizeof(c));
    for(int i = 0; i < k; i++) {
        int x, y, a, b;
        scanf("%d%d%d%d", &x, &y, &a, &b);
        for(int j = 1; j <= y; j++)
            c[((long long)a*(long long)j+(long long)b)%n] += x;
    }
    int cc = 0;
    for(int i = n-1, s = 0; i >= 0; i--) {
        s += c[i];
        if(i+s-n > cc)
            cc = i+s-n;
    }
    int res = n;
    for(int i = 0; i < n; i++) {
        cc += c[i];
        if(cc == 0) {
            res = i;
            break;
        }
        cc--;
    }
    printf("%d\n", res);
    return 0;
}

