/*
 * =====================================================================================
 *
 *       Filename:  HAMILG.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月09日 14时24分11秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <map>
using namespace std;

#define N 1000000009

class Graph {
  public:
    Graph(int n, int m)
    : m_sz(n), m_adj(n) {
    }
    void add_edge(int x, int y)
    {
      m_adj[x-1].push_back(y-1);
      m_adj[y-1].push_back(x-1);
      m_win.clear();
    }
    bool can_win(int st, int x) {
      if(m_win.find(make_pair(st, x)) == m_win.end()) {
        bool win = false;
        for(int i = 0; i < m_adj[x].size(); i++) {
          int y = m_adj[x][i];
          if(((st >> y) & 1) && !can_win(st&~(1<<x), y)) {
            win = true;
            break;
          }
        }
        m_win[make_pair(st, x)] = win;
      }
      return m_win[make_pair(st, x)];
    }
    int count_win()
    {
      int cnt = 0;
      if(m_sz > 20)
        return 0;
      for(int s = 0; s < m_sz; s++) {
        bool win = true;
        int st = (~(1<<s))&((1<<m_sz)-1);
        for(int i = 0; i < m_adj[s].size(); i++) {
          int x = m_adj[s][i];
          if(!can_win(st, x)) {
            win = false;
            break;
          }
        }
        if(win) {
          printf("win: %d\n", s+1);
          cnt++;
        }
      }
      return cnt;
    }
  private:
    int m_sz;
    vector<vector<int> > m_adj;
    map<pair<int, int>, bool> m_win;
};

int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    int n, m;
    scanf("%d%d", &n, &m);
    Graph g(n, m);
    for(int i = 0; i < m; i++) {
      int x, y;
      scanf("%d%d", &x, &y);
      g.add_edge(x, y);
    }
    printf("%d\n", g.count_win());
  }
  return 0;
}


