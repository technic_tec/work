from collections import deque

class HLPP(object):
    class Vertex(object):
        def __init__(self,idx):
            self.idx = idx         # vertex index
            self.in_edges = list()  # list of incoming edges
            self.out_edges = list() # list of outgoing edges
            self.e = 0             # excess flow
            self.h = 0             # height label
            self.current_edge = 0  # current edge idx
            
    class Edge(object):
        def __init__(self,src,dst,cap):
            self.src = src    # source
            self.dst = dst    # destination
            self.cap = cap    # capacity
            self.flow = 0     # flow on edge (initially 0)

    def __init__(self, n):
        self.nVertices = n
        self.vs = [HLPP.Vertex(i) for i in range(n)]      # vertices
        self.vc = [n] + [0] * (2*n)                  # vertices count of each label
        self.active = [set() for _ in range(2*n+1)]  # active vertex lists by label
        self.level = 0                               # current highest label of active vertices

    def add_edge(self,x,y,c):
        assert min(x,y) >= 0 and max(x,y) < self.nVertices
        e = HLPP.Edge(x,y,c)
        self.vs[x].out_edges.append(e)
        self.vs[y].in_edges.append(e)
        
    def computeLabel(self,src,dest):
        q = deque()
        inque = [False]*self.nVertices
        self.vc[self.vs[dest].h] -= 1
        self.vs[dest].h = 0
        self.vc[self.vs[dest].h] += 1
        q.append(dest)
        inque[dest] = True
        while len(q) > 0:
            v = q.popleft()
            for e in self.vs[v].in_edges:
                if not inque[e.src] and e.flow < e.cap:
                    self.vc[self.vs[e.src].h] -= 1
                    self.vs[e.src].h = self.vs[v].h + 1
                    self.vc[self.vs[e.src].h] += 1
                    q.append(e.src)
                    inque[e.src] = True
            for e in self.vs[v].out_edges:
                if not inque[e.dst] and e.flow > 0:
                    self.vc[self.vs[e.dst].h] -= 1
                    self.vs[e.dst].h = self.vs[v].h + 1
                    self.vc[self.vs[e.dst].h] += 1
                    q.append(e.dst)
                    inque[e.dst] = True
        self.vc[self.vs[src].h] -= 1
        self.vs[src].h = self.nVertices
        self.vc[self.vs[src].h] += 1

    def maxflow(self,src,dest):
        self.computeLabel(src,dest)
        u = self.vs[src]
        for e in u.out_edges:
            if e.cap > 0:
                v = self.vs[e.dst]
                e.flow = e.cap
                if v.e == 0:
                    self.active[v.h].add(v)
                v.e += e.flow
                if self.level < v.h:
                    self.level = v.h
        while self.level >= 0:
            if len(self.active[self.level]) > 0:
                u = self.active[self.level].pop()
                if u.idx != src and u.idx != dest:
                  self.discharge(u)
            else:
                self.level -= 1
        return sum([e.flow for e in self.vs[src].out_edges])

    def discharge(self,u):
        while u.e > 0:
            if u.current_edge < len(u.out_edges):
                e = u.out_edges[u.current_edge]
                v = self.vs[e.dst]
                if u.h == v.h + 1 and e.flow < e.cap: # push
                    d = min(u.e, e.cap - e.flow)
                    e.flow += d
                    u.e -= d
                    v.e += d
                    if d > 0 and v.e == d:
                        self.active[v.h].add(v)
                        if v.h > self.level:
                            self.level = v.h
                else:              # skip
                    u.current_edge += 1
            elif u.current_edge < len(u.out_edges) + len(u.in_edges):
                e = u.in_edges[u.current_edge-len(u.out_edges)]
                v = self.vs[e.src]
                if u.h == v.h + 1 and e.flow > 0:  # reverse push
                    d = min(u.e,e.flow)
                    e.flow -= d
                    u.e -= d
                    v.e += d
                    if d > 0 and v.e == d:
                        self.active[v.h].add(v)
                        if v.h > self.level:
                            self.level = v.h
                else:               # skip
                    u.current_edge += 1
            else:                   # relabel
                hx = min([self.vs[e.dst].h for e in u.out_edges if e.flow < e.cap] + [self.vs[e.src].h for e in u.in_edges if e.flow > 0])
                self.vc[u.h] -= 1
                if self.vc[u.h] == 0:
                    #gap opt
                    pass
                u.h = hx + 1
                self.vc[u.h] += 1
                u.current_edge = 0

    #gap heuristic
f = HLPP(6)
f.add_edge(0,1,15)
f.add_edge(0,3,4)
f.add_edge(1,2,12)
f.add_edge(2,3,3)
f.add_edge(2,5,7)
f.add_edge(3,4,10)
f.add_edge(4,1,5)
f.add_edge(4,5,10)
print f.maxflow(0,5)

