#!/usr/bin/python

import sys

# (n!)p mod P=p^q
# (up!)p = +-product{j=1..r}((jp!)p^bj) (mod p^(2r+1)), bj = (u/j)*product{i=1..r,i!=j}((u^2-i^2)/(j^2-i^2))
def facterialp(n, P):
    pass
# Cp(n, m) = (n!)p/(m!)p/((n-m)!)p mod P=p^q
def binomialp(n, m, P):
    pass

# find k in [0, 2^n) such that C(2^n-1, k) == r (mod 2^n), and c = C(2^n-1, k) % P
# C(2^n-1, k) = C(2^(n-1)-1, [k/2]) * (2^n-1)(2^n-3)...(2^n-(k-(k+1)&1))/(1*3*...*(k-(k+1)&1))
# C(2^n-1, 4m)   = C(2^(n-1)-1, 2m)   * (2^n-1)(2^n-3)...(2^n-(4m-1))/(1*3*...*(4m-1))
#                = C(2^(n-1)-1, 2m)   (mod 2^n)
# C(2^n-1, 4m+3) = C(2^(n-1)-1, 2m+1) * (2^n-1)(2^n-3)...(2^n-(4m+3))/(1*3*...*(4m+3))
#                = C(2^(n-1)-1, 2m+1) (mod 2^n)
def solve(n, r, P):
    if r & 1:
        return -1
    elif n == 1:
        return 0
    elif n == 2:
        return r/2
    k, c = solve(n-1, r & ~(1L<<(n-1)), P)
    k = ((k<<1) | (k&1))
    if ((r-c)>>(n-1)) & 1:
        k ^= 2
    c *= binomialp((1L<<n)-1, k, P)
    return k, c

def main():
    t = int(sys.stdin.readline())
    for _ in range(t):
        n, r = map(long, sys.stdin.readline().split())
        k, c = solve(n, r)
        print k

if __name__ == '__main__':
    main()
