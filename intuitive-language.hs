-- Number: n where n is any integer number.
-- PNumber: n where n > 0.
-- Value: Number | Number / PNumber | FValue.
-- FValue: Calling function of n with all n parameters.
-- OPa: + | -.
-- OPb: * | /.
-- Expression: Term [OPa Term1 ...].
-- Term: Fact [OPb Fact1 ...].
-- Fact: Value | Function Call | (Expression)
-- Letter: 'A'..'Z' | 'a'..'z'.
-- Function call: %Name%[Expression1][Expression2]...[ExpressionM].
-- OPb > OPa
-- Variable consists of 1 or more letters followed by 0 or more digits; Key words (is,  of,  assign,  what,  function,  do,  and,  to) are not allowed as variable names.
-- Statements:
--   %Variable% is function of %N%: %Expression1%,  %Expression2%,  ...,  %ExpressionN%,  %Expression0%.
--   %Variable% is %Expression%. 
--   Assign %Expression1% to %Variable1% [ AND %Expression2% to %Variable2% ... ]!
--   do {Expression} %Assign values(s) to variable(s)%!
--   what is %Function1 call% [AND %Variable1% [ AND %Function2 call% ... ] ]?

import Char
import Control.Monad
import Data.Maybe
import qualified Data.Map as Map
import Text.ParserCombinators.Parsec(Parser, (<|>), try, letter, alphaNum, oneOf, parse, option, many, many1, sepBy1, eof)
import qualified Text.ParserCombinators.Parsec.Language as L
import qualified Text.ParserCombinators.Parsec.Token as T
import Text.ParserCombinators.Parsec.Expr

data Variable = Var String
	deriving (Eq, Show)
data Expr = Fraction Integer Integer | OpAdd Expr Expr | OpSub Expr Expr | OpMul Expr Expr | OpDiv Expr Expr | FunCall Variable [Expr]
	deriving (Eq, Show)
data Stmt = Def Variable [Expr] | Assign [(Variable, Expr)] | Do Integer Stmt | Query [Expr]
	deriving (Eq, Show)

def = L.emptyDef{ L.identStart = letter
		,  L.identLetter = alphaNum
		,  L.opStart = oneOf "+-*/.!?:"
		,  L.opLetter = oneOf "+-*/.!:?"
		,  L.reservedOpNames = ["+", "-", "*", "/", ".", "!", "?", ":"]
		,  L.reservedNames = ["is", "of", "assign", "what", "function", "do", "and", "to"]
        ,  L.caseSensitive = False
}
T.TokenParser{ T.parens = parens
        ,  T.braces = braces
        ,  T.brackets = brackets
		,  T.whiteSpace = whiteSpace
		,  T.integer = integer
		,  T.identifier = identifier
		,  T.reservedOp = reservedOp
		,  T.reserved = reserved
        ,  T.commaSep1 = commaSep1
} = T.makeTokenParser def

opa = [
 [Infix (reservedOp "*" >> return OpMul) AssocLeft, Infix (reservedOp "/" >> return OpDiv) AssocLeft], 
 [Infix (reservedOp "+" >> return OpAdd) AssocLeft, Infix (reservedOp "-" >> return OpSub) AssocLeft]]

parseVar :: Parser Variable
parseVar = liftM (Var.(map toLower)) identifier

parseNum :: Parser Expr
parseNum = do
            num <- integer
            denom <- option 1 (reservedOp "/" >> integer)
            return (Fraction num denom)

parseFunctionCall :: Parser Expr
parseFunctionCall = do
                      fn <- parseVar
                      exps <- many (brackets parseExpr)
                      return (FunCall fn exps)

parseExpr :: Parser Expr
parseExpr = buildExpressionParser opa (parens parseExpr <|> parseNum <|> parseFunctionCall)

-- data Stmt = Def Variable [Expr] | Assign [(Variable, Expr)] | Do Integer Stmt | Query [Expr]
--   %Variable% is function of %N%: %Expression1%,  %Expression2%,  ...,  %ExpressionN%,  %Expression0%.
--   %Variable% is %Expression%. 
--   Assign %Expression1% to %Variable1% [ AND %Expression2% to %Variable2% ... ]!
--   do {Expression} %Assign values(s) to variable(s)%!
--   what is %Function1 call% [AND %Variable1% [ AND %Function2 call% ... ] ]?
parseAssignment :: Parser Stmt
parseAssignment = reserved "assign" >> liftM Assign (sepBy1 (parseExpr >>= (\e->(reserved "to" >> parseVar >>=(\v->return (v, e))))) (reserved "and")) >>= (\r->(reservedOp "!" >> return r))
                    
parseDo :: Parser Stmt
parseDo = reserved "do" >> (braces integer) >>= (\n -> (parseAssignment >>= (\e->return (Do n e))))

parseQuery :: Parser Stmt
parseQuery = reserved "what" >> reserved "is" >> liftM Query (sepBy1 parseExpr (reserved "and")) >>= (\r->(reservedOp "?" >> return r))

parseFunction :: Parser [Expr]
parseFunction = reserved "function" >> reserved "of" >> integer >> reservedOp ":" >> commaSep1 parseExpr

parseDef :: Parser Stmt
parseDef = parseVar >>= (\v -> (reserved "is" >> (parseFunction <|> liftM (\x->[x]) parseExpr) >>= (\e -> (reservedOp "." >> return (Def v e)))))

parseStmt :: Parser [Stmt]
parseStmt = whiteSpace >> many1 (parseAssignment <|> parseDo <|> parseQuery <|> parseDef) >>= (\x->(eof >> return x))

-- evalExp :: AExp -> Map.Map String Integer -> Integer
-- evalExp (Var x) vars = fromJust (Map.lookup x vars)
-- evalExp (Num x) vars = x
-- evalExp (OpAdd e1 e2) vars = (evalExp e1 vars) + (evalExp e2 vars)
-- evalExp (OpSub e1 e2) vars = (evalExp e1 vars) - (evalExp e2 vars) 
-- evalExp (OpMul e1 e2) vars = (evalExp e1 vars) * (evalExp e2 vars) 
-- evalExp (OpDiv e1 e2) vars = (evalExp e1 vars) `div` (evalExp e2 vars) 
-- 
-- evalCond :: BExp -> Map.Map String Integer -> Bool
-- evalCond (Boolean x) vars = x
-- evalCond (OpAnd e1 e2) vars = ((evalCond e1 vars) && (evalCond e2 vars))
-- evalCond (OpOr e1 e2) vars = ((evalCond e1 vars) || (evalCond e2 vars))
-- evalCond (OpGreater e1 e2) vars = (evalExp e1 vars) > (evalExp e2 vars)
-- evalCond (OpSmaller e1 e2) vars = (evalExp e1 vars) < (evalExp e2 vars)
-- 
-- printVars :: [(String, Integer)] -> IO()
-- printVars [] = return()
-- printVars ((x, v):cs) = putStrLn(x++" "++(show v)) >> printVars cs
-- 
-- runStmt :: Stmt -> Map.Map String Integer -> Map.Map String Integer
-- runStmt (Assign (Var x) e) vars = Map.insert x (evalExp e vars) vars
-- runStmt (If cond s1 s2) vars | evalCond cond vars = runStmts s1 vars
--                              | otherwise = runStmts s2 vars
-- runStmt (While cond s) vars | evalCond cond vars = runStmt (While cond s) (runStmts s vars)
--                             | otherwise = vars
-- 
-- runStmts :: [Stmt] -> Map.Map String Integer -> Map.Map String Integer
-- runStmts [] vars = vars
-- runStmts (c:cs) vars = runStmts cs vars1
--   where vars1 = runStmt c vars
-- 
process :: String->IO()
process s = case parse parseStmt "whileparser" s of
	Left err -> print err
	Right stmt -> print stmt -- printVars $ Map.toAscList $ runStmts stmt Map.empty

main = getContents >>= process
