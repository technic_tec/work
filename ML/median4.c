/*
 * calculates the median of co-ordinates for our linknew
 * try defining triangle by diagonal line
 * find median absolute deviation of Xs, and of Ys
 * take intersections(?) as diagonal
 *
 * used for FIT2
 *
 * read in super-data format
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define FALSE 0
#define TRUE (!FALSE)

#define ABS(x) ((x) < 0 ? (-(x)) : (x))

#define LISTS 1
#define LINES 2
#define ARCS 3
#define ENDL 4
#define ENDF 5

#define MAX_POINTS 100000

int datax[MAX_POINTS],datay[MAX_POINTS];
int store_x_diffs[MAX_POINTS],store_y_diffs[MAX_POINTS];
FILE *fp;
char *filename;
int total_no = 0;

main(argc,argv)
int argc;
char *argv[];
{
   char file_type[50];
   int i;
   int end_of_file;
   int med_x,med_y;
   int diff_x,diff_y;
   int med_dev_x,med_dev_y;
   float factorx = 6;
   float factory = 6;
   float x1,y1,x2,y2,xx;

   filename = argv[1];

   if (argc != 3) {
       printf("Usage: %s filename median_factor\n",argv[0]);
       exit(-1);
   }

   factorx = factory = atof(argv[2]);
   if ((fp = fopen(filename,"r")) == NULL) {
       printf("cant open %s\n",filename);
       exit(-1);
   }
   /* read magic word for format of file */
   fscanf(fp,"%s\n",file_type);
   i = strcmp2(file_type,"super");
   if (i != 0) {
       printf("not super data file - aborting\n");
       exit(-1);
   }

   do {
      read_line(&end_of_file);
   } while (!end_of_file);

   sort(total_no,datax);
   sort(total_no,datay);

   med_x = datax[total_no/2];
   med_y = datay[total_no/2];

/*
printf("med X %d med Y %d\n",med_x,med_y);
*/
   for (i = 1; i <= total_no; i++) {
       diff_x = ABS(med_x - datax[i]);
       diff_y = ABS(med_y - datay[i]);
       store_x_diffs[i] = diff_x;
       store_y_diffs[i] = diff_y;
   }

   sort(total_no,store_x_diffs);
   sort(total_no,store_y_diffs);

   med_dev_x = store_x_diffs[total_no/2];
   med_dev_y = store_y_diffs[total_no/2];

/*
printf("MX %d MY %d\n",med_dev_x,med_dev_y);
*/

/*
   printf("super\nlist: 0\n");
   printf("line: 0.0 %d %d %d %d\n",0,med_y,500,med_y);
   printf("line: 0.0 %d %d %d %d\n",0,med_y+3*med_dev_y,500,med_y+3*med_dev_y);
   printf("line: 0.0 %d %d %d %d\n",med_x+3*med_dev_x,0,med_x+3*med_dev_x,500);
   printf("endl:\nendf:\n");
*/

   /**
   printf("%f %f %f %f\n",
        0.0,med_y+factory*med_dev_y,med_x+factorx*med_dev_x,(float)med_y);
   **/

/*
printf("MED Y %d\n",med_y);
printf("MAD Y %d\n",med_dev_y);
printf("MED X %d\n",med_x);
printf("MAD X %d\n",med_dev_x);
*/
   printf("%f %f %f %f\n",
        0.0,med_y+factory*med_dev_y,
        med_x+factorx*med_dev_x,(float)med_y+factory*med_dev_y/2);

   y1 = med_y+factory*med_dev_y;
   x2 = med_x+factorx*med_dev_x;
   y2 = med_y+factory*med_dev_y/2.0;
   /***
   xx = y1 * (x1 - x2) / (y2 - y1);
   printf("%f\n",xx);
   ***/
}

read_line(end_of_file)
int *end_of_file;
{
    int i,j,l;
    float f;
    int d;
    char data_type[40],ch;

    do {
        i = -1;
        do {
            fscanf(fp,"%c",&ch);
            data_type[++i] = ch;
        } while(ch != ':');
        data_type[++i] = '\0';
        j = curve_type(data_type);

        if (j == LISTS) {
            fscanf(fp,"%d\n",&l);
        }
        else if (j == LINES) {
            total_no++;
            fscanf(fp,"%f %d %d %d %d\n",
                &f,&d,&d,&datax[total_no],&datay[total_no]);
            if (total_no > MAX_POINTS) {
                printf("ERROR: too many points\n");
                exit(-1);
            }
        }
        else if (j == ARCS) {
            printf("warning! ARC in data - ignoring\n");
            exit(-1);
        }
        else if (j == ENDL) {  /* read to end of line */
            fscanf(fp,"\n");
        }
    } while((j != ENDL) && (j != ENDF));
    if (j == ENDF)
        *end_of_file = TRUE;
    else
        *end_of_file = FALSE;
}

int curve_type(array)
char array[50];
{
    int i,j;

    if ((j = strcmp2(array,"list:")) == 0)
        i = 1;
    else if ((j = strcmp2(array,"line:")) == 0)
        i = 2;
    else if ((j = strcmp2(array,"arc:")) == 0)
        i = 3;
    else if ((j = strcmp2(array,"endl:")) == 0)
        i = 4;
    else if ((j = strcmp2(array,"endf:")) == 0)
        i = 5;
    return(i);
}

int strcmp2(s,t)
char s[],t[];
{
    int i;

    i = 0;
    while(s[i] == t[i])
        if (s[i++] == '\0')
            return(0);
    return(s[i] - t[i]);
}

/* from numerical recipes in C */
sort(n,ra)
int n;
int ra[];
{
   int l,j,ir,i;
   float rra;

   l=(n >> 1)+1;
   ir=n;
   for (;;) {
      if (l > 1)
         rra=ra[--l];
      else {
         rra=ra[ir];
         ra[ir]=ra[1];
         if (--ir == 1) {
            ra[1]=rra;
            return;
         }
      }
      i=l;
      j=l << 1;
      while (j <= ir) {
         if (j < ir && ra[j] < ra[j+1]) ++j;
         if (rra < ra[j]) {
            ra[i]=ra[j];
            j += (i=j);
         }
         else j=ir+1;
      }
      ra[i]=rra;
   }
}

