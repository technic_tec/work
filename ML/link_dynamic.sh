#!/bin/csh -f
# C shell script to run automatic thresholding
# Paul Rosin & Svetha Venkatesh - 1993
#
# described in:
# Svetha Venkatesh & Paul L. Rosin,
# Dynamic threshold determination by local and global edge evaluation,
# Graphical Models & Image Processing,
# Vol. 75, No. 2, pp. 146-160, 1995.

if ($#argv != 2) then
	echo Usage: $0 "edge-map output-file"
	exit
endif

set im = $1
set fac = 6

./link_dynamic -i $im -o /tmp/link_dynamic_data
./median4 /tmp/link_dynamic_data $fac >! /tmp/link_dynamic_params
set x = `cat /tmp/link_dynamic_params`
./link_dynamic -d $x[1] $x[2] $x[3] $x[4] -i $im -o $2
