/* read in image and dynamically allocate memory for it
 *
 * modified to use fopen with "rb" and "wb" instead of just "r"/"w"
 * hopefully this will make it more portable w.r.t. PC platforms
 *
 * check for correct code (P5) in input image
 *
 * copes with MATLAB style headers
 * and blank lines after comment
 *
 * Paul Rosin
 * July 2012
 * Cardiff
 */

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>

#define ALPHA

unsigned char ** malloc_char_image();
int ** malloc_int_image();
long ** malloc_long_image();
float ** malloc_float_image();
double ** malloc_double_image();

extern FILE *fp_in;

unsigned char ** malloc_char_image(xres,yres)
int xres,yres;
{
    unsigned char ** image;
    int j;

    image = (unsigned char **) malloc( sizeof(unsigned char *) * yres);
    if (image == NULL)
        message_exit("char");

    for (j=0;j<yres;j++) {
        image[j] = (unsigned char *) malloc(sizeof(unsigned char) * xres);
        if (image[j] == NULL)
            message_exit("char");
    }
    return(image);

}

int ** malloc_int_image(xres,yres)
int xres,yres;
{
    int ** image;
    int j;

    image = (int **) malloc( sizeof(int *) * yres);
    if (image == NULL)
        message_exit("int");

    for (j=0;j<yres;j++) {
        image[j] = (int *) malloc(sizeof(int) * xres);
        if (image[j] == NULL)
            message_exit("int");
    }
    return(image);
}

float ** malloc_float_image(xres,yres)
int xres,yres;
{
    float ** image;
    int j;

    image = (float **) malloc( sizeof(float *) * yres);
    if (image == NULL)
        message_exit("float");

    for (j=0;j<yres;j++) {
        image[j] = (float *) malloc(sizeof(float) * xres);
        if (image[j] == NULL)
            message_exit("float");
    }
    return(image);
}

long ** malloc_long_image(xres,yres)
int xres,yres;
{
    long ** image;
    int j;

    image = (long **) malloc( sizeof(long *) * yres);
    if (image == NULL)
        message_exit("long");

    for (j=0;j<yres;j++) {
        image[j] = (long *) malloc(sizeof(long) * xres);
        if (image[j] == NULL)
            message_exit("long");
    }
    return(image);
}

double ** malloc_double_image(xres,yres)
int xres,yres;
{
    double ** image;
    int j;

    image = (double **) malloc( sizeof(double *) * yres);
    if (image == NULL)
        message_exit("double");

    for (j=0;j<yres;j++) {
        image[j] = (double *) malloc(sizeof(double) * xres);
        if (image[j] == NULL)
            message_exit("double");
    }
    return(image);
}

message_exit(type)
char *type;
{
    printf("error occurred when allocating %s image memory - aborting\n",type);
    exit(-1);
}

/*
    reads in a PGM format image header
*/
read_pgm_header(filename,width,height)
char *filename;
int *width,*height;
{
    int i,j;
    char str[1000];
    char image_type[1000];
    int depth;

    if ((fp_in = fopen(filename,"rb")) == NULL) {
        printf("cant open %s\n",filename);
        exit(-1);
    }

    /* skip image type and comments in header */
    fgets(str,255,fp_in);
    i = sscanf(str,"%s %d %d %d",image_type,width,height,depth);
    if (i == 4) {
        /*
        printf("MATLAB style header\n");
        printf("width x height = %d x %d\n",*width,*height);
        */
        if (strcmp(image_type,"P5") != 0) {
            fprintf(stderr,"ERROR: image is not correct PGM format\n");
            exit(-1);
        }
    }
    else {
        //printf("regular style header\n");
        sscanf(str,"%s",image_type);
        if (strcmp(image_type,"P5") != 0) {
            fprintf(stderr,"ERROR: image is not correct PGM format\n");
            exit(-1);
        }
        do
            fgets(str,255,fp_in);
        while (str[0] == '#');

        /* read image parameters */
        /* the first line has already been read */
        i = sscanf(str,"%d %d",width,height);
        // cope with blank lines in header after comment
        while (i != 2) {
            fgets(str,255,fp_in);
            i = sscanf(str,"%d %d",width,height);
        }
        fscanf(fp_in,"%d",&depth);
        /* skip CR */
        getc(fp_in);
    }

    if (depth != 255) {
        fprintf(stderr,"ERROR: depth = %d; (instead of 255)\n",depth);
        exit(-1);
    }

    /***
    printf("image size: %d x %d\n",*width,*height);
    ***/
}

/*
    reads in an unsigned char image
*/
read_image_body(image,width,height)
unsigned char **image;
int width,height;
{
    int i,j;

    for (i=0;i<height;i++)
        for (j=0;j<width;j++)
            image[i][j] = (unsigned char) getc(fp_in);
    /* check the image file was the correct size */
    /***
    if (feof(fp_in)) {
        printf("ERROR: premature end of file - image too big?\n");
        exit(-1);
    }
    ***/
    if (getc(fp_in) != EOF) {
        printf("ERROR: extra characters in file - image too small?\n");
        exit(-1);
    }

    fclose(fp_in);
}

/*
    reads in an unsigned char image
*/
read_image(image,filename,width,height)
unsigned char **image;
char *filename;
int width,height;
{
    int i,j;
    FILE *fp_in;

    if ((fp_in = fopen(filename,"rb")) == NULL) {
        printf("cant open %s\n",filename);
        exit(-1);
    }
    for (i=0;i<height;i++)
        for (j=0;j<width;j++)
            image[i][j] = (unsigned char) getc(fp_in);
    /* check the image file was the correct size */
    if (feof(fp_in)) {
        printf("ERROR: premature end of file - image too big?\n");
        exit(-1);
    }
    else
        if (getc(fp_in) != EOF) {
            printf("ERROR: extra characters in file - image too small?\n");
            exit(-1);
        }
    fclose(fp_in);
}

/*
    reads in an unsigned 2 byte image
*/
read_image2(image,filename,width,height)
int **image;
char *filename;
int width,height;
{
    int i,j;
    FILE *fp_in;
    unsigned int i1,i2;

    if ((fp_in = fopen(filename,"rb")) == NULL) {
        printf("cant open %s\n",filename);
        exit(-1);
    }
    for (i=0;i<height;i++)
        for (j=0;j<width;j++) {
            i1 = getc(fp_in);
            i2 = getc(fp_in);
            image[i][j] = i1 + i2 * 256;
        }

    /* check the image file was the correct size */
    if (feof(fp_in)) {
        printf("ERROR: premature end of file - image too big?\n");
        exit(-1);
    }
    else
        if (getc(fp_in) != EOF) {
            printf("ERROR: extra characters in file - image too small?\n");
            exit(-1);
        }
    fclose(fp_in);
}

/*
    writes out an image in PGM format
*/
write_pgm(image,filename,width,height)
unsigned char **image;
char *filename;
int width,height;
{
    FILE *fp_out;
    int i,j;

    if ((fp_out = fopen(filename,"wb")) == NULL) {
        fprintf(stderr,"cant open %s\n",filename);
        exit(-1);
    }

    fprintf(fp_out,"P5\n");
    fprintf(fp_out,"#created by Paul Rosin\n");
    fprintf(fp_out,"%d %d\n",width,height);
    fprintf(fp_out,"255\n");

    /***
    printf("writing image to %s\n",filename);
    ***/

    for (i=0;i<height;i++)
        for (j=0;j<width;j++)
            putc(image[i][j],fp_out);
    fclose(fp_out);
}

/*
    writes out an unsigned char image
*/
write_image(image,filename,width,height)
unsigned char **image;
char *filename;
int width,height;
{
    int i,j;
    FILE *fp_out;

    if ((fp_out = fopen(filename,"wb")) == NULL) {
        printf("cant open %s\n",filename);
        exit(-1);
    }
    
    for (i=0;i<height;i++)
        for (j=0;j<width;j++)
            putc(image[i][j],fp_out);
    fclose(fp_out);
}

free_char_image(image,yres)
char **image;
int yres;
{
    int j;

#ifdef ALPHA
    for (j=0;j<yres;j++)
        free(image[j]);
    free((char *)image);
#else
    for (j=0;j<yres;j++) {
        if (free(image[j]) == 0)
            message_exit_free("char");
    }
    if (free((char *)image) == 0)
        message_exit_free("char");
#endif
}

free_int_image(image,yres)
int **image;
int yres;
{
    int j;

#ifdef ALPHA
    for (j=0;j<yres;j++)
        free((char *)image[j]);
    free((char *)image);
#else
    for (j=0;j<yres;j++) {
        if (free((char *)image[j]) == 0)
            message_exit_free("int");
    }
    if (free((char *)image) == 0)
        message_exit_free("int");
#endif
}

free_double_image(image,yres)
double **image;
int yres;
{
    int j;

#ifdef ALPHA
    for (j=0;j<yres;j++)
        free((char *)image[j]);
    free((char *)image);
#else
    for (j=0;j<yres;j++) {
        if (free((char *)image[j]) == 0)
            message_exit_free("double");
    }
    if (free((char *)image) == 0)
        message_exit_free("double");
#endif
}

message_exit_free(type)
char *type;
{
    printf("error occurred when de-allocating %s image memory - aborting\n",type);
    exit(-1);
}
