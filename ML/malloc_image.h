#include <malloc.h>

extern unsigned char ** malloc_char_image();
extern int ** malloc_int_image();
extern long ** malloc_long_image();
extern float ** malloc_float_image();
extern double ** malloc_double_image();
