#1<=A<=30,  1<=B<=10^8,  k=[log10(B)]+2
#U={x*y|1<=x<=A, 1<=y<=B}
#V={x^y|1<=x<=A, 1<=y<=B}
#cx = |U|,  sx = sum(U)
#cy = |V|,  sy = sum(V)

from fractions import gcd

def enum_prod(v, lp, vh):
    if v > vh:
        return
    if len(lp) == 0:
        yield v
        return
    p = lp[0]
    for x in enum_prod(v, lp[1:], vh):
        yield x
    for x in enum_prod(-v*p/gcd(v,p), lp[1:], vh):
        yield x
    
def count_sum_range(l, h, la):
    i = 0
    while i < len(la):
        la[i+1:] = filter(lambda y: y%la[i]>0, la[i+1:])
        i += 1
    tc = 0
    ts = 0
    for p in enum_prod(1, la, h):
        if p == 1:
            continue
        sp = -1
        if p < 0:
            p = -p
            sp = -sp
        ch, cl = h/p, l/p
        c = ch-cl
        s = (ch*(ch+1)/2-cl*(cl+1)/2)*p
        tc += sp*c
        ts += sp*s
    return (tc, ts)

def f(A, B):
    # sx*10^k*cy + sy*cx
    k = len(str(B))+1
    if B >= 32:
        ub = (B & ~31)
        u0 = set([x^y for x in range(1, A+1) for y in range(1, 32)])
        u1 = set([x^y for x in range(1, A+1) for y in range(ub, B+1)])
        cy = ub-32 + len(u0) + len(u1)
        sy = ub*(ub-1)/2 - 32*31/2 + sum(u0) + sum(u1)
    else:
        u0 = set([x^y for x in range(1, A+1) for y in range(1, B+1)])
        cy = len(u0)
        sy = sum(u0)
    # vk = {x | (k-1)*B < x <= k*B && d|x for some k<=d<=A}
    # v = v1 | v2 | ... | vA
    cx, sx = B, B*(B+1)/2
    for a in range(2, A+1):
        cx1, sx1 = count_sum_range((a-1)*B, a*B, range(a, A+1))
        cx += cx1
        sx += sx1
    #print cx, sx, cy, sy
    return sx * (10**k) * cy + sy * cx

A, B = map(int, raw_input().split())
print f(A, B)
