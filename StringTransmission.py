P = 1000000007

def solve(n, k, s, loop):
        unit = n / loop
        c0 = [s[i:n:unit].count('0') for i in range(unit)]
        c1 = [s[i:n:unit].count('1') for i in range(unit)]
        r = [[0] * (k+1) for i in range(unit+1)]
        r[0][0] = 1
        for i in range(1, unit+1):
                for j in range(k+1):
                        r[i][j] = 0
                        if j >= c0[i-1]:
                                r[i][j] += r[i-1][j-c0[i-1]]
                        if j >= c1[i-1]:
                                r[i][j] += r[i-1][j-c1[i-1]]
                        if r[i][j] >= P:
                                r[i][j] -= P
        res = sum(r[unit]) % P
        return res

def main():
        t = int(raw_input())
        for _ in range(t):
                n, k = map(int, raw_input().split())
                s = raw_input().strip()
                fcs = list()
                x = n
                for i in range(2, x+1):
                        if x % i == 0:
                                fcs.append(i)
                                while x % i == 0:
                                        x /= i
                m = len(fcs)
                res = 0
                for i in range(1<<m):
                        x = 1
                        for j, u in enumerate(fcs):
                                if (i & (1<<j)):
                                        x *= -u
                        if x < 0:
                                res -= solve(n, k, s, -x)
                                if res < 0:
                                        res += P
                        else:
                                res += solve(n, k, s, x)
                                if res >= P:
                                        res -= P
                print res

if __name__ == '__main__':
        main()

