def de_bruijn(k, n):
    """De Bruijn Sequence for alphabet size k 
    and subsequences of length n."""
    a = [0] * k * n
    sequence = []
    def db(t, p):
        if t > n:
            if n % p == 0:
                for j in range(1, p + 1):
                    sequence.append(a[j])
        else:
            a[t] = a[t - p]
            db(t + 1, p)
            for j in range(a[t - p] + 1, k):
                a[t] = j
                db(t + 1, t)
    db(1,1)
    print n, sequence
    s = ''.join(map(str, sequence))
    return s + s[:n-1]

r = ['']
for i in range(1, 7):
    r.append(de_bruijn(10, i))

n = int(raw_input())
while n > 0:
    print r[n]
    n = int(raw_input())
