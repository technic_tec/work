#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define N 100000

// xr+ixi = (cr+ici)*(br+ibi)+d, d^2<br^2+bi^2
int ComplexDivmod(int xr, int xi, int br, int bi, int &cr, int &ci)
{
    int d = -1;
    // xr-cr*br+ci*bi = d
    // xi-cr*bi-ci*br = 0
    if(bi == 0) {
        // xr-cr*br = d
        // xi-ci*br = 0
        if(xi%br) return -1;
        d = xr % abs(br);
        if(d < 0)
            d += abs(br);
        cr = (xr-d)/br;
        ci = xi/br;
    } else if(br == 0) {
        // xr+ci*bi = d
        // xi-cr*bi = 0
        if(xi%bi) return -1;
        d = xr % abs(bi);
        if(d < 0)
            d += abs(bi);
        ci = (d-xr)/bi;
        cr = xi/bi;
    } else {
        // d*br = xr*br-cr*br^2+(xi-cr*bi)*bi = (xr*br+xi*bi)-cr*(br^2+bi^2)
        // d*bi = xr*bi-br*(xi-ci*br)+ci*bi^2 = (xr*bi-xi*br)+ci*(br^2+bi^2)
        int m = br*br + bi*bi;
        int vr = xr*br + xi*bi;
        int vi = xr*bi - xi*br;
        d = -1;
        for(int i = 0; i*i < m; i++)
            if((i*br-vr)%m==0 && (i*bi-vi)%m==0) {
                d = i;
                cr = (vr-d*br)/m;
                ci = (d*bi-vi)/m;
            }
    }
    return d;
}

int ComplexBaseB(int xr, int xi, int br, int bi, int a[])
{
    int n = 0;
    if(br == 0 && bi==0)
        return -1;
    if(xr == 0 && xi == 0) {
        a[0] = 0;
        return 0;
    }
    for(n = 0; n <= 101 && (xr || xi); n++) {
        int cr, ci;
        a[n] = ComplexDivmod(xr, xi, br, bi, cr, ci);
        if(a[n] < 0) return -1;
        xr = cr; xi = ci;
    }
    if(n > 101) return -1;
    return (n-1);
}

int main()
{
    int t, xr, xi, br, bi, n, a[110];
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d%d%d", &xr, &xi, &br, &bi);
        n = ComplexBaseB(xr, xi, br, bi, a);
        if(n < 0)
            printf("The code cannot be decrypted.\n");
        else
            for(int i = n; i >= 0; i--)
                printf("%d%c", a[i], i?',':'\n');
    }
    return 0;
}
