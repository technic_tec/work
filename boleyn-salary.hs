import Control.Monad
import Data.Tree
import Data.List
import Data.Array
import qualified Data.Map as Map

query :: [Int]->Int

main :: IO()
main = do
  s <- getLine
  let [n, q] = map read (words s)
  es <- replicateM (n-1) getLine
  let e = map ((\x->(x!!1, x!!0)).(map read.words)) es
      g = buildG (1, n) e
  s <- getLine
  let sa = map read (words s)
  replicateM q (getLine >>= putStrLn.show.query.(map read).words)
