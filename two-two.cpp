#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <deque>
using namespace std;

#define N 100100
#define K 250
#define NC 10
#define C2I(x) (x-'0')

class AhoCorasick {
    public:
        struct Node {
            char c;
            Node *lnk[NC];
            Node *prelink;
            Node *parent;
            int wt;
            Node(char ch = 0) {
                c = ch;
                wt = 0;
                memset(lnk, 0, sizeof(lnk));
                prelink = parent = NULL;
            }
        };
        AhoCorasick() {
            m_root = new Node;
            m_nil = new Node;
            for(int i = 0; i < NC; i++)
                m_nil->lnk[i] = m_root;
            m_root->parent = m_nil->parent = m_nil;
            m_inited = false;
        }
        ~AhoCorasick() {
            release(m_root);
            delete m_nil;
            m_root = m_nil = NULL;
            m_inited = false;
        }
        void insert_pattern(const char *s) {
            Node *p = m_root;
            for(; *s; s++) {
                if(p->lnk[C2I(*s)] == NULL) {
                    p->lnk[C2I(*s)] = new Node(*s);
                    p->lnk[C2I(*s)]->parent = p;
                }
                p = p->lnk[C2I(*s)];
            }
            assert(p->wt == 0);
            p->wt++;
            m_inited = false;
        }
        void construct() {
            assert(!m_inited);
            deque<Node *> q;
            m_root->prelink = m_nil->prelink = m_nil;
            for(int i = 0; i < NC; i++)
                if(m_root->lnk[i])
                    q.push_back(m_root->lnk[i]);
            while(!q.empty()) {
                Node *p = q.front();
                q.pop_front();
                Node *pp = p->parent->prelink;
                assert(pp);
                while(!pp->lnk[C2I(p->c)])
                    pp = pp->prelink;
                p->prelink = pp->lnk[C2I(p->c)];
                assert(p->prelink != p);
                p->wt += p->prelink->wt;
                for(int i = 0; i < NC; i++)
                    if(p->lnk[i])
                        q.push_back(p->lnk[i]);
            }
            m_inited = true;
        }
        int count_matching(const char *s) const {
            int c = 0;
            const Node *p = m_root;
            for(; *s; s++) {
                if(!p->lnk[C2I(*s)]) {
                    while(!p->lnk[C2I(*s)])
                        p = p->prelink;
                }
                p = p->lnk[C2I(*s)];
                c += p->wt;
            }
            return c;
        }
    private:
        void release(Node* &p) {
            if(p == NULL)
                return;
            for(int i = 0; i < NC; i++)
                release(p->lnk[i]);
            p->prelink = NULL;
            delete p;
            p = NULL;
        }
        Node *m_root;
        Node *m_nil;
        bool m_inited;
};

char *dbl(char *p) {
    char *pp = p;
    while(*pp)++pp;
    --pp;
    int c = 0;
    while(pp >= p) {
        *pp = *pp + *pp - '0' + c;
        c = (*pp > 0x39);
        *pp -= c*10;
        --pp;
    }
    if(c)
        *--p = '1';
    return p;
}
char buf[N];
int main()
{
    char s[K], *p;
    AhoCorasick aho;
    s[K-1] = 0; s[K-2] = '1'; p = &s[K-2];
    aho.insert_pattern(p);
    for(int i = 1; i <= 800; i++) {
        p = dbl(p);
        //printf("%s\n", p);
        aho.insert_pattern(p);
    }
    aho.construct();
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%s", buf);
        printf("%d\n", aho.count_matching(buf));
    }
    return 0;
}

