def f(v):
    #Considering the output (32*n) bits against input:
    #The first k bits of all numbers exactly matches input.
    #The kth bit of first i numbers exactly matches input.
    #kth bit of ith number is less then input (0 vs 1)
    orf = reduce(lambda x, y: x|y, v)
    xorf = reduce(lambda x, y: x^y, v)
    c = 0
    for k in range(31, -1, -1):
        if ((flg>>(32-k)) & 1):
            break
    return c
