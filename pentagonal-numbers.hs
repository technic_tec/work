import Control.Monad

pentagonal :: Integer->Integer
pentagonal n = n*(3*n-1) `div` 2

main :: IO()
main = getLine >>= return.read >>= (\n -> replicateM n getLine) >>= return.map (pentagonal.read) >>= mapM_ (putStrLn.show)
