import struct, hashlib

# s specifies the per-round shift amounts
s = [ 7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
      5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
      4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
      6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21]

# Use binary integer part of the sines of integers (Radians) as constants
K = [ 0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
      0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
      0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
      0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
      0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
      0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
      0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
      0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
      0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
      0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
      0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
      0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
      0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
      0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
      0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
      0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391]

BLKSZ = 64
    
# leftrotate function definition
def leftrotate (x, c):
    return ((x << c) | (x >> (32-c))) & 0xffffffff;

class Md5(object):
    def __init__(self, msglen = 0, a0=0x67452301L, b0=0xefcdab89L, c0=0x98badcfeL, d0=0x10325476L):
        self.msglen, self.a0, self.b0, self.c0, self.d0 = msglen, a0, b0, c0, d0
        self.buf = ''

    def update(self, msg):
        # Process the message in successive 512-bit chunks:
        self.buf += msg
        self.msglen += len(msg)
        self.buf, self.a0, self.b0, self.c0, self.d0 = self.process_buf(self.buf, self.a0, self.b0, self.c0, self.d0)

    def hexdigest(self):
        pad = '\x80'
        if len(self.buf)+len(pad) <= 56:
            pad += '\x00'*(56-len(self.buf)-len(pad))
        else:
            pad += '\x00'*(120-len(self.buf)-len(pad))
        pad += struct.pack('<Q', 8*self.msglen)
        buf, a0, b0, c0, d0 = self.process_buf(self.buf+pad, self.a0, self.b0, self.c0, self.d0)
        return struct.pack('<LLLL', a0, b0, c0, d0).encode('hex')

    def process_buf(self, buf, a0, b0, c0, d0):
        whole_chunks = len(buf)/BLKSZ*BLKSZ
        #print 'process_buf('+buf.encode('string_escape')+')'
        for ii in range(0, whole_chunks, BLKSZ):
            M = struct.unpack('<'+'L'*16, buf[ii:ii+BLKSZ])
            # Initialize hash value for this chunk:
            A, B, C, D = a0, b0, c0, d0
            # Main loop:
            for i in range(BLKSZ):
                #print '%2d: %08x %08x %08x %08x' % (i, A, B, C, D)
                if i <= 15:
                    F = (B & C) | ((~B) & D)
                    g = i
                elif i <= 31:
                    F = (D & B) | ((~D) & C)
                    g = (5*i + 1) & 0xf
                elif i <= 47:
                    F = B ^ C ^ D
                    g = (3*i + 5) & 0xf
                else:
                    F = C ^ (B | (~D))
                    g = (7*i) & 0xf
                A, B, C, D = D, (B + leftrotate((A + F + K[i] + M[g])&0xffffffff, s[i]))&0xffffffff, B, C
            # Add this chunk's hash to result so far:
            a0, b0, c0, d0 = (a0+A)&0xffffffff, (b0+B)&0xffffffff, (c0+C)&0xffffffff, (d0+D)&0xffffffff
        return (buf[whole_chunks:], a0, b0, c0, d0)

class Md5Ext(object):
    def __init__(self, secret_len, msg, sig, ext):
        if len(sig) != 32:
            raise ValueError
        self.msglen = secret_len + len(msg)
        self.msg = msg
        padsz = (56-self.msglen-1) % 64
        if padsz < 0:
            padsz += 64
        #self.msg += '\x80' + '\x00'*padsz + struct.pack('<Q', 8*self.msglen)
        self.msglen += 9+padsz
        a0, b0, c0, d0 = struct.unpack('<LLLL', sig.decode('hex'))
        md5 = Md5(self.msglen, a0, b0, c0, d0)
        md5.update(ext)
        self.msg += ext
        self.hexdigest = md5.hexdigest()

def test(s):
    md5 = Md5()
    md5.update(s)
    print s, md5.hexdigest(), hashlib.md5(s).hexdigest()

#test('hello')
#test('secret?request=sample')
t = int(raw_input())
for _ in range(t):
    secret_len, msg, sig, ext = raw_input().split()
    secret_len = int(secret_len)
    cr = Md5Ext(secret_len, msg, sig, ext)
    print cr.msg, cr.hexdigest
