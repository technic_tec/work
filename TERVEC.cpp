#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;

int n = 20;
char fmt[20];
vector<int> v;
int count1s(int x)
{
    int c = 0;
    while(x) {
        ++c;
        x &= x-1;
    }
    return c;
}
void search(int k)
{
    if(k == n) {
        for(int i = 0; i < n; i++)
            printf(fmt,v[i]);
        printf("\n");
        fflush(stdout);
        return;
    }
    for(int i = (k==1?(1<<n):v[k-1])-1; i >= 0; i--) {
        if(count1s(i) == n/2) {
            bool valid = true;
            for(int j = 1; j < k; j++)
                if(count1s(i^v[j]) != n/2) {
                    valid = false;
                    break;
                }
            if(valid) {
                v.push_back(i);
                search(k+1);
                v.pop_back();
            }
        }
    }
}
int main(int argc, char *argv[]) {
	// your code goes here
  n = 20;
  if(argc == 2)
    n = atoi(argv[1]);
  sprintf(fmt, "%%0%dx\n", n/4);
	v.push_back(0);
  int blk = (1<<(n/4))-1;
  v.push_back(((blk<<(n/4))|blk)<<(n/2));
  v.push_back((blk<<(3*n/4))|(blk<<(n/4)));
  search(3);
	return 0;
}
