/*
 * =====================================================================================
 *
 *       Filename:  DEVGOSTR.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年04月05日 10时00分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <string>
#include <set>
#include <iostream>
using namespace std;

#define P 1000000007
#define N 100000000

class AntiArith {
  public:
    AntiArith() {}
    bool empty() const { return m_v.empty(); }
    int size() const { return m_v.size(); }
    bool insert(int x) {
      assert (m_v.empty() || x > m_v.back());
      if(forbid.count(x) > 0)
        return false;
      ops.push_back(make_pair(x, vector<int>()));
      for(int i = 0; i < m_v.size(); i++)
        if(forbid.count(2*x-m_v[i]) == 0) {
          forbid.insert(2*x-m_v[i]);
          ops.back().second.push_back(2*x-m_v[i]);
        }
      m_v.push_back(x);
      return true;
    }
    int pop() {
      assert(!ops.empty() && !m_v.empty() && ops.back().first == m_v.back());
      vector<int> &newset = ops.back().second;
      for(int i = 0; i < newset.size(); i++)
        forbid.erase(newset[i]);
      ops.pop_back();
      int x = m_v.back();
      m_v.pop_back();
      return x;
    }
    const set<int> &forbidden_set() const { return forbid; }
    void print() const {
      printf("{");
      for(int i = 0; i < m_v.size(); i++)
        printf("%s%d", i?",":"", m_v[i]);
      printf("}\n");
    }
  private:
    vector<int> m_v;
    set<int> forbid;
    vector<pair<int, vector<int> > > ops;
};

vector<vector<string> > str1, str2, str3;
void enumerate(AntiArith &s1, AntiArith &s2, AntiArith &s3, int x, string s = "") {
  if(str3.size() < x)
    str3.resize(x);
  str3[x-1].push_back(s);
  if(s3.empty()) {
    if(str2.size() < x)
      str2.resize(x);
    str2[x-1].push_back(s);
    if(s2.empty()) {
      if(str1.size() < x)
        str1.resize(x);
      str1[x-1].push_back(s);
    }
  }
  if(s1.insert(x)) {
//    vector<int> inter(s1.forbidden_set().size()), inter3(s1.forbidden_set().size());
//    vector<int>::iterator it = set_intersection(s1.forbidden_set().begin(), s1.forbidden_set().end(), s2.forbidden_set().begin(), s2.forbidden_set().end(), inter.begin());
//    vector<int>::iterator it3 = set_intersection(inter.begin(), it, s3.forbidden_set().begin(), s3.forbidden_set().end(), inter3.begin());
//    if(it3 == inter3.begin())
      enumerate(s1, s2, s3, x+1, s+"x");
    s1.pop();
  }
  if(!s1.empty() && s2.insert(x)) {
//    vector<int> inter(s1.forbidden_set().size()), inter3(s1.forbidden_set().size());
//    vector<int>::iterator it = set_intersection(s1.forbidden_set().begin(), s1.forbidden_set().end(), s2.forbidden_set().begin(), s2.forbidden_set().end(), inter.begin());
//    vector<int>::iterator it3 = set_intersection(inter.begin(), it, s3.forbidden_set().begin(), s3.forbidden_set().end(), inter3.begin());
//    if(it3 == inter3.begin())
      enumerate(s1, s2, s3, x+1, s+"y");
    s2.pop();
  }
  if(!s1.empty() && !s2.empty() && s3.insert(x)) {
//    vector<int> inter(s1.forbidden_set().size()), inter3(s1.forbidden_set().size());
//    vector<int>::iterator it = set_intersection(s1.forbidden_set().begin(), s1.forbidden_set().end(), s2.forbidden_set().begin(), s2.forbidden_set().end(), inter.begin());
//    vector<int>::iterator it3 = set_intersection(inter.begin(), it, s3.forbidden_set().begin(), s3.forbidden_set().end(), inter3.begin());
//    if(it3 == inter3.begin())
      enumerate(s1, s2, s3, x+1, s+"z");
    s3.pop();
  }
}
int hamming(const string &x, const string &y)
{
  int c = 0;
  for(int i = 0; i < x.size(); i++)
    c += (x[i] != y[i]);
  return c;
}
string sub(const string &x, const string &val) {
  string r = "";
  for(int i = 0; i < x.size(); i++)
    r += val[x[i]-'x'];
  return r;
}
int enum_vals(int n, int k, vector<string> &ass) {
  string s = "";
  for(char x = 'a'; x < 'a'+n; x++) {
    if(k == 1) {
      ass.push_back(s+x);
      continue;
    }
    for(char y = 'a'; y < 'a'+n; y++) {
      if(y == x) continue;
      if(k == 2) {
        ass.push_back(s+x+y);
        continue;
      }
      for(char z = 'a'; z < 'a'+n; z++) {
        if(z == x || z == y) continue;
        ass.push_back(s+x+y+z);
      }
    }
  }
  return ass.size();
}
int main()
{
  AntiArith s1, s2, s3;
  str1.clear();
  str2.clear();
  str3.clear();
  enumerate(s1, s2, s3, 1);
  assert(s1.empty() && s2.empty() && s3.empty());

  int t;
  scanf("%d", &t);
  while(t--) {
    int a, k;
    char s[100];
    scanf("%d%d%s", &a, &k, s);
    int n = strlen(s);
    int cnt = 0;
    vector<string> empty;
    vector<string> &str = empty;
    int nv = 0;
    if(a == 1) {
      str = n < str1.size() ? str1[n] : empty;
    } else if(a == 2) {
      str = n < str2.size() ? str2[n] : empty;
    } else {
      str = n < str3.size() ? str3[n] : empty;
    }
    for(int i = 0; i < str.size(); i++) {
      char cm = *max_element(str[i].begin(), str[i].end());
      vector<string> ass;
      int nv = enum_vals(a, cm-'w', ass);
      for(int j = 0; j < nv; j++) {
        string sr = sub(str[i], ass[j]);
        if(hamming(sr, s) <= k)
          cnt++;
      }
    }
    printf("%d\n", cnt);
  }
  return 0;
}
