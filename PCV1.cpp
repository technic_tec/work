/*
 * =====================================================================================
 *
 *       Filename:  PCV1.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年06月21日 15时55分22秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 10000000
//
//Problem 3 Consider the fraction, n/d, where n and d are positive integers. If n < d and HCF(n,d)=1, it is called a reduced proper fraction. If we list the
//set of reduced proper fractions for d <= 8 in ascending order of size, we get: 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/
//7, 3/4, 4/5, 5/6, 6/7, 7/8 It can be seen that there are 3 fraction between 1/3 and 1/2. How many fractions lie between 1/3 and 1/2 in the sorted set of
//reduced proper fractions for d <= 10,000?
//
int q[N][4], hd, tl;
int rpf(int na, int da, int nb, int db, int d)
{
    int c = 0;
    q[0][0] = na; q[0][1] = da; q[0][2] = nb; q[0][3] = db;
    hd = 0; tl = 1;
    while(hd < tl) {
        na = q[hd][0]; da = q[hd][1]; nb = q[hd][2]; db = q[hd++][3];
        if(da + da + db <= d) {
            q[tl][0] = na; q[tl][1] = da; q[tl][2] = na+nb; q[tl++][3] = da+db;
        }
        if(da + db + db <= d) {
            q[tl][0] = na+nb; q[tl][1] = da+db; q[tl][2] = nb; q[tl++][3] = db;
        }
        ++c;
    }
    return c;
}
//
//Problem 6 Euler's Totient function, phi(n), is used to determine the number of numbers less than n which are relatively prime to n. For example, as 1, 2, 4,
//5, 7, and 8, are all less than nine and relatively prime to nine, phi(9) = 6. Interestingly, phi(87109) = 79180,and it can be seen that 87109 is a
//permutation of 79180. Find the value of n, below ten million, for which phi(n) is a permutation of n and the ratio n/phi(n) - produces a minimum.
//
int fac[N], phi[N];

bool p6perm(int x, int y)
{
    char sx[20], sy[20];
    sprintf(sx, "%d", x);
    sprintf(sy, "%d", y);
    std::sort(sx, sx+strlen(sx));
    std::sort(sy, sy+strlen(sy));
    return (strcmp(sx, sy)==0);
}
int p6()
{
    int r = -1;
    memset(fac, 0, sizeof(fac));
    fac[0] = phi[0] = 0;
    fac[1] = phi[1] = 1;
    for(int i = 2; i < N; i++) {
        if(fac[i]) {
            int j = i/fac[i];
            if(fac[j] == fac[i])
                phi[i] = phi[j] * fac[i];
            else
                phi[i] = phi[j] * (fac[i]-1);
        } else {
            fac[i] = i;
            phi[i] = i-1;
            if(i < 32768)
                for(int j = i*i; j < N; j += i)
                    if(!fac[j])
                        fac[j] = i;
        }
        if(p6perm(i, phi[i]) && (r < 0 || (long long)r*(long long)phi[i] > (long long)i*(long long)phi[r]))
            r = i;
    }
    return r;
}
int main()
{
    printf("3 %d\n", rpf(1, 3, 1, 2, 10000));
    printf("6 %d\n", p6());
    return 0;
}

