/*
 * =====================================================================================
 *
 *       Filename:  FURGRAPH.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016/04/ 1 23:01:17
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), technic.tec@gmail.com
 *   Organization:  Verisilicon, Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;
#define N 100000000
#define P 1000000007

template<typename key_t = long long, typename val_t = int>
class RedBlackTree {
  public:
  enum Color {
    BLACK,
    RED
  };

  struct node {
    Color color;
    key_t key;
    int tc;
		key_t sc;
    struct node *left;
    struct node *right;
    struct node *parent;
  };

  struct node *root, *nil;

  RedBlackTree()
  {
    nil = new struct node;
    nil->color = BLACK;
    nil->key = -1;
    nil->tc = 0;
		nil->sc = 0;
    nil->left = nil->right = nil->parent = nil;
    root = nil;
  }

  ~RedBlackTree()
  {
    vector<node *> stk;
    if(root != nil)
      stk.push_back(root);
    while(!stk.empty()) {
      struct node *p = stk.back();
      stk.pop_back();
      if(p->right != nil)
        stk.push_back(p->right);
      if(p->left != nil)
        stk.push_back(p->left);
      p->left = p->right = p->parent = NULL;
      delete p;
    }
    delete nil;
    nil = NULL;
  }

  inline struct node *grandparent(struct node *n)
  {
    return n->parent->parent;
  }

  inline struct node *uncle(struct node *n)
  {
    struct node *g = grandparent(n);
    if (g == nil)
      return nil; // No grandparent means no uncle
    if (n->parent == g->left)
      return g->right;
    else
      return g->left;
  }

  inline struct node *sibling(struct node *n)
  {
    if (n == n->parent->left)
      return n->parent->right;
    else
      return n->parent->left;
  }

  inline void synth(struct node *n)
  {
    n->tc = n->left->tc + n->right->tc + (n != nil);
		if(n->right->tc & 1)
			n->sc = n->right->sc - n->key + n->left->sc;
		else
			n->sc = n->right->sc + n->key - n->left->sc;
  }

  inline bool is_leaf(struct node *n)
  {
    return (n == nil);
  }

  inline void rotate_left(struct node *n)
  {
    //  n       x
    // y x ->  n
    //        y
    struct node *x = n->right;
    x->parent = n->parent;
    if(n->parent == nil)
      root = x;
    else if(n->parent->left == n)
      n->parent->left = x;
    else
      n->parent->right = x;
    n->right = x->left;
    if(x->left != nil)
      x->left->parent = n;
    x->left = n;
    n->parent = x;
    synth(n);
    synth(x);
  }

  inline void rotate_right(struct node *n)
  {
    //  n     x
    // x y ->  n
    //          y
    struct node *x = n->left;
    x->parent = n->parent;
    if(n->parent == nil)
      root = x;
    else if(n->parent->left == n)
      n->parent->left = x;
    else
      n->parent->right = x;
    n->left = x->right;
    if(x->right != nil)
      x->right->parent = n;
    x->right = n;
    n->parent = x;
    synth(n);
    synth(x);
  }

  inline struct node *find(key_t key)
  {
    struct node *ret = _find(key);
    return (ret==nil ? NULL : ret);
  }
  struct node *_find(key_t key, bool exact = true)
  {
    struct node *p = root;
    while(p != nil && p->key != key)
      if(key < p->key && p->left != nil)
        p = p->left;
      else if(key > p->key && p->right != nil)
        p = p->right;
      else
        break;
    if(exact) {
      if(p == nil || p->key != key)
        return nil;
      else
        return p;
    } else 
      return p;
  }

  void insert_key(key_t key)
  {
    struct node *p = _find(key, false);
    if(p == nil) {
      root = new struct node;
      root->key = root->sc = key;
      root->color = BLACK;
      root->tc = 1;
      root->left = root->right = root->parent = nil;
      p = root;
    } else {
      struct node *pp = p;
      if(key < p->key)
        p = pp->left = new struct node;
      else if(key > p->key)
        p = pp->right = new struct node;
      else if(pp->left == nil)
        p = pp->left = new struct node;
      else if(pp->right == nil)
        p = pp->right = new struct node;
      else {
        pp = pp->right;
        while(pp->left != nil)
          pp = pp->left;
        p = pp->left = new struct node;
      }
      p->color = RED;
      p->key = p->sc = key;
      p->tc = 1;
      p->left = p->right = nil;
      p->parent = pp;
      for(struct node *q = p->parent; q != nil; q = q->parent)
        synth(q);
      insert_adjust(p);
    }
  }

  void insert_adjust(struct node *n)
  {
    struct node *u, *g;
    while(1) {
      if (n->parent == nil) {
        n->color = BLACK;
        break;
      } else if (n->parent->color == BLACK)
        break; /* Tree is still valid */
      else if (((u = uncle(n)) != nil) && (u->color == RED)) {
        //       g(B)     ->     g(R)
        //   y(R)    u(R)     y(B)   u(B)
        // n(R)             n(R)
        n->parent->color = BLACK;
        u->color = BLACK;
        n = grandparent(n);
        n->color = RED;
        continue;
      } else {
        g = grandparent(n);

        if ((n == n->parent->right) && (n->parent == g->left)) {
          //       g(B)     ->     g(B)
          //   y(R)    u(B)     n(R)   u(B)
          //     n(R)         y(R) 
          rotate_left(n->parent);
          n = n->left;
        } else if ((n == n->parent->left) && (n->parent == g->right)) {
          //       g(B)     ->     g(B)
          //   u(B)    y(R)     u(B)   n(R)
          //         n(R)                y(R)
          rotate_right(n->parent);
          n = n->right;
        }

        n->parent->color = BLACK;
        g->color = RED;
        if (n == n->parent->left)
          //        g(B)     ->       g(R)     ->     y(B)
          //     y(R)   u(B)       y(B)   u(B)     n(R)  g(R)
          //   n(R)              n(R)                      u(B)
          rotate_right(g);
        else
          //      g(B)       ->      g(R)      ->     y(B)
          //   u(B)   y(R)       u(B)   y(B)       g(R)  n(R)
          //            n(R)              n(R)    u(B)
          rotate_left(g);
        break;
      }
    }
  }

  bool delete_key(key_t key)
  {
    struct node *p = _find(key);
    if(p == nil)
      return false;
    if(p->left != nil && p->right != nil) {
      struct node *q = p->right;
      while(q->left != nil)
        q = q->left;
      p->key = q->key;
      p = q;
    }
    delete_one_child(p);
    return true;
  }

  void delete_one_child(struct node *n)
  {
    /*
     * Precondition: n has at most one non-null child.
     */
    struct node *child = is_leaf(n->right) ? n->left : n->right;
    struct node *n0 = n;

    if(n->parent == nil)
      root = child;
    else if(n->parent->left == n)
      n->parent->left = child;
    else
      n->parent->right = child;
    child->parent = n->parent;
    for(struct node *q = child->parent; q != nil; q = q->parent)
      synth(q);
    if (n->color == BLACK) {
      if (child->color == RED)
        //   n(B)   ->   c(B)
        // c(R) nil     
        child->color = BLACK;
      else {
        n = child;
        while(n->parent != nil) {
          struct node *s = sibling(n);
          if (s->color == RED) {
            //       y(B)             s(B)         y(B)         s(B)
            //   n(BB)  s(R)  ->   y(R)        s(R)  n(BB)  ->    y(R)
            //                   n(BB)                              n(BB)    
            n->parent->color = RED;
            s->color = BLACK;
            if (n == n->parent->left)
              rotate_left(n->parent);
            else
              rotate_right(n->parent);
          }

          s = sibling(n);

          if ((n->parent->color == BLACK) &&
              (s->color == BLACK) &&
              (s->left->color == BLACK) &&
              (s->right->color == BLACK)) {
            //       y(B)              y(BB)
            //   n(BB)  s(B)    ->  n(B)   s(R)
            //        z(B) w(B)          z(B) w(B)
            s->color = RED;
            n = n->parent;
            nil->parent = nil;
            continue;
          }
          else if ((n->parent->color == RED) &&
              (s->color == BLACK) &&
              (s->left->color == BLACK) &&
              (s->right->color == BLACK)) {
            //       y(R)              y(B)
            //   n(BB)  s(B)    ->  n(B)   s(R)
            //        z(B) w(B)          z(B) w(B)
            s->color = RED;
            n->parent->color = BLACK;
            break;
          } else {
            if ((n == n->parent->left) &&
                (s->right->color == BLACK) &&
                (s->left->color == RED)) {
              //       y(?)               y(?)
              //   n(BB)  s(B)    ->  n(BB)   z(B)
              //        z(R) w(B)               s(R)
              //                                  w(B)
              s->color = RED;
              s->left->color = BLACK;
              rotate_right(s);
            } else if ((n == n->parent->right) &&
                (s->left->color == BLACK) &&
                (s->right->color == RED)) {
              //       y(?)                 y(?)
              //   s(B)  n(BB)    ->    z(B)   n(BB)
              //w(B) z(R)             s(R)
              //                    w(B)
              s->color = RED;
              s->right->color = BLACK;
              rotate_left(s);
            }

            s = sibling(n);
            s->color = n->parent->color;
            n->parent->color = BLACK;

            if (n == n->parent->left) {
              //       y(X)                 s(X)
              //   n(BB)  s(B)    ->    y(B)     w(B)
              //        z(?) w(R)    n(B) z(?)
              s->right->color = BLACK;
              rotate_left(n->parent);
            } else {
              //       y(X)                 s(X)
              //   s(B)  n(BB)    ->    w(B)    y(B)
              //w(R) z(?)                    z(?) n(B)
              s->left->color = BLACK;
              rotate_right(n->parent);
            }
            break;
          }
        }
      }
    }
    nil->parent = nil;
    delete n0;
  }
	void print(const struct node *p) const {
		if(p == nil) return;
		print(p->left);
		printf("%lld(%lld) ", p->key, p->sc);
		print(p->right);
		if(p == root)
			printf("\n");
	}
	key_t query() const {
		return root->sc/2;
	}
};

int main()
{
	int t;
	scanf("%d", &t);
	while(t--) {
		int n, m;
		scanf("%d%d", &n, &m);
		RedBlackTree<> tr;
		vector<int> sc(n, 0);
		for(int i = 0; i < n; i++)
			tr.insert_key(0);
		while(m--) {
			int u, v, c;
			scanf("%d%d%d", &u, &v, &c);
			--u; --v;
			tr.delete_key(sc[u]); sc[u]+=c; tr.insert_key(sc[u]);
			tr.delete_key(sc[v]); sc[v]+=c; tr.insert_key(sc[v]);
			printf("%lld\n", tr.query());
		}
	}
  return 0;
}
