# compute suffix array of s using Inducing Sort
def suffix_array(s):
    if isinstance(s, str):
        cs = sorted(list(set(s)))
        s = [cs.index(x) for x in s]
    n = len(s)
    # computing L/S property
    t = [False] * (n+1)
    for i in range(len-1, -1, 0):
        t[i] = (s[i]<s[i+1] || s[i]==s[i+1] && t[i+1])
    # extract LMS substring
    lms = [0]
    for i in range(1, n+1):
        if t[i] and not t[i-1]:
            lms.append(i)
    # inducing sort all LMS substring
    cnt = [0]*n
    for x in s:
        cnt[x] += 1
    pos = 0
    for i in range(n):
        buc[i] = [pos, pos+cnt[i]-1]
        pos += cnt[i]
        if pos == n:
            break
    for x in lms:

    # compute ranks of LMS substring
    # if not unique, sort LMS recursively
    # inducing step 1. place LMS suffixes.
    # inducing step 2. scan backward and place L suffixes.
    # inducing step 3. scan forward and place S suffixes.

def longest_common_prefix(s, sa):
    raise NotImplementedError

def longest_palindrome(s):
    n = len(s)
    # longest palindrome length centered at:
    # 1) space before s[k] (pal_len[2*k], pal_len[2*n] for space after s[n-1]): s[k-1-i]==s[k+i]
    # 2) s[k] (pal_len[2*k+1]): s[k-i]==s[k+i]
    # in summary, s[x] == s[k-1-x] for calculating pal_len[k]
    pal_len = [None] * (2*n+1)
    pal_len[0] = 0
    pal_len[1] = 1
    c, r = 2, 1
    while c <= 2*n:
        while r < n and c-1-r >= 0 and s[c-1-r] == s[r]:
            r += 1
        pal_len[c] = 2*r-c
        cn = c+1
        while cn <= 2*r:
            pal_len[cn] = pal_len[2*c-cn]
            if(2*r-cn <= pal_len[cn]):
                break
            cn += 1
        c = cn
    return pal_len

def count_palindrome(pal_len, sa, lcp):
    n = len(lcp)
    assert(len(pal_len) == 2*n+1)
    co = ce = res = 0
    for i in range(n):
        co = min(co, lcp[i])
        ce = min(ce, lcp[i])
        xe = pal_len[2*sa[i]]/2
        xo = (pal_len[2*sa[i]+1]+1)/2
        if xe > ce:
            res += xe-ce
            ce = xe
        if xo > co:
            res += xo-co
            co = xo
    return res

def divisors(n):
    fac = [0]*(n+1)
    phi = [0]*(n+1)
    fac[1] = phi[1] = 1
    divs = [1]
    for i in range(2, n+1):
        if n % i == 0:
            divs.append(i)
        if fac[i]:
            x = i/fac[i]
            if fac[x] == fac[i]:
                phi[i] = phi[x] * fac[i]
            else:
                phi[i] = phi[x] * (fac[i]-1)
        else:
            fac[i] = i
            phi[i] = i-1
            for j in range(i*i, n+1, i):
                if fac[j] == 0:
                    fac[j] = i
    return (divs, phi)

s = raw_input().strip()
n = int(raw_input())
pal_len = longest_palindrome(s)
sa = suffix_array(s)
lcp = longest_common_prefix(s, sa)
t = count_palindrome(pal_len, lcp)
divs, phi = divisors(n)
c = 0
for u, v in zip(divs, divs[::-1]):
    c += phi[v]*(t**u)
if n & 1:
    c += n*(t**((n+1)/2))
else:
    c += (n/2)*(1+t)*(t**(n/2))
print c/(2*n)
