P = 1000000007
N = 1000
c = [[0]*(N+1) for _ in range(N+1)]
def prep():
    for i in range(N+1):
        c[i][0] = c[i][i] = 1
        for j in range(1, i):
            c[i][j] = c[i-1][j]+c[i-1][j-1]
            if c[i][j] >= P:
                c[i][j] -= P

def power_mod(n, k, p):
    b = (1<<30)
    while b > 0 and (k & b)==0:
        b >>= 1
    r = 1
    while b > 0:
        r *= r
        if k & b:
            r *= n
        r %= p
        b >>= 1
    return r

def dset(n, d):
    if d == 0:
        return 1
    r = 0
    for i in range(n+1):
        v = power_mod(d, i, P-1)*power_mod(d+1, n-i, P-1)%(P-1)
        r += (-1)**i * c[n][i] * power_mod(2, v, P)
    r %= P
    if r < 0:
        r += P
    return r

import cProfile,  pstats,  StringIO
pr = cProfile.Profile()
pr.enable()
prep()
t = int(raw_input())
for _ in range(t):
    n, d = map(int, raw_input().split())
    r = dset(n, d) - dset(n, d-1)
    if r < 0:
        r += P
    print r
pr.disable()
s = StringIO.StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr,  stream=s).sort_stats(sortby)
ps.print_stats()
print s.getvalue()
