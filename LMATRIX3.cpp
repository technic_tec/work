#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
using namespace std;

#define N 100000
#define DIFF(a,b,p) ((a)<(b)?((a)-(b)+(p)):((a)-(b)))

class Frac {
    public:
        Frac(long long x = 0, int y = 1) : num(x), denom(y) {
            simplify();
        }
        inline Frac operator+(const Frac &o) const {
            return Frac(num*o.denom + o.num*denom, denom*o.denom);
        }
        inline Frac operator-(const Frac &o) const {
            return Frac(num*o.denom - o.num*denom, denom*o.denom);
        }
        inline Frac operator*(const Frac &o) const {
            return Frac(num*o.num, denom*o.denom);
        }
        inline Frac operator/(const Frac &o) const {
            return Frac(num*o.denom, denom*o.num);
        }
        inline Frac& operator+=(const Frac &o) {
            num = num*o.denom + o.num*denom;
            denom = denom*o.denom;
            simplify();
            return *this;
        }
        inline Frac& operator-=(const Frac &o) {
            num = num*o.denom - o.num*denom;
            denom = denom*o.denom;
            simplify();
            return *this;
        }
        inline Frac& operator*=(const Frac &o) {
            num *= o.num;
            denom *= o.denom;
            simplify();
            return *this;
        }
        inline Frac& operator/=(const Frac &o) {
            num *= o.denom;
            denom *= o.num;
            simplify();
            return *this;
        }
        inline bool operator<(const Frac &o) const {
            return num*o.denom < denom*o.num;
        }
        inline bool operator>(const Frac &o) const {
            return o < *this;
        }
        inline bool operator==(const Frac &o) const {
            return num*o.denom == denom * o.num;
        }
        inline int gcd(long long x, int y) {
            int a = y, b = x%y;
            while(b) {
                int c = a; a = b; b = c % b;
            }
            return a;
        }
        inline void simplify() {
            int d = gcd(num, denom);
            num /= d; denom /= d;
        }
        long long num;
        int denom;
};

Frac tr[15][55];
long long ILP(int p, long long c[])
{
    if(p == 1)
        return 0;
    // compute LP matrix
    int var[10][10], nvar = 1, neq = p + (p-1)/2;
    fill(&tr[0][0], &tr[15][0], 0);
    tr[0][0] = 1;
    for(int i = 0; i < p; i++)
        for(int j = i; j < p; j++)
            if(i+j != 0 && i+j != p) {
                tr[0][nvar] = -(i==0?1:2);
                tr[(i+j)%p][nvar] = 1;
                if(i) {
                    if(i < p-i)
                        tr[p+i-1][nvar] += 1;
                    else if(i > p-i)
                        tr[p+(p-i)-1][nvar] -= 1;
                }
                if(j < p-j)
                    tr[p+j-1][nvar] += 1;
                else if(j > p-j)
                    tr[p+(p-j)-1][nvar] -= 1;
                var[i][j] = nvar++;
            } else
                var[i][j] = -1;
    tr[0][nvar] = 0;
    for(int i = 1; i < p; i++)
        tr[i][nvar] = c[i];
    for(int i = p; i < neq; i++)
        tr[i][nvar] = 0;

    // normalize
    int bs[15] = {0};
    for(int i = 1; i < p; i++)
        bs[i] = i;
    bs[p] = neq-1;
    for(int i = p+1; i < neq; i++)
        bs[i] = i-1;
    for(int i = 1; i < neq; i++) {
        if(i == p) continue;
        if(tr[i][bs[i]] < 0)
            for(int j = 0; j <= nvar; j++)
                tr[i][j] = Frac(0)-tr[i][j];
        assert(tr[i][bs[i]] == 1);
        for(int j = 0; j < neq; j++)
            if(j != i && tr[j][bs[i]] > 0) {
                Frac cc = tr[j][bs[i]];
                for(int k = 0; k <= nvar; k++)
                    tr[j][k] -= cc*tr[i][k];
            }
    }
#if 0
    {
        Frac cc = tr[p][bs[p]];
        for(int j = 0; j <= nvar; j++)
            if(tr[p][j] > 0) {
                assert(tr[p][j].num % cc.num == 0);
                tr[p][j] /= cc;
            }
        for(int j = 0; j < neq; j++)
            if(j != p && tr[j][bs[p]] > 0) {
                Frac cc = tr[j][bs[p]];
                for(int k = 0; k <= nvar; k++)
                    tr[j][k] -= tr[p][k]*cc;
            }
    }
#endif
    for(int i = 0; i < neq; i++)
        for(int j = 0; j <= nvar; j++)
            printf("%2lld%c", tr[i][j].num, (j<nvar?' ':'\n'));
    return 0;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        long long n;
        int p, m;
        scanf("%lld%d%d", &n, &p, &m);
        long long *c = new long long[p];
        fill(c, c+p, 0);
        int lst = 0;
        for(int i = 0; i < m; i++) {
            int fi, ci, di;
            long long li;
            scanf("%d%d%d%lld", &fi, &ci, &di, &li);
            c[DIFF(fi, lst, p)]++;
            int *bi = new int[p+1];
            int *ps = new int[p];
            int init, cyc;
            fill(ps, ps+p, -1);
            for(int j = 0, bj = fi; j <= p; j++, bj = (bj*ci+di)%p) {
                if(ps[bj] >= 0) {
                    bi[j] = bj;
                    init = ps[bj];
                    cyc = j - ps[bj];
                    break;
                }
                bi[j] = bj;
                ps[bj] = j;
            }
            if(li <= init+cyc) {
                for(int j = 1; j < li; j++)
                    c[DIFF(bi[j], bi[j-1], p)]++;
                lst = bi[li-1];
            } else {
                // 0, .., init, init+1, init+cyc, ..., init+k*cyc, ..., li-1=init+k*cyc+(li-1-init)%cyc
                for(int j = 1; j <= init; j++)
                    c[DIFF(bi[j], bi[j-1], p)]++;
                for(int j = init+1; j <= init+cyc; j++)
                    c[DIFF(bi[j], bi[j-1], p)] += (li-1-init)/cyc + (j-init <= (li-1-init)%cyc);
                lst = bi[init+(li-1-init)%cyc];
            }
            delete[] bi;
            delete[] ps;
        }
        c[DIFF(0, lst, p)]++;
        printf("%lld\n", ILP(p, c));
        delete[] c;
    }
    return 0;
}

