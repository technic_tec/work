#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <algorithm>

#define N 100100
#define P 1000000009

class Comparator {
    public:
        Comparator(int *v) : m_v(v) {};
        bool operator() (const int &a, const int &b) {
            return m_v[a] < m_v[b];
        }
    private:
        int *m_v;
};

class SuffixArray {
    public:
        SuffixArray(int v[], int len) {
            m_v = v;
            m_len = len;
            m_sa = m_rank = m_lcp = NULL;
            /* printf("m_v:  ");
            for(int i = 0; i < m_len; i++)
                printf("%d%c", m_v[i], i<m_len-1?' ':'\n'); */
            construct_sa();
            compute_lcp();
        }

        ~SuffixArray() {
            if(m_sa) {
                delete[] m_sa;
                m_sa = NULL;
            }
            if(m_rank) {
                delete[] m_rank;
                m_rank = NULL;
            }
            if(m_lcp) {
                delete[] m_lcp;
                m_lcp = NULL;
            }
        }
        long long SubstringCount()
        {
            long long res = m_len - m_sa[0];
            for(int i = 1; i < m_len; i++)
                res += m_len - m_sa[i] - m_lcp[i];
            return res;
        }
    private:
        void construct_sa()
        {
            m_sa = new int[m_len];
            m_rank = new int[m_len];
            for(int i = 0; i < m_len; i++)
                m_sa[i] = i;
            Comparator com(m_v);
            std::sort(m_sa, m_sa + m_len, com);
            m_rank[m_sa[0]] = 1;
            for(int i = 1; i < m_len; i++)
                m_rank[m_sa[i]] = (m_v[m_sa[i]] == m_v[m_sa[i-1]] ? m_rank[m_sa[i-1]] : m_rank[m_sa[i-1]]+1);

            int *buckets = new int[m_len+1];
            int *bucnext = new int[m_len];
            int *rank2 = new int[m_len];
            for(int k = 1; k < m_len; k <<= 1) {
                std::fill(buckets, buckets+m_len+1, -1);
                int rm = m_rank[m_sa[m_len-1]];
                for(int i = m_len - 1; i >= 0; i--) {
                    int j = (m_sa[i] + k < m_len ? m_rank[m_sa[i]+k] : 0);
                    bucnext[m_sa[i]] = buckets[j];
                    buckets[j] = m_sa[i];
                }
                for(int i = 0, j = 0; i <= rm; i++)
                    for(int x = buckets[i]; x >= 0; x = bucnext[x])
                        m_sa[j++] = x;
                std::fill(buckets, buckets+m_len+1, -1);
                for(int i = m_len - 1; i >= 0; i--) {
                    int j = m_rank[m_sa[i]];
                    bucnext[m_sa[i]] = buckets[j];
                    buckets[j] = m_sa[i];
                }
                for(int i = 0, j = 0; i <= rm; i++)
                    for(int x = buckets[i]; x >= 0; x = bucnext[x])
                        m_sa[j++] = x;
                rank2[m_sa[0]] = 1;
                for(int i = 1; i < m_len; i++)
                    rank2[m_sa[i]] = (m_rank[m_sa[i]] == m_rank[m_sa[i-1]] && m_sa[i]+k < m_len && m_sa[i-1]+k < m_len && m_rank[m_sa[i]+k] == m_rank[m_sa[i-1]+k] ? rank2[m_sa[i-1]] : rank2[m_sa[i-1]]+1);
                int *pt = m_rank; m_rank = rank2; rank2 = pt;
            }
            /* for(int i = 0; i < m_len; i++) {
                assert(m_rank[m_sa[i]] == i+1);
                assert(m_sa[m_rank[i]-1] == i);
            } */
            delete[] buckets;
            delete[] bucnext;
            delete[] rank2;
            /* printf("m_sa: ");
            for(int i = 0; i < m_len; i++)
                printf("%d%c", m_sa[i], i<m_len-1?' ':'\n'); */
        }
        void compute_lcp()
        {
            m_lcp = new int[m_len];
            int j;
            int x = m_rank[0]-1;
            if(x == 0)
                m_lcp[x] = 0;
            else {
                for(j = 0; m_sa[x]+j < m_len && m_sa[x-1]+j < m_len && m_v[m_sa[x]+j] == m_v[m_sa[x-1]+j]; j++);
                m_lcp[x] = j;
            }
            for(int i = 1; i < m_len; i++) {
                x = m_rank[i]-1;
                if(x == 0 || m_v[m_sa[x]] != m_v[m_sa[x-1]])
                    m_lcp[x] = 0;
                else {
                    int y = m_rank[m_sa[x]-1]-1;
                    for(j = std::max(m_lcp[y]-1, 0); m_sa[x]+j < m_len && m_sa[x-1]+j < m_len && m_v[m_sa[x]+j] == m_v[m_sa[x-1]+j]; j++);
                    m_lcp[x] = j;
                }
            }
            /* printf("lcp: ");
            for(int i = 0; i < m_len; i++)
                printf("%d%c", m_lcp[i], i<m_len-1?' ':'\n'); */
        }
        int *m_v, m_len;
        int *m_sa, *m_rank;
        int *m_lcp;
};

int n, v[N];
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &n);
        for(int i = 0; i < n; i++)
            scanf("%d", &v[i]);
        for(int i = n-1; i > 0; i--)
            v[i] -= v[i-1];
        SuffixArray sa(v+1, n-1);
        printf("%lld\n", sa.SubstringCount() % P);
    }
    return 0;
}

