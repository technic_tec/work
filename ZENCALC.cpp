/*
 * =====================================================================================
 *
 *       Filename:  ZINCALC.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年03月04日 10时38分25秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define N 100000
#define NQ 11

/* 
 * 32-bit residue of A is defined as the unique integer X in the range [-2^31,  2^31) for which the usual difference of A and X is divisible by 2^32.
 * 32-bit difference A - B is defined as a 32-bit residue of the usual difference of A and B.
 * 32-bit sum A + B is defined as a 32-bit residue of the usual sum of A and B.
 * 32-bit product A * B is defined as a 32-bit residue of the usual product of A and B.
 * 32-bit quotient A / B is defined as the smallest integer C in the range [-2^31,  2^31) such that B * C = A (note the B * C here is a 32-bit product of B and C). If no such integer C exists then result of division is undefined,  which means that error occurs during the calculation.
 * 32-bit power A ^ B is defined as follows. For B > 0 we define A ^ B = A * A * ... * A,  where A repeats B times (here * is a 32-bit multiplication). For B = 0 we define A ^ 0 = 1. Finally,  for B < 0 we define A ^ B = 1 / (A ^ (-B)),  where A ^ (-B) is defined above (-B is the usual negation of B) and / is a 32-bit division operation. If result of division here is undefined it means that error occurs during the calculation.
 * 32-bit factorial A! is defined as follows. For A > 0 we define A! = 1 * 2 * 3 * ... * A (here * is a 32-bit multiplication). For A = 0 we define 0! = 1. Finally,  for A < 0 we define A! = (-1) * (-2) * (-3) * ... * A (here again * is a 32-bit multiplication and -X for X = 1,  2,  3,  ... means usual negation).
 */
const static int fact[34] = {
    1,  1,  2,  6,  24,  120,  720,  5040,  40320,  362880,  3628800,  39916800,  479001600,  1932053504,  1278945280,  2004310016,  2004189184,  -288522240,  -898433024,  109641728,  -2102132736,  -1195114496,  -522715136,  862453760,  -775946240,  2076180480,  -1853882368,  1484783616,  -1375731712,  -1241513984,  1409286144,  738197504,  -2147483648,  -2147483648
};
class Expr {
    public:
        Expr(int n) : tail(&root) {}
        ~Expr(){
            while(tail != &root) {
                Operation *p = tail;
                tail = p->pre;
                tail->nxt = p->pre = NULL;
                delete p;
            }
        }
        void appendOp(char op, int id) {
            Operation *pOp = new Operation(op, id);
            tail->nxt = pOp;
            pOp->pre = tail;
            tail = pOp;
        }
        void appendArg(int x) {
            Arg *p = new Arg(x);
            tail->appendArgs(p, p, 1);
        }
        bool cal() {
            while(true) {
                int i;
                for(i = 0; i < NQ; i++)
                    if(qhd[i].qnxt) {
                        try {
                            qhd[i].qnxt->perform();
                        } catch (int e) {
                            return false;
                        }
                        break;
                    }
                if(i >= NQ) break;
            }
            return (root.nxt == NULL && root.nargs == 1);
        }

    private:
        class Operation;
        class Arg {
            friend class Expr::Operation;
            public:
                Arg(int x) : val(x), pre(NULL), nxt(NULL) {}
            private:
                int val;
                Arg *pre;
                Arg *nxt;
        };
        class Operation {
            friend class Expr;
            public:
                Operation(char _op = 0, int _id = -1)
                    : op(_op), id(_id), args(NULL), last(NULL), nargs(0),
                    pre(NULL), nxt(NULL), qpre(NULL), qnxt(NULL) {}
                ~Operation() {
                    while(args) {
                        Arg *p = args;
                        args = p->nxt;
                        p->nxt = args->pre = NULL;
                        delete p;
                    }
                    last = NULL;
                }
                void appendArgs(Arg *p_first, Arg *p_last, int sz)
                {
                    if(!sz) {
                        assert(!p_first && !p_last);
                        return;
                    }
                    assert(p_first && p_last);
                    if(args == NULL) {
                        args = p_first;
                        last = p_last;
                        nargs = sz;
                    } else {
                        last->nxt = p_first;
                        p_first->pre = last;
                        last = p_last;
                        nargs += sz;
                    }
                }
                void perform() {
                    int argA, argB, res;
                    argA = args->val;
                    if(op == '!')
                        argB = -1;
                    else {
                        Arg *p = args;
                        args = args->nxt;
                        delete p;
                        --nargs;
                        argB = args->val;
                    }
                    switch(op) {
                        case '+':
                            res = argA + argB;
                            break;
                        case '-':
                            res = argA - argB;
                            break;
                        case '*':
                            res = argA * argB;
                            break;
                        case '/':
                            res = div(argA, argB);
                            break;
                        case '^':
                            if(argB == 0) {
                                res = 1;
                            } else if(argB < 0) {
                                res = div(1, pow(argA, -argB));
                            } else {
                                res = pow(argA, argB);
                            }
                            break;
                        case '!':
                            res = argA - argB;
                            break;
                    }
                    args->val = res;
                }
            private:
                int div(int a, int b) {
                    int d = (a & (-a));
                    if(b % d)
                        throw(0);
                }
                int pow(int a, int k);
                char op;
                int id;
                Arg *args;
                Arg *last;
                int nargs;
                Operation *pre;
                Operation *nxt;
                Operation *qpre;
                Operation *qnxt;
        };
        Operation root, *tail, qhd[NQ];
};

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        int n;
        char sym[20];
        scanf("%d",  &n);
        Expr *exp = new Expr(n);
        for(int i = 1; i <= n; i++) {
            scanf("%s", sym);
            if(sym[1] == 0 && (sym[0] == '!' || sym[0] == '^' || sym[0] == '/' || sym[0] == '*' || sym[0] == '+' || sym[0] == '-')) {
                exp->appendOp(sym[0], i);
            } else {
                int v = atoi(sym);
                exp->appendArg(v);
            }
        }
        if(exp->cal())
            printf("OK\n");
        else
            printf("NOT OK\n");
        delete exp;
    }
    return 0;
}

