# s1=1
# sn = sn-1 + n*(sn-1 % n)
# [sn/(n+1)] = sn-1 - (n-1)*[sn-1/n] - [sn-1/(n(n+1))]
# sn <= sum(k*(k-1), k, 1, n-1) = n(n-1)(n-2)/3
s, i = 1, 1
while i < 100:
  i += 1
  ai = s % i
  s1 = s + i*ai
  print i, s1, ai, s/i
  s = s1
