import sys
from random import randint

class DisjointSet(object):
  def __init__(self, n):
    self.n = n
    self.pa = range(n+1)
    self.ht = [1] * (n+1)
  def get_root(self, x):
    if self.pa[x] == x:
      return x
    self.pa[x] = self.get_root(self.pa[x])
    return self.pa[x]
  def merge(self, rx, ry):
    assert(self.pa[rx] == rx and self.pa[ry] == ry and rx != ry)
    if self.ht[rx] < self.ht[ry]:
      self.pa[rx] = ry
    else:
      self.pa[ry] = rx
      if self.ht[rx] == self.ht[ry]:
        self.ht[rx] += 1

n = 100
if len(sys.argv) > 1:
  n = int(sys.argv[1])
print 1 # T
print n
m = n-1
ds = DisjointSet(n)
while m > 0:
  x = randint(1, n)
  y = randint(1, n)
  if x == y:
    continue
  rx = ds.get_root(x)
  ry = ds.get_root(y)
  if rx != ry:
    print x, y, randint(1, 10**4)
    ds.merge(rx, ry)
    m -= 1
