/*
 * =====================================================================================
 *
 *       Filename:  AMAEXPER.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年04月05日 14时28分35秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define P 1000000007
#define N 100000000

class Tree {
  public:
    struct Node {
      int h, d, r;
      vector<int> hs;
      Node() : h(0), d(0), r(0) {}
    };
    struct Edge {
      int dst, w;
      Edge(int _dst, int _w) : dst(_dst), w(_w) {}
    };
    Tree(int n) : m_sz(n), m_nodes(n), m_adj(n) {}
    void add_edge(int u, int v, int w)
    {
      m_adj[u].push_back(Edge(v, w));
      m_adj[v].push_back(Edge(u, w));
    }
    void cost(int v, int pa = -1) {
      Node *pv = &m_nodes[v];
      int h1 = 0, h2 = 0, p1 = -1, p2 = -1, d = 0, r = 0;
      for(int i = 0; i < m_adj[v].size(); i++) {
        int w = m_adj[v][i].w, u = m_adj[v][i].dst;
        if(u != pa) {
          Node *pu = &m_nodes[u];
          cost(u, v);
          if(pu->h + w > h1) {
            h2 = h1; h1 = pu->h+w;
            p2 = p1; p1 = u;
          } else if(pu->h + w > h2) {
            h2 = pu->h + w;
            p2 = u;
          }
          if(pu->d > d) {
            d = pu->d;
            r = pu->r;
          }
        }
      }
      pv->h = h1;
      if(p1 >= 0) {
        pv->hs.swap(m_nodes[p1].hs);
        assert(!pv->hs.empty() && pv->hs.back() <= pv->h);
      } else {
        assert(pv->hs.empty());
      }
      pv->hs.push_back(pv->h);
      if(h1 + h2 > d) {
        pv->d = h1+h2;
        if(h1 == h2)
          pv->r = h1;
        else {
          vector<int>::iterator it = upper_bound(pv->hs.begin(), pv->hs.end(), pv->d/2);
          assert(it != pv->hs.end());
          pv->r = (it==pv->hs.begin() ? *it : min(*it, pv->d-*(it-1)));
        }
      } else {
        pv->d = d;
        pv->r = r;
      }
    }
    void print_cost() const {
      for(int i = 0; i < m_sz; i++)
        printf("%d\n", m_nodes[i].r);
    }
  private:
    int m_sz;
    vector<Node> m_nodes;
    vector<vector<Edge> > m_adj;
};
int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    int n;
    scanf("%d", &n);
    Tree tr(n);
    for(int i = 0; i < n-1; i++) {
      int u, v, w;
      scanf("%d%d%d", &u, &v, &w);
      --u; --v;
      tr.add_edge(u, v, w);
    }
    tr.cost(0);
    tr.print_cost();
  }
  return 0;
}
