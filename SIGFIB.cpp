// fib(x)*fib(y)*fib(z) = (fib(x+y+z) - fib(y+z-x)*(-1)^x - fib(z+x-y)*(-1)^y - fib(x+y-z)*(-1)^z)/5
// x+y+z=n => fib(x)*fib(y)*fib(z) = (fib(n) - fib(n-2x)*(-1)^x - fib(n-2y)*(-1)^y - fib(n-2z)*(-1)^z)/5
// sum(x*y*z*fib(x)*fib(y)*fib(z)) = (fib(n)*sum(x*y*z) - 3*sum((-1)^x*x*y*z*fib(n-2*x)))/5
// sum(y*(t-y), 0<=y<=t) = (t-1)*t*(t+1)/6
// sum(xyz, 0<=x<=n, 0<=y<=n-x, z=n-x-y)
// = sum(x*(n-x-1)*(n-x)*(n-x+1)/6, 0<=x<=n)
// = sum(-u^4+n*u^3+u^2-n*u, 0<=u<=n)/6
// sum(xyz*(-1)^x*fib(n-2x), 0<=x<=n, 0<=y<=n-x, z=n-x-y)
// = sum((-1)^x*fib(n-2x)*x*(n-x+1)*(n-x)*(n-x-1)/6
#include <cstdio>
#include <cstdlib>
#include <cassert>
class FibM {
    public:
        FibM(int m) : m_mod(m) {
            int f0 = 0, f1 = 1;
            m_cyc = 0;
            do {
                int c = f0 + f1;
                if(c >= m) c -= m;
                f0 = f1; f1 = c;
                m_cyc++;
            } while(f0 != 0 || f1 != 1);
            m_fib = new int[m_cyc];
            printf("cyc(%d) = %d\n", m_mod, m_cyc);
            m_fib[0] = 0; m_fib[1] = 1;
            for(int i = 2; i < m_cyc; i++) {
                m_fib[i] = m_fib[i-1] + m_fib[i-2];
                if(m_fib[i] >= m)
                    m_fib[i] -= m;
            }
            assert((m_fib[m_cyc-2] + m_fib[m_cyc-1]) % m == 0);
            assert((m_fib[m_cyc-2] + 2*m_fib[m_cyc-1]) % m == 1);
        }
        ~FibM() {
            if(m_fib)
                delete[] m_fib;
            m_fib = NULL;
        }
        inline int operator[](int n) const {
            n %= m_cyc;
            if(n < 0) n += m_cyc;
            return m_fib[n];
        }
    private:
        int m_mod;
        int m_cyc;
        int *m_fib;
};

int main()
{
    int t;
    for(int m = 5; m <= 500000; m+=5) {
        FibM fib(m);
    }
    scanf("%d",&t);
    while(t--) {
        long long n;
        int m;
        scanf("%d%lld", &m, &n);
        m *= 5;
        FibM fib(m);
        int r = 0, s = 0;
        int fn = fib[n];
        for(int x = 0; x <= n; x++) {
            int rx = fn - 3*(x&1?-1:1)*fib[n-2*x];
            rx = (int)((long long)x*(long long)(n-x+1)%m*(long long)(n-x)%m*(long long)(n-x-1)%m*(long long)rx%m);
            if(rx < 0) rx += m;
            r += rx;
            if(r >= m) r -= m;
        }
        printf("%d\n", r/5);
    }
    return 0;
}
