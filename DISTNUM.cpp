/*
 * =====================================================================================
 *
 *       Filename:  DISTNUM.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年08月10日 16时00分42秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009

class RangeTree2D {
  public:
    class Node {
      int l, r;
      Node *
    };
    RangeTree2D();
    ~RangeTree2D();
    bool add(int x, int y, int v);
    bool remove(int x, int y);
    // rs[k] -- sum(v[x][y]^(k+1), xl<=x<=xr, yl<=y<=yr) modulo P
    void range_sum(int xl, int yl, int xr, int yr, int rs[3]);
    // count(v[x][y], xl<=x<=xr, yl<=y<=yr, v[x][y]!=0)
    int range_cnt(int xl, int yl, int xr, int yr);
  private:
};

struct Query {
  int op, x, y;
};

int main()
{
  int n, m;
  vector<int> v;
  vector<Query> q;

  scanf("%d%d", &n, &q);
  v.reserve(n);
  q.reserve(m);
  for(int i = 0; i < n; i++) {
    int x;
    scanf("%d", &x);
    v.push_back(x);
  }
  for(int i = 0; i < q; i++) {
    Query qx;
    scanf("%d", &qx.op);
    if(q[i].op == 3)
      scanf("%d", &qx.x);
    else
      scanf("%d%d", &qx.x, &qx.y);
    q.push_back(qx);
  }
  // dynamic array -> static array
  // insert: expand & assign
  // delete: assign 0
  // adjust subsequent indexes
  for(int i = 0; i < m; i++) {
    switch(q[i].op) {
      case 1: // sum(si*sj*sk) = (sum(si)^3-3*sum(si^2)*sum(si)+2*sum(si^3))/6
        break;
      case 2: // v[x]=y
        break;
      case 5: // count_nz
        break;
      default:
        assert(0);
    }
  }
  return 0;
}


