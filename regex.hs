import Control.Monad
import Text.ParserCombinators.Parsec

data Exp = Alter [Exp] | Seq [Exp] | Star Exp | Var Char
	deriving (Read, Show)

atom_parser :: Parser Exp
atom_parser = try (char '(' >> regex_parser >>= (\e -> char ')' >> return e)) <|> liftM Var letter

star_parser :: Parser Exp
star_parser = do
  x <- atom_parser
  s <- option ' ' (char '*')
  return (star_construction s x)
 where star_construction ' ' x = x
       star_construction '*' x = Star x

regex_parser :: Parser Exp
regex_parser = sepBy (many1 star_parser >>= (liftM Seq).return) (char '|') >>= (liftM Alter).return

reduceExp :: Exp -> Exp
reduceExp (Var x) = Var x
reduceExp (Alter [e]) = reduceExp e
reduceExp (Alter es) = Alter $ map reduceExp es
reduceExp (Seq [e]) = reduceExp e
reduceExp (Seq es) = Seq $ map reduceExp es
reduceExp (Star (Star e)) = reduceExp (Star e)
reduceExp (Star e) = Star (reduceExp e)

process s = case (parse regex_parser "RegexParser" s) of
	Left err -> print err
	Right exp -> putStrLn $ show $ reduceExp exp

main :: IO()
main = getLine >>= process
