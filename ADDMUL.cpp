/*
 * =====================================================================================
 *
 *       Filename:  ADDMUL.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月05日 13时26分11秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define P 1000000007

class SegmentTree {
  public:
    class Node {
      public:
        int l, r;
        int v;
        int va;
        int vm;
        int sum;
    };
    SegmentTree(const vector<int> &v)
      : m_sz(v.size()), m_nodes(2*v.size())
    {
      int m = m_sz;
      while(m & (m-1))
        m += (m&(-m));
      for(int j = 0; j < m_sz; j++) {
        int i = m + j;
        if(i >= 2*m_sz)
          i -= m_sz;
        m_nodes[i].l = m_nodes[i].r = j+1;
        m_nodes[i].v = m_nodes[i].sum = v[j];
        m_nodes[i].va = 0;
        m_nodes[i].vm = 1;
      }
      for(int i = m_sz-1; i > 0; i--) {
        //assert(m_nodes[2*i].l <= m_nodes[2*i].r && m_nodes[2*i].r == m_nodes[2*i+1].l-1 && m_nodes[2*i+1].l <= m_nodes[2*i+1].r);
        m_nodes[i].l = m_nodes[2*i].l;
        m_nodes[i].r = m_nodes[2*i+1].r;
        m_nodes[i].sum = m_nodes[2*i].sum + m_nodes[2*i+1].sum;
        if(m_nodes[i].sum >= P)
          m_nodes[i].sum -= P;
        m_nodes[i].v = -1;
        m_nodes[i].va = 0;
        m_nodes[i].vm = 1;
      }
    }
    void proc(int x, int y, int v, void (SegmentTree::*op)(int, int))
    {
      int i = 1;
      while(m_nodes[i].l < m_nodes[i].r) {
        if(y <= m_nodes[2*i].r) {
          lazy_process(i);
          i = 2*i;
        } else if(x >= m_nodes[2*i+1].l) {
          lazy_process(i);
          i = 2*i+1;
        } else
          break;
      }
      if(x <= m_nodes[i].l && y >= m_nodes[i].r) {
        (this->*op)(i, v);
      } else {
        lazy_process(i);
        int j = 2*i;
        while(1) {
          if(x <= m_nodes[j].l) {
            (this->*op)(j, v);
            break;
          } else if(x <= m_nodes[2*j].r) {
            lazy_process(j);
            (this->*op)(2*j+1, v);
            j = 2*j;
          } else {
            lazy_process(j);
            j = 2*j+1;
          }
        }
        for(j >>= 1; j > i; j >>= 1) {
          m_nodes[j].sum = m_nodes[2*j].sum + m_nodes[2*j+1].sum;
          if(m_nodes[j].sum >= P)
            m_nodes[j].sum -= P;
        }
        j = 2*i+1;
        while(1) {
          if(y >= m_nodes[j].r) {
            (this->*op)(j, v);
            break;
          } else if(y >= m_nodes[2*j+1].l) {
            lazy_process(j);
            (this->*op)(2*j, v);
            j = 2*j+1;
          } else {
            lazy_process(j);
            j = 2*j;
          }
        }
        for(j >>= 1; j >= i; j >>= 1) {
          m_nodes[j].sum = m_nodes[2*j].sum + m_nodes[2*j+1].sum;
          if(m_nodes[j].sum >= P)
            m_nodes[j].sum -= P;
        }
      }
      for(i >>= 1; i > 0; i >>= 1) {
        m_nodes[i].sum = m_nodes[2*i].sum + m_nodes[2*i+1].sum;
        if(m_nodes[i].sum >= P)
          m_nodes[i].sum -= P;
      }
    }
    inline void add(int x, int y, int v) {
      proc(x, y, v, &SegmentTree::_add);
    }
    inline void mul(int x, int y, int v){
      proc(x, y, v, &SegmentTree::_mul);
    }
    inline void set(int x, int y, int v){
      proc(x, y, v, &SegmentTree::_set);
    }
    inline int sum(int x, int y)
    {
      int res = 0;
      int i = 1;
      while(m_nodes[i].l < m_nodes[i].r) {
        if(y <= m_nodes[2*i].r) {
          lazy_process(i);
          i = 2*i;
        } else if(x >= m_nodes[2*i+1].l) {
          lazy_process(i);
          i = 2*i+1;
        } else
          break;
      }
      if(x <= m_nodes[i].l && y >= m_nodes[i].r) {
        _sum(i, res);
      } else {
        lazy_process(i);
        int j = 2*i;
        while(1) {
          if(x <= m_nodes[j].l) {
            _sum(j, res);
            break;
          } else if(x <= m_nodes[2*j].r) {
            lazy_process(j);
            _sum(2*j+1, res);
            j = 2*j;
          } else {
            lazy_process(j);
            j = 2*j+1;
          }
        }
        j = 2*i+1;
        while(1) {
          if(y >= m_nodes[j].r) {
            _sum(j, res);
            break;
          } else if(y >= m_nodes[2*j+1].l) {
            lazy_process(j);
            _sum(2*j, res);
            j = 2*j+1;
          } else {
            lazy_process(j);
            j = 2*j;
          }
        }
      }
      return res;
    }
    void check() {
      for(int i = 2*m_sz-1; i > 0; i--) {
        int l = m_nodes[i].l, r = m_nodes[i].r, v = m_nodes[i].v, va = m_nodes[i].va, vm = m_nodes[i].vm, sum = m_nodes[i].sum;
        if(m_nodes[i].l == m_nodes[i].r) {
          assert(va == 0 && vm == 1 && sum == v);
        } else {
          int ll = m_nodes[2*i].l, lr = m_nodes[2*i].r, rl = m_nodes[2*i+1].l, rr = m_nodes[2*i+1].r;
          int sl = m_nodes[2*i].sum, sr = m_nodes[2*i+1].sum;
          assert(l < r && l == ll && lr + 1 == rl && r == rr);
          if(v >= 0) {
            assert(va == 0 && vm == 1 && sum == (int)((long long)v * (long long)(r-l+1) % P));
          } else {
            sl = (int)(((long long)sl*(long long)vm + (long long)va) % P);
            sr = (int)(((long long)sr*(long long)vm + (long long)va) % P);
            assert(sum == (sl + sr) % P);
          }
        }
      }
    }
  private:
    void lazy_process(int i) {
      if(m_nodes[i].l == m_nodes[i].r)
        return;
      if(m_nodes[i].v >= 0) {
        _set(2*i, m_nodes[i].v);
        _set(2*i+1, m_nodes[i].v);
        m_nodes[i].v = -1;
      } else {
        if(m_nodes[i].vm != 1) {
          _mul(2*i, m_nodes[i].vm);
          _mul(2*i+1, m_nodes[i].vm);
          m_nodes[i].vm = 1;
        }
        if(m_nodes[i].va != 0) {
          _add(2*i, m_nodes[i].va);
          _add(2*i+1, m_nodes[i].va);
          m_nodes[i].va = 0;
        }
      }
    }
    inline void _add(int i, int v) {
      if(m_nodes[i].v >= 0) {
        m_nodes[i].v += v;
        if(m_nodes[i].v >= P)
          m_nodes[i].v -= P;
      } else {
        m_nodes[i].va += v;
        if(m_nodes[i].va >= P)
          m_nodes[i].va -= P;
      }
      m_nodes[i].sum = (int)(((long long)m_nodes[i].sum + (long long)(m_nodes[i].r-m_nodes[i].l+1)*(long long)v) % P);
    }
    inline void _mul(int i, int v) {
      if(m_nodes[i].v >= 0) {
        m_nodes[i].v = (int)((long long)m_nodes[i].v * (long long)v % P);
      } else {
        m_nodes[i].va = (int)((long long)m_nodes[i].va * (long long)v % P);
        m_nodes[i].vm = (int)((long long)m_nodes[i].vm * (long long)v % P);
      }
      m_nodes[i].sum = (int)((long long)m_nodes[i].sum * (long long)v % P);
    }
    inline void _set(int i, int v) {
      m_nodes[i].v = v;
      m_nodes[i].va = 0;
      m_nodes[i].vm = 1;
      m_nodes[i].sum = (int)((long long)(m_nodes[i].r-m_nodes[i].l+1) * (long long)v % P);
    }
    inline void _sum(int i, int &res) {
      res += m_nodes[i].sum;
      if(res >= P)
        res -= P;
    }
    int m_sz;
    vector<Node> m_nodes;
};

int main()
{
  int n, q;
  vector<int> v;
  scanf("%d%d", &n, &q);
  for(int i = 0; i < n; i++) {
    int x;
    scanf("%d", &x);
    v.push_back(x);
  }
  SegmentTree st(v);
  //st.check();
  while(q--) {
    int type, x, y, v;
    scanf("%d", &type);
    switch(type) {
      case 1:
        scanf("%d%d%d", &x, &y, &v);
        st.add(x, y, v);
        break;
      case 2:
        scanf("%d%d%d", &x, &y, &v);
        st.mul(x, y, v);
        break;
      case 3:
        scanf("%d%d%d", &x, &y, &v);
        st.set(x, y, v);
        break;
      case 4:
        scanf("%d%d", &x, &y);
        printf("%d\n", st.sum(x, y));
        break;
      default:
        assert(0);
    }
    //st.check();
  }
  return 0;
}
