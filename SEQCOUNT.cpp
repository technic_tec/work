#include <stdio.h>
#include <string.h>
#define P 1000000007
#define N 100100
int n, k, d, c[2][N];
int main()
{
    scanf("%d%d%d", &n, &k, &d);
    n -= k*(k+1)/2;
    --d;
    if(n < 0 || d < 0) {
        printf("0\n");
        return 0;
    }
    for(int j = 0; j <= n; j++) {
        c[0][j] = (j == 0);
        c[1][j] = (j <= d);
    }
    for(int i = 2; i <= k; i++)
        for(int j = 0; j <= n; j++) {
            c[i&1][j] = c[(i-1)&1][j];
            if(j >= i) {
                c[i&1][j] += c[i&1][j-i];
                if(c[i&1][j] >= P)
                    c[i&1][j] -= P;
            }
            if(j >= (d+1)*i) {
                c[i&1][j] -= c[(i-1)&1][j-(d+1)*i];
                if(c[i&1][j] < 0)
                    c[i&1][j] += P;
                if(j % i == 0)
                    c[i&1][j] ++;
            }
        }
    printf("%d\n", c[k&1][n]);
    return 0;
}
