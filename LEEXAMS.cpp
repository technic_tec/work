/*
 * =====================================================================================
 *
 *       Filename:  LEEXAMS.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年09月13日 14时04分02秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

int n, p[N], a[N], b[N], v[N];

bool check(int mask)
{
    for(int i = 0; i < n; i++) {
        v[i] = (((mask>>i)&1) ? a[i] : b[i]);
        for(int j = 0; j < i; j++)
            if(v[j] == v[i])
                return false;
    }
    return true;
}

double prob(int mask)
{
    double prob = 1.0;
    for(int i = 0; i < n; i++)
        prob *= (((mask>>i)&1) ? p[i]/100.0 : (100.0-p[i])/100.0);
    return prob;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &n);
        for(int i = 0; i < n; i++)
            scanf("%d%d%d", &p[i], &a[i], &b[i]);
        if(n > 16)
            printf("%.9lf\n", 0.0);
        else {
            double pb = 0.0;
            for(int i = 0; i < (1<<n); i++)
                if(check(i))
                    pb += prob(i);
            printf("%.9lf\n", pb);
        }
    }
    return 0;
}

