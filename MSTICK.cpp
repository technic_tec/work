/*
 * =====================================================================================
 *
 *       Filename:  MSTICK.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年05月06日 14时19分49秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100100
#define K 20
#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))
#define INF 99999999

int n, bmn[K][N], bmx[K][N], q;

int get_min(int l, int r)
{
    int c = r-l+1, k = 0;
    if(c <= 0) return INF;
    while((1<<(k+1)) < c)
        ++k;
    return MIN(bmn[k][l], bmn[k][r+1-(1<<k)]);
}
int get_max(int l, int r)
{
    int c = r-l+1, k = 0;
    if(c <= 0) return -INF;
    while((1<<(k+1)) < c)
        ++k;
    return MAX(bmx[k][l], bmx[k][r+1-(1<<k)]);
}

int main()
{
    scanf("%d", &n);
    for(int i = 0; i < n; i++) {
        scanf("%d", &bmn[0][i]);
        bmx[0][i] = bmn[0][i];
    }
    for(int k = 1; (1<<k) < n; k++)
        for(int i = 0; i+(1<<k)<=n; i++) {
            bmn[k][i] = MIN(bmn[k-1][i],  bmn[k-1][i+(1<<(k-1))]);
            bmx[k][i] = MAX(bmx[k-1][i],  bmx[k-1][i+(1<<(k-1))]);
        }
    scanf("%d", &q);
    for(int i = 0; i < q; i++) {
        int l, r;
        scanf("%d%d", &l, &r);
        int t1 = 2*(get_min(l, r) + MAX(get_max(0, l-1), get_max(r+1, n-1)));
        int t2 = (l==r ? 2*get_min(l, r) : get_min(l, r) + get_max(l, r));
        int t = MAX(t1, t2);
        printf("%d.%d\n", t/2, ((t&1)?5:0));
    }
    return 0;
}

