#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define INF 1000000000
#define N 100100
enum Color {
    BLACK,
    RED
};

typedef int key_t;

struct node {
    Color color;
    key_t key;
    int delay, mdelay, offset;
    struct node *left;
    struct node *right;
    struct node *parent;
};

struct node *root, *nil;

void rb_tree_init()
{
    nil = new struct node;
    nil->color = BLACK;
    nil->key = -INF;
    nil->delay = nil->mdelay = -INF;
    nil->offset = 0;
    nil->left = nil->right = nil->parent = nil;
    root = nil;
}

struct node *stk[2*N];
int top;
void rb_tree_release()
{
    top = -1;
    if(root != nil)
        stk[++top] = root;
    while(top >= 0) {
        struct node *p = stk[top--];
        if(p->left != nil)
            stk[++top] = p->left;
        if(p->right != nil)
            stk[++top] = p->right;
        p->left = p->right = p->parent = NULL;
        delete p;
    }
    delete nil;
    nil = NULL;
}

inline struct node *grandparent(struct node *n)
{
    return n->parent->parent;
}

inline struct node *uncle(struct node *n)
{
    struct node *g = grandparent(n);
    if (g == nil)
        return nil; // No grandparent means no uncle
    if (n->parent == g->left)
        return g->right;
    else
        return g->left;
}

inline struct node *sibling(struct node *n)
{
    if (n == n->parent->left)
        return n->parent->right;
    else
        return n->parent->left;
}

inline void push(struct node *n)
{
    if(n->offset) {
        n->delay += n->offset;
        n->mdelay += n->offset;
        if(n->left != nil)
            n->left->offset += n->offset;
        if(n->right != nil)
            n->right->offset += n->offset;
        n->offset = 0;
    }
}

inline void synth(struct node *n)
{
    push(n);
    n->mdelay = MAX3(n->left->mdelay, n->right->mdelay, n->delay);
}

inline bool is_leaf(struct node *n)
{
    return (n == nil);
}

inline void rotate_left(struct node *n)
{
    //  n       x
    // y x ->  n
    //        y
    struct node *x = n->right;
    x->parent = n->parent;
    if(n->parent == nil)
        root = x;
    else if(n->parent->left == n)
        n->parent->left = x;
    else
        n->parent->right = x;
    n->right = x->left;
    if(x->left != nil)
        x->left->parent = n;
    x->left = n;
    n->parent = x;
    synth(n);
    synth(x);
}

inline void rotate_right(struct node *n)
{
    //  n     x
    // x y ->  n
    //          y
    struct node *x = n->left;
    x->parent = n->parent;
    if(n->parent == nil)
        root = x;
    else if(n->parent->left == n)
        n->parent->left = x;
    else
        n->parent->right = x;
    n->left = x->right;
    if(x->right != nil)
        x->right->parent = n;
    x->right = n;
    n->parent = x;
    synth(n);
    synth(x);
}

struct node *query_update(key_t key, int m, int &delay)
{
    struct node *p = root;
    while(p != nil && p->key != key)
        if(key < p->key && p->left != nil)
            p = p->left;
        else if(key > p->key && p->right != nil)
            p = p->right;
        else
            break;
    return p;
}

void insert_adjust(struct node *n);
void insert_key(key_t key, int m)
{
    int delay;
    struct node *p = query_update(key, m, delay);
    if(p == nil) {
        root = new struct node;
        root->key = key;
        root->color = BLACK;
        root->delay = delay;
        root->mdelay = delay;
        root->offset = 0;
        root->left = root->right = root->parent = nil;
        p = root;
    } else {
        struct node *pp = p;
        if(key < p->key)
            p = pp->left = new struct node;
        else if(key > p->key)
            p = pp->right = new struct node;
        else if(pp->left == nil)
            p = pp->left = new struct node;
        else if(pp->right == nil)
            p = pp->right = new struct node;
        else {
            pp = pp->right;
            while(pp->left != nil)
                pp = pp->left;
            p = pp->left = new struct node;
        }
        p->color = RED;
        p->key = key;
        p->delay = delay;
        p->mdelay = delay;
        p->offset = 0;
        p->left = p->right = nil;
        p->parent = pp;
        insert_adjust(p);
    }
}

void insert_adjust(struct node *n)
{
    struct node *u, *g;
    while(1) {
        if (n->parent == nil) {
            n->color = BLACK;
            break;
        } else if (n->parent->color == BLACK)
            break; /* Tree is still valid */
        else if (((u = uncle(n)) != nil) && (u->color == RED)) {
            //       g(B)     ->     g(R)
            //   y(R)    u(R)     y(B)   u(B)
            // n(R)             n(R)
            n->parent->color = BLACK;
            u->color = BLACK;
            synth(n);
            synth(n->parent);
            n = grandparent(n);
            n->color = RED;
            continue;
        } else {
            g = grandparent(n);

            if ((n == n->parent->right) && (n->parent == g->left)) {
                //       g(B)     ->     g(B)
                //   y(R)    u(B)     n(R)   u(B)
                //     n(R)         y(R) 
                rotate_left(n->parent);
                n = n->left;
            } else if ((n == n->parent->left) && (n->parent == g->right)) {
                //       g(B)     ->     g(B)
                //   u(B)    y(R)     u(B)   n(R)
                //         n(R)                y(R)
                rotate_right(n->parent);
                n = n->right;
            }

            n->parent->color = BLACK;
            g->color = RED;
            if (n == n->parent->left)
                //        g(B)     ->       g(R)     ->     y(B)
                //     y(R)   u(B)       y(B)   u(B)     n(R)  g(R)
                //   n(R)              n(R)                      u(B)
                rotate_right(g);
            else
                //      g(B)       ->      g(R)      ->     y(B)
                //   u(B)   y(R)       u(B)   y(B)       g(R)  n(R)
                //            n(R)              n(R)    u(B)
                rotate_left(g);
            break;
        }
    }
    for(struct node *q = n; q != nil; q = q->parent)
        synth(q);
}

int main()
{
    int n;
    rb_tree_init();
    scanf("%d", &n);
    for(int i = 0; i < n; i++) {
        int d, m;
        scanf("%d%d", &d, &m);
        insert_key(d, m);
        printf("%d\n", query());
    }
    rb_tree_release();
    return 0;
}
