/*
 * =====================================================================================
 *
 *       Filename:  NTHCIR.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月04日 15时15分40秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009

// 1/R3 + 1/R5 = 2(1/R2 + 1/R4 - 1/R1)
// tk = 1/Rk
// => tk = 2tk-1 - tk-2 + 2(t2-t1),  k>=5
//       = 3tk-1 - 3tk-2 + tk-3,  k>=6
inline double cir_get(int n, double r1, double r2, double r3, double r4)
{
  if(n >= 4) {
    return 1.0/((n-3)/r4+(n-3)/r2*(n-4) - (n-4)/r3-(n-3)/r1*(n-4));
  } else if(n == 3)
    return r3;
  else if(n == 2)
    return r2;
  else if(n == 1)
    return r1;
}

int main()
{
  int t;
  int n, p, m, b;
  double r1, r2, r3, r4;
  scanf("%d", &t);
  scanf("%d%d%d%d", &n, &p, &m, &b);
  scanf("%lf%lf%lf%lf", &r1, &r2, &r3, &r4);
  double rs = 0.0;
  while(t--) {
    n = (int)((long long)p*(long long)n%m)+b;
    double rx = cir_get(n, r1, r2, r3, r4);
    rs += rx;
  }
  printf("%.6lf\n", rs);
  return 0;
}
