/*
 * =====================================================================================
 *
 *       Filename:  CHAORNOT.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年06月12日 12时11分52秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>
#include <limits.h>
#include <sys/time.h>
#include <time.h>

#define N 100000

int b[N+10];
int v[N+10], m;
int c[N+10], n, cm[N+10], nm;

void pupdate(double p=1.0)
{
    memset(b, -1, sizeof(b));
    c[0] = v[0]; n = 1;
    b[v[0]] = 0;
    for(int k = 1; k < m; k++) {
        if(b[v[k]] >= 0) continue;
        unsigned key = rand();
        if((double)key > p * (double)INT_MAX)
            continue;
        c[n++] = v[k];
        for(int i = 0; i < n; i++)
            if(2*v[k]-c[i] <= N)
                b[2*v[k]-c[i]] = k;
    }
    if(n > nm) {
        //printf("%d->%d\n", nm, n);
        nm = n;
        for(int i = 0; i < n; i++)
            cm[i] = c[i];
    } //else 
        //printf("%d\n", n);
}
int main()
{
    struct timeval tval1, tval2;
    int seed = time(0);
    double prob = 0.95;
#if 1
    const char *sn = getenv("SEED");
    if(sn) {
        seed = atoi(sn);
        printf("seed %d\n", seed);
    }
    const char *sp = getenv("PROB");
    if(sp) {
        prob = atof(sp);
        printf("prob %f\n", prob);
    }
#endif
    gettimeofday(&tval1, NULL);
    scanf("%d", &m);
    for(int i = 0; i < m; i++)
        scanf("%d", &v[i]);
    std::sort(v, v+m);
    srand(seed);
    nm = -1;
    memset(b, -1, sizeof(b));
    n = 0;
    pupdate();
    int nrounds = 1;
    while(gettimeofday(&tval2, NULL) == 0 && (tval2.tv_sec - tval1.tv_sec)*1000000 + (tval2.tv_usec - tval1.tv_usec) <= 960000) {
        pupdate(prob);
        ++nrounds;
    }
    printf("%d %d\n", nm, nrounds);
#if 0
    for(int i = 0; i < nm; i++)
        printf("%d%c", cm[i], i<nm-1 ? ' ':'\n');
#endif
    return 0;
}

