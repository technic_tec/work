N = 10**18
m, v = 2, [1, 1]
while v[-1] <= N:
# c[2k, k] = c[2k-1][k-1] * 2
# c[2k+1, k] = c[2k, k] * (2k+1)/(k+1)
    v.append(v[-1] * m/((m+1)/2))
    m += 1

t = int(raw_input())
for _ in range(t):
    n = long(raw_input())
    l, h = 1, m-1
    if n <= v[l]:
        print l
    else:
        while h - l > 1:
            k = (h+l)/2
            if v[k] < n:
                l = k
            else:
                h = k
        print h
