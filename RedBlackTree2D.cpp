#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <ctime>
#include <algorithm>
#include <vector>
using namespace std;

#define N 10000000
#define P 100000007
#define INF 1000000009

/* 
 * RedBlackTree1D: Red Black Tree with all data in leaf nodes, internal nodes representing ranges. (Dynamic segment tree).
 * RangeTree2D: Static segment Tree associating a child segment tree with each node organizing all points, whose 1st dimension in the range, ordered by second dimension.
 */
template<typename key_t = int, typename val_t = int>
class RedBlackTree {
  public:
  enum Color {
    BLACK,
    RED
  };

  struct node {
    Color color;
    int tc;
    val_t val;
    key_t il, ir;
    struct node *left;
    struct node *right;
    struct node *parent;
    node(node *nil = NULL, key_t key = 0, int v = 0, Color c = BLACK)
    : color(c), tc(1), val(v), il(key), ir(key), left(nil), right(nil), parent(nil) {}
  };

  struct node *root, *nil;

  RedBlackTree()
  {
    nil = new node;
    nil->tc = 0;
    nil->il = INF; nil->ir = -INF;
    nil->left = nil->right = nil->parent = nil;
    root = nil;
  }

  virtual ~RedBlackTree()
  {
    vector<node *> stk;
    if(root != nil)
      stk.push_back(root);
    while(!stk.empty()) {
      struct node *p = stk.back();
      stk.pop_back();
      if(p->right != nil)
        stk.push_back(p->right);
      if(p->left != nil)
        stk.push_back(p->left);
      p->left = p->right = p->parent = NULL;
      delete p;
    }
    delete nil;
    nil = NULL;
  }

  inline struct node *grandparent(struct node *n)
  {
    return n->parent->parent;
  }

  inline struct node *uncle(struct node *n)
  {
    struct node *g = grandparent(n);
    if (g == nil)
      return nil; // No grandparent means no uncle
    if (n->parent == g->left)
      return g->right;
    else
      return g->left;
  }

  inline struct node *sibling(struct node *n)
  {
    if (n == n->parent->left)
      return n->parent->right;
    else
      return n->parent->left;
  }

  inline void synth(struct node *n)
  {
    if(!is_leaf(n)) {
      n->il = n->left->il; n->ir = n->right->ir;
      n->tc = n->left->tc + n->right->tc;
      synth_val(n);
    }
  }

  virtual void synth_val(struct node *n) {};

  inline bool is_leaf(struct node *n)
  {
    return n != nil && n->left == nil && n->right == nil;
  }

  inline void rotate_left(struct node *n)
  {
    //  n       x
    // y x ->  n
    //        y
    struct node *x = n->right;
    x->parent = n->parent;
    if(n->parent == nil)
      root = x;
    else if(n->parent->left == n)
      n->parent->left = x;
    else
      n->parent->right = x;
    n->right = x->left;
    if(x->left != nil)
      x->left->parent = n;
    x->left = n;
    n->parent = x;
    synth(n);
    synth(x);
  }

  inline void rotate_right(struct node *n)
  {
    //  n     x
    // x y ->  n
    //          y
    struct node *x = n->left;
    x->parent = n->parent;
    if(n->parent == nil)
      root = x;
    else if(n->parent->left == n)
      n->parent->left = x;
    else
      n->parent->right = x;
    n->left = x->right;
    if(x->right != nil)
      x->right->parent = n;
    x->right = n;
    n->parent = x;
    synth(n);
    synth(x);
  }

  inline struct node *find(key_t key)
  {
    struct node *ret = _find(key);
    return (ret==nil ? NULL : ret);
  }
  struct node *_find(key_t key, bool exact = true)
  {
    struct node *p = root;
    while(p != nil && !is_leaf(p))
      if(key < p->right->il)
        p = p->left;
      else
        p = p->right;
    if(exact) {
      if(p == nil || p->il != key)
        return nil;
      else
        return p;
    } else 
      return p;
  }

  struct node *_find_nth(int n)
  {
    struct node *p = root;
    if(n > root->tc)
      return nil;
    while(p != nil && !is_leaf(p))
      if(n < p->left->tc)
        p = p->left;
      else {
        n -= p->left->tc;
        p = p->right;
      }
    return p;
  }

  void insert_key(key_t key, int val = 0)
  {
    struct node *p = _find(key, false);
    if(p == nil) {
      p = root = new node(nil, key, val);
    } else {
      struct node *pp = new node(nil, key, val, RED);
      struct node *q = new node(nil, key, val);
      pp->parent = p->parent;
      p->parent = q->parent = pp;
      if(key < p->il) {
        pp->left = q; pp->right = p;
      } else {
        pp->left = p; pp->right = q;
      }
      if(pp->parent == nil)
        root = pp;
      else if(pp->parent->left == p)
        pp->parent->left = pp;
      else
        pp->parent->right = pp;
      for(q = pp; q != nil; q = q->parent)
        synth(q);
      insert_adjust(pp);
    }
  }

  void insert_adjust(struct node *n)
  {
    struct node *u, *g;
    while(1) {
      if (n->parent == nil) {
        n->color = BLACK;
        break;
      } else if (n->parent->color == BLACK)
        break; /* Tree is still valid */
      else if (((u = uncle(n)) != nil) && (u->color == RED)) {
        //       g(B)     ->     g(R)
        //   y(R)    u(R)     y(B)   u(B)
        // n(R)             n(R)
        n->parent->color = BLACK;
        u->color = BLACK;
        n = grandparent(n);
        n->color = RED;
        continue;
      } else {
        g = grandparent(n);

        if ((n == n->parent->right) && (n->parent == g->left)) {
          //       g(B)     ->     g(B)
          //   y(R)    u(B)     n(R)   u(B)
          //     n(R)         y(R) 
          rotate_left(n->parent);
          n = n->left;
        } else if ((n == n->parent->left) && (n->parent == g->right)) {
          //       g(B)     ->     g(B)
          //   u(B)    y(R)     u(B)   n(R)
          //         n(R)                y(R)
          rotate_right(n->parent);
          n = n->right;
        }

        n->parent->color = BLACK;
        g->color = RED;
        if (n == n->parent->left)
          //        g(B)     ->       g(R)     ->     y(B)
          //     y(R)   u(B)       y(B)   u(B)     n(R)  g(R)
          //   n(R)              n(R)                      u(B)
          rotate_right(g);
        else
          //      g(B)       ->      g(R)      ->     y(B)
          //   u(B)   y(R)       u(B)   y(B)       g(R)  n(R)
          //            n(R)              n(R)    u(B)
          rotate_left(g);
        break;
      }
    }
  }

  bool delete_key(key_t key)
  {
    struct node *p = _find(key);
    if(p == nil)
      return false;
    delete_one_child(p);
    return true;
  }

  void delete_one_child(struct node *n)
  {
    /*
     * Precondition: n has at most one non-null child.
     */
    if(n->parent == nil) {
      root = nil;
      return;
    }
    struct node *child = sibling(n);
    n = n->parent;
    struct node *n0 = n;

    if(n->parent == nil)
      root = child;
    else if(n->parent->left == n)
      n->parent->left = child;
    else
      n->parent->right = child;
    child->parent = n->parent;
    for(struct node *q = child->parent; q != nil; q = q->parent)
      synth(q);
    if (n->color == BLACK) {
      if (child->color == RED)
        //   n(B)   ->   c(B)
        // c(R) nil     
        child->color = BLACK;
      else {
        n = child;
        while(n->parent != nil) {
          struct node *s = sibling(n);
          if (s->color == RED) {
            //       y(B)             s(B)         y(B)         s(B)
            //   n(BB)  s(R)  ->   y(R)        s(R)  n(BB)  ->    y(R)
            //                   n(BB)                              n(BB)    
            n->parent->color = RED;
            s->color = BLACK;
            if (n == n->parent->left)
              rotate_left(n->parent);
            else
              rotate_right(n->parent);
          }

          s = sibling(n);

          if ((n->parent->color == BLACK) &&
              (s->color == BLACK) &&
              (s->left->color == BLACK) &&
              (s->right->color == BLACK)) {
            //       y(B)              y(BB)
            //   n(BB)  s(B)    ->  n(B)   s(R)
            //        z(B) w(B)          z(B) w(B)
            s->color = RED;
            n = n->parent;
            nil->parent = nil;
            continue;
          }
          else if ((n->parent->color == RED) &&
              (s->color == BLACK) &&
              (s->left->color == BLACK) &&
              (s->right->color == BLACK)) {
            //       y(R)              y(B)
            //   n(BB)  s(B)    ->  n(B)   s(R)
            //        z(B) w(B)          z(B) w(B)
            s->color = RED;
            n->parent->color = BLACK;
            break;
          } else {
            if ((n == n->parent->left) &&
                (s->right->color == BLACK) &&
                (s->left->color == RED)) {
              //       y(?)               y(?)
              //   n(BB)  s(B)    ->  n(BB)   z(B)
              //        z(R) w(B)               s(R)
              //                                  w(B)
              s->color = RED;
              s->left->color = BLACK;
              rotate_right(s);
            } else if ((n == n->parent->right) &&
                (s->left->color == BLACK) &&
                (s->right->color == RED)) {
              //       y(?)                 y(?)
              //   s(B)  n(BB)    ->    z(B)   n(BB)
              //w(B) z(R)             s(R)
              //                    w(B)
              s->color = RED;
              s->right->color = BLACK;
              rotate_left(s);
            }

            s = sibling(n);
            s->color = n->parent->color;
            n->parent->color = BLACK;

            if (n == n->parent->left) {
              //       y(X)                 s(X)
              //   n(BB)  s(B)    ->    y(B)     w(B)
              //        z(?) w(R)    n(B) z(?)
              s->right->color = BLACK;
              rotate_left(n->parent);
            } else {
              //       y(X)                 s(X)
              //   s(B)  n(BB)    ->    w(B)    y(B)
              //w(R) z(?)                    z(?) n(B)
              s->left->color = BLACK;
              rotate_right(n->parent);
            }
            break;
          }
        }
      }
    }
    nil->parent = nil;
    delete n0;
  }

  void check()
  {
    assert(root->color == BLACK);
    assert(root->parent == nil);
    assert(nil->color == BLACK);
    assert(nil->left == nil && nil->right == nil && nil->parent == nil);
    assert(nil->tc == 0);
    assert(nil->il > nil->ir);

    vector<node *> stk;
    vector<int> lstk;
    stk.reserve(N);
    lstk.reserve(N);
    stk.push_back(root);
    lstk.push_back(1);
    int nil_lvl = -1;
    while(!stk.empty()) {
      struct node *p = stk.back();
      int pl = lstk.back();
      stk.pop_back(); lstk.pop_back();
      if(p == nil) {
        if(nil_lvl == -1)
          nil_lvl = pl;
        else
          assert(nil_lvl == pl);
      } else {
        assert(p->color == BLACK || p->parent->color == BLACK);
        assert(root == p || p->parent != nil);
        if(is_leaf(p)) {
          assert(p->il == p->ir);
          assert(p->tc == 1);
        } else {
          assert(p->il < p->ir && p->il == p->left->il && p->ir == p->right->ir && p->left->ir < p->right->il);
          assert(p->left->parent == p && p->right->parent == p);
          assert(p->tc == p->left->tc + p->right->tc);
        }
      }
      if(p != nil) {
        stk.push_back(p->right);
        lstk.push_back(pl + (p->right->color == BLACK));
        stk.push_back(p->left);
        lstk.push_back(pl + (p->left->color == BLACK));
      }
    }
  }
};

struct Data {
  int s[4];
  Data(int val) {
    s[0] = (int)(bool)val;
    s[1] = val;
    s[2] = (int)((long long)s[1] * (long long)val % P);
    s[3] = (int)((long long)s[2] * (long long)val % P);
  }
};

class RedBlackTree1D : public RedBlackTree<int, Data> {
  virtual void synth_val(struct node *n) {
    for(int i = 0; i < 4; i++)
      n->val.s[i] = n->left->val.s[i] + n->right->val.s[i];
  }
};

class RangeTree2D {
  public:
    class Node {
      public:
        int l, r;
        int key;
        RedBlackTree1D tr;
    };
    RangeTree2D(int n);
    ~RangeTree2D();
    bool add(int x, int y, int v);
    bool remove(int x, int y);
    // rs[k] -- sum(v[x][y]^(k+1), xl<=x<=xr, yl<=y<=yr) modulo P
    void range_sum(int xl, int yl, int xr, int yr, int rs[3]);
    // count(v[x][y], xl<=x<=xr, yl<=y<=yr, v[x][y]!=0)
    int range_cnt(int xl, int yl, int xr, int yr);
  private:
};

int main(int argc, char *argv[])
{
  int seed;
  if(argc == 2)
    seed = atoi(argv[1]);
  else
    seed = time(NULL);
  printf("seed %d\n", seed);
  srand(seed);
  RedBlackTree<> rb;
  int cnt = 0;
  for(int i = 0; i < 10000; i++) {
    int v = rand() & 1023;
    if(rb.find(v)) {
      rb.delete_key(v);
      --cnt;
      //printf("delete %d\n", v);
    } else {
      rb.insert_key(v);
      ++cnt;
      //printf("insert %d\n", v);
    }
    rb.check();
    assert(rb.root->tc == cnt);
  }
}
