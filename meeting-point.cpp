/*
 * =====================================================================================
 *
 *       Filename:  meeting-point.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年11月28日 16时08分34秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 100100
#define INF 1100000000

int n, x[N], y[N], ix[N], iy[N];
long long sx, sy, d[N];

int compx(const void *a, const void *b)
{
    return (x[*(int *)a] < x[*(int *)b] ? -1 : (x[*(int *)a] > x[*(int *)b] ? 1 : 0));
}

int compy(const void *a, const void *b)
{
    return (y[*(int *)a] < y[*(int *)b] ? -1 : (y[*(int *)a] > y[*(int *)b] ? 1 : 0));
}
// u=x+y, v=x-y <=> x=(u+v)/2, y=(u-v)/2
// max(|x|, |y|) = max(|(u+v)/2|, |(u-v)/2|) = (|u|+|v|)/2
int main()
{
    scanf("%d", &n);
    sx = 0; sy = 0;
    for(int i = 0; i < n; i++) {
        int u, v;
        scanf("%d%d", &u, &v);
        x[i] = u+v; y[i] = u-v;
        sx += x[i]; sy += y[i];
        ix[i] = iy[i] = i; d[i] = 0;
    }
    qsort(ix, n, sizeof(int), compx);
    qsort(iy, n, sizeof(int), compy);
    long long cx = 0, cy = 0;
    for(int i = 0; i < n; i++) {
        d[ix[i]] += ((long long)i*(long long)x[ix[i]] - cx) + ((sx-cx) - (long long)(n-i)*(long long)x[ix[i]]);
        d[iy[i]] += ((long long)i*(long long)y[iy[i]] - cy) + ((sy-cy) - (long long)(n-i)*(long long)y[iy[i]]);
        cx += x[ix[i]]; cy += y[iy[i]];
    }
    long long dm = d[0];
    for(int i = 0; i < n; i++)
        if(d[i] < dm)
            dm = d[i];
    dm /= 2;
    printf("%lld\n", dm);
    return 0;
}

