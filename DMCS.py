N=65536
cls = [-1]*N
allcls = []

def bitcnt(x):
  c = 0
  while x:
    x &= (x-1)
    c += 1
  return c

def mirror(n, x):
  r = 0
  for i in range(16):
    if (i & (1<<x)):
      continue
    j = (i^(1<<x))
    r |= ((((n>>i)&1)<<j) | (((n>>j)&1)<<i))
  assert bitcnt(n) == bitcnt(r)
  return r

def rotate(n, x, y):
  r = 0
  for i in range(16):
    if i & ((1<<x)|(1<<y)):
      continue
    j = (i^(1<<x))
    k = (i^(1<<x)^(1<<y))
    l = (i^(1<<y))
    r |= ((((n>>i)&1)<<j) | (((n>>j)&1)<<k) | (((n>>k)&1)<<l) | (((n>>l)&1)<<i))
  assert bitcnt(n) == bitcnt(r)
  return r

def swap(n, x, y):
  r = 0
  for i in range(16):
    # (xyzw) -> (yxzw)
    ix, iy = (i>>x)&1, (i>>y)&1
    j = (i&~((1<<x)|(1<<y))) | (ix<<y) | (iy << x)
    r |= (((n >> j) & 1) << i)
  assert bitcnt(n) == bitcnt(r)
  return r

def classify(n, c):
  if cls[n] >= 0:
    return
  cls[n] = c
  allcls[c].append(n)
  # mirror x
  classify(mirror(n, 0), c)
  # rotate xy
  classify(rotate(n, 0, 1), c)
  # rotate xz
  classify(rotate(n, 0, 2), c)
  # rotate xw
  classify(rotate(n, 0, 3), c)

cf = [0]*N
def fill(x, st = 0):
  if x >= 16:
    cf[st] += 1
    return
  if ((st >> x) & 1) == 0:
    for m in [1, 2, 4, 8]:
      if (x & m) == 0 and ((st >> (x^m)) & 1) == 0:
        fill(x+1, st|(1<<x)|(1<<(x^m)))
  fill(x+1, st)

for i in range(N):
  if cls[i] < 0:
    allcls.append([])
    classify(i, len(allcls)-1)
print len(allcls)
for i, c in enumerate(allcls):
  c.sort()
fill(0)
cx = sx = xx = 0
for i, c in enumerate(allcls):
  cfx = cf[c[0]]
  if cfx:
    print i, cfx
    xx += 1
    cx += len(c)
    sx += len(c)*cfx
  for x in c:
    if cf[x] != cfx:
      print "%x:%d - %x:%d" % (c[0], cfx, x, cf[x])
    assert cf[x] == cfx
print xx, cx, sx

tf = [0] * len(allcls)
es = dict()
def totalfill(cx):
  if tf[cx]:
    return
  tf[cx] = 1
  x = allcls[cx][0]
  for m in range(N):
    if (x & m) == 0 and cf[m] > 0:
      ex = (cx, cls[~(x|m)])
      es[ex] = es.get(ex, 0)+cf[m]
      totalfill(ex[1])

totalfill(0)
print sum(tf), len(es), sum(es.values())
for (x, y), v in sorted(es.items()):
  print '%d -> %d:  %d' % (x, y, es[(x, y)])
