from random import seed, choice
from copy import deepcopy

class Graph(object):
  def __init__(self, arg):
    if isinstance(arg, list):
      adj = arg
      self.adj = []
      self.sz = len(adj)
      for i in range(self.sz):
        assert(len(adj[i]) == self.sz)
        assert(adj[i][i] == '0')
        for j in range(i+1, self.sz):
          assert(adj[i][j] == adj[j][i])
        self.adj.append(int(adj[i][::-1], 2))
    elif isinstance(arg, int):
      self.sz = arg
      self.adj = [0]*self.sz
    else:
      raise ValueError
    self.cply = dict({0:0})
    self.cpth = dict()

  def add_node(self, adju):
    assert min(adju) >= 0 and max(adju) < self.sz
    u = self.sz
    self.adj.append(0)
    self.sz += 1
    for v in adju:
      self.adj[-1] |= (1<<v)
      self.adj[v] |= (1<<u)
    
  def count_poly(self, m = None):
    if m is None:
      m = (1<<self.sz)-1
    if m in self.cply:
      return self.cply[m]
    x = 0
    while ((m >> x) & 1) == 0:
      x += 1
    cnt = self.count_poly(m&~(1<<x))
    for i in range(x+1, self.sz):
      if ((m >> i) & 1) and ((self.adj[x] >> i) & 1):
        for j in range(i+1, self.sz):
          if((m >> j) & 1) and ((self.adj[x] >> j) & 1):
            cnt += self.count_path(i, j, m&~(1<<x))
    self.cply[m] = cnt
    return cnt

  def count_path(self, s, t, m = None):
    if m is None:
      m = (1<<self.sz)-1
    assert s != t and ((m>>s)&1) and ((m>>t)&1)
    if (m, s, t) in self.cpth:
      return self.cpth[(m, s, t)]
    cnt = 0
    if (self.adj[s] >> t) & 1:
      cnt += 1
    for i in range(self.sz):
      if i!=t and ((m>>i)&1) and ((self.adj[s]>>i)&1):
        cnt += self.count_path(i, t, m&~(1<<s))
    self.cpth[(m, s, t)] = cnt
    return cnt

s = """011111
101001
110100
101010
100101
110010""".splitlines()
print Graph(s).count_poly()

# (0, 1, 2, 3)
# -> (0, 1, 3, 4) (1, 2, 3, 5) (0, 2, 3, 6)
# -> (0, 1, 4, 7), (1, 3, 4, 8), (0, 3, 4, 9), (1, 2, 5, 10), (2, 3, 5, 11), (1, 3, 5, 12), (0, 2, 6, 13), (2, 3, 6, 14), (0, 3, 6, 15)
seed()
opt = (0, '')
try:
  while True:
    s2 = [['0']*16 for _ in range(16)]
    tr = set([(0, 1, 2)])
    s2[0][1] = s2[1][0] = '1'
    s2[0][2] = s2[2][0] = '1'
    s2[1][2] = s2[2][1] = '1'
    for i in range(3, 16):
      a, b, c = choice(list(tr))
      tr.remove((a, b, c))
      s2[i][a] = s2[a][i] = '1'
      s2[i][b] = s2[b][i] = '1'
      s2[i][c] = s2[c][i] = '1'
      tr |= set([(a, b, i), (b, c, i), (a, c, i)])
    s = [''.join(x) for x in s2]
    c = Graph(s).count_poly()
    if c > opt[0]:
      opt = (c, '\n'.join(s))
    print c
except KeyboardInterrupt:
  print opt[0]
  print opt[1]
