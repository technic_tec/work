#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <deque>
#include <vector>
using namespace std;

#define INF 99999999

class Dinic {
    public:
        Dinic(int nNodes)
        {
            m_nNodes = nNodes;
            adj.clear();
            radj.clear();
            for(int i = 0; i < nNodes; i++) {
                adj.push_back(-1);
                radj.push_back(-1);
            }
            dst.clear();
            nxt.clear();
            rdst.clear();
            rnxt.clear();
            cap.clear();
            flow.clear();
        }

        int add_edge(int _s, int _t, int _c = 1)
        {
            int ne = dst.size();
            dst.push_back(_t);
            nxt.push_back(adj[_s]);
            adj[_s] = ne;
            rdst.push_back(_s);
            rnxt.push_back(radj[_t]);
            radj[_t] = ne;
            cap.push_back(_c);
            flow.push_back(0);
        }

        int maximum_flow(int s, int t)
        {
            int mf = 0;
            while (BFS(s, t))
                mf += DFS(s, t);
            return mf;
        }

    private:
        bool BFS(int s, int t)
        {
            deque<int> Q;
            Q.clear();
            Dist.clear();
            blocking.clear();
            for(int v = 0; v < m_nNodes; v++) {
                Dist.push_back(INF);
                blocking.push_back(false);
            }
            Dist[s] = 0;
            Q.push_back(s);
            while (!Q.empty()) {
                int v = Q.front();
                Q.pop_front();
                if(Dist[v] < Dist[t]) {
                    for(int e = adj[v]; e >= 0; e = nxt[e]) {
                        int u = dst[e], c = cap[e], f = flow[e];
                        if(f < c && Dist[u] == INF) {
                            //printf("BFS G[%d]-->G[%d]\n", v, u);
                            Dist[u] = Dist[v] + 1;
                            Q.push_back(u);
                        }
                    }
                    for(int e = radj[v]; e >= 0; e = rnxt[e]) {
                        int u = rdst[e], c = cap[e], f = flow[e];
                        if(f > 0 && Dist[u] == INF) {
                            //printf("BFS G[%d]-->G[%d]\n", v, u);
                            Dist[u] = Dist[v] + 1;
                            Q.push_back(u);
                        }
                    }
                }
            }
            return (Dist[t] != INF);
        }

        int DFS(int v, int t, int fl = INF)
        {
            int fl0 = fl;
            if(v == t) {
                return fl;
            } else {
                for(int e = adj[v]; e >= 0; e = nxt[e]) {
                    int u = dst[e], c = cap[e], f = flow[e];
                    if(Dist[u] == Dist[v]+1 && f < c && !blocking[u]) {
                        //printf("DFS G[%d]-->G[%d]\n", v, u);
                        int res = DFS(u, t, min(fl, c-f));
                        flow[e] += res;
                        fl -= res;
                        if(fl == 0)
                            break;
                    }
                }
                for(int e = radj[v]; e >= 0; e = rnxt[e]) {
                    int u = rdst[e], c = cap[e], f = flow[e];
                    if(Dist[u] == Dist[v]+1 && f > 0 && !blocking[u]) {
                        //printf("DFS G[%d]-->G[%d]\n", v, u);
                        int res = DFS(u, t, min(fl, f));
                        flow[e] -= res;
                        fl -= res;
                        if(fl == 0)
                            break;
                    }
                }
                if(fl0 == fl)
                    blocking[v] = true;
                return fl0 - fl;
            }
        }

        int m_nNodes;
        vector<int> adj, dst, nxt;
        vector<int> radj, rdst, rnxt;
        vector<int> cap, flow, Dist;
        vector<bool> blocking;
};

const int n = 21;
const int e[][2] = {
    {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5},
    {0, 6}, {0, 7}, {0, 8}, {0, 9}, {0, 10},
    {1, 11}, {2, 13}, {5, 14}, {3, 15}, {4, 14},
    {7, 18}, {8, 17}, {6, 19}, {9, 14}, {3, 19},
    {11, 20}, {12, 20}, {13, 20}, {14, 20}, {15, 20},
    {16, 20}, {17, 20}, {18, 20}, {19, 20}, {20, 20}
};
int main()
{
    Dinic mf(n);
    for(int j = 0; j < sizeof(e)/sizeof(e[0]); j++)
        mf.add_edge(e[j][0], e[j][1]);
    printf("%d\n", mf.maximum_flow(0, n-1));
    return 0;
}
