/*
 * =====================================================================================
 *
 *       Filename:  CHNGSS.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年03月15日 11时48分04秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec)
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <map>
using namespace std;

#define P 1000000007
#define N 100000000

typedef pair<pair<int, int>, pair<int, int> > Rect;
#define RECT(iL, iR, jL, jR) make_pair(make_pair(iL, iR), make_pair(jL, jR))

class Matrix {
  public:
    Matrix(int _n, int _m, int _c) : n(_n), m(_m), c(_c), m_nq(500000), m_v(n+1, vector<int>(m+1, -1)) {
      init_unknowns(1, n, 1, m);
    }
    bool locate(int val) {
      if(!m_nq) return true;
      int total = ask1(1, n, 1, m, val, val);
      int unknown = _locate(1, n, 1, m, val, total);
      return (unknown == 0 || m_nq == 0);
    }
    inline int ask1(int iL, int iR, int jL, int jR, int x, int y) {
      int ans;
      printf("1 %d %d %d %d %d %d\n", iL, iR, jL, jR, x, y);
      fflush(stdout);
      --m_nq;
      scanf("%d", &ans);
      return ans;
    }
    inline int ask2(int iL, int iR, int jL, int jR) {
      int ans;
      printf("2 %d %d %d %d\n", iL, iR, jL, jR);
      fflush(stdout);
      --m_nq;
      scanf("%d", &ans);
      return ans;
    }
    inline void answer() {
      printf("3\n");
      for(int i = 1; i <= n; i++)
        for(int j = 1; j <= m; j++)
          printf("%d%c", guess(i, j), j<m?' ':'\n');
    }
  private:
    inline int guess(int x, int y) {
      return m_v[x][y] < 0 ? 25 : m_v[x][y];
    }
    int _locate(int iL, int iR, int jL, int jR, int val, int total) {
      int unknown = m_unknowns[RECT(iL, iR, jL, jR)];
      if(unknown == 0 || total == 0)
        return unknown;
      else if(unknown == total) {
        for(int i = iL; i <= iR; i++)
          for(int j = jL; j <= jR; j++)
            if(m_v[i][j] < 0)
              m_v[i][j] = val;
        unknown -= total;
        m_unknowns[RECT(iL, iR, jL, jR)] = unknown;
        return unknown;
      }
      if(m_nq == 0)
        return unknown;
      if(jR-jL > iR-iL) {
        int jM = (jL+jR)/2;
        int cnt = ask1(iL, iR, jL, jM, val, val);
        unknown = _locate(iL, iR, jL, jM, val, cnt);
        unknown += _locate(iL, iR, jM+1, jR, val, total-cnt);
      } else {
        assert(iR > iL);
        int iM = (iL+iR)/2;
        int cnt = ask1(iL, iM, jL, jR, val, val);
        unknown = _locate(iL, iM, jL, jR, val, cnt);
        unknown += _locate(iM+1, iR, jL, jR, val, total-cnt);
      }
      m_unknowns[RECT(iL, iR, jL, jR)] = unknown;
      return unknown;
    }
    void init_unknowns(int iL, int iR, int jL, int jR) {
      m_unknowns[RECT(iL, iR, jL, jR)] = (iR-iL+1)*(jR-jL+1);
      if(jR-jL > iR-iL) {
        int jM = (jL+jR)/2;
        init_unknowns(iL, iR, jL, jM);
        init_unknowns(iL, iR, jM+1, jR);
      } else if(iR > iL) {
        int iM = (iL+iR)/2;
        init_unknowns(iL, iM, jL, jR);
        init_unknowns(iM+1, iR, jL, jR);
      }
    }
    int n, m, c;
    vector<vector<int> > m_v;
    map<Rect, int> m_unknowns;
    int m_nq;
};
int main()
{
  int n, m, c;
  scanf("%d%d%d", &n, &m, &c);
  Matrix mat(n, m, c);
  bool done = false;
  for(int i = 1; i < 50 && !done; i++)
    done = mat.locate((i&1) ? 50-(i>>1) : (i>>1));
  mat.answer();
  return 0;
}
