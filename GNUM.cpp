#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <deque>
#include <vector>
#include <map>
using namespace std;

#define INF 99999999

class Dinic {
    public:
        Dinic(int nNodes)
        {
            m_nNodes = nNodes;
            adj.clear();
            radj.clear();
            for(int i = 0; i < nNodes; i++) {
                adj.push_back(-1);
                radj.push_back(-1);
            }
            dst.clear();
            nxt.clear();
            rdst.clear();
            rnxt.clear();
            cap.clear();
            flow.clear();
        }

        int add_edge(int _s, int _t, int _c = 1)
        {
            int ne = dst.size();
            dst.push_back(_t);
            nxt.push_back(adj[_s]);
            adj[_s] = ne;
            rdst.push_back(_s);
            rnxt.push_back(radj[_t]);
            radj[_t] = ne;
            cap.push_back(_c);
            flow.push_back(0);
        }

        int maximum_flow(int s, int t)
        {
            int mf = 0;
            while (BFS(s, t))
                mf += DFS(s, t);
            return mf;
        }

    private:
        bool BFS(int s, int t)
        {
            deque<int> Q;
            Q.clear();
            Dist.clear();
            blocking.clear();
            for(int v = 0; v < m_nNodes; v++) {
                Dist.push_back(INF);
                blocking.push_back(false);
            }
            Dist[s] = 0;
            Q.push_back(s);
            while (!Q.empty()) {
                int v = Q.front();
                Q.pop_front();
                if(Dist[v] < Dist[t]) {
                    for(int e = adj[v]; e >= 0; e = nxt[e]) {
                        int u = dst[e], c = cap[e], f = flow[e];
                        if(f < c && Dist[u] == INF) {
                            //printf("BFS G[%d]-->G[%d]\n", v, u);
                            Dist[u] = Dist[v] + 1;
                            Q.push_back(u);
                        }
                    }
                    for(int e = radj[v]; e >= 0; e = rnxt[e]) {
                        int u = rdst[e], c = cap[e], f = flow[e];
                        if(f > 0 && Dist[u] == INF) {
                            //printf("BFS G[%d]-->G[%d]\n", v, u);
                            Dist[u] = Dist[v] + 1;
                            Q.push_back(u);
                        }
                    }
                }
            }
            return (Dist[t] != INF);
        }

        int DFS(int v, int t, int fl = INF)
        {
            int fl0 = fl;
            if(v == t) {
                return fl;
            } else {
                for(int e = adj[v]; e >= 0; e = nxt[e]) {
                    int u = dst[e], c = cap[e], f = flow[e];
                    if(Dist[u] == Dist[v]+1 && f < c && !blocking[u]) {
                        //printf("DFS G[%d]-->G[%d]\n", v, u);
                        int res = DFS(u, t, min(fl, c-f));
                        flow[e] += res;
                        fl -= res;
                        if(fl == 0)
                            break;
                    }
                }
                for(int e = radj[v]; e >= 0; e = rnxt[e]) {
                    int u = rdst[e], c = cap[e], f = flow[e];
                    if(Dist[u] == Dist[v]+1 && f > 0 && !blocking[u]) {
                        //printf("DFS G[%d]-->G[%d]\n", v, u);
                        int res = DFS(u, t, min(fl, f));
                        flow[e] -= res;
                        fl -= res;
                        if(fl == 0)
                            break;
                    }
                }
                if(fl0 == fl)
                    blocking[v] = true;
                return fl0 - fl;
            }
        }

        int m_nNodes;
        vector<int> adj, dst, nxt;
        vector<int> radj, rdst, rnxt;
        vector<int> cap, flow, Dist;
        vector<bool> blocking;
};

#define N 32000
#define K 500
bool isp[N];
int prm[N], np;
void precompute()
{
    memset(isp, true, sizeof(isp));
    isp[0] = isp[1] = false;
    np = 0;
    for(int i = 2; i < N; i++) {
        if(!isp[i]) continue;
        prm[np++] = i;
        for(int j = i*i; j < N; j += i)
            isp[j] = false;
    }
}

int factor(int n, int f[])
{
    int nf = 0;
    for(int i = 0; i < np && prm[i]*prm[i] <= n; i++)
        if(n % prm[i] == 0) {
            f[nf++] = prm[i];
            while(n % prm[i] == 0)
                n /= prm[i];
        }
    if(n > 1)
        f[nf++] = n;
    return nf;
}

int gcd(int fa[], int nfa, int fb[], int nfb, int fc[])
{
    int nfc = 0;
    int i = 0, j = 0;
    while(i < nfa && j < nfb) {
        if(fa[i] < fb[j])
            ++i;
        else if(fa[i] > fb[j])
            ++j;
        else {
            fc[nfc++] = fa[i];
            ++i; ++j;
        }
    }
    return nfc;
}

int n, a[K], b[K], fa[K][10], fb[K][10], nfa[K], nfb[K];
int main()
{
    precompute();
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &n);
        for(int i = 0; i < n; i++) {
            scanf("%d", &a[i]);
            nfa[i] = factor(a[i], fa[i]);
        }
        for(int j = 0; j < n; j++) {
            scanf("%d", &b[j]);
            nfb[j] = factor(b[j], fb[j]);
        }
        int nNode = 0;
        map<int, int> cf;
        for(int i = 0; i < n; i++)
            for(int j = 0; j < n; j++) {
                if(a[i] == b[j]) continue;
                int nfc, fc[10];
                nfc = gcd(fa[i], nfa[i], fb[j], nfb[j], fc);
                if(nfc == 0) continue;
                if(a[i] > b[j])
                    for(int k = 0; k < nfc; k++) {
                        map<int, int>::iterator it = cf.find(fc[k]);
                        if(it == cf.end())
                            cf[fc[k]] = 1;
                        else
                            cf[fc[k]] = it->second | 1;
                    }
                else
                    for(int k = 0; k < nfc; k++) {
                        map<int, int>::iterator it = cf.find(fc[k]);
                        if(it == cf.end())
                            cf[fc[k]] = 2;
                        else
                            cf[fc[k]] = it->second | 2;
                    }
            }
        int src = nNode++;
        int sink = nNode++;
        map<int, int> fm;
        vector<vector<int> > dm(n, vector<int>(n, -1));
        for(map<int, int>::iterator it = cf.begin(); it != cf.end(); ++it) {
            if((*it).second == 3)
                fm[(*it).first] = nNode++;
        }
        for(int i = 0; i < n; i++)
            for(int j = 0; j < n; j++) {
                if(a[i] == b[j]) continue;
                int nfc, fc[10];
                nfc = gcd(fa[i], nfa[i], fb[j], nfb[j], fc);
                if(nfc != 0) dm[i][j] = nNode++;
            }
        Dinic mf(nNode);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < n; j++) {
                if(a[i] == b[j]) continue;
                int nfc, fc[10];
                nfc = gcd(fa[i], nfa[i], fb[j], nfb[j], fc);
                if(nfc == 0) continue;
                if(a[i] > b[j]) {
                    mf.add_edge(src, dm[i][j]);
                    for(int k = 0; k < nfc; k++)
                        if(cf[fc[k]] == 3)
                            mf.add_edge(dm[i][j], fm[fc[k]]);
                } else {
                    for(int k = 0; k < nfc; k++)
                        if(cf[fc[k]] == 3)
                            mf.add_edge(fm[fc[k]], dm[i][j]);
                    mf.add_edge(dm[i][j], sink);
                }
            }
        printf("%d\n", mf.maximum_flow(src, sink));
    }
    return 0;
}
