#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 1002000

int n, k, v[N], q[N], w[N];

int query(int n, int k, int v[], int q[])
{
    int l = 0, r = n-1;
    int nq = 0;
    memcpy(w, v, n*sizeof(int));
    std::sort(w, w+n);
    while(l < n && v[l] == w[l])
        ++l;
    if(l == n)
        return 0;
    while(r >= 0 && v[r] == w[r])
        --r;
    while(l <= r) {
        int x = r-(2*k/3)+1 < l ? l : r-(2*k/3)+1;
        int i = 0;
        while(v[i] < w[x])
            ++i;
        while(i+k <= r) {
            q[nq++] = i;
            std::sort(v+i, v+i+k);
            while(v[i] < w[x])
                ++i;
        }
        q[nq++] = i;
        std::sort(v+i, v+std::min(i+k, n));
        r = x-1;
        while(l <= r && v[l] == w[l])
            ++l;
        while(r >= l && v[r] == w[r])
            --r;
    }
    return nq;
}
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d", &n, &k);
        for(int i = 0; i < n; i++)
            scanf("%d", &v[i]);
        int nq = query(n, k, v, q);
        printf("%d\n", nq);
        for(int i = 0; i < nq; i++)
            printf("%d%c", q[i]+1, i<nq-1?' ':'\n');
    }
    return 0;
}

