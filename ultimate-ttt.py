def check_result(b):
    assert (len(b)==3 and [len(br) for br in b] == [3]*3)
    bs = b[0]+b[1]+b[2]
    for x in (bs[:3], bs[3:6], bs[6:9], bs[0::3], bs[1::3], bs[2::3], bs[0::4], bs[2::2]):
        if x == 'XXX' or x == 'OOO':
            return x[0]
    if '-' in bs:
        return None
    else:
        return 'Draw'

def choose_board(b):
    for bx in range(3):
        for by in range(3):
            if check_result([br[3*by:3*(by+1)] for br in b[3*bx:3*(bx+1)]]) == None:
                return (bx, by)

def place(b, i, j, p):
    r = []
    for k in range(3):
        if k == i:
            r.append(b[i][:j] + p + b[i][j+1:])
        else:
            r.append(b[i])
    return r

def play_board(b):
    assert (len(b)==3 and [len(br) for br in b] == [3]*3)
    for i in range(3):
        for j in range(3):
            if b[i][j] == '-' and (check_result(place(b, i, j, 'O')) == 'O' or check_result(place(b,  i,  j,  'X')) == 'X'):
                return (i, j)
    for i, j in [(1, 1), (0, 1), (1, 0), (1, 2), (2, 1), (0, 0), (0, 2), (2, 0), (2, 2)]:
        if b[i][j] == '-':
            return (i, j)

player = raw_input().strip()
bx, by = map(int, raw_input().split())
b = []
for i in range(9):
    b.append(raw_input().strip())
if (bx, by) == (-1, -1):
    bx, by = choose_board(b)
px, py = play_board([br[3*by:3*(by+1)] for br in b[3*bx:3*(bx+1)]])
print bx, by, px, py
