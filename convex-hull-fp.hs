import Text.Printf
import Data.List

convex_hull :: [(Int, Int)] -> [(Int, Int)]
convex_hull points = (convex_bound pts []) ++ (convex_bound (reverse pts) [])
  where pts = sort points
        convex_bound [] res = res
        convex_bound ((x, y):ps) [] = convex_bound ps ((x, y):[])
        convex_bound ((x, y):ps) ((x0, y0):[]) = convex_bound ps ((x, y):(x0, y0):[])
        convex_bound ((x, y):ps) ((x1, y1):(x0, y0):rs) | csp < 0 = convex_bound ps ((x,  y):(x1,  y1):(x0,  y0):rs) 
                                                        | otherwise = convex_bound ((x,  y):ps) ((x0,  y0):rs)
           where csp = (x1-x0)*(y-y1) - (y1-y0)*(x-x1)

distance :: (Int, Int) -> (Int, Int) -> Double
distance (x1, y1) (x2, y2) = sqrt (fromIntegral ((x1-x2)^2+(y1-y2)^2))

pathlen :: [(Int, Int)] -> Double
pathlen [] = 0
pathlen [(x, y)] = 0
pathlen ((x1, y1):(x2, y2):ps) = (distance (x1, y1) (x2, y2)) + (pathlen ((x2, y2):ps))

peremeter :: [(Int, Int)] -> Double
peremeter [] = 0
peremeter (c:cs) = pathlen ((c:cs)++[c])

solve :: [(Int,  Int)] -> Double
solve points = cvp
    where
      cv = convex_hull points
      cvp = peremeter cv

main :: IO ()
main = do
  n <- readLn :: IO Int
  content <- getContents
  let  
    points = map (\[x,  y] -> (x,  y)). map (map (read::String->Int)). map words. lines $ content
    ans = solve points
  printf "%.1f\n" ans
