#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

const double eps = 1e-8;
int n, x[N], y[N];

bool incircle(int a, int b, int c, int u)
{
    if((x[c]-x[b])*(y[b]-y[a]) == (x[b]-x[a])*(y[c]-y[b]))
        return false;
    int tu = (x[u]-x[a])*(y[b]-y[a])-(y[u]-y[a])*(x[b]-x[a]);
    int tc = (x[c]-x[a])*(y[b]-y[a])-(y[c]-y[a])*(x[b]-x[a]);
    double cu = (double)((x[a]-x[u])*(x[b]-x[u])+(y[a]-y[u])*(y[b]-y[u]))/(sqrt((double)((x[a]-x[u])*(x[a]-x[u])+(y[a]-y[u])*(y[a]-y[u])))*sqrt((double)((x[b]-x[u])*(x[b]-x[u])+(y[b]-y[u])*(y[b]-y[u]))));
    double cc = (double)((x[a]-x[c])*(x[b]-x[c])+(y[a]-y[c])*(y[b]-y[c]))/(sqrt((double)((x[a]-x[c])*(x[a]-x[c])+(y[a]-y[c])*(y[a]-y[c])))*sqrt((double)((x[b]-x[c])*(x[b]-x[c])+(y[b]-y[c])*(y[b]-y[c]))));
    if(tu == 0){
        return cu <= eps;
    } else if((tu > 0 && tc < 0) || (tu < 0 && tc > 0)) {
        // U >= pi-C => cosU <= -cosC
        return cu + cc <= eps;
    } else {
        // U >= C => cosU <= cosC
        return cu - cc <= eps;
    }
    return false;
}
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &n);
        for(int i = 0; i < n; i++)
            scanf("%d%d", &x[i], &y[i]);
        int c = 0, s = 0;
        for(int i = 0; i < n; i++)
            for(int j = i+1; j < n; j++)
                for(int k = j+1; k < n; k++)
                    for(int u = 0; u < n; u++)
                        if(u != i && u != j && u != k) {
                            s++;
                            if(incircle(i, j, k, u))
                                c++;
                        }
        printf("%.6lf\n", (double)c/(double)s);
    }
    return 0;
}

