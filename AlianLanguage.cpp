#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100100
#define M 500500
#define K 22
#define P 1000000007

int c[M], w[N][K], s[K];
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        for(int i = n; 2*i > n; i--) {
            w[i][0] = 1;
            w[i][1] = -1;
        }
        int j = n+1;
        memset(s, -1, sizeof(s));
        for(int i = n/2; i > 0; i--) {
            while(j > 2*i) {
                --j;
                for(int k = 0; k < K && w[j][k] >= 0; k++) {
                    if(s[k] < 0) s[k] = 0;
                    s[k] += w[j][k];
                    if(s[k] >= P) s[k] -= P;
                }
            }
            w[i][0] = 0;
            for(int k = 0; k+1 < K && (k==0||s[k-1] >= 0); k++)
                w[i][k+1] = s[k];
        }
        for(int k = 0; k < K && w[1][k] >= 0; k++) {
            if(s[k] < 0) s[k] = 0;
            s[k] += w[1][k];
            if(s[k] >= P) s[k] -= P;
        }
        c[0] = 1;
        for(int i = 1; i <= m; i++) {
            c[i] = 0;
            for(int j = 0; j < K && j < i && s[j] >= 0; j++) {
                c[i] += (int)((long long)c[i-j-1]*(long long)s[j]%P);
                if(c[i] >= P) c[i] -= P;
            }
        }
        printf("%d\n", c[m]);
    }
    return 0;
}

