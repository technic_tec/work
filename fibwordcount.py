def fib_count(n, s):
    c = 0
    q = [(n, s)]
    while q[0][0] < q[-1][0]) or (q[0][0] > 1 and max([len(y) for x, y in q]) <= 2:
        n, s = q.pop(0)
        for sx in revert(s):
            q.append(n-1, sx)
    return c

#P0(k)=ceiling(P1(P1(k)))
#P1(k)=k+count(k-1, 1)
#     = P1(k-1)+1+(s[k-1]==1)
print fib_count(3)
print fib_count(4)
print fib_count(5)
print fib_count(6)
