N = 10
c = [[[[0]*(N+1) for k in range(N+1)] for j in range(N+1)] for i in range(N+1)]
for i in range(N+1):
    for j in range(N+1):
        for k in range(N+1):
            for l in range(N+1):
                cs = set([c[t][j][k][l] for t in range(i)] + [c[i][t][k][l] for t in range(j)] + [c[i][j][t][l] for t in range(k)] + [c[i][j][k][t] for t in range(l)])
                x = 0
                while x in cs:
                    x += 1
                c[i][j][k][l] = x
                if x == 0 and i <= j and j <= k and k <= l:
                    print i,j,k,l
print c
# l+r = w-1, u+d=h-1 l^r^u^d=0