def compose(a, b):
    assert(len(a)==len(b))
    c = []
    for x in a:
        c.append(b[x])
    return c

fi = open('shufflegold.in')
fo = open('shufflegold.out', 'w')
n, m, q = map(int, fi.readline().split())
v = range(n)
for i in range(m):
    x = int(fi.readline())
    v[x-1] = i
v = v[1:]+v[:1]
vr = range(n)
k = n-m+1
while k > 0:
    if k & 1:
        vr = compose(v, vr)
    v = compose(v, v)
    k >>= 1
vr = vr[m-1:]+vr[:m-1]
vr = vr[::-1]

for _ in range(q):
    x = int(fi.readline())
    fo.write('%d\n' % (vr[x-1]+1))
fi.close()
fo.close()
