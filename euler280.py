from pprint import pprint
import numpy as np 
from itertools import izip, permutations

# Absorbing_Markov_chain
# N = I + M + M^2 + ... = (I-M)^(-1) (Fundamental matrix)
# B = R + MR + M^2R + ... = NR     (Absorbing probabilities for each absorbing state)
# T = R + 2MR + 3M^2R + ... = NB  (Expected number of steps to each absorbing state)
r = [0, 16, 8, 24, 4, 20, 12, 28, 2, 18, 10, 26, 6, 22, 14, 30, 1, 17, 9, 25, 5, 21, 13, 29, 3, 19, 11, 27, 7, 23, 15, 31]
m0 = np.asmatrix([[(1.0/(2+(x>=5 and x<20)+(x%5>0 and x%5<4)) if abs(x-y) == 1 and x/5==y/5 or abs(x-y)==5 else 0.0) for y in range(25)] for x in range(25)])
steps = [None]*32
probs = [None]*32
for x in range(1, 32):
#  if x <= r[x]:
    m = m0.copy()
    tr = filter(lambda i: ((x>>i)&1)==0, range(25))
    ntr = filter(lambda i: ((x>>i)&1)!=0, range(25))
    M = m[np.ix_(tr, tr)]
    R = m[np.ix_(tr, ntr)]
    N = (np.eye(M.shape[0]) - M).getI()
    B = np.asmatrix([[0.0]*25 for _ in range(25)])
    T = np.asmatrix([[0.0]*25 for _ in range(25)])
    print x, m, M, R, N, B, T
    B[np.ix_(tr, ntr)] = N*R
    T[np.ix_(tr, ntr)] = N*N*R
    probs[x], steps[x] = B, T

avg = [[None]*32 for _ in range(32)]
avg[0][0] = [0.0]*25
def solve(x, y):
  if avg[x][y] is None:
    t, p = steps[x], probs[x]
    res = [0.0]*25
    for i in range(5):
      if ((x>>i)&1):
        rem = solve(y, x&(~(1<<i)))[20+i]
        for u in range(25):
          res[u] += p[u,i]*(t[u, i]+rem)
    #print 'avg(%d, %d)=%s' % (x, y, res)
    avg[x][y] = res
  #print avg[x][y]
  return avg[x][y]
print solve(31, 31)[12]
p, s = 0.0, 0.0
for px in permutations(range(5)):
  for py in permutations(range(5)):
    pos, sx, sy = 12, 31, 31
    pcur, scur = 1.0, 0.0
    for i, j in izip(px, py):
      pcur *= probs[sx][pos, i]
      scur += steps[sx][pos, i]
      pos = i+20
      sx &= ~(1<<i)
      pcur *= probs[sy][pos, j]
      scur += steps[sy][pos, j]
      pos = j+20
      sy &= ~(1<<j)
    p += pcur
    s += pcur*scur
print p, s
