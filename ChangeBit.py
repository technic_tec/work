#!/usr/bin/python

# Changing Bits on http://www.hackerrank.com

def update(c, i, v):
	c[i] = v
	i >>= 1
	while i > 0:
		if c[2*i+1] != 1:
			c[i] = c[2*i+1]
		else:
			c[i] = c[2*i]
		i >>= 1
	pass

def query(c, i):
	v = c[i]
	i, j = (i>>1), i
	while i > 0:
		if (j & 1) and c[j-1] != 1:
			v += (c[j-1]>>1)
			break
		i, j = (i>>1), i
	return (v & 1)

def main():
	n, q = map(int, raw_input().split())
	m = n
	while m & (m-1) != 0:
		m += (m & (-m))
	a = list(raw_input().strip()[::-1])
	b = list(raw_input().strip()[::-1])
	c = [0] * m + [int(x)+int(y) for x, y in zip(a, b)] + [0]*(m-n+1)
	for i in range(m-1, 0, -1):
		if c[2*i+1] != 1:
			c[i] = c[2*i+1]
		else:
			c[i] = c[2*i]

	res = ''
	for _ in range(q):
		req = raw_input().split()
		if req[0] == 'set_a':
			i, v = int(req[1]), req[2]
			if a[i] != v:
				a[i] = v
				update(c, m+i, int(a[i])+int(b[i]))
		elif req[0] == 'set_b':
			i, v = int(req[1]), req[2]
			if b[i] != v:
				b[i] = v
				update(c, m+i, int(a[i])+int(b[i]))
		else:
			i = int(req[1])
			res += str(query(c, m+i))
	print res

if __name__ == '__main__':
	main()
