/*
 * =====================================================================================
 *
 *       Filename:  simple_game.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年05月26日 14时33分22秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define P 1000000007
#define N 100000000

vector<vector<int> > ch;
void prep(int n, int m) {
  ch.clear();
  for(int i = 0; i <= n; i++) {
    vector<int> cx(m+1, 0);
    cx[0] = 1;
    for(int j = 1; j < i; j++) {
      cx[j] = ch[i-1][j] + ch[i-1][j-1];
      if(ch[i][j] >= P)
        cx[j] -= P;
    }
    if(i <= m) cx[i] = 1;
    ch.push_back(cx);
  }
}
int f2(int n, int m)
{
  // e[m][n] = e[m-1][n-1] + o[m-1][n-2] + e[m-1][n-3] + ... = se[m-1][n-1] + so[m-1][n-2]
  // o[m][n] = o[m-1][n-1] + e[m-1][n-2] + o[m-1][n-3] + ... = so[m-1][n-1] + se[m-1][n-2]
  vector<int> se, so;
  for(int j = 0; j <= n; j++) {
    if(j & 1) {
      se[j] = (j+1)/2;
      so[j] = 0;
    } else {
      se[j] = 0;
      so[j] = (j+1)/2;
    }
  }
  vector<int> sel(n+1), sol(n+1);
  for(int i = 2; i <= m; i++) {
    sol.swap(so); sel.swap(se);
    se[0] = 0; so[0] = 0;
    se[1] = 0; so[1] = 0;
    for(int j = 2; j <= n; j++) {
      int e, o;
      e = sel[j-1]+sol[j-2];
      o = sol[j-1]+sel[j-2];
      if(e >= P) e -= P;
      if(o >= P) o -= P;
      se[j] = se[j-2] + e;
      so[j] = so[j-2] + o;
      if(se[j] >= P) se[j] -= P;
      if(so[j] >= P) so[j] -= P;
    }
  }
  int res = (n >= 2 ? se[n] - se[n-2] : se[n]);
  if(res < 0) res += P;
  return res;
}
int f3(int n, int m) {
  vector<int> v(n+1);
  vector<set<int> > s2(n+1);
  v[0] = v[1] = 0;
  for(int i = 2; i <= n; i++) {
    for(j = 1; j < i; j++)
      s2[i].add(v[j]^v[i-j]);
    int vi = 0;
    for(int j = 0; j < i; j++)
    for(int vi = 0; ; vi++) {
      for(int j = 2; j < i; j++) {
      }
    }
  }
}
int fk(int n, int m, vector<int> &f) {
  if(f[n] < 0) {
    int sn = 0;
    for(int i = 0; i <= min(n, m); i+=2) {
      sn = (int)((long long)sn + (long long)ch[m][i]*(long long)fk((n-i)/2, m, f)) % P;
      f[n] = sn;
    }
  }
  return f[n];
}
int main()
{
  int n, m, k;
  int res;
  scanf("%d%d%d", &n, &m, &k);
  prep(n, m);
  if(k == 2)
    res = f2(n, m);
  else if(k == 3)
    res = f3(n, m);
  else {
    vector<int> f(n-m+1, -1);
    for(int i = 1; i <= n-m; i+=2)
      f[i] = 0;
    f[0] = 1;
    res = fk(n-m, m, f);
  }
  res = ch[n-1][m-1] - res;
  if(res < 0) res += P;
  printf("%d\n", res);
  return 0;
}
