/*
 * =====================================================================================
 *
 *       Filename:  ural1004.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月31日 13时52分01秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define INF 1000000009
#define N 210

struct Node {
  int adj;
  int depth;
  int prev;
  Node() : adj(-1), depth(-1), prev(-1) {}
};

struct Edge {
  int dst;
  int nxt;
  Edge(int _dst = -1, int _nxt = -1) : dst(_dst), nxt(_nxt) {}
};

int n, m;
vector<Node> p;
vector<Edge> e;
vector<vector<int> > pth;

inline void init(int n) {
  p.clear();
  p.resize(n+1);
  e.clear();
}

inline void add_edge(int x, int y)
{
  e.push_back(Edge(y, p[x].adj)); p[x].adj = e.size()-1;
  e.push_back(Edge(x, p[y].adj)); p[y].adj = e.size()-1;
}

void dfs(int x, int depth = 0, int prev = -1)
{
  p[x].depth = depth;
  p[x].prev = prev;
  for(int i = p[x].adj; i >= 0; i = e[i].nxt) {
    int y = e[i].dst;
    if(p[y].depth < 0)
      dfs(y, depth+1, x);
    else if(p[y].depth > p[x].depth) {
      vector<int> px;
      for(int j = y; j != x; j = p[j].prev)
        px.push_back(j);
      px.push_back(x);
      pth.push_back(px);
    }
  }
}

int main()
{
  scanf("%d%d", &n, &m);
  init(n);
  for(int i = 0; i < m; i++) {
    int x, y, w;
    scanf("%d%d", &x, &y);
    add_edge(x, y);
  }
  pth.clear();
  for(int i = 1; i <= n; i++)
    if(p[i].depth < 0)
      dfs(i);
  printf("%d\n", pth.size());
  for(int i = 0; i < pth.size(); i++) {
    printf("%d", pth[i].size());
    for(int j = 0; j < pth[i].size(); j++)
      printf(" %d", pth[i][j]);
    printf("\n");
  }
  return 0;
}
