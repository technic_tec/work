#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define N 100000
#define P 142857 // = 27*11*13*37

int rev27[] = {
     0,  1, 14,  0,  7, 11,  0,  4, 17,
     0, 19,  5,  0, 25,  2,  0, 22,  8,
     0, 10, 23,  0, 16, 20,  0, 13, 26
};
int fac27[] = {
     1,  1,  2,  2,  8, 13, 13, 10, 26,
    26, 17, 25, 25,  1, 14, 14,  8,  1,
     1, 19,  2,  2, 17, 13, 13,  1, 26
};
int fp27(int n)
{
    const int p = 3, pp = 27;
    int r = 1;
    while(n) {
        if((n/pp)&1)
            r *= pp-1;
        r *= fac27[n%pp]; r %= pp;
        n /= p;
    }
    return r;
}
int nCr_27(int n, int m)
{
    int c = 0, r = 1;
    const int p = 3, pp = 27;
    for(int n1 = n, m1 = m, r1 = n-m, c1 = 0; n1 || m1 || r1; n1 /= p, m1 /= p, r1 /= p) {
        int x = n1 % p, y = m1 % p, z = r1 % p;
        if(z + y + c1 >= p) {
            ++c; r *= p;
            if(c >= 3) return 0;
        }
		c1 = (z+y+c1)/p;
    }
    r *= fp27(n) * rev27[fp27(m)] * rev27[fp27(n-m)];
    r %= pp;
    return r;
}

int nCr_p(int n, int m, int p, int c[])
{
    int r = 1;
    while(n || m) {
        int n1 = n % p, m1 = m % p;
        r *= c[n1*p+m1];
        r %= p;
        n /= p; m /= p;
    }
    return r;
}

void nCr_init(int p, int c[])
{
    for(int i = 0; i < 27; i++)
        if(i % 3)
            assert(i*rev27[i]%27 == 1);
        else
            assert(rev27[i] == 0);
    for(int i = 1; i < 27; i++)
        if(i % 3)
            assert(fac27[i] == fac27[i-1]*i%27);
        else
            assert(fac27[i] == fac27[i-1]);
    memset(c, 0, sizeof(c));
    for(int i = 0; i < p; i++) {
        c[i*p] = 1;
        for(int j = 1; j < i; j++)
            c[i*p+j] = (c[(i-1)*p+j] + c[(i-1)*p+j-1]) % p;
        c[i*p+i] = 1;
    }
}

int c11[11*11], c13[13*13], c37[37*37];
int main()
{
    nCr_init(11, c11);
    nCr_init(13, c13);
    nCr_init(37, c37);
    int t;
    scanf("%d", &t);
    while(t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        int r27 = nCr_27(n, m);
        int r11 = nCr_p(n, m, 11, c11);
        int r13 = nCr_p(n, m, 13, c13);
        int r37 = nCr_p(n, m, 37, c37);
        // (-10)*11 + (3) *37 = 1
        //  (-2)*13 + (1) *27 = 1
        // (-94)*407+(109)*351= 1
        // ax+by=1, n=ux+c=vy+d => vy%x=c-d => v=(c-d)*b, n=(c-d)*b*y+d
        int r407 = ((r11-r37+44)*3*37+r37)%407;
        int r351 = ((r13-r27+39)*1*27+r27)%351;
        int r    = ((r407-r351+407)*109*351+r351)%142857;
        printf("%d\n", r);
    }
    return 0;
}

