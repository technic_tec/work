#include <message.h>
#include <cstdio>
#include <algorithm>
#include "again.h"
using namespace std;

#define MASTER_NODE 0
#define P 1000000007
#define DONE -1

int main() {
  long long N = GetN();
  long long nodes = NumberOfNodes();
  long long my_id = MyNodeId();
  long long vmin, vmax;
#if 0
  if(nodes > N) {
    nodes = N;
    if(my_id >= nodes) return 0;
  }
#endif
  long long sa = 0, sb = 0;
  for (long long i = my_id; i < N; i += nodes)
    sa += GetA(i);
  for (long long i = my_id?(nodes-my_id):0; i < N; i += nodes)
    sb += GetB(i);

  long long sc = (sa%P) * (sb%P) % P;
  if (my_id == MASTER_NODE) {
    for (int i = 1; i < nodes; ++i) {
        int node = Receive(-1);
        sa += GetLL(node);
        sb += GetLL(node);
        sc += GetLL(node);
    }
    sa %= P; sb %= P;
    long long r = (sa*sb-sc)%P;
    if(r < 0) r += P;
    printf("%lld\n", r);
  } else {
    PutLL(MASTER_NODE, sa);
    PutLL(MASTER_NODE, sb);
    PutLL(MASTER_NODE, sc);
    Send(MASTER_NODE);
  }
  return 0;
}
