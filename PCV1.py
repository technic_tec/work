##
##Problem 1 It is easily proved that no equilateral triangle exists with integral length sides and integral area. However, the almost equilateral triangle
##5-5-6 has an area of 12 square units. We shall define an almost equilateral triangle to be a triangle for which two sides are equal and the third differs by
##no more than one unit. Find the sum of the perimeters of every almost equilateral triangles with integral side lengths and area and whose perimeters do not
##exceed one billion (1,000,000,000).
##
# (2k+1, 2k+1, 2k) -> (h, k, 2k+1) -> h^2=(k+1)(3k+1), 6k+2<=N -> v^2-3u^2=-2 && 2v^2<=N && h=uv
# (2k-1, 2k-1, 2k) -> (h, k, 2k-1) -> h^2=(k-1)(3k-1), 6k-2<=N -> v^2-3u^2=1 && 4v^2<=N && h=2uv
def p1(N):
    s = 0
    x, y = 5, 3
    while 2*x**2<=N:
        s += 2*x**2
        x, y = 2*x+3*y, x+2*y
    x, y = 2, 1
    while 4*x**2<=N:
        s += 4*x**2
        x, y = 2*x+3*y, x+2*y
    return s
print 1, p1(10**9)

##Problem 2 If a box contains twenty-one coloured discs, composed of fifteen blue discs and six red discs, and two discs were taken at random, it can be seen
##that the probability of taking two blue discs, P(BB) = (15/21)*(14/20) = 1/2. The next such arrangement, for which there is exactly 50% chance of taking two
##blue discs at random, is a box containing eighty-five blue discs and thirty-five red discs. By finding the first arrangement to contain over 10^12 =
##1,000,000,000,000 discs in total, determine the number of blue discs that the box would contain.
##
# s(s-1) = 2b(b-1), s > 10**12
# (2s-1)^2 - 2(2b-1)^2 = -1, 2s-1>2*10**12-1
def p2(n):
    x, y = 1, 1
    while x <= 2*n-1:
        x, y = 3*x+4*y, 2*x+3*y
    return ((y+1)/2, (x+1)/2-(y+1)/2)
print 2,  p2(10**12)[0]

##Problem 3 Consider the fraction, n/d, where n and d are positive integers. If n < d and HCF(n,d)=1, it is called a reduced proper fraction. If we list the
##set of reduced proper fractions for d <= 8 in ascending order of size, we get: 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/
##7, 3/4, 4/5, 5/6, 6/7, 7/8 It can be seen that there are 3 fraction between 1/3 and 1/2. How many fractions lie between 1/3 and 1/2 in the sorted set of
##reduced proper fractions for d <= 10,000?
##
#def rpf(na, da, nb, db, d):
#    c = 0
#    m5 = 1
#    q = [(na, da, nb, db)]
#    while len(q):
#        na, da, nb, db = q.pop(0)
#        if da + db > d:
#            pass
#        else:
#            q.append((na, da, na+nb, da+db))
#            q.append((na+nb, da+db, nb, db))
#            c += 1
#            #if c == m5:
#                #m5 *= 2
#                #print '%d: %d/%d -- %d/%d' % (c, na, da, nb, db)
#    return c
#print 3, rpf(1, 3, 1, 2, 10000)

##Problem 4 The series, 1^1 + 2^2 + 3^3+ ... + 10^10 = 10405071317. Find the last ten digits of the series, 1^1 + 2^2 + 3^3+ ... + 1000^1000
##
def mpow(n, k, P):
    r = 1
    while k > 0:
        if k & 1:
            r *= n
            r %= P
        n *= n;
        n %= P
        k >>= 1
    return r
s4 = 0
P4 = 10**10
for n4 in range(1, 1001):
    s4 += mpow(n4, n4, P4)
print 4, s4%P4

##Problem 5 The cube, 41063625 (345^3), can be permuted to produce two other cubes: 56623104 (384^3) and 66430125 (405^3). In fact, 41063625 is the smallest
##cube which has exactly three permutations of its digits which are also cube. Find the smallest cube for which exactly five permutations of its digits are
##cube.
##
n5 = 1
r5 = 10**10
cube = dict()
done = False
while not done:
    if len(str(n5**3)) > len(str((n5-1)**3)):
        for k in cube:
            if cube[k][1] == 5:
                if cube[k][0] < r5:
                    r5 = cube[k][0]
                done = True
        cube = dict()
    s5 = ''.join(sorted(str(n5**3)))
    if s5 in cube:
        v, c = cube[s5]
        cube[s5] = (v, c+1)
    else:
        cube[s5] = (n5, 1)
    n5 += 1
print 5,r5**3
##Problem 6 Euler's Totient function, phi(n), is used to determine the number of numbers less than n which are relatively prime to n. For example, as 1, 2, 4,
##5, 7, and 8, are all less than nine and relatively prime to nine, phi(9) = 6. Interestingly, phi(87109) = 79180,and it can be seen that 87109 is a
##permutation of 79180. Find the value of n, below ten million, for which phi(n) is a permutation of n and the ratio n/phi(n) - produces a minimum.
##
##Problem 7 The prime 41, can be written as the sum of six consecutive primes: 41 = 2 + 3 + 5 + 7 + 11 + 13 This is the longest sum of consecutive primes that
##adds to a prime below one-hundred. The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.
##Which prime, below one-million, can be written as the sum of the most consecutive primes? (Integer 1 isn't prime)
##
def p7(n):
    isp = [True]*n
    isp[0] = isp[1] = False
    p = list()
    i = 2
    while(i < n):
        if isp[i]:
            p.append(i)
            j = i*i
            while j < n:
                isp[j] = False
                j += i
        i += 1
    lm, xm = 0, 0
    i, j, s, m = 0, -1, 0, len(p)
    while j+1 < n and s + p[j+1] < n:
        k, sc = i, s
        while j-k+1 > lm and not isp[sc]:
            sc -= p[k]
            k += 1
        if j-k+1 > lm:
            lm, xm = j-k+1, sc
        j += 1
        s += p[j]
    while i < m and j < m and j-i+1 > lm:
        k, sc = i, s
        while j-k+1 > lm and not isp[sc]:
            sc -= p[k]
            k += 1
        if j-k+1 > lm:
            lm, xm = j-k+1, sc
        j += 1
        s += p[j]
        while s >= n:
            s -= p[i]
            i += 1
    return xm
print 7, p7(10**6)

##Problem 8 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145. Find the sum of all numbers which are equal to the sum of the factorial of their
##digits. Note: as 1! = 1 and 2! = 2 are not sums they are not included.
##
# 10^(k-1) <= n = sum(ai!) <= k*9!, k <= 7
def p8():
    fac = [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880]
    c = [0]*10
    sc = 0
    while True:
        c[9] += 1
        i = 9
        while sum(c) > 7 and i > 0:
            c[i] = 0
            c[i-1] += 1
            i -= 1
        if c[0] > 7:
            break
        if sum(c) <= 1:
            continue
        s = sum([c[i]*fac[i] for i in range(10)])
        e = ''.join([str(i)*c[i] for i in range(10)])
        if ''.join(sorted(str(s))) == ''.join(sorted(e)):
            #print s
            sc += s
    return sc
print 8, p8()

##Problem 9 A permutaion is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the
##permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are: 012, 021, 102, 120,
##201, 210 What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
##
def p9(s, m):
    n = len(s)
    fac = [1]
    for i in range(1, n+1):
        fac.append(fac[-1]*i)
    if m > fac[n]:
        return None
    m -= 1
    r9 = list()
    i = n-1
    while i >= 0:
        t = m / fac[i]
        r9.append(s.pop(t))
        m %= fac[i]
        i -= 1
    return ''.join(map(str, r9))
print 9, p9(range(10), 1000000)

##Problem 10 By counting carefully it can be seen that a rectangular grid measuring 3 by 2 contains eighteen rectangles. Although there exists no rectangular
##grid that contains exactly two million rectangles, find the area of the grid with the nearest solution.
def p10(m):
    a = b = 1
    while b*(b+1)/2 < m:
        b += 1
    sm, am = b*(b+1)/2, a*b
    while a <= b:
        s = (a*(a+1)/2) * (b*(b+1)/2)
        if abs(s-m) < abs(sm-m):
            sm, am = s, a*b
        if s > m:
            b -= 1
        else:
            a += 1
    return am
print 10, p10(2000000)
