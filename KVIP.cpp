/*
 * =====================================================================================
 *
 *       Filename:  KVIP.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年12月02日 14时33分16秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 2000

int n, v[N][N];
long long f[N];
int main()
{
    long long c = 0;
    scanf("%d", &n);
    for(int i = 0; i < n; i++) {
        for(int j = 0; j < n; j++)
            scanf("%d", &v[i][j]);
        c += v[i][i];
    }
    for(int i = n-1; i >= 0; i--) {
        f[i] = v[i][0] - v[i][i];
        for(int j = i+1; j < n; j++)
            if(v[i][j]-v[i][i] + f[j] > f[i])
                f[i] = v[i][j]-v[i][i]+f[j];
    }
    printf("%d\n", c+f[0]);
    return 0;
}

