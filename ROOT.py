import sys, fractions
def rat(p,q):
  if p==0:
    print "Invalid Input!!!"
    return

  d=fractions.Fraction(q,p)
  q,p = d.numerator,d.denominator

  z,q = divmod(q,p)
  if q==0:
    print '%.1f' % z
    return

  bits=0
  p1,t = divmod(p,10)
  while t==0:
    p = p1
    p1,t = divmod(p,10)
    bits += 1

  p1,t = divmod(p,5)
  while t==0:
    p = p1
    q <<= 1
    bits += 1
    p1,t = divmod(p,5)
    
  p1,t = (p>>1),(p&1)
  while t==0:
    p = p1
    q += (q<<2)
    bits += 1
    p1,t = (p>>1),(p&1)
    
  if p == 1:
    print '%%d.%%0%dd' % bits % (z,q)
  else:
    m,q = divmod(q,p)
    q0 = q
    d, q = divmod(q*10, p)
    s = str(d)
    while q != q0:
        d, q = divmod(q*10, p)
        s += str(d)
    if bits > 0:
	      print '%%d.%%0%dd(%%s)' % bits % (z,m,s)
    else:
      print '%d.(%s)' % (z,s)

n = int(sys.stdin.readline())
for i in range(n):
  q,p=map(int,sys.stdin.readline().split())
  rat(p,q)

