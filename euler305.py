def acc_length(m):
  # S = sum(k*9*10^(k-1))
  #   = n*10^n - (10^n-1)/9
  # 9 -> 189 -> 2889
  k, s, b = 1, 0, 1
  while m >= 10*b:
    s = (s+b)*10-1
    k += 1
    b *= 10
  s += k*(m-b+1)
  return s

#AB -> BxABxA' | xABy
def count(sn, l, tp, pre = ''):
  if pre == '':
    return (len(tp)+1)*10**(l-len(sn))+(l-len(sn))*(9*10**(l-len(sn)-1))
  elif pre[0] == '0':
    return 0
  tc = 0
  for px, sx in tp:
    if px[:min(len(px), len(pre))] != pre[:min(len(px), len(pre))]:
      continue
    if len(pre) + len(sx) > l:
      cm = len(pre)+len(sx)-l
      if pre[-cm:] == sx[:cm]:
        tc += 1
    else:
      if len(pre) > len(pre):
        px = pre
      tc += 10**(l-len(px)-len(sx))
  for i in range(l-len(sn)):
    if i < len(pre) and pre[i:] != sn[:len(pre)-i]:
      continue
    tc += 10**(+)

def reflect(n, k):
  sn = str(n)
  sm = list()
  for l in range(1, len(sn)+1):
    for i in range(l):
      if sn[i] == '0':
        continue
      if(i+l <= len(sn)):
        x = int(sn[i:i+l])
      else:
        x = int(sn[i:] + sn[len(sn)-l:i]) + 1
      sx = (str(x-1)[-i:] if i>0 else '') + str(x)
      y = x
      while len(sx) < len(sn):
        y += 1
        sx += str(y)
      if sx[:len(sn)] == sn:
        sm.append((x-1, i))
  sm.sort()
  if k <= len(sm):
    x, i = sm[k-1]
  else:
    k -= len(sm)
    l = len(sn)+1
    tp = [(sn[i:], sn[:i]) for i in range(1, len(sn)) if sn[i] != '0']
    tp.sort()
    tc = count(sn, l, tp) 
    while k > tc:
      k -= tc
      l += 1
      tc = count(sn, l, tp)
    p = ''
    while len(p) < l:
      for c in '0123456789':
        tc = count(sn, l, tp, p+c)
        if k > tc:
          k -= tc
        else:
          p += c
          break
    
  return acc_length(x)-i+1

def euler305(n):
  res = reflect(n, n)
  print 'euler305(%d)=%s' % (n, res)
  return res

map(euler305, [1, 5, 12, 7780])
map(euler305, [3**k for k in range(1, 14)])
