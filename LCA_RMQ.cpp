#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <deque>
using namespace std;

class RMQ01 {
    public:
        RMQ01(int n, const int v[]) {
            m_v.assign(v, v+n);
            init();
        }
        RMQ01(vector<int>::iterator st, vector<int>::iterator ed) {
            m_v.assign(st, ed);
            init();
        }
        int query(int l, int r) {
            if(l > r) {
                int tmp = l; l = r; r = tmp;
            }
            int blkl = l / m_blksz;
            int blkr = r / m_blksz;
            int res = -1;
            if(blkl == blkr) {
                res = blkl*m_blksz + m_blkmvs[m_blktype[blkl]][(l-blkl*m_blksz)*m_blksz+(r-blkr*m_blksz)];
            } else {
                int res1 = blkl*m_blksz + m_blkmvs[m_blktype[blkl]][(l-blkl*m_blksz)*m_blksz+(m_blksz-1)];
                int res2 = blkr*m_blksz + m_blkmvs[m_blktype[blkr]][(r-blkr*m_blksz)];
                res = (m_v[res1] > m_v[res2] ? res2 : res1);
                if(blkr - blkl > 1) {
                    int bl = blkl+1, br = blkr-1;
                    int lvl = 0;
                    while((1<<(lvl+1)) < br-bl+1)
                        ++lvl;
                    int res3 = (m_v[m_minblkidx[lvl][bl]] > m_v[m_minblkidx[lvl][br-(1<<lvl)+1]] ? m_minblkidx[lvl][br-(1<<lvl)+1] : m_minblkidx[lvl][bl]);
                    if(m_v[res3] < m_v[res])
                        res = res3;
                }
            }
            return res;
        }
    private:
        void init() {
            int n = m_v.size();
            m_blksz = 0;
            for(int i = n; i; i >>= 1)
                m_blksz++;
            m_blksz = ((m_blksz+1)>>1);
            m_nblks = (n+m_blksz-1)/m_blksz;
            m_blksz = (n+m_nblks-1)/m_nblks;
            // rmq for each type of blks.
            m_blkmvs.clear();
            for(int i = 0; i < (1<<(m_blksz-1)); i++) {
                vector<int> tv;
                tv.push_back(0);
                for(int k = 0; k < m_blksz-1; k++)
                    tv.push_back(tv.back() + (((i>>k)&1) ? 1 : (-1)));
                vector<int> mv;
                for(int j = 0; j < m_blksz; j++) {
                    for(int k = 0; k < j; k++)
                        mv.push_back(mv[k*m_blksz+j]);
                    mv.push_back(j);
                    for(int k = j+1; k < m_blksz; k++)
                        mv.push_back(tv[k]<tv[mv.back()] ? k : mv.back());
                }
                m_blkmvs.push_back(mv);
            }
            // blk type & min blk val
            m_blktype.clear();
            m_minblkval.clear();
            for(int i = 0; i < m_nblks; i++) {
                int type = 0;
                for(int j = i*m_blksz+1; j < (i+1)*m_blksz; j++)
                    if(j < n && m_v[j] == m_v[j-1]-1)
                        type = (type << 1);
                    else if(j >= n || m_v[j] == m_v[j-1]+1)
                        type = (type << 1) + 1;
                    else
                        assert(0 && "difference of neighbouring values must be +/-1");
                m_blktype.push_back(type);
                m_minblkval.push_back(m_v[i*m_blksz+m_blkmvs[type][m_blksz-1]]);
            }
            // cross blk rmq precompute.
            m_minblkidx.clear();
            vector<int> blkmin;
            for(int i = 0; i < m_nblks; i++)
                blkmin.push_back(i);
            m_minblkidx.push_back(blkmin);
            for(int i = 1; (1<<i) < m_nblks; i++) {
                const vector<int> &blkl = m_minblkidx.back();
                blkmin.clear();
                for(int j = 0; j < m_nblks; j++)
                    if(j+(1<<(i-1)) < m_nblks && m_minblkval[blkl[j]] > m_minblkval[blkl[j+(1<<(i-1))]])
                        blkmin.push_back(blkl[j+(1<<(i-1))]);
                    else
                        blkmin.push_back(blkl[j]);
                m_minblkidx.push_back(blkmin);
            }
        }
        int m_blksz;
        int m_nblks;
        vector<int> m_v;
        vector< vector<int> > m_blkmvs;
        vector<int> m_blktype;
        vector<int> m_minblkval;
        vector< vector<int> > m_minblkidx;
};

class LCA {
    public:
        // n: number of nodes in tree
        // T: parent node array
        LCA(int n, const int T[]) {
            m_T.assign(T, T+n);
            m_root = -1;
            m_edges.assign(n, vector<int>());
            for(int i = 0; i < n; i++)
                if(T[i] < 0) {
                    assert(m_root < 0);
                    m_root = i;
                } else
                    m_edges[T[i]].push_back(i);
            assert(m_root >= 0);
            init();
        }
    private:
        void init() {
            m_H.assign(n, -1);
            m_ET.clear();
            m_EL.clear();
            deque<int> Q;
            Q.push_back(makepair(m_root, 0));
            while(!Q.empty()) {
                int x = Q.back().first;
                int xl = Q.back().second;
                if(m_H[x] < 0) {
                    m_H[x] = m_ET.size();
                    m_ET.push_back(x);
                    m_EL.push_back(xl);
                    for(vector<int>::iterator it = m_edges[x].begin(); it != m_edges[x].end(); ++it)
                        Q.push_back(makepair(*it, xl+1));
                } else {
                    if(m_ET.back() != x) {
                        m_ET.push_back(x);
                        m_EL.push_back(xl);
                    }
                    Q.pop_back();
                }
            }
            assert(m_ET.size() == 2*n-1 && m_EL.size() == 2*n-1);
        }
        int m_root;
        vector<int> m_T;
        vector< vector<int> > m_edges;
        vector<int> m_H;
        vector<int> m_ET;
        vector<int> m_EL;
};

class RMQ {
    public:
        RMQ(int n, const int v[]);
        int query(int l, int r);
};

int main()
{
    const int v[] = {3, 2, 3, 4, 5, 4, 3};
    const int n = sizeof(v)/sizeof(v[0]);
    for(int i = 0; i < n; i++)
        printf("%d%c", v[i], i<n-1?' ':'\n');
    RMQ01 rmq(n, v);
    for(int i = 0; i < n; i++)
        for(int j = i; j < n; j++)
            printf("%d %d: %d\n", i, j, rmq.query(i, j));
    return 0;
}

