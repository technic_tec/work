sum_of_pow x n = arr ! (x, 1)
  where arr
  where sum_pow x k | x < 0 = 0
                    | x==0 = 1
					| x < k^n = 0
					| otherwise = sum_pow (x-k^n) (k+1) + sum_pow x (k+1)
