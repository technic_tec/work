#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <ctime>
#include <algorithm>
#include <vector>
using namespace std;

//#define ASSERT(x) if(!(x)) { fprintf(stderr, "ASSERT "#x"FAILED"); exit(0); }
#define ASSERT(x) assert(x)
//#define ASSERT(x)

#define N 10000000
enum Color {
    BLACK,
    RED
};

typedef int key_t;

class PersistRbTree {
    public:
        struct Node {
            Color color;
            key_t key;
            struct node *left;
            struct node *right;
            Node(key_t kx = -1, Color cx = BLACK)
                : color(cx), key(kx), left(nil), right(nil) {    }
            bool updateChildPtr(const Node *old_ch, const Node *new_ch) {
                ASSERT(old_ch != nil || new_ch != nil);
                if(old_ch == nil) {
                    if(new_ch->key < key) {
                        ASSERT(left == old_ch);
                        left = new_ch;
                    } else {
                        ASSERT(right == old_ch);
                        right = new_ch;
                    }
                } else {
                    if(left == old_ch)
                        left = new_ch;
                    else if(right == old_ch)
                        right = new_ch;
                    else
                        return false;
                }
                return true;
            }
        };

        PersistRbTree() {
            nil = NULL;
            nil = new Node;
            nil->left = nil->right = nil;
            root = nil;
        }
        ~PersistRbTree() {
            release(root);
            delete nil;
            nil = NULL;
        }
        void insert_key(key_t key) {
            vector<Node*> path;
            struct node *p = find(key, path);
            ASSERT(p == nil);
            path.pop_back();
            p = new Node(key);
            if(!path.empty()) {
                Node *pp = path.back();
                pp->updateChildPtr(nil, p);
            } else {
                ASSERT(root == nil);
                root = p;
            }
            path.push_back(p);
            insert_adjust(path);
        }

        inline void insert_adjust(vector<Node*> &path) {
            Node *n, *u, *g, *y;
            while(path.length() >= 3) {
                n = path.back();
                path.pop_back();
                y = path.back();
                path.pop_back();
                g = path.back();
                path.pop_back();
                u = (g->left == y ? g->right : g->left);
                if (y->color == BLACK)
                    break; /* Tree is still valid */
                else if (u != nil && u->color == RED) {
                    //       g(B)     ->     g(R)
                    //   y(R)    u(R)     y(B)   u(B)
                    // n(R)             n(R)
                    y->color = BLACK;
                    u->color = BLACK;
                    g->color = RED;
                    plen -= 2;
                    continue;
                } else {
                    if ((n == y->right) && (y == g->left)) {
                        //       g(B)     ->     g(B)
                        //   y(R)    u(B)     n(R)   u(B)
                        //     n(R)         y(R) 
                        --plen;
                        rotate_left(y);
                        y = n; n = n->left;
                    } else if ((n == y->left) && (y == g->right)) {
                        //       g(B)     ->     g(B)
                        //   u(B)    y(R)     u(B)   n(R)
                        //         n(R)                y(R)
                        --plen;
                        rotate_right(y);
                        y = n; n = n->right;
                    }

                    y->color = BLACK;
                    g->color = RED;
                    plen -= 2;
                    if (n == y->left)
                        //        g(B)     ->       g(R)     ->     y(B)
                        //     y(R)   u(B)       y(B)   u(B)     n(R)  g(R)
                        //   n(R)              n(R)                      u(B)
                        rotate_right(g);
                    else
                        //      g(B)       ->      g(R)      ->     y(B)
                        //   u(B)   y(R)       u(B)   y(B)       g(R)  n(R)
                        //            n(R)              n(R)    u(B)
                        rotate_left(g);
                    break;
                }
            }
        }
    private:
        bool is_leaf(Node *n) {
            return (n == nil);
        }
        void release(Node* &p) {
            if(p && p != nil) {
                release(p->left);
                release(p->right);
            }
            delete p;
            p = NULL;
        }
        Node *find(key_t key, vector<Node*> &path) {
            Node *p = root;
            path.push_back(p);
            while(p != nil && p->key != key) {
                if(key < p->key)
                    p = p->left;
                else if(key > p->key)
                    p = p->right;
                else
                    break;
                path.push_back(p);
            }
            return p;
        }

        Node *root, *nil;
};

inline void rotate_left(struct node *n)
{
    //  n       x
    // y x ->  n
    //        y
    ASSERT(plen && path[plen-1] == n);
    struct node *x = n->right;
    if(plen == 1)
        root = x;
    else if(path[plen-2]->left == n)
        path[plen-2]->left = x;
    else
        path[plen-2]->right = x;
    n->right = x->left;
    x->left = n;
    path[plen-1] = x;
    path[plen++] = n;
}

inline void rotate_right(struct node *n)
{
    //  n     x
    // x y ->  n
    //          y
    ASSERT(plen && path[plen-1] == n);
    struct node *x = n->left;
    if(plen == 1)
        root = x;
    else if(path[plen-2]->left == n)
        path[plen-2]->left = x;
    else
        path[plen-2]->right = x;
    n->left = x->right;
    x->right = n;
    path[plen-1] = x;
    path[plen++] = n;
}

void insert_adjust(struct node *n);
void insert_key(key_t key)
{
    struct node *p = find(key, false);
    if(p == nil) {
        root = new struct node;
        root->key = key;
        root->color = BLACK;
        root->left = root->right = nil;
        p = root;
        path[plen++] = p;
    } else {
        struct node *pp = p;
        if(key < p->key)
            p = pp->left = new struct node;
        else if(key > p->key)
            p = pp->right = new struct node;
        else if(pp->left == nil)
            p = pp->left = new struct node;
        else if(pp->right == nil)
            p = pp->right = new struct node;
        else {
            pp = pp->right;
            path[plen++] = pp;
            while(pp->left != nil) {
                pp = pp->left;
                path[plen++] = pp;
            }
            p = pp->left = new struct node;
        }
        p->color = RED;
        p->key = key;
        p->left = p->right = nil;
        path[plen++] = p;
        insert_adjust(p);
    }
}

void insert_adjust(struct node *n)
{
    struct node *u, *g, *y;
    ASSERT(plen && path[plen-1] == n);
    while(1) {
        n = current();
        y = parent();
        u = uncle();
        g = grandparent();
        if (plen == 1) {
            n->color = BLACK;
            break;
        } else if (y->color == BLACK)
            break; /* Tree is still valid */
        else if (u != nil && u->color == RED) {
            //       g(B)     ->     g(R)
            //   y(R)    u(R)     y(B)   u(B)
            // n(R)             n(R)
            y->color = BLACK;
            u->color = BLACK;
            g->color = RED;
            plen -= 2;
            continue;
        } else {
            if ((n == y->right) && (y == g->left)) {
                //       g(B)     ->     g(B)
                //   y(R)    u(B)     n(R)   u(B)
                //     n(R)         y(R) 
                --plen;
                rotate_left(y);
                y = n; n = n->left;
            } else if ((n == y->left) && (y == g->right)) {
                //       g(B)     ->     g(B)
                //   u(B)    y(R)     u(B)   n(R)
                //         n(R)                y(R)
                --plen;
                rotate_right(y);
                y = n; n = n->right;
            }

            y->color = BLACK;
            g->color = RED;
            plen -= 2;
            if (n == y->left)
                //        g(B)     ->       g(R)     ->     y(B)
                //     y(R)   u(B)       y(B)   u(B)     n(R)  g(R)
                //   n(R)              n(R)                      u(B)
                rotate_right(g);
            else
                //      g(B)       ->      g(R)      ->     y(B)
                //   u(B)   y(R)       u(B)   y(B)       g(R)  n(R)
                //            n(R)              n(R)    u(B)
                rotate_left(g);
            break;
        }
    }
}

void delete_one_child(struct node *n);
bool delete_key(key_t key)
{
    struct node *p = find(key);
    if(p == nil)
        return false;
    if(p->left != nil && p->right != nil) {
        struct node *pp = parent();
        int ip = plen-1;
        struct node *q = p->right;
        path[plen++] = q;
        while(q->left != nil) {
            q = q->left;
            path[plen++] = q;
        }
#if 0
        p->key = q->key;
        p = q;
#else
        struct node *pq = parent();
        int iq = plen-1;
        struct node *child = q->right;
        Color cq = q->color;
        if(pp == nil)
            root = q;
        else if(pp->left == p)
            pp->left = q;
        else
            pp->right = q;
        q->color = p->color;
        p->color = cq;
        q->left = p->left;
        if(pq->left == q) {
            assert(pq != p);
            pq->left = p;
            q->right = p->right;
        } else {
            assert(pq == p);
            q->right = p;
        }
        p->left = nil;
        p->right = child;
        path[ip] = q;
        path[iq] = p;
#endif
    }
    delete_one_child(p);
    return true;
}

void delete_one_child(struct node *n)
{
    /*
     * Precondition: n has at most one non-null child.
     */
    ASSERT(plen && path[plen-1] == n);
    struct node *child = is_leaf(n->right) ? n->left : n->right;
    struct node *n0 = n;
    struct node *y = parent();

    if(y == nil)
        root = child;
    else if(y->left == n)
        y->left = child;
    else
        y->right = child;
    if (n->color == BLACK) {
        if (child->color == RED)
            //   n(B)   ->   c(B)
            // c(R) nil     
            child->color = BLACK;
        else {
            path[plen-1] = child;
            while(parent() != nil) {
                struct node *n = current();
                struct node *y = parent();
                struct node *s = sibling();
                if (s->color == RED) {
                    //       y(B)             s(B)         y(B)         s(B)
                    //   n(BB)  s(R)  ->   y(R)        s(R)  n(BB)  ->    y(R)
                    //                   n(BB)                              n(BB)    
                    y->color = RED;
                    s->color = BLACK;
                    --plen;
                    if (n == y->left)
                        rotate_left(y);
                    else
                        rotate_right(y);
                    path[plen++] = n;
                    s = sibling();
                }
                ASSERT(s->color == BLACK);
                if ((y->color == BLACK) &&
                        (s->left->color == BLACK) &&
                        (s->right->color == BLACK)) {
                    //       y(B)              y(BB)
                    //   n(BB)  s(B)    ->  n(B)   s(R)
                    //        z(B) w(B)          z(B) w(B)
                    s->color = RED;
                    --plen;
                    continue;
                }
                else if ((y->color == RED) &&
                        (s->left->color == BLACK) &&
                        (s->right->color == BLACK)) {
                    //       y(R)              y(B)
                    //   n(BB)  s(B)    ->  n(B)   s(R)
                    //        z(B) w(B)          z(B) w(B)
                    s->color = RED;
                    y->color = BLACK;
                    break;
                } else {
                    if ((n == y->left) &&
                            (s->right->color == BLACK) &&
                            (s->left->color == RED)) {
                        //       y(?)               y(?)
                        //   n(BB)  s(B)    ->  n(BB)   z(B)
                        //        z(R) w(B)               s(R)
                        //                                  w(B)
                        s->color = RED;
                        s->left->color = BLACK;
                        path[plen-1] = s;
                        rotate_right(s);
                        --plen;
                        path[plen-1] = n;
                        s = sibling();
                    } else if ((n == y->right) &&
                            (s->left->color == BLACK) &&
                            (s->right->color == RED)) {
                        //       y(?)                 y(?)
                        //   s(B)  n(BB)    ->    z(B)   n(BB)
                        //w(B) z(R)             s(R)
                        //                    w(B)
                        s->color = RED;
                        s->right->color = BLACK;
                        path[plen-1] = s;
                        rotate_left(s);
                        --plen;
                        path[plen-1] = n;
                        s = sibling();
                    }

                    s->color = y->color;
                    y->color = BLACK;

                    --plen;
                    if (n == y->left) {
                        //       y(X)                 s(X)
                        //   n(BB)  s(B)    ->    y(B)     w(B)
                        //        z(?) w(R)    n(B) z(?)
                        s->right->color = BLACK;
                        rotate_left(y);
                    } else {
                        //       y(X)                 s(X)
                        //   s(B)  n(BB)    ->    w(B)    y(B)
                        //w(R) z(?)                    z(?) n(B)
                        s->left->color = BLACK;
                        rotate_right(y);
                    }
                    break;
                }
            }
        }
    }
    delete n0;
}

void print()
{
    struct node *p = root;
    if(p == nil) return;
    plen = 0; path[plen++] = p;
    while(p->left != nil) {
        p = p->left;
        path[plen++] = p;
    }
    path[plen] = nil;
    while(plen) {
        if(path[plen-1]->right == nil || path[plen-1]->right == path[plen]) {
            if(path[plen-1]->right == nil)
                printf("%d ", path[plen-1]->key);
            --plen;
        } else {
            printf("%d ", path[plen-1]->key);
            p = path[plen-1]->right;
            path[plen++] = p;
            while(p->left != nil) {
                p = p->left;
                path[plen++] = p;
            }
            path[plen] = nil;
        }
    }
    printf("\n");
}

void rb_check()
{
    ASSERT(root->color == BLACK);
    ASSERT(nil->color == BLACK);
    ASSERT(nil->left == nil && nil->right == nil);

    struct node *p = root;
    if(p == nil) return;
    int lvl = 0, nil_lvl = -1;
    plen = 0;
    path[plen++] = p;
    lvl += (p->color == BLACK);
    while(p->left != nil) {
        p = p->left;
        path[plen++] = p;
        lvl += (p->color == BLACK);
    }
    path[plen] = nil;
    while(plen) {
        p = path[plen-1];
        if(p->right == nil || p->right == path[plen]) {
            if(p->left == nil || p->right == nil) {
                if(nil_lvl == -1)
                    nil_lvl = lvl;
                else
                    ASSERT(nil_lvl == lvl);
            }
            if(p->left != nil) {
                ASSERT(p->left->key <= p->key);
                //if(p->color == RED && p->left->color == RED)
                //    exit(1);
                ASSERT(p->color == BLACK || p->left->color == BLACK);
            }
            if(p->right != nil) {
                ASSERT(p->right->key >= p->key);
                //if(p->color == RED && p->right->color == RED)
                //    exit(1);
                ASSERT(p->color == BLACK || p->right->color == BLACK);
            }
            lvl -= (p->color == BLACK);
            --plen;
        } else {
            p = p->right;
            path[plen++] = p;
            lvl += (p->color == BLACK);
            while(p->left != nil) {
                p = p->left;
                path[plen++] = p;
                lvl += (p->color == BLACK);
            }
            path[plen] = nil;
        }
    }
}

int main()
{
    srand(time(NULL));
    rb_tree_init();
    int tsz = 0;
    for(int i = 0; i < 10000; i++) {
        int v = rand() & 1023;
        if(find(v) != nil) {
            if(rand() & 1) {
                delete_key(v);
                --tsz;
            } else {
                insert_key(v);
                ++tsz;
            }
        } else {
            insert_key(v);
            ++tsz;
        }
        rb_check();
    }
    print();
    printf("%d\n", tsz);
    rb_tree_release();
}
