/*
 * =====================================================================================
 *
 *       Filename:  PathTree.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年11月11日 09时45分26秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

class TreeNode {
    public:
        int m_deg;
        int m_pa;
        int m_wt;
};
class PathTree {
    public:
        PathTree(int n, const int p[N], int root = 0) : m_size(n), m_root(root) {
            m_nodes = new TreeNode[n];
            int *deg = new int[n];
            memset(deg, 0, n*sizeof(int));
            for(int i = 0; i < n-1; i++)
                deg[p[i]]++;
            for(int i = 0; i < n; i++) {
                m_nodes[i].m_deg = deg[i];
                m_nodes[i].m_pa = p[i];
                m_nodes[i].m_wt = 1;
            }
            int *q = new int[n], hd = 0, tl = 0;
            for(int i = 0; i < n; i++)
                if(deg[i] == 0)
                    q[tl++] = i;
            while(hd < tl) {
                int x = q[hd++];
                if(p[x] >= 0) {
                    m_nodes[p[x]].m_wt += m_nodes[x].m_wt;
                    if(--deg[p[x]] == 0)
                        q[tl++] = p[x];
                }
            }
            for(int i = 0; i < n; i++)
            delete[] q;
            delete[] deg;
        }
        PathTree(int n, const int p[N][2], int root = 0) : m_size(n), m_root(root) {
            m_nodes = NULL;
        }
        ~PathTree() {
            if(m_nodes)
                delete[] m_nodes;
            m_nodes = NULL;
        }
    private:
        int m_size, m_root;
        TreeNode *m_nodes;
};

const int pa[] = {0, 1, 2};
const int pb[][2] = {{0, 1}, {1, 2}, {2, 3}};
int main()
{
    PathTree a(sizeof(pa)/sizeof(pa[0]), pa);
    PathTree b(sizeof(pb)/sizeof(pb[0]), pb);
    return 0;
}

