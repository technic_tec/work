N = 10000
t = int(raw_input())
for _ in range(t):
    p = float(raw_input())
    if 2*p < 1.0:
        r = N+p*(1-2*p)*N
    else:
        r = N+(2*p-1)*(1-p)*N
    print "%.7f" % r
