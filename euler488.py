def score(N):
  sc = dict()
  for i in range(N+1):
    for j in range(i+1, N+1):
      for k in range(j+1, N+1):
        cand = set()
        for u in range(i):
          cand.add(sc[(u, i, j)])
          cand.add(sc[(u, i, k)])
          cand.add(sc[(u, j, k)])
        for u in range(i+1, j):
          cand.add(sc[(i, u, j)])
          cand.add(sc[(i, u, k)])
        for u in range(j+1, k):
          cand.add(sc[(i, j, u)])
        x = 0
        while x in cand:
          x += 1
        sc[(i, j, k)] = x
  print sorted(k for k in sc if sc[k] == 0)
score(31)
