/*
 * =====================================================================================
 *
 *       Filename:  TULIPS.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016/03/14  6:37:55
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), technic.tec@gmail.com
 *   Organization:  Verisilicon, Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;
#define N 100000000
#define P 1000000007

struct Edge {
	int u, v, l;
	Edge(int _u, int _v, int _l) : u(_u), v(_v), l(_l) {}
	bool operator<(const Edge &o) const {
		return l < o.l;
	}
};
struct Query {
	int d, u, k, tr;
	Query(int _d, int _u, int _k) : d(_d), u(_u), k(_k), tr(-1) {}
	bool operator<(const Query &o) const {
		return d < o.d;
	}
};
bool compk(const Query &x, const Query &o) {
	return x.k < o.k;
}
class Region {
  public:
    struct Node {
      int l, r, p, il, ir;
      Node() : l(-1), r(-1), p(-1), il(-1), ir(-1) {}
    };
    struct Val {
      int p, v;
      Val(int _v = 0) : p(0), v(_v){}
      bool operator<(const Val &o) const {
        return v < o.v;
      }
    };
    struct Block {
      int shift;
      bool constant;
      int off;
      vector<Val> v;
      Block(int s, int t) : shift(0), constant(true), off(s), v(t-s) {
        for(int i = s; i < t; i++) {
          v[i-s].p = i; v[i-s].v = 0;
        }
      }
      void grow(int d) {
        shift += d;
      }
      int collect(int x) {
        int cnt;
        if(constant) {
          cnt = (shift >= x ? v.size() : 0);
        } else {
          cnt = v.end()-lower_bound(v.begin(), v.end(), x-shift);
        }
        shift = 0;
        constant = true;
        return cnt;
      }
      int collect(int x, int s, int t) {
        if(s <= off && t >= off+v.size()-1)
          return collect(x);
        if(constant) {
          for(int i = 0; i < v.size(); i++)
            v[i].v = shift;
          constant = false;
        } else {
          for(int i = 0; i < v.size(); i++)
            v[i].v += shift;
        }
        shift = 0;
        int cnt = 0;
        vector<Val> nv;
        nv.reserve(v.size());
        for(int i = 0; i < v.size(); i++)
          if(v[i].p >= s && v[i].p <= t) {
            if(v[i].v >= x) ++cnt;
            v[i].v = 0;
            nv.push_back(v[i]);
          }
        for(int i = 0; i < v.size(); i++)
          if(v[i].p < s || v[i].p > t)
            nv.push_back(v[i]);
        v.swap(nv);
        return cnt;
      }
      int size() { return v.size(); }
    };
    Region(int n) : m_sz(n), m_nodes(n) {
      m_nodes.reserve(2*n-1);
      m_blksz = (int)(floor(sqrt((double)n))+0.5);
      m_nblk = (n+m_blksz-1)/m_blksz;
    }
    int merge(int u, int v) {
      assert(u != v && m_nodes[u].p < 0 && m_nodes[v].p < 0);
      int x = m_nodes.size();
      m_nodes.push_back(Node());
      m_nodes.back().l = u;
      m_nodes.back().r = v;
      m_nodes[u].p = m_nodes[v].p = x;
    }
    void preprocess() {
      int x = m_root = m_nodes.size()-1;
      assert(m_nodes.size() == 2*m_sz-1 && m_nodes[x].p == -1);
      int nleaves = 0;
      while(1) {
        while(m_nodes[x].l >= 0)
          x = m_nodes[x].l;
        assert(x < m_sz && m_nodes[x].r < 0);
        m_nodes[x].il = m_nodes[x].ir = nleaves++;
        while(m_nodes[x].p >= 0 && x == m_nodes[m_nodes[x].p].r) {
          x = m_nodes[x].p;
          m_nodes[x].il = m_nodes[m_nodes[x].l].il;
          m_nodes[x].ir = m_nodes[m_nodes[x].r].ir;
        }
        if(m_nodes[x].p < 0)
          break;
        x = m_nodes[m_nodes[x].p].r;
      }
      assert(nleaves == m_sz && m_nodes[m_root].il == 0 && m_nodes[m_root].ir == nleaves-1);
      m_blks.clear();
      for(int i = 0; i < m_nblk; i++)
        m_blks.push_back(Block(i*m_blksz, min((i+1)*m_blksz, m_sz)));
    }
    int grow(int d) {
      for(int i = 0; i < m_nblk; i++)
        m_blks[i].grow(d);
    }
    int collect(int tr, int x) {
      int cnt = 0;
      int il = m_nodes[tr].il, ir = m_nodes[tr].ir;
      int bl = il/m_blksz, br = ir/m_blksz;
      if(bl == br) {
        cnt += m_blks[bl].collect(x, il, ir);
      } else {
        if(il > bl * m_blksz) {
          cnt += m_blks[bl].collect(x, il, bl*m_blksz+m_blksz-1);
          ++bl;
        }
        if(ir < br * m_blksz + m_blksz - 1) {
          cnt += m_blks[br].collect(x, br*m_blksz, ir);
          --br;
        }
        for(int i = bl; i <= br; i++)
          cnt += m_blks[i].collect(x);
      }
      return cnt;
    }
  private:
    int m_sz;
    int m_root;
    int m_nblk, m_blksz;
    vector<Node> m_nodes;
    vector<Block> m_blks;
};
class DisjointSet {
  public:
    DisjointSet(int n) : m_sz(n), m_pa(n, -1), m_ht(n, 1), m_lbl(n, -1) {
      for(int i = 0; i < n; i++) {
        m_pa[i] = i;
        m_lbl[i] = i;
      }
    }
    int find_root(int x) {
      int i = x;
      while(m_pa[i] != i)
        i = m_pa[i];
      while(m_pa[x] != x) {
        int y = x;
        x = m_pa[x];
        m_pa[y] = i;
      }
      return i;
    }
    void merge(int rx, int ry, int lbl) {
      assert(rx != ry && m_pa[rx] == rx && m_pa[ry] == ry);
      if(m_ht[rx] > m_ht[ry]) {
        m_pa[ry] = rx;
        m_lbl[rx] = lbl;
      } else {
        m_pa[rx] = ry;
        m_lbl[ry] = lbl;
        if(m_ht[rx] == m_ht[ry])
          m_ht[ry]++;
      }
    }
    int label(int x) {
      int rx = find_root(x);
      return m_lbl[rx];
    }
  private:
    int m_sz;
    vector<int> m_pa;
    vector<int> m_ht;
    vector<int> m_lbl;
};
int main()
{
	int t;
	scanf("%d", &t);
	while(t--) {
		int n, q, x;
		vector<Edge> es;
		vector<Query> qs;
		scanf("%d", &n);
		for(int i = 0; i < n-1; i++) {
			int u, v, l;
			scanf("%d%d%d", &u, &v, &l);
			es.push_back(Edge(u-1, v-1, l));
		}
		sort(es.begin(), es.end());
		scanf("%d%d", &q, &x);
		for(int i = 0; i < q; i++) {
			int d, u, k;
			scanf("%d%d%d", &d, &u, &k);
			qs.push_back(Query(d, u-1, k));
		}
		sort(qs.begin(), qs.end(), compk);

    Region tr(n);
    DisjointSet ds(n);
    int j = 0;
    for(int i = 0; i < n-1; i++) {
      while(j < qs.size() && qs[j].k < es[i].l) {
        qs[j].tr = ds.label(qs[j].u);
        ++j;
      }
      int ru = ds.find_root(es[i].u);
      int rv = ds.find_root(es[i].v);
      ds.merge(ru, rv, tr.merge(ds.label(ru), ds.label(rv)));
    }
    while(j < qs.size()) {
      qs[j].tr = ds.label(qs[j].u);
      ++j;
    }

		sort(qs.begin(), qs.end());
		tr.preprocess();
		int ld = -x;
		for(int i = 0; i < q; i++) {
			tr.grow(qs[i].d-ld);
			printf("%d\n", tr.collect(qs[i].tr, x));
			ld = qs[i].d;
		}
	}
  return 0;
}
