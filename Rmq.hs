data SegTree a b = Branch a b (SegTree a b) (SegTree a b) | Leaf a
  deriving (Eq, Show)
construct :: (Num a, Ord a, Integral b) => [a] -> SegTree a b
construct l | length l <= 1 = Leaf (head l)
            | otherwise = Branch minV sz lc rc
                 where (sl, sr) = splitAt (div (length l) 2) l
                       lc = construct sl
                       rc = construct sr
                       getMin (Leaf v) = v
                       getMin (Branch vMin _sz _lc _rc) = vMin
                       getSize(Leaf v) = 1
                       getSize(Branch minV _sz _lc _rc) = _sz
                       minV = min (getMin lc) (getMin rc)
                       sz = (getSize lc) + (getSize rc)
queryMin :: (Num a, Ord a, Integral b) => SegTree a b -> (b -> (b -> Maybe a))
queryMin (Leaf v) l r | l <= 1 && r >= 1 = Just v
                      | otherwise = Nothing
queryMin (Branch minV _sz _lc _rc) l r | r < 1 || l > _sz = Nothing
                                       | l <= 1 && r >= _sz = Just minV
                                       | otherwise = (minM (queryMin _lc l r) (queryMin _rc (l-szl) (r-szl)))
                                           where getSize(Leaf v) = 1
                                                 getSize(Branch minV _sz _lc _rc) = _sz
                                                 szl = getSize _lc
                                                 minM Nothing Nothing = Nothing
                                                 minM Nothing (Just y) = Just y
                                                 minM (Just x) Nothing = Just x
                                                 minM (Just x) (Just y) = Just (min x y)
process :: Int -> (SegTree Int Int -> IO())
process 0 t = return()
process m t = do
               sl <- getLine
               [il, ir] <- return (map read (words sl))
               Just res <- return (queryMin t (il+1) (ir+1))
               putStrLn (show res)
               process (m-1) t

main::IO()
main = do
        sl <- getLine
        [n, m] <- return (map read (words sl))
        sl <- getLine
        v <- return (map read (words sl))
        t <- return (construct v)
        process m t
