/*
 * =====================================================================================
 *
 *       Filename:  fairy_chess.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年05月24日 10时04分17秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 210
#define P 1000000007
int n, m, s;
char b[N][N];
int c[N][N][N];

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d%d", &n, &m, &s);
        for(int i = 0; i < n; i++)
            scanf("%s", b[i]);
    }
    return 0;
}

