/*
 * =====================================================================================
 *
 *       Filename:  EASYEX.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月13日 14时19分11秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009
#define P 2003

int power_mod(int n, int k)
{
  int r = 1;
  while(k) {
    if(k&1)
      r = (int)((long long)r * (long long)n % P);
    n = (int)((long long)n * (long long)n % P);
    k >>= 1;
  }
  return r;
}
int ans(int n, int k, int f, int l)
{
  int res = 0;
  if(n < l)
    return 0;
  if(f > 1)
    return 0;
  if(l == k) {
    // E(N, K, 1) = N!/(N-K)!*K^(-K)
    long long ck = 0;
    for(int i = n/P; i > 0; i/=P)
      ck += i;
    for(int i = (n-k)/P; i > 0; i/=P)
      ck -= i;
    for(int i = k; i % P == 0; i /= P)
      ck -= k;
    if(ck) return 0;
    res = 1;
    for(int i = n; i > n-k; i--) {
      int j = i;
      while(j % P == 0)
        j /= P;
      res = (int)((long long)res * (long long)j % P);
    }
    int j = k;
    while(j % P == 0)
      j /= P;
    res = (int)((long long)res * (long long)power_mod(j, P-1-k%(P-1)) % P);
  } else {
    // E(N, L, 1) = N!/(N-L)!*K^(-L)*(L/K+1)^(N-L)
    long long ck = 0;
    for(int i = n/P; i > 0; i/=P)
      ck += i;
    for(int i = (n-l)/P; i > 0; i/=P)
      ck -= i;
    for(int i = k+l; i % P == 0; i /= P)
      ck += n-l;
    for(int i = k; i % P == 0; i /= P)
      ck -= n;
    if(ck) return 0;
    res = 1;
    for(int i = n; i > n-l; i--) {
      int j = i;
      while(j % P == 0)
        j /= P;
      res = (int)((long long)res * (long long)j % P);
    }
    int j = k+l;
    while(j % P == 0)
      j /= P;
    res = (int)((long long)res * (long long)power_mod(j, (n-l) % (P-1)) % P);
    j = k;
    while(j % P == 0)
      j /= P;
    res = (int)((long long)res * (long long)power_mod(j, P-1-n%(P-1)) % P);
  }
  return res;
}
int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    int n, k, f, l;
    scanf("%d%d%d%d", &n, &k, &l, &f);
    printf("%d\n", ans(n, k, f, l));
  }
  return 0;
}
