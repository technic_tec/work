from fractions import gcd

def f(a, b, c, n):
  return n**4+a*n**3+b*n**2+c*n

def d(a, b, c):
  return gcd(gcd(24, f(a, b, c, -1)), gcd(f(a, b, c, 1), f(a, b, c, 2)))

val = dict()
for a in range(24):
  for b in range(24):
    for c in range(24):
      x = d(a, b, c)
      val.setdefault(x, set()).add((a, b, c))

cr = []
for r in range(24):
  cc = [0]*4
  for d in val:
    vd = val[d]
    c0 = len([(a, b, c) for a, b, c in vd if (a >= 1 and a <= r) and (b >= 1 and b <= r) and (c >= 1 and c <= r)])
    c1 = len([(a, b, c) for a, b, c in vd if (a == 0 or a > r) and (b >= 1 and b <= r) and (c >= 1 and c <= r)])
    c2 = len([(a, b, c) for a, b, c in vd if (a >= 1 and a <= r) and (b == 0 or b > r) and (c >= 1 and c <= r)])
    c3 = len([(a, b, c) for a, b, c in vd if (a >= 1 and a <= r) and (b >= 1 and b <= r) and (c == 0 or c > r)])
    c4 = len([(a, b, c) for a, b, c in vd if (a == 0 or a > r) and (b == 0 or b > r) and (c >= 1 and c <= r)])
    c5 = len([(a, b, c) for a, b, c in vd if (a == 0 or a > r) and (b >= 1 and b <= r) and (c == 0 or c > r)])
    c6 = len([(a, b, c) for a, b, c in vd if (a >= 1 and a <= r) and (b == 0 or b > r) and (c == 0 or c > r)])
    c7 = len([(a, b, c) for a, b, c in vd if (a == 0 or a > r) and (b == 0 or b > r) and (c == 0 or c > r)])
    #assert(c0+c1+c2+c3+c4+c5+c6+c7 == len(vd))
    #s += d*(c0*(k+1)*(k+1)*(k+1)+(c1+c2+c3)*(k+1)*(k+1)*k+(c4+c5+c6)*(k+1)*k*k+c7*k*k*k)
    cc[3] += d*(c0+c1+c2+c3+c4+c5+c6+c7)
    cc[2] += d*(3*c0+2*c1+2*c2+2*c3+c4+c5+c6)
    cc[1] += d*(3*c0+c1+c2+c3)
    cc[0] += d*c0
  cr.append(cc)
print cr

fib = [1, 2]
while fib[-2:] != [0, 1]:
  fib.append((fib[-1]+fib[-2])%24)
print fib

def S(N):
  k, r = N/24, N%24
  cc = cr[r]
  s = ((cc[3]*k+cc[2])*k+cc[1])*k+cc[0]
  print 'S(%d) = %d' % (N, s)
  return s

# S(N) = c0+c1k+c2k^2+c3k^3, [c0, c1, c2, c3] = cr[r=N%24], k=[N/24] = (N-r)/24
# SF(N) = sum{k}(S(Fib(k)))
#       = sum{t}sum{k}(S(Fib(24k+t))
#       = sum{t}(c0*sum{k}(1)+c1*sum{k}([Fib(24k+t)/24]+c2*sum{k}([Fib(24k+t)/24]^2))+c3*sum{k}([Fib(24k+t)/24]^3)), 2<=24k+t<=N
# Fib(n) = [1, 1]^n [0][1]
#          [1, 0]
# Fib(n)^2 = [1, 2, 1]^n [0][2]
#            [1, 1, 0]
#            [1, 0, 0]
# Fib(n)^3 = [1, 3, 3, 1]^n [0][3]
#            [1, 2, 1, 0]
#            [1, 1, 0, 0]
#            [1, 0, 0, 0]
# sum(M^(kp+r), k, 0, n) = M^r*(M^(n+1)p-I)/(M^p-I)
def SF(N):
  raise NotImplementedError

S(10)
S(10000)
SF(1234567890123);
