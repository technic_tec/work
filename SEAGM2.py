t = int(raw_input())
for _ in range(t):
  n, m = map(int, raw_input().split())
  sr = pr = 0
  for i in range(n):
    rx = 1
    for px in map(float, raw_input().split()):
      px = int(10000*px+0.5)
      rx *= px
    sr += rx
    if i == 0:
      pr = rx
  if sr == 0:
    r = 0
  else:
    r = pr * 100000000 / sr
  print '%.6f' % (float(r)/100000000.0)
