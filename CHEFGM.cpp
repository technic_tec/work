/*
 * =====================================================================================
 *
 *       Filename:  CHEFGM.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年11月07日 19时06分15秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 100000

int k, n, v[N];

class Frac2 {
    public:
        Frac2(long long v, int rank = 0) : m_v(v), m_rank(rank)
        {
            while(m_rank > 0 && !(m_v & 1)) {
                m_v >>= 1;
                m_rank--;
            }
        }
        bool isEmpty() const
        {
            return m_rank < 0;
        }
        int sign() const {
            return (m_v > 0 ? 1 : (m_v < 0 ? -1 : 0));
        }
        const Frac2 operator+(const Frac2& x) const {
            if(isEmpty() || x.isEmpty())
                return Frac2(0, -1);
            int rank = std::max(m_rank, x.m_rank);
            long long v = (m_v << (rank-m_rank)) + (x.m_v << (rank-x.m_rank));
            return Frac2(v, rank);
        }
        const Frac2 operator-(const Frac2& x) const {
            if(isEmpty() || x.isEmpty())
                return Frac2(0, -1);
            int rank = std::max(m_rank, x.m_rank);
            long long v = (m_v << (rank-m_rank)) - (x.m_v << (rank-x.m_rank));
            return Frac2(v, rank);
        }
        const Frac2 operator>>(const int c) const {
            if(isEmpty())
                return Frac2(0, -1);
            return Frac2(m_v, m_rank+c);
        }
    private:
        long long m_v;
        int m_rank;
};

Frac2 Next(Frac2 vl, Frac2 vr)
{
    if(vl.isEmpty() && vr.isEmpty())
        return Frac2(0);
    else if(vl.isEmpty())
        return vr-1;
    else if(vr.isEmpty())
        return vl+1;
    else
        return ((vl+vr)>>1);
}

Frac2 Surreal_Number(int n, int v[])
{
    std::sort(v, v+n);
    n = std::unique(v, v+n) - v;
    Frac2 l(0, -1), r(0, -1), x(0);
    x = Next(l, r);
    for(int i = 0; i < n; i++) {
        if(v[i] & 1)
            r = x;
        else
            l = x;
        x = Next(l, r);
    }
    return x;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &k);
        Frac2 s(0);
        while(k--) {
            scanf("%d", &n);
            for(int i = 0; i < n; i++)
                scanf("%d", &v[i]);
            s = s + Surreal_Number(n, v);
        }
        const char *sRes[] = {"ODD", "DON'T PLAY", "EVEN"};
        puts(sRes[s.sign() + 1]);
    }
    return 0;
}
