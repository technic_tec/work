def f(B, pts):
    xmin = min([x for x, y in pts])-B
    ymin = min([y for x, y in pts])-B
    xmax = max([x for x, y in pts])+B
    ymax = max([y for x, y in pts])+B
    s, p = 0, 1
    for xc in range(xmin, xmax+1):
        for yc in range(ymin, ymax+1):
            prb = ['%.8f' % (pow(abs(abs(x-xc)+abs(y-yc)-B), -1.2) if abs(abs(x-xc)+abs(y-yc)-B)>0 else 0) for x, y in pts]
            print (xc, yc), prb, sum(map(float, prb)), reduce(lambda x, y: x*y, map(float, prb))

f(2, [(4, 3), (2, 4), (3, 1)])
#f(8, [(200,200), (130,180), (785,312), (283,851), (591,768), (283,361), (912,351), (612,683), (815,31)])
