#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>
#include <set>
#include <algorithm>
using namespace std;

#define N 200000

class LinkCutTree {
    struct Node {
        // left, right, parent -- splay tree of preferred path (solid edges)
        // path_pa -- path between preferred paths (dash edges)
        struct {  // subtree data
            int inc;  // subtree increament
            int sz;   // subtree size;
            long long sum;  // subtree sum of node val
        };
        struct {  // node data
            int linc;
            int lsz;
            long long lsum;
        };
        Node *left, *right, *parent, *path_pa;
        Node() : left(NULL), right(NULL), parent(NULL), path_pa(NULL), inc(0), sum(0), sz(1), linc(0), lsum(0), lsz(1) {}
        inline Node* & Left() { return left; }
        inline Node* & Right() { return right; }
        inline void add(int x) {
            linc += x;
            lsum += (long long)x*(long long)lsz;
            sum += (long long)x*(long long)lsz;
        }
        inline void push() {
            if(Left()) {
                Left()->inc += inc;
                Left()->sum += (long long)inc*(long long)Left()->sz;
            }
            if(Right()) {
                Right()->inc += inc;
                Right()->sum += (long long)inc*(long long)Right()->sz;
            }
            linc += inc;
            lsum += (long long)inc*(long long)lsz;
            inc = 0;
        }
        inline void synth() {
            assert(inc == 0);
            sum = lsum; sz = lsz;
            if(Left()) {
                sz += Left()->sz;
                sum += Left()->sum;
            }
            if(Right()) {
                sz += Right()->sz;
                sum += Right()->sum;
            }
        }
    };
    public:
        LinkCutTree(int sz, int n = 0, int *v = NULL){
            m_poolsz = sz;
            m_nodes = new Node[sz];
            m_sz = n;
            if(v)
                for(int i = 0; i < n; i++)
                    m_nodes[i].sum = m_nodes[i].lsum = v[i];
        }
        ~LinkCutTree() {
            if(m_nodes) {
                delete[] m_nodes;
                m_nodes = NULL;
            }
        }
        int getRoot(int x) {
            Node *p = &m_nodes[x];
            access(p);
            while(p->Left())
                p = p->Left();
            splay(p);
            return p-m_nodes;
        }
        void cut(int x) {
            // NULL -> x
            assert(x > 0 && x < m_sz);
            Node *p = &m_nodes[x];
            access(p);
            if(p->Left()) {
                p->push();
                p->Left()->parent = NULL;
                p->Left() = NULL;
                p->synth();
            }
        }
        void join(int x, int y)
        {
            // y -> x, x is root of sub tree
            access(&m_nodes[x]);
            access(&m_nodes[y]);
            assert(m_nodes[x].Left() == NULL);
            assert(m_nodes[y].parent == NULL);
            assert(m_nodes[y].path_pa == NULL);
            m_nodes[x].push();
            m_nodes[x].Left() = &m_nodes[y];
            m_nodes[y].parent = &m_nodes[x];
            m_nodes[x].synth();
        }
        void extend(int p, int v) {
            assert(p >= 0 && p < m_sz);
            int x = m_sz++;
            assert(x < m_poolsz);
            m_nodes[x].lsum = m_nodes[x].sum = v;
            if(p >= 0)
                join(x, p);
        }
        void add(int x, int v)
        {
            assert(x >= 0 && x < m_sz);
            access(&m_nodes[x]);
            m_nodes[x].add(v);
        }
        long long sum(int x) {
            assert(x >= 0 && x < m_sz);
            access(&m_nodes[x]);
            assert(m_nodes[x].inc == 0);
            return m_nodes[x].lsum;
        }
    private:
        inline void toSolid(Node *w, Node *v)
        {
            assert(w->Right() == NULL && v->path_pa == w);
            w->Right() = v;
            v->parent = w;
            v->path_pa = NULL;
            w->lsz -= v->sz;
            v->inc += w->linc;
            v->sum += (long long)w->linc*(long long)v->sz;
            w->lsum -= v->sum;
        }
        inline void toDashed(Node *v, Node *u)
        {
            assert(v->Right() == u && u->parent == v);
            u->path_pa = v;
            u->parent = NULL;
            v->Right() = NULL;
            v->lsz += u->sz;
            v->lsum += u->sum;
            u->inc -= v->linc;
            u->sum -= (long long)v->linc*(long long)u->sz;
        }
        void access(Node *v)
        {
            Node *v0 = v;
            splay(v);
            if(v->Right()) {
                Node *u = v->Right();
                v->push();
                toDashed(v, u);
                v->synth();
            }
            while(v->path_pa != NULL) {
                Node *w = v->path_pa;
                splay(w);
                Node *u = w->Right();
                w->push();
                if(u)
                    toDashed(w, u);
                toSolid(w, v);
                w->synth();
                v = w;
            }
            splay(v0);
        }
        void splay(Node *v) {
            while(v->parent && v->parent->parent) {
                Node *p = v->parent;
                Node *g = p->parent;
                g->push();
                p->push();
                v->push();
                if(p == g->Left()) {
                    if(v == p->Left()) {
                        //    g        v
                        //  p      ->    p
                        //v                g
                        p->Left() = v->Right();
                        if(p->Left())
                            p->Left()->parent = p;
                        g->Left() = p->Right();
                        if(g->Left())
                            g->Left()->parent = g;
                        v->Right() = p;
                        p->Right() = g;
                        v->parent = g->parent;
                        g->parent = p;
                        p->parent = v;
                    } else {
                        //    g          v
                        // p      ->   p   g
                        //  v
                        p->Right() = v->Left();
                        if(p->Right())
                            p->Right()->parent = p;
                        g->Left() = v->Right();
                        if(g->Left())
                            g->Left()->parent = g;
                        v->Left() = p;
                        v->Right() = g;
                        v->parent = g->parent;
                        g->parent = p->parent = v;
                    }
                } else {
                    if(v == p->Left()) {
                        //  g                v
                        //     p      ->   g   p
                        //   v
                        p->Left() = v->Right();
                        if(p->Left())
                            p->Left()->parent = p;
                        g->Right() = v->Left();
                        if(g->Right())
                            g->Right()->parent = g;
                        v->Right() = p;
                        v->Left() = g;
                        v->parent = g->parent;
                        g->parent = p->parent = v;
                    } else {
                        //  g                v
                        //    p      ->    p
                        //      v        g
                        p->Right() = v->Left();
                        if(p->Right())
                            p->Right()->parent = p;
                        g->Right() = p->Left();
                        if(g->Right())
                            g->Right()->parent = g;
                        v->Left() = p;
                        p->Left() = g;
                        v->parent = g->parent;
                        g->parent = p;
                        p->parent = v;
                    }
                }
                g->synth();
                p->synth();
                v->synth();
                if(v->parent)
                    if(v->parent->Right() == g)
                        v->parent->Right() = v;
                    else
                        v->parent->Left() = v;
                else {
                    v->path_pa = g->path_pa;
                    g->path_pa = NULL;
                }
            }
            if(v->parent) {
                Node *p = v->parent;
                p->push();
                v->push();
                if(v == p->Left()) {
                    //   p
                    // v
                    p->Left() = v->Right();
                    if(p->Left())
                        p->Left()->parent = p;
                    v->Right() = p;
                    v->parent = p->parent;
                    p->parent = v;
                } else {
                    //   p
                    //     v
                    p->Right() = v->Left();
                    if(p->Right())
                        p->Right()->parent = p;
                    v->Left() = p;
                    v->parent = p->parent;
                    p->parent = v;
                }
                v->path_pa = p->path_pa;
                p->path_pa = NULL;
                p->synth();
                v->synth();
            }
            assert(v->parent == NULL);
        }

        int m_poolsz;
        int m_sz;
        Node *m_nodes;
};

int n, m, v[N], e[N][2];
int main()
{
    scanf("%d", &n);
    for(int i = 0; i < n; i++)
        scanf("%d", &v[i]);
    for(int i = 0; i < n-1; i++) {
        int x,y;
        scanf("%d%d", &x, &y);
        e[i][0] = x; e[i][1] = y;
    }
    scanf("%d", &m);
    LinkCutTree lct(n+m, n, v);
    for(int i = 0; i < n-1; i++) {
        lct.join(e[i][1], e[i][0]);
    }
    long long special = 0;
    for(int i = 0; i < m; i++) {
        int op, vx;
        long long px;
        scanf("%d", &op);
        switch(op) {
            case 1:
                scanf("%lld%d", &px, &vx);
                px += special;
                lct.extend((int)px,vx);
                break;
            case 2:
                scanf("%lld%d", &px, &vx);
                px += special;
                lct.add((int)px,vx);
                break;
            case 3:
                scanf("%lld", &px);
                px += special;
                lct.cut((int)px);
                break;
            case 4:
                scanf("%lld", &px);
                px += special;
                special = lct.sum((int)px);
                printf("%lld\n", special);
                break;
            default:
                assert(0 && "unknown op");
        }
    }
    return 0;
}

