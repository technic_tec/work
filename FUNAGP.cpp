#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <time.h>

#define N 200000

int n, q, r, p1, p2, r1, r2, pr1[N], pr2[N];
struct Node {
    int il, ir;
    int s1, d1;
    int s2, d2;
    int sum;
} v[N];

inline int power_mod(int a, int k, int p) {
    int res = 1;
    while(k) {
        if(k & 1)
            res = (int)((long long)res * (long long)a % p);
        a = (int)((long long)a * (long long)a % p);
        k >>= 1;
    }
    return res;
}
inline int sum_agp(int s, int d, int r, int r1, int pr[], int m, int p) {
    // s, (s+d)*r, (s+2d)*r^2, ..., (s+md)*r^m
    // Sm = ((s+md)*r^(m+1) - s - dr(r^m-1)/(r-1))/(r-1), (r>1)
    // Sm = (2s+md)(m+1)/2,   (r=1)
    int res = 0;
    if(r % p == 1) {
        res = (int)(((long long)(m+1)*(long long)s + (long long)m*(long long)(m+1)/2%p*(long long)d)%p);
    } else {
        res = (int)((((long long)s+(long long)m*(long long)d)%p*(long long)pr[m+1] - s - (long long)d*(long long)r%p*(long long)(pr[m]+p-1)%p*(long long)r1)%p * (long long)r1 % p);
        if(res < 0)
            res += p;
    }
    //printf("sum_agp(s=%d, d=%d, r=%d, m=%d, p=%d) = %d\n", s, d, r, m, p, res);
    return res;
}

void init(int n, int val[]) {
    int b = 2*n;
    while(b & (b-1))
        b &= b-1;
    for(int i = n; i < b; i++) {
        v[i].il = v[i].ir = n-b+i+1;
        v[i].sum = v[i].s1 = val[n-b+i] % p1;
        v[i].s2 = val[n-b+i] % p2;
        v[i].d1 = v[i].d2 = 0;
    }
    for(int i = b; i < 2*n; i++) {
        v[i].il = v[i].ir = i-b+1;
        v[i].sum = v[i].s1 = val[i-b] % p1;
        v[i].s2 = val[i-b] % p2;
        v[i].d1 = v[i].d2 = 0;
    }
    for(int i = n-1; i > 0; i--) {
        v[i].il = v[2*i].il;
        v[i].ir = v[2*i+1].ir;
        v[i].s1 = v[i].s2 = v[i].d1 = v[i].d2 = 0;
        v[i].sum = v[2*i].sum + v[2*i+1].sum;
        if(v[i].sum >= p1)
            v[i].sum -= p1;
        assert(v[i].il < v[i].ir);
    }
}

void update(Node *px, int s1, int d1, int s2, int d2, int x, int y) {
    if(px->il == px->ir)
        assert(px->s1 == px->sum);
    int qx1 = pr1[px->il-x];
    int qx2 = pr2[px->il-x];
    s1 = (int)(((long long)s1+(long long)(px->il-x)*(long long)d1)%p1 * (long long)qx1 % p1);
    d1 = (int)((long long)d1*(long long)qx1 % p1);
    s2 = (int)(((long long)s2+(long long)(px->il-x)*(long long)d2)%p2 * (long long)qx2 % p2);
    d2 = (int)((long long)d2*(long long)qx2 % p2);
    px->s1 += s1;
    px->d1 += d1;
    px->s2 += s2;
    px->d2 += d2;
    px->sum += sum_agp(s1, d1, r, r1, pr1, px->ir-px->il, p1);
    if(px->s1 >= p1) px->s1 -= p1;
    if(px->d1 >= p1) px->d1 -= p1;
    if(px->s2 >= p2) px->s2 -= p2;
    if(px->d2 >= p2) px->d2 -= p2;
    if(px->sum >= p1) px->sum -= p1;
    if(px->il == px->ir)
        assert(px->s1 == px->sum);
}

void accume(int px, int py = 0) {
    if(v[px].il < v[px].ir) {
        v[px].sum = v[2*px].sum + v[2*px+1].sum;
        if(v[px].sum >= p1)
            v[px].sum -= p1;
        v[px].sum += sum_agp(v[px].s1, v[px].d1, r, r1, pr1, v[px].ir-v[px].il, p1);
        if(v[px].sum >= p1)
            v[px].sum -= p1;
    } else {
        v[px].sum = v[px].s1;
    }
    for(px >>= 1; px > py; px >>= 1) {
        v[px].sum = v[2*px].sum + v[2*px+1].sum;
        if(v[px].sum >= p1)
            v[px].sum -= p1;
        v[px].sum += sum_agp(v[px].s1, v[px].d1, r, r1, pr1, v[px].ir-v[px].il, p1);
        if(v[px].sum >= p1)
            v[px].sum -= p1;
    }
}

void insert(int s, int d, int x, int y) {
    int px = 1;
    while(v[px].il != v[px].ir) {
        if(x >= v[2*px+1].il)
            px = 2*px+1;
        else if(y <= v[2*px].ir)
            px = 2*px;
        else
            break;
    }
    if(x <= v[px].il && y >= v[px].ir) {
        update(&v[px], s, d, s, d, x, y);
        accume(px);
        return;
    }
    int py = px;
    px = 2*px;
    while(1) {
        if(x <= v[px].il) {
            update(&v[px], s, d, s, d, x, y);
            break;
        } else if(x <= v[2*px].ir) {
            update(&v[2*px+1], s, d, s, d, x, y);
            px = 2*px;
        } else
            px = 2*px+1;
    }
    accume(px, py);
    py = 2*py+1;
    while(1) {
        if(y >= v[py].ir) {
            update(&v[py], s, d, s, d, x, y);
            break;
        } else if(y >= v[2*py+1].il) {
            update(&v[2*py], s, d, s, d, x, y);
            py = 2*py+1;
        } else
            py = 2*py;
    }
    accume(py);
}
void pow(int x, int g) {
    int px = 1;
    while(v[px].il < v[px].ir) {
        update(&v[2*px], v[px].s1, v[px].d1, v[px].s2, v[px].d2, v[px].il, v[px].ir);
        update(&v[2*px+1], v[px].s1, v[px].d1, v[px].s2, v[px].d2, v[px].il, v[px].ir);
        v[px].s1 = v[px].d1 = v[px].s2 = v[px].d2 = 0;
        if(x <= v[2*px].ir)
            px = 2*px;
        else
            px = 2*px+1;
    }
    v[px].s2 = power_mod(v[px].s2, g, p2);
    v[px].s1 = v[px].s2 % p1;
    accume(px);
}
int  sum(int x, int y) {
    int ss = 0;
    int px = 1;
    while(v[px].il != v[px].ir) {
        if(x >= v[2*px+1].il || y <= v[2*px].ir) {
            //if(n <= 10) printf("push %d\n", px);
            update(&v[2*px], v[px].s1, v[px].d1, v[px].s2, v[px].d2, v[px].il, v[px].ir);
            update(&v[2*px+1], v[px].s1, v[px].d1, v[px].s2, v[px].d2, v[px].il, v[px].ir);
            v[px].s1 = v[px].d1 = v[px].s2 = v[px].d2 = 0;
        }
        if(x >= v[2*px+1].il)
            px = 2*px+1;
        else if(y <= v[2*px].ir)
            px = 2*px;
        else
            break;
    }
    if(x <= v[px].il && y >= v[px].ir) {
        return v[px].sum;
    }
    //if(n <= 10) printf("push %d\n", px);
    update(&v[2*px], v[px].s1, v[px].d1, v[px].s2, v[px].d2, v[px].il, v[px].ir);
    update(&v[2*px+1], v[px].s1, v[px].d1, v[px].s2, v[px].d2, v[px].il, v[px].ir);
    v[px].s1 = v[px].d1 = v[px].s2 = v[px].d2 = 0;
    int py = px;
    px = 2*px;
    while(1) {
        if(x > v[px].il) {
            //if(n <= 10) printf("push %d\n", px);
            update(&v[2*px], v[px].s1, v[px].d1, v[px].s2, v[px].d2, v[px].il, v[px].ir);
            update(&v[2*px+1], v[px].s1, v[px].d1, v[px].s2, v[px].d2, v[px].il, v[px].ir);
            v[px].s1 = v[px].d1 = v[px].s2 = v[px].d2 = 0;
        }
        if(x <= v[px].il) {
            ss += v[px].sum;
            if(ss >= p1) ss -= p1;
            break;
        } else if(x <= v[2*px].ir) {
            ss += v[2*px+1].sum;
            if(ss >= p1) ss -= p1;
            px = 2*px;
        } else
            px = 2*px+1;
    }
    py = 2*py+1;
    while(1) {
        if(y < v[py].ir) {
            //if(n <= 10) printf("push %d\n", py);
            update(&v[2*py], v[py].s1, v[py].d1, v[py].s2, v[py].d2, v[py].il, v[py].ir);
            update(&v[2*py+1], v[py].s1, v[py].d1, v[py].s2, v[py].d2, v[py].il, v[py].ir);
            v[py].s1 = v[py].d1 = v[py].s2 = v[py].d2 = 0;
        }
        if(y >= v[py].ir) {
            ss += v[py].sum;
            if(ss >= p1) ss -= p1;
            break;
        } else if(y >= v[2*py+1].il) {
            ss += v[2*py].sum;
            if(ss >= p1) ss -= p1;
            py = 2*py+1;
        } else
            py = 2*py;
    }
    return ss;
}

int val[N], v1[N], v2[N];
void test(int _n, int _q, int _r, int _p1, int _p2)
{
    n = _n; q = _q; r = _r; p1 = _p1; p2 = _p2;
    const int nn = 100000;
    const int pp = 100000000;
    srand(time(NULL));
    for(int i = 0; i < n; i++)
        val[i] = (unsigned)rand() % pp;
    r1 = power_mod(r-1, p1-2, p1);
    r2 = power_mod(r-1, p2-2, p2);
    pr1[0] = pr2[0] = 1;
    for(int i = 1; i <= n; i++) {
        pr1[i] = (int)((long long)pr1[i-1]*(long long)r%p1);
        pr2[i] = (int)((long long)pr2[i-1]*(long long)r%p2);
    }
    init(n, val);
    if(n <= 10) {
        printf("n = %d, r = %d, p1 = %d, p2 = %d\n", n, r, p1, p2);
        for(int i = 1; i < 2*n; i++)
            printf("[%d-%d] (%d, %d) (%d, %d) %d\n", v[i].il, v[i].ir, v[i].s1, v[i].d1, v[i].s2, v[i].d2, v[i].sum);
    }
    for(int i = 1; i <= n; i++) {
        v1[i] = val[i-1] % p1;
        v2[i] = val[i-1] % p2;
    }
    for(int i = 0; i < q; i++) {
        unsigned x = rand();
        if(x < 0x30000000U) {
            int s = (unsigned)rand() % nn;
            int d = (unsigned)rand() % nn;
            int x = (unsigned)rand() % n + 1;
            int y = (unsigned)rand() % n + 1;
            if(x > y) {
                int z = x; x = y; y = z;
            }
            insert(s, d, x, y);
            if(n <= 10) {
                printf("insert(s=%d, d=%d, x=%d, y=%d)\n", s, d, x, y);
                for(int i = 1; i < 2*n; i++)
                    printf("[%d-%d] (%d, %d) (%d, %d) %d\n", v[i].il, v[i].ir, v[i].s1, v[i].d1, v[i].s2, v[i].d2, v[i].sum);
            }
            for(int i = x, s1 = s, r1 = 1; i <= y; i++, s1 = (s1+d)%p1, r1 = (int)((long long)r1*(long long)r%p1))
                v1[i] = (int)(((long long)s1 * (long long)r1 + (long long)v1[i]) % p1);
            for(int i = x, s1 = s, r1 = 1; i <= y; i++, s1 = (s1+d)%p2, r1 = (int)((long long)r1*(long long)r%p2))
                v2[i] = (int)(((long long)s1 * (long long)r1 + (long long)v2[i]) % p2);
        } else if(x < 0x60000000U) {
            int s = rand() % n + 1;
            int g = rand() % 1000;
            pow(s, g);
            if(n <= 10) {
                printf("pow(s=%d, g=%d)\n", s, g);
                for(int i = 1; i < 2*n; i++)
                    printf("[%d-%d] (%d, %d) (%d, %d) %d\n", v[i].il, v[i].ir, v[i].s1, v[i].d1, v[i].s2, v[i].d2, v[i].sum);
            }
            v2[s] = power_mod(v2[s], g, p2);
            v1[s] = v2[s] % p1;
        } else {
            int x = rand() % n + 1;
            int y = rand() % n + 1;
            if(x > y) {
                int z = x; x = y; y = z;
            }
            int res = sum(x, y);
            if(n <= 10) {
                printf("sum(x=%d, y=%d)\n", x, y);
                for(int i = 1; i < 2*n; i++)
                    printf("[%d-%d] (%d, %d) (%d, %d) %d\n", v[i].il, v[i].ir, v[i].s1, v[i].d1, v[i].s2, v[i].d2, v[i].sum);
            }
            int exp = 0;
            for(int i = x; i <= y; i++) {
                exp += v1[i];
                if(exp >= p1) exp -= p1;
            }
            if(exp != res)
                printf("exp: %d, res: %d\n", exp, res);
            assert(exp == res);
        }
    }
    printf("%d %d %d %d %d: Passed\n", n, q, r, p1, p2);
}
void tryrun(int _n, int _q, int _r, int _p1, int _p2)
{
    n = _n; q = _q; r = _r; p1 = _p1; p2 = _p2;
    const int nn = 100000;
    const int pp = 100000000;
    srand(time(NULL));
    for(int i = 0; i < n; i++)
        val[i] = (unsigned)rand() % pp;
    r1 = power_mod(r-1, p1-2, p1);
    r2 = power_mod(r-1, p2-2, p2);
    pr1[0] = pr2[0] = 1;
    for(int i = 1; i <= n; i++) {
        pr1[i] = (int)((long long)pr1[i-1]*(long long)r%p1);
        pr2[i] = (int)((long long)pr2[i-1]*(long long)r%p2);
    }
    init(n, val);
    for(int i = 0; i < q; i++) {
        unsigned x = rand();
        if(x < 0x30000000U) {
            int s = (unsigned)rand() % nn;
            int d = (unsigned)rand() % nn;
            int x = (unsigned)rand() % n + 1;
            int y = (unsigned)rand() % n + 1;
            if(x > y) {
                int z = x; x = y; y = z;
            }
            insert(s, d, x, y);
        } else if(x < 0x60000000U) {
            int s = rand() % n + 1;
            int g = rand() % 1000;
            pow(s, g);
        } else {
            int x = rand() % n + 1;
            int y = rand() % n + 1;
            if(x > y) {
                int z = x; x = y; y = z;
            }
            int res = sum(x, y);
            printf("%d\n", res);
        }
    }
}

int main()
{
#if 0
    tryrun(100000, 100000, 555555555, 99999971, 99999989);
    return 0;
    test(100, 100, 1, 19, 17);
    test(100, 100, 5, 17, 19);
    test(100, 100, 36, 5, 7);
    test(100, 100, 41, 5, 7);
    test(100, 100, 50, 5, 7);
    test(1000, 1000, 500000000, 99999989, 99999971);
    test(1000, 1000, 555555555, 99999971, 99999989);
    return 0;
#endif
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d%d%d%d", &n, &q, &r, &p1, &p2);
        for(int i = 0; i < n; i++)
            scanf("%d", &val[i]);
        r1 = power_mod(r-1, p1-2, p1);
        r2 = power_mod(r-1, p2-2, p2);
        pr1[0] = pr2[0] = 1;
        for(int i = 1; i <= n; i++) {
            pr1[i] = (int)((long long)pr1[i-1]*(long long)r%p1);
            pr2[i] = (int)((long long)pr2[i-1]*(long long)r%p2);
        }
        init(n, val);
        while(q--) {
            int op;
            scanf("%d", &op);
            if(op == 0) {
                int s, d, x, y;
                scanf("%d%d%d%d", &s, &d, &x, &y);
                insert(s, d, x, y);
            } else if(op == 1) {
                int x, g;
                scanf("%d%d", &x, &g);
                pow(x, g);
            } else {
                int x, y;
                scanf("%d%d", &x, &y);
                printf("%d\n", sum(x, y));
            }
        }
    }
    return 0;
}

