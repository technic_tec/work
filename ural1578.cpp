#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;
#define N 2010

typedef struct Point {
  int x, y;
  bool v;
  Point(int _x, int _y) : x(_x), y(_y), v(false) { }
} Point;

int main()
{
  int n;

#ifndef ONLINE_JUDGE
  freopen("in", "r", stdin);
  freopen("out", "w", stdout);
#endif
  scanf("%d", &n);
  n += 2;

  vector<Point> pts;
  for(int i = 0; i < n; i++) {
    int x, y;
    scanf("%d%d", &x, &y);
    pts.push_back(Point(x, y));
  }

  printf("YES\n");
  int p = 0;
  pts[p].v = true;
  printf("1");
  for(int i = 1; i < n; i++) {
    int im = 0;
    while(pts[im].v) ++im;
    int dm = (pts[im].x-pts[p].x)*(pts[im].x-pts[p].x) + (pts[im].y-pts[p].y)*(pts[im].y-pts[p].y);
    for(int i = im+1; i < n; i++)
      if(!pts[i].v) {
        int d = (pts[i].x-pts[p].x)*(pts[i].x-pts[p].x) + (pts[i].y-pts[p].y)*(pts[i].y-pts[p].y);
        if(d > dm) {
          dm = d; im = i;
        }
      }
    p = im;
    pts[p].v = true;
    printf(" %d", 1+p);
  }
  printf("\n");
  return 0;
}
