/*
 * =====================================================================================
 *
 *       Filename:  2048.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年03月31日 15时50分26秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstring>
#include <cmath>
#include <sys/time.h>
#include <cassert>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;

#define WIN_VAL 4096
#define WIN_SCORE 10000
#define WIN_PANAL  9900
#define MIN_SEARCH_TIME 200000 // in usec

typedef pair<int, int> Point;
typedef vector<vector<int> > Board;

const double epsilon = 1e-6;
const int dv[][2] = {
    {-1, 0}, //NORTH
    {0, 1},  //EAST
    {1, 0},  //SOUTH
    {0, -1}  //WEST
};
const char *strMove[] = {
    "UP",
    "RIGHT",
    "DOWN",
    "LEFT"
};

inline int ilog2(int v) {
    int r = 0;
    assert(v > 0);
    while(v > 1) {
        v >>= 1;
        ++r;
    }
    return r;
}

class Grid {
    public:
        Grid(Board &val) {
            m_cells = val;
            m_playerTurn = true;
        }
        Grid(Grid &o) {
            m_cells = o.m_cells;
            m_playerTurn = o.m_playerTurn;
        }
        ~Grid() {}
        void insertTile(Point x, int val) {
            assert(x.first >= 0 && x.first < 4 && x.second >= 0 && x.second < 4);
            assert(val > 0 && m_cells[x.first][x.second] == 0);
            m_cells[x.first][x.second] = val;
        }
        void removeTile(Point x) {
            assert(x.first >= 0 && x.first < 4 && x.second >= 0 && x.second < 4);
            assert(m_cells[x.first][x.second] > 0);
            m_cells[x.first][x.second] = 0;
        }
        bool move(int direction)
        {
            assert(direction >= 0 && direction < 4);
            bool moved = false;
            const int *vec = dv[direction];
            Board merged(4, vector<int>(4));
            for(int i = 0; i < 4; i++) {
                for(int j = 0; j < 4; j++)
                    merged[i][j] = false;
            }

            for(int i = 0; i < 4; i++) {
                int x, y;
                if(vec[0] == -1) {
                    x = 0; y = i;
                } else if(vec[0] == 1) {
                    x = 3; y = i;
                } else if(vec[1] == -1) {
                    x = i; y = 0;
                } else {
                    x = i; y = 3;
                }
                while(x >= 0 && x < 4 && y >= 0 && y < 4) {
                    if(m_cells[x][y]) {
                        int nx = x + vec[0], ny = y + vec[1];
                        while(nx >= 0 && nx < 4 && ny >= 0 && ny < 4 && !m_cells[nx][ny]) {
                            nx += vec[0]; ny += vec[1];
                        }
                        if(nx >= 0 && nx < 4 && ny >= 0 && ny < 4 && m_cells[nx][ny] == m_cells[x][y] && !merged[x][y]) {
                            m_cells[nx][ny] += m_cells[x][y];
                            m_cells[x][y] = 0;
                            merged[nx][ny] = true;
                            moved = true;
                        } else if(nx - vec[0] != x || ny - vec[1] != y) {
                            m_cells[nx-vec[0]][ny-vec[1]] = m_cells[x][y];
                            m_cells[x][y] = 0;
                            moved = true;
                        }
                    }
                    x -= vec[0]; y -= vec[1];
                }
            }
            if(moved)
                m_playerTurn = false;
            return moved;
        }
        bool isWin() {
            for(int x = 0; x < 4; x++)
                for(int y = 0; y < 4; y++)
                    if(m_cells[x][y] == WIN_VAL)
                        return true;
            return false;
        }
        vector<Point> availableCells() {
            vector<Point> cells;
            for(int x = 0; x < 4; x++)
                for(int y = 0; y < 4; y++)
                    if(m_cells[x][y] == 0)
                        cells.push_back(make_pair(x, y));
            return cells;
        }
        // measures how smooth the grid is (as if the values of the pieces
        // were interpreted as elevations). Sums of the pairwise difference
        // between neighboring tiles (in log space, so it represents the
        // number of merges that need to happen before they can merge). 
        // Note that the pieces can be distant
        int smoothness() {
            int smoothness = 0;
            for(int x = 0; x < 4; x++)
                for(int y = 0; y < 4; y++)
                    if(m_cells[x][y]) {
                        int value = ilog2(m_cells[x][y]);
                        for(int direction = 1; direction <= 2; direction++) {
                            int tx = x + dv[direction][0], ty = y + dv[direction][1];
                            while(tx >= 0 && tx < 4 && ty >= 0 && ty < 4 && !m_cells[tx][ty]) {
                                tx += dv[direction][0]; ty += dv[direction][1];
                            }
                            if(tx >= 0 && tx < 4 && ty >= 0 && ty < 4) {
                                int targetValue = ilog2(m_cells[tx][ty]);
                                smoothness -= abs(value - targetValue);
                            }
                        }
                    }
            return smoothness;
        }
        // count islands of connected equal-value filled cells
        int islands() {
            Board marked(4, vector<int>(4));
            int islands = 0;
            for(int x = 0; x < 4; x++) {
                for(int y = 0; y < 4; y++)
                    if(m_cells[x][y])
                        marked[x][y] = false;
            }
            for(int x = 0; x < 4; x++)
                for(int y = 0; y < 4; y++)
                    if(m_cells[x][y] && !marked[x][y]) {
                        vector<Point> hp;
                        marked[x][y] = true;
                        hp.push_back(make_pair(x, y));
                        while(!hp.empty()) {
                            Point p = hp.back();
                            hp.pop_back();
                            for(int d = 0; d < 4; d++) {
                                if(p.first + dv[d][0] >= 0 && p.first + dv[d][0] < 4 && p.second + dv[d][1] >= 0 && p.second + dv[d][1] < 4 && m_cells[p.first+dv[d][0]][p.second+dv[d][1]] == m_cells[p.first][p.second] && !marked[p.first+dv[d][0]][p.second+dv[d][1]]) {
                                    marked[p.first+dv[d][0]][p.second+dv[d][1]] = true;
                                    hp.push_back(make_pair(p.first+dv[d][0], p.second+dv[d][1]));
                                }
                            }
                        }
                        islands++;
                    }
            return islands;
        }
        double eval() {
            int emptyCells = availableCells().size();
            double smoothWeight = 0.1;
            double monoWeight = 1.0;
            double emptyWeight = 2.7;
            double maxWeight = 1.0;
            return smoothness() * smoothWeight + monotonicity() * monoWeight + log(emptyCells) * emptyWeight + maxValue() * maxWeight;
        }
        bool m_playerTurn;
    private:
        int maxValue() {
            int maxVal = 0;
            for(int x = 0; x < 4; x++)
                for(int y = 0; y < 4; y++)
                    if(m_cells[x][y] > maxVal)
                        maxVal = m_cells[x][y];
            return ilog2(maxVal);
        }
        int monotonicity() {
            int totals[] = {0, 0, 0, 0};
            // left/right direction
            for(int x = 0; x < 4; x++) {
                int curr = 0;
                int next = 1;
                while(next < 4) {
                    while(next < 4 && !m_cells[x][next])
                        ++next;
                    if(next >= 4)
                        --next;
                    int currVal = (m_cells[x][curr] ? ilog2(m_cells[x][curr]) : 0);
                    int nextVal = (m_cells[x][next] ? ilog2(m_cells[x][next]) : 0);
                    if(currVal > nextVal)
                        totals[0] += nextVal - currVal;
                    else if(nextVal > currVal)
                        totals[1] += currVal - nextVal;
                    curr = next;
                    ++next;
                }
            }
            // up/down direction
            for(int y = 0; y < 4; y++) {
                int curr = 0;
                int next = 1;
                while(next < 4) {
                    while(next < 4 && !m_cells[next][y])
                        ++next;
                    if(next >= 4)
                        --next;
                    int currVal = (m_cells[curr][y] ? ilog2(m_cells[curr][y]) : 0);
                    int nextVal = (m_cells[next][y] ? ilog2(m_cells[next][y]) : 0);
                    if(currVal > nextVal)
                        totals[2] += nextVal - currVal;
                    else if(nextVal > currVal)
                        totals[3] += currVal - nextVal;
                    curr = next;
                    ++next;
                }
            }
            return max(totals[0], totals[1]) + max(totals[2], totals[3]);
        }
        Board m_cells;
};

struct Result {
    int move;
    double score;
};

#if 1
Result search(int depth, double alpha, double beta, Grid &grid) {
    double bestScore;
    int bestMove = -1;
    Result res;

    if(grid.m_playerTurn) {
        bestScore = alpha;
        for(int direction = 0; direction < 4; direction++) {
            Grid newGrid(grid);
            if(newGrid.move(direction)) {
                if(newGrid.isWin()) {
                    res.move = direction; res.score = WIN_SCORE;
                    return res;
                }
                if(depth == 0) {
                    res.move = direction;
                    res.score = newGrid.eval();
                } else {
                    res = search(depth-1, bestScore, beta, newGrid);
                    if(res.score > WIN_PANAL)
                        res.score -= 1;
                }
                if(res.score > bestScore) {
                    bestScore = res.score;
                    bestMove = direction;
                }
                if(bestScore > beta) {
                    res.move = bestMove;
                    res.score = bestScore;
                    return res;
                }
            }
        }
    } else {
        bestScore = beta;
        vector<pair<Point, int> > candidates;
        vector<Point> cells = grid.availableCells();
        vector<int> scores[2];
        for(int value = 2; value <= 4; value += 2)
            for(int i = 0; i < cells.size(); i++) {
                grid.insertTile(cells[i], value);
                scores[value/2-1].push_back(-grid.smoothness()+grid.islands());
                grid.removeTile(cells[i]);
            }
        // pick out the most annoying moves;
        double maxScore = max(*max_element(scores[0].begin(), scores[0].end()), *max_element(scores[1].begin(), scores[1].end()));
        for(int value = 2; value <= 4; value += 2)
            for(int i = 0; i < scores[value/2-1].size(); i++)
                if(fabs(scores[value/2-1][i] - maxScore) < epsilon)
                    candidates.push_back(make_pair(cells[i], value));
        // search on each candidate
        for(int i = 0; i < candidates.size(); i++) {
            Point pos = candidates[i].first;
            int value = candidates[i].second;
            Grid newGrid(grid);
            newGrid.insertTile(pos, value);
            newGrid.m_playerTurn = true;
            res = search(depth, alpha, bestScore, newGrid);
            if(res.score < bestScore)
                bestScore = res.score;
            if(bestScore < alpha) {
                res.move = bestMove;
                res.score = bestScore;
                return res;
            }
        }
    }
    res.move = bestMove;
    res.score = bestScore;
    return res;
}

// iterative deepening over the alpha-beta search
int DFS_ID(Grid &grid) {
    struct timeval start, now, diff;
    int depth = 0;
    Result best;
    gettimeofday(&start, NULL);
    //printf("start %d.%06d\n", start.tv_sec, start.tv_usec);
    do {
        //printf("depth %d\n", depth);
        Result newBest = search(depth, -WIN_SCORE, WIN_SCORE, grid);
        if(newBest.move == -1)
            break;
        else
            best = newBest;
        depth++;
        gettimeofday(&now, NULL);
        timersub(&now, &start, &diff);
        //printf("now %d.%06d diff %d.%06d\n", now.tv_sec, now.tv_usec, diff.tv_sec, diff.tv_usec);
    } while(diff.tv_sec*1000000 + diff.tv_usec < MIN_SEARCH_TIME);
    return best.move;
}

const char *nextMove(vector<vector<int> > board) {
    const char *res = "";
    Grid grid(board);
    int move = DFS_ID(grid);
    if(move >= 0)
        res = strMove[move];
    return res;
}
#endif

int main() {
#if 1
    vector<vector<int> > board(4,  vector<int>(4));

    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            scanf("%d",  &board[i][j]);

    printf("%s\n", nextMove(board));
#endif
    return 0;
}
