#!/usr/bin/python
# Head ends here
from fractions import gcd
from pprint import pprint
# if n < 1,373,653, it is enough to test a = 2 and 3;
# if n < 9,080,191, it is enough to test a = 31 and 73;
# if n < 4,759,123,141, it is enough to test a = 2, 7, and 61;
# if n < 1,122,004,669,633, it is enough to test a = 2, 13, 23, and 1662803;
# if n < 2,152,302,898,747, it is enough to test a = 2, 3, 5, 7, and 11;
# if n < 3,474,749,660,383, it is enough to test a = 2, 3, 5, 7, 11, and 13;
# if n < 341,550,071,728,321, it is enough to test a = 2, 3, 5, 7, 11, 13, and 17.
# if n < 3,825,123,056,546,413,051, it is enough to test a = 2, 3, 5, 7, 11, 13, 17, 19, and 23.
def power_mod(n, k, p):
    r = 1
    while k > 0:
        if k & 1:
            r = r * n % p
        n = n * n % p
        k >>= 1
    return r

def witness(n, p):
    t, k = n-1, 0
    while (t & 1) == 0:
        t >>= 1
        k += 1
    r = power_mod(p, t, n)
    if r == 1 or r == n-1:
        return False
    for _ in range(k):
        r = r * r % n
        if r == n-1:
            return False
        elif r == 1:
            return True
    return True

def is_prime(n):
    if n in [0, 1]:
        return False
    for p in [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]:
        if n == p:
            return True
        elif n % p == 0:
            return False
    if n < 1373653L:
        l = [2, 3]
    elif n < 9080191L:
        l = [31, 73]
    elif n < 4759123141L:
        l = [2, 7, 61]
    elif n < 1122004669633L:
        l = [2, 13, 23, 1662803]
    elif n < 2152302898747L:
        l = [2, 3, 5, 7, 11]
    elif n < 3474749660383L:
        l = [2, 3, 5, 7, 11, 13]
    elif n < 341550071728321L:
        l = [2, 3, 5, 7, 11, 13, 17]
    elif n < 3825123056546413051L:
        l = [2, 3, 5, 7, 11, 13, 17, 19, 23]
    else:
        raise ValueError
    for p in l:
        if witness(n, p):
            return False
    return True

def pollard_rho(n):
    x, y, d, c = 2, 2, 1, 1
    while True:
        while d == 1:
            x = (x*x+c)%n
            y = (y*y+c)%n
            y = (y*y+c)%n
            d = gcd(abs(x - y),  n)
        if d == n:
            x, y, d, c = 2, 2, 1, c+2
        else:
            return d

def factor(n):
    k = 0
    while (n & 1) == 0:
        n >>= 1
        k += 1
    if n == 1:
        return [2]*k
    elif is_prime(n):
        return [2]*k + [n]
    else:
        x = pollard_rho(n)
        return [2]*k + factor(x)+factor(n/x)

def divisor_dag(n):
    dag = dict()
    chld = dict()
    f = factor(n)
    f.sort()
    dag[n] = set()
    cand = {n}
    while len(cand) > 0:
        x = cand.pop()
        cx = set()
        yl = None
        for y in f:
            if y != yl and x % y == 0:
                z = x/y
                dag.setdefault(z, set()).add(x)
                cx.add(z)
                cand.add(z)
            yl = y
        chld[x] = cx
    return dag, chld

def unfriendlyNumbers(a,k):
    answer = 0
    dag, chld = divisor_dag(k)
    b = set(gcd(x,k) for x in a)
    q = [k]
    while len(q) > 0:
        x = q.pop()
        if x in b:
            continue
        answer += 1
        for y in chld[x]:
            dag[y].remove(x)
            if len(dag[y]) == 0:
                q.append(y)
    return answer

# Tail starts here
if __name__ == '__main__':
    a = map(int, raw_input().strip().split(" "))
    _a_size=a[0]
    _k=a[1]
    b = map(int, raw_input().strip().split(" "))
    print unfriendlyNumbers(b,_k)
