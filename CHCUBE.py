t = int(raw_input())
for _ in range(t):
  fr, bk, l, r, tp, bt = raw_input().split()
  res = 'NO'
  for x in (fr, bk):
    for y in (l, r):
      for z in (tp, bt):
        if x == y and y == z:
          res = 'YES'
  print res
