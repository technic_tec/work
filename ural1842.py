# length of local root ri = local period at position i
# maxima local period in a word = global period
# global period can be computed using KMP
# Critical Factorization Theorem: the period of a word w is always locally detectable in at least one position of w resulting in a corresponding critical factorization.
# critical factorization can be found in p=|w|-l, where l=min(|v|, |v'|), v is the maximal suffix in alphabet order, while v' is maximal suffix in reverse alphabet order.

def maximal_suffix(w):
  i, j, k, p = 0, 1, 0, 1
  while j+k < len(w):
    if w[i+k] > w[j+k]:
      j += k + 1
      k, p = 0, j-i
    elif w[i+k] < w[j+k]:
      i, j, k, p = j, j+1, 0, 1
    elif k != p:
      k += 1
    else:
      j += p
      k = 0
  return (i, p)

def maximal_suffix_tilded(w):
  i, j, k, p = 0, 1, 0, 1
  while j+k < len(w):
    if w[i+k] < w[j+k]:
      j += k + 1
      k, p = 0, j-i
    elif w[i+k] > w[j+k]:
      i, j, k, p = j, j+1, 0, 1
    elif k != p:
      k += 1
    else:
      j += p
      k = 1
  return (i, p)

def critical_factorization(w):
  r1, r2 = maximal_suffix(w), maximal_suffix_tilded(w)
  return max(r1, r2)[0]

def global_period(w):
  t = [-1, 0]
  pos, cnd = 2, 0
  while pos <= len(w):
    if w[pos-1] == w[cnd]:
      cnd += 1
      t.append(cnd)
      pos += 1
    elif cnd > 0:
      cnd = t[cnd]
    else:
      t.append(0)
      pos += 1
  return len(w)-t[-1]

w = raw_input().strip()
p = critical_factorization(w)
l = global_period(w)
if p == 0:
  p += l
print p, l
