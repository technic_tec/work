#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 50

int n, m, k, c[N], s[N];
struct Baloon{
    int c, p;
    bool operator<(const Baloon &o) const {
        return c < o.c;
    }
} b[N];
double pp[N][N], ps[N][N];

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d", &n, &m);
        for(int i = 0; i < n; i++)
            scanf("%d%d", &b[i].c, &b[i].p);
        std::sort(b, b+n);
        k = 0;
        for(int i = 0; i < n; i++)
            if(i && b[i].c == b[i-1].c) {
                c[k-1]++; s[k-1] += b[i].p;
            } else {
                c[k++] = 1; s[k-1] = b[i].p;
            }
        memset(pp, 0, sizeof(pp));
        pp[0][0] = 1.0;
        for(int i = 1; i <= k; i++) {
            double p1 = pow(0.5, c[i-1]);
            pp[i][0] = pp[i-1][0]*p1;
            for(int j = 1; j <= i; j++)
                pp[i][j] = pp[i-1][j]*p1 + pp[i-1][j-1]*(1.0-p1);
        }
        memset(ps, 0, sizeof(pp));
        ps[k][0] = 1.0;
        for(int i = k-1; i >= 0; i--) {
            double p1 = pow(0.5, c[i]);
            ps[i][0] = ps[i+1][0]*p1;
            for(int j = 1; j <= k-i; j++)
                ps[i][j] = ps[i+1][j]*p1 + ps[i+1][j-1]*(1.0-p1);
        }
        for(int i = k; i >= 0; i--)
            for(int j = k-i; j >= 0; j--)
                ps[i][j] += ps[i][j+1];
        double res = 0.0;
        for(int i = 0; i < k; i++) {
            double p0 = 0.0, p1 = 0.0, px = pow(0.5, c[i]);
            for(int j = 0; j <= k; j++)
                p0 += pp[i][j]*ps[i+1][std::max(0, m-j)]*px;
            for(int j = 0; j <= k; j++)
                p1 += pp[i][j]*ps[i+1][std::max(0, m-1-j)]*(1.0-px);
            res += p1/(p0+p1)*s[i]*0.5/(1.0-px);
        }
        printf("%.9lf\n", res);
    }
    return 0;
}

