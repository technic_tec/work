import qualified Data.IntMap as IntMap

rev_fac :: Int -> [Int] -> IntMap.IntMap (Maybe [Int]) -> (Maybe [Int], IntMap.IntMap (Maybe [Int]))
rev_fac n a mp | IntMap.member n mp = (mp IntMap.! n, mp)
               | otherwise = foldl facUpdate (Nothing, mp) a
              where mergeUpdate rs Nothing x = rs
                    mergeUpdate Nothing (Just cs') x = Just (x:cs')
                    mergeUpdate (Just cs) (Just cs') x | (length cs') + 1 < (length cs) = Just (x:cs')
                                                       | otherwise = Just cs
                    facUpdate (rs, mp) x | n `mod` x > 0 = (rs, mp)
                                         | otherwise = let (rs', mp') = rev_fac (div n x) a mp
                                                           rs'' = mergeUpdate rs rs' x
                                                           mp'' = IntMap.insert n rs'' mp'
                                                       in (rs'', mp'')

answer :: Maybe [Int] -> [Int]
answer Nothing = [-1]
answer (Just cs) = scanl (\x y -> x*y) 1 cs

main :: IO()
main = do
    s <- getLine
    let [n, k] = map read $ words s
    s <- getLine
    let a = map read $ words s
        mp = IntMap.fromList [(1, Just [])]
        (rs,mp') = rev_fac n a mp
    putStrLn $ unwords $ map show $ answer rs
