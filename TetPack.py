#Pieces
PCS = {
        'Iv':[(0, 0), (1, 0), (2, 0), (3, 0)],
        'Ih':[(0, 0), (0, 1), (0, 2), (0, 3)],
        'Tn':[(0, 0), (1, -1), (1, 0), (1, 1)],
        'Te':[(0, 0), (1, 0), (1, 1), (2, 0)],
        'Ts':[(0, 0), (0, 1), (0, 2), (1, 1)],
        'Tw':[(0, 0), (1, -1), (1, 0), (2, 0)],
        'Jn':[(0, 0), (1, 0), (2, -1), (2, 0)],
        'Je':[(0, 0), (1, 0), (1, 1), (1, 2)],
        'Js':[(0, 0), (0, 1), (1, 0), (2, 0)],
        'Jw':[(0, 0), (0, 1), (0, 2), (1, 2)],
        'Ln':[(0, 0), (1, 0), (2, 0), (2, 1)],
        'Le':[(0, 0), (0, 1), (0, 2), (1, 0)],
        'Ls':[(0, 0), (0, 1), (1, 1), (2, 1)],
        'Lw':[(0, 0), (1, -2), (1, -1), (1, 0)],
        'Sv':[(0, 0), (1, 0), (1, 1), (2, 1)],
        'Sh':[(0, 0), (0, 1), (1, -1), (1, 0)],
        'Zv':[(0, 0), (1, -1), (1, 0), (2, -1)],
        'Zh':[(0, 0), (0, 1), (1, 1), (1, 2)],
        'O':[(0, 0), (0, 1), (1, 0), (1, 1)],
        'j':[(0, 0), (1, -1), (1, 0)],
        'l':[(0, 0), (1, 0), (1, 1)],
        '7':[(0, 0), (0, 1), (1, 1)],
        'r':[(0, 0), (0, 1), (1, 0)],
        '-':[(0, 0), (0, 1)],
        '|':[(0, 0), (1, 0)],
        'o':[(0, 0)]
}

class Tetris(object):
    def __init__(self, w, h, pcs, layout = None):
        self.w, self.h = w, h
        if layout is None:
            self.b = [['.']*w for _ in range(h)]
        else:
            self.b = [list(r.strip()) for r in layout if len(r.strip()) > 0]
        assert len(self.b) == h and max(map(len, self.b)) == w and min(map(len, self.b)) == w
        self.pcs = sorted(pcs)
        self.used = set()

    def solve(self):
        if self.try_fill(*self.next_empty(0, 0)):
            print '\n'.join(''.join(c[0] for c in r) for r in self.b)
            return True
        else:
            print 'No solution!'
            return False

    def try_fill(self, x, y):
        if x >= self.h:
            return True
        last = None
        for i, p in enumerate(self.pcs):
            if self.pcs[i] == last or i in self.used:
                continue
            if self.try_place(x, y, p):
                self.used.add(i)
                if self.try_fill(*self.next_empty(x, y)):
                    return True
                #print '\n'.join(''.join(c[0] for c in r) for r in self.b)+'\n'
                self.remove(x, y, p)
                self.used.remove(i)
            last = p
        return False

    def next_empty(self, x, y):
        while x < self.h:
            if self.b[x][y] == '.':
                return (x, y)
            y += 1
            if y >= self.w:
                x, y = x+1, 0
        return (x, y)

    def try_place(self, x, y, p):
        for dx, dy in PCS[p]:
            if x+dx<0 or x+dx>=self.h or y+dy<0 or y+dy>=self.w or self.b[x+dx][y+dy] != '.':
                return False
        for dx, dy in PCS[p]:
            self.b[x+dx][y+dy] = p[0]
        return True

    def remove(self, x, y, p):
        for dx, dy in PCS[p]:
            assert self.b[x+dx][y+dy] == p[0]
            self.b[x+dx][y+dy] = '.'

def draw_piece(p):
    mx, my = min(x for (x, y) in p), min(y for (x, y) in p)
    sx, sy = max(x for (x, y) in p), max(y for (x, y) in p)
    b = [['.']*(sy-my+1) for _ in range(sx-mx+1)]
    for x, y in p:
        b[x-mx][y-my] = '*'
    print '\n'.join('\t'+''.join(r) for r in b)

if __name__ == '__main__':
    n = 8
    layout = """........
                ..X.....
                .XXX....
                ..X.....
                .....X..
                ....XXX.
                .....X..
                ........""".splitlines()
    pcs = ['Sh', 'Sv', 'Zv', 'Tw', 'Te', 'Ts', 'Ts', 'Tn', 'O', 'Le', 'Ln', 'l', '7', 'j', 'o']
    #for p in pcs:
    #    print p
    #    draw_piece(PCS[p])
    Tetris(n, n, pcs, layout).solve()
