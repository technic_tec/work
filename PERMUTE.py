from itertools import permutations
import sys

def check(p, s):
    for i in range(1, len(p)):
        if p[i-1]+p[i] > s:
            return False
    return True

k = int(sys.argv[1])
cs = list()
for i in range(1, 9):
    c = 0
    for p in permutations(range(1, i+1)):
        if check(p, i+k):
            c += 1
    cs.append(c)
print cs
