import Data.List
import Data.Array
import Data.Maybe

partial_sum :: [Integer] -> Array Int Integer
partial_sum vs = listArray (0, length vs) $ scanl (+) 0 $ reverse $ sort vs

binarylookup :: Integer->Array Int Integer->(Int, Int)->Int
binarylookup s arr (l, h) | s <= arr!l = l
                    | s > arr!h = -1
					| h-l==1 = h
					| s > arr!m = binarylookup s arr (m, h)
					| otherwise = binarylookup s arr (l, m)
	where m = div (h+l) 2

process :: Int->Array Int Integer->IO()
process 0 arr = return()
process t arr = do
  sl <- getLine
  let s = read sl
  putStrLn $ show $ binarylookup s arr (bounds arr)
  process (t-1) arr

main :: IO()
main = do
  s <- getLine
  --let n = read s
  s <- getLine
  let v = map read $ words s
  let arr = partial_sum v
  s <- getLine
  let t = read s
  process t arr
