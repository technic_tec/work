rects = []
evtsH = []
evtsV = []
l = raw_input().strip()
while l[0] in '0123456789':
    x1, y1, x2, y2 = rect = l.split(',')
    rects.append(rect)
    evtsV.append([x1, y1, y2, len(rects), (1, 0)])
    evtsV.append([x2, y1, y2, len(rects), (-1, 0)])
    evtsH.append([y1, x1, x2, len(rects), (0, 1)])
    evtsH.append([y2, x1, x2, len(rects), (0, -1)])
    l = raw_input().strip()
ops = l.split(',')
evtsH.sort()
evtsV.sort()

itrsH = []
i = 0
while i < len(evtsH):
    j = i
    while(j+1 < len(evtsH) and evtsH[j+1][0] == evtsH[j][0]):
        j += 1
    itrsH.append(IntervalTree(evtsH[i:j+1]))
i = 0
while i < len(evtsV):
    j = i
    while(j+1 < len(evtsV) and evtsV[j+1][0] == evtsV[j][0]):
        j += 1
    itrsH.append(IntervalTree(evtsV[i:j+1]))

inrect = [False] * len(rects)
x, y, d = 0, 0, (0, 1)
tr = []
for op in ops:
    if op == 'L':
        d = (-d[1], d[0])
    elif op == 'R':
        d = (d[1], -d[0])
    else:
        assert(op[0] in 'FB')
        steps = int(op[1:])
        if op[0] == 'F':
            dx = d
        else:
            dx = (-d[0], -d[1])
        while steps > 0:
