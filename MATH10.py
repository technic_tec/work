from fractions import Fraction

def f(q, p):
    s = []
    x = Fraction(q, p);
    while x.numerator > 1:
        y = long(1/x)+1
        s.append(y)
        x -= Fraction(1, y)
    s.append(x.denominator)
    print sum(s)
    #print s, sum(Fraction(1, i) for i in s)

f(231, 431)
f(1234, 4321)
