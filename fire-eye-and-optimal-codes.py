from heapq import heappush, heappop, heappushpop, heapreplace, nsmallest

#f(n) = min(f(i) + f(n-i) + (a-b)*i+b*n)
def f(a, b, n):
    c = [(0, 0), (0, 1)]
    for k in range(2, n+1):
        c.append(min([(c[i][0]+c[k-i][0]+(a-b)*i+b*k, k-i) for i in range(1, k)]))
    return c[n][0]

# countxy(k) = |{(x, y) | ax+by <= k, x >= 0, y >= 0}|
#            = sum([(k-a*x)/b], x, 0, [k/a])
def countxy(a, b, k, xl = 0, xh = None):
    # sum([(k-ax)/b], x, 0, [k/a])
    # = sum([(k-by)/a], y, 0, [k/b])
    # = sum(([(k%b+[k/b]*(b%a)-b%a*y)/a], y, 0, [k/b]) + sum(([k/b]-y)*[b/a],  y,  0,  [k/b])
    assert(a > 0 and b > 0 and k >= 0)
    if xh is None:
        xh = k/a
    cnt = 0
    while a > 0:
        assert(a*xh <= k)
        if xl > 0:
            # sum([(k-ax)/b], x, xl, xh) = sum([(k-a(x+xl))/b], x, 0, xh-xl)
            k -= a*xl
            xl, xh = 0, xh-xl
        elif (k-a*xh)/b > 0:
            c = (k-a*xh)/b
            # sum([(k-ax)/b], x, xl, xh) = sum([(k-b*c-ax)/b], x, xl, xh) + sum(c, x, xl, xh)
            cnt += c*(xh-xl+1)
            k -= b*c
        elif a < b:
            # sum([(k-ax)/b], x, 0, xh) (0<=(k-a*xh)/b<1) = sum([(k-bx)/a, x, 1, [k/b]) + xh+1
            cnt += xh+1
            a, b, xl, xh = b, a, 1, k/b
        else:  # xl == 0 && 0<=(k-a*xh)/b<1 && a >= b
            # sum([(k-ax)/b], x, xl, xh) = sum([(k-ax)/b] + [a/b]*(x-xh), x, xl, xh) - sum([a/b]*(x-xh), x, xl, xh)
            #                            = sum([(k-(a-a%b)*xh - (a%b)*x)/b], x, xl, xh) - sum([a/b]*(x-xh), x, xl, xh)
            cnt += (a/b)*(xh-xl+1)*(xh-xl)/2
            a, k = a%b, k-(a-a%b)*xh
    cnt += (k/b+1)*(xh-xl+1)
    return cnt

def c(a, b, n):
    lo, hi = 0, a*n+b
    while hi-lo > 1:
        k = (hi+lo)/2
        if countxy(a, b, k) < n-1:
            lo = k
        else:
            hi = k
    print lo, countxy(a, b, lo), countxy(a, b, lo-a), countxy(a, b, lo-b)

    # internal nodes includes all a*x+b*y<=lo, plus some of x*x+b*y==hi

print f(11, 100, 300)
print c(11, 100, 300)
print c(33248400, 1110001500, 918127337978L)
