p::(Integral a) => a
p = 1000000007

gcdl::(Integral a) => a->([a]->[a])
gcdl x l = _gcdl x (reverse l) [1]
  where _gcdl x [] r = r
        _gcdl x (c:cs) (d0:l0) = _gcdl (div x d) cs (d0*d:((div c d):l0))
		  where d = gcd x c

gcdll::(Integral a) => [a]->([a]->a)
gcdll x y = _gcdll x y 1
  where _gcdll [] y r = r
        _gcdll (c:cs) y r = _gcdll cs yr (mod (r*d) p)
		  where (d:yr) = gcdl c y

main::IO()
main = do
	m <- getLine
	l <- getLine
	a <- return (map read (words l))
	n <- getLine
	l <- getLine
	b <- return (map read (words l))
	putStrLn(show (gcdll a b))
