/*
 * =====================================================================================
 *
 *       Filename:  CODR.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年04月12日 13时58分33秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

char *reverse(char *s)
{
    int n = strlen(s);
    for(int i = 0, j = n-1; i < j; i++, j--) {
        char c = s[i]; s[i] = s[j]; s[j] = c;
    }
    return s;
}
int main()
{
    int n;
    while(scanf("%d", &n) == 1 && n) {
        int a = n / 100;
        int b = n / 10 % 10;
        int c = n % 10;
        int x = a*b;
        int y = b*c;
        char u[10], v[10];
        sprintf(u, "%d", x%10+y%10);
        sprintf(v, "%d", x/10+y/10);
        printf("%02d%02d%s%s\n", x, y, reverse(u), reverse(v));
    }
    return 0;
}

