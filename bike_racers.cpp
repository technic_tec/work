#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <algorithm>
#define N 300
#define M 90000
#define INF 99999999

int nu, nv, adj[N], dst[M], nxt[M], ne;
int Pair_G1[N], Pair_G2[N], Dist[N], NIL;

int hk_init(int _nu, int _nv)
{
    nu = _nu; nv = _nv; ne = 0;
    for(int i = 0; i < nu; i++)
        adj[i] = -1;
    NIL = nu;
    adj[NIL] = -1;
}

int add_edge(int _s, int _t)
{
    dst[ne] = _t;
    nxt[ne] = adj[_s];
    adj[_s] = ne++;
}

int Q[N], hd, tl;
bool BFS()
{
    hd = tl = 0;
    for(int v = 0; v < nu; v++) {
        if(Pair_G1[v] == NIL) {
            Dist[v] = 0;
            Q[tl++] = v;
        } else {
            Dist[v] = INF;
        }
    }
    Dist[NIL] = INF;
    while (hd < tl) {
        int v = Q[hd++];
        if(Dist[v] < Dist[NIL])
            for(int e = adj[v]; e >= 0; e = nxt[e]) {
                int u = dst[e];
                if(Dist[ Pair_G2[u] ] == INF) {
                    //printf("BFS G1[%d]-->G2[%d]==>G1[%d]\n", v, u, Pair_G2[u]);
                    Dist[ Pair_G2[u] ] = Dist[v] + 1;
                    Q[tl++] = Pair_G2[u];
                }
            }
    }
    return (Dist[NIL] != INF);
}

bool DFS(int v)
{
    if(v != NIL) {
        for(int e = adj[v]; e >= 0; e = nxt[e]) {
            int u = dst[e];
            if (Dist[ Pair_G2[u] ] == Dist[v] + 1) {
                if(DFS(Pair_G2[u])) {
                    //printf("DFS G1[%d]==>G2[%d]-->G1[%d]\n", v, u, Pair_G2[u]);
                    Pair_G2[u] = v;
                    Pair_G1[v] = u;
                    return true;
                }
            }
        }
        Dist[v] = INF;
        return false;
    }
    return true;
}

int hk_match()
{
    for(int i = 0; i < nu; i++)
        Pair_G1[i] = NIL;
    for(int i = 0; i < nv; i++)
        Pair_G2[i] = NIL;
    int matching = 0;
    while (BFS())
        for(int v = 0; v < nu; v++)
            if(Pair_G1[v] == NIL)
                if (DFS(v))
                    matching++;
    return matching;
}

int n, m, k, ax[N], ay[N], bx[N], by[N];
long long d[N][N], dv[M], nd;

int main()
{
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 0; i < n; i++)
        scanf("%d%d", &ax[i], &ay[i]);
    for(int i = 0; i < m; i++)
        scanf("%d%d", &bx[i], &by[i]);
    for(int i = 0, u = 0; i < n; i++) {
        for(int j = 0; j < m; j++) {
            long long dij = (long long)(ax[i]-bx[j])*(long long)(ax[i]-bx[j])+(long long)(ay[i]-by[j])*(long long)(ay[i]-by[j]);
            dv[u++] = d[i][j] = dij;
        }
    }
    std::sort(dv, dv+n*m);
    nd = n*m;
    int c = 0;
    for(int i = 0; i < nd; i++) {
        if(i && dv[i] == dv[i-1])
            ++c;
        else if(c)
            dv[i-c] = dv[i];
    }
    nd -= c;

    int l = 0, h = nd;
    while(h - l > 1) {
        int mid = (h+l)/2;
        hk_init(n, m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                if(d[i][j] < dv[mid])
                    add_edge(i, j);
        if(hk_match() >= k)
            h = mid;
        else
            l = mid;
    }
#if 0 // check
    do {
        hk_init(n, m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                if(d[i][j] <= dv[l]) {
                    printf("add_edge(%d, %d, %lld)\n", i, j, d[i][j]);
                    add_edge(i, j);
                }
        assert(hk_match() >= k);
        int c = 0;
        for(int i = 0; i < nu; i++)
            if(Pair_G1[i] != NIL) {
                int j = Pair_G1[i];
                printf("(%d, %d, %lld)\n", i, j, d[i][j]);
                ++c;
                assert(d[i][j] <= dv[l]);
            }
        assert(c >= k);
        hk_init(n, m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                if(d[i][j] < dv[l])
                    add_edge(i, j);
        assert(hk_match() < k);
    } while (0);
#endif
    printf("%lld\n", dv[l]);
    return 0;
}
