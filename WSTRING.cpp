/*
 * =====================================================================================
 *
 *       Filename:  WSTRING.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年06月10日 17时05分33秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 20000
#define K 26

char s[N];
int wc[N][K], lw[N], cw[N], rw[N];
int wcount(const char *s)
{
    int k = 0;
    for(int j = 0; j < K; j++)
        wc[k][j] = 0;
    for(int i = 0; s[i]; i++) {
        if(s[i] == '#') {
            ++k;
            for(int j = 0; j < K; j++)
                wc[k][j] = wc[k-1][j];
        } else 
            wc[k][s[i]-'a']++;
    }
    for(int i = 1; i < k; i++) {
        lw[i] = cw[i] = rw[i] = 0;
        for(int j = 0; j < K; j++) {
            if(wc[i-1][j] > lw[i])
                lw[i] = wc[i-1][j];
            if(wc[i][j] - wc[i-1][j] > cw[i])
                cw[i] = wc[i][j] - wc[i-1][j];
            if(wc[k][j] - wc[i][j] > rw[i])
                rw[i] = wc[k][j] - wc[i][j];
        }
    }
    int sm = 0;
    for(int i = 1; i+1 < k; i++)
        if(lw[i] && cw[i] && cw[i+1] && rw[i+1] && lw[i] + cw[i] + cw[i+1] + rw[i+1] + 3 > sm)
            sm = lw[i] + cw[i] + cw[i+1] + rw[i+1] + 3;
    return sm;
}
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%s", s);
        printf("%d\n", wcount(s));
    }
    return 0;
}

