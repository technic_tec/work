#!/usr/bin/python

from Crypto.Util.number import *
from Crypto.Hash import *
from secret import PRIMES, SECRET, FLAG

C0 = [ SHA.new(SECRET[i:] + SECRET[:i]).digest()[:16] for i in xrange(10) ]
C1 = (''.join([ SHA.new(SECRET[i:] + SECRET[:i]).hexdigest()[24:] for i in xrange(8) ])).decode('hex')

def xor_str(x, y):
    if len(x) > len(y):
        return ''.join([chr(ord(z) ^ ord(p)) for (z, p) in zip(x[:len(y)], y)])
    else:
        return ''.join([chr(ord(z) ^ ord(p)) for (z, p) in zip(x, y[:len(x)])])

def hashi(x):
    if len(x) < 16:
        x = (16 - len(x)) * chr(0) + x
    return SHA.new(xor_str(x + x, C1)).digest()[:16]

def G(x):
    a = 2 ** 127 - 1
    g = ''.join([ long_to_bytes(bytes_to_long(hashi(x + C0[0])) & a) ] + [hashi(x + C0[i]) for i in range(1, 9)] + [hashi(x + C0[9])[:8]])
    if len(g) < 152:
        g = (152 - len(g)) * chr(0) + g
    return g

def H(x):
    if len(x) < 152:
        x = (152 - len(x)) * chr(0) + x
    x = x + chr(0) * 8
    X = [x[i*16:(i+1)*16] for i in xrange(10)]
    res = hashi(X[0] + C0[0])
    for i in xrange(1, 10):
        res = xor_str(res, hashi(X[i] + C0[i]))
    if len(res) < 16:
        res = (152 - len(res)) * chr(0) + res
    return res

def genKey():
    p, q = PRIMES()
    N = p**2 * q
    a, b = (p+1)/4, (q+1)/4
    z = inverse(p, q)
    pubkey, privkey = N, (p, q, a, b, z)
    return pubkey, privkey

def onver(m, R):
    R = long_to_bytes(R)
    r = SHA.new(R).digest()[:16]
    if len(bin(m)[2:]) < 1088:
        M = (1088 - len(bin(m)[2:]))*'0' + bin(m)[2:]
    M = M + '0' * 128
    mp = long_to_bytes(int(M, 2))
    if len(mp) < 152:
        mp = (152 - len(mp)) * chr(0) + mp
    s = xor_str(mp, G(r))
    t = xor_str(r, H(s))
    return bytes_to_long(s + t)

def revno(mp):
    mp = long_to_bytes(mp)
    if len(mp) < 168:
        mp = (168 - len(mp)) * chr(0) + mp
    s, t = mp[:152], mp[-16:]
    r = xor_str(t, H(s))
    M = xor_str(s, G(r))
    m, w = M[:136], M[136:]
    return m, w

def encrypt(m, N):
    R = getRandomInteger(192)
    x = onver(m, R)
    c = pow(x, 2, N)
    return c

N, privkey = genKey()
m = bytes_to_long(FLAG)
enc = encrypt(m, N)
print 'pubkey =', N
print 'enc =', enc    