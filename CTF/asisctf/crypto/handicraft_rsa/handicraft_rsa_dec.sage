import sys
from Crypto.Util.number import *
from Crypto.PublicKey import RSA

enc = "eER0JNIcZYx/t+7lnRvv8s8zyMw8dYspZlne0MQUatQNcnDL/wnHtkAoNdCalQkpcbnZeAz4qeMX5GBmsO+BXyAKDueMA4uy3fw2k/dqFSsZFiB7I9M0oEkqUja52IMpkGDJ2eXGj9WHe4mqkniIayS42o4p9b0Qlz754qqRgkuaKzPWkZPKynULAtFXF39zm6dPI/jUA2BEo5WBoPzsCzwRmdr6QmJXTsau5BAQC5qdIkmCNq7+NLY1fjOmSEF/W+mdQvcwYPbe2zezroCiLiPNZnoABfmPbWAcASVU6M0YxvnXsh2YjkyLFf4cJSgroM3Aw4fVz3PPSsAQyCFKBA==".decode('base64')
skey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq+m7iHurBa9G8ujEiTpZ71aHOVNhQXpd6jCQNhwMN3hD6JHkv0HSxmJwfGe0EnXDtjRraWmS6OYzT4+LSrXsz9IkWGzRlJ4lC7WHS8D3NWIWYHCP4TRt2N0TlWXWm9nFCrEXqQ3IWgYQpQvKzsdsetnIZJL1tf1wQzGE6rbkbvURlUBbzBSuidkmi0kY5Qxp2Jfb6OUI647zx2dPxJpDffSCNffVIDUYOvrgYxIhs5HmCF3XECC3VfaKtRceL5JM8R0qz5nVU2Ns8hPvSVP+7/i7G447cjW151si0joB7RpBplu44Vk8TXXDAk0JZdW6KwJn7ITaX04AAAAAAAAAAQIDAQAB".decode('base64')
rsa = RSA.importKey(skey)
print 'n:', rsa.n
print 'e:', rsa.e

N, e = rsa.n, rsa.e
#B = (1<<20)
#if len(sys.argv) > 1:
#  B = int(argv[1])
#P = Primes()
#p = P.first()
#r = 2
#g = 1
#while p <= B and g in [1, N]:
#  if p > 20000:
#    print p
#  q = p
#  while q <= N:
#    q *= p
#  r = pow(r, q/p, N)
#  p = P.next(p)
#  g = gcd(r-1, N)
#print ('N' if g == N else g)
p = 139457081371053313087662621808811891689477698775602541222732432884929677435971504758581219546068100871560676389156360422970589688848020499752936702307974617390996217688749392344211044595211963580524376876607487048719085184308509979502505202804812382023512342185380439620200563119485952705668730322944000000001
q = N/p
d = inverse(e, (p-1)*(q-1))
key = RSA.construct((long(N), long(e), long(d), long(p), long(q)))
for _ in range(20):
  enc = key.decrypt(enc)
print enc
