#!/usr/bin/python

import random

KEY = 'musZTXmxV58UdwiKt8Tp'

def xor_str(x, y):
    if len(x) > len(y):
        return ''.join([chr(ord(z) ^ ord(p)) for (z, p) in zip(x[:len(y)], y)])
    else:
        return ''.join([chr(ord(z) ^ ord(p)) for (z, p) in zip(x, y[:len(x)])])

ef = open('flag.enc', 'r')
ENC = ef.read()
ef.close()

enc, key = ENC, KEY.encode('hex')
flag = xor_str(key * (len(enc) // len(key) + 1), enc).decode('hex')

ef = open('flag.png', 'wb')
ef.write(flag)
ef.close()


