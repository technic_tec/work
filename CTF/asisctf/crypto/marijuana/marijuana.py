#!/usr/bin/python

import random, math
from operator import mul
from fractions import Fraction
from Crypto.Util.number import *
from flag import FLAG

def n_Choose_k(n, k):
    return int(reduce(mul, (Fraction(n-i, i+1) for i in range(k)), 1) )

def hamweight(h, n):
    if h <= n:
        H = []
        while True:
            r = random.randint(0, n-1)
            if 2**r not in H:
                H.append(2**r)
                if len(H) == h:
                    break
        return sum(H)

def ham(n):
    nbin = bin(n)[2:].replace('0', '')
    return len(nbin)

def keygen(l):
    n = 2 * random.randint(4*l, 5*l) + 1
    p = (1 << n) - 1
    while True:
        h = random.randint(1, int(math.sqrt(n) / 2) - 1)
        if n_Choose_k(n-1, h-1) >= 2**l:
            break
    F, G = hamweight(h, n), hamweight(h, n)
    pk = (F * inverse(G, p)) % p
    sk = G
    return n, p, h, pk, sk

def encrypt(msg, h, pk):
    msg = bin(bytes_to_long(msg))[2:]
    enc = []
    for b in msg:
        A, B = hamweight(h, n), hamweight(h, n)
        enc.append(int(((-1)**int(b) * (A * pk + B)) % p))
    return enc

n, p, h, pk, sk = keygen(32)
f = open('FLAG.enc', 'w')
E = encrypt(FLAG, h, pk)
f.write('\n'.join([str(d) for d in E]))
f.close()