from hashlib import sha1
v='69fc8b9b1cdfe47e6b51a6804fc1dbddba1ea1d9'
hexchar='0123456789abcdefABCDEFx '
vischar=''.join(map(chr, range(33, 127)))
selfhash = []
for c0 in hexchar:
  for c1 in hexchar:
    for c2 in hexchar:
      for c3 in hexchar:
        w = c0+c1+c2+c3
        if sha1(w).hexdigest().strip().lower()[:4] == w:
          selfhash.append(w)
print selfhash
for pre in selfhash:
  for suf in selfhash:
    if pre < suf:
      for c0 in vischar:
        for c1 in vischar:
          for c2 in vischar:
            for c3 in vischar:
              w = pre + c0+c1+c2+c3 + suf
              #print w[3:-3]
              if sha1(w[3:-3]).hexdigest().strip().lower() == v:
                print w
                print 'ASIS{%s}' % sha1(w).hexdigest()
                break
