from pwn import *
from struct import unpack, pack

p = remote('vortex.labs.overthewire.org', 5842)
s = p.recvn(16)
o = pack('<L', sum(unpack('<LLLL', s))&0xffffffff)
p.send(o)
print p.recvall()
p.close()
