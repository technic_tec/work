/*
 * =====================================================================================
 *
 *       Filename:  malloc.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2017年04月07日 10时09分51秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 128

int main()
{
  unsigned *p = (unsigned *)malloc(N);
  unsigned *r = (unsigned *)malloc(N);
  unsigned *q = (unsigned *)malloc(N);
  unsigned *ptr[] = {NULL, q, r, p};
  printf("[%p] %p %p %p %p\n", ptr, ptr[0], ptr[1], ptr[2], ptr[3]);
  p[0] = N+(r-p)*4;
  p[1] = N+(r-p)*4;
  p[2] = ptr;
  p[3] = ptr+1;
  r[N/4] = N+(r-p)*4;
  r[N/4+1] = 0x50;
  r[N/4+21] = r[N/4+25] = 0x11;
  free(q);
  printf("[%p] %p %p %p %p\n", ptr, ptr[0], ptr[1], ptr[2], ptr[3]);
  free(r);
  exit(0);
  free(p);
}
