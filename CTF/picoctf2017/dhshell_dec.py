#!/usr/bin/python2 -u
from hashlib import sha256
from Crypto import Random
from Crypto.Random import random
from Crypto.Cipher import AES
from subprocess import check_output, STDOUT, CalledProcessError
from itertools import izip
import sys

BLOCK_SIZE = 16
R = Random.new()

def pad(m):
    o = BLOCK_SIZE - len(m) % BLOCK_SIZE
    return m + o * chr(o)

def unpad(p):
    return p[0:-ord(p[-1])]

def send_encrypted(KEY, m):
    IV = R.read(BLOCK_SIZE)
    aes = AES.new(KEY, AES.MODE_CBC, IV)
    c = aes.encrypt(pad(m))
    return (IV + c).encode('hex')

def read_encrypted(KEY, data):
    data = data.decode('hex')
    IV, data = data[:BLOCK_SIZE], data[BLOCK_SIZE:]
    aes = AES.new(KEY, AES.MODE_CBC, IV)
    m = unpad(aes.decrypt(data))
    return m

def serve_commands(KEY):
    while True:
        cmd = read_encrypted(KEY)
        try:
            output = check_output(cmd, shell=True, stderr=STDOUT)
        except CalledProcessError as e:
            output = str(e) + "\n"
        send_encrypted(KEY, output)

pw = 'ThisIsMySecurePasswordPleaseGiveMeAShell\n'
#pwenc = '93dc028122341e84d2c6da68877a709d76ebd64cb13b2e1df5c69d798cb38f3014fcc821ccb1fb9ccf49ce23877f1b3d6f7db349b0c0e5d421a3b98ab591dd9a'
#req1='04c33e306b27ffcac9866fa520d844d39bacb33fdd561f5f658d13053889f4a4469934e23d5c3934f3daaa48178b0cf4'
#res1='69542b9d9808a550c6b5044f8b8ef975c7e47365c7733f149535e919d19db55f7b0f318b5d19b958f9981479bb203ea7'
#req2='380a68df8b23563c33be4318a6692a9e490400ac0dbc616daefec956b8de48ed'
#res2='86ab5ee463129ada4e7b80fb2cbf1a7d218d93ae4aafe0824cf64498b96be4df'
K = 151241387009817985757580001743366712399069066910261315547890638293652730283322654189331135554447992269577665992424199796812365261990780256372642179373794916833078706649652195058773604863911281130574158334843459962312399275163278864380075672974080865211441383331273644770012519087045723872076082949848516353025L
#K = long(sys.argv[1])
KEY = sha256(str(K)).digest()
pwenc = send_encrypted(KEY, pw)

#print repr(read_encrypted(KEY, pwenc))
print 'pw = {}'.format(repr(pw))
print 'pwenc = {}'.format(pwenc)
while True:
  try:
    s = raw_input().lower().split(None, 1)
  except EOFError:
    break
  if s[0] == 'enc':
    print 'req = {}'.format(send_encrypted(KEY, s[1]))
  elif s[0] == 'dec':
    print 'res = {}'.format(repr(read_encrypted(KEY, s[1])))
  else:
    raise ValueError("Invalid command: %s %s" % (s[0], s[1]))
