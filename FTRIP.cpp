#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

double C(int n, int m)
{
    if(m < 0 || n < m)
        return 0.0;
    else if(m > n-m)
        m = n-m;
    double r = 1.0;
    for(int i = n, j = 1; j <= m; i--, j++)
        r *= (double)i/(double)j;
    return r;
}
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        int s, n, m, k;
        scanf("%d%d%d%d", &s, &n, &m, &k);
        // sum(C(s-m, n-1-i)*C(m-1, i)/C(s-1, n-1))
        // s-m >= n-1-i >= 0 && m-1 >= i >= 0 && i >= k => MAX(k, n+m-s-1)<=i<=MIN(m-1, n-1)
        int i = (k>=n+m-s-1?k:(n+m-s-1));
        double c1 = C(s-m, n-1-i);
        double c2 = C(m-1, i);
        double r = c1*c2;
        for(++i; i <= m-1 && i <= n-1; ++i) {
            c1 *= (double)(s-n-m+1+i)/double(n-i);
            c2 *= (double)i/(double)(m-i);
            r += c1*c2;
        }
        r /= C(s-1, n-1);
        printf("%.6lf\n", r);
    }
    return 0;
}

