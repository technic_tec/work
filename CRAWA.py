# (-2k,-2k) -- (2k+1,-2k) -- (2k+1,2k+2) -- (-2k-2,2k+2) -- (-2k-2,-2k-2), k = 0, 1, 2, ...

def on_trace(x,y):
    if x > 0 and (x & 1):
        k = (x-1)/2
        if y >= -2*k and y <= 2*k+2:
            return True
    elif x < 0 and (x & 1) == 0:
        k = -(x+2)/2
        if y >= -2*k-2 and y <= 2*k+2:
            return True
    if y > 0 and (y & 1) == 0:
        k = (y-2)/2
        if x >= -2*k-2 and x <= 2*k+1:
            return True
    elif y <= 0 and (y & 1) == 0:
        k = -y/2
        if x >= -2*k and x <= 2*k+1:
            return True
    return False

t = int(raw_input())
for _ in range(t):
    x,y = map(int,raw_input().split())
    if on_trace(x,y):
        print 'YES'
    else:
        print 'NO'

