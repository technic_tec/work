// Sample input 4, in CPP.

#include <cassert>

int NumberOfPeaks() {
  return 10;
}

int GetHeight(long long i) {
  switch ((int)i) {
    case 0: return 1;
    case 1: return 3;
    case 2: return 4;
    case 3: return 2;
    case 4: return 5;
    case 5: return 7;
    case 6: return 6;
    case 7: return 9;
    case 8: return 8;
    case 9: return 3;
    default: assert(0);
  }
}
