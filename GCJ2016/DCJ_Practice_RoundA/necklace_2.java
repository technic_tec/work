// Sample input 2, in Java.
public class necklace {
  public necklace() {
  }

  public static long GetNecklaceLength() {
    return 3L;
  }

  public static long GetMessageLength() {
    return 3L;
  }

  public static int GetNecklaceElement(long index) {
    switch ((int)index) {
      case 0: return 0;
      case 1: return 1;
      case 2: return 2;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }

  public static int GetMessageElement(long index) {
    switch ((int)index) {
      case 0: return 0;
      case 1: return 3;
      case 2: return 2;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}