// Sample input 1, in Java.
public class shipping {
  public shipping() {
  }

  public static long NumberOfVillages() {
    return 3L;
  }

  public static int VillageFaction(long index) {
    switch ((int)index) {
      case 0: return 2;
      case 1: return 2;
      case 2: return 2;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }

  public static long VillageImmediatelyDownstream(long index) {
    switch ((int)index) {
      case 0: return 0L;
      case 1: return 0L;
      case 2: return 0L;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }

  public static int NumberOfShipments() {
    return 3;
  }

  public static int GetShipmentSource(long index) {
    switch ((int)index) {
      case 0: return 0;
      case 1: return 0;
      case 2: return 1;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }

  public static int GetShipmentDestination(long index) {
    switch ((int)index) {
      case 0: return 1;
      case 1: return 2;
      case 2: return 2;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}