// Sample input 1, in Java.
public class highest_mountain {
  public highest_mountain() {
  }

  public static int NumberOfPeaks() {
    return 4;
  }

  public static int GetHeight(long i) {
    switch ((int)i) {
      case 0: return 3;
      case 1: return 1;
      case 2: return 2;
      case 3: return 4;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}