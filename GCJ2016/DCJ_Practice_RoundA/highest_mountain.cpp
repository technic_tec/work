#include <cstdio>
#include <cassert>
#include <algorithm>
#include <vector>
#include "message.h"
#include "highest_mountain.h"
using namespace std;
#if 1
#define RECEIVE(x...) Receive(x)
#define SEND(x...) Send(x)
#define PUTChar(x...) PutChar(x)
#define PUTInt(x...) PutInt(x)
#define PUTLL(x...) PutLL(x)
#define GETChar(x...) GetChar(x)
#define GETInt(x...) GetInt(x)
#define GETLL(x...) GetLL(x)
#else
void SEND(int target) {
  fprintf(stderr, "%s(%d)\n", __FUNCTION__, target);
  Send(target);
}
int RECEIVE(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int src = Receive(source);
  fprintf(stderr, "->%d)\n", src);
  return src;
}
void PUTChar(int target, char value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutChar(target, value);
}
void PUTInt(int target, int value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutInt(target, value);
}
void PUTLL(int target, long long value) {
  fprintf(stderr, "%s(%d, %lld)\n", __FUNCTION__, target, value);
  PutLL(target, value);
}
char GETChar(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  char x = GetChar(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
int GETInt(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int x = GetInt(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
long long GETLL(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  long long x = GetLL(source);
  fprintf(stderr, ", %lld)\n", x);
  return x;
}
#endif

class ConvexHull {
  public:
    typedef struct {
      int node, sz, acc;
    } Size;
    void add(int x, int y) {
      m_h.push_back(make_pair(x, y));
    }
    void build() {
      vector<pair<int, int> > hull;
      for(int i = 0; i < m_h.size(); i++) {
        while(hull.size() >= 2 && cross(hull[hull.size()-2], hull[hull.size()-1], m_h[i]) > 0)
          hull.pop_back();
        hull.push_back(m_h[i]);
      }
      m_h.swap(hull);
      Size sz = {MyNodeId(), (int)m_h.size(), (int)m_h.size()};
      m_sz.clear();
      m_sz.push_back(sz);
      ht_off = 0;
      ht_ed = m_h.size()-1;
    }
    void merge(int target) {
      RECEIVE(target);
      int cnt = GETInt(target);
      int spl = GETInt(target);
      vector<Size> sz2(cnt);
      for(int i = 0; i < cnt; i++) {
        sz2[i].node = GETInt(target);
        sz2[i].sz = GETInt(target);
        sz2[i].acc = (i==0 ? 0 : sz2[i-1].acc) + sz2[i].sz;
      }
      int ul = 0, ur = m_sz.back().acc-1;
      int vl = 0, vr = sz2.back().acc-1;
      while(ul < ur || vl < vr) {
        int u = (ul+ur)/2, v = (vl+vr)/2;
        pair<int, int> pu, pul, pur, pv, pvl, pvr;
        query(u, m_sz, pu, pul, pur);
        query(v, sz2, pv, pvl, pvr);
        int tu = intersect_type(pu, pul, pur, pv);
        int tv = intersect_type(pv, pvl, pvr, pu);
        if(tu == 0 && tv == 0) {
          ul = ur = u;
          vl = vr = v;
        } else if(tu == 0) {
          ur = u;
          if(tv > 0) vr = v-1; // ul..u, vl..v-1
          else vl = v+1;       // ul..u, v+1..vr
        } else if(tv == 0) {
          vl = v;
          if(tu > 0) ur = u-1; // ul..u-1, v..vr
          else ul = u+1;       // u+1..ur, v..vr
        } else if(tu > 0) {
          if(tv < 0) {
            ur = u-1; vl = v+1; // ul..u-1, v+1..vr
          } else {
            ur = u-1;         // ul..u-1, vl..vr
          }
        } else {
          if(tv < 0) {
            vl = v+1;         // ul..ur, v+1..vr
          } else {
            // (pu--pur,  pv--pvl)
            double sx = (((double)pv.first - pu.first)*(double)(pvl.second-pv.second) - (double)(pv.second - pu.second)*(double)(pvl.first-pv.first));
            sx /= ((double)(pur.first-pu.first)*(double)(pvl.second-pv.second) - (double)(pur.second-pu.second)*(double)(pvl.first-pv.first));
            sx = (double)pu.first + sx*(double)(pur.first-pu.first);
            if(sx < spl - 1e-2)
              ul = u+1;       // u+1..ur, vl..vr
            if(sx > spl-1 + 1e-2)
              vr = v-1;       // ul..ur, vl..v-1
          }
        }
      }
      pair<int, int> pu, pul, pur, pv, pvl, pvr;
      int i = query(ul, m_sz, pu, pul, pur);
      int j = query(vr, sz2, pv, pvl, pvr);
      int ret = cut_half(m_sz[i], pu, pv, 2); // cut right
      if(ret < 0) {
        int lo = i, hi = m_sz.size();
        while(hi-lo > 1) {
          i = (hi+lo)/2;
          ret = cut_half(m_sz[i], pu, pv, 2); // cut right
          if(ret < 0)
            lo = i;
          else if(ret > 0)
            hi = i;
          else {
            lo = hi = i;
            break;
          }
        }
        i = lo;
      } else {
        assert(ret == 0);
      }
      m_sz.resize(i+1);

      ret = cut_half(sz2[j], pu, pv, 1); // cut left
      if(ret > 0) {
        int lo = -1, hi = j;
        while(hi-lo > 1) {
          j = (hi+lo)/2;
          ret = cut_half(sz2[j], pu, pv, 1); // cut left
          if(ret < 0)
            lo = j;
          else if(ret > 0)
            hi = j;
          else {
            lo = hi = i;
            break;
          }
        }
        j = hi;
      } else {
        assert(ret == 0);
      }
      for(; j < sz2.size(); j++) {
        sz2[j].acc = m_sz.back().acc + sz2[j].sz;
        m_sz.push_back(sz2[j]);
      }
    }
    void serve(int src) {
      PUTInt(src, m_sz.size());
      PUTInt(src, m_h.front().first);
      for(int i = 0; i < m_sz.size(); i++) {
        PUTInt(src, m_sz[i].node);
        PUTInt(src, m_sz[i].sz);
      }
      SEND(src);
      while(1) {
        src = RECEIVE(-1);
        char op = GETChar(src);
        if(op == -1)       // Exit
          break;
        else if(op == 0) {
          int x = GETInt(src);  // Query vertex x
          PUTInt(src, (x>0 ? m_h[ht_off+x-1].first : -1));
          PUTInt(src, (x>0 ? m_h[ht_off+x-1].second : -1));
          PUTInt(src, m_h[ht_off+x].first);
          PUTInt(src, m_h[ht_off+x].second);
          PUTInt(src, x < ht_ed-ht_off ? m_h[ht_off+x+1].first : -1);
          PUTInt(src, x < ht_ed-ht_off ? m_h[ht_off+x+1].second : -1);
          SEND(src);
        } else if(op == 1 || op == 2) { // cut
          int ux = GETInt(src);
          int uy = GETInt(src);
          int vx = GETInt(src);
          int vy = GETInt(src);
          int ret;
          if(op == 1) { // cut left
            ret = do_cut_left(ux, uy, vx, vy);
          } else { // cut right
            ret = do_cut_right(ux, uy, vx, vy);
          }
          PUTInt(src, ret);
          if(ret == 0)
            PUTInt(src, ht_ed-ht_off+1);
          SEND(src);
        } else
          assert(0 && "Invalid command");
      }
    }
    int do_cut_left(int ux, int uy, int vx, int vy) {
      if(colinear(m_h[ht_off], ux, uy, vx, vy)) {
        return 1;
      } else if(!colinear(m_h[ht_ed], ux, uy, vx, vy)) {
        if(ht_ed-ht_off <= 1 || cross2(vx-ux,vy-uy,m_h[ht_ed].first-m_h[ht_ed-1].first,m_h[ht_ed].second-m_h[ht_ed-1].second) > 0)
          return -1;
        int lo = ht_off, hi = ht_ed-1;
        while(hi - lo > 1) {
          int mid = (lo+hi)/2;
          if(cross2(vx-ux,vy-uy,m_h[mid+1].first-m_h[mid].first,m_h[mid+1].second-m_h[mid].second) > 0)
            lo = mid;
          else
            hi = mid;
        }
        ht_off = hi;
        return 0;
      } else {
        int lo = ht_off, hi = ht_ed;
        while(hi - lo > 1) {
          int mid = (lo+hi)/2;
          if(colinear(m_h[mid], ux, uy, vx, vy))
            hi = mid;
          else
            lo = mid;
        }
        ht_off = hi;
        return 0;
      }
    }
    int do_cut_right(int ux, int uy, int vx, int vy) {
      if(colinear(m_h[ht_ed], ux, uy, vx, vy)) {
        return -1;
      } else if(!colinear(m_h[ht_off], ux, uy, vx, vy)) {
        if(ht_ed-ht_off <= 1 || cross2(vx-ux,vy-uy,m_h[ht_off+1].first-m_h[ht_off].first,m_h[ht_off+1].second-m_h[ht_off].second) < 0)
          return 1;
        int lo = ht_off, hi = ht_ed-1;
        while(hi - lo > 1) {
          int mid = (lo+hi)/2;
          if(cross2(vx-ux,vy-uy,m_h[mid+1].first-m_h[mid].first,m_h[mid+1].second-m_h[mid].second) >= 0)
            lo = mid;
          else
            hi = mid;
        }
        ht_off = lo;
        return 0;
      } else {
        int lo = ht_off, hi = ht_ed;
        while(hi - lo > 1) {
          int mid = (lo+hi)/2;
          if(colinear(m_h[mid], ux, uy, vx, vy))
            lo = mid;
          else
            hi = mid;
        }
        ht_ed = lo;
        return 0;
      }
    }
    inline int size() const {
      return m_sz.back().acc;
    }
  private:
    inline long long cross(pair<int, int> a, pair<int, int> b, pair<int, int> c) {
      return (long long)(b.first-a.first)*(long long)(c.second-b.second)-(long long)(b.second-a.second)*(long long)(c.first-b.first);
    }
    inline long long cross2(int ax, int ay, int bx, int by) {
      return (long long)ax*(long long)by-(long long)ay*(long long)bx;
    }
    void query_single(int target, int pos, pair<int, int> &pu, pair<int, int> &pul, pair<int, int> &pur) {
      if(target != MyNodeId()) {
        PUTChar(target, 0);
        PUTInt(target, pos);
        SEND(target);
        RECEIVE(target);
        pul.first = GETInt(target);
        pul.second = GETInt(target);
        pu.first = GETInt(target);
        pu.second = GETInt(target);
        pur.first = GETInt(target);
        pur.second = GETInt(target);
      } else {
        pul.first = (pos>0 ? m_h[ht_off+pos-1].first : -1);
        pul.second = (pos>0 ? m_h[ht_off+pos-1].second : -1);
        pu.first = (m_h[ht_off+pos].first);
        pu.second = (m_h[ht_off+pos].second);
        pur.first = (pos < ht_ed-ht_off ? m_h[ht_off+pos+1].first : -1);
        pur.second = (pos < ht_ed-ht_off ? m_h[ht_off+pos+1].second : -1);
      }
    }
    int query(int u, const vector<Size> &sz, pair<int, int> &pu, pair<int, int> &pul, pair<int, int> &pur) {
      int lo = 0, hi = sz.size()-1;
      while(hi > lo) {
        int mid = (lo + hi)/2;
        if(u >= sz[mid].acc)
          lo = mid+1;
        else if(u < sz[mid].acc-sz[mid].sz)
          hi = mid-1;
        else {
          hi = lo = mid;
        }
      }
      int pos = u - (sz[lo].acc-sz[lo].sz);
      query_single(sz[lo].node, pos, pu, pul, pur);
      if(pos == 0) {
        pair<int, int> tmp;
        if(lo > 0)
          query_single(sz[lo-1].node, sz[lo-1].sz-1, pul, tmp, tmp);
        else {
          pul.first = pu.first;
          pul.second = -10;
        }
      }
      if(pos == sz[lo].sz-1) {
        pair<int, int> tmp;
        if(lo < sz.size()-1)
          query_single(sz[lo+1].node, 0, pur, tmp, tmp);
        else {
          pur.first = pu.first;
          pur.second = -10;
        }
      }
      return lo;
    }
    int intersect_type(pair<int, int> pu, pair<int, int> pul, pair<int, int> pur, pair<int, int> pv) const {
      int dx = pv.first-pu.first, dy = pv.second-pu.second;
      int ix = pu.first-pul.first, iy = pu.second-pul.second;
      int ox = pur.first-pu.first, oy = pur.second-pu.second;
      if(dx < 0) {
        dx = -dx; dy = -dy;
      }
      if((long long)ix*(long long)dy-(long long)dx*(long long)iy > 0) {  // oy/ox <= iy/ix < dy/dx
        return 1;
      } else if((long long)dx*(long long)oy-(long long)ox*(long long)dy > 0) { // dy/dx < oy/ox <= iy/ix
        return -1;
      } else { // oy/ox <= dy/dx <= iy/ix
        return 0;
      }
    }
    int cut_half(Size &psz, pair<int, int> u, pair<int, int> v, int dir) {
      int res;
      if(psz.node != MyNodeId()) {
        PUTChar(psz.node, (char)dir);
        PUTInt(psz.node, u.first);
        PUTInt(psz.node, u.second);
        PUTInt(psz.node, v.first);
        PUTInt(psz.node, v.second);
        SEND(psz.node);
        RECEIVE(psz.node);
        res = GETInt(psz.node);
        if(res == 0) {
          int inc = GETInt(psz.node) - psz.sz;
          psz.sz += inc;
          psz.acc += inc;
        }
      } else {
        int ux = u.first, uy = u.second;
        int vx = v.first, vy = v.second;
        if(dir == 1) { // cut left
          res = do_cut_left(ux, uy, vx, vy);
        } else { // cut right
          res = do_cut_right(ux, uy, vx, vy);
        }
        if(res == 0) {
          int inc = ht_ed-ht_off+1 - psz.sz;
          psz.sz += inc;
          psz.acc += inc;
        }
      }
      return res;
    }
    bool colinear(pair<int, int> p, int ux, int uy, int vx, int vy) {
      int px = p.first, py = p.second;
      return ((long long)(ux-px)*(long long)(vy-py)==(long long)(vx-px)*(long long)(uy-py));
    }

    int ht_off, ht_ed;
    vector<Size> m_sz;
    vector<pair<int, int> > m_h;
};
int main()
{
  int n = NumberOfPeaks();
  int nNodes = min(NumberOfNodes(), n);
  if(MyNodeId() >= nNodes)
    return 0;
  int rem = n%nNodes;
  int blksz = n/nNodes + (MyNodeId()<rem);
  int st = (MyNodeId()<rem ? MyNodeId()*blksz : MyNodeId()*blksz+rem);

  ConvexHull hull;
  for(int i = st; i < st+blksz; i++) {
    hull.add(i, GetHeight(i));
  }
  hull.build();
  for(int blk = 1; (MyNodeId() & blk)==0 && (MyNodeId() ^ blk) < nNodes; blk <<= 1) {
    // merge master id <- (id^blk)
    hull.merge(MyNodeId() ^ blk);
  }
  if(MyNodeId() > 0) {
    // merge slave  id -> (id^blk)
    hull.serve(MyNodeId() & (MyNodeId()-1));
  } else {
    // output
    printf("%d\n", hull.size());
    for(int i = 1; i < nNodes; i++) {
      PUTChar(i, -1);
      SEND(i);
    }
  }
  return 0;
}
