// Sample input 2, in Java.
public class load_balance {
  public load_balance() {
  }

  public static int GetN() {
    return 4;
  }

  public static long GetWeight(long i) {
    switch ((int)i) {
      case 0: return 4L;
      case 1: return 1L;
      case 2: return 6L;
      case 3: return 5L;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}