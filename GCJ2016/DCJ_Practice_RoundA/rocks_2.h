// Sample input 2, in CPP.

#include <cassert>

int GetN() {
  return 4;
}

int GetK() {
  return 4;
}

bool IsRock(long long x, long long y) {
  if (x == 0 && y == 0) return false;
  if (x == 0 && y == 1) return false;
  if (x == 0 && y == 2) return false;
  if (x == 0 && y == 3) return false;
  if (x == 1 && y == 0) return false;
  if (x == 1 && y == 1) return false;
  if (x == 1 && y == 2) return false;
  if (x == 1 && y == 3) return false;
  if (x == 2 && y == 0) return false;
  if (x == 2 && y == 1) return false;
  if (x == 2 && y == 2) return false;
  if (x == 2 && y == 3) return true;
  if (x == 3 && y == 0) return false;
  if (x == 3 && y == 1) return false;
  if (x == 3 && y == 2) return true;
  if (x == 3 && y == 3) return false;
  assert(0);
}
