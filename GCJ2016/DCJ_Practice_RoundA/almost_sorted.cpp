#include <cstdio>
#include <cassert>
#include <algorithm>
#include <vector>
#include "message.h"
#include "almost_sorted.h"
using namespace std;
#if 1
#define RECEIVE(x...) Receive(x)
#define SEND(x...) Send(x)
#define PUTChar(x...) PutChar(x)
#define PUTInt(x...) PutInt(x)
#define PUTLL(x...) PutLL(x)
#define GETChar(x...) GetChar(x)
#define GETInt(x...) GetInt(x)
#define GETLL(x...) GetLL(x)
#else
void SEND(int target) {
  fprintf(stderr, "%s(%d)\n", __FUNCTION__, target);
  Send(target);
}
int RECEIVE(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int src = Receive(source);
  fprintf(stderr, "->%d)\n", src);
  return src;
}
void PUTChar(int target, char value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutChar(target, value);
}
void PUTInt(int target, int value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutInt(target, value);
}
void PUTLL(int target, long long value) {
  fprintf(stderr, "%s(%d, %lld)\n", __FUNCTION__, target, value);
  PutLL(target, value);
}
char GETChar(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  char x = GetChar(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
int GETInt(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int x = GetInt(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
long long GETLL(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  long long x = GetLL(source);
  fprintf(stderr, ", %lld)\n", x);
  return x;
}
#endif

int main()
{
  int n = NumberOfFiles();
  int k = MaxDistance();
  int nNodes = min(NumberOfNodes(), n);
  if(MyNodeId() >= nNodes)
    return 0;
  int rem = n%nNodes;
  int blksz = n/nNodes + (MyNodeId()<rem);
  int st = MyNodeId()*blksz+(MyNodeId()<rem ? 0 : rem);

  vector<long long> v(blksz+2*k);
  int il = max(-k,-st), ir = min(blksz+k, n-st);
  for(int i = il; i < ir; i++)
    v[i+k] = Identifier(st+i);
  sort(v.begin()+il+k, v.begin()+ir+k);
  long long s = 0;
  for(int i = 0; i < blksz; i++)
    s += (st+i)*v[k+i];
  if(MyNodeId() > 0) {
    PUTLL(0,s);
    SEND(0);
  } else {
    for(int i = 0; i < nNodes-1; i++) {
      int src = RECEIVE(-1);
      s += GETLL(src);
    }
    printf("%lld\n", s & 0xfffff);
  }
  return 0;
}
