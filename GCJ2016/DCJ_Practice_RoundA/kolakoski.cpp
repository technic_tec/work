#include <cstdio>
#include <cassert>
#include <algorithm>
#include <vector>
#include "message.h"
#include "kolakoski.h"
using namespace std;
#if 1
#define RECEIVE(x...) Receive(x)
#define SEND(x...) Send(x)
#define PUTChar(x...) PutChar(x)
#define PUTInt(x...) PutInt(x)
#define PUTLL(x...) PutLL(x)
#define GETChar(x...) GetChar(x)
#define GETInt(x...) GetInt(x)
#define GETLL(x...) GetLL(x)
#else
void SEND(int target) {
  fprintf(stderr, "%s(%d)\n", __FUNCTION__, target);
  Send(target);
}
int RECEIVE(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int src = Receive(source);
  fprintf(stderr, "->%d)\n", src);
  return src;
}
void PUTChar(int target, char value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutChar(target, value);
}
void PUTInt(int target, int value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutInt(target, value);
}
void PUTLL(int target, long long value) {
  fprintf(stderr, "%s(%d, %lld)\n", __FUNCTION__, target, value);
  PutLL(target, value);
}
char GETChar(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  char x = GetChar(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
int GETInt(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int x = GetInt(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
long long GETLL(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  long long x = GetLL(source);
  fprintf(stderr, ", %lld)\n", x);
  return x;
}
#endif

int main()
{
  long long n = GetIndex();
  int nNodes = NumberOfNodes();
  int blksz = 100;

  vector<int> v;
  v.push_back(1);
  v.push_back(2);
  v.push_back(2);
  long long vi = 2;
  while(v.size() > n)
    v.pop_back();
  while(v.size() < min(vi+blksz*nNodes, n)) {
    int d = 1+(vi&1);
    v.push_back(d);
    if(v[vi++] == 2 && v.size() < n)
      v.push_back(d);
  }
  long long s = 0;
  if(MyNodeId() == 0) {
    for(long long i = 0; i < v.size(); i++) {
      s += (long long)v[i]*GetMultiplier(i);
      //fprintf(stderr, "GetMultiplier(%lld) %d\n", i, v[i]);
    }
    if(v.size() == n)
      printf("%lld\n", s);
  }
  if(v.size() == n)
    return 0;
  long long off = v.size();
  vector<int> job(v.begin()+vi+MyNodeId()*blksz, v.begin()+vi+(MyNodeId()+1)*blksz);

  while(off < n) {
    long long vis = 0, offs = 0;
    vis += job.size();
    for(int i = 0; i < job.size(); i++)
      offs += job[i];
    vector<long long> viv, offv;
    for(int i = 1; !(MyNodeId()&i) && MyNodeId()+i < nNodes; i <<= 1) {
      int src = MyNodeId()+i;
      viv.push_back(vis);
      offv.push_back(offs);
      RECEIVE(src);
      vis += GETLL(src);
      offs += GETLL(src);
    }
    if(MyNodeId() > 0) {
      int dest = (MyNodeId() & (MyNodeId()-1));
      PUTLL(dest, vis);
      PUTLL(dest, offs);
      SEND(dest);
      RECEIVE(dest);
      vi = GETLL(dest);
      off = GETLL(dest);
      offs = GETLL(dest);
    } else {
      vis += vi;
      offs += off;
    }
    for(int i = 1, j = 0; !(MyNodeId()&i) && MyNodeId()+i < nNodes; i <<= 1, j++) {
      int src = MyNodeId()+i;
      PUTLL(src, vi+viv[j]);
      PUTLL(src, off+offv[j]);
      PUTLL(src, offs);
      SEND(src);
    }
    vector<int> vo;
    int d = 1+(vi&1);
    for(int i = 0; i < job.size() && off < n; i++) {
      vo.push_back(d);
      //fprintf(stderr, "GetMultiplier(%lld) %d\n", off, d);
      s += (GetMultiplier(off++) << (d-1));
      if(job[i] == 2 && off < n) {
        vo.push_back(d);
        //fprintf(stderr, "GetMultiplier(%lld) %d\n", off, d);
        s += (GetMultiplier(off++) << (d-1));
      }
      d = 3-d;
      ++vi;
    }
    if(MyNodeId() == 0) {
      vi = vis;
    }
    off = offs;
    job.swap(vo);
  }
  if(MyNodeId() > 0) {
    PUTLL(0, s);
    SEND(0);
  } else {
    for(int i = 1; i < nNodes; i++) {
      int src = RECEIVE(i);
      s += GETLL(src);
    }
    printf("%lld\n", s);
  }
  return 0;
}
