#include <cstdio>
#include <cassert>
#include <vector>
#include <algorithm>
#include "message.h"
#include "rocks.h"
using namespace std;

int main()
{
  int n = GetN(),k = GetK();
  int nNodes = NumberOfNodes();
  int blksz = (n+179)/180, rs = 20, rs_step = rs*nNodes;
  // processing columns
  int nr = (n-MyNodeId()*rs+nNodes*rs-1)/(nNodes*rs)*rs;
  vector<vector<int> > up(nr, vector<int>(n,-1));
  //fprintf(stderr,"%d\n", up.size());
  vector<int> cs(n,0);
  for(int c = MyNodeId(); c < n; c += nNodes) {
    for(int r = 0, s = 0; r < n; r++) {
      if(IsRock(r,c)) s++;
      cs[r] = s;
    }
    for(int i = 0, target = 0, j = 0, r = 0, rt = 0; r < n; i++,r++) {
      if(i == rs) {
        i = 0;
        target++;
        if(target == nNodes)
          target = 0;
      }
      while((rt+k+1<n && cs[rt+k+1]-cs[r] <= k) || (rt+k+1>=n && cs.back()-cs[r] < n-1-rt))
        ++rt;
      if(target == MyNodeId())
        up[j++][c] = rt;
      else
        PutInt(target,rt);
    }
  }
  // send & receive column results
  for(int i = 0; i < nNodes; i++) {
    if(i != MyNodeId()) Send(i);
  }
  for(int cnt = 0; cnt < nNodes; cnt++) {
    if(cnt == MyNodeId()) continue;
    int src = Receive(cnt);
    for(int c = src; c < n; c += nNodes)
      for(int ri = rs*MyNodeId(), j=0; ri < n; ri += rs_step)
        for(int r = ri; r < n && r < ri+rs; r++)
          up[j++][c] = GetInt(src);
  }
  // processing rows
  int src = MyNodeId()-1;
  int dest = MyNodeId()+1;
  if(src < 0)
    src += nNodes;
  if(dest >= nNodes)
    dest -= nNodes;
  vector<vector<int> > rt(rs, vector<int>(n,-1));
  vector<int> rsum(n,0);
  for(int rb = rs*MyNodeId(), ib = 0; rb < n; rb += rs_step, ib += rs) {
    for(int r = rb; r < rb+rs && r < n; r++) {
      for(int c = 0, s = 0; c < n; c++) {
        if(IsRock(r,c)) s++;
        rsum[c] = s;
      }
      for(int c = 0, ct = 0; c < n; c++) {
        while((ct+k+1<n && rsum[ct+k+1]-rsum[c] <= k) || (ct+k+1>=n && rsum.back()-rsum[c] < n-1-ct))
          ++ct;
        rt[r-rb][c] = ct;
      }
    }
    vector<int> rtc(rs, -1);
    for(int cb = 0; cb < n; cb += blksz) {
      vector<int> upc(blksz,-1);
      if(rb != 0) {
        //fprintf(stderr, "%d: Receiving from %d\n", __LINE__, src);
        Receive(src);
        //fprintf(stderr, "%d: Received from %d\n", __LINE__, src);
        for(int c = cb; c < n && c < cb+blksz; c++)
          upc[c-cb] = GetInt(src);
      } else if(cb == 0)
        upc[0] = up[0][0];
      for(int r = rb, i = ib; r < rb+rs && r < n; r++, i++)
        for(int c = cb; c < n && c < cb+blksz; c++) {
          int upn = upc[c-cb], rtn = rtc[r-rb];
          if(upc[c-cb] >= r)
            rtn = rt[r-rb][c];
          if(rtc[r-rb] >= c)
            upn = up[i][c];
          upc[c-cb] = upn;
          rtc[r-rb] = rtn;
        }
      if(rb + rs < n) {
        for(int c = cb; c < n && c < cb+blksz; c++)
          PutInt(dest, upc[c-cb]);
        Send(dest);
        //fprintf(stderr, "%d: Sent to %d\n", __LINE__, dest);
      } else if(cb+blksz >= n)
        printf((upc[(n-1)%blksz] >= n-1 || rtc[(n-1)%rs] >= n-1) ? "YES\n" : "NO\n");
    }
  }
  return 0;
}

