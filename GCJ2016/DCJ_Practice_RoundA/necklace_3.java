// Sample input 3, in Java.
public class necklace {
  public necklace() {
  }

  public static long GetNecklaceLength() {
    return 5L;
  }

  public static long GetMessageLength() {
    return 8L;
  }

  public static int GetNecklaceElement(long index) {
    switch ((int)index) {
      case 0: return 0;
      case 1: return 1;
      case 2: return 0;
      case 3: return 2;
      case 4: return 0;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }

  public static int GetMessageElement(long index) {
    switch ((int)index) {
      case 0: return 0;
      case 1: return 0;
      case 2: return 0;
      case 3: return 1;
      case 4: return 2;
      case 5: return 0;
      case 6: return 0;
      case 7: return 0;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}