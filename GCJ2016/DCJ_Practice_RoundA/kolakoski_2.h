// Sample input 2, in CPP.

#include <cassert>

long long GetIndex() {
  return 10LL;
}

long long GetMultiplier(long long index) {
  switch ((int)index) {
    case 0: return 1LL;
    case 1: return 1LL;
    case 2: return 1LL;
    case 3: return 1LL;
    case 4: return 1LL;
    case 5: return 1LL;
    case 6: return 1LL;
    case 7: return 1LL;
    case 8: return 1LL;
    case 9: return 1LL;
    default: assert(0);
  }
}
