// Sample input 1, in Java.
public class load_balance {
  public load_balance() {
  }

  public static int GetN() {
    return 4;
  }

  public static long GetWeight(long i) {
    switch ((int)i) {
      case 0: return 1L;
      case 1: return 1L;
      case 2: return 1L;
      case 3: return 1L;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}