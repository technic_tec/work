// Sample input 3, in Java.
public class rocks {
  public rocks() {
  }

  public static int GetN() {
    return 5;
  }

  public static int GetK() {
    return 1;
  }

  public static boolean IsRock(long x, long y) {
    if (x == 0L && y == 0L) return false;
    if (x == 0L && y == 1L) return false;
    if (x == 0L && y == 2L) return false;
    if (x == 0L && y == 3L) return false;
    if (x == 0L && y == 4L) return false;
    if (x == 1L && y == 0L) return true;
    if (x == 1L && y == 1L) return true;
    if (x == 1L && y == 2L) return true;
    if (x == 1L && y == 3L) return true;
    if (x == 1L && y == 4L) return false;
    if (x == 2L && y == 0L) return false;
    if (x == 2L && y == 1L) return false;
    if (x == 2L && y == 2L) return false;
    if (x == 2L && y == 3L) return false;
    if (x == 2L && y == 4L) return false;
    if (x == 3L && y == 0L) return false;
    if (x == 3L && y == 1L) return true;
    if (x == 3L && y == 2L) return true;
    if (x == 3L && y == 3L) return true;
    if (x == 3L && y == 4L) return true;
    if (x == 4L && y == 0L) return false;
    if (x == 4L && y == 1L) return false;
    if (x == 4L && y == 2L) return false;
    if (x == 4L && y == 3L) return false;
    if (x == 4L && y == 4L) return false;
    throw new IllegalArgumentException("Invalid argument");
  }
}