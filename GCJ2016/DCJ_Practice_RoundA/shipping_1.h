// Sample input 1, in CPP.

#include <cassert>

long long NumberOfVillages() {
  return 3LL;
}

int VillageFaction(long long index) {
  switch ((int)index) {
    case 0: return 2;
    case 1: return 2;
    case 2: return 2;
    default: assert(0);
  }
}

long long VillageImmediatelyDownstream(long long index) {
  switch ((int)index) {
    case 0: return 0LL;
    case 1: return 0LL;
    case 2: return 0LL;
    default: assert(0);
  }
}

int NumberOfShipments() {
  return 3;
}

int GetShipmentSource(long long index) {
  switch ((int)index) {
    case 0: return 0;
    case 1: return 0;
    case 2: return 1;
    default: assert(0);
  }
}

int GetShipmentDestination(long long index) {
  switch ((int)index) {
    case 0: return 1;
    case 1: return 2;
    case 2: return 2;
    default: assert(0);
  }
}
