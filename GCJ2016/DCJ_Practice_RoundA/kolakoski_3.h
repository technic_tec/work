// Sample input 3, in CPP.

#include <cassert>

long long GetIndex() {
  return 20LL;
}

long long GetMultiplier(long long index) {
  switch ((int)index) {
    case 0: return 1LL;
    case 1: return 2LL;
    case 2: return 2LL;
    case 3: return 1LL;
    case 4: return 1LL;
    case 5: return 2LL;
    case 6: return 1LL;
    case 7: return 2LL;
    case 8: return 2LL;
    case 9: return 1LL;
    case 10: return 2LL;
    case 11: return 2LL;
    case 12: return 1LL;
    case 13: return 1LL;
    case 14: return 2LL;
    case 15: return 1LL;
    case 16: return 1LL;
    case 17: return 2LL;
    case 18: return 2LL;
    case 19: return 1LL;
    default: assert(0);
  }
}
