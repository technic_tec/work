// Sample input 3, in Java.
public class shipping {
  public shipping() {
  }

  public static long NumberOfVillages() {
    return 8L;
  }

  public static int VillageFaction(long index) {
    switch ((int)index) {
      case 0: return 0;
      case 1: return 1;
      case 2: return 2;
      case 3: return 0;
      case 4: return 2;
      case 5: return 0;
      case 6: return 0;
      case 7: return 0;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }

  public static long VillageImmediatelyDownstream(long index) {
    switch ((int)index) {
      case 0: return 0L;
      case 1: return 0L;
      case 2: return 0L;
      case 3: return 1L;
      case 4: return 2L;
      case 5: return 4L;
      case 6: return 4L;
      case 7: return 2L;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }

  public static int NumberOfShipments() {
    return 3;
  }

  public static int GetShipmentSource(long index) {
    switch ((int)index) {
      case 0: return 1;
      case 1: return 3;
      case 2: return 5;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }

  public static int GetShipmentDestination(long index) {
    switch ((int)index) {
      case 0: return 6;
      case 1: return 7;
      case 2: return 6;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}