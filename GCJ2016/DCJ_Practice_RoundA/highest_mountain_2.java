// Sample input 2, in Java.
public class highest_mountain {
  public highest_mountain() {
  }

  public static int NumberOfPeaks() {
    return 3;
  }

  public static int GetHeight(long i) {
    switch ((int)i) {
      case 0: return 2;
      case 1: return 2;
      case 2: return 2;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}