#include <cstdio>
#include <cassert>
#include "message.h"
#include "majority.h"
using namespace std;

int main()
{
  int nNodes = NumberOfNodes();
  int target = (MyNodeId() - 1) / 2;
  int src1 = 2*MyNodeId()+1;
  int src2 = src1+1;
  int nsrcs = (src1<nNodes) + (src2<nNodes);
  long long n = GetN();
  // partial majority
  long long maj = -1, c = 0;
  for(long long i = MyNodeId(); i < n; i += nNodes) {
    long long x = GetVote(i);
    if(c == 0) {
      maj = x; c++;
    } else if(x == maj)
      c++;
    else
      c--;
  }
  // receiving
  for(int i = 0; i < nsrcs; i++) {
    int src = Receive(-1);
    long long maj1 = GetLL(src);
    long long c1 = GetLL(src);
    if(maj1 == maj)
      c += c1;
    else if(c1 > c) {
      maj = maj1;
      c = c1-c;
    } else
      c -= c1;
  }
  // sending
  if(MyNodeId() > 0) {
    PutLL(target, maj);
    PutLL(target, c);
    Send(target);
  } else {
    for(int i = 0; i < nNodes; i++) {
      PutLL(i,maj);
      Send(i);
    }
  }
  // verify
  Receive(0);
  maj = GetLL(0);
  c = 0;
  for(long long i = MyNodeId(); i < n; i += nNodes) {
    long long x = GetVote(i);
    if(x == maj)
      c++;
  }
  for(int i = 0; i < nsrcs; i++) {
    int src = Receive(-1);
    c += GetLL(src);
  }
  if(MyNodeId() > 0) {
    PutLL(target, c);
    Send(target);
  } else if(c > n-c) {
    printf("%lld\n", maj);
  } else {
    printf("NO WINNER\n");
  }
  return 0;
}
