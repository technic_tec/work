// Sample input 2, in CPP.

#include <cassert>

int GetN() {
  return 4;
}

long long GetWeight(long long i) {
  switch ((int)i) {
    case 0: return 4LL;
    case 1: return 1LL;
    case 2: return 6LL;
    case 3: return 5LL;
    default: assert(0);
  }
}
