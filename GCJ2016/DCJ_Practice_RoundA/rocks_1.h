// Sample input 1, in CPP.

#include <cassert>

int GetN() {
  return 3;
}

int GetK() {
  return 1;
}

bool IsRock(long long x, long long y) {
  if (x == 0 && y == 0) return false;
  if (x == 0 && y == 1) return true;
  if (x == 0 && y == 2) return true;
  if (x == 1 && y == 0) return true;
  if (x == 1 && y == 1) return true;
  if (x == 1 && y == 2) return false;
  if (x == 2 && y == 0) return false;
  if (x == 2 && y == 1) return false;
  if (x == 2 && y == 2) return false;
  assert(0);
}
