// Sample input 3, in Java.
public class highest_mountain {
  public highest_mountain() {
  }

  public static int NumberOfPeaks() {
    return 5;
  }

  public static int GetHeight(long i) {
    switch ((int)i) {
      case 0: return 1;
      case 1: return 3;
      case 2: return 4;
      case 3: return 2;
      case 4: return 1;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}