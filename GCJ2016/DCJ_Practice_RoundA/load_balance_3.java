// Sample input 3, in Java.
public class load_balance {
  public load_balance() {
  }

  public static int GetN() {
    return 7;
  }

  public static long GetWeight(long i) {
    switch ((int)i) {
      case 0: return 1L;
      case 1: return 2L;
      case 2: return 3L;
      case 3: return 4L;
      case 4: return 5L;
      case 5: return 6L;
      case 6: return 21L;
      default: throw new IllegalArgumentException("Invalid argument");
    }
  }
}