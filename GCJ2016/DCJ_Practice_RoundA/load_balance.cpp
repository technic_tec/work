#include <cstdio>
#include <cassert>
#include <algorithm>
#include <vector>
#include "message.h"
#include "load_balance.h"
using namespace std;

inline long long elem(const vector<long long> &v, int off, int b) {
  int i = off;
  if(b >> 16) {
    b >>= 16;
    b &= 0xffff;
    i += 16;
  }
  if(b >> 8) {
    b >>= 8;
    i += 8;
  }
  if(b >> 4) {
    b >>= 4;
    i += 4;
  }
  const int bits[] = {-1, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3};
  i += bits[b];
  return v[i];
}
void subset_sum(const vector<long long> &v, long long sv, int off, int sz, vector<long long> &ss, int id, int cnt) {
  int n = v.size();
  int bits = sz-off;
  int blk = (1<<bits) / cnt;
  int rem = (1<<bits) % cnt;
  int boff = id*blk;
  if(id < rem) {
    boff += id;
    blk ++;
  } else
    boff += rem;
  ss.clear();
  if(blk == 0) return;
  long long sc = 0;
  int lst = (boff ^ (boff >> 1));
  for(int i = 0, b = 1; i < bits; i++, b<<=1)
    if(lst & b)
      sc += v[off+i];
  if(sc <= sv)
    ss.push_back(sc);
  for(int i = boff+1; i < boff+blk; i++) {
    int mi = (i ^ (i >> 1));
    int bi = (mi ^ lst);
    assert((bi & (bi-1)) == 0);
    long long e = elem(v, off, bi);
    if(mi & bi)
      sc += e;
    else
      sc -= e;
    if(sc <= sv)
      ss.push_back(sc);
    lst = mi;
  }
  sort(ss.begin(), ss.end());
}

int main()
{
  int nNodes = NumberOfNodes();
  int n = GetN();
  vector<long long> v;
  long long sv = 0;
  for(int i = 0; i < n; i++) {
    v.push_back(GetWeight(i));
    sv += v.back();
  }
  sort(v.begin(),v.end());
  bool res = false;
  if((sv&1)==0 && (v.back() << 1) < sv) {
    sv >>= 1;
    sv -= v.back();
    int r, c;
    if(n & 1) {
      r = c = (nNodes == 100 ? 10 : 3);
    } else {
      if(nNodes == 100) {
        r = 14; c = 7;
      } else {
        r = 5; c = 2;
      }
    }
    int rid = MyNodeId() / c, cid = MyNodeId() % c;
    if(rid < r) {
      vector<long long> rs, cs;
      subset_sum(v, sv, 0, n/2, rs, rid, r);
      subset_sum(v, sv, n/2, n-1, cs, cid, c);
      if((!rs.empty() && rs.back() == sv) || (!cs.empty() && cs.back() == sv))
        res = true;
      else {
        int ri = 0, ci = cs.size()-1;
        while(ri < rs.size() && ci >= 0) {
          if(rs[ri]+cs[ci] > sv)
            --ci;
          else if(rs[ri]+cs[ci] < sv)
            ++ri;
          else {
            res = true;
            break;
          }
        }
      }
    }
  } else if((v.back()<<1) == sv)
    res = true;
  // sending
  if(MyNodeId() > 0) {
    PutChar(0, res);
    Send(0);
  } else {
    for(int i = 1; i < nNodes; i++) {
      int src = Receive(-1);
      if(GetChar(src))
        res = true;
    }
    printf(res ? "POSSIBLE\n" : "IMPOSSIBLE\n");
  }
  return 0;
}
