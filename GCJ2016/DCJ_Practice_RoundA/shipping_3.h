// Sample input 3, in CPP.

#include <cassert>

long long NumberOfVillages() {
  return 8LL;
}

int VillageFaction(long long index) {
  switch ((int)index) {
    case 0: return 0;
    case 1: return 1;
    case 2: return 2;
    case 3: return 0;
    case 4: return 2;
    case 5: return 0;
    case 6: return 0;
    case 7: return 0;
    default: assert(0);
  }
}

long long VillageImmediatelyDownstream(long long index) {
  switch ((int)index) {
    case 0: return 0LL;
    case 1: return 0LL;
    case 2: return 0LL;
    case 3: return 1LL;
    case 4: return 2LL;
    case 5: return 4LL;
    case 6: return 4LL;
    case 7: return 2LL;
    default: assert(0);
  }
}

int NumberOfShipments() {
  return 3;
}

int GetShipmentSource(long long index) {
  switch ((int)index) {
    case 0: return 1;
    case 1: return 3;
    case 2: return 5;
    default: assert(0);
  }
}

int GetShipmentDestination(long long index) {
  switch ((int)index) {
    case 0: return 6;
    case 1: return 7;
    case 2: return 6;
    default: assert(0);
  }
}
