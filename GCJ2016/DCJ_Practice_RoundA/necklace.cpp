#include <cstdio>
#include <cassert>
#include <algorithm>
#include <vector>
#include "message.h"
#include "necklace.h"
using namespace std;

#define M 10000
int main()
{
  int n = GetNecklaceLength();
  int sz = n / NumberOfNodes();
  int st = MyNodeId() * sz;
  if(MyNodeId() < n % NumberOfNodes()) {
    st += MyNodeId(); sz++;
  } else {
    st += n % NumberOfNodes();
  }
  vector<vector<int> > q(M+1);
  for(int i = st; i < st+sz; i++) {
    int x = GetNecklaceElement(i);
    q[x].push_back(i);
  }
  int m = GetMessageLength();
  int bulk = (m+999)/1000;
  vector<int> msg(m,-M-1);
  int src = MyNodeId()-1, dest = MyNodeId()+1;
  if(dest >= NumberOfNodes()) dest = -1;
  int res = 0;
  vector<int> buf;
  buf.reserve(2*bulk);
  for(int i = 0, j = 0; i < m; i++, j++) {
    int fst,cur;
    if(j == bulk) j = 0;
    if(src < 0) {
      fst = cur = i;
    } else {
      if(j == 0)
        Receive(src);
      fst = GetInt(src);
      cur = GetInt(src);
    }
    int l = st, r = st+sz-1;
    while(cur < m) {
      int elem = msg[cur];
      if(elem < 0)
        elem = msg[cur] = GetMessageElement(cur);
      if(q[elem].empty() || q[elem].back() < l)
        break;
      else if(q[elem].front() >= l)
        l = q[elem].front()+1;
      else {
        int lo = 0, hi = q[elem].size()-1;
        while(hi-lo>1) {
          int mid = (lo+hi)/2;
          if(q[elem][mid] < l)
            lo = mid;
          else
            hi = mid;
        }
        l = q[elem][hi]+1;
      }
      ++cur;
    }
    if(dest < 0) {
      res = max(res, cur-fst);
    } else {
      if(j == 0 && buf.size() > 0) {
        for(int i = 0; i < buf.size(); i++)
          PutInt(dest,buf[i]);
        Send(dest);
        buf.clear();
      }
      buf.push_back(fst);
      buf.push_back(cur);
    }
  }
  if(dest < 0) {
    printf("%d\n", res);
  } else if(buf.size() > 0){
    for(int i = 0; i < buf.size(); i++)
      PutInt(dest,buf[i]);
    Send(dest);
    buf.clear();
  }
  return 0;
}
