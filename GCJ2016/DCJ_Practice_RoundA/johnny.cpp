#include <cstdio>
#include <cassert>
#include <algorithm>
#include <vector>
#include "message.h"
#include "johnny.h"
using namespace std;
#if 1
#define RECEIVE(x...) Receive(x)
#define SEND(x...) Send(x)
#define PUTChar(x...) PutChar(x)
#define PUTInt(x...) PutInt(x)
#define PUTLL(x...) PutLL(x)
#define GETChar(x...) GetChar(x)
#define GETInt(x...) GetInt(x)
#define GETLL(x...) GetLL(x)
#else
void SEND(int target) {
  fprintf(stderr, "%s(%d)\n", __FUNCTION__, target);
  Send(target);
}
int RECEIVE(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int src = Receive(source);
  fprintf(stderr, "->%d)\n", src);
  return src;
}
void PUTChar(int target, char value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutChar(target, value);
}
void PUTInt(int target, int value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutInt(target, value);
}
void PUTLL(int target, long long value) {
  fprintf(stderr, "%s(%d, %lld)\n", __FUNCTION__, target, value);
  PutLL(target, value);
}
char GETChar(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  char x = GetChar(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
int GETInt(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int x = GetInt(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
long long GETLL(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  long long x = GetLL(source);
  fprintf(stderr, ", %lld)\n", x);
  return x;
}
#endif

class DescCmp {
  public:
    DescCmp(vector<int> &v) : m_key(v) {}
    bool operator() (const int &a, const int &b) {
      return m_key[a] > m_key[b];
    }
  private:
    vector<int> &m_key;
};
int main()
{
  int n = NumberOfCards();// n <= 20000
  int nNodes = min(NumberOfNodes(), n);
  if(MyNodeId() >= nNodes)
    return 0;
  int rem = n%nNodes;
  int blksz = n/nNodes;
  int st = 0;
  vector<int> off;
  for(int i = 0; i < nNodes; i++) {
    off.push_back(st);
    st += blksz;
    if(rem) {
      ++st; --rem;
    }
  }
  assert(st == n);
  off.push_back(st);
  st = off[MyNodeId()];
  int ed = off[MyNodeId()+1];
  bool master = (MyNodeId() == 0);

  // 1. compute out degree for each v
  vector<int> deg(n, 0);
  for(int i = st; i < ed; i++) {
    int _deg = 0;
    for(int j = 0; j < n; j++)
      if(j != i && IsBetter(j, i))
        _deg++;
    deg[i] = _deg;
  }
  // 2. order by out degree
  vector<int> p;
  if(!master) {
    for(int i = st; i < ed; i++)
      PUTInt(0, deg[i]);
    SEND(0);
    RECEIVE(0);
    for(int i = 0; i < n; i++)
      p.push_back(GETInt(0));
  } else {
    for(int i = 1; i < nNodes; i++) {
      int dst = RECEIVE(-1);
      for(int j = off[dst]; j < off[dst+1]; j++)
        deg[j] = GETInt(dst);
    }
    for(int i = 0; i < n; i++)
      p.push_back(i);
    sort(p.begin(), p.end(), DescCmp(deg));
    for(int i = 1; i < nNodes; i++) {
      for(int j = 0; j < n; j++)
        PUTInt(i, p[j]);
      SEND(i);
    }
  }
  // 3. compute min({deg(y)|v<y}) and max({deg(x)|x<v)
  vector<int> imin(n, -1);
  vector<int> omax(n, n);
  for(int i = st; i < ed; i++) {
    for(int j = 0; j < n; j++) {
      if(j == i) continue;
      if(IsBetter(p[j], p[i])) {
        omax[i] = min(omax[i], j); // maxDeg better than p[i]
      } else {
        imin[i] = max(imin[i], j); // minDeg worse than p[i]
      }
    }
  }
  // 4. G=S+T, any (s in S, t in T) s < t => deg(s) > deg(t)
  if(master) {
    for(int i = 1; i < nNodes; i++) {
      int dst = RECEIVE(-1);
      for(int j = off[dst]; j < off[dst+1]; j++) {
        imin[j] = GETInt(dst);
        omax[j] = GETInt(dst);
      }
    }
    // max(imin[0..i-1]) <= i-1, min(omax[i..n-1]) >= i
    vector<int> rmin(n);
    for(int i = n-1, _rmin = n; i >= 0; i--) {
      _rmin = min(_rmin, omax[i]);
      rmin[i] = _rmin;
    }
    int res = n;
    for(int i = 1, _lmax = imin[0]; i < n; i++) {
      if(deg[p[i-1]] > deg[p[i]] && _lmax <= i-1 && rmin[i] >= i) {
        res = i;
        break;
      }
      _lmax = max(_lmax, imin[i]);
    }
    if(res < n) printf("%d\n", n-res);
    else printf("IMPOSSIBLE\n");
  } else {
    for(int i = st; i < ed; i++) {
      PUTInt(0, imin[i]);
      PUTInt(0, omax[i]);
    }
    SEND(0);
  }
  return 0;
}

