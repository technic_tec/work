// Sample input 2, in Java.
public class rocks {
  public rocks() {
  }

  public static int GetN() {
    return 4;
  }

  public static int GetK() {
    return 4;
  }

  public static boolean IsRock(long x, long y) {
    if (x == 0L && y == 0L) return false;
    if (x == 0L && y == 1L) return false;
    if (x == 0L && y == 2L) return false;
    if (x == 0L && y == 3L) return false;
    if (x == 1L && y == 0L) return false;
    if (x == 1L && y == 1L) return false;
    if (x == 1L && y == 2L) return false;
    if (x == 1L && y == 3L) return false;
    if (x == 2L && y == 0L) return false;
    if (x == 2L && y == 1L) return false;
    if (x == 2L && y == 2L) return false;
    if (x == 2L && y == 3L) return true;
    if (x == 3L && y == 0L) return false;
    if (x == 3L && y == 1L) return false;
    if (x == 3L && y == 2L) return true;
    if (x == 3L && y == 3L) return false;
    throw new IllegalArgumentException("Invalid argument");
  }
}