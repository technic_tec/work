#include <cstdio>
#include <cassert>
#include <algorithm>
#include <vector>
#include "message.h"
#include "sandwich.h"
using namespace std;
#if 1
#define RECEIVE(x...) Receive(x)
#define SEND(x...) Send(x)
#define PUTChar(x...) PutChar(x)
#define PUTInt(x...) PutInt(x)
#define PUTLL(x...) PutLL(x)
#define GETChar(x...) GetChar(x)
#define GETInt(x...) GetInt(x)
#define GETLL(x...) GetLL(x)
#else
void SEND(int target) {
  fprintf(stderr, "%s(%d)\n", __FUNCTION__, target);
  Send(target);
}
int RECEIVE(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int src = Receive(source);
  fprintf(stderr, "->%d)\n", src);
  return src;
}
void PUTChar(int target, char value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutChar(target, value);
}
void PUTInt(int target, int value) {
  fprintf(stderr, "%s(%d, %d)\n", __FUNCTION__, target, value);
  PutInt(target, value);
}
void PUTLL(int target, long long value) {
  fprintf(stderr, "%s(%d, %lld)\n", __FUNCTION__, target, value);
  PutLL(target, value);
}
char GETChar(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  char x = GetChar(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
int GETInt(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  int x = GetInt(source);
  fprintf(stderr, ", %d)\n", x);
  return x;
}
long long GETLL(int source) {
  fprintf(stderr, "%s(%d", __FUNCTION__, source);
  fflush(stderr);
  long long x = GetLL(source);
  fprintf(stderr, ", %lld)\n", x);
  return x;
}
#endif

int main()
{
  int n = GetN();
  int nNodes = min(NumberOfNodes(), n);
  if(MyNodeId() >= nNodes)
    return 0;
  int rem = n%nNodes;
  int blksz = n/nNodes + (MyNodeId()<rem);
  int st = (MyNodeId()<rem ? MyNodeId()*blksz : MyNodeId()*blksz+rem);

  long long tot = 0, pmin = 0, pmax = 0, smin = 0, s = 0;
  for(int i = st; i < st+blksz; i++) {
    long long x = GetTaste(i);
    tot += x;
    s = min(s+x,(long long)0);
    smin = min(s, smin);
    pmin = min(tot, pmin);
    pmax = max(tot, pmax);
  }
  if(MyNodeId() > 0) {
    PUTLL(0, smin);
    PUTLL(0, pmin);
    PUTLL(0, pmax);
    PUTLL(0, tot);
    SEND(0);
  } else {
    long long sum = tot, min_gap = smin, max_pre = pmax;
    for(int i = 1; i < nNodes; i++) {
      int src = RECEIVE(i);
      smin = GETLL(src);
      pmin = GETLL(src);
      pmax = GETLL(src);
      tot  = GETLL(src);
      min_gap = min(min_gap, min(smin, pmin+sum-max_pre));
      max_pre = max(max_pre, sum+pmax);
      sum += tot;
    }
    // output
    printf("%lld\n", sum-min_gap);
  }
  return 0;
}
