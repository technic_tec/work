// Sample input 5, in CPP.

#include <cassert>

const long long _nn = 3000000000LL;
long long GetIndex() {
  return _nn;
}

long long GetMultiplier(long long index) {
  assert(index >= 0 && index < _nn);
  return 1LL;
}
