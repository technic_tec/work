// Sample input 5, in CPP.

#include <cstdlib>
#include <cassert>
#include <vector>

class Gen {
  public:
    Gen(int n, int seed = 0) : m_n(n) {
      const int N = 1000000000;
      srand(seed);
      for(int i = 0; i < n; i++)
        m_v.push_back(rand() % N);
    }
    int getN() { return m_n; }
    int get(long long i) {
      assert(i >= 0 && i < m_n);
      return m_v[i];
    }
  private:
    int m_n;
    std::vector<int> m_v;
};

Gen g(1000,1133);
int NumberOfPeaks() {
  return g.getN();
}

int GetHeight(long long i) {
  return g.get(i);
}
