// Sample input 4, in CPP.

#include <cassert>

const int _n = 10000;
const int _k = 100;
int GetN() {
  return _n;
}

int GetK() {
  return _k;
}

bool IsRock(long long x, long long y) {
  assert(x >= 0 && y >= 0 && x < _n && y < _n);
  if(((x+y)&1) && (y<_n-1 || x<_n-2))return true;
  else return false;
}
