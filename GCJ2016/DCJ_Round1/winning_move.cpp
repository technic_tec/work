#include <cstdio>
#include <cassert>
#include <algorithm>
#include <vector>
#include "message.h"
#include "winning_move.h"
using namespace std;

int main()
{
  int nNodes = NumberOfNodes();
  int n = GetNumPlayers();
  int my_id = MyNodeId();
  // partial majority
  vector<long long> vs;
  for(long long i = my_id; i < n; i += nNodes)
    vs.push_back(GetSubmission(i));
  sort(vs.begin(),vs.end());
  vector<long long> vo;
  vector<bool> vf;
  for(int i = 0; i < vs.size(); i++) {
    vo.push_back(vs[i]);
    if(i+1 < vs.size() && vs[i] == vs[i+1]) {
      while(i+1 < vs.size() && vs[i] == vs[i+1])
        ++i;
      vf.push_back(false);
    } else
      vf.push_back(true);
  }
  vs.swap(vo);
  int i = 0;
  for(int step = 1; my_id + step < nNodes && !(my_id & step); step <<= 1) {
    int target = my_id + step;
    Receive(target);
    int sz = GetInt(target);
    vector<bool> vfo;
    vo.clear();
    while(sz--) {
      long long x = GetLL(target);
      bool xf = GetChar(target);
      while(i < vs.size() && vs[i] < x) {
        vo.push_back(vs[i]);
        vfo.push_back(vf[i++]);
      }
      if(i >= vs.size() || vs[i] > x) {
        vo.push_back(x);
        vfo.push_back(xf);
      } else {
        vo.push_back(x);
        vfo.push_back(false);
        ++i;
      }
    }
    vs.swap(vo);
    vf.swap(vfo);
  }
  if(my_id > 0) {
    int src = (my_id & (my_id-1));
    PutInt(src,vs.size());
    for( int j = 0; j < vs.size(); j++) {
      PutLL(src,vs[j]);
      PutChar(src,vf[j]);
      printf("%lld %d\n", vs[j], (int)vf[j]);
    }
    Send(src);
  } else {
    int i = 0;
    while(i < vs.size() && !vf[i])
      ++i;
    printf("%lld\n", (vs.size() == i ? 0LL : vs[i]));
  }
  return 0;
}
