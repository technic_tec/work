#include <message.h>
#include <cstdio>
#include <vector>
#include <algorithm>
#if 1
#include "crates.h"
#else
#include <cassert>

long long GetNumStacks() {
  return 1000000000LL;
}

long long GetStackHeight(long long i) {
  assert(i >= 1 && i <= 1000000000LL);
  if(i <= 500000000)
    return 1000000000LL;
  else
    return 1LL;
}
#endif
using namespace std;

#define MASTER_NODE 0
#define DONE -1
#define P 1000000007

int main() {
  long long N = GetNumStacks();
  long long nodes = NumberOfNodes();
  long long my_id = MyNodeId();
  if(nodes > N) {
    nodes = N;
    if(my_id >= nodes) return 0;
  }
  long long blksz = N / nodes + (my_id < N % nodes);
  long long st = (my_id < N % nodes ? blksz * my_id : blksz * my_id + N % nodes);
  long long s = 0;
  for(long long i = st; i < st+blksz; i++)
    s += GetStackHeight(i+1);

  long long acc, tot;
  if (my_id == MASTER_NODE) {
    vector<long long> ss(nodes, 0);
    ss[MASTER_NODE] = s; 
    for (int i = 1; i < nodes; ++i) {
        int src = Receive(-1);
        ss[src] = GetLL(src);
    }
    for (int i = 1; i < nodes; ++i)
      ss[i] += ss[i-1];
    for (int i = 1; i < nodes; i++) {
      PutLL(i, ss[i-1]);
      PutLL(i, ss[nodes-1]);
      Send(i);
    }
    acc = 0;
    tot = ss[nodes-1];
  } else {
    PutLL(MASTER_NODE, s);
    Send(MASTER_NODE);
    Receive(MASTER_NODE);
    acc = GetLL(MASTER_NODE);
    tot = GetLL(MASTER_NODE);
  }
  long long eh = tot / N;
  long long rem = tot % N;
  long long exp = eh*st + min(st, rem);
  long long mov = 0;
  for(long long i = st; i < st+blksz; i++) {
    mov += llabs(acc-exp);
    mov %= P;
    acc += GetStackHeight(i+1);
    exp += eh + (i<rem);
  }
  if (my_id == MASTER_NODE) {
    for (int i = 1; i < nodes; ++i) {
        int src = Receive(i);
        mov += GetLL(src);
        if(mov >= P) mov -= P;
    }
    printf("%lld\n", mov);
  } else {
    PutLL(MASTER_NODE, mov);
    Send(MASTER_NODE);
  }
  return 0;
}
