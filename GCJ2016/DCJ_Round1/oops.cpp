#include <message.h>
#include <cstdio>
#include <algorithm>
#include "oops.h"
using namespace std;

#define MASTER_NODE 0
#define DONE -1

int main() {
  long long N = GetN();
  long long nodes = NumberOfNodes();
  long long my_id = MyNodeId();
  long long vmin, vmax;
  if(nodes > N) {
    nodes = N;
    if(my_id >= nodes) return 0;
  }
  vmin = vmax = GetNumber(my_id);
  for (long long i = my_id; i < N; i += nodes) {
    long long x = GetNumber(i);
    vmin = min(x, vmin);
    vmax = max(x, vmax);
  }
  PutLL(MASTER_NODE, vmin);
  PutLL(MASTER_NODE, vmax);
  Send(MASTER_NODE);

  if (my_id == MASTER_NODE) {
    for (int i = 0; i < nodes; ++i) {
        int node = Receive(-1);
        vmin = min(vmin, GetLL(node));
        vmax = max(vmax, GetLL(node));
    }
    printf("%lld\n", vmax-vmin);
  }
  return 0;
}
