#!/bin/bash

if [ $# -eq 0 ]; then
  set *.cpp
  objs=${@%.cpp}
  set $objs
fi
for obj in $@; do
  src=$obj.cpp
  if [ ! -f $src ]; then
    echo "no module file $src"
    exit -1
  fi
  for f in ${obj}_*.h; do
    cp $f ${obj}.h
    make -B run-${obj}
  done
done
