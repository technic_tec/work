#include <message.h>
#include <cstdio>
#include <algorithm>
#include "rps.h"
using namespace std;

#define MASTER_NODE 0

int pk(int x, int y, vector<char> &p)
{
  if((p[x] == 'R' && p[y] == 'P') || (p[x] == 'P' && p[y] == 'S') || (p[x] == 'S' && p[y] == 'R'))
    return y;
  else
    return x;
}

int main() {
  int N = GetN();
  int tot = (1<<N);
  long long nodes = NumberOfNodes();
  long long my_id = MyNodeId();
  if(nodes > tot) {
    nodes = tot;
    if(my_id >= nodes) return 0;
  }
  // 128 = 28*2+72, 10=6*2+4
  int blks = nodes;
  while(blks & (blks-1))
    blks += blks & (-blks);
  int blksz = tot/blks;
  int num_cand = 1+(MyNodeId()>=2*nodes-blks);
  int st = (num_cand == 1 ? blksz*MyNodeId() : blksz*2*MyNodeId()-blksz*(2*nodes-blks));
  vector<char> p;
  vector<int> pi;
  for (int i = st; i < st+(blksz<<(num_cand-1)); ++i) {
    p.push_back(GetFavoriteMove(i));
    pi.push_back(i);
  }
  while(pi.size() > num_cand) {
    vector<int> pr;
    for(int i = 0; i < pi.size(); i+=2) {
      pr.push_back(pk(pi[i]-st,pi[i+1]-st,p)+st);
    }
    pi.swap(pr);
  }
  PutInt(MASTER_NODE, num_cand);
  for(int i = 0; i < num_cand; i++) {
    PutInt(MASTER_NODE, pi[i]);
    PutChar(MASTER_NODE, p[pi[i]-st]);
  }
  Send(MASTER_NODE);

  if (my_id == MASTER_NODE) {
    p.clear();
    pi.clear();
    vector<int> po;
    for (int i = 0,k=0; i < nodes; ++i) {
        int node = Receive(i);
        num_cand = GetInt(node);
        for(int j = 0; j < num_cand; j++) {
          po.push_back(GetInt(node));
          p.push_back(GetChar(node));
          pi.push_back(k++);
        }
    }
    while(pi.size() > 1) {
      vector<int> pr;
      for(int i = 0; i < pi.size(); i+=2) {
        pr.push_back(pk(pi[i],pi[i+1],p));
      }
      pi.swap(pr);
    }
    printf("%d\n", po[pi[0]]);
  }
  return 0;
}
