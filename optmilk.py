import sys
class Stat(object):
    def __init__(self, v):
        self.sz = len(v)
        self.vl = [0]*(2*self.sz)
        self.vr = [0]*(2*self.sz)
        self.vn = [0]*(2*self.sz)
        self.vb = [0]*(2*self.sz)
        j = self.sz
        while (j & (j-1)):
            j += (j&(-j))
        for k in range(self.sz):
            if k < 2*self.sz-j:
                i = j+k
            else:
                i = j+k-self.sz
            self.vl[i] = self.vr[i] = self.vn[i] = 0
            self.vb[i] = v[k]
        for i in range(self.sz-1, 0, -1):
            self.vb[i] = max([self.vl[2*i]+self.vb[2*i+1], self.vl[2*i]+self.vr[2*i+1], self.vb[2*i]+self.vr[2*i+1]])
            self.vl[i] = max([self.vl[2*i]+self.vl[2*i+1], self.vl[2*i]+self.vn[2*i+1], self.vb[2*i]+self.vn[2*i+1]])
            self.vr[i] = max([self.vn[2*i]+self.vb[2*i+1], self.vn[2*i]+self.vr[2*i+1], self.vr[2*i]+self.vr[2*i+1]])
            self.vn[i] = max([self.vn[2*i]+self.vl[2*i+1], self.vn[2*i]+self.vn[2*i+1], self.vr[2*i]+self.vn[2*i+1]])

    def update(self, k, x):
        j = self.sz
        while (j & (j-1)):
            j += (j&(-j))
        k -= 1
        if k < 2*self.sz-j:
            i = j+k
        else:
            i = j+k-self.sz
        self.vb[i] = x
        i >>= 1
        while i > 0:
            self.vb[i] = max([self.vl[2*i]+self.vb[2*i+1], self.vl[2*i]+self.vr[2*i+1], self.vb[2*i]+self.vr[2*i+1]])
            self.vl[i] = max([self.vl[2*i]+self.vl[2*i+1], self.vl[2*i]+self.vn[2*i+1], self.vb[2*i]+self.vn[2*i+1]])
            self.vr[i] = max([self.vn[2*i]+self.vb[2*i+1], self.vn[2*i]+self.vr[2*i+1], self.vr[2*i]+self.vr[2*i+1]])
            self.vn[i] = max([self.vn[2*i]+self.vl[2*i+1], self.vn[2*i]+self.vn[2*i+1], self.vr[2*i]+self.vn[2*i+1]])
            i >>= 1

    def query(self):
        return max([self.vl[1], self.vr[1], self.vb[1], self.vn[1]])

#fi = open('optmilk.in')
#fo = open('optmilk.out', 'w')
fi = sys.stdin
fo = sys.stdout
n, m = map(int, fi.readline().split())
v = []
for _ in range(n):
    v.append(int(fi.readline()))
st = Stat(v)
res = 0
for _ in range(m):
    k, x = map(int, fi.readline().split())
    st.update(k, x)
    res += st.query()
fo.write('%d\n' % res)
fi.close()
fo.close()
