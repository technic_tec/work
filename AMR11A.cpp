/*
 * =====================================================================================
 *
 *       Filename:  AMR11A.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2012年04月07日 17时28分03秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (), thang@c2micro.com
 *        Company:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <algorithm>

#define N 500
#define MIN(x,y) ((x)<(y)?(x):(y))
#define MAX(x,y) ((x)>(y)?(x):(y))

int r, c, v[N][N], sv[N][N];

bool acceptable(int s0)
{
    memset(sv, 0, sizeof(sv));
    sv[0][0] = s0 + v[0][0];
    if(sv[0][0] <= 0)
        return false;
    for(int j = 1; j < c; j++) {
        if(sv[0][j-1] + v[0][j] <= 0)
            break;
        sv[0][j] = sv[0][j-1]+v[0][j];
    }
    for(int i = 1; i < r; i++) {
        if(sv[i-1][0] + v[i][0] <= 0)
            break;
        sv[i][0] = sv[i-1][0]+v[i][0];
    }
    for(int i = 1; i < r; i++)
        for(int j = 1; j < c; j++) {
            if(sv[i-1][j] > 0 && sv[i-1][j]+v[i][j] > sv[i][j])
                sv[i][j] = sv[i-1][j]+v[i][j];
            if(sv[i][j-1] > 0 && sv[i][j-1]+v[i][j] > sv[i][j])
                sv[i][j] = sv[i][j-1]+v[i][j];
        }
    return (sv[r-1][c-1] > 0);
}
int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    scanf("%d%d", &r, &c);
    for(int i = 0; i < r; i++)
      for(int j = 0; j < c; j++)
        scanf("%d", &v[i][j]);
    int h = 0, l = 0, s = 0;
    for(int i = 0; i < c; i++) {
        s += v[0][i];
        if(h+s <= 0) h = 1-s;
    }
    for(int i = 1; i < r; i++) {
        s += v[i][c-1];
        if(h+s <= 0) h = 1-s;
    }
    while(h - l > 1) {
        int m = (h+l)/2;
        if(acceptable(m))
            h = m;
        else
            l = m;
    }
    printf("%d\n", h);
  }
  return 0;
}


