/*
 * =====================================================================================
 *
 *       Filename:  coin-on-the-table.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年05月23日 10时24分38秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 55
#define K 1100

int n, m, k;
char b[N][N];
int c[K][N][N];

int minCost(int x, int y, int k)
{
    const int inf = 2*n*m+10;
    if (x + y > k)
        return inf;
    else if(c[k][x][y] < 0) {
        int r = inf;
        if(x>0 && minCost(x-1, y, k-1) + int(b[x-1][y] != 'D') < r)
            r = minCost(x-1, y, k-1) + int(b[x-1][y] != 'D');
        if(x<n-1 && minCost(x+1, y, k-1) + int(b[x+1][y] != 'U') < r)
            r = minCost(x+1, y, k-1) + int(b[x+1][y] != 'U');
        if(y>0 && minCost(x, y-1, k-1) + int(b[x][y-1] != 'R') < r)
            r = minCost(x, y-1, k-1) + int(b[x][y-1] != 'R');
        if(y<m-1 && minCost(x, y+1, k-1) + int(b[x][y+1] != 'L') < r)
            r = minCost(x, y+1, k-1) + int(b[x][y+1] != 'L');
        c[k][x][y] = r;
    }
    return c[k][x][y];
}
int main()
{
    int sx, sy;
    scanf("%d%d%d", &n, &m, &k);
    for(int i = 0; i < n; i++) {
        scanf("%s", b[i]);
        char *pc = strchr(b[i], '*');
        if(pc) {
            sx = i;
            sy = pc - b[i];
        }
    }
    memset(c, -1, sizeof(c));
    for(int i = 0; i <= k; i++)
        c[i][0][0] = 0;
    int r = minCost(sx, sy, k);
    printf("%d\n", (r>n*m ? -1 : r));
    return 0;
}

