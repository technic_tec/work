#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <map>
using namespace std;

#define N 2000

int n, m, p[N], v[N][N];

void check(int n, int m, int p[], int v[N][N])
{
    bool *u = new bool[n*m];
    fill(u, u+n*m, false);
    for(int i = 0; i < n; i++)
        for(int j = 0; j < m; j++) {
            assert(v[i][j] >= 0 && v[i][j] < n*m);
            assert(j == 0 || v[i][j] > v[i][j-1]);
            assert(!u[v[i][j]]);
            u[v[i][j]] = true;
        }
    for(int i = 0; i < n; i++) {
        int sc = 0;
        for(int j = 0; j < m; j++)
            for(int k = 0; k < m; k++)
                if(v[i][j] > v[p[i]][k])
                    ++sc;
        assert(2*sc > m*m);
    }
    delete[] u;
}
int fill_cycle(int x, int k)
{
    const int c34[3][4] = {{0,  6,  7,  8}, {2,  4,  5,  11}, {1,  3,  9,  10}};
    int c = 0;
    for(int y = x, i = 0; y != x || !i; y = p[y], ++i)
        ++c;
    for(int y = x, i = 0; y != x || !i; y = p[y], ++i)
        for(int j = 0; j < m; j++)
            if(j < 4-(m&1)) {
                if(c == 3 && !(m&1))
                    v[y][j] = k+c34[i][j];
                else
                    v[y][j] = k+j*c+(j>=i?j-i:c+j-i);
            } else if(j & 1) {
                v[y][j] = j*c+k+c-1-i;
            } else {
                v[y][j] = j*c+k+i;
            }
    return c;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        bool valid = true;
        scanf("%d%d", &n, &m);
        for(int i = 0; i < n; i++) {
            scanf("%d", &p[i]);
            v[i][0] = -1;
        }
        if(n <= 2 || m <= 2) {
            printf("No Solution\n");
            continue;
        }
        int k = 0;
        for(int i = 0; i < n; i++) {
            if(v[i][0] >= 0) continue;
            int c = fill_cycle(i, k);
            if(c < 3) {
                printf("No Solution\n");
                valid = false;
                break;
            }
            k += c*m;
        }
        if(valid) {
            check(n, m, p, v);
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    printf("%d%c", v[i][j]+1, (j<m-1?' ':'\n'));
        }
    }
    return 0;
}

