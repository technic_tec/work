#nim = dict()
#
#def partition(n, m, nosingle=False):
#    if m > n:
#        return set()
#    r = set()
#    for k in range(m, n):
#        y = pile(k)
#        r = r.union(set([x^y for x in partition(n-k, k+1)]))
#    if not nosingle:
#        r = r.union(set([pile(n)]))
#    return r
#
#def mex(s):
#    r = 0
#    while r in s:
#        r += 1
#    return r
#
#def pile(n):
#    if n not in nim:
#        s=partition(n, 1, True)
#        nim[n] = mex(s)
#        print n, s, nim[n]
#    return nim[n]
#
#ts = [pile(x) for x in range(1, 101)
nim = [0, 0, 0, 1, 0, 2, 3, 4, 0] + range(5, 51)
t = int(raw_input())
for _ in range(t):
    n = int(raw_input())
    v = [nim[int(x)] for x in raw_input().split()]
    if reduce(lambda x, y: x^y, v) == 0:
        print 'BOB'
    else:
        print 'ALICE'
