fi = open('shufflegold.in')
fo = open('shufflegold.out', 'w')
n, m, q = map(int, fi.readline().split())
v = range(m)
for i in range(m):
    x = int(fi.readline())
    v[x-1] = i
v = v[1:]+v[:1]
rot = [None] * m
idx = [-1] * m
for i in range(m-1, -1, -1):
    if rot[i] is None:
        l = [v[i]]
        while v[l[-1]] != l[0]:
            l.append(v[l[-1]])
        for i, x in enumerate(l):
            rot[x] = l
            idx[x] = i
for _ in range(q):
    x = int(fi.readline())
    x = (n-x) + (m-1)
    if x >= n:
        x -= n
    if x >= m or rot[x][0] == rot[m-1][0]:
        l = rot[m-1]
        k = len(l)+n-m
        if x >= m:
            c = len(l)+x-m
        else:
            c = idx[x]
        c = (c+(n-m+1)) % k
        if c >= len(l):
            ans = m+(c-len(l))
        else:
            ans = l[c]
    else:
        l = rot[x]
        k = len(l)
        c = (idx[x]+(n-m+1)) % k
        ans = l[c]
    fo.write('%d\n' % (ans+1))
fi.close()
fo.close()
