#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <algorithm>
using namespace std;
#define N 300100

template<typename T>
class Comparator {
    public:
        Comparator(const T *v) : m_v(v) {};
        bool operator() (const int &a, const int &b) {
            return m_v[a] < m_v[b];
        }
    private:
        const T *m_v;
};

struct Node {
    int x, y, v, ypre;
    bool operator<(const Node &o) const {
        return (x < o.x || (x == o.x && y < o.y));
    }
};

inline int lsb(unsigned x)
{
    const int ilsb[] = {4, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};
    int c = 0;
    if(!(x & 0xffff)) {
        c += 16; 
        x >>= 16;
    }
    if(!(x & 0xff)) {
        c += 8;
        x >>= 8;
    }
    if(!(x & 0xf)) {
        c += 4;
        x >>= 4;
    }
    c += ilsb[x&0xf];
    return c;
}
class SuffixArray {
    public:
        SuffixArray(const char v[], int len) : m_v(v), m_len(len) {
            m_sa = m_rank = m_lcp = NULL;
            m_nodes = NULL;
            m_nNodes = 0;
            construct_sa();
            compute_lcp();
        }

        ~SuffixArray() {
            if(m_sa) {
                delete[] m_sa;
                m_sa = NULL;
            }
            if(m_rank) {
                delete[] m_rank;
                m_rank = NULL;
            }
            if(m_lcp) {
                delete[] m_lcp;
                m_lcp = NULL;
            }
            if(m_nodes) {
                delete[] m_nodes;
                m_nodes= NULL;
            }
#if 1
            if(m_iv) {
                delete[] m_iv;
                m_iv = NULL;
            }
#endif
        }
        inline void add_node(int x, int y, int v, int ypre) {
            m_nodes[m_nNodes].x = x;
            m_nodes[m_nNodes].y = y;
            m_nodes[m_nNodes].ypre = ypre;
            m_nodes[m_nNodes++].v = v;
        }
        void count(int nV, const long long w0[], long long w[])
        {
            int *b = new int[2*m_len];
            int *bp = new int[2*m_len];
            int *sc = new int[2*m_len];
            m_nodes = new Node[2*m_len];
            m_nNodes = 0;
            int top = -1;
            for(int i = 0; i < nV; i++)
                w[i] = 0;
            b[++top] = 0; bp[top] = 0; sc[top] = -1;
            int c = m_len - m_sa[0], vc = 0;
            for(int i = 1; i < m_len; i++) {
                int x = m_lcp[i];
                while(top >= 0 && b[top] > x) {
                    // complete branches
                    int p = b[top];
                    // Update p from c
                    if(p != c) {
                        add_node(bp[top], c, vc, p);
                        // update pos c
                        w[vc] += w0[vc];
                        // score of child of p along p..c
                        if(!((p+c)&1))
                            vc = !vc;
                        else if(c-p != 1)
                            vc = !!vc;
                        sc[top] &= ~(1<<vc);
                        // update pos p+1..c-1
                        w[!vc] += (long long)(c-p-1)/2*(long long)w0[!vc];
                        w[!!vc] += (long long)(c-p)/2*(long long)w0[!!vc];
                    }
                    c = p; vc = lsb(sc[top]);
                    --top;
                }
                if(top < 0 || b[top] < x) {
                    // new branch at x
                    b[++top] = x; bp[top] = bp[top-1]; sc[top] = -1;
                }
                // Update x from c
                if(x != c) {
                    add_node(bp[top], c, vc, x);
                    // update pos c
                    w[vc] += w0[vc];
                    // score of child of x along x..c
                    if(!((x+c)&1))
                        vc = !vc;
                    else if(c-x != 1)
                        vc = !!vc;
                    sc[top] &= ~(1<<vc);
                    // update pos x+1..c-1
                    w[!vc] += (long long)(c-x-1)/2*(long long)w0[!vc];
                    w[!!vc] += (long long)(c-x)/2*(long long)w0[!!vc];
                }
                // fwd to sa[i]
                c = m_len - m_sa[i];
                bp[top] = i;
                if(c != b[top])
                    vc = 0;
            }
            while(top >= 0) {
                // complete branches
                int p = b[top];
                // Update p from c
                if(p != c) {
                    add_node(bp[top], c, vc, p);
                    // update pos c
                    w[vc] += w0[vc];
                    // score of child of p along p..c
                    if(!((p+c)&1))
                        vc = !vc;
                    else if(c-p != 1)
                        vc = !!vc;
                    sc[top] &= ~(1<<vc);
                    // update pos p+1..c-1
                    w[!vc] += (long long)(c-p-1)/2*(long long)w0[!vc];
                    w[!!vc] += (long long)(c-p)/2*(long long)w0[!!vc];
                }
                c = p; vc = lsb(sc[top]);
                --top;
            }
            assert(c == 0);
            add_node(0, c, vc, -1);
            w[vc] += w0[vc];
            delete[] b;
            delete[] bp;
            delete[] sc;
            sort(m_nodes, m_nodes + m_nNodes);
            //for(int i = 0; i < m_nNodes; i++)
            //    printf("%d %d %d %.*s\n", m_nodes[i].x, m_nodes[i].y, m_nodes[i].v, m_nodes[i].y, m_v + m_sa[m_nodes[i].x]);
        }
        long long kth(int nV, const long long w[], long long k, int &rx, int &ry, int &rv) {
            assert(m_nodes);
            assert(m_nodes[0].x == 0 && m_nodes[0].y == 0);
            int *stk = new int[m_nNodes];
            int top = -1;
            if(k <= w[m_nodes[0].v]) {
                rx = m_sa[m_nodes[0].x];
                ry = m_nodes[0].y;
                rv = m_nodes[0].v;
                delete[] stk;
                //printf("%d %d %d %.*s\n", m_nodes[0].x, m_nodes[0].y, m_nodes[0].v, ry, m_v + rx);
                return k;
            }
            k -= w[m_nodes[0].v];
            stk[++top] = 0;
            for(int i = 1; i < m_nNodes; i++) {
                while(top >= 0 && m_nodes[stk[top]].y > m_nodes[i].ypre) 
                    --top;
                int p = stk[top];
                long long tot = w[m_nodes[i].v] + (long long)(m_nodes[i].y - m_nodes[p].y)/2*(long long)w[!m_nodes[i].v] + (long long)(m_nodes[i].y - m_nodes[p].y - 1)/2*(long long)w[!!m_nodes[i].v];
                if(k <= tot) {
                    rx = m_sa[m_nodes[i].x];
                    if(k > tot - w[m_nodes[i].v]) {
                        k -= tot - w[m_nodes[i].v];
                        ry = m_nodes[i].y;
                        rv = m_nodes[i].v;
                    } else {
                        long long ck = k / (w[0]+w[1]);
                        k %= (w[0]+w[1]);
                        if(k == 0) {
                            --ck;
                            k = w[0] + w[1];
                        }
                        ry = m_nodes[p].y + 2*ck + 1;
                        rv = (((m_nodes[p].y+m_nodes[i].y)&1) ? !!m_nodes[i].v : !m_nodes[i].v);
                        if(k > w[rv]) {
                            k -= w[rv];
                            ++ry;
                            rv = !rv;
                        }
                    }
                    delete[] stk;
                    //printf("%d %d %d %.*s\n", m_nodes[i].x, m_nodes[i].y, m_nodes[i].v, ry, m_v + rx);
                    return k;
                }
                stk[++top] = i;
                k -= tot;
            }
            rx = ry = rv = -1;
            delete[] stk;
            return -1;
        }
    private:
        inline bool eq(int x, int y, int P1[], int len1, int S[]) {
            if(x == y)
                return true;
            else if(x >= len1-1 || y >= len1-1 || P1[x+1]-P1[x] != P1[y+1]-P1[y])
                return false;
            for(int i = P1[x], j = P1[y]; i <= P1[x+1]; i++, j++)
                if(S[i] != S[j])
                    return false;
            return true;
        }
        void SA_IS(int S[], int len, int vmax, int SA[]) {
            // S is the input string;
            // SA is the output suffix array of S;
            assert(len > 0 && S[len-1] == 0);
            // 1. Scan S once to classify all the characters as L- or S-type into t;
            bool *t = new bool[len];
            t[len-1] = true;
            for(int i = len-2; i >= 0; i--)
                t[i] = (S[i]<S[i+1] || (S[i]==S[i+1] && t[i+1]));
            // 2. Scan t to find all the LMS-substrings in S into P1;
            int *P1 = new int[len];
            int *iP1 = new int[len];
            int *B = new int[vmax];
            int *nxtB = new int[len];
            int len1 = 0;
            iP1[0] = -1;
            fill(B, B+vmax, -1);
            for(int i = 1; i < len; i++)
                if(!t[i-1] && t[i]) {
                    iP1[i] = len1;
                    nxtB[len1] = B[S[i]];
                    B[S[i]] = len1;
                    P1[len1++] = i;
                } else
                    iP1[i] = -1;
            // 3. Induced sort all the LMS-substrings
            int *SA1 = new int[len1];
            for(int i = 0, k = 0; i < vmax; i++)
                for(int j = B[i]; j >= 0; j = nxtB[j])
                    SA1[k++] = j;
            delete[] B;
            delete[] nxtB;
            B = nxtB = NULL;
            induce(SA, len, SA1, len1, P1, S, vmax, t);
            int *S1 = new int[len1];
            int j = 0, vm1 = -1;
            for(int i = 0, lst = -1; i < len; i++) {
                assert(SA[i] >= 0 && SA[i] < len);
                if(iP1[SA[i]] >= 0) {
                    if(lst < 0 || !eq(lst, iP1[SA[i]], P1, len1, S))
                        ++vm1;
                    S1[iP1[SA[i]]] = vm1;
                    lst = iP1[SA[i]];
                }
            }
            ++vm1;
            delete[] iP1;
            iP1 = NULL;
            // 4. Directly compute SA1 from S1 if all unique; recusively otherwise.
            if(vm1 < len1) {
                SA_IS(S1, len1, vm1, SA1);
            } else {
                int *B = new int[vm1];
                int *nxtB = new int[len1];
                fill(B, B+vm1, -1);
                for(int i = 0; i < len1; i++) {
                    nxtB[i] = B[S1[i]];
                    B[S1[i]] = i;
                }
                for(int i = 0, k = 0; i < vm1; i++)
                    for(int j = B[i]; j >= 0; j = nxtB[j])
                        SA1[k++] = j;
                delete[] B;
                delete[] nxtB;
                B = nxtB = NULL;
            }
            delete[] S1;
            S1 = NULL;
            // 6. Induce SA from SA1.
            induce(SA, len, SA1, len1, P1, S, vmax, t);
            delete[] P1;
            delete[] t;
            delete[] SA1;
            P1 = NULL;
            t = NULL;
            SA1 = NULL;
        }
        // LMS (length len1) order in SA1, pointer in P
        // Result in SA1
        inline void induce(int SA[], int len, int SA1[], int len1, int P[], int S[], int vmax, bool t[])
        {
            int (*Bp)[2] = new int[vmax][2];
            fill((int *)Bp, (int *)(Bp+vmax), 0);
            for(int i = 0; i < len; i++)
                Bp[S[i]][1]++;
            Bp[0][1]--;
            for(int i = 1; i < vmax; i++) {
                Bp[i][0] += Bp[i-1][1]+1;
                Bp[i][1] += Bp[i-1][1];
            }
            assert(Bp[vmax-1][1] == len-1);
            fill(SA, SA+len, -1);
            // 1. Find the end of each S-type bucket;
            //    put all the items of SA1 into their corresponding S-type buckets in SA,
            //    with their relative orders unchanged as that in SA1;
            for(int i = len1-1; i >= 0; i--)
                SA[Bp[S[P[SA1[i]]]][1]--] = P[SA1[i]];
            for(int i = 0; i < vmax-1; i++)
                Bp[i][1] = Bp[i+1][0]-1;
            Bp[vmax-1][1] = len-1;
            // 2. Find the head of each L-type bucket in SA;
            //    scan SA from head to end, for each item SA[i], 
            //    if S[SA[i]-1] is L-type, put SA[i]-1 to the current head of the L-type bucket for S[SA[i]-1] and forward the current head one item to the right.
            for(int i = 0; i < len; i++) 
                if(SA[i] > 0 && !t[SA[i]-1])
                    SA[Bp[S[SA[i]-1]][0]++] = SA[i]-1;
            // 3. Find the end of each S-type bucket in SA;
            //    scan SA from end to head, for each item SA[i], 
            //    if S[SA[i]-1] is S-type, put SA[i]-1 to the current end of the S-type bucket for S[SA[i]-1] and forward the current end one item to the left.
            for(int i = len-1; i >= 0; i--)
                if(SA[i] > 0 && t[SA[i]-1])
                    SA[Bp[S[SA[i]-1]][1]--] = SA[i]-1;
            delete[] Bp;
        }
        void construct_sa()
        {
            m_sa = new int[m_len+1];
            m_rank = new int[m_len+1];
#if 1
            m_iv = new int[m_len+1];
            for(int i = 0; i < m_len; i++)
                m_iv[i] = (m_v[i] - 'a'+1);
            m_iv[m_len] = 0;
            SA_IS(m_iv, m_len+1, 27, m_sa);
            assert(m_sa[0] == m_len);
            for(int i = 1; i <= m_len; i++) {
                assert(m_sa[i] >= 0 && m_sa[i] < m_len);
                m_sa[i-1] = m_sa[i];
                m_rank[m_sa[i]] = i;
            }
#else
            for(int i = 0; i < m_len; i++)
                m_sa[i] = i;
            Comparator<char> com(m_v);
            std::sort(m_sa, m_sa + m_len, com);
            m_rank[m_sa[0]] = 1;
            for(int i = 1; i < m_len; i++)
                m_rank[m_sa[i]] = (m_v[m_sa[i]] == m_v[m_sa[i-1]] ? m_rank[m_sa[i-1]] : m_rank[m_sa[i-1]]+1);

            int *buckets = new int[m_len+1];
            int *bucnext = new int[m_len];
            int *rank2 = new int[m_len];
            for(int k = 1; k < m_len; k <<= 1) {
                std::fill(buckets, buckets+m_len+1, -1);
                int rm = m_rank[m_sa[m_len-1]];
                for(int i = m_len - 1; i >= 0; i--) {
                    int j = (m_sa[i] + k < m_len ? m_rank[m_sa[i]+k] : 0);
                    bucnext[m_sa[i]] = buckets[j];
                    buckets[j] = m_sa[i];
                }
                for(int i = 0, j = 0; i <= rm; i++)
                    for(int x = buckets[i]; x >= 0; x = bucnext[x])
                        m_sa[j++] = x;
                std::fill(buckets, buckets+m_len+1, -1);
                for(int i = m_len - 1; i >= 0; i--) {
                    int j = m_rank[m_sa[i]];
                    bucnext[m_sa[i]] = buckets[j];
                    buckets[j] = m_sa[i];
                }
                for(int i = 0, j = 0; i <= rm; i++)
                    for(int x = buckets[i]; x >= 0; x = bucnext[x])
                        m_sa[j++] = x;
                rank2[m_sa[0]] = 1;
                for(int i = 1; i < m_len; i++)
                    rank2[m_sa[i]] = (m_rank[m_sa[i]] == m_rank[m_sa[i-1]] && m_sa[i]+k < m_len && m_sa[i-1]+k < m_len && m_rank[m_sa[i]+k] == m_rank[m_sa[i-1]+k] ? rank2[m_sa[i-1]] : rank2[m_sa[i-1]]+1);
                int *pt = m_rank; m_rank = rank2; rank2 = pt;
            }
            /* for(int i = 0; i < m_len; i++) {
                assert(m_rank[m_sa[i]] == i+1);
                assert(m_sa[m_rank[i]-1] == i);
            } */
            delete[] buckets;
            delete[] bucnext;
            delete[] rank2;
#endif
            //for(int i = 0; i < m_len; i++)
            //    printf("sa %d %d\n", i, m_sa[i]);
        }
        void compute_lcp()
        {
            m_lcp = new int[m_len];
            int j;
            int x = m_rank[0]-1;
            if(x == 0)
                m_lcp[x] = 0;
            else {
                for(j = 0; m_sa[x]+j < m_len && m_sa[x-1]+j < m_len && m_v[m_sa[x]+j] == m_v[m_sa[x-1]+j]; j++);
                m_lcp[x] = j;
            }
            for(int i = 1; i < m_len; i++) {
                x = m_rank[i]-1;
                if(x == 0 || m_v[m_sa[x]] != m_v[m_sa[x-1]])
                    m_lcp[x] = 0;
                else {
                    int y = m_rank[m_sa[x]-1]-1;
                    for(j = std::max(m_lcp[y]-1, 0); m_sa[x]+j < m_len && m_sa[x-1]+j < m_len && m_v[m_sa[x]+j] == m_v[m_sa[x-1]+j]; j++);
                    m_lcp[x] = j;
                }
            }
            //for(int i = 0; i < m_len; i++)
            //    printf("lcp %d %d\n", i, m_lcp[i]);
        }
        const char *m_v;
#if 1
        int *m_iv;
#endif
        int  m_len;
        int *m_sa, *m_rank;
        int *m_lcp;
        Node *m_nodes;
        int m_nNodes;
};

#define K 30
int n, m;
long long k;
char a[N], b[N];

int main()
{
    int t;
    scanf("%d%d%lld%s%s", &n, &m, &k, a, b);
    SuffixArray sa(a, n), sb(b, m);
    long long w0[K], wa[K], wb[K];
    for(int i = 0; i < K; i++)
        w0[i] = 1;
    sb.count(K, w0, wb);
    long long cb = 0;
    for(int i = 0; i < K; i++)
        cb += wb[i];
    for(int i = 0; i < K; i++)
        wb[i] = cb - wb[i];
    sa.count(K, wb, wa);
    int ax, ay, av, bx, by, bv;
    k = sa.kth(K, wb, k, ax, ay, av);
    if(k < 0)
        printf("no solution\n");
    else {
        w0[av] = 0;
        k = sb.kth(K, w0, k, bx, by, bv);
        assert(k == 1);
        printf("%.*s\n%.*s\n", ay, a+ax, by, b+bx);
        //printf("%d \"%.*s\" %d \"%.*s\"\n", ay, ay, a+ax, by, by, b+bx);
    }
    return 0;
}

