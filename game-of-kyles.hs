import Data.Bits
import Data.List
import Data.Array

n :: Int
n = 500

mex :: [Int]->Int
mex cs = _mex cs 0
  where _mex cs x | elem x cs = _mex cs (x+1)
                  | otherwise = x

bowling :: Array Int Int
bowling = array (0, n) ([(0, 0)] ++ [(k, mex([xor (bowling!x) (bowling!(k-1-x)) | x<-[0..k-1]] ++ [xor (bowling!x) (bowling!(k-2-x)) | x<-[0..k-2]])) | k<-[1..n]])

play :: String->Int
play s = foldl1 xor (map (\x->bowling!(length x)) (filter (\x->(x!!0)=='I') (group s)))

printResult :: Int->IO()
printResult 0 = putStrLn "LOSE"
printResult x = putStrLn "WIN"

process :: Int->IO()
process 0 = return()
process n = getLine >> getLine >>= (printResult.play) >> process (n-1)

main :: IO()
main = getLine >>= (return.read) >>= process
