import Data.List
import Data.Array
import Data.Maybe

substr :: Array Int Char->Int->Int->String
substr sa i j | j1 < i = ""
              | otherwise = elems $ ixmap (i,j1) id sa
    where j1 = min (j-1) (snd $ bounds sa)

pass_list :: Array Int String -> Int -> [String] -> [String]
pass_list arr 0 r = r
pass_list arr n r = pass_list arr (n-(length (arr!n))) ((arr!n):r)

tryLogin :: [String] -> String -> String
tryLogin ws s | arr!n == "" = "WRONG PASSWORD" -- ++ (show arr)
              | otherwise = unwords $ pass_list arr n []
  where n = length s
        sa = listArray (0, n-1) s
        arr = array  (0,n) [(i, substrMatch ws s i) | i<-[0..n]]
        substrMatch ws s 0 = ""
        substrMatch [] s k = ""
        substrMatch (w:ws) s k | (k == length w || (k > length w && arr!(k-(length w)) /= "")) && (substr sa (k-(length w)) k) == w = w
                               | otherwise = substrMatch ws s k

process :: Int->IO()
process 0 = return()
process t = do
  s <- getLine
  --let n = read s
  s <- getLine
  let ws = words s
  pas <- getLine
  putStrLn $ tryLogin ws pas
  process (t-1)

main :: IO()
main = do
  s <- getLine
  let t = read s
  process t
