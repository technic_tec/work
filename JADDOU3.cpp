/*
 * =====================================================================================
 *
 *       Filename:  JADDOU3.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年04月12日 11时58分24秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000
#define P 123456789

// 3, 10 , 36 , 136 ,528
// an = 2^(2n-1) + 2^(n-1)
int power_mod(int n, long long k, int p)
{
    int r = 1;
    while(k) {
        if(k & 1)
            r = (int)((long long)r * (long long)n % p);
        n = (int)((long long)n * (long long)n % p);
        k >>= 1;
    }
    return r;
}
int main()
{
    long long n;
    scanf("%lld", &n);
    int r = power_mod(2, 2*n-1, P);
    r += power_mod(2, n-1, P);
    if(r >= P) r -= P;
    printf("%d\n", r);
    return 0;
}

