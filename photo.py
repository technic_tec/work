from collections import deque
from random import randint

def gen(N, M):
    r = list()
    for _ in range(N):
        a = randint(1, M)
        b = randint(1, M)
        r.append((min(a, b), max(a, b)))
    return r

r = gen(100000, 200000)
r.sort(key = lambda x: (x[0], -x[1]))
src = deque()
sink= deque()
src.append(r[0])
sink.append(r[0])
for x in r[1:]:
    if x[1] > src[-1][1]:
        src.append(x)
    while len(sink) > 0 and x[1] <= sink[-1][1]:
        sink.pop()
    sink.append(x)
for x in src:
    xs, xt = x
    while len(sink) > 0 and sink[0][0] >= x[0] and sink[0][1] <= x[1]:
        ys, yt = sink.popleft()
        xs, xt = max(xs, ys), min(xt, yt)
    if xs > xt:
        return None
    if 
    sink.pushleft((xs, xt))
