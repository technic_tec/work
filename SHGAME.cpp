#include <stdio.h>
#include <string.h>
#define N (1<<20)

int w, h, cw[N], ch[N], nw, nh;
int split(int m, int c[])
{
    int n = m+1;
    while(n & (n-1))
        n += n&(-n);
    memset(c, 0, n*sizeof(int));
    for(int i = 0, j = m; i < j; i++, j--)
        c[i^j] += 2;
    if(!(m & 1)) c[0]++;
    return n;
}

int main()
{
    int t;
    scanf("%d",&t);
    while(t--) {
        scanf("%d%d", &w, &h);
        long long cnt = (long long)(w)*(long long)(h);
        if((w+h)&1) {
            printf("%lld\n", cnt);
            continue;
        }
        nw = split(w-1, cw);
        nh = split(h-1, ch);
        for(int i = 0; i < nw && i < nh; i++)
            cnt -= (long long)cw[i] * (long long)ch[i];
        printf("%lld\n", cnt);
    }
    return 0;
}

