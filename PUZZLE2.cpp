#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#define N 200
#define INF 999999

int nu, nv, adj[N], dst[N], nxt[N], ne;
int Pair_G1[N], Pair_G2[N], Dist[N], NIL;

int hk_init(int _nu, int _nv)
{
    nu = _nu; nv = _nv; ne = 0;
    for(int i = 0; i < nu; i++)
        adj[i] = -1;
    NIL = nu;
    adj[NIL] = -1;
}

int add_edge(int _s, int _t)
{
    dst[ne] = _t;
    nxt[ne] = adj[_s];
    adj[_s] = ne++;
}

int Q[N], hd, tl;
bool BFS()
{
    hd = tl = 0;
    for(int v = 0; v < nu; v++) {
        if(Pair_G1[v] == NIL) {
            Dist[v] = 0;
            Q[tl++] = v;
        } else {
            Dist[v] = INF;
        }
    }
    Dist[NIL] = INF;
    while (hd < tl) {
        int v = Q[hd++];
        if(Dist[v] < Dist[NIL])
            for(int e = adj[v]; e >= 0; e = nxt[e]) {
                int u = dst[e];
                if(Dist[ Pair_G2[u] ] == INF) {
                    Dist[ Pair_G2[u] ] = Dist[v] + 1;
                    Q[tl++] = Pair_G2[u];
                }
            }
    }
    return (Dist[NIL] != INF);
}

bool DFS(int v)
{
    if(v != NIL) {
        for(int e = adj[v]; e >= 0; e = nxt[e]) {
            int u = dst[e];
            if (Dist[ Pair_G2[u] ] == Dist[v] + 1) {
                if(DFS(Pair_G2[u])) {
                    Pair_G2[u] = v;
                    Pair_G1[v] = u;
                    return true;
                }
            }
        }
        Dist[v] = INF;
        return false;
    }
    return true;
}

int hk_match()
{
    for(int i = 0; i < nu; i++)
        if(adj[i] < 0)
            return -1;
    for(int i = 0; i < nu; i++)
        Pair_G1[i] = NIL;
    for(int i = 0; i < nv; i++)
        Pair_G2[i] = NIL;
    int matching = 0;
    while (BFS())
        for(int v = 0; v < nu; v++)
            if(Pair_G1[v] == NIL)
                if (DFS(v))
                    matching++;
    return matching;
}

int r, c;
struct Column {
    int v[4];
    Column(int v0 = 0, int v1 = 0, int v2 = 0, int v3 = 0) {
        v[0] = v0; v[1] = v1; v[2] = v2; v[3] = v3;
    }
    void operator^=(const Column &x) {
        v[0] ^= x.v[0]; v[1] ^= x.v[1]; v[2] ^= x.v[2]; v[3] ^= x.v[3];
    }
} bs[N], bt[N];
bool operator<(const Column &x, const Column &y)
{
    return (x.v[3] < y.v[3])
        || (x.v[3] == y.v[3] && x.v[2] < y.v[2])
        || (x.v[3] == y.v[3] && x.v[2] == y.v[2] && x.v[1] < y.v[1])
        || (x.v[3] == y.v[3] && x.v[2] == y.v[2] && x.v[1] == y.v[1] && x.v[0] < y.v[0]);
}
Column operator^(const Column &x, const Column &y)
{
    return Column(x.v[0]^y.v[0], x.v[1]^y.v[1], x.v[2]^y.v[2], x.v[3]^y.v[3]);
}

// Convert color matrix from original to target.
// Op1. inverse whole row.
// Op2. swap 2 columns

#if 0
struct Edge {
    int s, t;
    int key[4];
} e[N*N];
int m;

int compE(const void *a, const void *b)
{
    if(((Edge*)a)->key[3] != ((Edge*)b)->key[3])
        return ((Edge*)a)->key[3] - ((Edge*)b)->key[3];
    if(((Edge*)a)->key[2] != ((Edge*)b)->key[2])
        return ((Edge*)a)->key[2] - ((Edge*)b)->key[2];
    if(((Edge*)a)->key[1] != ((Edge*)b)->key[1])
        return ((Edge*)a)->key[1] - ((Edge*)b)->key[1];
    return ((Edge*)a)->key[0] - ((Edge*)b)->key[0];
}

bool valid(int bs[][4], int bt[][4], int r, int c)
{
    m = 0;
    for(int i = 0; i < c; i++)
        for(int j = 0; j < c; j++) {
            e[m].key[0] = (bs[i][0]^bt[j][0]);
            e[m].key[1] = (bs[i][1]^bt[j][1]);
            e[m].key[2] = (bs[i][2]^bt[j][2]);
            e[m].key[3] = (bs[i][3]^bt[j][3]);
            e[m].s = i; e[m++].t = j;
        }
    qsort(e, m, sizeof(Edge), compE);
    for(int i = 0; i < m; i++) {
        int j = i;
        while(i+1 < m && compE(&e[i+1], &e[i])==0)
            ++i;
        if(i-j+1 < c) continue;
        hk_init(c, c);
        for(; j <= i; j++)
            add_edge(e[j].s, e[j].t);
        if(hk_match() == c)
            return true;
    }
    return false;
}
#endif

bool valid(Column bs[], Column bt[], int r, int c)
{
    int key[4];
    std::sort(bt, bt+c);
    Column bx = bs[0];
    for(int i = 0; i < c; i++) {
        Column key = bx ^ bt[i];
        for(int j = 0; j < c; j++)
            bs[j] ^= key;
        bx ^= key;
        std::sort(bs, bs+c);
        bool done = true;
        for(int j = 0; j < c; j++)
            if(bs[j] < bt[j] || bt[j] < bs[j]) {
                done = false;
                break;
            }
        if(done) return true;
    }
    return false;
}
int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    scanf("%d%d", &r, &c);
    memset(bs, 0, sizeof(bs));
    for(int i = 0; i < r; i++)
      for(int j = 0; j < c; j++) {
        char col[10];
        scanf("%s", col);
        bs[j].v[i>>5] |= ((col[0]=='B'?1:0) << (i&31));
      }
    memset(bt, 0, sizeof(bt));
    for(int i = 0; i < r; i++)
      for(int j = 0; j < c; j++) {
        char col[10];
        scanf("%s", col);
        bt[j].v[i>>5] |= ((col[0]=='B'?1:0) << (i&31));
      }
    printf(valid(bs, bt, r, c) ? "YES\n" : "NO\n");
  }
  return 0;
}
