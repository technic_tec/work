class Stern(object):
  @staticmethod
  def maximal(n):
    s = [(1, 1), (2, 3), (3, 5)]
    lll, ll, l, c = 0, 1, 2, 3
    while s[-1][1] <= n:
      ii, i, j = lll, ll, l
      while (ii < ll or i < l) and j < c and s[-1][1] <= n:
        if min(4*s[ii][1], 2*s[i][1]) < s[j][1]:
          if 2*s[i][1]+1 == s[j][1] and s[i][0]+s[j][0] > s[-1][0]:
            s.append((s[i][0]+s[j][0], 2*s[j][1]-1))
          elif 4*s[ii][1]+1 == s[j][1] and s[ii][0]+s[j][0] > s[-1][0]:
            s.append((s[ii][0]+s[j][0], 2*s[j][1]-1))
          if 4*s[ii][1] < 2*s[i][1]:
            ii += 1
          else:
            i += 1
        else:
          if 2*s[i][1]-1 == s[j][1] and s[i][0]+s[j][0] > s[-1][0]:
            s.append((s[i][0]+s[j][0], 2*s[j][1]+1))
          elif 4*s[ii][1]+1 == s[j][1] and s[ii][0]+s[j][0] > s[-1][0]:
            s.append((s[ii][0]+s[j][0], 2*s[j][1]+1))
          j += 1
      lll, ll, l, c = ll, l, c, len(s)
    return s

print Stern.maximal(10**18)
