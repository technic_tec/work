#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <deque>
#include <vector>
using namespace std;

#define N 10
#define INF 99999999

int nu;
vector<int> adj, dst, nxt;
char color[3][N][N];
int id[26][2];
vector<char> cl;

void init(int _nu)
{
    nu = _nu;
    adj.clear();
    for(int i = 0; i < nu; i++)
        adj.push_back(-1);
    dst.clear();
    nxt.clear();
}

int add_edge(int _s, int _t)
{
	assert(_s < nu && _t < nu);
    int ne = dst.size();
    dst.push_back(_t);
    nxt.push_back(adj[_s]);
    adj[_s] = ne;
    ne = dst.size();
    dst.push_back(_s);
    nxt.push_back(adj[_t]);
    adj[_t] = ne;
}


int main()
{
	int n;
	scanf("%d", &n);
	assert(n < N);
    init(3*n*n);
	for(int f = 0; f < 3; f++)
		for(int x = 0; x < n; x++)
			for(int y = 0; y < n; y++) {
				int v = (f*n+x)*n+y;
				if(x > 0) {
					int w = v-n;
					add_edge(v, w);
				} else if(f != 0) {
					int w = (f == 1 ? (n-1)*n + y : (n-y)*n-1);
					add_edge(v, w);
				}
				if(y > 0) {
					int w = v-1;
					add_edge(v, w);
				} else if(f == 2) {
					int w = v - n*(n-1) - 1;
					add_edge(v, w);
				}
			}
	int f, x, y;
	char c[5];
	memset(id, -1, sizeof(id));
	while(scanf("%d%d%d%s", &f, &x, &y, c) == 4) {
		if(c[0] >= 'a' && c[0] <= 'z')
			c[0] -= 32;
		assert(0 <= f && f <= 2 && 0 <= x && x < n && 0 <= y && y < n && c[0] >= 'A' && c[0] <= 'Z' && c[1] == 0);
		int px = (f*n+x)*n+y;
		if(id[c[0]-'A'][0] < 0) {
			id[c[0]-'A'][0] = px;
			cl.push_back(c[0])
		} else {
			assert(id[c[0]-'A'][1] < 0);
			id[c[0]-'A'][1] = px;
		}
	}
    return 0;
}
