/*
 * =====================================================================================
 *
 *       Filename:  rblock.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年02月09日 17时40分42秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <set>
#include <vector>
#include <algorithm>
using namespace std;

#define N 100000
#define INF 1000000000

int n, m, ve[N], dst[N], wt[N], nxt[N], ne;
int ds[N], dt[N], preS[N], path[N];
bool intree[N];

inline void init_graph(int n, int m)
{
    ne = 0;
    memset(ve, -1, sizeof(ve));
    memset(nxt, 0, sizeof(nxt));
}

inline void add_edge(int s, int t, int w)
{
    dst[ne] = t;
    wt[ne] = w;
    nxt[ne] = ve[s];
    ve[s] = ne++;
}

void sp(int s, int d[], int pre[] = NULL)
{
    set<pair<int, int> > dis;

    fill(d, d+n+1, INF);
    d[s] = 0;
    if(pre) pre[s] = 0;
    dis.insert(pair<int, int>(d[s], s));
    while(!dis.empty()) {
        int x = (*dis.begin()).second;
        dis.erase(dis.begin());
        for(int e = ve[x]; e >= 0; e = nxt[e]) {
            int y = dst[e];
            if(d[y] > d[x] + wt[e]) {
                if(d[y] < INF)
                    dis.erase(dis.find(pair<int, int>(d[y], y)));
                d[y] = d[x] + wt[e];
                if(pre) pre[y] = x;
                dis.insert(pair<int, int>(d[y], y));
            }
        }
    }
}

int dsp(int s, int t, int ds[], int dt[], int preS[])
{
    int gdm = 0;
    set<pair<int, pair<int, int> > > dis;
    std::fill(intree, intree+n+1, false);
    int lp = 0;
    for(int j = t; j > 0; j = preS[j])
        path[lp++] = j;
    for(int i = --lp; i > 0; i--) {
        int x = path[i], y = path[i-1];
        vector<int> q;
        q.push_back(x);
        while(!q.empty()) {
            int xx = q.back();
            q.pop_back();
            intree[xx] = true;
            for(int e = ve[xx]; e >= 0; e = nxt[e]) {
                int yy = dst[e];
                if(intree[yy])
                    dis.erase(pair<int, pair<int, int> >(ds[yy]+dt[xx]+wt[e], pair<int, int>(yy, xx)));
                else {
                    if(xx != x || yy != y)
                        dis.insert(pair<int, pair<int, int> >(ds[xx]+dt[yy]+wt[e], pair<int, int>(xx, yy)));
                    if(preS[yy] == xx && yy != y)
                        q.push_back(yy);
                }
            }
        }
        if(!dis.empty()) {
            int dm = (*dis.begin()).first - dt[s];
            if(dm > ds[y] - ds[x])
                dm = ds[y] - ds[x];
            if(dm > gdm)
                gdm = dm;
        }
        dis.insert(pair<int, pair<int, int> >(ds[x]+dt[y]+(ds[y]-ds[x]), pair<int, int>(x, y)));
    }
    return gdm;
}

int main()
{
    freopen("rblock.in", "r", stdin);
    freopen("rblock.out", "w", stdout);
    scanf("%d%d", &n, &m);
    init_graph(n, m);
    for(int i = 0; i < m; i++) {
        int x, y, w;
        scanf("%d%d%d", &x, &y, &w);
        add_edge(x, y, w);
        add_edge(y, x, w);
    }
    sp(1, ds, preS);
    sp(n, dt);
    int dm = dsp(1, n, ds, dt, preS);
    printf("%d\n", dm);
    return 0;
}

