/*
 * =====================================================================================
 *
 *       Filename:  SEAGM2.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月08日 11时37分14秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009

int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    int n, m;
    scanf("%d%d", &n, &m);
    double s = 0.0;
    double p = 0.0;
    bool allzero = true;
    for(int i = 0; i < n; i++) {
      double x = 1.0;
      bool zero = false;
      for(int j = 0; j < m; j++) {
        double px;
        scanf("%lf", &px);
        x *= px;
        if(px < 1e-7)
          zero = true;
      }
      if(!zero) allzero = false;
      s += x;
      if(i == 0)
        p = x;
    }
    // sum(p[i][0]*p[i][1]*...*p[i][m], i=0..n)
    double res = 0.0;
    if(!allzero) res = p/s;
    printf("%.6lf\n", res);
  }
  return 0;
}


