#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

class LinkCutTree {
    struct Node {
        // left, right, parent -- splay tree of preferred path (solid edges)
        // path_pa -- path between preferred paths (dash edges)
        // key -- key of edge between current and parent (dash edge for path head, solid otherwise)
        int key;
        Node *left, *right, *parent, *path_pa;
        Node(int _key = -1) : left(NULL), right(NULL), parent(NULL), path_pa(NULL), key(_key) {}
        Node* & Left() { return left; }
        Node* & Right() { return right; }
    };
    public:
        LinkCutTree(int sz){
            m_sz = sz;
            m_nodes = new Node[m_sz];
        }
        ~LinkCutTree() {
            if(m_nodes) {
                delete[] m_nodes;
                m_nodes = NULL;
            }
        }
        int getRoot(int x) {
            Node *p = &m_nodes[x];
            access(p);
            while(p->Left())
                p = p->Left();
            splay(p);
            return p-m_nodes;
        }
        void cut(int x) {
            Node *p = &m_nodes[x];
            access(p);
            if(p->Left()) {
                p->Left()->parent = NULL;
                p->Left() = NULL;
            }
        }
        void cutEdge(int x) {
            Node *p = &m_nodes[x];
            assert(p->right);
            cut(p->right-m_nodes);
            cut(p-m_nodes);
        }
        void join(int x, int y)
        {
            assert(m_nodes[x].Left() == NULL);
            access(&m_nodes[x]);
            access(&m_nodes[y]);
            assert(m_nodes[y].parent == NULL);
            assert(m_nodes[y].path_pa == NULL);
            m_nodes[x].Left() = &m_nodes[y];
            m_nodes[y].parent = &m_nodes[x];
        }
        void join(int x, int y, int key, int eid) {
            Node *p = &m_nodes[eid];
            *p = Node(key);
            join(eid, y);
            join(x, eid);
        }
        void evert(int v);
        int getMin(int v);
    private:
        void access(Node *v)
        {
            Node *v0 = v;
            splay(v);
            if(v->Right()) {
                Node *u = v->Right();
                u->path_pa = v;
                u->parent = NULL;
                v->Right() = NULL;
            }
            while(v->path_pa != NULL) {
                Node *w = v->path_pa;
                splay(w);
                Node *u = w->Right();
                if(u) {
                    u->parent = NULL;
                    u->path_pa = w;
                }
                w->Right() = v;
                v->parent = w;
                v->path_pa = NULL;
                v = w;
            }
            splay(v);
        }
        void splay(Node *v) {
            while(v->parent && v->parent->parent) {
                Node *p = v->parent;
                Node *g = p->parent;
                if(p == g->Left()) {
                    if(v == p->Left()) {
                        //    g        v
                        //  p      ->    p
                        //v                g
                        p->Left() = v->Right();
                        if(p->Left())
                            p->Left()->parent = p;
                        g->Left() = p->Right();
                        if(g->Left())
                            g->Left()->parent = g;
                        v->Right() = p;
                        p->Right() = g;
                        v->parent = g->parent;
                        g->parent = p;
                        p->parent = v;
                    } else {
                        //    g          v
                        // p      ->   p   g
                        //  v
                        p->Right() = v->Left();
                        if(p->Right())
                            p->Right()->parent = p;
                        g->Left() = v->Right();
                        if(g->Left())
                            g->Left()->parent = g;
                        v->Left() = p;
                        v->Right() = g;
                        v->parent = g->parent;
                        g->parent = p->parent = v;
                    }
                } else {
                    if(v == p->Left()) {
                        //  g                v
                        //     p      ->   g   p
                        //   v
                        p->Left() = v->Right();
                        if(p->Left())
                            p->Left()->parent = p;
                        g->Right() = v->Left();
                        if(g->Right())
                            g->Right()->parent = g;
                        v->Right() = p;
                        v->Left() = g;
                        v->parent = g->parent;
                        g->parent = p->parent = v;
                    } else {
                        //  g                v
                        //    p      ->    p
                        //      v        g
                        p->Right() = v->Left();
                        if(p->Right())
                            p->Right()->parent = p;
                        g->Right() = p->Left();
                        if(g->Right())
                            g->Right()->parent = g;
                        v->Left() = p;
                        p->Left() = g;
                        v->parent = g->parent;
                        g->parent = p;
                        p->parent = v;
                    }
                }
                if(v->parent)
                    if(v->parent->Right() == g)
                        v->parent->Right() = v;
                    else
                        v->parent->Left() = v;
                else {
                    v->path_pa = g->path_pa;
                    g->path_pa = NULL;
                }
            }
            if(v->parent) {
                Node *p = v->parent;
                if(v == p->Left()) {
                    //   p
                    // v
                    p->Left() = v->Right();
                    if(p->Left())
                        p->Left()->parent = p;
                    v->Right() = p;
                    v->parent = p->parent;
                    p->parent = v;
                } else {
                    //   p
                    //     v
                    p->Right() = v->Left();
                    if(p->Right())
                        p->Right()->parent = p;
                    v->Left() = p;
                    v->parent = p->parent;
                    p->parent = v;
                }
                v->path_pa = p->path_pa;
                p->path_pa = NULL;
            }
        }

        int m_sz;
        Node *m_nodes;
};

class BIT {
    public:
        BIT(int n) {
            m_sz = n;
            while(m_sz & (m_sz-1))
                m_sz += m_sz & (-m_sz);
            m_dat = new int[m_sz];
            memset(m_dat, 0, m_sz*sizeof(int));
        }
        ~BIT() {
            delete[] m_dat;
            m_dat = NULL;
        }
        void inc(int x) {
            while(x < m_sz) {
                ++m_dat[x];
                x += x & (-x);
            }
        }
        int query(int x) {
            int s = 0;
            while(x > 0) {
                s += m_dat[x];
                x &= x-1;
            }
            return x;
        }
    private:
        int m_sz;
        int *m_dat;
};

#define N 201000
int n, m, q;
struct Edge {
    int id;
    int life;
} e[N];
struct Quest {
    int id;
    int u, v;
    int res;
} que[N];

int compElife(const void *a, const void *b) {
    return ((Edge*)a)->life - ((Edge*)b)->life;
}
int compQR(const void *a, const void *b) {
    return ((Quest*)a)->v - ((Quest*)b)->v;
}
int compQid(const void *a, const void *b) {
    return ((Quest*)a)->id - ((Quest*)b)->id;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d%d", &n, &m, &q);
        LinkCutTree lct(n+m+1);
        for(int i = 1; i <= m; i++) {
            int u, v;
            scanf("%d%d", &u, &v);
            if(u == v) {
                e[i].id = i;
                e[i].life = i;
                continue;
            }
            lct.evert(u);
            if(lct.getRoot(v) == u) {
                int x = lct.getMin(v);
                e[x-n].life = i;
                lct.cutEdge(x);
            }
            e[i].id = i;
            e[i].life = m+1;
            lct.join(u, v, i, n+i);
        }
        qsort(e, m, sizeof(Edge), compElife);
        for(int i = 0; i < q; i++) {
            scanf("%d%d", &que[i].u, &que[i].v);
            que[i].id = i;
        }
        qsort(que, q, sizeof(Quest), compQR);
        BIT bt(m+1);
        int m1 = m;
        for(int i = q-1; i >= 0; i--) {
            while(m1 > 0 && e[m1].life > que[i].v)
                bt.inc(m1--);
            int cnt = bt.query(que[i].v);
            if(que[i].u > 1)
                cnt -= bt.query(que[i].u-1);
            que[i].res = n-cnt;
        }
        qsort(que, q, sizeof(Quest), compQid);
        for(int i = 0; i < q; i++)
            printf("%d\n", que[i].res);
    }
    return 0;
}

