main :: IO()
main = do
  s <- getLine
  --let n = read s
  s <- getLine
  let v = map read $ words s
  let lcm = \x y->div (x*y) (gcd x y)
  putStrLn $ show $ foldl1 lcm v
