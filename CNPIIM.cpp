/*
 * =====================================================================================
 *
 *       Filename:  CNPIIM.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年04月14日 09时31分47秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 1562500

long long c[N+1];
int fac[N+1];

int main()
{
    memset(fac, 0, sizeof(fac));
    fac[1] = 1;
    for(int p = 2; p <= N; p++) {
        if(fac[p]) continue;
        fac[p] = p;
        if(p < 32768)
        for(int n = p*p; n <= N; n += p)
            if(!fac[n])
                fac[n] = p;
    }
    c[0] = 0;
    for(int nx = 1; nx <= N; nx++) {
        int cc = 1, n = nx;
        while(n > 1) {
            int f = fac[n], cf = 1;
            while(fac[n] == f) {
                n /= f;
                ++cf;
            }
            cc *= cf;
        }
        c[nx] = c[nx-1] + cc;
    }
    
    int t;
    scanf("%d", &t);
    while(t--) {
        int n;
        scanf("%d", &n);
        long long cnt = 0;
        for(int i = 1; i < n; i++)
            cnt += c[i*(n-i)-1];
        printf("%lld\n", cnt);
    }
    return 0;
}

