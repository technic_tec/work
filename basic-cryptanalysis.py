# Enter your code here. Read input from STDIN. Print output to STDOUT
from string import maketrans

def pattern(s):
    o = ''
    nc = 'A'
    mp = dict()
    for c in s:
        if c not in mp:
            mp[c] = nc
            nc = chr(ord(nc)+1)
        o += mp[c]
    return o

def crack(ws, pats):
    mp = dict()
    cand = dict()
    for w in ws:
        pat = pattern(w)
        cand[w] = set(pats[pat])
    for w in ws:
        com = common(cand[w])
        if com != '?'*len(w):
            for ch, cv in zip(com, w):
                if ch != '?':
                    mp[cv] = ch

f = open('dictionary.lst')
words = map(lambda x: x.lower(),f.read().split())
f.close()
pats = dict()
for w in words:
    pat = pattern(w)
    pats.setdefault(pat,[]).append(w)
print pats

s = sys.stdin.read()
ws = s.split()
trans = crack(ws, pats)
sys.stdout.write(s.translate(trans))
