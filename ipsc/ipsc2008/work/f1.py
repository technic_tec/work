f = open('../f1.in')
dat = map(int, f.read().split())
f.close()
m = min(dat)-10
s=''.join(chr(x-m) for x in dat)
print ''.join(x for x in s if x.isupper())
