import sys

LIMIT = 10

def play(ws, pre):
  choice = dict()
  k = len(pre)
  assert len(ws[0]) > k
  if k == 2*LIMIT-1:
    cand = [w[k] for w in ws if len(w) > k+1]
    if len(cand) > 0:
      choice[pre] = set(cand)
    return choice
  i = 0
  cand = set()
  while i < len(ws):
    c = ws[i][k]
    j = i
    while i < len(ws) and ws[i][k] == c:
      i += 1
    while j < i and len(ws[j]) == k+1:
      j += 1
    if j < i:
      choice.update(play(ws[j:i], pre+c))
      if pre+c not in choice:
        cand.add(c)
  if len(cand) > 0:
    choice[pre] = cand
  return choice

n = int(sys.stdin.readline())
words = [x.strip() for x in sys.stdin.readlines()]
assert(len(words) == n)
words.sort()
tr = play(words, '')
print tr['']
