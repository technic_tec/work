import matplotlib.pyplot as plt

def update(x, m):
  for k in m:
    x = x.replace(k, m[k])
  return x

def simp(x, y, m):
  x = update(x, m)
  y = update(y, m)
  if x == y:
    return ('', '')
  i, j = 0, -1
  while x[i] == y[i]:
    i += 1
  while x[j] == y[j]:
    j -= 1
  if j == -1:
    x, y = x[i:], y[i:]
  else:
    x, y = x[i:j+1], y[i:j+1]
  assert x != '' and y != ''
  if len(x) > len(y) or (len(x) == len(y) and x < y):
    x, y = y, x
  return (x, y)

f = open('../f2.in')
dat = [l.strip() for l in f.readlines()]
f.close()
sp = dat.index('---')
rules = set()
mapping = dict()
for x, y in zip(dat[:sp], dat[sp+1:]):
  x = x.replace('.', ' ').split()
  y = y.replace('.', ' ').split()
  for u, v in zip(x, y):
    x1, y1 = simp(u, v, mapping)
    if x1+y1 != '':
      rules.add((x1, y1))
newrules = set()
try:
  while len(rules) > 0:
    x, y = rules.pop()
    #print '(%s, %s) ->' % (x, y),  
    x, y = simp(x, y, mapping)
    #print '(%s, %s), mapping %s' % (x, y, str(mapping))
    if x + y == '':
      pass
    elif len(x) > 1:
      newrules.add((x, y))
    elif x in mapping:
      x, y = simp(y, mapping[x], mapping)
      if x + y != '':
        newrules.add((x, y))
    else:
      for k in mapping:
        mapping[k] = mapping[k].replace(x, y)
      mapping[x] = y
    if len(rules) == 0:
      rules = newrules
      newrules = set()
  #print sorted(mapping.items())
except KeyboardInterrupt:
  print mapping
  print rules|newrules
  raise

digits = dict(A='0', B='1', C='2', D='3', E='4', F='5', G='6', H='7', I='8', J='9')
sa = '\n'.join(update(update(l, mapping), digits) for l in dat[:sp])
sb = '\n'.join(update(update(l, mapping), digits) for l in dat[sp+1:])
assert(sa == sb)
#print sa
vx, vy = list(), list()
for l in sa.splitlines():
  x, y = map(float, l.split())
  vx.append(x)
  vy.append(y)
plt.plot(vx, [max(vy)+min(vy)-y for y in vy], ',')
plt.show()
# Show: Lorem ipsum
# Diff: draco dormiens nunquam titillandus
# Answer: Hogwarts
print 'draco dormiens nunquam titillandus'
print 'Hogwarts'.upper()
