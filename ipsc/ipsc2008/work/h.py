from PIL import Image
from pprint import pprint
from math import exp, isinf, floor
from copy import deepcopy
from scipy.fftpack import fft2, ifft2
import numpy as np
import sys

#for f in ['../h1.png', '../h2.png', '../H-ex1.png', '../H-ex2.png']:
#  print f
#  im = Image.open(f)
#  if im.mode == 'RGB':
#    print set(max([abs(r-g), abs(g-b), abs(b-r)]) for r, g, b in im.getdata())
#  elif im.mode == 'RGBA':
#    print set(max([abs(r-g), abs(g-b), abs(b-r)]) for r, g, b, a in im.getdata())
#  h = im.histogram()
#  print h[:256]

def maxi(s):
  return (min(min(x) for x in s), max(max(x) for x in s))

def count(s):
  cnt = [0]*256
  for r in s:
    for x in r:
      cnt[x] += 1
  return cnt

def filter(dat, mat, w, h, R):
  assert(len(dat[0]) == w+2*R and len(dat) == h+2*R)
  res = [[0.0]*(w+2*R) for _ in dat]
  for x in range(R, R+h):
    for y in range(R, R+w):
      for i in range(-R, R+1):
        for j in range(-R, R+1):
          res[x][y] += dat[x+i][y+j] * mat[i+R][j+R] 
  return res

def defilter(dat, mat, w, h, R):
  assert(len(dat[0]) == w+2*R and len(dat) == h+2*R)
  res = [[0.0]*(w+2*R) for _ in dat]
  for x in range(R, R+h):
    for y in range(R, R+w):
      for i in range(-R, R+1):
        for j in range(-R, R+1):
          res[x+i][y+j] += duat[x][y] * mat[i+R][j+R] 
  return res

def Richardson_Lucy_deconvolution(dat, mat, w, h, R, sigma):
  u = deepcopy(dat)
  U = [[max(0, min(255, int(u[R+x][R+y]+0.5))) for y in range(w)] for x in range(h)]
  print maxi(u), count(U)
  while True:
    v = filter(u, mat, w, h, R)
    for x in range(R, R+h):
      for y in range(R, R+w):
        v[x][y] = dat[x][y] / v[x][y]
    t = defilter(v, mat, w, h, r)
    for x in range(2*R+h):
      for y in range(2*R+w):
        u[x][y] *= t[x][y]
    U = [[max(0, min(255, int(u[R+x][R+y]+0.5))) for y in range(w)] for x in range(h)]
    print maxi(u), count(U)

def blur(dat, mat, w, h, R, sigma, sx = 0, sy = 0):
  assert(len(dat) == w*h)
  dat = np.pad(np.reshape(np.array(dat), (h, w)), ((R, R), (R, R)), 'constant', constant_values=((255, 255), (255, 255)))
  mat = np.pad(np.array(mat), ((0, h-1), (0, w-1)), 'constant', constant_values=((0, 0), (0, 0)))
  assert(dat.shape == mat.shape)
  fdat = fft2(dat)
  fmat = fft2(mat)
  fdat *= fmat
  res = ifft2(fdat)
  res = res[:h, :w].flatten().tolist()
  assert(max([abs(x.imag) for x in res]) < 1e-9)
  return res

def deblur(dat, mat, w, h, R, sigma, sx = 0, sy = 0):
  assert(len(dat) == w*h)
  dat0 = np.reshape(np.array(dat), (h, w))
  mat0 = np.array(mat)
  pad = mat0.copy()
  pad.fill(255.0)
  pad = fft2(pad)
  pad *= fft2(mat0)
  pad = ifft2(pad)
  assert(abs(pad.imag).max() < 1e-9)
  pad = pad.real
  def padding(x, y):
    r, c = dat0.shape
    if x < R:
      if y < R:
        return pad[x, y]
      elif y < R+c:
        return pad[x, R]
      elif y < 2*R+c:
        return pad[x, y-(2*R+c)]
      else:
        raise ValueError('padding(%d, %d)' % (x, y))
    elif x < R+r:
      if y < R:
        return pad[R, y]
      elif y < R+c:
        return dat0[x-R, y-R]
      elif y < 2*R+c:
        return pad[R, y-(2*R+c)]
      else:
        raise ValueError('padding(%d, %d)' % (x, y))
    elif x < 2*R+r:
      if y < R:
        return pad[x-(2*R+r), y]
      elif y < R+c:
        return pad[x-(2*R+r), R]
      elif y < 2*R+c:
        return pad[x-(2*R+r), y-(2*R+c)]
      else:
        raise ValueError('padding(%d, %d)' % (x, y))
    else:
      raise ValueError('padding(%d, %d)' % (x, y))
  mat1 = np.pad(mat0, ((0, h-1), (0, w-1)), 'constant', constant_values=((0, 0), (0, 0)))
  dat1 = np.fromfunction(np.frompyfunc(padding, 2, 1), mat1.shape)
  fdat = fft2(dat1)
  fmat = fft2(mat1)
  fdat /= fmat
  res = ifft2(fdat)
  assert(abs(res.imag).max() < 1e-9)
  res = res[:h, :w].real.flatten().tolist()
  return res

def deblur2(dat, mat, w, h, R, sigma, sx = 0, sy = 0):
  assert(len(dat) == w*h)
  dat0 = np.reshape(np.array(dat), (h, w))
  mat0 = np.array(mat)
  mat1 = np.pad(mat0, ((0, h-1), (0, w-1)), 'constant', constant_values=((0, 0), (0, 0)))
  dat1 = np.pad(dat0, ((R, R), (R, R)), 'constant', constant_values=((0, 0), (0, 0)))
  fmat = fft2(mat1)
  dat2 = ifft2(fft2(dat1)*fmat)
  dat2[R:R+h, R:R+w] = dat0
  res = ifft2(fft2(dat2)/fmat)
  assert(abs(res.imag).max() < 1e-9)
  res = res[:h, :w].real.flatten().tolist()
  return res

sigma = int(sys.argv[1])
R = 3*sigma
fn = sys.argv[2]
if len(sys.argv) == 5:
  sx, sy = map(int, sys.argv[3:5])
else:
  sx, sy = 0, 0

mat = [[0]*(2*R+1) for _ in range(2*R+1)]
for r in range(-R, R+1):
  for c in range(-R, R+1):
    mat[r+R][c+R] = exp(-(r**2+c**2)/(2.0*sigma**2))
s = sum(map(sum, mat))
for r in range(-R, R+1):
  for c in range(-R, R+1):
    mat[r+R][c+R] /= s

fp = open(fn, 'rb')
im = Image.open(fp)
w, h = im.size
dr, dg, db = [], [], []
for px in im.getdata():
  r, g, b = px[:3]
  dr.append(r)
  dg.append(g)
  db.append(b)
fp.close()

resR = deblur2(dr, mat, w, h, R, sigma, sx, sy)
resG = deblur2(dg, mat, w, h, R, sigma, sx, sy)
resB = deblur2(db, mat, w, h, R, sigma, sx, sy)
resR1 = [max(0, min(255, int(x+0.5))) for x in resR]
resG1 = [max(0, min(255, int(x+0.5))) for x in resG]
resB1 = [max(0, min(255, int(x+0.5))) for x in resB]
im = Image.new('RGB', (w, h))
im.putdata(zip(resR1, resG1, resB1))
print im.histogram()[:256]
print im.histogram()[256:512]
print im.histogram()[512:]
im.show()
