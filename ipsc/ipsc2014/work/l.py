import pymedia.muxer as muxer
import pymedia.audio.acodec as acodec
from pprint import pprint

f = open( '../l1.mp3', 'rb' ) 
data = f.read()
f.close()

dm = muxer.Demuxer('mp3')
frames = dm.parse( data )
pprint(dm.streams[0])

dec = acodec.Decoder( dm.streams[ 0 ] )
f = open('l1.wav', 'wb')
for fr in frames:
  r = dec.decode(fr[1])
  f.write(r.data)
f.close()
