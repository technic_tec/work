#Python 2.x:
from __future__ import division

import numpy as np
from matplotlib import mlab,  pyplot

f.readline()
n = int(f.readline())
dat = [int(f.readline()) for _ in range(n)]

Fs = 22050
N = 512
x = np.array(dat)
w = np.hamming(N)
ov = N - Fs // 1000 # e.g. 512 - 48000 // 1000 == 464
Pxx,  freqs,  bins = mlab.specgram(x,  NFFT=N,  Fs=Fs,  window=w,  
                                     noverlap=ov)

#plot the spectrogram in dB

Pxx_dB = np.log10(Pxx)
pyplot.subplots_adjust(hspace=0.4)

#pyplot.subplot(100+iCase)
ex1 = (bins[0],  bins[10000],  freqs[0],  freqs[-1])
pyplot.imshow(np.flipud(Pxx_dB[:,:10001]),  extent=ex1)
pyplot.axis('auto')
pyplot.axis(ex1)
pyplot.xlabel('time (s)')
pyplot.ylabel('freq (Hz)')

pyplot.show()
