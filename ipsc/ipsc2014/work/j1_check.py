import j1_01
import j1_02
import j1_03
import j1_04
import j1_05
import j1_06
import j1_07
import j1_08
import j1_09
import j1_10
import j1_11

import sys

n, m = map(int, sys.argv[1:3])
for o in [j1_01,j1_02,j1_03,j1_04,j1_05,j1_06,j1_07,j1_08,j1_09,j1_10,j1_11]:
  try:
    print o.solve(n, m)
  except:
    print 'Exception'
