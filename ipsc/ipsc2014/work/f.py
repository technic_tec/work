import time
from itertools import islice
BUFSZ = 1000000
def pi():
  i = idx = 0
  buf = ''
  while True:
    if idx == 2*len(buf):
      f = open('pi/pi100m.hexbin.%03d' % (i/100000000), 'rb')
      f.seek((i%100000000)>>1)
      buf = f.read(BUFSZ)
      f.close()
      idx = 0
    res = (ord(buf[idx>>1]) >> (4*((idx&1)^1))) & 0xf
    idx += 1
    i += 1
    yield res

def row_comb(x, ix, y, iy):
  if abs(ix-iy) < 9:
    return False
  if len(set(x[:3]+y[:3])) < 6:
    return False
  if len(set(x[3:6]+y[3:6])) < 6:
    return False
  if len(set(x[6:]+y[6:])) < 6:
    return False
  return True

def blk_comb(x, y, rid):
  ix = [rid[i] for i in x]
  iy = [rid[i] for i in y]
  if min(abs(u-v) for u in ix for v in iy) < 9:
    return False
  for i in range(9):
    if len(set(t[i] for t in x|y)) < 6:
      return False
  return True

def sudoku():
  rows = dict()
  rid = dict()
  blks = dict()

  cur = ''
  i = 0
  for x in pi():
    cur += str(x)
    if len(cur) < 9:
      continue
    if (i & 0x1fff) == 0:
      print i, len(rows), len(blks)
    if len(set(cur)) == 9 and cur not in rows:
      # processing new row
      adj = set()
      for x in rows:
        if row_comb(x, rid[x], cur, i):
          adj.add(x)
          rows[x].add(cur)
      rows[cur] = adj
      rid[cur] = i
      for x in adj:
        for y in rows[x]:
          if cur in rows[y] and frozenset([x, y, cur]) not in blks:
            # processing new blk
            blk = frozenset([x, y, cur])
            badj = set()
            for u in blks:
              if blk_comb(u, blk, rid):
                badj.add(u)
                blks[u].add(blk)
            blks[blk] = badj
            for u in badj:
              for v in blks[u]:
                if blk in blks[v]:
                  return [(x, rid[x]) for x in set.union(*blk)]
    cur = cur[1:]
    i += 1
  return None

print 'Start: ' + time.strftime('%Y-%m-%d %H:%M:%S')
try:
  r = sudoku()
  assert r is not None
except Exception:
  print 'Error: ' + time.strftime('%Y-%m-%d %H:%M:%S')
  raise
print 'Complete: ' + time.strftime('%Y-%m-%d %H:%M:%S')
for x, y in union(r):
  print x, y
r = sudoku()
