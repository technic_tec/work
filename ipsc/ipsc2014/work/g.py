from itertools import count

def mex(s):
  i = 0
  while i in s:
    i += 1
  return i

sc = [0]
f = open('g.out', 'w')
for n in count(1):
  x = n
  s = set()
  while x > 0:
    b = x&(-x)
    s.add(sc[(n&~b)>>1])
    x &= ~b
  sc.append(mex(s))
  f.write('%d %s\n' % (sc[-1], bin(n)[2:]))
  try:
    print '%d: %d' % (n, sc[-1])
  except KeyboardInterrupt:
    break
f.close()

