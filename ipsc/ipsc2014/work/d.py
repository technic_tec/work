import sys
from itertools import izip
from bz2 import BZ2Compressor, BZ2Decompressor

def shift(o, k):
  k &= 7
  if k == 0:
    return o
  oo = ''
  for i in range(1, len(o)):
    oo += chr(((ord(o[i-1])<<k)&0xff) | ((ord(o[i])>>(8-k)) & ((1<<k)-1)))
  return oo

com = BZ2Compressor()
com.compress('\0'*45899235)
hdr = com.compress('\0'*45899235)[:4]
unit = com.compress('\0'*45899235)
unit = unit[1:] + unit[0]
com.flush()
blkhdr = '314159265359'.decode('hex')
blkhdrs= [shift(blkhdr, k) for k in range(8)]
print blkhdrs

class BZ2Reader(object):
  BUFSZ = 1048576
  def __init__(self, fp):
    self.fp = fp
    self.buf = ''
    self.offset = 0
  def getBZ2Unit(self):
    while True:
      bitsoff = -1
      idx = self.buf.find(blkhdr, self.offset):
        if idx >= 0:
          bitsoff = (idx<<3)
          break
      else:
        for h in blkhdrs[1:]:
          off = self.offset
          while self.buf.find(blkhdr, self.offset
      self.offset = len(self.buf)
      rd = self.fp.read(BZ2Reader.BUFSZ)
      if rd == '':
        o = self.buf
        self.buf = ''
        self.offset = 0
        return o
      self.buf += rd
    i = self.buf.index(blkhdr, self.offset)
    o = self.buf[:i]
    self.buf = self.buf[i:]
    self.offset = len(blkhdr)
    return o
  
def d1():
  dec = BZ2Decompressor()
  fp = open('d1.in.out')
  rd = BZ2Reader(fp)
  assert rd.getBZ2Unit() == hdr
  dec.decompress(hdr)
  dat = rd.getBZ2Unit()
  while dat == unit:
    dat = rd.getBZ2Unit()
  o = ''
  while o=='' or o[-1] != '\0':
    if not dat:
      raise EOFError
    for i in range(0, len(dat), 32):
      o += dec.decompress(dat[i:i+32])
      if o != '' and o[-1] == '\0':
        break
    dat = rd.getBZ2Unit()
  fp.close()
  print o.replace('\0', '')

def d2():
  com = BZ2Compressor()
  com.compress(hdr)
  dat = []
  while len(dat) < 1000:
    o = com.compress(unit)
    if o:
      ld = 0
      for k in range(8):
        if shift(o, k).find(blkhdr) >= 0:
          ld = shift(o, k).index(blkhdr)*8 + k
      print len(o), ld, shift(o, ld&7).encode('string_escape')
      dat.append(o)

#d1()
d2()
