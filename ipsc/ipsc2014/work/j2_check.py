import j2_01
import j2_02
import j2_03
import j2_04
import j2_05
import j2_06
import j2_07
import j2_08
import j2_09
import j2_10
import j2_11
import j2_12
import j2_13
import j2_14
import j2_15
from j2_read_input import read_input

inp = read_input()
for o in [j2_01,j2_02,j2_03,j2_04,j2_05,j2_06,j2_07,j2_08,j2_09,j2_10,j2_11,j2_12,j2_13,j2_14,j2_15]:
  try:
    print o.solve(*inp)
  except KeyboardInterrupt:
    raise
  except Exception, e:
    print e, e.message
