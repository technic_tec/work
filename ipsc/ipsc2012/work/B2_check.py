import sys

def badness(s):
  v = {
    "000009":252,
    "000008":239,
    "000007":228,
    "000006":219,
    "000005":212,
    "000004":207,
    "000003":204,
    "000200":195,
    "000100":200,
    "000020":183,
    "000011":191,
    "000010":194,
    "000002":203,
    "000001":204,
    "000000":207,
    "123456":86,
  }
  return v.get(s, 0)

notes = '(Note: You may make at most 30 submissions for this subproblem.)'
s = sys.stdin.read().strip()
if len(s) != 6 or len(set(s)-set('0123456789')):
  print "Wrong answer: Not a string of six digits (0-9). " + notes
  exit(1)
bad = badness(s)
if bad:
  print 'Wrong answer: "%s" is not the correct answer (badness %d). ' % (s, bad) + notes
  exit(bad)
print 'OK'
