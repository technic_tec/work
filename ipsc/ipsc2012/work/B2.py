def badness(x, y):
  x = '%06d'%x
  y = '%06d'%y
  s = sum((ord(dx)-ord(dy))**2 for dx, dy in zip(x, y))
  return s

for x in range(472, 1000000, 1000):
  if badness(x, 0) == 207 and badness(x, 123456) == 86:
    print '%06d' % x
