import sys
notes = '(Note: You may make at most 30 submissions for this subproblem.)'
try:
  vs = map(int, sys.stdin.read().split())
  if len(vs) == 0:
    raise ValueError
except Exception:
  print "Wrong answer: Not a space-separated sequence of positive integers. " + notes
  exit(1)
if sum(vs) != 123456789:
  print "Wrong answer: Sum of the sequence is not 123456789. " + notes
  exit(2)
for i in range(1, len(vs)):
  if vs[i-1] >= vs[i]:
    print "Wrong answer: Element #%d must be smaller than element #%d. " % (i, i+1) + notes
    exit(3)
  if vs[i] % vs[i-1]:
    print "Wrong answer: Element #%d must be a multiple of element #%d. " % (i+1, i) + notes
    exit(4)
print 'OK'
