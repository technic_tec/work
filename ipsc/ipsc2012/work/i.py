from hashlib import md5

f = open('/home/terence/work/work/ipsc/john-1.8.0-jumbo-1/run/mypassword.lst')
words = [x.strip().lower() for x in f.readlines()]
f.close()

f = open('../i.in')
rec = dict()
for l in f.readlines():
  u, p = l.strip().split(':')
  rec.setdefault(p, list()).append(u)
  u = u.lower()
  if u not in words:
    words.append(u)
f.close()
words = sorted(list(set(words)))

def getdigits(n):
  s = ''
  while n[-1] in '0123456789':
    s = n[-1] + s
    n = n[:-1]
  return s

cracked = []
digs = map(str, range(1, 16))
for w in words:
  w = w.strip()
  suf = ['', 'y'] + digs
  for tr in suf:
    w1 = 'salt'+w+tr
    h = md5(w1).hexdigest().lower()
    if h in rec:
      for u in rec[h]:
        cracked.append((u, h))
        print '%s:%s:%s' % (u, h, w1)
        dx = getdigits(u)
        if dx != '':
          digs.remove(dx)
      del rec[h]
f = open('/home/terence/work/work/ipsc/john-1.8.0-jumbo-1/run/mypassword.lst')
words = f.readlines()
f.close()
for w0 in words:
  w0 = w0.strip()
  for w in [w0.lower(), w0.upper(), w0.capitalize()]:
    if w == w0:
      continue
    for tr in ['', 'y'] + digs:
      w1 = 'salt'+w+tr
      h = md5(w1).hexdigest().lower()
      if h in rec:
        for u in rec[h]:
          cracked.append((u, h))
          print '%s:%s:%s' % (u, h, w1)
          dx = getdigits(u)
          if dx != '':
            digs.remove(dx)
        del rec[h]
print 'uncracked:', len(rec), rec
