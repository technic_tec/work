from satispy.solver.minisat import Minisat
from satispy.cnf import Cnf, Variable
from collections import deque
class Level(object):
  def __init__(self, name, data):
    self.name, self.data = name, data
    self.nregion = 0
    self.entry = self.exit = None
    self.buttons = {}
    self.doors = set()
    self.closed = set()
    for x, row in enumerate(data):
      for y, c in enumerate(row):
        if c > 0 and c < 200:
          self.nregion += 1
          self.closed.add(self.nregion)
          self.fill_region(x, y)
        elif c >= 200 and c < 400:
          self.doors.add((x, y))
    self.conn = dict()
    for x, y in self.doors:
      rs = set()
      for x1, y1 in [(x-1, y), (x, y-1), (x+1, y), (x, y+1)]:
        if self.data[x1][y1] & ~0x3ff:
          rs.add(self.data[x1][y1]>>10)
      self.conn.setdefault(frozenset(rs), set()).add((x, y))
    #print '%s: buttons %d,  doors %d,  regions %d-%d [%d]' % (self.name, len(self.buttons), len(self.doors), self.nregion, len(self.closed), len(set(self.buttons[b][2] for b in self.buttons)))

  def fill_region(self, x, y):
    if self.data[x][y] == 2:
      self.entry = (x, y)
    elif self.data[x][y] == 3:
      self.exit = (x, y)
    elif self.data[x][y] >= 100:
      self.buttons[getVarName(self.data[x][y]-100)] = (x, y, self.nregion)
    self.data[x][y] |= (self.nregion<<10)
    for x1, y1 in [(x-1, y), (x, y-1), (x+1, y), (x, y+1)]:
      if self.data[x1][y1] > 0 and self.data[x1][y1] < 200:
        self.fill_region(x1, y1)
      elif self.data[x1][y1] >= 200 and self.data[x1][y1] < 400:
        if self.nregion in self.closed:
          self.closed.remove(self.nregion)

def getVarName(x):
  if x < 26:
    return chr(65+x)
  elif x < 52:
    return chr(97+x-26)
  elif x < 78:
    return chr(65+x-52)+chr(97+x-52)
  elif x < 104:
    return chr(104+x-78)+chr(104+x-78)
  else:
    raise ValueError(x)

f = open('../l/data.js')
dat = f.read()
f.close()
exec(dat[dat.index('levels'):])
levels = dict((level['name'], Level(level['name'], level['data'])) for level in levels)

for slevel in sorted(levels.keys()):
  level = levels[slevel]
  bid = {}
  for i, b in enumerate(level.buttons):
    bid[b] = i
  if len(set(level.buttons[b][2] for b in level.buttons)) == 1:
    #3SAT
    constraints = Cnf()
    for cn in level.conn:
      clause = Cnf()
      for x, y in level.conn[cn]:
        w = level.data[x][y]
        if w >= 200 and w < 300:
          clause |= Variable(getVarName(w-200))
        elif w >= 300 and w < 400:
          clause |= -Variable(getVarName(w-300))
        else:
          raise ValueError(w)
      constraints &= clause
    sat = Minisat()
    sol = sat.solve(constraints)
    assert(sol.success and not sol.error)
    #res = '%s:'%level.name
    #for k in sorted(level.buttons.keys(), key=lambda x: level.buttons[x]):
    #  res += ' %s=%d' % (k, sol[Variable(k)])
    #print res
    x, y = level.entry
    ss = ''
    acts = set([(level.buttons[b][0], level.buttons[b][1]) for b in level.buttons if sol[Variable(b)]])
    while len(acts):
      q = deque()
      q.append((x, y))
      steps = dict({(x, y):''})
      while len(q):
        x, y = q.popleft()
        step = steps[(x, y)]
        if (x, y) in acts:
          seg = ''
          x1, y1 = x, y
          while steps[(x1, y1)] != '':
            seg += steps[(x1, y1)]
            if seg[-1] == 'U':
              x1 += 1
            elif seg[-1] == 'D':
              x1 -= 1
            elif seg[-1] == 'R':
              y1 -= 1
            elif seg[-1] == 'L':
              y1 += 1
          ss += seg[::-1]+'P'
          acts.remove((x, y))
          break
        for x1, y1, mv in [(x+1, y, 'D'), (x-1, y, 'U'), (x, y-1, 'L'), (x, y+1, 'R')]:
          c = level.data[x1][y1] & 0x3ff
          if c > 0 and c < 200 and (x1, y1) not in steps:
            steps[(x1, y1)] = mv
            q.append(((x1, y1)))
    x0, y0 = x, y
    q = deque()
    q.append(level.exit)
    steps = dict({level.exit:''})
    while len(q):
      x, y = q.popleft()
      step = steps[(x, y)]
      for x1, y1, mv in [(x-1, y, 'D'), (x+1, y, 'U'), (x, y+1, 'L'), (x, y-1, 'R')]:
        c = level.data[x1][y1] & 0x3ff
        if c != 0 and not (c >= 200 and c < 300 and not sol[Variable(getVarName(c-200))]) and not (c >= 300 and c < 400 and sol[Variable(getVarName(c-300))]) and (x1, y1) not in steps:
          steps[(x1, y1)] = mv
          q.append(((x1, y1)))
    x, y = x0, y0
    while (x, y) != level.exit:
      ss += steps[(x, y)]
      if ss[-1] == 'U':
        x -= 1
      elif ss[-1] == 'D':
        x += 1
      elif ss[-1] == 'L':
        y -= 1
      elif ss[-1] == 'R':
        y += 1
    print '%s: %s' % (level.name, ss)
  else:
    # Normal case, only deal with few buttons.
    assert len(level.buttons) < 16
    sol = None
    st = 0
    q = deque()
    q.append((level.entry, st))
    steps = dict({(level.entry, st):''})
    while len(q):
      (x, y), st = q.popleft()
      step = steps[((x, y), st)]
      for x1, y1, mv in [(x-1, y, 'U'), (x+1, y, 'D'), (x, y+1, 'R'), (x, y-1, 'L')]:
        c = level.data[x1][y1] & 0x3ff
        if c != 0 and not (c >= 200 and c < 300 and (getVarName(c-200) not in bid or not ((st >> bid[getVarName(c-200)]) & 1))) and not (c >= 300 and c < 400 and getVarName(c-300) in bid and ((st >> bid[getVarName(c-300)]) & 1)) and ((x1, y1), st) not in steps:
          steps[((x1, y1), st)] = mv
          q.append(((x1, y1), st))
          if c == 3:
            q.clear()
            sol = ''
            st = ((x1, y1), st)
            while steps[st] != '':
              sol += steps[st]
              if sol[-1] == 'U':
                st = ((st[0][0]+1, st[0][1]), st[1])
              elif sol[-1] == 'D':
                st = ((st[0][0]-1, st[0][1]), st[1])
              elif sol[-1] == 'R':
                st = ((st[0][0], st[0][1]-1), st[1])
              elif sol[-1] == 'L':
                st = ((st[0][0], st[0][1]+1), st[1])
              elif sol[-1] == 'P':
                c = level.data[st[0][0]][st[0][1]] & 0x3ff
                st = (st[0], st[1]^(1<<bid[getVarName(c-100)]))
            sol = sol[::-1]
            break
      c = level.data[x][y] & 0x3ff
      if c >= 100 and c < 200:
        st1 = (st ^ (1<<bid[getVarName(c-100)]))
        if ((x, y), st1) not in steps:
          steps[((x, y), st1)] = 'P'
          q.append(((x, y), st1))
    print '%s: %s' % (level.name, sol)
