def monotonic(n, v):
  for i in range(1<<n):
    if (v>>i)&1:
      x = (~i) & ((1<<n)-1)
      while x:
        j = i ^ (x&(-x))
        x &= x-1
        if ((v>>j)&1) == 0:
          return False
  return True

def affinity(n, v):
  for i in range(n):
    f = 0
    for j in range(1<<n):
      k = j ^ (1<<i)
      if ((v>>j)&1) == ((v>>k)&1):
        f |= 1
      else:
        f |= 2
    if f == 3:
      return False
  return True

def self_dual(n, v):
  for i in range(1<<(n-1)):
    j = (~i) & ((1<<n)-1)
    if ((v>>i) & 1) == ((v>>j) & 1):
      return False
  return True

def universal_gate(n):
  pos = (1<<n)
  # 0xxx..x1, skipping truth-preserving & falsity-preserving functions.
  for i in range(1, (1<<(pos-1)), 2):
    if not (monotonic(n, i) or affinity(n, i) or self_dual(n, i)):
      yield ' '.join(list(bin(i)[2:].zfill(pos)[::-1]))

#for x in universal_gate(2):
#  print x
for x in universal_gate(3):
  print x
