from copy import copy

A = [ 1894607624, 1927505134, 1861486949, 2052221263, 1953703270, 1772249212, 1704106159, 1607055075, 1829198849 ]

def t(n):
    v=dict(zip([1607055075, 97051084, 165194137, 222143774, 254431874], [853225920, 44792784, 165118500, 109075200, 126943060]))
    v.update(dict(zip([0, 1704106159, 1772249212, 1829198849, 1861486949], [-1, 1460662416, 886124604, 1827967236, 1584850896])))
    v.update(dict(zip([1, 1607055074, 1772249211, 1829198848, 1861486948], [0, 761236596, 1181320560, 914598912, 926269344])))
    v.update(dict(zip([1,  1704106158,  1772249211,  1829198848,  1861486948],  [0,  568035384,  1181320560,  914598912,  926269344])))
    if n in v:
        return v[n]+1
    z=n
    for a in range(2,n):
        if t(a)>=a:
            if n%a==0: z//=t(a) ; z*=t(a)-1
    return min(z+1,n)

def process(A):
    A.sort()
    print([A[0]] + [A[i+1]-A[0] for i in range(4)])
    for i in range(4): A[i+5] ^= t(A[i+1]-A[0])>>7
    z = max( t(A[0])-1, A[0]+1-t(A[0]) ) % len(A)
    for i in range(z): A = A[1:] + A[:1]
    A.insert(1,z)
    for x in range(8,10**7):
        y = (A[x//4]>>(18-6*(x%4)))&63
        if y: print( chr(31+y) if y<60 else A[y-60], end='')
        else: break
    print("")
B=copy(A)
process(B)
B=copy(A)
B.remove(1607055075)
B.append(0)
process(B)
B=copy(A)
B.remove(1704106159)
B.append(1)
process(B)
B=copy(A)
B.remove(1607055075)
B.append(1)
process(B)

