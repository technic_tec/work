import sys

P=10**9+9

cu = [1]
cd = [1]
c1 = [1]
cu.append((cu[-1])%P)
cd.append((cd[-1])%P)
c1.append(cu[-1]*cd[-1]%P)
cu.append((cu[-1]+cu[-2])%P)
cd.append((cd[-1]+cd[-2])%P)
c1.append(cu[-1]*cd[-1]%P)
cu.append((cu[-1]+cu[-2])%P)
cd.append((cd[-1]+cd[-2]+cd[-3])%P)
c1.append(cu[-1]*cd[-1]%P)
def k1(n):
  # cd[n] = cd[n-1] + cd[n-2] + cd[n-3] + cd[n-4]
  # cu[n] = cu[n-1] + cu[n-2]
  # c1[n] = cu[n] * cd[n]
  while len(c1) <= n:
    cu.append((cu[-1]+cu[-2])%P)
    cd.append((cd[-1]+cd[-2]+cd[-3]+cd[-4])%P)
    c1.append(cu[-1]*cd[-1]%P)
  return c1[n]

k1(4)
cc1, cc2, cc3, cc4 = cu[1:5]
c2 = [1]
c2.append((cc1*c2[-1])%P)
c2.append((cc1*c2[-1]+cc2*c2[-2])%P)
c2.append((cc1*c2[-1]+cc2*c2[-2]+cc3*c2[-3])%P)
def k2(n):
  # c2[n] = cu[1]*c2[n-1] + cu[2]*c2[n-2] + cu[3]*c2[n-3] + cu[4]*c2[n-4]
  while len(c2) <= n:
    c2.append((cc1*c2[-1]+cc2*c2[-2]+cc3*c2[-3]+cc4*c2[-4])%P)
  return c2[n]

m = int(sys.argv[1])
if m == 1:
  f = k1
else:
  f = k2

t = int(raw_input())
for _ in range(t):
  raw_input()
  n = int(raw_input())
  print f(n)
