t = int(raw_input())
for _ in range(t):
  raw_input()
  n = int(raw_input())
  deck = dict()
  deck['S'] = [-1]*(n+1)
  deck['H'] = [-1]*(n+1)
  deck['D'] = [-1]*(n+1)
  deck['C'] = [-1]*(n+1)
  for i, card in enumerate(raw_input().split()):
    tp, val = card[0], int(card[1:])
    deck[tp][val] = i
  cm = 0
  for d in deck:
    dk = deck[d]
    c = 1
    for i in range(1, n+1):
      if dk[i] < dk[i-1]:
        c += 1
    cm = max(c, cm)
  print cm
