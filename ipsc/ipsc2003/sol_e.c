/*
 * IPSC 2003, Problem E
 * by Davidko, 5. maja 2003
 * vzorak
 */

#include <stdio.h>
#include <stdlib.h>
#define MAX 1000
#define INFTY 987654321

struct point
{
	int x,y;
};

const struct point delta[] = { {1,0}, {0,1}, {-1,0}, {0,-1} };

int a[MAX][MAX];	// original image
int b[MAX][MAX];	// floodfilled image
struct point c[MAX*MAX]; // coordinates of points
int d[MAX*MAX];			// distances
struct point e[4*MAX*MAX];	// edges
int idx[MAX*MAX];		// indices to e[]
int ex[MAX];		// excentricities
int N,M,K,E;
int centre;		// centre of the graph

int queue[MAX*MAX];	// a BFS queue
int l,r;

int perm[MAX*MAX];

inline int legal(int x, int y)
{
	return (0<=x && x<N && 0<=y && y<M);
}

int color;
void visit(int x, int y)
{
	int i;

	if(!legal(x,y)) return;
	if(b[x][y]!=-1) return;
	if(a[x][y]!=color) return;

	b[x][y]=K;

	for(i=0; i<4; i++) visit(x+delta[i].x, y+delta[i].y);
}

int cmp(const void *a, const void *b)
{
	if (((struct point *)a)->x == ((struct point *)b)->x)
		return ((struct point *)a)->y - ((struct point *)b)->y;
	
	return ((struct point *)a)->x - ((struct point *)b)->x;
}

int main()
{
	int P,p;

	scanf("%d", &P);
	for(p=1; p<=P; p++)
	{
		int i,j,k;
		scanf("%d %d", &N, &M);
		
		// read input
		for(i=0; i<N; i++)
		for(j=0; j<M; j++)
		{
			int c;
			do
			{
				c=getchar();
			}
			while (!(c=='0' || c=='1'));
			a[i][j]=c-'0';
			b[i][j]=-1;
		}

		// floodfill to find vertices
		K=0;
		for(i=0; i<N; i++)	
		for(j=0; j<M; j++)
		if(b[i][j]==-1)
		{
			color=a[i][j];
			visit(i,j);
			c[K].x=i;
			c[K].y=j;
			K++;
		}

		// compute edges between vertices
		E=0;
		for(i=0; i<N; i++)
		for(j=0; j<M; j++)		
		for(k=0; k<4; k++)
		if( legal(i+delta[k].x, j+delta[k].y) )
		if( b[i][j] != b[i+delta[k].x][j+delta[k].y] )
		{
			e[E].x=b[i][j];
			e[E].y=b[i+delta[k].x][j+delta[k].y];
			E++;
		}
		
		// sort edges
		qsort(e, E, sizeof(struct point), cmp);

		// unique egdes
		j=0;
		for(i=0; i<E; i++)
		if( (i==0) || !(e[i].x==e[i-1].x && e[i].y==e[i-1].y) )
			e[j++]=e[i];
		
		E=j;
		
		// index the edges
		j=0;
		for(i=0; i<K; i++)
		{
			idx[i] = j;
			while(e[j].x==i) j++;
		}
		idx[K] = E;

		// randomize
		srand(K+M+N);
		for(i=0; i<K; i++) perm[i] = i;
		for(i=K-1; i>0; i--)
		{
			int r = rand()%(i+1);
			int t = perm[i];
			perm[i] = perm[r];
			perm[r] = t;
		}

		// K times Breadth First Search
		centre = 0;
		ex[0] = INFTY;
		
		for(j=0, k=perm[0]; j<K; j++, k=perm[j])
		{
			/*
			if(j%100==0)
				fprintf(stdout, "BFS j=%d k=%d K=%d centre=%d ex[center]=%d x=%d y=%d\n", j, k, K, centre, ex[centre], c[centre].x, c[centre].y);
			*/

			for(i=0; i<K; i++) d[i]=-1;

			l=r=0;
			queue[r++] = k;
			d[k] = 0;

			while(l<r)
			{
				int v = queue[l++];

				if (d[v]>=ex[centre]) break;
				
				for(i=idx[v]; i<idx[v+1]; i++)
				if(d[e[i].y]==-1)
				{
					d[e[i].y]=d[v]+1;
					queue[r++] = e[i].y;
				}
			}

			ex[k] = d[queue[l-1]];
			if(ex[k]<ex[centre]) centre = k;
		}

		// print radius and center
		printf("%d\n", ex[centre]);
		for(i=0; i<ex[centre]; i++)
			printf("%d %d\n", c[centre].x+1, c[centre].y+1);

		printf("\n");
	}

	return 0;
}
