/* IPSC 2003, Problem A */
#include <stdio.h>
#include <stdlib.h>
#define MAX 1000

int N,M,P,L;
int b[MAX+1][MAX+1];	/* bipartite graph */
int m[2][MAX+1];	/* matching */
int path[2*MAX];
int visited[2][MAX+1];

/* Depth-First Search */
int visit(int v, int side)
{
	int i;

	visited[side][v] = 1;
	path[L++] = v; 

	if(side==0)
	{
		for(i=1; i<=N; i++)
			if(b[v][i] && m[0][v]!=i && !visited[1][i])
				if (visit(i,1)) return 1;
	}
	else /* side==1 */
	{
		if(m[1][v]>0)	
		{
			if(! visited[0][m[1][v]])	
				if (visit(m[1][v], 0)) return 1;
		}
		else
		{
			return 1;
		}
	}
	
	L--;
	return 0;
}

int main()
{
	int i,j,k,p;

	scanf("%d", &P);	/* number of test cases */

	for(p=1; p<=P; p++)
	{
		scanf("%d %d", &N, &M);
		printf("%d\n", N-M);

		for(i=1; i<=N; i++)
		for(j=1; j<=N; j++)
			b[i][j]=1;

		/* bipartite graph: columns <--> missing numbers */
		for(j=1; j<=M; j++)
		for(i=1; i<=N; i++)
		{
			int x;
			scanf("%d", &x);
			b[i][x] = 0;
		}
		
		/* add N-M rows, each row = matching */
		for(j=M+1; j<=N; j++)
		{
			memset(m, 0, sizeof(m)); /* we start with empty matching */

			/* augment macthing N times to get perfect matching */
			for(k=1; k<=N; k++)
			{
				/* find augmenting path */
				L=0;
				memset(visited, 0, sizeof(visited));

				for(i=1; i<=N; i++)			
					if(! m[0][i])
					{
						visit(i,0);
						break;
					}

				/* augment by the path */
				for(i=0; i<L; i+=2)
				{
					m[0][path[i]] = path[i+1];
					m[1][path[i+1]] = path[i];
				}
			}
			
			/* remove the matching m from graph */
			for(i=1; i<=N; i++)
			{
				b[i][m[0][i]] = 0;
				printf("%d ", m[0][i]);
			}
			printf("\n");
		}

		printf("\n");
	}		

	return 0;
}
