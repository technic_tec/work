#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;
const int P = 32768;
bool isp[P];
int prm[P], np;
const int il61 = (100000000+4)/6, ih61 = (198765432-1)/6;
const int vl61 = 6*il61+1, vh61 = 6*ih61+1;
const int il65 = 100000000/6, ih65 = (198765432-5)/6;
const int vl65 = 6*il65+5, vh65 = 6*ih65+5;
bool isp61[ih61-il61+1];
bool isp65[ih65-il65+1];
int main()
{
  // isp({n, n+18, n+36, n+138, n+198, n+240, n+258}) & n=4k+1
  // 7k+4 & 4k+1 => 28k+25
  memset(isp, true, sizeof(isp));
  isp[0] = isp[1] = false;
  np = 0;
  for(int n = 2; n < P; n++) {
    if(!isp[n]) continue;
    prm[np++] = n;
    for(int j = n*n; j < P; j += n)
      isp[j] = false;
  }
  //printf("Stage 1: np=%d\n", np);
  memset(isp61, true, sizeof(isp61));
  memset(isp65, true, sizeof(isp65));
  for(int ip = 2; ip < np; ip++) {
    int p = prm[ip];
    int i, j = max(p, (vl61+p-1)/p)*p;
    while(j % 6 != 1)
      j += p;
    for(i = (j-vl61)/6; j < vh61; j += 6*p, i += p)
      isp61[i] = false;
    j = max(p, (vl65+p-1)/p)*p;
    while(j % 6 != 5)
      j += p;
    for(i = (j-vl65)/6; j < vh65; j += 6*p, i += p)
      isp65[i] = false;
  }
  //printf("Stage 2: Done\n");
  vector<int> out;
  for(int i = il61 + (il61&1); i <= ih61; i+=2)
    if(isp61[i-il61] && isp61[i+3-il61] && isp61[i+6-il61] && isp61[i+23-il61] && isp61[i+33-il61] && isp61[i+40-il61] && isp61[i+43-il61])
      out.push_back(6*i+1);
  for(int i = il65 + (il65&1); i <= ih65; i+=2)
    if(isp65[i-il65] && isp65[i+3-il65] && isp65[i+6-il65] && isp65[i+23-il65] && isp65[i+33-il65] && isp65[i+40-il65] && isp65[i+43-il65])
      out.push_back(6*i+5);
  sort(out.begin(), out.end());
  for(int i = 0; i < out.size(); i++)
      printf("%d\n", out[i]);
  return 0;
}
