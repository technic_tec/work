N = 655076673443707551099143071618051666714277042558983269995765119
a, b, c = 599561157376811721953, 943753934567180439929, 1157710228741282062487
assert(a*b*c==N)

def gcdex(a, b):
  # ax+by=1
  if b == 0:
    return (a, 1, 0)
  else:
    d, x, y = gcdex(b, a%b)
    #b(x-a/b*y)+ay=d
    return (d, y, x-a/b*y)

def f(a, b):
  d, x, y = gcdex(a, b)
  assert(d==1)
  x1 = 2*x % b
  if x1 < 0:
    x1 += b
  x2 = 2*y % a
  if x2 < 0:
    x2 += a
  return [a*x1-1, b*x2-1]

res = sorted(f(a, b*c) + f(b, c*a) + f(c, a*b))
for x in res:
  assert (x > 1 and x < N and (x**2-1)%N==0)
for x in res:
  print x
