class Paren(object):
  def __init__(self, xl):
    self.xl, self.xr, self.depth, self.area = xl, None, 1, 0

def area(s):
  stk = []
  for i, c in enumerate(s):
    if c == '(':
      stk.append(Paren(i))
    elif c == ')':
      area = 0
      d = 1
      o = stk.pop()
      while o.xr is not None:
        d = max(d, 1+o.depth)
        area -= o.area
        o = stk.pop()
      o.xr = i
      area += (o.xr - o.xl)*d
      o.depth, o.area = d, area
      stk.append(o)
    else:
      raise ValueError
  #assert(len(stk) > 0 and len(filter(lambda x: x.xr is None, stk)) == 0)
  return sum([x.area for x in stk])

t = int(raw_input())
for _ in range(t):
  s = raw_input().strip()
  if s == '':
    s = raw_input().strip()
  print area(s)
