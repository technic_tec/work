import sys
from random import seed, sample, shuffle
from string import maketrans

sc = dict()
f = open('ENGLISH.TET')
for l in f.readlines():
  s, _, c, w = l.split()
  sc[s] = float(w)
f.close()

def score(s):
  res = 0.0
  for i in range(3, len(s)):
    w = s[i-3:i+1]
    res += sc.get(w, 0.0)
  return res

seed()
s = ''.join(sys.stdin.read().split()).upper()
abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
key0, score0 = abc, score(s)
print key0, score0, s

while True:
  kx = list(abc)
  shuffle(kx)
  km = ''.join(kx)
  sm = score(s.translate(maketrans(abc, km)))
  update = True
  while update:
    update = False
    for i in range(26):
      if update:
        break
      for j in range(i+1, 26):
        if update:
          break
        kx[i], kx[j] = kx[j], kx[i]
        k1 = ''.join(kx)
        s1 = score(s.translate(maketrans(abc, k1)))
        if s1 > sm:
          km, sm = k1, s1
          update = True
        else:
          kx[i], kx[j] = kx[j], kx[i]
    if not update and sm > score0:
      key0, score0 = km, sm
      print key0, score0, s.translate(maketrans(abc, km))
