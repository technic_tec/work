from hashlib import md5

a = '\0'*128
va = chr(142)+chr(len(a))+a

f = open('../h2.in', 'rb')
hdr = f.read()
f.close()

pre1 = hdr+"/Courier 20 selectfont 200 700 moveto ("
pad = ''
pre2 = ") pop\n"
post = """
19 get 7 ne {(Terence are mighty hackers!)} {(IPSC rulez!)} ifelse show showpage
"""
pad = ' '*(64-((len(pre1+pre2)+2) & 63))
sa = pre1 + pad + pre2 + va + post

f = open('h2t.ps', 'wb')
f.write(sa)
f.close()

