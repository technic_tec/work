from zipfile import ZipFile
from StringIO import StringIO
from PIL import Image
from pytesseract import image_to_string
import sys

w, h = 724, 452
nrows = 0
buf = ''
img = [list(), list(), list()]
for l in ZipFile(StringIO(file('../m2.in').read().decode('base64'))).read('m_h3').splitlines():
  if l.strip() == 'colorimage':
    nrows = 12
    buf = ''
  elif nrows > 0:
    buf += l.strip()
    if buf[-1] == '>':
      dat = buf[:-1].decode('hex').decode('zip')
      assert(len(dat) == w)
      nrows -= 1
      img[nrows % 3].extend(map(ord, dat))
      buf = ''
assert(len(img[0]) == w*h and len(img[1]) == w*h and len(img[2]) == w*h)
#for i in range(20, h-70, 18):
#  img[0][i*w+2:i*w+254] = [255]*252
#  img[1][i*w+2:i*w+254] = [255]*252
#  img[2][i*w+2:i*w+254] = [255]*252
#for i in range(3, 263, 9):
#  img[0][i+18*w:(h-72)*w:w] = [255]*(h-90)
#  img[1][i+18*w:(h-72)*w:w] = [255]*(h-90)
#  img[2][i+18*w:(h-72)*w:w] = [255]*(h-90)
#assert(len(img[0]) == w*h and len(img[1]) == w*h and len(img[2]) == w*h)
im = Image.new('RGB', (w, h))
dat = zip(img[2], img[1], img[0])
im.putdata(dat)
im.save('m2.png')
#im2 = im.crop((3, 20, 300, 400))
#dat = list(im2.getdata())
#print sorted([(px, dat.count(px)) for px in set(dat)])
#im2.save('m2.png')
#im.show()
o = ''
for y in range(20, h-70, 18):
  for x in range(3, 263, 9):
    im2 = im.crop((x, y, x+9, y+18))
    if len(set(im2.getdata())) > 1:
      #im2.save('char/m2-%d_%d.png' % ((y-20)/18, x/9))
      o += image_to_string(im2, config="-psm 10")
    else:
      #im2.save('empty/m2-%d_%d.png' % ((y-20)/18, x/9))
      o += ' '
  o += '\n'
sys.stdout.write(o)

