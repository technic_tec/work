from string import maketrans

abc = 'abcdefghijklmnopqrstuvwxyz'

def pattern(s):
  ch = 'A'
  mp = dict()
  o = ''
  for c in s:
    if c not in mp:
      mp[c] = ch
      ch = chr(ord(ch)+1)
    o += mp[c]
  return o

print 'Processing words...'
f = open('/usr/share/dict/words')
words = set(w.strip().lower() for w in f.readlines())
words = filter(lambda w: len(set(w)-set(abc)) == 0, words)
f.close()
pats = {}
for w in words:
  pats.setdefault(pattern(w), set()).add(w)

print 'Processing input...'
f = open('../e1.in')
s = ''.join(f.read().split()).replace('_', ' ')
ws = s.split()
print s
f.close()

print 'Guessing...'
for w in ws:
  pat = pattern(w)
  if len(pats.get(pat, set())) == 1:
    print w, '->', pats[pat]
