import sys
from random import seed, choice

def push(stk, v):
  stk.append(v)

def pop(stk):
  if len(stk) == 0:
    stk.append(0)
  return stk.pop()

#fp = file('m.bf')
fp = sys.stdin
seed()
pc, d = (0, 0), (0, 1)
src = fp.read().splitlines()
step = dict({'<':(0, -1), '>':(0, 1), '^':(-1, 0), 'v':(1, 0)})
trace = ''

stk = []
string_mode = False
try:
  while True:
    ins = src[pc[0]][pc[1]]
    trace += ins
    if ins == '"':
      string_mode = not string_mode
    elif string_mode:
      push(stk, ord(ins))
    elif ins == "@":
      break
    elif ins in "0123456789":
      push(stk, int(ins))
    elif ins in "+-*/%":
      a = pop(stk)
      b = pop(stk)
      push(stk, eval('%d %s %d' % (b, ins, a)))
    elif ins in "><^v":
      d = step[ins]
    elif ins == '?':
      d = step[choice(ins)]
    elif ins == '_':
      a = pop(stk)
      if a:
        d = step['<']
      else:
        d = step['>']
    elif ins == '|':
      a = pop(stk)
      if a:
        d = step['^']
      else:
        d = step['v']
    elif ins == '#':
      pc = (pc[0]+d[0], pc[1]+d[1])
    elif ins == '!':
      a = pop(stk)
      push(stk, int(not a))
    elif ins == '`':
      a = pop(stk)
      b = pop(stk)
      push(stk, int(b>a))
    elif ins == ':':
      if len(stk) > 0:
        x = stk[-1]
      else:
        x = 0
      push(stk, x)
    elif ins == '\\':
      a = pop(stk)
      b = pop(stk)
      push(stk, a)
      push(stk, b)
    elif ins == '$':
      pop(stk)
    elif ins == '.':
      x = pop(stk)
      sys.stdout.write('%d' % x)
      sys.stdout.flush()
    elif ins == ',':
      x = pop(stk)
      sys.stdout.write(chr(x))
      sys.stdout.flush()
    elif ins == ' ':
      pass
    else:
      raise NotImplementedError(ins)
      
    pc = (pc[0]+d[0], pc[1]+d[1])
except:
  print 'pc:', pc, 'dir:', d
  print 'stack:', stk
  print 'trace:', trace
  raise
