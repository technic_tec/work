#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
using namespace std;
#define P 1000000007
#define K 10
#define N 100
#define KK 1024

int n;
char buf[KK];
int c[2][KK],*pc,*pl;
int main()
{
  int t;
  cin >> t;
  while(t--) {
    vector<int> own[N];
    cin >> n;
    cin.getline(buf,KK);
    for(int i = 0; i < n; i++) {
      cin.getline(buf,KK);
      stringstream ss(buf);
      int x;
      while(ss >> x)
        own[x-1].push_back(i);
    }
    fill(&c[0][0],&c[2][0],0);
    pc = c[0]; pl = c[1];
    pc[0] = 1;
    for(int i = 0; i < N; i++) {
      if(own[i].empty()) continue;
      int *pt = pc; pc = pl; pl = pt;
      copy(pl, pl+KK, pc);
      for(vector<int>::iterator it = own[i].begin(); it != own[i].end(); ++it)
        for(int k = 0; k < (1<<n); k++)
          if((k >> *it) & 1) {
            pc[k] += pl[k & ~(1<<*it)];
            if(pc[k] >= P)
              pc[k] -= P;
          }
    }
    cout << pc[(1<<n)-1] << endl;
  }
  return 0;
}
