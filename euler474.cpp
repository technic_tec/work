/*
 * =====================================================================================
 *
 *       Filename:  euler474.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年06月30日 14时03分41秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define P 10000000000000061LL

int gcd(int x, int y)
{
  while(y) {
    int c = x; x = y; y = c % y;
  }
  return x;
}

void gen_fact(int n, vector<int> &fac)
{
  fac.assign(n+1, 0);
  fac[1] = 1;
  for(int p = 2; p <= n; p++) {
    if(fac[p])
      continue;
    fac[p] = p;
    if(p < 32768)
      for(int j = p*p; j <= n; j+=p)
        if(!fac[j])
          fac[j] = p;
  }
}

long long mul_mod(long long x, long long y) {
  if(x == 0 || y == 0)
    return 0;
  assert(x > 0 && x < P && y > 0);
  vector<int> yi;
  while(y) {
    yi.push_back(y&255);
    y >>= 8;
  }
  long long r = 0;
  for(int i = yi.size()-1; i >= 0; i--) {
    r <<= 8; r += x*yi[i]; r %= P;
  }
  return r;
}

long long euler474(int n, int d0)
{
  int m = 1, d = d0;
  while(m < d) m *= 10;
  int dc = gcd(m, d);
  m /= dc; d /= dc;
  assert(m < 32768);

  vector<int> fac;
  gen_fact(n, fac);

  vector<int> cf(m, 0);
  for(int i = 2; i <= n; i++) {
    int x = i;
    while(x > 1) {
      int f = fac[x];
      cf[f%m]++;
      x /= f;
    }
  }

  while(dc % 2 == 0) {
    dc /= 2;
    cf[2]--;
  }
  while(dc % 5 == 0) {
    dc /= 5;
    cf[5]--;
  }
  assert(cf[2] >= 0 && cf[5] >= 0);
  if(m % 2 == 0)
    cf[2] = 0;
  if(m % 5 == 0)
    cf[5] = 0;

  vector<long long> rl(m, 0);
  rl[1] = 1;
  for(int i = 0; i < m; i++) {
    if(cf[i] <= 0) continue;
    printf("%d\n", i);
    int loop = m;
    for(int j = i, ci = 1; ci <= cf[i]; j = j * i % m, ci++)
      if(j == 1) {
        loop = ci;
        break;
      }
    int ki = cf[i]/loop, cr = cf[i]%loop;
    vector<long long> rc(m, 0);
    for(int k = 0; k < m; k++) {
      if((m % 2 == 0 && k % 2 == 0) || (m % 5 == 0 && k % 5 == 0)) continue;
      for(int j = 1, ci = 0; ci < loop; j = j * i % m, ci++) {
        if(ki == 0 && ci > cr) break;
        int k1 = k*j%m;
        rc[k1] += mul_mod(rl[k], ki+(ci<=cr));
        if(rc[k1] >= P) rc[k1] -= P;
        assert(rc[k1] >= 0 && rc[k1] < P);
      }
    }
    rl = rc;
  }
  printf("(%d, %d): %lld\n", n, d0, rl[d]);
  return rl[d];
}

int main()
{
  euler474(12, 12);
  euler474(50, 123);
  euler474(1000000, 65432);
  return 0;
}
