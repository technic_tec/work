from pprint import pprint
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans, MiniBatchKMeans
import sys

import numpy as np

n = int(raw_input())
data = list()
for _ in range(n):
    data.append(raw_input())
assert(raw_input().strip() == '*'*5)
for _ in range(n):
    data.append(raw_input())

###############################################################################
vectorizer = TfidfVectorizer(max_df=0.5, max_features=10000,
                                 stop_words='english', use_idf=True)
X = vectorizer.fit_transform(data)

###############################################################################
# Do the actual clustering
if False:
    km = MiniBatchKMeans(n_clusters=len(data)/2, init='k-means++', n_init=1,
                         init_size=1000, batch_size=1000, verbose=False)
else:
    km = KMeans(n_clusters=len(data)/2, init='k-means++', max_iter=100, n_init=1,
                verbose=False)

pprint(sorted(zip(km.fit_predict(X).tolist(), data)))
print km.score()
