import Data.Array
data Inst = Inc | Dec | LeftP | RightP | LoopB Int | LoopE Int | Inp | Out | EOP
data Context = C (Array Int Int) Int String

buildProg :: String->[Inst]
buildProg s = _buildProg s []
    where _buildProg "" [] insts = inst++[EOP]V
          _buildProg "" stk insts = error "Unmatched [!"
          _buildProg ('+':cs) stk insts = _buildProg cs stk (Inc : insts)
          _buildProg ('-':cs) stk insts = _buildProg cs stk (Dec : insts)
          _buildProg ('<':cs) stk insts = _buildProg cs stk (LeftP : insts)
          _buildProg ('>':cs) stk insts = _buildProg cs stk (RightP : insts)
          _buildProg ('.':cs) stk insts = _buildProg cs stk (Inp : insts)
          _buildProg (',':cs) stk insts = _buildProg cs stk (Out : insts)
          _buildProg ('[':cs) stk insts = _buildProg cs (stk (LoopB : insts)
          _buildProg (']':cs) [] insts = error "Unmatched ]!"
          _buildProg (']':cs) stk insts = _buildProg cs stk (LoopE 0 : insts)
          _buildProg (c:cs) stk = _buildProg cs stk insts

runProg :: Int -> Inst -> Context -> IO()
runProg k EOP (C mem ip inp) = putChar '\n'
runProg 0 inst (C mem ip inp) = putStrLn("PROCESS TIME OUT. KILLED!!!")
runProg k (Inst Inc next) (C mem ip inp) = runProg (k-1) next (C (mem//[(ip, (mem!ip)+1)]) ip inp)
runProg k (Inst Dec next) (C mem ip inp) = runProg (k-1) next (C (mem//[(ip, (mem!ip)-1)]) ip inp)
runProg k (Inst LeftP next) (C mem ip inp) = runProg (k-1) next (C mem (ip+1) inp)
runProg k (Inst RightP next) (C mem ip inp) = runProg (k-1) next (C mem (ip-1) inp)
runProg k (Inst Inp next) (C mem ip (c:inp)) | c == '$' = runProg (k-1) next (C (mem//[(ip, -1)]) ip (c:inp))
                                             | otherwise = runProg (k-1) next (C (mem//[(ip, fromEnum c)]) ip inp)
runProg k (Inst Out next) (C mem ip inp) = putChar (toEnum (mem!ip)) >> runProg (k-1) next (C mem ip inp)
runProg k (Inst (LoopB dst) next) (C mem ip inp) | mem!ip == 0 = runProg (k-1) dst (C mem ip inp)
                                                 | otherwise = runProg (k-1) next (C mem ip inp)
runProg k (Inst (LoopE dst) next) (C mem ip inp) | mem!ip == 0 = runProg (k-1) next (C mem ip inp)
                                                 | otherwise = runProg (k-1) dst (C mem ip inp)

main :: IO()
main = do
    s <- getLine
    --let [n, m] = map read $ words s
    inp <- getLine
    prog <- getContents
    runProg 100000 (buildProg prog) (C (array (0, 255) [(i, 0)|i<-[0..255]]) 0 inp)
