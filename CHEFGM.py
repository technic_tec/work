from fractions import Fraction

def Next(vl, vr):
    z = Fraction(0, 1)
    if vl is None and vr is None:
        return z
    elif vl is None:
        #assert(vr <= 0)
        return vr-1
    elif vr is None:
        #assert(vl >= 0)
        return vl+1
    else:
        #assert(vl < vr and (vr-vl)*max(vl.denominator, vr.denominator) == 1) 
        return (vl+vr)/2

def Surreal_Number(s):
    s = sorted(set(s))
    l = r = None
    v = Next(l, r)
    for x in s:
        if x & 1:
            r = v
        else:
            l = v
        v = Next(l, r)
    #print s, v
    return v

def main():
    t = int(raw_input())
    for _ in range(t):
        k = int(raw_input())
        s = Fraction(0, 1)
        for i in range(k):
            v = map(int, raw_input().split())
            assert(len(v)==v[0]+1)
            s += Surreal_Number(v[1:])
        if s > 0:
            print 'EVEN'
        elif s < 0:
            print 'ODD'
        else:
            print "DON'T PLAY"

if __name__ == '__main__':
    main()
