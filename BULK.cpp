#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>
#define N 500
#define INF 1050000000

class ZFace {
    public:
        ZFace(int nv, int v[][3])
        {
            m_nv = nv;
            m_z = v[0][2];
            m_v = new int[nv+1][2];
            m_area = 0;
            for(int i = 0; i < nv; i++) {
                m_v[i][0] = v[i][0];
                m_v[i][1] = v[i][1];
                if(i)
                    m_area += m_v[i-1][0]*m_v[i][1] - m_v[i-1][1] * m_v[i][0];
            }
            m_v[nv][0] = m_v[0][0]; m_v[nv][1] = m_v[0][1];
            m_area += m_v[nv-1][0]*m_v[0][1] - m_v[nv-1][1] * m_v[0][0];
            m_area /= 2;
        }
        ~ZFace()
        {
            delete[] m_v;
        }
        // (x+0.5, y+0.5) is inner point
        void getInnerPoint(int &x, int &y)
        {
            if(m_area > 0) {
                // counter-clockwise
                if(m_v[0][0] == m_v[1][0]) {
                    if(m_v[0][1] < m_v[1][1]) {
                        x = m_v[0][0]-1;
                        y = m_v[0][1];
                    } else {
                        x = m_v[1][0];
                        y = m_v[1][1];
                    }
                } else {
                    if(m_v[0][0] < m_v[1][0]) {
                        x = m_v[0][0];
                        y = m_v[0][1];
                    } else {
                        x = m_v[1][0];
                        y = m_v[1][1]-1;
                    }
                }
            } else {
                // clockwise
                if(m_v[0][0] == m_v[1][0]) {
                    if(m_v[0][1] < m_v[1][1]) {
                        x = m_v[0][0];
                        y = m_v[0][1];
                    } else {
                        x = m_v[1][0]-1;
                        y = m_v[1][1];
                    }
                } else {
                    if(m_v[0][0] < m_v[1][0]) {
                        x = m_v[0][0];
                        y = m_v[0][1]-1;
                    } else {
                        x = m_v[1][0];
                        y = m_v[1][1];
                    }
                }
            }
        }

        // test (x+0.5, y+0.5)
        bool isInnerPoint(int x, int y)
        {
            int cross = 0;
            for(int i = 0; i < m_nv; i++)
                if(m_v[i][0] == m_v[i+1][0] && m_v[i][0] > x && std::min(m_v[i][1], m_v[i+1][1]) <= y && std::max(m_v[i][1], m_v[i+1][1]) > y)
                    ++cross;
            return (cross&1);
        }

        int area() { return abs(m_area); }

        int m_z;

    private:
        int m_nv, (*m_v)[2];
        int m_area;
};

struct ZFace_ptr_cmp {
    bool operator()(const ZFace *lhs, const ZFace *rhs) const {
        return lhs->m_z < rhs->m_z;
    }
};

int nf, v[N][3], nv;
ZFace *pf[N];

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        int f;
        scanf("%d", &f);
        nf = 0;
        for(int i = 0; i < f; i++) {
            scanf("%d", &nv);
            int xmn, xmx, ymn, ymx, zmn, zmx;
            xmn = ymn = zmn = INF;
            xmx = ymx = zmx = -INF;
            for(int j = 0; j < nv; j++) {
                scanf("%d%d%d", &v[j][0], &v[j][1], &v[j][2]);
                if(v[j][0] < xmn) xmn = v[j][0];
                if(v[j][0] > xmx) xmx = v[j][0];
                if(v[j][1] < ymn) ymn = v[j][1];
                if(v[j][1] > ymx) ymx = v[j][1];
                if(v[j][2] < zmn) zmn = v[j][2];
                if(v[j][2] > zmx) zmx = v[j][2];
            }
            if(zmn == zmx) {
                pf[nf++] = new ZFace(nv, v);
            }
        }
        std::sort(pf, pf+nf, ZFace_ptr_cmp());
        int fa = pf[0]->area();
        int vol = 0;
        for(int i = 1; i < nf; i++) {
            vol += fa * (pf[i]->m_z - pf[i-1]->m_z);
            bool inner = false;
            int px, py;
            pf[i]->getInnerPoint(px, py);
            for(int j = 0; j < i; j++)
                if(pf[j]->isInnerPoint(px, py))
                    inner = !inner;
            if(inner)
                fa -= pf[i]->area();
            else
                fa += pf[i]->area();
        }
        printf("The bulk is composed of %d units.\n", vol);
    }
    return 0;
}

