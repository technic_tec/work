#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

int n, T[N], T2[N];
char W[N];

void kmp_table(int n, const char W[], int T[])
{
    int pos = 2, cnd = 0;
    T[0] = -1; T[1] = 0;
    while(pos < n) {
        if(W[pos-1] == W[cnd])
            T[pos++] = ++cnd;
        else if(cnd > 0)
            cnd = T[cnd];
        else
            T[pos++] = 0;
    }
}

bool check_kmp(int n, const char W[], const int T[])
{
    kmp_table(n, W, T2);
    for(int i = 0; i < n; i++)
        if(T[i] != T2[i])
            return false;
    return true;
}

inline int bitpos(int m)
{
    const int bp[] = {-1, 0, 1, 0, 2, 0, 1, 0, 3, 0, 1, 0, 2, 0, 1, 0};
    int p = 0;
    if(!(m & 0xff)) {
        m >>= 8;
        p += 8;
    }
    m &= 0xff;
    if(!(m & 0xf)) {
        m >>= 4;
        p += 4;
    }
    m &= 0xf;
    return bp[m];
}

const char *hex = "0123456789ABCDEF";
bool rekmp_table(int n, char W[], const int T[])
{
    int pos = 2, cnd = 0;
    if(T[0] != -1 || T[1] != 0)
        return false;
    W[0] = 0;
    while(pos <= n) {
        if(T[pos] < 0 || T[pos] >= pos)
            return false;
        if(T[pos] > 0)
            W[pos-1] = W[T[pos]-1];
        else
            W[pos-1] = '\xff';
        int cand = 0xffff;
        while(cnd > 0 && W[pos-1] != W[cnd]) {
            cand &= ~(1<<W[cnd]);
            cnd = T[cnd];
        }
        int exp;
        if(W[pos-1] == W[cnd])
            exp = ++cnd;
        else {
            cand &= ~(1<<W[cnd]);
            exp = 0;
        }
        if(W[pos-1] == '\xff') {
            if(cand == 0)
                return false;
            W[pos-1] = bitpos(cand & (-cand));
        } else {
            if(T[pos] != exp)
                return false;
        }
        ++pos;
    }
    for(int i = 0; i < n; i++)
        W[i] = hex[W[i]];
    W[n] = 0;
    //return check_kmp(n, W, T);
    return true;
}

/* 
 * For given string S, 
 * kmp[i] = maximum L such that S[0..L-1] = S[i-L+1..i]
 * Given kmp[0..N-1], find the smallest hexidecimal string S that meets the kmp requirement.
 */
int main()
{
    scanf("%d", &n);
    T[0] = -1;
    for(int i = 1; i <= n; i++)
        scanf("%d", &T[i]);
    if(rekmp_table(n, W, T))
        puts(W);
    else
        puts("-1");
    return 0;
}

