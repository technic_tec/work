import sys

base='CS'
fn = open(base+'.names', 'w')
fd = open(base+'.data', 'w')
n = int(sys.stdin.readline())
rec = [x.split() for x in sys.stdin.readlines()]
words = set()
classes = set()
for x in rec:
    classes.add(x[0])
    words |= set(x[1:])
classes = sorted(list(classes))
words = sorted(list(words))
fn.write("class.                     | the target attribute" + '\n')
fn.write("class:                     "+", ".join(classes) + '\n')
for w in words:
    fn.write((w+':').ljust(27) + "0, 1\n")
for x in rec:
    fd.write(','.join(x[:1]+[str(int(x[1:].count(w)>0)) for w in words]) + '\n')
fn.close()
fd.close()
