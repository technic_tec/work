/*
 * =====================================================================================
 *
 *       Filename:  STREETTA.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年03月08日 18时48分40秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

int n, m;

struct Node {
    int l, r, a, b;
    bool available;
};

class SegmentTree {
    public:
        SegmentTree(int n) {
            m_nodes = new Node[2*n];
            m_sz = n;
            init(1, 1, n);
        }
        virtual ~SegmentTree() {
            if(m_nodes) {
                delete[] m_nodes;
                m_nodes = NULL;
            }
        }
        // insert arthi seq a*i+b at segment [u, v], ie. inc v[u] by b, v[u+1] by a+b, etc.
        virtual void insert(int u, int v, int a, int b)
        {
            int x = 1;
            if(u > m_nodes[x].r || v < m_nodes[x].l)
                return 0;
            else if(u <= m_nodes[x].l && v >= m_nodes[x].r) {
                if(!m_nodes[x].available) {
                    m_nodes[x].a = a;
                    m_nodes[x].b = (long long)a*(long long)(m_nodes[x].l-u)+(long long)b;
                    m_nodes[x].available = true;
                } else {
                    m_nodes[x].a += a;
                    m_nodes[x].b += a*(m_nodes[x].l-u)+b;
                }
            }
        }
        // query value at u
        virtual long long query(int u);
    private:
        void init(int v, int l, int r) {
            m_nodes[v].l = l;
            m_nodes[v].r = r;
            m_nodes[v].a = m_nodes[v].b = 0;
            m_nodes[v].available = false;
            if(l < r) {
                init(2*v, l, (l+r)/2);
                init(2*v+1, (l+r)/2+1, r);
            }
        }
        // synth update function
        virtual long long f(long long x, long long y) = 0;
        Node *m_nodes;
        int m_sz;
};

class SumSegmentTree : public SegmentTree {
    public:
        SumSegmentTree(int n) : SegmentTree(n) {}
    private:
        // synth update function
        virtual long long f(long long x, long long y)
        {
            return x+y;
        }
};

class MaxSegmentTree : public SegmentTree {
    public:
        MaxSegmentTree(int n) : SegmentTree(n) {}
    private:
        // synth update function
        virtual long long f(long long x, long long y)
        {
            return ((x)>(y)?(x):(y));
        }
};

int main()
{
    scanf("%d%d", &n, &m);
    MaxSegmentTree st1(n);
    SumSegmentTree st2(n);
    for(int i = 0; i < m; i++) {
        int t, u, v, a, b;
        scanf("%d", &t);
        if(t == 1) {
            scanf("%d%d%d%d", &u, &v, &a, &b);
            st1.insert(u, v, a, b);
        } else if(t == 2) {
            scanf("%d%d%d%d", &u, &v, &a, &b);
            st2.insert(u, v, a, b);
        } else {
            scanf("%d", &u);
            int ret = st1.query(u);
            if(ret < 0) {
                printf("NA\n");
            } else {
                ret += st2.query(u);
                printf("%d\n", ret);
            }
        }
    }
    return 0;
}

