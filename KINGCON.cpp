/*
 * =====================================================================================
 *
 *       Filename:  KINGCON.cpp
 *
 *    Description:  Kingdom connectivity
 *
 *        Version:  1.0
 *        Created:  2013年04月04日 11时52分23秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#define N 3100
#define M (N*N)
#define P 1000000000

enum {
	NOT_VISITED = 0, 
	VISITING, 
	VISITED
};
int nv, ne, adj[N], nxt[M], dst[M], cur[N];
int st[N], d[N], low[N], pa[N];
int cut_v[N], ncv;

void init(int n)
{
	nv = n; ne = 0;
	for(int i = 0; i <= n; i++) {
		adj[i] = -1;
		st[i] = NOT_VISITED;
		d[i] = low[i] = -1;
	}
	ncv = 0;
}

void add_edge(int x, int y)
{
	dst[ne] = y;
	nxt[ne] = adj[x];
	adj[x] = ne++;
}

int stk[N], top;
void dfs(int x)
{
	top = -1;
	low[x] = d[x] = 0;
	pa[x] = -1;
	st[x] = VISITING;
	cur[x] = adj[x];
	stk[++top] = x;
	while(top >= 0) {
		x = stk[top];
		assert(st[x] == VISITING);
		while(cur[x] >= 0 && st[dst[cur[x]]] != NOT_VISITED) {
			if(d[dst[cur[x]]] < low[x])
				low[x] = d[dst[cur[x]]];
			cur[x] = nxt[cur[x]];
		}
		if(cur[x] < 0) {
			if(pa[x] >= 0 && low[x] < low[pa[x]])
				low[pa[x]] = low[x];
			if(pa[x] >= 0) {
				for(int e = adj[x]; e >= 0; e = nxt[e])
					if(low[dst[e]] >= d[x]) {
						cut_v[ncv++] = x;
						break;
					}
			} else {
				int c = 0;
				for(int e = adj[x]; e >= 0; e = nxt[e])
					if(pa[dst[e]] == x) {
						++c;
						if(c > 1) {
							cut_v[ncv++] = x;
							break;
						}
					}
			}
			st[x] = VISITED;
			--top;
		} else {
			int y = dst[cur[x]];
			cur[x] = nxt[cur[x]];
			low[y] = d[y] = d[x]+1;
			pa[y] = x;
			st[y] = VISITING;
			cur[y] = adj[y];
			stk[++top] = y;
		}
	}
}

int main()
{
	int t;
	scanf("%d", &t);
	while(t--) {
		int n, m, k;
		scanf("%d%d%d", &n, &m, &k);
		init(n);
		for(int i = 0; i < m; i++) {
			int x, y;
			scanf("%d%d", &x, &y);
			add_edge(x, y);
			add_edge(y, x);
		}
		dfs(0);
		printf("%d\n", ncv*k);
	}
	return 0;
}
