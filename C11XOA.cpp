#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class BigInt {
    public:
        BigInt(int v = 0);
        const BigInt operator+(const BigInt &o) const;
        void print();
    private:
};

int n, k[10], p[10];
BigInt r[10];

int comp(const void *a, const void *b) {
  return k[*(int *)a] - k[*(int *)b];
}

/*
 * count the ways to remove edges from a size-n cycle, without producing isolate vertex.
 */
int main()
{
  scanf("%d",&n);
  for(int i = 0; i < n; i++) {
    scanf("%d",&k[i]);
    p[i] = i;
  }
  qsort(p, n,sizeof(int), comp);
  int i = 3;
  BigInt a(1), b(2);
  for(int j = 0; j < n; j++) {
    while(i < k[p[j]]) {
        BigInt c(a+b);
        a = b; b = c;
        ++i;
    }
    r[p[j]] = a + a + b;
  }
  for(int j = 0; j < n; j++)
    r[j].print();
  return 0;
}
