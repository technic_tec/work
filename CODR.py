n=raw_input().strip()
while n != '0':
  x, y = '%02d' % (int(n[0])*int(n[1])), '%02d' % (int(n[1])*int(n[2]))
  print '%s%s%s%s' % (x, y, str(int(x[1])+int(y[1])), str(int(x[0])+int(y[0]))[::-1])
  n=raw_input().strip()
