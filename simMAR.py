import sys

f = open(sys.argv[1])
rules = list()
for l in f.readlines():
    x, y = l.strip().split('->')
    rules.append((x, y))
f.close()

while True:
    try:
        s = sys.stdin.readline().strip()
    except KeyboardInterrupt:
        break
    try:
        while True:
            r = None
            for rx in rules:
                if s.find(rx[0])>=0:
                    r = rx
                    break
            if not r:
                break
            s = s.replace(r[0], r[1], 1)
    except KeyboardInterrupt:
        print s
        raise KeyboardInterrupt
    print s
