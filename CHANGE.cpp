#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define N 100000
#define P 1000000007

int n, D[100];
char sC[200];

int mod(char *sC, int m)
{
    int r = 0;
    for(int i = 0; sC[i]; i++)
        r = (int)(((long long)r * 10LL + (long long)(sC[i]-'0')) % m);
    return r;
}

/*
 * f(z) = 1/((1-z^D1)(1-z^D2)...(1-z^Dn)z^m)
 *      = A1/z+A2/z^2+...+Ak/z^m + B1/(z-1)+B2/(z-1)^2+...+Bn/(z-1)^n + s{-m}(D2,D3,...,Dn;D1) + ... + s{-m}(D1,D2,...,Dn-1;Dn)
 * pA(C) = const(f(z)) = -B1 + B2 - B3 + ... + (-1)^n*Bn + s{-C}(D2,D3,...,Dn;D1) + s{-C}(D1,D3,...,Dn;D2) + ... + s{-C}(D1,D2,...,Dn-1;Dn)
 * poly_part = -B1 + B2 - B3 + ... + (-1)^n*Bn
 * dedekind_sum(k) = s{-C}(D1,D2,...,Dk-1,Dk+1,...,Dn;Dk)
 */
 
/*
 * Bn = lim{z->1}((z-1)^n*f(z))
 * Bk = lim{z->1}((z-1)^k*(f(z)-Bk+1/(z-1)^(k+1)-...-Bn/(z-1)^n))
 *
 * Q(z):=(1+z+z^2+...+z^(D1-1))(1+z+z^2+...+z^(D2-1))...(1+z+z^2+...+z^(Dn-1))
 * P{k}(z):= Q(z)*(z-1)^k*(f(z)-Bk+1/(z-1)^(k+1)-...-Bn/(z-1)^n)
 * f(z) = 1/(Q(z)*(1-z)^n*z^m) = (-1)^n*z^(-m)/((z-1)^n*Q(z))
 *
 * P{n}(z)=Q(z)*(z-1)^n*f(z) = (-1)^n*z^(-m)
 * P{k}(z)/((z-1)^k*Q(z)) - P{k-1}(z)/((z-1)^(k-1)*Q(z)) = Bk/(z-1)^k
 * => Bk = (P{k}(z) - (z-1)P{k-1}(z))/Q(z)
 *       = lim{z->1}(P{k}(z)/Q(z)) = P{k}(1)/Q(1)
 * P{k-1}(z):=(P{k}(z)-Bk*Q(z))/(z-1)
 *
 * P{k}(z) = g{k}(z)*z^(-m) + h{k}(z)
 * Bk = (g{k}(1)+h{k}(1))/Q(1)
 * g{n}(z) = (-1)^n, h{n}(z) = 0
 * g{k-1}(z) = g{k}(z)*(1+z+z^2+...+z^(m-1))
 * h{k-1}(z) = (g{k}(z) + h{k}(z) - Bk*Q(z))/(z-1)
 * g{k-1}(1) = m*g{k}(1)
 * h{k-1](1) = g{k}'(1) + h{k}'(1) - Bk*Q'(1)
 */
int poly_part(int n, int D[], int C);

/*
 * A=prod{i=1..n}(1-t^ai)
 * B=(1-t^r)/(1-t)
 * alpha*A + beta*B = 1
 * Step I) PolynomialExtendedGCD(A,B) => alpha
 * Step II) alpha[i] - q(r-i) = gamma, alpha[r-(a1+a2+...+an+i)%r] - q((a1+a2+...+an+i)%r) = gamma
 * 		odd(n)  -> (alpha[i] + alpha[r-(a1+a2+...+an+i)%r])/2 = gamma, i=0..r-1
 * 		even(n) -> sum{i=0..r-1}(alpha[i])/n = gamma
 * Step III) P(t) = alpha(t)-gamma*B = sum(sigma(r-i)*t^i, i=0..r-1)
 */
int dedekind_sum(int n, int D[], int k, int C);

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%s", &n, sC);
        for(int i = 0; i < n; i++)
            scanf("%d", &D[i]);
        int r = poly_part(n, D, mod(sC,P));
        for(int i = 0; i < n; i++) {
            r += dedekind_sum(n, D, i, mod(sC,D[i]));
            if(r >= P) r -= P;
        }
        printf("%d\n", r);
    }
	system("pause");
	return 0;
}
