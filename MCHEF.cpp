/*
 * =====================================================================================
 *
 *       Filename:  MCHEF.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月05日 16时09分53秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;

#define N 1000000009

struct Event {
  int x, c;
  bool operator<(const Event &o) const {
    return x < o.x || (x == o.x && c < o.c);
  }
};

int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    int n, b, m;
    vector<int> v;
    vector<Event> e;
    scanf("%d%d%d", &n, &b, &m);
    v.reserve(n);
    e.reserve(m);
    for(int i = 0; i < n; i++) {
      int x;
      scanf("%d", &x);
      v.push_back(x);
    }
    for(int i = 0; i < m; i++) {
      Event x, y;
      scanf("%d%d%d", &x.x, &y.x, &x.c);
      x.x--;
      y.c = -x.c;
      e.push_back(x);
      e.push_back(y);
    }
    sort(e.begin(), e.end());
    multiset<int> cst;
    int c = -1;
    vector<pair<int, long long> > r[2];
    r[0].push_back(make_pair(0, 0));
    int k = 0;
    long long res = 0;
    for(int i = 0, j = 0; i < n; i++) {
      if(j < e.size() && e[j].x == i) {
        while(j < e.size() && e[j].x == i) {
          if(e[j].c > 0)
            cst.insert(e[j].c);
          else
            cst.erase(cst.find(-e[j].c));
          ++j;
        }
        if(!cst.empty())
          c = *cst.begin();
        else
          c = -1;
      }
      res += v[i];
      if(v[i] < 0 && c >= 0) {
        int kl = k;
        k = (1^k);
        r[k].clear();
        int u = 0, w = 0;
        while(u < r[kl].size() || (w < r[kl].size() && r[kl][w].first + c <= b)) {
          int pr = b+1;
          long long gain = 0;
          if(u < r[kl].size())
            pr = min(pr, r[kl][u].first);
          if(w < r[kl].size())
            pr = min(pr, r[kl][w].first + c);
          if(u < r[kl].size() && pr == r[kl][u].first) {
            gain = max(gain, r[kl][u].second);
            ++u;
          }
          if(w < r[kl].size() && pr == r[kl][w].first + c) {
            gain = max(gain, r[kl][w].second - v[i]);
            ++w;
          }
          r[k].push_back(make_pair(pr, gain));
        }
      }
    }
    long long mg = 0;
    for(int i = 0; i < r[k].size(); i++)
      if(r[k][i].second > mg)
        mg = r[k][i].second;
    res += mg;
    printf("%lld\n", res);
  }
  return 0;
}
