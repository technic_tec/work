/*
 * =====================================================================================
 *
 *       Filename:  DELISH.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年06月10日 16时22分55秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 20000

int n, v[N];
long long lmn[N], lmx[N], rmn[N], rmx[N];
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d", &n);
        for(int i = 0; i < n; i++)
            scanf("%d", &v[i]);
        long long s1 = 0, s2 = 0;
        for(int i = 0; i < n; i++) {
            s1 += v[i];
            s2 += v[i];
            lmn[i] = (i > 0 && lmn[i-1] < s1 ? lmn[i-1] : s1);
            lmx[i] = (i > 0 && lmx[i-1] > s2 ? lmx[i-1] : s2);
            if(s1 > 0) s1 = 0;
            if(s2 < 0) s2 = 0;
        }
        s1 = 0; s2 = 0;
        for(int i = n-1; i >= 0; i--) {
            s1 += v[i];
            s2 += v[i];
            rmn[i] = (i < n-1 && rmn[i+1] < s1 ? rmn[i+1] : s1);
            rmx[i] = (i < n-1 && rmx[i+1] > s2 ? rmx[i+1] : s2);
            if(s1 > 0) s1 = 0;
            if(s2 < 0) s2 = 0;
        }
        long long r = -1;
        for(int i = 1; i < n; i++) {
            if(llabs(lmx[i-1] - rmn[i]) > r)
                r = llabs(lmx[i-1] - rmn[i]);
            if(llabs(rmx[i] - lmn[i-1]) > r)
                r = llabs(rmx[i] - lmn[i-1]);
        }
        printf("%lld\n", r);
    }
    return 0;
}

