/*
 * =====================================================================================
 *
 *       Filename:  bst-maintenance.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年04月28日 10时22分54秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <algorithm>

#define N 250000

class PathTree;
class CartesianTree {
    public:
        struct Node {
            int val, pos;
            int sz;
            Node *lch, *rch;
        };
        CartesianTree(int n, int pos[]) {
            m_size = n;
            m_nodes = new Node[n];
            Node **stk = new Node*[n];
            int top = -1;
            for(int i = 0; i < n; i++) {
                Node *p = &m_nodes[pos[i]];
                p->pos = pos[i]; p->val = i;
                p->lch = p->rch = NULL;
                if(top >= 0 && stk[top]->pos >= p->pos) {
                    Node *q = stk[top--];
                    while(top >= 0 && stk[top]->pos >= p->pos) {
                        Node *r = stk[top--];
                        r->rch = q;
                        q = r;
                    }
                    p->lch = q;
                }
                stk[++top] = p;
            }
            Node *q = stk[top--];
            while(top >= 0) {
                Node *r = stk[top--];
                r->rch = q;
                q = r;
            }
            m_root = q;
            delete[] stk;
            for(int i = n-1; i >= 0; i--) {
                m_nodes[i].sz = (m_nodes[i].lch ? m_nodes[i].lch->sz : 0) + (m_nodes[i].rch ? m_nodes[i].rch->sz : 0) + 1;
            }
        }
        ~CartesianTree() {
            if(m_nodes)
                delete[] m_nodes;
            m_root = m_nodes = NULL;
        }
        long long sum_dist() {
            long long res = 0;
            long long *sd = new long long[m_size];
            for(int i = m_size-1; i >= 0; i--) {
                const Node *p = &m_nodes[i];
                int l = (p->lch ? p->lch->pos : -1);
                int r = (p->rch ? p->rch->pos : -1);
                sd[i] = (l>=0?(sd[l]+p->lch->sz):0) + (r>=0?(sd[r]+p->rch->sz):0);
                res += (p->lch?(sd[l]+p->lch->sz):0)*(long long)((p->rch ? p->rch->sz : 0) + 1);
                res += (p->rch?(sd[r]+p->rch->sz):0)*(long long)((p->lch ? p->lch->sz : 0) + 1);
            }
            delete[] sd;
            return res;
        }
        const Node *getRoot() const {
            return m_root;
        }
        const Node& operator[](int i) const {
            return m_nodes[i];
        }
    private:
        Node *m_nodes;
        Node *m_root;
        int m_size;
};

class PathTree {
    public:
        struct PathNode {
            int l, r;
            long long val;
            int d;
        };
        struct Path {
            int size; // subtree size
            int len;  // path length
            PathNode *delta;
            Path **subpath;
            Path *pa;
            int idx;
        };
        PathTree(const CartesianTree &tr) {
            m_sz = tr.getRoot()->sz;
            m_paths = new Path*[m_sz];
            m_pathidx = new int[m_sz];
            memset(m_paths, 0, m_sz*sizeof(Path*));
            memset(m_pathidx, 0, m_sz*sizeof(int));
            m_root = path_init(tr.getRoot(), NULL, 0);
        }
        ~PathTree() {
            release(m_root);
            delete[] m_paths;
            delete[] m_pathidx;
        }
        void init_dist(PathNode *delta, int len) {
            int i = 2*len;
            while(i & (i-1))
                i &= i-1;
            for(int j = len; j < i; j++) {
                delta[j].l = delta[j].r = j+len-i;
                delta[j].val = 0;
                delta[j].d = 0;
            }
            for(int j = i; j < 2*len; j++) {
                delta[j].l = delta[j].r = j-i;
                delta[j].val = 0;
                delta[j].d = 0;
            }
            for(int j = len-1; j > 0; j--) {
                delta[j].l = delta[2*j].l;
                delta[j].r = delta[2*j+1].r;
                delta[j].val = 0;
                delta[j].d = 0;
            }
        }
        void insert_dist(PathNode *delta,  int idx, int dist) {
            assert(idx >= delta[1].l && idx <= delta[1].r);
            // insert (idx, dist)...(r, dist+r-idx)
            int v = 1;
            while(1) {
                if(idx <= delta[v].l) {
                    delta[v].val += dist + (delta[v].l-idx);
                    delta[v].d ++;
                    break;
                } else if(idx <= delta[2*v].r) {
                    delta[2*v+1].val += dist + (delta[2*v+1].l-idx);
                    delta[2*v+1].d++;
                    v = 2*v;
                } else {
                    v = 2*v+1;
                }
            }
            // insert (l, dist+idx-l)...(idx-1, dist+1)
            if(idx == delta[1].l)
                return;
            v = 1;
            while(1) {
                if(idx-1 >= delta[v].r) {
                    delta[v].val += dist + (idx - delta[v].l);
                    delta[v].d--;
                    break;
                } else if(idx-1 >= delta[2*v+1].l) {
                    delta[2*v].val += dist + (idx - delta[2*v].l);
                    delta[2*v].d--;
                    v = 2*v+1;
                } else {
                    v = 2*v;
                }
            }
        }
        long long query_dist(PathNode *delta, int idx) {
            assert(idx >= delta[1].l && idx <= delta[1].r);
            int v = 1;
            while(delta[v].l < delta[v].r) {
                if(delta[v].val || delta[v].d) {
                    delta[2*v].val += delta[v].val;
                    delta[2*v].d += delta[v].d;
                    delta[2*v+1].val += delta[v].val + (long long)(delta[2*v+1].l - delta[v].l)*(long long)delta[v].d;
                    delta[2*v+1].d += delta[v].d;
                    delta[v].val = 0;
                    delta[v].d = 0;
                }
                if(idx <= delta[2*v].r)
                    v = 2*v;
                else
                    v = 2*v+1;
            }
            return delta[v].val;
        }
        long long insert(int x) {
            Path *pth = m_paths[x];
            int idx = m_pathidx[x];
            long long res = 0;
            // insert
            insert_dist(pth->delta, idx, 0);
            pth->size++;
            // query
            res += query_dist(pth->delta, idx);
            int dist = idx;
            Path *lst;
            for(lst = pth, idx = pth->idx, pth = pth->pa; pth != NULL; lst = pth, idx = pth->idx,  pth = pth->pa) {
                // insert
                ++dist;
                insert_dist(pth->delta, idx, dist);
                pth->size++;
                // query
                res += query_dist(pth->delta, idx) - (query_dist(lst->delta, 0) + lst->size) + (long long)(pth->size - lst->size) * (long long)dist;
                dist += idx;
            }
            return res;
        }
    private:
        Path *path_init(const CartesianTree::Node *ptr, Path *pa, int idx) {
            if(ptr == NULL)
                return NULL;
            Path *p = new Path;
            p->size = 0;
            p->len = 0;
            const CartesianTree::Node *ptr0 = ptr;
            while(ptr0 != NULL) {
                if(ptr0->rch == NULL || (ptr0->lch && ptr0->lch->sz >= ptr0->rch->sz))
                    ptr0 = ptr0->lch;
                else
                    ptr0 = ptr0->rch;
                ++p->len;
            }
            p->subpath = new Path*[p->len];
            p->delta = new PathNode[2*p->len];
            init_dist(p->delta, p->len);
            p->pa = pa;
            p->idx = idx;
            int i = 0;
            while(ptr != NULL) {
                m_paths[ptr->pos] = p;
                m_pathidx[ptr->pos] = i;
                if(ptr->rch == NULL || (ptr->lch && ptr->lch->sz >= ptr->rch->sz)) {
                    p->subpath[i] = path_init(ptr->rch, p, i);
                    ptr = ptr->lch;
                    ++i;
                } else {
                    p->subpath[i] = path_init(ptr->lch, p, i);
                    ptr = ptr->rch;
                    ++i;
                }
            }
            assert(i == p->len);
            return p;
        }
        void release(Path* &ptr) {
            if(ptr == NULL)
                return;
            if(ptr->subpath) {
                for(int i = 0; i < ptr->len; i++)
                    release(ptr->subpath[i]);
                delete[] ptr->subpath;
                ptr->subpath = NULL;
            }
            if(ptr->delta) {
                delete[] ptr->delta;
                ptr->delta = NULL;
            }
            ptr->pa = NULL;
            delete ptr;
            ptr = NULL;
        }
        int m_sz;
        Path *m_root;
        Path**m_paths;
        int *m_pathidx;
};

int n, pos[N];

int main()
{
    scanf("%d", &n);
    for(int i = 0; i < n; i++) {
        int x;
        scanf("%d", &x);
        --x;
        pos[x] = i;
    }
    CartesianTree tr(n, pos);
    PathTree pt(tr);
    long long res = 0;
    for(int i = 0; i < n; i++) {
        res += pt.insert(i);
        printf("%lld\n", res);
    }
    assert(tr.sum_dist() == res);
    return 0;
}
