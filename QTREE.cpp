#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

int main()
{
    scanf("%d", &n);
    init_graph(n);
    for(int i = 0; i < n; i++) {
        int u, v, c;
        scanf("%d%d%d", &u, &v, &c);
        add_edge(u, v, c);
    }
    topo();
    return 0;
}

