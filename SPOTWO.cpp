/*
 * =====================================================================================
 *
 *       Filename:  SPOTWO.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年11月01日 17时57分32秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000
#define P 1000000007

int dec2bin(int n)
{
    int s[20], m = 0;
    while(n) {
        s[m++] = (n & 1);
        n >>= 1;
    }
    int r = 0;
    while(m)
        r = (int)(((long long)r * 10LL + (long long)s[--m]) % (P-1));
    return r;
}

int pow_mod(int n, int k)
{
    int r = 1;
    while(k) {
        if(k & 1)
            r = (int)((long long)r * (long long)n % P);
        n = (int)((long long)n * (long long)n % P);
        k >>= 1;
    }
    return r;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        int n;
        long long m;
        scanf("%d", &n);
        printf("%d\n", pow_mod(2, 2*dec2bin(n)));
    }
    return 0;
}

