from fractions import Fraction

def collatz(n):
  r = []
  while n & (n-1):
    r.append(n)
    if n & 1:
      n = 3*n+1
    else:
      n /= 2
  return r
def euler494(k):
  c = 0
  for m in range(1<<(k-1)):
    i = (m<<1)|1
    if i & (i>>1):
      continue
    x = (1, 0, 1)
    cons = []
    for _ in range(k):
      p, q, r = x
      if i & 1:
        x = (p, q+r, 3*r)
        cons.append(x)
      else:
        x = (2*p, 2*q, r)
      i >>= 1
    
    p, q, r = x
    y = 8
    while p*y<=q or (p*y-q)%r:
      y *= 2
    assert(len(collatz((p*y-q)/r)) == k)
    if k <= 10:
      print cons #collatz((p*y-q)/r)
    c += 1
  print k, c

euler494(5)
euler494(10)
euler494(20)
