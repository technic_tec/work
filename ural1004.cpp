/*
 * =====================================================================================
 *
 *       Filename:  ural1004.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年07月31日 13时52分01秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <deque>
using namespace std;

#define INF 1000000009
#define N 110

int n, m, dis[N][N];
int d[N], prev[N], min_cyc;
bool v[N];
deque<int> pth;

inline void init(int n) {
  for(int i = 1; i <= n; i++) {
    for(int j = 1; j <= n; j++)
      dis[i][j] = INF;
  }
}

inline void add_edge(int x, int y, int w)
{
  dis[x][y] = dis[y][x] = min(dis[x][y], w);
}

void dijkstra(int x)
{
  for(int i = 1; i <= n; i++) {
    d[i] = INF;
    prev[i] = -1;
    v[i] = false;
  }
  d[x] = 0;
  for(int k = 0; k < n; k++) {
    x = -1;
    for(int i = 1; i <= n; i++)
      if(!v[i] && (x < 0 || d[i] < d[x]))
        x = i;
    if(x < 0 || d[x] >= INF)
      break;
    v[x] = true;
    for(int i = 1; i <= n; i++)
      if(!v[i] && d[i] > d[x] + dis[x][i]) {
        d[i] = d[x] + dis[x][i];
        prev[i] = x;
      }
      else if(v[i] && i != prev[x] && d[x] + d[i] + dis[x][i] < min_cyc) {
        min_cyc = d[x]+d[i]+dis[x][i];
        pth.clear();
        pth.push_back(i);
        pth.push_back(x);
      }
  }
  if(min_cyc < INF && pth.size() < 3) {
    x = pth.front();
    while(prev[x] >= 0) {
      x = prev[x];
      pth.push_front(x);
    }
    x = pth.back();
    while(prev[x] >= 0) {
      x = prev[x];
      pth.push_back(x);
    }
    assert(pth.front() == pth.back());
    pth.pop_back();
  }
}
int main()
{
  while(scanf("%d%d", &n, &m) == 2 && n >= 0) {
    init(n);
    for(int i = 0; i < m; i++) {
      int x, y, w;
      scanf("%d%d%d", &x, &y, &w);
      add_edge(x, y, w);
    }
    min_cyc = INF;
    pth.clear();
    for(int i = 1; i <= n; i++)
      dijkstra(i);
    if(min_cyc >= INF)
      printf("No solution.\n");
    else {
      assert(pth.size() >= 3 && pth[1] != pth.back());
      for(int i = 0; i < pth.size(); i++)\
        printf("%d%c", pth[i], i<pth.size()-1?' ':'\n');
    }
  }
  return 0;
}
