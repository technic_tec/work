#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 1000000
#define P 1000007

bool isp[N+1];
int main()
{
    int n, c;
    // 1/x + 1/y = 1/n! => uv=n!^2
    memset(isp, true, sizeof(isp));
    isp[0] = isp[1] = false;
    scanf("%d", &n);
    c = 1;
    for(int i = 2; i <= n; i++) {
        if(!isp[i]) continue;
        int cx = 0;
        for(int j = n/i; j > 0; j /= i)
            cx += j;
        c = (int)((long long)c * (long long)(2*cx+1) % P);
        if(i < 32768)
            for(int j = i*i; j <= N; j+=i)
                isp[j] = false;
    }
    printf("%d\n", c);
    return 0;
}

