/*
 * =====================================================================================
 *
 *       Filename:  CONPOIN.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年06月12日 13时51分48秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 1000000009

#if 0
#define BACK_EDGE 1
#define INVERTED  2

class PlanarGraph {
  public:
    struct Vertex;
    struct RootVertex;
    struct Edge {
      Vertex *dst;
      Edge *nxt;
      int flag;
      Edge(Vertex *_src, Vertex *_dst) {
        dst = _dst;
        nxt = _src->adj;
        _src->adj = this;
        flag = 0;
      }
    };
    struct HalfEdge {
      HalfEdge *link[2];
      HalfEdge *twin;
      Vertex *neighbor;
      int flag;
    };

    struct Vertex {
      Edge *adj;
      HalfEdge *link[2];
      Vertex *DFSparent;
      int leastAncestor;
      int lowpoint;
      int visited;
      int backedgeFlag;
      RootVertex* pertinentRoots;
      Vertex *separatedDFSChildList;
      Vertex *pNodeInChildListOfParent;
      Vertex(int id = 0) {
        adj = NULL;
        link[0] = link[1] = NULL;
        visited = -1;
        backedgeFlag = 0;
        pertinentRoots = NULL;
        pNodeInChildListOfParent = NULL;
      }
    };

    struct RootVertex {
      RootVertex *link[2];
      int parent;
    };

    PlanarGraph(int n) {
      m_vertices.assign(n, 0);
      m_edges.clear();
    }
    ~PlanarGraph() {
    }
    void add_edge(int x, int y) {
      m_edges.push_back(Edge(&m_vertices[x], &m_vertices[y]));
      m_edges.push_back(Edge(&m_vertices[y], &m_vertices[x]));
    }
    int DFS();
    bool planarity();
  private:
    vector<Vertex> m_vertices;
    vector<Edge> m_edges;
    HalfEdge *m_he;
};

int main()
{
  int t;
  scanf("%d", &t);
  while(t--) {
    int n, m;
    scanf("%d%d", &n, &m);
    PlanarGraph g(n);
    for(int i = 0; i < m; i++) {
      int x, y;
      scanf("%d%d", &x, &y);
      g.add_edge(x, y);
    }
    bool res = true;
    if(n == 1)
      res = (m == 0);
    else if(n == 2)
      res = (m == 1);
    else if(m != 3*n-6)
      res = false;
    else if(g.DFS() < n)
      res = false;
    else {
      res = g.planarity();
    }
    printf("%d\n", (int)res);
  }
  return 0;
}
#endif

class PlaneTriangulation {
  public:
    struct Node {
      Node *prev, *next;
      Node *q;
      Node *lchild, *rchild, *parent;
      Node() : prev(NULL), next(NULL), q(NULL), lchild(NULL), rchild(NULL), parent(NULL) {}
      ~Node(){}
      inline void linkto(Node *o) {
        next = o;
        o->prev = this;
      }
    };
    // enumerate all triangulations with at most n vertices.
    inline int find_all_triangulations(int n) {
      Node *s = init_K3(n);
      return find_all_child_triangulations(n, 3, s);
    }
    // enumerate all based triangulations with exactly n vertices and n-r inner vertices
    inline int find_based_n_r_triangulations(int n, int r, bool (PlaneTriangulation::*test)(void) = NULL) {
      Node *s = init_K3(n);
      return find_n_r_child_triangulations(n, r, 3, 0, s, test);
    }
    // enumerate all (non-based) triangulations with exactly n vertices and n-r inner vertices
    inline int find_all_n_r_triangulations(int n,  int r) {
      return find_based_n_r_triangulations(n, r, &PlaneTriangulation::is_n_r_smallest);
    }
    // enumerate all maximal planar graphs with n vertices
    inline int find_all_maximal_planar_graphs(int n) {
      return find_based_n_r_triangulations(n, 3, &PlaneTriangulation::is_maximal_planar_smallest);
    }
  private:
    inline Node *init_K3(int n) {
      m_nodes.clear();
      m_nodes.assign(n, Node());
      m_nodes[0].linkto(&m_nodes[1]);
      m_nodes[1].linkto(&m_nodes[2]);
      m_nodes[2].linkto(&m_nodes[0]);
      m_nodes[0].q = &m_nodes[2];
      return &m_nodes[1];
    }
    inline void add_node(Node *pk, Node *pi, Node *pj) {
      if(pi->next != pj) {
        pk->lchild = pi->next; pk->rchild = pj->prev;
        pi->next->parent = pj->prev->parent = pk;
        pi->next->prev = pj->prev->next = NULL;
      }
      pi->linkto(pk);
      pk->linkto(pj);
    }
    inline void remove_node(Node *pk) {
      Node *pi = pk->prev, *pj = pk->next;
      if(pk->lchild) {
        pi->linkto(pk->lchild);
        pk->rchild->linkto(pj);
        pk->lchild->parent = pk->rchild->parent = NULL;
        pk->lchild = pk->rchild = NULL;
      } else {
        pi->linkto(pj);
        pj->linkto(pi);
      }
      pk->prev = pk->next = NULL;
    }
    int find_all_child_triangulations(int n, int nc, Node *ps) {
      int c = 1;
      if(nc == n)
        return c;
      Node *pk = &m_nodes[nc];
      for(Node *pi = &m_nodes[0]; pi != ps; pi = pi->next)
        for(Node *pj = pi; pj != pi->q;) {
          pj = pj->next;
          add_node(pk, pi, pj);
          c += find_all_child_triangulations(n, nc+1, pk);
          remove_node(pk);
        }
      add_node(pk, ps, ps->next);
      ps->q = ps->next;
      c += find_all_child_triangulations(n, nc+1, pk);
      ps->q = NULL;
      remove_node(pk);

      return c;
    }
    int find_n_r_child_triangulations(int n, int r, int nc, int tc, Node *ps, bool (PlaneTriangulation::*test)(void)) {
      int c = 0;
      if(nc == n) {
        assert(tc == n-r);
        return (int)(test==NULL || (this->*test)());
      }
      Node *pk = &m_nodes[nc];
      for(Node *pi = &m_nodes[0]; pi != ps; pi = pi->next) {
        Node *pj = pi;
        for(int ni = 0; tc+ni <= n-r && pj != pi->q; ni++) {
          pj = pj->next;
          if(nc < n-1 || tc+ni == n-r) {
            add_node(pk, pi, pj);
            c += find_all_child_triangulations(n, nc+1, pk);
            remove_node(pk);
          }
        }
      }
      if(nc < n-1 || tc == n-r) {
        add_node(pk, ps, ps->next);
        ps->q = ps->next;
        c += find_all_child_triangulations(n, nc+1, pk);
        ps->q = NULL;
        remove_node(pk);
      }

      return c;
    }
    bool is_n_r_smallest();
    bool is_maximal_planar_smallest();
    vector<Node> m_nodes;
};

int main()
{
  PlaneTriangulation pt;
  pt.find_all_triangulations(6);
  //pt.find_based_n_r_triangulations(9, 4);
  //pt.find_all_n_r_triangulations(9, 4);
  //pt.find_all_maximal_planar_graphs(16);
  return 0;
}
