import sys

n = int(sys.argv[1])
b = [[None]*n for _ in range(n)]
for i in range(n):
    for j in range(i):
        b[i][j] = -(n-1)
    b[i][i] = 0

def search(x, y):
    if y >= n-1:
        b1 = list(set(reduce(lambda x,y: x+y, b)))
        if len(b1) < n+1:
            return 0
        #for i in range(n):
        #    for j in range(i+1):
        #        print b[i][j], 
        #    print
        #print
        return 1
    if x >= n:
        return search(y+2, y+1)
    if b[x][y] >= 0:
        return search(x+1, y)
    res = 0
    for k in range(1, -b[x][y]+1):
        valid = True
        fil = [(x, y, b[x][y])]
        b[x][y] = k
        for x1 in range(y+1, x):
            if b[x1][y] != b[x][y]:
                if b[x][x1] < 0:
                    if max(b[x1][y], b[x][y]) > -b[x][x1]:
                        valid = False
                        break
                    fil.append((x, x1, b[x][x1]))
                    b[x][x1] = max(b[x1][y], b[x][y])
                elif b[x][x1] != max(b[x1][y], b[x][y]):
                    valid = False
                    break
            elif b[x][x1] >= 0:
                if b[x][x1] > b[x][y]:
                    valid = False
                    break
            elif b[x][y] < -b[x][x1]:
                fil.append((x, x1, b[x][x1]))
                b[x][x1] = -b[x][y]
        if valid:
            res += search(x+1, y)
        for x1, y1, v1 in fil:
            b[x1][y1] = v1
    return res

print search(1, 0)
