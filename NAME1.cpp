/*
 * =====================================================================================
 *
 *       Filename:  NAME1.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年05月06日 14时50分39秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000
#define K 26

int cc[K];
char sn[N];
int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        for(int i = 0; i < K; i++)
            cc[i] = 0;
        scanf("%s", sn);
        for(int i = 0; sn[i]; i++)
            cc[sn[i]-'a']++;
        scanf("%s", sn);
        for(int i = 0; sn[i]; i++)
            cc[sn[i]-'a']++;
        int n;
        scanf("%d", &n);
        for(int i = 0; i < n; i++) {
            scanf("%s", sn);
            for(int i = 0; sn[i]; i++)
                cc[sn[i]-'a']--;
        }
        bool r = true;
        for(int i = 0; i < K; i++)
            if(cc[i] < 0)
                r = false;
        printf(r ? "YES\n" : "NO\n");
    }
    return 0;
}

