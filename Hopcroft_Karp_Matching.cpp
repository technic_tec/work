#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <deque>
#include <vector>
using namespace std;

#define INF 99999999

class HK_Match {
    public:
        HK_Match(int _nu, int _nv)
        {
            nu = _nu; nv = _nv;
            adj.clear();
            for(int i = 0; i < nu; i++)
                adj.push_back(-1);
            NIL = nu;
            adj.push_back(-1);
            dst.clear();
            nxt.clear();
        }

        int add_edge(int _s, int _t)
        {
            int ne = dst.size();
            dst.push_back(_t);
            nxt.push_back(adj[_s]);
            adj[_s] = ne;
        }

        int hk_match()
        {
            Pair_G1.clear();
            for(int i = 0; i < nu; i++)
                Pair_G1.push_back(NIL);
            Pair_G2.clear();
            for(int i = 0; i < nv; i++)
                Pair_G2.push_back(NIL);
            int matching = 0;
            while (BFS())
                for(int v = 0; v < nu; v++)
                    if(Pair_G1[v] == NIL)
                        if (DFS(v))
                            matching++;
            return matching;
        }

    private:
        bool BFS()
        {
            deque<int> Q;
            Q.clear();
            Dist.clear();
            for(int v = 0; v < nu; v++) {
                if(Pair_G1[v] == NIL) {
                    Dist.push_back(0);
                    Q.push_back(v);
                } else {
                    Dist.push_back(INF);
                }
            }
            Dist.push_back(INF);
            while (!Q.empty()) {
                int v = Q.front();
                Q.pop_front();
                if(Dist[v] < Dist[NIL])
                    for(int e = adj[v]; e >= 0; e = nxt[e]) {
                        int u = dst[e];
                        if(Dist[ Pair_G2[u] ] == INF) {
                            //printf("BFS G1[%d]-->G2[%d]==>G1[%d]\n", v, u, Pair_G2[u]);
                            Dist[ Pair_G2[u] ] = Dist[v] + 1;
                            Q.push_back(Pair_G2[u]);
                        }
                    }
            }
            return (Dist[NIL] != INF);
        }

        bool DFS(int v)
        {
            if(v != NIL) {
                for(int e = adj[v]; e >= 0; e = nxt[e]) {
                    int u = dst[e];
                    if (Dist[ Pair_G2[u] ] == Dist[v] + 1) {
                        if(DFS(Pair_G2[u])) {
                            //printf("DFS G1[%d]==>G2[%d]-->G1[%d]\n", v, u, Pair_G2[u]);
                            Pair_G2[u] = v;
                            Pair_G1[v] = u;
                            return true;
                        }
                    }
                }
                Dist[v] = INF;
                return false;
            }
            return true;
        }

        int nu, nv, NIL;
        vector<int> adj, dst, nxt;
        vector<int> Pair_G1, Pair_G2, Dist;
};

const int n = 10, m = 10;
const int e[][2] = {
    {1, 1}, {2, 3}, {5, 4}, {3, 5}, {4, 4}, 
    {7, 8}, {8, 7}, {6, 9}, {9, 4}, {3, 9}
};
int main()
{
    HK_Match hk(n, m);
    for(int j = 0; j < m; j++)
        hk.add_edge(e[j][0], e[j][1]);
    printf("%d\n", hk.hk_match());
    return 0;
}
