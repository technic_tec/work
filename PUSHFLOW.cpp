#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>

class Graph {
    public:
        typedef struct Edge {
            int s, t, c, nxt;
            Edge() : s(0),t(0),c(0),nxt(-1){}
        } Edge;

        typedef struct Node {
            int label;  // label by DFS in preorder
            int parent; // parent node in DFS spanning treea
            int eh;     // head of linked list of edges
            Node() : label(-1),parent(-1),eh(-1) {}
        } Node;

        Graph(int n, int m) : m_nNodes(n),m_nEdges(0) {
            m_v = new Node[n];
            m_e = new Edge[2*m];
        }
        virtual ~Graph() {
            if(m_v)
                delete[] m_v;
            if(m_e)
                delete[] m_e;
            m_v = NULL;
            m_e = NULL;
        }
        void add_edge(int u, int v, int c = 1) {
            m_e[m_nEdges].s = u;
            m_e[m_nEdges].t = v;
            m_e[m_nEdges].c = c;
            m_e[m_nEdges].nxt = m_v[u].eh;
            m_v[u].eh = m_nEdges++;
        }

        void prepare() {
            // Find all bridges
            int idx = 0;
            vector<int> stk;
            stk.push_back(make_pair(0,-1));
            while(!stk.empty()) {
                int x = stk.back().first;
                int pa = stk.back().second;
                stk.pop_back();
                if(x < 0) {                   // subtree complete
                    x = -x-1;
                } else if(m_v[x].label < 0) { // first visit
                    m_v[x].label = idx++;
                    m_v[x].parent = pa;
                    for(int e = m_v[x].eh; e >= 0; e = m_e[e].nxt) {
                        if(m_v[e.t].label < 0) {         // forward edge
                            stk.push_back(make_pair(e.t,x));
                        } else if(e.t != pa) { // backward edge
                        }
                    }
                }
            }
        }

        void set_cap(int x, int c);
        int max_flow(int s, int t);
    private:
        int m_nNodes, m_nEdges;
        Node *m_v;
        Edge *m_e;
};

int main()
{
    int n, m;
    scanf("%d%d", &n, &m);
    Graph g(n,m);
    for(int i = 0; i < m; i++) {
        int u, v, c;
        scanf("%d%d%d", &u, &v, &c);
        g.add_edge(u-1,v-1,c);
        g.add_edge(v-1,u-1,c);
    }
    g.prepare();

    int q;
    scanf("%d", &q);
    while(q--) {
        int op, x, y;
        scanf("%d%d%d", &op, &x, &y);
        if(op) {
            g.set_cap(x-1, y);
        } else {
            printf("%d\n",g.max_flow(x-1, y-1));
        }
    }
    return 0;
}
