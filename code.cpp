// =====================================================================================
//
//       Filename:  code.cpp
//
//    Description:  
//
//        Version:  1.0
//        Created:  04/08/14 17:09:05
//       Revision:  none
//       Compiler:  gcc
//
//         Author:  Terence (tec), 
//   Organization:  Intel
//
// =====================================================================================
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <sstream>
using namespace std;

#define N 100000
#define P 1234567

class State {
    public:
        State() : m_pa(NULL) {
            words.push_back("");
        }
        ~State() {}
        void setParent(const State *pa) {
            m_pa = pa;
        }
        void addString(const char *ptr) {
            string s(ptr);
            if(s == "")
                return;
            words[s] = -1;
            if(m_pa)
                m_pa->addString(s+1);
        }
        int count(const string pre = "");
    private:
        const State *m_pa;
        set<string> words;
};

int n, m;
State st[N];
int main()
{
    scanf("%d%d", &n, &m);
    st[0].setParent(NULL);
    for(int i = 1; i < n; i++) {
        int x;
        scanf("%d", &x);
        st[i].setParent(&st[x]);
    }
    for(int i = 0; i < m; i++) {
        int v;
        char s[10];
        scanf("%d%s", &v, s);
        st[i].addString(s);
    }
    printf("%d\n", st[0].count());
    return 0;
}

