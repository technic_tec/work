/*
 * =====================================================================================
 *
 *       Filename:  COOLGUYS.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年09月12日 15时02分58秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

long long count_multiple(int n)
{
    int i;
    long long c = 0;
    if(n < 5) {
        for(i = 1; i <= n; i++)
            c += n / i;
        return c;
    }
    for(i = 1; i*i <= n; i++)
        c += n / i;
    int d = n/i;
    while(d > 0) {
        // n/(n/d) >= d, n/(n/d+1) < d
        int j = n/d;
        c += (long long)d * (long long)(j-i+1);
        i = j+1;
        --d;
    }
    return c;
}

long long gcd(long long a, long long b)
{
    long long c;
    while(b) {
        c = a; a = b; b = c % b;
    }
    return a;
}

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        int n;
        scanf("%d", &n);
        long long c = count_multiple(n);
        long long s = (long long)n * (long long)n;
        long long d = gcd(c, s);
        c /= d; s /= d;
        printf("%lld/%lld\n", c, s);
    }
    return 0;
}

