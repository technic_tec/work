#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 1000000
#define P 1000000007

int fac[N+1];

int power_mod(int n, int k)
{
    int r = 1;
    while(k) {
        if(k & 1)
            r = (int)((long long)r * (long long)n % P);
        n = (int)((long long)n * (long long)n % P);
        k >>= 1;
    }
    return r;
}
int f(int n, int m)
{
    if(m >= 2*n-1)
        return fac[n];
    int r = fac[m-n+1];
    r = (int)((long long)r * (long long)power_mod(m-n, (n-(m-n+1)+1)/2) % P);
    r = (int)((long long)r * (long long)power_mod(m-n+1, (n-(m-n+1))/2) % P);
    return r;
}
int main()
{
    int t;
    fac[0] = 1;
    for(int i = 1; i <= N; i++)
        fac[i] = (int)((long long)fac[i-1] *(long long)i % P);
    scanf("%d", &t);
    while(t--) {
        int n, m;
        scanf("%d%d", &n, &m);
        printf("%d\n", f(n, m));
    }
    return 0;
}
