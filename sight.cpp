/*
 * =====================================================================================
 *
 *       Filename:  sight.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年12月17日 16时44分28秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

int n, r;
double t[2*N][2];
const double pi = acos(-1.0);
const double eps = 1e-10;

int comp(const void *a, const void *b)
{
    double (*x)[2] = (double (*)[2])a;
    double (*y)[2] = (double (*)[2])b;
    if((*x)[0] < (*y)[0] - eps)
        return -1;
    else if((*x)[0] > (*y)[0] + eps)
        return 1;
    else if((*x)[1] < (*y)[1] - eps)
        return -1;
    else if((*x)[1] > (*y)[1] + eps)
        return 1;
    else
        return 0;
}

int main()
{
    freopen("sight.in", "r", stdin);
    freopen("sight.out", "w", stdout);
    scanf("%d%d", &n, &r);
    for(int i = 0; i < n; i++) {
        int x, y;
        scanf("%d%d", &x, &y);
        // x*x+y*y = r*r
        // x*xc+y*yc=r*r
        // yc*yc*x*x + (r*r-xc*x)*(r*r-xc*x) = r*r*yc*yc
        // (xc*xc+yc*yc)*x^2 - 2*xc*r*r*x + r*r*(r*r-yc*yc) = 0
        double t0 = atan2((double)y, (double)x);
        double td = acos((double)r/sqrt((double)x*(double)x+(double)y*(double)y));
        t[i][0] = t0-td; t[i][1] = t0+td;
        t[n+i][0] = t0-td+2*pi; t[n+i][1] = t0+td+2*pi;
    }
    qsort(t, 2*n, 2*sizeof(double), comp);
    int i = 0, j = 0, c = 0;
    while(i < n) {
        while(j+1 < 2*n && t[j+1][0] <= t[i][1] + eps)
            ++j;
        c += j-i;
        ++i; ++j;
    }
    printf("%d\n", c);
    return 0;
}

