/*
 * =====================================================================================
 *
 *       Filename:  LEMOUSE.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年06月10日 19时49分22秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 110
#define INF 999999

#define MIN(a, b) ((a)<(b)?(a):(b))

int n, m;
char b[N][N];
int d[N][N][2];//[r][c][left/top]

int main()
{
    int t;
    scanf("%d", &t);
    while(t--) {
        scanf("%d%d", &n, &m);
        for(int i = 0; i < n; i++)
            scanf("%s", b[i]);
        for(int j = 0; j < m; j++)
            b[n][j] = '0';
        for(int i = 0; i < n; i++)
            b[i][m] = '0';

        d[0][0][0] = d[0][0][1] = (b[0][0]-'0') + (b[1][0]-'0') + (b[0][1]-'0');

        for(int j = 1; j < m; j++) {
            d[0][j][0] = d[0][j-1][0] + (b[1][j]-'0') + (b[0][j+1]-'0');
            d[0][j][1] = INF;
        }

        for(int i = 1; i < n; i++) {
            d[i][0][1] = d[i-1][0][1] + (b[i+1][0]-'0') + (b[i][1]-'0');
            d[i][0][0] = INF;
        }

        for(int i = 1; i < n; i++)
            for(int j = 1; j < m; j++) {
                d[i][j][0] = MIN(d[i][j-1][0] + (b[i-1][j]-'0') + (b[i+1][j]-'0') + (b[i][j+1]-'0'), d[i][j-1][1] + (b[i+1][j]-'0') + (b[i][j+1]-'0'));
                d[i][j][1] = MIN(d[i-1][j][1] + (b[i+1][j]-'0') + (b[i][j-1]-'0') + (b[i][j+1]-'0'), d[i-1][j][0] + (b[i+1][j]-'0') + (b[i][j+1]-'0'));
            }
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                printf("(%c, L%-2d, U%-2d)%c", b[i][j], d[i][j][0], d[i][j][1], j < m-1 ? ' ':'\n');
        printf("%d\n", MIN(d[n-1][m-1][0], d[n-1][m-1][1]));
    }
    return 0;
}

