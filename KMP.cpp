#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 100000

void kmp_table(int n, const char W[], int T[])
{
    int pos = 2, cnd = 0;
    T[0] = -1; T[1] = 0;
    while(pos < n) {
        if(W[pos-1] == W[cnd])
            T[pos++] = ++cnd;
        else if(cnd > 0)
            cnd = T[cnd];
        else
            T[pos++] = 0;
    }
}

int T[N];
int kmp_search(const char *s,  const char *w)
{
    int m = 0, i = 0, n = strlen(s), nw = strlen(w);
    kmp_table(nw, w, T);
    while(m + i < n) {
        if(w[i] == s[m+i]) {
            if(i == nw-1)
                return m;
            ++i;
        } else {
            m = m+i-T[i];
            if(T[i] > -1)
                i = T[i];
            else
                i = 0;
        }
    }
    return n;
}

int main()
{
    return 0;
}

