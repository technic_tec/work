n = int(raw_input())
v = map(long, raw_input().split())
x = int(raw_input())
v.sort()
c = 0
if x >= n:
    c = sum([-k for k in v if k < 0])
elif v[x] < 0:
    c = sum([v[x]-k for k in v if k < v[x]]) - v[x] * x
else:
    c = sum([-k for k in v if k < 0])
print c
