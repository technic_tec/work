from PIL import Image, ImageFileIO
from itertools import groupby
import sys, urllib2, urllib

def barCode(f):
  im = Image.open(f)
  w, h = im.size
  try:
    im.load()
  except AttributeError:
    pass
  dat = list(im.getdata())
  i = 0
  s = dat[i:i+w]
  while len(set(s)) == 1:
    i += w
    s = dat[i:i+w]
  bs = [len(list(y)) for x, y in groupby(s)]
  assert bs[0] == 10 and bs[-1] == 10
  bs = bs[1:-1]
  unit = bs[0]
  assert all(x%unit==0 for x in bs)
  bs = map(lambda x: x/unit, bs)
  assert (bs[0] == 1 and bs[1] == 1 and bs[2] == 1)
  assert (bs[-1] == 1 and bs[-2] == 1 and bs[-3] == 1)
  assert (len(bs) == 59 and sum(bs) == 95 and bs[27:32] == [1]*5)
  ds = dict({(3, 2, 1, 1) : 0, (2, 2, 2, 1) : 1, (2, 1, 2, 2) : 2,
    (1, 4, 1, 1) : 3, (1, 1, 3, 2) : 4, (1, 2, 3, 1) : 5, (1, 1, 1, 4) : 6,
    (1, 3, 1, 2) : 7, (1, 2, 1, 3) : 8, (3, 1, 1, 2) : 9})
  code = [ds[tuple(bs[3+i*4:7+i*4])] for i in range(6)] + [ds[tuple(bs[32+i*4:36+i*4])] for i in range(6)]
  valid = True
  if (sum(bs[4:27:2]) & 1) == 0 or (sum(bs[32:56:2]) & 1) == 1:
    valid = False
  chksum = (sum(code[::2])*3 + sum(code[1::2])) % 10
  if chksum != 0:
    valid = False
  post_data = dict(barcode=''.join(map(str, code)), validity=('valid' if valid else 'invalid'))
  return post_data

url = 'https://www.hellboundhackers.org/challenges/timed/timed7/index.php'
img_url = 'https://www.hellboundhackers.org/challenges/timed/timed7/barcode.php'
hdrs = dict({'Cookie': 'PHPSESSID=6l8hbqo6rn2phibir0r08e1lq7; fusion_visited=TRUE; fusion_user=52347.6009f08e034c239d98b84aeb6f75e353; fusion_lastvisit=1464751119', 'Referer': url})
req = urllib2.Request(img_url, None, hdrs)
f = ImageFileIO.ImageFileIO(urllib2.urlopen(req))
post_data = urllib.urlencode(barCode(f))
print post_data
req = urllib2.Request(url, post_data, hdrs)
try:
  fo = urllib2.urlopen(req)
  print fo.read()
except urllib2.HTTPError, error:
  print error.read()
  raise error
fo.close()
