#include <cstdio>
#include <cassert>
#include <algorithm>
#include <vector>
#include <deque>
#include <set>
using namespace std;

class Edge {
  public:
    int src;
    int dst;
    int cap;
    int flow;
    Edge(int _src,int _dst,int _cap) : src(_src),dst(_dst),cap(_cap),flow(0) {}
};
class Vertex {
  public:
    int idx;
    vector<Edge*> in_edges;
    vector<Edge*> out_edges;
    int e;
    int h;
    int current_edge;
    Vertex(int _idx) : idx(_idx),e(0),h(0),current_edge(0) {}
};

class HLPP {
  public:
    HLPP(int n, int m) : nVertices(n), nEdges(m), vc(2*n+1,0), active(2*n+1), level(0) {
      for(int i = 0; i < n; i++)
        vs.push_back(Vertex(i));
      vc[0] = n;
      es.reserve(m);
    }
    void add_edge(int x,int y,int c) {
      assert(es.size() < nEdges && min(x,y)>=0 && max(x,y) < nVertices);
      es.push_back(Edge(x,y,c));
      vs[x].out_edges.push_back(&es[es.size()-1]);
      vs[y].in_edges.push_back(&es[es.size()-1]);
    }
    int maxflow(int src, int dest) {
      computeLabel(src,dest);
      Vertex *u = &vs[src];
      for(int i = 0; i < u->out_edges.size(); i++) {
        Edge *e = u->out_edges[i];
        if(e->cap > 0) {
          Vertex *v = &vs[e->dst];
          e->flow = e->cap;
          if(v->e==0)
            active[v->h].push_back(v);
          v->e += e->flow;
          if(level < v->h)
            level = v->h;
        }
      }
      while(level >= 0) {
        if(active[level].size() > 0) {
          u = active[level].back();
          active[level].pop_back();
          if(u->idx != src && u->idx != dest)
            discharge(u);
        } else
          level--;
      }
      int res = 0;
      for(int i = 0; i < vs[src].out_edges.size(); i++)
        res += vs[src].out_edges[i]->flow;
      return res;
    }
  private:
    void computeLabel(int src, int dest) {
      deque<int> q;
      vector<bool> inque(nVertices,false);
      vc[vs[dest].h] --;
      vs[dest].h = 0;
      vc[vs[dest].h] ++;
      q.push_back(dest);
      while(!q.empty()) {
        int v = q.front();
        q.pop_front();
        for(int i = 0; i < vs[v].in_edges.size(); i++) {
          Edge *e = vs[v].in_edges[i];
          if(!inque[e->src] && e->flow < e->cap) {
            vc[vs[e->src].h] --;
            vs[e->src].h = vs[v].h + 1;
            vc[vs[e->src].h] ++;
            q.push_back(e->src);
            inque[e->src] = true;
          }
        }
        for(int i = 0; i < vs[v].out_edges.size(); i++) {
          Edge *e = vs[v].out_edges[i];
          if(!inque[e->dst] && e->flow > 0) {
            vc[vs[e->dst].h] --;
            vs[e->dst].h = vs[v].h + 1;
            vc[vs[e->dst].h] ++;
            q.push_back(e->dst);
            inque[e->dst] = true;
          }
        }
      }
      vc[vs[src].h] --;
      vs[src].h = nVertices;
      vc[vs[src].h] ++;
    }
    void discharge(Vertex *u) {
      while(u->e > 0) {
        if(u->current_edge < u->out_edges.size()) {
          Edge *e = u->out_edges[u->current_edge];
          Vertex *v = &vs[e->dst];
          if(u->h == v->h+1 && e->flow < e->cap) {
            int d = min(u->e, e->cap - e->flow);
            e->flow += d;
            u->e -= d;
            v->e += d;
            if(v->e == d) {
              active[v->h].push_back(v);
              level = max(level, v->h);
            }
          } else 
            u->current_edge++;
        } else if(u->current_edge < u->out_edges.size() + u->in_edges.size()) {
          Edge *e = u->in_edges[u->current_edge - u->out_edges.size()];
          Vertex *v = &vs[e->src];
          if(u->h == v->h+1 && e->flow > 0) {
            int d = min(u->e, e->flow);
            e->flow -= d;
            u->e -= d;
            v->e += d;
            if(v->e == d) {
              active[v->h].push_back(v);
              level = max(level,v->h);
            }
          } else
            u->current_edge++;
        } else {
          int hx = 2*nVertices;
          for(int i = 0; i < u->out_edges.size(); i++)
            if(u->out_edges[i]->flow < u->out_edges[i]->cap)
              hx = min(hx, vs[u->out_edges[i]->dst].h);
          for(int i = 0; i < u->in_edges.size(); i++)
            if(u->in_edges[i]->flow > 0)
              hx = min(hx, vs[u->in_edges[i]->src].h);
          vc[u->h]--;
          if(vc[u->h] == 0) {
            // gap heuristic
          }
          u->h = hx + 1;
          vc[u->h]++;
          u->current_edge = 0;
        }
      }
    }

    int nVertices,nEdges;
    vector<Vertex> vs;
    vector<int> vc;
    vector<deque<Vertex*> > active;
    int level;
    vector<Edge> es;
};

int main()
{
  HLPP f(6,8);
  f.add_edge(0,1,15);
  f.add_edge(0,3,4);
  f.add_edge(1,2,12);
  f.add_edge(2,3,3);
  f.add_edge(2,5,7);
  f.add_edge(3,4,10);
  f.add_edge(4,1,5);
  f.add_edge(4,5,10);
  printf("%d\n",f.maxflow(0,5));
  return 0;
}
