/*
 * =====================================================================================
 *
 *       Filename:  euler461.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年03月25日 10时41分27秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <algorithm>
#include <vector>
#include <limits>
using namespace std;
#define N 100000

long double f(int n, int k) {
    return expl((long double)k / (long double)n) - 1.0L;
}

vector<pair<int, long double> > p;
vector<pair<long double, pair<int, int> > > pp;
const long double pi = acosl(-1.0L);
const long double eps = 1.0e-6L;

void euler461(int n) {
    long double v = 0.0;
    p.clear();
    for(int k = 0; v <= pi + eps; k++) {
        v = f(n, k);
        assert(k == 0 || v > p.back().second);
        p.push_back(make_pair(k, v));
    }
    pp.clear();
    pp.reserve(72302123);
    for(int i = 0; i < p.size(); i++)
        for(int j = 0; j <= i; j++) {
            if(p[i].second + p[j].second > p[0].second + p.back().second + eps)
                break;
            pp.push_back(make_pair(p[i].second + p[j].second, make_pair(p[j].first, p[i].first)));
        }
    sort(pp.begin(), pp.end());
    int i = 0, j = pp.size()-1;
    long double err = 4.0*pi;
    int a = -1, b = -1, c = -1, d = -1;
/*     while(i <= j) {
        assert(pp[i].first + pp[j].first >= pi);
        while(i < j && pp[i].first + pp[j-1].first >= pi)
            --j;
        long double ex = fabsl(pp[i].first + pp[j].first - pi);
        if(ex < err) {
            err = ex;
            a = pp[i].second.first; b = pp[i].second.second;
            c = pp[j].second.first; d = pp[j].second.second;
        }
        --j;
        assert(pp[i].first + pp[j].first <= pi);
        while(i < j && pp[i+1].first + pp[j].first <= pi)
            ++i;
        ex = fabsl(pp[i].first + pp[j].first - pi);
        if(ex < err) {
            err = ex;
            a = pp[i].second.first; b = pp[i].second.second;
            c = pp[j].second.first; d = pp[j].second.second;
        }
        ++i;
    } */
    for(i = 0; i < pp.size(); i++) {
        assert(i == 0 || pp[i].first >= pp[i-1].first);
        assert(pp[i].first + pp[j].first >= pi);
        while(j > 0 && pp[i].first + pp[j-1].first >= pi)
            --j;
        long double ex = fabsl(pp[i].first + pp[j].first - pi);
        if(ex < err) {
            err = ex;
            a = pp[i].second.first; b = pp[i].second.second;
            c = pp[j].second.first; d = pp[j].second.second;
            //printf("%d %d: %d %d %d %d: %.16Lf %d\n", i, j, a, b, c, d, err, a*a+b*b+c*c+d*d);
        }
        //else if(ex < err + 5e-14)
        //    printf("*%d %d: %d %d %d %d: %.16Lf %d\n", i, j, pp[i].second.first, pp[i].second.second, pp[j].second.first, pp[j].second.second, ex, pp[i].second.first*pp[i].second.first+pp[i].second.second*pp[i].second.second+pp[j].second.first*pp[j].second.first+pp[j].second.second*pp[j].second.second);
        if(j > 0) {
            ex = fabsl(pp[i].first + pp[j-1].first - pi);
            if(ex < err) {
                err = ex;
                a = pp[i].second.first; b = pp[i].second.second;
                c = pp[j-1].second.first; d = pp[j-1].second.second;
                //printf("%d %d: %d %d %d %d: %.16Lf %d\n", i, j-1, a, b, c, d, err, a*a+b*b+c*c+d*d);
            } //else if(ex < err + 5e-14)
              //  printf("*%d %d: %d %d %d %d: %.16Lf %d\n", i, j-1, pp[i].second.first, pp[i].second.second, pp[j-1].second.first, pp[j-1].second.second, ex, pp[i].second.first*pp[i].second.first+pp[i].second.second*pp[i].second.second+pp[j-1].second.first*pp[j-1].second.first+pp[j-1].second.second*pp[j-1].second.second);
        }

    }
    printf("%d %d %d %d: %.16Lf %d\n", a, b, c, d, err, a*a+b*b+c*c+d*d);
}
int main()
{
    //printf("%e %le %Le\n", numeric_limits<float>::epsilon(), numeric_limits<double>::epsilon(), numeric_limits<long double>::epsilon());
    //printf("%e %le %Le\n", numeric_limits<float>::min(), numeric_limits<double>::min(), numeric_limits<long double>::min());
    euler461(200);
    euler461(10000);
    return 0;
}

