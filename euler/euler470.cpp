/*
 * =====================================================================================
 *
 *       Filename:  euler470.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年05月08日 10时04分55秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>
#include <set>
using namespace std;

#define N 2000000

const double epsilon = 1e-8;
double _R(const set<int> &ds, double c) {
    int d = ds.size();
    double res = 0;
    if(fabs(c) < epsilon)
        res = (double)(*max_element(ds.begin(), ds.end()));
    else {
        double sm = 0.0;
        for(int t = 0; ; t++) {
            double s = 0.0;
            for(set<int>::iterator it = ds.begin(); it != ds.end(); it++)
                s += max((double)*it, sm);
            s /= (double)d;
            if(s - sm > c)
                sm = s;
            else {
                res = sm - t*c;
                break;
            }
        }
    }
#if 0
    printf("_R({");
    for(set<int>::iterator it = ds.begin(); it != ds.end(); it++) {
        if(it != ds.begin()) printf(",");
        printf("%d", *it);
    }
    printf("}, %lf) = %lf\n", c, res);
#endif
    return res;
}

double R(int d, double c) {
    set<int>ds;
    for(int i = 1; i <= d; i++)
        ds.insert(i);
    double res = _R(ds, c);
    printf("R(%d, %lf) = %lf\n", d, c, res);
    return res;
}

void inverse(double m[25][25], int n)
{
    double mr[25][25];
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++) {
            mr[i][j] = (double)(i==j);
        }
    for(int i = 0; i < n; i++) {
        int r = i;
        for(int j = i+1; j < n; j++)
            if(fabs(m[j][i]) > fabs(m[r][i]))
                r = j;
        if(r != i) {
            double t = m[r][i];
            for(int j = i; j < n; j++) {
                double ft = m[i][j]; m[i][j] = m[r][j]/t; m[r][j] = ft;
            }
            for(int j = 0; j < n; j++) {
                double ft = mr[i][j]; mr[i][j] = mr[r][j]/t; mr[r][j] = ft;
            }
        } else {
            double t = m[i][i];
            for(int j = i; j < n; j++)
                m[i][j] /= t;
            for(int j = 0; j < n; j++) 
                mr[i][j] /= t;
        }
        for(int j = i+1; j < n; j++) {
            double t = m[j][i];
            for(int k = i; k < n; k++)
                m[j][k] -= t*m[i][k];
            for(int k = 0; k < n; k++)
                mr[j][k] -= t*mr[i][k];
        }
    }
    for(int i = n-1; i >= 0; i--) {
        for(int j = i-1; j >= 0; j--) {
            double t = m[j][i];
            for(int k = i; k < n; k++)
                m[j][k] -= t*m[i][k];
            for(int k = 0; k < n; k++)
                mr[j][k] -= t*mr[i][k];
        }
    }
#if 0
    printf("---------------------------------------\n");
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            printf("%lf%c", m[i][j], (j<n-1?' ':'\n'));
    printf("---------------------------------------\n");
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            printf("%lf%c", mr[i][j], (j<n-1?' ':'\n'));
    printf("---------------------------------------\n");
#endif
    memcpy(m, mr, sizeof(mr));
}

double m[25][25], tot[25], cb[25];
int bits[N];

double S(int d, int c) {
    double res = 0.0;
    double pp = 1.0/d;
    for(int i = 1; i <= d; i++) {
        for(int j = 1; j <= d; j++)
            m[i-1][j-1] = (double)(i==j);
        if(i > 1)
            m[i-1][i-2] -= (d-i+1.0)/d;
        if(i < d)
            m[i-1][i] -= (i+1.0)/d;
    }
    inverse(m, d);
    for(int i = 0; i < d; i++) {
        tot[i] = 0.0;
        cb[i] = 0;
    }
    bits[0] = 0;
    for(int i = 1; i < (1<<d); i++) {
        bits[i] = bits[i&(i-1)]+1;
        set<int>ds;
        for(int j = 1; j <= d; j++)
            if(i & (1<<(j-1)))
                ds.insert(j);
        cb[bits[i]-1]++;
        tot[bits[i]-1] += _R(ds, c);
    }
    for(int i = 0; i < d; i++)
        res += m[i][d-1] * (tot[i]/cb[i]);
    printf("S(%d, %d) = %lf\n", d, c, res);
    return res; 
}

double F(int n) {
    double res = 0;
    for(int d = 4; d <= n; d++)
        for(int c = 0; c <= n; c++)
            res += S(d, c);
    printf("F(%d) = %lf\n", n, res);
    return res;
}
int main()
{
    R(4, 0.2);
    S(6, 1);
    F(20);
    return 0;
}

