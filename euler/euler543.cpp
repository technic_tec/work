/*
 * =====================================================================================
 *
 *       Filename:  euler543.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年02月05日 15时11分33秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <map>
using namespace std;

#define N 100000000

map<int, int> prime_count(vector<int> &ns) {
  map<int, int> pc;
  sort(ns.begin(), ns.end());
  int i = 0, vm = ns.back();
  vector<bool> isp(N, true);
  vector<int> prm;
  isp[0] = isp[1] = false;
  while(i < ns.size() && ns[i] < 2)
    pc[ns[i++]] = 0;
  int c = 0;
  for(int p = 2; p < N; p++) {
    c += isp[p];
    while(i < ns.size() && p == ns[i])
      pc[ns[i++]] = c;
    if(isp[p] && p < 32768) {
      if(p*p <= vm)
        prm.push_back(p);
      for(int j = p*p; j < N; j += p)
        isp[j] = false;
    }
  }
  printf("PC %d: %d\n", N-1, c);
  for(int s = N; s <= vm; s+=N) {
    int t = min(s+N, vm+1);
    isp.assign(N, true);
    for(int pi = 0; pi < prm.size(); pi++) {
      int p = prm[pi];
      for(int j = (s+p-1)/p*p; j < t; j += p)
        isp[j-s] = false;
    }
    for(int j = s+1; j < t; j += 2) {
      while(i < ns.size() && j == ns[i]-1)
        pc[ns[i++]] = c;
      c += isp[j-s];
      while(i < ns.size() && j == ns[i])
        pc[ns[i++]] = c;
    }
    printf("PC %d: %d\n", t-1, c);
  }
  for(map<int, int>::iterator it = pc.begin(); it != pc.end(); ++it)
    printf("%d: %d\n", it->first, it->second);
  return pc;
}
long long euler543(int n, map<int, int> &pc)
{
  // P(n, 1) = isprime(n)
  // P(n, 2) = odd n ? isprime(n-2) : n>=4
  // P(n, k), k>2 = (n>=2k)
  long long res = 0;
  if(n >= 2) res += pc[n];
  if(n >= 4) res += pc[n-2] + (n-4)/2;
  // s += sum(n-2*k+1, k, 3, [n/2])
  if(n >= 6) {
    int k = n/2;
    res += (long long)(n-k-2)*(long long)(k-2);
  }
  res = (long long)(n/2)*(long long)(n-(n/2)) - (n-pc[n]-1) - (n>=4)*((n+1)/2 - pc[n-2] - 1);
  printf("S(%d) = %lld\n", n, res);
  return res;
}

int main()
{
  vector<int> ns;
  ns.push_back(10);
  ns.push_back(10-2);
  ns.push_back(100);
  ns.push_back(100-2);
  ns.push_back(1000);
  ns.push_back(1000-2);
  for(int x = 1, y = 1, i = 3; i <= 44; i++) {
    int z = x+y;
    ns.push_back(z);
    ns.push_back(z-2);
    x = y; y = z;
  }
  map<int, int> pc = prime_count(ns);

  euler543(10, pc);
  euler543(100, pc);
  euler543(1000, pc);
  long long s = 0;
  for(int x = 1, y = 1, i = 3; i <= 44; i++) {
    int z = x+y;
    s += euler543(z, pc);
    x = y; y = z;
  }
  printf("%lld\n", s);
  return 0;
}
