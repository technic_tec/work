/*
 * =====================================================================================
 *
 *       Filename:  euler518.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年03月07日 15时14分50秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <map>
using namespace std;

#define P 1000000007
#define N 100000000

vector<int> fac(N, 0);
map<int, vector<int> > ps;

void prep()
{
  vector<pair<int, int> > sq;
  sq.reserve(N);
  ps.clear();
  fac[1] = 1;
  sq.push_back(make_pair(1, 0));
  sq.push_back(make_pair(1, 1));
  for(int i = 2; i < N; i++) {
    if(fac[i]) {
      int j = i/fac[i];
      int x = sq[j].first, y = sq[j].second;
      if(y % fac[i])
        y *= fac[i];
      else {
        y /= fac[i];
        x *= fac[i];
      }
      sq.push_back(make_pair(x, y));
    } else {
      fac[i] = i;
      sq.push_back(make_pair(1, i));
      if(i < 32768)
        for(int j = i*i; j < N; j += i)
          if(!fac[j]) fac[j] = i;
    }
    if(i > 2 && fac[i-1] == i-1)
      ps[sq[i].second].push_back(sq[i].first);
  }
  int sz0 = ps.size();
  for(map<int, vector<int> >::iterator it = ps.begin(); it != ps.end();)
    if(it->second.size() <= 1)
      ps.erase(it++);
    else
      ++it;
  printf("prep: %d -> %d\n", sz0, ps.size());
}
long long euler(int n){
  long long s = 0;
  for(map<int, vector<int> >::iterator it = ps.begin(); it != ps.end() && it->first < n; ++it) {\
    int y = it->first;
    for(int i = 1; i < it->second.size(); i++) { 
      int x1 = it->second[i];
      int c = x1*x1*y-1;
      if(c >= n) break;
      for(int j = 0; j < i; j++) {
        int x2 = it->second[j];
        int a = x2*x2*y-1, b = x1*x2*y-1;
        if(fac[b] == b) {
          s += a+b+c;
          if(n <= 100)
            printf("%d %d %d\n", a, b, c);
        }
      }
    }
  }
  printf("S(%d) = %lld\n", n, s);
  return s;
}
int main()
{
  prep();
  euler(100);
  euler(N);
  return 0;
}
