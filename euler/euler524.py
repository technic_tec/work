def perm(c):
  v, r = range(1, len(c)+2), []
  for i in range(len(c), 0, -1):
    x = c[i-1]
    assert x>=0 and x <= i
    r.append(v.pop(x))
  r.append(v.pop())
  assert(len(v) == 0)
  return r[::-1]

def fill_part(s, vl, n, v):
  if n < 0:
    yield v
    return
  for i in range(0, n+1):
    if (1<<i) > s:
      break
    elif (1<<i) < vl or s-(1<<i) >= (1<<n):
      continue
    v[n] = i
    for p in fill_part(s-(1<<i), vl, n-1, v):
      yield p
    v[n] = 0
  if s < (1<<n):
    v[n] = n+1
    for p in fill_part(s, vl, n-1, v):
      yield p
    v[n] = 0

def part(s):
  vl = (s & (-s))
  n, x = 0, s
  while x > 1:
    x >>= 1
    n += 1
  v = [0]*(n+1)
  mp = None
  for p in fill_part(s, vl, n, v):
    print "\r%s\r" % p, 
    pp = perm(p)
    if mp is None or pp < mp:
      print ([(i+1, x) for i, x in enumerate(p) if i+1 != x])
      print pp
      print
      mp = pp

part(12**12)
