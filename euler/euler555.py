# mks(n) = mks(n+k-s) if n<=m,  n-s if n>m
# mks(n) = n1-s where n1==n (mod k-s) and m<n1<=m+k-s
# mks(n) == n => s==0 (mod k-s) && m-s<n=n1-s<=m+k-2s
# => s = x(k-s),  s/k = x/(x+1) >= 1/2,  
# F(m, k, s) = {n|m-s<n<=m+k-2s} if s/k=x/(x+1) for some integer x >= 1, otherwise {}
# SF(p, m) = sum(d=1, [p/2], sum(i=2, [p/d], sum({m-(i-1)d+1..m+id-2(i-1)d})))
#          = sum(d=1, [p/2], sum(i=1, [p/d]-1, d*(m-i*d)+d*(d+1)/2))
def SF(p, m):
  s = 0
  for d in range(1, p/2+1):
    t = p/d-1
    s += (d*m+d*(d+1)/2)*t-d*d*t*(t+1)/2
  return s

for n in [10, 1000, 1000000]:
  print n, SF(n, n)
