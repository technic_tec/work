from math import exp, log
from copy import copy

def gen_prime(n):
  fac = [0] * n
  fac[1] = 1
  prm = []
  for i in range(2, n):
    if fac[i] > 0:
      continue
    fac[i] = i
    prm.append(i)
    for j in range(i*i, n, i):
      if fac[j] == 0:
        fac[j] = i
  return (prm, fac)

N = 10**10
#NN = long(exp(20*log(10.0)/3.0))
#assert(NN**3 <= N**2 and (NN+1)**3 > N**2)
NN = 10**5
prm, fac = gen_prime(NN+1)

def min_est(_rem1, _rem2):
  rem1 = copy(_rem1)
  rem2 = copy(_rem2)
  v = 1
  while len(rem1)+len(rem2) > 0:
    p = max(rem1|rem2)
    v *= p
    rem1.discard(p)
    rem2.discard(p)
    for px, kx in factor(p-1):
      kx %= 3
      if kx == 0:
        continue
      if px in rem1:
        rem1.remove(px)
        if kx == 1:
          rem2.add(px)
      elif px in rem2:
        rem2.remove(px)
        if kx == 2:
          rem1.add(px)
  return v

def factor(n):
  fs = []
  while n > 1:
    f, c = fac[n], 0
    while fac[n] == f:
      n /= f
      c += 1
    fs.append((f, c))
  return fs

def search(v = 1, idx = len(prm)-1, rem1 = set(), rem2 = set()):
  if v * min_est(rem1, rem2) >= N:
    return
  if v > 1 and len(rem1)+len(rem2) == 0:
    yield v
  m = max({0}|rem1|rem2)
  for i in range(idx, -1, -1):
    p = prm[i]
    if p < m:
      break
    elif p in rem1: # 3k
      k = 3
    elif p in rem2: # 3k+1
      k = 1
    else: # 3k+2
      k = 2
    vx = v*p**k
    if vx >= N:
      continue
    f = factor(p-1) + [(p, 2*k-1)]
    for px, rx in f:
      rx %= 3
      if rx == 1:
        if px in rem1:
          rem1.remove(px)
          rem2.add(px)
        elif px in rem2:
          rem2.remove(px)
        else:
          rem1.add(px)
      elif rx == 2:
        if px in rem1:
          rem1.remove(px)
        elif px in rem2:
          rem2.remove(px)
          rem1.add(px)
        else:
          rem2.add(px)
    while vx < N: 
      for x in search(vx, i-1, rem1, rem2):
        yield x
      vx *= p**3
    for px, rx in f:
      rx %= 3
      if rx == 1:
        if px in rem1:
          rem1.remove(px)
        elif px in rem2:
          rem2.remove(px)
          rem1.add(px)
        else:
          rem2.add(px)
      elif rx == 2:
        if px in rem1:
          rem1.remove(px)
          rem2.add(px)
        elif px in rem2:
          rem2.remove(px)
        else:
          rem1.add(px)
    if v == 1:
      assert (len(rem1)==0 and len(rem2)==0)

def rsearch():
  n = 2
  while n**3 <= N**2:
    f = dict([(p, 3*k) for p, k in factor(n)])
    v = 1
    while len(f) > 0 and v > 0:
      p = max(f)
      k = f.pop(p)
      if (k & 1) == 0:
        v = 0
        break
      for p1, k1 in factor(p-1):
        if p1 not in f or f[p1] < k1:
          v = 0
          break
        f[p1] -= k1
        if f[p1] == 0:
          f.pop(p1)
      if v > 0:
        v *= p**((k+1)/2)
    if v > 1 and v < N:
      yield v
    n += 1
#v = []
#for x in search():
#  v.append(x)
#  print len(v), x
v = list(search())
v.sort()
for x in v:
  print x
print len(v), sum(v)

