def ds(n):
    return n+sum(map(int,str(n)))
class Chain(object):
    def __init__(self,n):
        k = 1
        while 9*k*n >= 10**k:
            k += 1
        self.n,self.ms = n,9*k
        self.v = dict()
    def __repr__(self):
        return str((self.n,self.ms,self.v))
    def get_step(self,x,e,b):
        if (x,e,b) in self.v:
            return self.v[(x,e,b)]
        assert b > self.ms
        x0 = x
        b1 = b/10
        steps = 0
        if b1 > self.ms:
            for i in range(10):
                x,c = self.get_step(x,e+i,b1)
                steps += c
        else:
            while(x < b):
                x = ds(x)+e
                steps += 1
            x -= b
        self.v[(x0,e,b)] = (x,steps)
        return (x,steps)
    def eval(self):
        n = self.n
        if n <= 1:
            return 1
        b = 1
        while b <= self.ms:
            b *= 10
        x1,c1 = self.get_step(1,0,b)
        while 1 + c1 <= n:
            b *= 10
            x1,c1 = self.get_step(1,0,b)
        b /= 10
        p,e = 0,0
        x,s = 1,1
        while(b > self.ms):
            d = 0
            x1,c1 = self.get_step(x,e+d,b)
            while s + c1 <= n:
                x,s,d = x1,s+c1,d+1
                x1,c1 = self.get_step(x,e+d,b)
            assert d < 10
            p,e,b = p+d*b,e+d,b/10
        #print p,e,x,s
        for _ in range(n-s):
            x = ds(x)+e
        #print self.ms, self.v
        assert x < 10*b
        return p+x
        
def euler551(n):
    return Chain(n).eval()

for n in range(27)+[10**6,10**15]:
    print n,euler551(n)
