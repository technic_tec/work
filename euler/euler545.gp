/* p1/q1 = B_k, D(k)=q1
 * D(0) = 1, D(1) = 2, D(2n+1) = 1 for n > 0
 * Von Staudt-Clausen theorem: D(2n)=product({p|p is prime, p-1|2n})
 * D(308) = 20010, F(1) = 308
 */
valid(n) = local(r=0); fordiv(308, x, r=(r|isprime(x*n+1)); if(r, break)); !r
N = 3000000
v = vector(N, x, -1)
v[1] = 1
c = 1
m = 308
for(n=2, N, vn=1; fn=factorint(n); for(i=1, matsize(fn)[1], if(!v[n/fn[i,1]], vn=0; break)); if(vn, vn=valid(n)); v[n]=vn; if(vn, c=c+1; if(c==10^5, print(c, ", ", n, ", ", n*308); quit)))
quit
