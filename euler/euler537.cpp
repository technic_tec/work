/*
 * =====================================================================================
 *
 *       Filename:  euler537.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年12月29日 11时49分17秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 400000
#define MOD 1004535809
// MOD-1 = 2^21*479

vector<int> prm;

void prep()
{
  vector<bool> isp(N, true);
  isp[0] = isp[1] = false;
  prm.clear();
  for(int i = 2; i < N; i++)
    if(isp[i]) {
      prm.push_back(i);
      if(i < 32768)
        for(int j = i*i; j < N; j+= i)
          isp[j] = false;
    }
  for(int i = prm.size()-1; i > 0; i--)
    prm[i] -= prm[i-1];
  prm[0]--;
}

int power_mod(int x, int k) {
  int r = 1;
  while(k) {
    if(k & 1)
      r = (int)((long long)r * (long long)x % MOD);
    x = (int)((long long)x * (long long)x % MOD);
    k >>= 1;
  }
  return r;
}

int gcdex(int x, int y, int &a, int &b)
{
  if(y == 0) {
    a = 1; b = 0;
    return x;
  } else {
    int d = gcdex(y, x%y, b, a);
    b -= a*(x/y);
    return d;
  }
}
int inverse (int x, int p) {
  int a, b;
  int d = gcdex(x, p, a, b);
  assert(d == 1);
  assert(abs(a) < p);
  if(a < 0) a += p;
  return a;
}

// DIF2:
// X[2k  ] = sum(w^2kn*(x[n]     + x[n+N/2]),n,0,N/2-1)
// X[2k+1] = sum(w^2kn*w^n*(x[n] - x[n+N/2]),n,0,N/2-1)
// (0,1,2,3,4,5,6,7)->(0,4,2,6,1,5,3,7)
void FFT(vector<int> &v, int w0)
{
  int sz = v.size();
  assert(!(sz & (sz-1)));
  // DIFFFT
  for(int step = sz/2, w = w0; step > 0; step >>= 1, w = (int)((long long)w*(long long)w%MOD))
    for(int i = 0; i < sz; i += 2*step)
      for(int j = i, tw = 1; j < i+step; j++, tw = (int)((long long)tw*(long long)w%MOD)) {
        int x = v[j], y = v[j+step];
        int x1 = x + y;
        int y1 = x - y;
        if(x1 >= MOD) x1 -= MOD;
        if(y1 < 0) y1 += MOD;
        y1 = (int)((long long)y1 * (long long)tw % MOD);
        v[j] = x1; v[j+step] = y1;
      }
}

// DIT2:
// X[k    ] = sum((w^2)^kn*x[2n],n,0,N/2-1) + w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
// X[k+N/2] = sum((w^2)^kn*x[2n],n,0,N/2-1) - w^k*sum((w^2)^kn*x[2n+1],n,0,N/2-1)
// (0,4,2,6,1,5,3,7)->(0,1,2,3,4,5,6,7)
void IFFT(vector<int> &v, int w0)
{
  int sz = v.size();
  assert(!(sz & (sz-1)));
  vector<int> wn(1, w0);
  while(wn.back() != MOD-1)
    wn.push_back((int)((long long)wn.back()*(long long)wn.back()%MOD));
  // DITFFT
  for(int step = 1, w = wn.back(); step < sz; step <<= 1, wn.pop_back(), w = wn.back())
    for(int i = 0; i < sz; i += 2*step)
      for(int j = i, tw = 1; j < i+step; j++, tw = (int)((long long)tw*(long long)w%MOD)) {
        int x = v[j], y = v[j+step];
        y = (int)((long long)y * (long long)tw % MOD);
        int x1 = x + y;
        int y1 = x - y;
        if(x1 >= MOD) x1 -= MOD;
        if(y1 < 0) y1 += MOD;
        v[j] = x1; v[j+step] = y1;
      }
  // FFT->IFFT
  int szr = inverse(sz, MOD);
  for(int i = 0; i < sz; i++)
    v[i] = (int)((long long)v[i] * (long long)szr % MOD); 
}

//vector<int> truncated_multiply(const vector<int> &a, const vector<int> &b, int rsz) {
//  int sz = a.size()+b.size()-1;
//  while(sz & (sz-1))
//    sz += (sz & (-sz));
//  vector<int> va(a);
//  vector<int> vb(b);
//  vector<int> vc;
//  va.resize(sz, 0);
//  vb.resize(sz, 0);
//  assert((MOD-1) % sz == 0); 
//  int w = power_mod(3, (MOD-1)/sz);
//  int wr = inverse(w, MOD);
//
//  FFT(va, w);
//  FFT(vb, w);
//  for(int i = 0; i < sz; i++)
//    vc.push_back((int)((long long)va[i] * (long long)vb[i] % MOD));
//  IFFT(vc, wr);
//  if(sz > rsz)
//    vc.resize(rsz);
//  return vc;
//}

// T[n, k] = coeff((c0+c1*x+c2*x^2+...)^k), x, n)
int euler537(int n, int k0)
{
  assert(n+1 <= prm.size()); 
  int k = k0;
  int sz = 2*n+1;
  while(sz & (sz-1))
    sz += (sz & (-sz));
  int w = power_mod(3, (MOD-1)/sz);
  int wr = inverse(w, MOD);
  vector<int> c(prm.begin(), prm.begin()+n+1);
  vector<int> r(1, 1);
  while(k > 0) {
    c.resize(sz, 0);
    FFT(c, w);
    if(k & 1) {
      r.resize(sz, 0);
      FFT(r, w);
      for(int i = 0; i < sz; i++)
        r[i] = (int)((long long)r[i] * (long long)c[i] % MOD);
      IFFT(r, wr);
      r.resize(n+1);
    }
    for(int i = 0; i < sz; i++)
      c[i] = (int)((long long)c[i] * (long long)c[i] % MOD);
    IFFT(c, wr);
    c.resize(n+1);
    k >>= 1;
  }
  printf("euler537(%d, %d) = %d\n", n, k0, r[n]);
  return r[n];
}

int main()
{
  prep();
  euler537(10, 10);
  euler537(1000, 1000);
  euler537(20000, 20000);
  return 0;
}
