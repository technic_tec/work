def power_mod(n, k, p):
    r = 1
    while k > 0:
        if k & 1:
            r = r * n % p
        n = n * n % p
        k >>= 1
    return r

def witness(n, p):
    t, k = n-1, 0
    while (t & 1) == 0:
        t >>= 1
        k += 1
    r = power_mod(p, t, n)
    if r == 1 or r == n-1:
        return False
    for _ in range(k):
        r = r * r % n
        if r == n-1:
            return False
        elif r == 1:
            return True
    return True

def is_prime(n):
    if n in [0, 1]:
        return False
    for p in [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]:
        if n == p:
            return True
        elif n % p == 0:
            return False
    if n < 1373653L:
        l = [2, 3]
    elif n < 9080191L:
        l = [31, 73]
    elif n < 4759123141L:
        l = [2, 7, 61]
    elif n < 1122004669633L:
        l = [2, 13, 23, 1662803]
    elif n < 2152302898747L:
        l = [2, 3, 5, 7, 11]
    elif n < 3474749660383L:
        l = [2, 3, 5, 7, 11, 13]
    elif n < 341550071728321L:
        l = [2, 3, 5, 7, 11, 13, 17]
    elif n < 3825123056546413051L:
        l = [2, 3, 5, 7, 11, 13, 17, 19, 23]
    else:
        raise ValueError
    for p in l:
        if witness(n, p):
            return False
    return True

def HammingNumber(n):
  v = [1]
  for p in [2, 3, 5]:
    s, t = 0, len(v)
    while s < t:
      for i in range(s, t):
        if v[i]*p <= n:
          v.append(v[i]*p)
      s, t = t, len(v)
  #v.sort()
  return v

def expand(ham, prm, N):
  for p in prm:
    n = len(ham)
    for x in ham[:n]:
      y = x * p
      if y <= N:
        ham.append(y)

def gen(prm, N):
  if N<=0:
    return
  elif len(prm) == 0:
    yield 1
    return
  for x in gen(prm[:-1], N/prm[-1]):
    yield x*prm[-1]
  for x in gen(prm[:-1], N):
    yield x

def euler516(N, K):
  ham = HammingNumber(N)
  prm = [x+1 for x in ham if is_prime(x+1)]
  expand(ham, prm[3:K], N)
  rem = list(gen(prm[K:], N))
  ham.sort()
  rem.sort()
  sr = [0]
  for x in rem:
    sr.append(sr[-1]+x)
  j = len(rem)
  s = 0
  for x in ham:
    while j > 0 and x*rem[j-1] > N:
      j -= 1
    if j == 0:
      break
    s += x*sr[j]
  print '%d: %d' % (N, s)
  return s

euler516(100, 5)
euler516(10**12, 12)
