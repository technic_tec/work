-- a(1) = 1                     |  1 -> 1
-- a(3) = 3                     | 11 -> 11
-- a(2n) = a(n)                 | x0 -> 0x'
-- a(4n+1) = 2*a(2n+1) - a(n)   | x01-> 10x' = 1x'*2-x'
-- a(4n+3) = 3*a(2n+1) - 2*a(n) | x11-> 11x' = 1x'*3-2x'

-- s(4n)   = 6*s(2n) - 5*s(n) - 3*s(n-1) - 1
-- s(4n+1) = 2*s(2n+1) + 4*s(2n) - 6*s(n) - 2*s(n-1) - 1
-- s(4n+2) = 3*s(2n+1) + 3*s(2n) - 6*s(n) - 2*s(n-1) - 1
-- s(4n+3) = 6*s(2n+1) - 8*s(n) - 1
-- (x00, x01, x10, x11) -> (x0, x1, x, x)

-- [s(2n), s(2n+1), s(4n), s(4n+1, s(4n+2), s(4n+3), a(4n), a(4n+1), a(4n+2), a(4n+3)]
-- depends on [s(2n+1), s(2n), s(n), a(2n+1), a(n)]
s4 :: Integer -> [Integer]
s4 0 = [0, 1, 0, 1, 2, 5, 0, 1, 1, 3]
s4 n = [sp0', sp1', s0', s1', s2', s3', a0', a1', a2', a3']
    where [sp0, sp1, s0, s1, s2, s3, a0, a1, a2, a3] = s4 (div n 2)
          [z, x, y, u, v] = [[sp0, s0, s1, a0, a1], [sp1, s2, s3, a2, a3]] !! (fromInteger $ mod n 2)
          [a0', a1', a2', a3'] = [u, 2*v-u, v, 3*v-2*u]
          [s0', s1', s2', s3'] = [6*x-8*z+3*u-1, 2*y+4*x-8*z+2*u-1, 3*y+3*x-8*z+2*u-1, 6*y-8*z-1]
          [sp0', sp1'] = [x, y]

s :: Integer->Integer
s n = (s4 (div n 4))!!(fromInteger (2+(mod n 4)))

main = do
  putStrLn $ show $ s 8
  putStrLn $ show $ s 100
  putStrLn $ show $ s (3^37)
