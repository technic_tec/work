def gen_prime(N):
  isp = [True] * N
  isp[0] = isp[1] = False
  prm = []
  for i in range(2, N):
    if not isp[i]:
      continue
    prm.append(i)
    for j in range(i*i, N, i):
      isp[j] = False
  return prm

def enum_6k1(N, prm, k=5, off=0):
  if k == 0:
    yield 1
    return
  elif off+k-1 >= len(prm) or reduce(lambda x, y: x*y, prm[off:off+k]) > N:
    return
  while off < len(prm) and prm[off] <= N:
    p = x = prm[off]
    while x <= N:
      if x == 3:
        k1 = k
      else:
        k1 = k-1
      for y in enum_6k1(N/x, prm, k1, off+1):
        yield x*y
      x *= p
    off += 1

def count_remain(N, prm):
  v = [True]*(N+1)
  for p in prm:
    if p > N:
      break
    for j in range(p, N+1, p):
      v[j] = False
  c = [0]
  for i in range(1, N+1):
    if v[i]:
      c.append(c[-1]+i)
    else:
      c.append(c[-1])
  return c

def euler242(N):
  prm0 = gen_prime(N/(7*9*13*19)+1)
  prm = filter(lambda x: x % 3 != 2, prm0)
  print 'prm: %d/%d [%d, %d, %d, ...]' % (len(prm), len(prm0), prm[0], prm[1], prm[2])
  p6k1 = list()
  for x in enum_6k1(N, prm):
    p6k1.append(x)
    if (len(p6k1) & 0x1ffff) == 0:
      print len(p6k1), x
  print '6k1: %d' % len(p6k1)
  s = count_remain(N/(7*9*13*19*31), prm)
  print 'summing...'
  res = sum(x*s[N/x] for x in p6k1)
  print res

euler242(10**11)
