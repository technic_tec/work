from math import *

def ilog(n, b):
  r = log(n, b)
  ir = int(floor(r)+0.5)
  while(b**(ir+1)<=n):
    ir += 1
  return ir

def iroot(n, k):
  r = exp(log(n)/k)
  ir = int(floor(r)+0.5)
  while((ir+1)**k<=n):
    ir += 1
  return ir
  
# b^k <= n => k*ln b < = ln n => k <= ln n / ln b && b <= e^(ln n / k)
def S(n):
  res = (0, -1)
  k=ilog(n, 2)
  while k >= 2:
    q = iroot(n, k)
    # v(n, q, k) = (n-n%q^k)*((1-1/q)^(k+1)-1)/((1-1/q)-1) <= n*(q^(k+1)-1)/(q-1)
    while q > 1:
      qk = q**k
      rat = qk*q-(q-1)**(k+1)
      if n*rat/qk < res[0]:
        break
      rx = (n/qk)*rat
      if rx > res[0]:
        #print rx, q, k, n/qk*qk
        res = (rx, n-n%qk)
      q -= 1
    k -= 1
  return res

def bf(N):
  v = dict()
  for q in range(2, N):
    for n in range(q**2, N, q**2):
      s = x = n
      while x % q == 0:
        x /= q
        x *= q-1
        s += x
      if s not in v:
        v.setdefault(s, set()).add((n, q))
  lst = -1
  for nq, s in sorted((list(nq), s) for s, nq in v.iteritems()):
    if s > lst:
      print s, nq
      lst = s

def euler542(n):
  s = 0
  n0 = n
  while(n >= 4):
    res = S(n)
    #print 'S(%d): %s' % (n, res)
    if (n-res[1]+1) & 1:
      s += (-1)**(n & 1) * res[0]
    n = res[1]-1
  print 'T(%d)=%d' % (n0, s)
  return s

map(euler542,[1000, 10**17])
