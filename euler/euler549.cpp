#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;
#define N 100000000

int main()
{
  vector<int> p(N+1,1);
  long long s = 0;

  for(int n = 2; n <= N; n++) {
    if(p[n] == 1) {
      int nn = n, c = 1;
      while(nn <= N) {
        int x = n*c;
        for(int j = nn; j <= N; j += nn)
          p[j] = max(p[j],x);
        while(x % n == 0) {
          long long vn = (long long)nn * (long long)n;
          nn = (int)min(vn,(long long)(N+1));
					x /= n;
        }
				++c;
      }
    }
    s += p[n];
    if(n == 10 || n == 25)
      printf("s(%d) = %d\n", n, p[n]);
    else if(n == 100)
      printf("S(%d) = %lld\n", n, s);
  }
  printf("S(%d) = %lld\n", N, s);

  return 0;
}
