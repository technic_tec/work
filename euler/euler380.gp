T1 = (n, m) -> {
  my(expand = ((tr, n) -> x=matsize(tr);while(matsize(tr)[1]<n, tr=concat(tr, vector(x[2])));if(x[2]<n, tr=concat(tr, matrix(n, n-x[2])));tr), 
  T0=matrix(n, n, i, j, if(i==j, if(i==1 || i==n, 2.0, 3.0), -1.0*(abs(i-j)==1))),
  T=matrix(n, n, i, j, if(i==j, if(i==1 || i==n, 3.0, 4.0), -1.0*(abs(i-j)==1))),
  D=T0[^n, ^n],
  r=matdet(D));
  for(i=1, m-2, D=T-expand(D^(-1), n);r*=matdet(D));
  D=T0-expand(D^(-1), n); r *= matdet(D);
  r}
T = (n, m) -> if(n>m, T(m, n), if(n==1, 1.0, T1(n, m)))
T(1, 1)
T(2, 2)
T(3, 4)
T(9, 12)
T(100, 500)
