#
g(x, y) = if(y==0, 0, y^3*asinh(x/abs(y)))
f00(x, y) = (g(x, y)+g(y, x)+2*x*y*sqrt(x^2+y^2))/6        /* integrate(integrate(sqrt(x^2+y^2), x), y) */
f01(x, y) = (3*y*g(x, y)+(5*x*y^2+2*x^3)*sqrt(x^2+y^2))/24 /* integrate(integrate(y*sqrt(x^2+y^2), x), y) */
f11(x, y) = (x^2+y^2)^(5/2)/15                             /* integrate(integrate(x*y*sqrt(x^2+y^2), x), y) */
f(a, b, c, d, x, y) = a*c*vf11[x+voff, y+voff] + a*d*vf01[y+voff,x+voff] + b*c*vf01[x+voff, y+voff] + b*d*vf00[x+voff, y+voff] /* (ax+b)*(cy+d) */
fn(px, py) = my(a=px[1], b=px[2], xl=px[3], xh=px[4], c=py[1], d=py[2], yl=py[3], yh=py[4]);f(a, b, c, d, xh, yh)-f(a, b, c, d, xh, yl)-f(a, b, c, d, xl, yh)+f(a, b, c, d, xl, yl)
rect_area(x) = (x[3]-x[1])*(x[4]-x[2])
prop(xl, xr, yl, yr) = s=(xr-xl)*(yr-yl);h=min(xr-xl, yr-yl);w=max(xr-xl, yr-yl);lo=xl-yr;hi=xr-yl;[[1/s, -lo/s, lo, lo+h], [0, h/s, lo+h, hi-h], [-1/s,hi/s, hi-h, hi]]
avl(rx, ry) = my(pxs=prop(rx[1], rx[3], ry[1], ry[3]), pys=prop(rx[2], rx[4], ry[2], ry[4])); sum(i=1, length(pxs), sum(j=1, length(pys), fn(pxs[i], pys[j])))
avs(n, l, b, r, t) = vs=[[0, 0, l, t], [l, 0, n, b], [r, b, n, n], [0, t, r, n]]; sum(i=1, length(vs), sum(j=1, length(vs), avl(vs[i], vs[j])*rect_area(vs[i])*rect_area(vs[j])/(n^2-rect_area([l, b, r, t]))^2))
print(avl([0, 0, 1, 1], [0, 0, 1, 1]))
print(avl([0, 0, 2, 3], [0, 0, 2, 3]))
euler517(n)= local(voff = n+1, vf00=matrix(2*n+1, 2*n+1, i, j, f00(i-n-1, j-n-1)),vf01=matrix(2*n+1, 2*n+1, i, j, f01(i-n-1, j-n-1)),vf11=matrix(2*n+1, 2*n+1, i, j, f11(i-n-1, j-n-1))); sum(l=1, n-2, sum(b=1, n-2, print([n, l, b]);sum(r=l+1, n-1, sum(t=b+1, n-1, avs(n, l, b, r, t)))))
print(euler517(3))
print(euler517(4))
print(euler517(40))
