/* a=3k-1, b^2*c=(8k-3)k^2, a+b+c<=n */
euler251 = (n) -> {
  my(N=16224760, cnt = 0);
  for(k=1, N,
    my(a=3*k-1, u, v, d, i);
    if(a>n, break);
    [v, u] = core(8*k-3, 1);
    u *= k;
    v = (8*k-3)*k^2;
    d=divisors(u);
    i=setsearch(d, k);
    for(j=i, length(d), my(b=d[j], c=v/b^2);if(a+b+c<=n, cnt += 1, if(b>2*c, break)));
    for(j=1, i-1, my(b=d[i-j], c=v/b^2);if(a+b+c<=n, cnt += 1, if(b<2*c, break))));
  cnt
}
euler251(1000)
euler251(110000000)
##
