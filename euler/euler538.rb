require 'rbtree'

tr = RBTree.new

def bits(n)
  c = 0
  while n > 0
    n &= n-1
    c += 1
  end
  c
end

def u(n)
  2**bits(3*n)+3**bits(n)+bits(n+1)
end

def area(a, b, c, d)
  s = a+b+c+d
  if s > 2*[a, b, c, d].max
    (s-2*a)*(s-2*b)*(s-2*c)*(s-2*d)
  else
    0
  end
end

vs = MultiRBTree.new
(1..3).each {|n|
  vs[u(n)] = 1
}

s = f = a = 0
(4..3000000).each {|n|
  v = u(n)
  vs[v] = 1
  cand = []
  vs.bound(v) {|key, val|
    break if cand.size == 4
    cand.push key
  }
  l0 = cand.size
  i = v
  while cand.size < 4
    i = vs.upper_bound (i-1)
    break unless i
    i = i[0]
    vs.bound(i) {|key, val|
      break if cand.size == 4
      cand.unshift key
    }
  end
  l0 = cand.size - l0 + 4
  i = v
  while cand.size < l0
    i = vs.lower_bound (i+1)
    break unless i
    i = i[0]
    vs.bound(i) {|key, val|
      break if cand.size == l0
      cand.push key
    }
  end
  (3..cand.size-1).each {|i|
    ax = area(cand[i-3], cand[i-2], cand[i-1], cand[i])
    if ax > a
      a, f = ax, cand[i-3]+cand[i-2]+cand[i-1]+cand[i]
    end
  }
  s += f
  if [5, 10, 150].member?(n)
    print "#{n}: #{f} #{s}\n"
  end
}
print "#{f} #{s}\n"
