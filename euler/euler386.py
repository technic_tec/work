p = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
def gen(n, l = (), v = 1):
  yield (v, l)
  k = len(l)
  ci = 1
  v *= p[k]
  while v <= n and (k==0 or ci <= l[-1]):
    for x in gen(n, l+(ci,), v):
      yield x
    ci += 1
    v *= p[k]

def graph(l):
  if len(l) == 0:
    return ([], 1)
  le, vc = graph(l[:-1])
  k = len(le)
  for i in range(l[-1]*k):
    le.append((le[-k][0]+vc, le[-k][1]+vc))
  for i in range(vc, (l[-1]+1)*vc):
    le.append((i-vc, i))
  vc *= l[-1]+1
  return (le, vc)

for i, (v, l) in enumerate(gen(10**8)):
  e, v = graph(l)
  c = antichain(v, e)
  print i, v, l
