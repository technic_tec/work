P = (41**2)
Q = (271**2)
Pr = 1966
N = 10**14

def dsum(n):
  s = 0
  while n > 0:
    s += n % 10
    n /= 10
  return s

def S(n, p):
  #n*(n+1)/2 % 15 = 0, 1, 3, 6, 10,  0,  6, 13,   6,  0,  10,    6,    3,    1,    0
  #n*(n-1)/2 % 15 = 0, 0, 1, 3,  6, 10,  0,  6,  13,  6,   0,   10,    6,    3,    1
  #                    1  2  3   4  32 123  43 2123 432 1234 32123 43212 34321 23432
  res = 0
  vl = dict({
      123432:{0, 32, 432, 23432},
      234321:{1, 34321},
      343212:{2, 43212},
      432123:{3, 123, 2123, 32123},
      321234:{4, 1234},
      212343:{43}
      })
  for k in vl:
    for x in vl[k]:
      vk = [x%p]
      while True:
        r = (vk[-1]*1000000+k)%p
        if r != vk[0]:
          vk.append(r)
        else:
          break
      c = n / 15
      if n % 15 >= dsum(x):
        c += 1
      res += c/len(vk)*sum(vk)+sum(vk[:c%len(vk)])
      #print p, x, c, res
  return res % p
  
 
def euler506(n, ans=None):
  x = S(n, P)
  y = S(n, Q)
  # r % P == x,  r % Q == y
  r = ((y-x)*Pr%Q)*P+x
  if ans is None:
    print '%d: %d' % (n, r)
  else:
    print '%d: %d (ans %d)' % (n, r, ans)
  return r

euler506(11, 36120)
euler506(1000, 18232686)
euler506(10**14)
