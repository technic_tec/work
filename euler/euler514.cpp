/*
 * =====================================================================================
 *
 *       Filename:  euler514.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年06月19日 14时16分44秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

void gen_coprime(vector<pair<int, int> > &cop, int Nx, int Ny, int Lx = 1, int Ly = 0, int Rx = 0, int Ry = 1)
{
  int Mx = Lx+Rx, My = Ly+Ry;
  if(Mx > Nx || My > Ny) {
    if(Lx <= Nx && Ly <= Ny)
      cop.push_back(make_pair(Lx,Ly));
  } else {
    gen_coprime(cop, Nx, Ny, Lx, Ly, Mx, My);
    gen_coprime(cop, Nx, Ny, Mx, My, Rx, Ry);
  }
}

double E(int n, double ans = -1.0)
{
  double res = 0.0;
  double px = 1.0-1.0/(n+1), pp = 1.0-(px+(n+1)*(n+1)*(1.0-px))*pow(px,(n+1)*(n+1)-1); // pp -- probability of nonzero hole area.
  for(int x = 0; x <= n; x++)
    for(int y = 0; y <= n; y++) {
      vector<pair<int, int> > cop1, cop2;
      int c0 = 0, cu = (n+1)*(n-y)+n-x, c1 = 0, cd = (n+1)*y+x;
      double pe = 0.0, pi = 1.0-(px+(n+1)*(n+1)*(1.0-px))*pow(px,(n+1)*(n+1)-1); // pi/pe -- probability of current lattice is internal/boundary lattice
      gen_coprime(cop1, n-x,n-y);
      gen_coprime(cop2, x, y);
      for(int i = 0, j = 0; i < cop1.size() || j < cop2.size(); ) {
        if(j >= cop2.size() || (i < cop1.size() && cop1[i].second*cop2[j].first < cop2[j].second*cop1[i].first)) {
          c0 = min(cop1[i].first ? (n-x)/cop1[i].first : (n+1), cop1[i].second ? (n-y)/cop1[i].second : (n+1));
          c1 = 0;
          ++i;
        } else if(i >= cop1.size() || (j < cop2.size() && cop1[i].second*cop2[j].first > cop2[j].second*cop1[i].first)) {
          c0 = 0;
          c1 = min(cop2[j].first ? x/cop2[j].first : (n+1), cop2[j].second ? y/cop2[j].second : (n+1));
          ++j;
        } else {
          c0 = min(cop1[i].first ? (n-x)/cop1[i].first : (n+1), cop1[i].second ? (n-y)/cop1[i].second : (n+1));
          c1 = min(cop2[j].first ? x/cop2[j].first : (n+1), cop2[j].second ? y/cop2[j].second : (n+1));
          ++i; ++j;
        }
        cu -= c0; cd -= c1;
        double pud1 = pow(px,cd)*(1.0-pow(px,cu))*(1.0-pow(px,c0));
        double pud2 = (1.0-pow(px,cd))*pow(px,cu)*(1.0-pow(px,c1));
        pe += pud1*(1.0-pow(px,c1))+pud2*(1.0-pow(px,c0));
        pe += pud1*pow(px,c1)*(1.0-px)+pud2*pow(px,c0)*(1.0-px);
        pi -= pud1*pow(px,c1)*px+pud2*pow(px,c0)*px + pow(px, cu+cd)*(1-pow(px, c0+c1+1)-pow(px, c0+c1)*(1-px)*(c0+c1+1));
        pp -= pow(px,c1+cu+cd)*(1.0-pow(px,c0))*(1.0-px);
//        printf("(%d,%d)[%d,%d] pi %lf pe %lf\n", x, y, c0, c1, pi, pe);
        cu += c1; cd += c0;
      }
      vector<pair<int, int> > cop3, cop4;
      gen_coprime(cop3, n-y,x);
      gen_coprime(cop4, y,n-x);
      for(int i = 0, j = 0; i < cop3.size() || j < cop4.size(); ) {
        if(j >= cop4.size() || (i < cop3.size() && cop3[i].second*cop4[j].first < cop4[j].second*cop3[i].first)) {
          c0 = min(cop3[i].first ? (n-y)/cop3[i].first : (n+1), cop3[i].second ? x/cop3[i].second : (n+1));
          c1 = 0;
          ++i;
        } else if(i >= cop3.size() || (j < cop4.size() && cop3[i].second*cop4[j].first > cop4[j].second*cop3[i].first)) {
          c0 = 0;
          c1 = min(cop4[j].first ? y/cop4[j].first : (n+1), cop4[j].second ? (n-x)/cop4[j].second : (n+1));
          ++j;
        } else {
          c0 = min(cop3[i].first ? (n-y)/cop3[i].first : (n+1), cop3[i].second ? x/cop3[i].second : (n+1));
          c1 = min(cop4[j].first ? y/cop4[j].first : (n+1), cop4[j].second ? (n-x)/cop4[j].second : (n+1));
          ++i; ++j;
        }
        cu -= c0; cd -= c1;
        double pud1 = pow(px,cd)*(1.0-pow(px,cu))*(1.0-pow(px,c0));
        double pud2 = (1.0-pow(px,cd))*pow(px,cu)*(1.0-pow(px,c1));
        pe += pud1*(1.0-pow(px,c1))+pud2*(1.0-pow(px,c0));
        pe += pud1*pow(px,c1)*(1.0-px)+pud2*pow(px,c0)*(1.0-px);
        pi -= pud1*pow(px,c1)*px+pud2*pow(px,c0)*px + pow(px, cu+cd)*(1-pow(px, c0+c1+1)-pow(px, c0+c1)*(1-px)*(c0+c1+1));
        pp -= pow(px,c1+cu+cd)*(1.0-pow(px,c0))*(1.0-px);
//        printf("(%d,%d)[%d,%d] pi %lf pe %lf\n", x, y, c0, c1, pi, pe);
        cu += c1; cd += c0;
      }
      pi -= pe;
      res += pi + pe*0.5;
//      printf("(%d,%d) pi %lf pe %lf\n", x, y, pi, pe);
    }
  res -= pp;
//  printf("(%d) pp %lf\n", n, pp);
  if(ans < 0)
    printf("%d: %.5lf\n", n, res);
  else
    printf("%d: %.5lf (ans %.5lf)\n", n, res, ans);
  return res;
}

int main()
{
  E(1, 0.18750);
  E(2, 0.94335);
  E(10, 55.03013);
  E(100);
  return 0;
}
