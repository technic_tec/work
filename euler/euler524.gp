p = (s) -> {
  n = floor(log(s)/log(2));
  v = List();
  res = List();
  stk = List();
  zc = sum(i=0, n, if(bitand(s>>i, 1), 0, 1));
  lv = 0;
  i = 1;
  while(length(v)<=n+1, 
      if(bitand(s, -s) == (1<<if(lv>i, length(v)-1, length(v))),
        listput(stk, [i, length(v)+1]);
        s=bitand(s, s-1);
        i+=1,
        if(length(stk)>0 && stk[length(stk)][1] < lv,
            it = stk[length(stk)];
            listpop(stk);
            listinsert(v, it[1], it[2]);
            listput(res, it[1]),
            if(zc > 0,
              listput(v, i);
              listput(res, i);
              lv = i;
              zc -= 1;
              i += 1,
              listput(v, n+2);
              listput(res, n+2);
              lv=n+2))));
  res
};
pidx = (v) -> {
  n=length(v);
  s=vector(n, x, x);
  r=1;
  for(i=1, n,
    k=setsearch(s, v[i]);
    r+=(k-1)*(n-i)!;
    s=setminus(s, [v[i]]));
  r
};
cst = (v) -> {
  p = List();
  sum(i = 1, length(v),
    k = setsearch(p, v[i], 1);
    listinsert(p, v[i], k);
    if(k >= length(p), 0, 2^(k-1)))
};
vr = p(12^12);
vi = pidx(vr);
print(Vec(vr));
print(cst(vr), ", ", 12^12);
print(vi)
