euler546 = ((k, n) -> {
  my(c = 0, x = n);
  while(x > 0, x \= k; c += 1);
  local(tr = matrix(c+1, c+1, i, j, if(j==c+1, 1, -1)), coeff = List([['r-k, 'r*0+k]]));
  local(f = ((n, d, h)->if(tr[d, h] < 0, tr[d, h] = sum(i=1, d+1, while(length(coeff)<d, my(cl=coeff[length(coeff)],cx=vector(length(cl), i, sumformal(cl[i], 'r)),cc=vector(length(cl)+1, i, if(i>1, subst(cx[i-1], 'r, k), 0)+if(i<=length(cl), cx[i]-subst(cx[i], 'r, k), 0)+0*'r));listput(coeff, cc));subst(coeff[d][i], 'r, (n%k+1))*f(n\k, i, h+1)));tr[d, h]));
  f(n, 1, 1)
})
euler546(5, 10)
euler546(7, 100)
euler546(2, 1000)
sum(k=2, 10, euler546(k, 10^14))%(10^9+7)
