from time import time
import sys

prm = [2,  3,  5,  7,  11,  13,  17,  19,  23,  29,  31,  37,  41, 43]
N = 10**16

def genmodel(v, m):
  r = [(v, tuple(m))]
  p = prm[len(m)]
  v *= p
  m.append(1)
  while(v<=N and (len(m)<=1 or m[-1] <= m[-2])):
    r.extend(genmodel(v, m))
    v *= p
    m[-1] += 1
  m.pop()
  return r

sub = dict({tuple(): dict({tuple(): 1})})

def submodel(m):
  if m not in sub:
    ms = dict()
    for mx, cx in submodel(m[:-1]).iteritems():
      ms[mx] = ms.get(mx, 0) + cx
      ix = len(mx)
      for v in range(1, m[-1]+1):
        while ix > 0 and mx[ix-1] < v:
          ix -= 1
        my = mx[:ix]+(v,)+mx[ix:]
        ms[my] = ms.get(my, 0) + cx
    sub[m] = ms
  return sub[m]

cst = dict({tuple(): 1})
def cost(m):
  if m not in cst:
    cst[m] = s = 0
    for mx, cx in submodel(m).iteritems():
      s += cost(mx)*cx
    cst[m] = s
  return cst[m]

t1 = time()
models = genmodel(1, [])
t1d = time()
sys.stderr.write('%d %f\n' % (len(models), t1d-t1))
t2 = time()
ss = sum(len(submodel(m)) for v, m in models)
t2d = time()
sys.stderr.write('%d %f\n' % (ss, t2d-t2))
t3 = time()
cs = sum(cost(m) for v, m in models)
t3d = time()
sys.stderr.write('%d %f\n' % (cs, t3d-t3))
print "modval(v) = prod(x=1, length(v), prime(x)^v[x]);\ns=0;"
print "model(n) = if(n==1, [], vecsort(factorint(n)~[2,], , 4));"
for v, m in models:
  cm = cost(m)
  print "if(%d<=10^16 && modval(model(%d))==%d, print(%d); s += %d);" % (cm, cm, v, cm, cm)
print "print(\"s=\", s);"
