#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;

#define N  100000
#define P  1953125
#define Q  512
#define FP 1562500
#define FQ 256

void prep()
{
    for(int n = 1, k = 0; k==0 || n != 1; n = n * 2 % P, k++) {
        // 2^k == n (mod P), 2^(ux) == x -> x % P == n && u*x % FP == k
    }
    for(int n = 1, k = 0; k==0 || n != 1; n = n * 5 % Q, k++) {
        // 5^k == n (mod Q), 5^(vx) == x -> x % Q
    }
}

int power_mod(int n, int k, int p)
{
  int r = 1;
  while(k) {
    if(k & 1)
      r = (int)((long long)r * (long long)n % p);
    n = (int)((long long)n * (long long)n % p);
    k >>= 1;
  }
  return r;
}
// n^x == x (mod 10^9), x < 10^9
// 1. 2|n => 2^min(x,9) | x => x >= 9 && 2^9 | x
// 2. 5|n => 5^min(x,9) | x => x >= 9 && 5^9 | x
// 3. n = 2^u (mod 5^9) && n = 5^v or 3*5^v (mod 2^9)
//    => 2^(ux)==x (mod 5^9) && (5^(vx)==x || 3^x*5^(vx) == x)  (0<=u<4*5^8, 0<=v<2^7)
int euler455(int n)
{
    if(n % 10 == 0)
        return 0;
    // n^x-x == 0 (mod 10^k) => n^x-x == 0 (mod 10^(k-1))
    vector<int> cl;
    for(int i = 0, x = 1; i < 20; i++, x = x * n % 10)
      if((i<10?i:(i-10)) == x)
        cl.push_back(i);
    for(int i = 2, b = 100; i <= 9; i++, b*=10) {
      vector<int> cc;
      for(int j = 0; j < cl.size(); j++) {
        int x = cl[j];
        // 10^x - x == 0 (mod b/10, x=0..b/5-1)
        // => x' = b/5*k+x, k=0..9
        int bs = power_mod(n, b/5, b);
        int r = power_mod(n, x, b);
        int x1 = x;
        for(int k = 0; k < 10; k++) {
          if((x1<b?x1:(x1-b)) == r)
            cc.push_back(x1);
          r = (int)((long long)r * (long long)bs % b);
          x1 += b/5;
        }
      }
      cl = cc;
    }
    int b = 1000000000;
    int m = 0;
    for(int i = 0; i < cl.size(); i++)
      if(cl[i] < b && cl[i] > m)
        m = cl[i];
    return m;
}

int main()
{
    prep();
    printf("4: %d\n", euler455(4));
    printf("10: %d\n", euler455(10));
    printf("157: %d\n", euler455(157));
    long long s = 0;
    for(int i = 2; i <= 1000; i++)
        s += euler455(i);
    printf("1000: %lld\n", s);
    for(int i = 1001; i <= 1000000; i++)
        s += euler455(i);
    printf("1000000: %lld\n", s);
    return 0;
}

