P = 1000000007

/* 1. from column (w-1) to column w: (h, eo) <- (h+x, eo) | (h-2x, eo) | (h-2x-1, !eo) */
f1s(w, h) = {
	my(m1, m = matrix(2*h, 2*h, i, j, ((0==(i+j)%2 && (ceil(i/2)<=ceil(j/2) || 0==(ceil(i/2)-ceil(j/2))%2)) || ((i+j)%2 == 1 && ceil(i/2)>ceil(j/2) && (ceil(i/2)-ceil(j/2))%2 == 1))));
	m1 = Mod(m, P)^(w-1);
	vector(2*h, i, i%2==0)*m1*vectorv(2*h, i, i%4==0 || i%4==1)
};
f1(w, h) = if(h<=1, 0, f1s(w, h)-f1s(w, h-1))
/* 2. e(2*k-1) <- sum(i=1, ceil(h/2), e(2*i-1)) + sum(i=1, floor(h/2), e(2*i)) - sum(i=1, k-1, e(2*i)-o(2*i))
      o(2*k-1) <- sum(i=1, ceil(h/2), o(2*i-1)) + sum(i=1, floor(h/2), o(2*i)) + sum(i=1, k-1, e(2*i)-o(2*i))
      e(2*k) <- sum(i=1, ceil(h/2), e(2*i-1)) + sum(i=1, floor(h/2), e(2*i)) - sum(i=1, k, e(2*i-1)-o(2*i-1))
      o(2*k) <- sum(i=1, ceil(h/2), o(2*i-1)) + sum(i=1, floor(h/2), o(2*i)) + sum(i=1, k, e(2*i-1)-o(2*i-1))
      w=1 => e(2k) = o(2k-1) = 1, e(2k-1) = o(2k) = 0
 */
extendf2(w, hu, hd, fl) = {
  my(n=length(fl), see=fl[n][1], seo=fl[n][2], soe=fl[n][3], soo=fl[n][4]);
  for(i=n+1, w,
      my(ee, eo, oe, oo);
      eo = subst(seo, 'k, hu) + subst(see, 'k, hd) - subst(see, 'k, 'k-1) + subst(soe, 'k, 'k-1);
      oo = subst(soo, 'k, hu) + subst(soe, 'k, hd) + subst(see, 'k, 'k-1) - subst(soe, 'k, 'k-1);
      ee = subst(seo, 'k, hu) + subst(see, 'k, hd) - subst(seo, 'k, 'k) + subst(soo, 'k, 'k);
      oe = subst(soo, 'k, hu) + subst(soe, 'k, hd) + subst(seo, 'k, 'k) - subst(soo, 'k, 'k);
      see=sumformal(ee, 'k);
      seo=sumformal(eo, 'k);
      soe=sumformal(oe, 'k);
      soo=sumformal(oo, 'k);
      listput(fl, [see, seo, soe, soo]));
	fl
};
genf2(w, hu, hd, fl) = subst(fl[w][2], 'k, hu) + subst(fl[w][1], 'k, hd);
f2le = List(Mod([['k, 0, 0, 'k]], P)); f2lo = List(Mod([['k, 0, 0, 'k]], P));
f2(w, h) = if(h<=1, 0, {
	f2lo = extendf2(w, 'u, 'u-1, f2lo);
	f2le = extendf2(w, 'u, 'u, f2le);
	my(f2o=genf2(w, 'u, 'u-1, f2lo), f2e=genf2(w, 'u, 'u, f2le), k=ceil(h/2));
	if(h%2==1, subst(f2o, 'u, k)-subst(f2e, 'u, k-1), subst(f2e, 'u, k)-subst(f2o,'u, k))
})
/* 3. c[i][j][k] = c[i-1][h][k]+c[i-1][h-1][k]+...+c[i-1][j][k] + c[i-1][j-1][1-k]+c[i-1][j-2][k]+c[i-1][j-3][1-k]+...
    i=1..w, j=1..h, k=0..1 */
f3s(w, h) = {
	se = Mod(vector(h, i, if(i%2==0, floor(i/2), 0)), P);
	so = Mod(vector(h, i, if(i%2==0, 0, ceil(i/2))), P);
	te = if(h<=1, 0, se[h-1])+se[h];
	to = if(h<=1, 0, so[h-1])+so[h];
	for(i=2, w,
		sel = sol = 0;
		for(j=1, h,
			sec = if(j<=2, 0, se[j-2]) + te - sel + sol;
			soc = if(j<=2, 0, so[j-2]) + to - sol + sel;
			sel = se[j]; sol = so[j];
			se[j] = sec; so[j] = soc);
		te = if(h<=1, 0, se[h-1])+se[h];
		to = if(h<=1, 0, so[h-1])+so[h]);
	te
};
f3(w, h) = if(h<=1, 0, f3s(w, h)-f3s(w, h-1))

f(w, h) = my(r=List());if(h<=100, listput(r, f1(w, h))); if(w<=100, listput(r, f2(w, h))); if(w*h<=10^8, listput(r, f3(w, h))); Vec(r)

q = [2, 4; 4, 2; 13, 10; 10, 13; 100, 100; 10^12, 100; 100, 10^12; 10000, 10000]
nq = matsize(q)[1]
v = vector(nq)
for(i=1, nq, v[i] = f(q[i, 1], q[i, 2]); print(q[i, ], ": ", lift(v[i])))
print(lift(v[nq-2][1]+v[nq-1][1]+v[nq][1]))
