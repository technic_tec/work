def phigital(v, s):
  if len(v) == 0:
    yield 0
    return
  elif v[0] > s:
    return
  for x in phigital(v[1:], s):
    yield x
  for x in phigital(v[2+int(v[0]>2):], s-v[0]):
    yield v[0]+x

def euler473(n):
  fib = [0, 1]
  while len(fib) < 7 or fib[-1]+fib[-7] < n:
    fib.append(fib[-1]+fib[-2])
  phi = [fib[3]]
  for i in range(7, len(fib), 2):
    if fib[i] + fib[i-6] <= n:
      phi.append(fib[i] + fib[i-6])
  return list(phigital(phi, n))

l = euler473(1000)
print l, sum(l)+1
l = euler473(10**10)
print sum(l)+1
