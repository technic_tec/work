/*
 * =====================================================================================
 *
 *       Filename:  euler550.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016/03/ 5 21:49:30
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), technic.tec@gmail.com
 *   Organization:  Verisilicon, Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;
#define N 10000000
#define P 987654321

const int v[] = {0, 0, 1, 2, 4, 7, 8, 11, 13, 14, 16, 19, 21, 22, 25, 26, 28, 31, 32, 35, 37, 38, 41, 42, 44, 47, 49, 50, 52, 55, 56};
vector<int> pc;
int cm;
void prep() {
	vector<int> fac(N+1, 0);
	fac[1] = 1;
	pc.clear();
	pc.push_back(0);
	pc.push_back(0);
	cm = 1;
	for(int i = 2; i <= N; i++) {
		if(fac[i]) {
			pc.push_back(pc[i/fac[i]]+1);
			cm = max(cm, pc.back());
		} else {
			fac[i] = i;
			pc.push_back(1);
			if(i < 32768)
				for(int j = i*i; j <= N; j += i)
					if(!fac[j]) fac[j] = i;
		}
	}
}

int f(int n, long long k) {
	vector<int> c(cm+1, 0);
	for(int i = 2; i <= n; i++)
		c[pc[i]]++;
}

int main()
{
  return 0;
}


