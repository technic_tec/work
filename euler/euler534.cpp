/*
 * =====================================================================================
 *
 *       Filename:  euler534.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年03月02日 10时27分08秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>
#include <cassert>
#include <algorithm>
#include <vector>
#include <map>
#include <numeric>
using namespace std;

#define N 1000000009

typedef map<int, int> dict;
class Queen {
  public:
    Queen(int n) : m_sz(n), m_c(n, 0) {
      vector<int> head(1, -1);
      vector<int> tail(1, -1);
      vector<dict> tr(1, dict());
      for(int i = 0; i < n; i++) {
        head.push_back(i);
        tail.push_back(0);
        tr.push_back(dict());
        tr[0][i] = i+1;
      }
      m_c[n-1] = 1;
      for(int i = 0; i < n; i++)
        m_c[n-1] *= n;
      int s = 1, t = n+1;
      for(int h = 2; h <= n; h++) {
        int tn = t;
        for(int i = s; i < t; i++) {
          dict dx = tr[tail[i]];
          for(dict::iterator it = dx.begin(); it != dx.end(); ++it) {
            if(it->first == head[i] || abs(it->first - head[i]) == h-1)
              continue;
            int j = it->second;
            assert(j >= s && j < t);
            head.push_back(head[i]);
            tail.push_back(j);
            tr.push_back(dict());
            tr[i][it->first] = tn++;
            assert(head.size() == tn && tail.size() == tn);
          }
        }
        vector<long long> c(t-s, 1);
        for(int k = h; k <= n; k++) {
          vector<long long> cn(t-s, 0);
          for(int i = s; i < t; i++)
            for(dict::iterator it = tr[i].begin(); it != tr[i].end(); ++it)
              cn[tail[it->second]-s] += c[i-s];
          c.swap(cn);
        }

        m_c[n-h] = accumulate(c.begin(), c.end(), 0LL);
        s = t; t = tn;
        if(n >= 14)
          printf("%d: %d nodes,  %lld total\n", h, t-s, m_c[n-h]);
      }
    }
    long long Q(int w = 0) {
      return m_c[w];
    }
    long long S() {
      return accumulate(m_c.begin(), m_c.end(), 0LL);
    }
  private:
    int m_sz;
    vector<long long> m_c;
};

int main()
{
  Queen q4(4);
  printf("Q(4, 0) = %lld\n", q4.Q(0)); // 2
  printf("Q(4, 2) = %lld\n", q4.Q(2)); // 16
  printf("Q(4, 3) = %lld\n", q4.Q(3)); // 256
  printf("S(4) = %lld\n", q4.S());     // 276
  Queen q5(5);
  printf("S(5) = %lld\n", q5.S());     // 3347
  Queen q8(8);
  printf("Q(8, 0) = %lld\n", q8.Q(0)); // 92
  for(int n = 14; n <= 16; n++) {
    struct timespec tp0, tp1;
    clock_gettime(CLOCK_REALTIME, &tp0);
    Queen qn(n);
    long long s = qn.S();
    clock_gettime(CLOCK_REALTIME, &tp1);
    if(tp1.tv_nsec < tp0.tv_nsec) {
      tp1.tv_sec--;
      tp1.tv_nsec += 1000000000;
    }
    printf("S(%d) = %lld (%d.%09d)\n", n, s, tp1.tv_sec-tp0.tv_sec, tp1.tv_nsec-tp0.tv_nsec);
  }

  return 0;
}
