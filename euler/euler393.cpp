/*
 * =====================================================================================
 *
 *       Filename:  euler393.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年01月19日 14时23分55秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <map>
using namespace std;

#define N 1000000009

const int _ns[] = {1, 3, 9, 27, 81, 243, 729, 2187, 6561, 19683, 59049};

typedef map<int, long long> dict;

inline void inc(dict &c, int k, long long cx)
{
  dict::iterator it = c.find(k);
  if(it != c.end())
    c[k] += cx;
  else
    c[k] = cx;
}
long long euler393(int n)
{
  int ns = _ns[n];
  dict c;
  c[0] = 1;
  for(int x = 0; x < n; x++) {
    for(int y = 0; y < n; y++) {
      // fill (x, y)
      dict nc;
      for(dict::iterator it = c.begin(); it != c.end(); it++) {
        int st = it->first;
        long long cx = it->second;
        int abv = (st>>(2*y)) & 3;
        int lft = (st>>(2*y+2)) & 3;
        if(abv & lft) continue;
        st &= ~(0xf<<(2*y));
        int nst;
        switch(abv|lft) {
          case 0:
            nst = (st|(6<<(2*y))); // 0110
            inc(nc, nst, cx);
            nst = (st|(9<<(2*y))); // 1001
            inc(nc, nst, cx);
            break;
          case 1:
            nst = (st|(1<<(2*y))); // 0001
            inc(nc, nst, cx);
            nst = (st|(4<<(2*y))); // 0100
            inc(nc, nst, cx);
            break;
          case 2:
            nst = (st|(2<<(2*y))); // 0010
            inc(nc, nst, cx);
            nst = (st|(8<<(2*y))); // 1000
            inc(nc, nst, cx);
            break;
          case 3:
            nst = (st);  // 0000
            inc(nc, nst, cx);
            break;
        }
      }
      swap(c, nc);
    }
    // complete column x && start column (x+1)
    dict nc;
    for(dict::iterator it = c.begin(); it != c.end(); it++) {
      int st = it->first;
      long long cx = it->second;
      if((st >> (2*n)) == 0)
        inc(nc, st<<2, cx);
    }
    swap(c, nc);
  }
  return c[0];
}

int main()
{
  for(int i = 2; i <= 10; i+=2) {
    printf("%d: %lld\n", i, euler393(i));
  }
  return 0;
}


