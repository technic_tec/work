Q = ((N) -> {
    my(idx = ((x, y) -> if(x>=0 && y>=0 && x+y <= 5, x*(13-x)/2+y+1, 0)),
        n = idx(5, 0),
        idx2 = ((u, v) -> (u-1)*n+v),
        tr = matrix(n*n, n*n));
    for(e=0, 5,
      for(o=0, 5-e,
        my(i=idx(e, o));
        if(e>0, for(j=1, n, tr[idx2(i, j), idx2(idx(e-1, o+1), j)]=tr[idx2(j, i), idx2(j, idx(e-1, o+1))]=o+1));
        if(o>0, for(j=1, n, tr[idx2(i, j), idx2(idx(e+1, o-1), j)]=tr[idx2(j, i), idx2(j, idx(e+1, o-1))]=e+1; tr[idx2(i, j), idx2(idx(e, o-1), j)]=tr[idx2(j, i), idx2(j, idx(e, o-1))]=6-e-o))));
    tr[idx2(idx(0, 1), 1), idx2(1, 1)] -= 1;
    tr[idx2(1, 1), idx2(1, 1)] += 1;
    tr = Mod(tr, 1000000123);
    vector(n*n, x, (x-1)%n<=5 && setsearch([0, 6, 11, 15, 18, 20], (x-1)\n)>0)*(tr^N)*vectorv(n*n, x, x==1)-1
})
print(Q(7))
##
print(Q(100))
##
/*print(lift(sum(u=1, 39, r=Q(2^u);print(2^u, ", ", r);r)))*/
Qr = ((N) -> {
    my(idx = ((x, y) -> if(x>=0 && y>=0 && x+y <= 5, x*(13-x)/2+y+1, 0)),
        n = idx(5, 0),
        idx2 = ((u, v) -> (u-1)*n+v),
        tr = matrix(n*n, n*n));
    for(e=0, 5,
      for(o=0, 5-e,
        my(i=idx(e, o));
        if(e>0, for(j=1, n, tr[idx2(i, j), idx2(idx(e-1, o+1), j)]=tr[idx2(j, i), idx2(j, idx(e-1, o+1))]=o+1));
        if(o>0, for(j=1, n, tr[idx2(i, j), idx2(idx(e+1, o-1), j)]=tr[idx2(j, i), idx2(j, idx(e+1, o-1))]=e+1; tr[idx2(i, j), idx2(idx(e, o-1), j)]=tr[idx2(j, i), idx2(j, idx(e, o-1))]=6-e-o))));
    tr[idx2(idx(0, 1), 1), idx2(1, 1)] -= 1;
    tr[idx2(1, 1), idx2(1, 1)] += 1;
    tr = Mod(tr, 1000000123);
    v1 = vector(n*n, x, (x-1)%n<=5 && setsearch([0, 6, 11, 15, 18, 20], (x-1)\n)>0);
    v2 = vectorv(n*n, x, x==1);
    lift(sum(i=1, N, tr=tr^2;r=v1*tr*v2-1;print(i, ", ", r);r))
})
print(Qr(39))
##
