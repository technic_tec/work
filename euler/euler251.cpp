/*
 * =====================================================================================
 *
 *       Filename:  euler251.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年01月18日 16时52分47秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define P 1000000009
#define N 16224760
// p|8x-3, p|8y-3 => p|x-y

int fac[N];  // k
int fac8[N]; // 8k-3

int ext_gcd(int a, int b, int &x, int &y) {
  int d;
  if(b == 0) {
    x = 1; y = 0;
    d = a;
  } else {
    d = ext_gcd(b, a%b, y, x);
    y -= a/b*x;
  }
  return d;
}
void prep()
{
  memset(fac, 0, sizeof(fac));
  memset(fac8, 0, sizeof(fac8));
  fac[1] = 1;
  for(int i = 2; i < N; i+=2)
    fac[i] = 2;
  for(int i = 3; i < N; i+=2) {
    if(fac[i]) continue;
    fac[i] = i;
    if(i < 32768) {
      int j, k;
      for(j = i*i; j < N; j += i)
        if(!fac[j]) fac[j] = i;
      ext_gcd(8, i, j, k);
      j *= 3; j %= i;
      if(8*j-3 <= 0) j+=i;
      for(; j<N; j+=i) {
        if(!fac8[j]) fac8[j] = i;
        else if(8*j+3>= N*fac8[j])
          fac8[j] *= i;
      }
    }
  }
}
int main()
{
  // a=3k-1, b^2*c=(8k-3)k^2
  prep();
  for(int k = 1; k < N; k++) {
    int b = k, c = 8*k-3;
    vector<pair<int, int> > fc = factor(c);
    vector<pair<int, int> > fb = factor(k);
    vector<pair<int, int> > fcc;
    int cc = 1;
    for(int i = 0; i < fc.size(); i++)
      if(fc[i].second > 1) {
        fcc.push_back(make_pair(fc[i].first, fc[i].second>>1));
        for(int i = 0; i < (fc[i].second>>1); i++)
          cc *= fc[i].first;
        fc[i] = make_pair(fc[i].first, fc[i].second & 1);
      }
    // (3k-1, k*cc, (8k-3)/cc^2) -- (3k-1, 1, k^2*(8k-3))
  }
  return 0;
}


