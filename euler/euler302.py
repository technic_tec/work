from math import exp, log
from copy import copy
from fractions import gcd

def gen_prime(n):
  fac = [0] * n
  fac[1] = 1
  prm = []
  for i in range(2, n):
    if fac[i] > 0:
      continue
    fac[i] = i
    prm.append(i)
    for j in range(i*i, n, i):
      if fac[j] == 0:
        fac[j] = i
  return (prm, fac)

N = 10**18
#NN = long(exp(20*log(10.0)/3.0))
#assert(NN**3 <= N**2 and (NN+1)**3 > N**2)
NN = 10**6
prm, fac = gen_prime(NN+1)

def min_est(_rem):
  rem = copy(_rem)
  v = 1
  while len(rem) > 0:
    p = max(rem)
    v *= p
    rem.discard(p)
    for px, kx in factor(p-1):
      rem.discard(px)
  return v

def factor(n):
  fs = []
  while n > 1:
    f, c = fac[n], 0
    while fac[n] == f:
      n /= f
      c += 1
    fs.append((f, c))
  return fs

def achilles(f):
  d = 0
  for p, k in f.iteritems():
    if k <= 1:
      return False
    d = gcd(k, d)
  return (d == 1)

def search(v = 1, fv = dict(), fp = dict(), idx = len(prm)-1, rem = set(), rem2 = set()):
  if v * min_est(rem) >= N:
    return
  if v > 1 and len(rem) == 0:
    if achilles(fv) and achilles(fp):
      yield v
  m = max({0}|rem)
  for i in range(idx, -1, -1):
    p = prm[i]
    if v == 1 and (p < 1000 or (i&(i-1)==0 or (i & 65535) == 0)):
      print 'processing %d...'%p
    if p in rem2:
      k = 2
    else:
      k = 3
    vx = v*p**k
    if vx >= N:
      continue
    f = factor(p-1)
    fa, fr = [], []
    for px, rx in f + [(p, k-1)]:
      assert rx > 0
      if px in rem:
        rem.remove(px)
        fr.append(px)
      elif px not in rem2:
        rem2.add(px)
        if rx == 1:
          rem.add(px)
        fa.append(px)
      fp[px] = fp.get(px, 0) + rx
    fv[p] = k
    while vx < N: 
      for x in search(vx, fv, fp, i-1, rem, rem2):
        yield x
      vx *= p
      fv[p] += 1
      fp[p] += 1
    for px in fr:
      rem.add(px)
    for px in fa:
      rem2.remove(px)
      rem.discard(px)
    k = fv.pop(p)
    for px, rx in f + [(p, k-1)]:
      fp[px] -= rx
      if fp[px] == 0:
        fp.pop(px)
    if v == 1:
      assert (len(rem)==0 and len(rem2)==0 and len(fv) == 0 and len(fp) == 0)

def rsearch():
  n = 2
  while n**3 <= N**2:
    f = dict([(p, 3*k) for p, k in factor(n)])
    v = 1
    while len(f) > 0 and v > 0:
      p = max(f)
      k = f.pop(p)
      if (k & 1) == 0:
        v = 0
        break
      for p1, k1 in factor(p-1):
        if p1 not in f or f[p1] < k1:
          v = 0
          break
        f[p1] -= k1
        if f[p1] == 0:
          f.pop(p1)
      if v > 0:
        v *= p**((k+1)/2)
    if v > 1 and v < N:
      yield v
    n += 1
#v = []
#for x in search():
#  v.append(x)
#  print len(v), x
v = list(search())
v.sort()
for x in v:
  print x
print len(v), sum(v)

