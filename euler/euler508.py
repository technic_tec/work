def f(x, y):
  if (x, y)==(0, 0):
    return 0
  elif (x+y)&1:
    return f((y-x+1)/2, -(x-1+y)/2) + 1
  else:
    return f((y-x)/2, -(x+y)/2)

vB = dict({(0, 0, 0, 0):(0, 1)})
def B(xl, xh, yl, yh):
  if xl > xh or yl > yh:
      return (0, 0)
  elif (xl, xh, yl, yh) not in vB:
    vB[(xl, xh, yl, yh)] = None
    # (x+yi)*(i-1)^2 = 2y-2xi
    s, c = B((1-yh)/2, -yl/2, (xl+1)/2, xh/2)
    rs, rc = s, c
    # (x+yi)*(i-1)^2+1 = 2y+1-2xi
    s, c = B((1-yh)/2, -yl/2, xl/2, (xh-1)/2)
    rs += s+c
    rc += c
    # (x+yi)*(i-1)^2+(i-1) = 2y-1+(1-2x)i
    s, c = B((2-yh)/2, (1-yl)/2, (xl+2)/2, (xh+1)/2)
    rs += s+c
    rc += c
    # (x+yi)*(i-1)^2+i = 2y+(1-2x)i
    s, c = B((2-yh)/2, (1-yl)/2, (xl+1)/2, xh/2)
    rs += s+2*c
    rc += c
    if rc != (xh-xl+1)*(yh-yl+1):
      raise ValueError("%s: %s" % (str((xl, xh, yl, yh)), str((rs, rc))))
    vB[(xl, xh, yl, yh)] = (rs, rc)
  elif vB[(xl, xh, yl, yh)] is None:
    raise Exception("Loop: %s" % str((xl, xh, yl, yh)))
  return vB[(xl, xh, yl, yh)]

def euler508(n):
  return B(-n, n, -n, n)[0]

for i in range(16):
  xl, xh, yl, yh = -((i>>3)&1), ((i>>2)&1), -((i>>1)&1), (i&1)
  s, c = 0, 0
  for x in range(xl, xh+1):
    for y in range(yl, yh+1):
      c += 1 
      s += f(x, y)
  if c != (xh-xl+1)*(yh-yl+1):
    raise ValueError("%s: %s" % (str((xl, xh, yl, yh)), str((s, c))))
  vB[(xl, xh, yl, yh)] = (s, c)

for x, y in [(11, 24), (24, -11), (8, 0), (-5, 0), (0, 0)]:
  print (x, y), f(x, y)
for n in [500, 10**15]:
  r=euler508(n)
  print n, r
print r%(10**9+7)
