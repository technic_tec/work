/*
 * =====================================================================================
 *
 *       Filename:  euler392.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年06月26日 14时31分18秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

const double pi = acos(-1.0);
const double eps = 1e-10;

double bestA(double A, double B)
{
  assert(A > B);
  // f(x) = (cos(x)-cos(A))*sin(A) + (cos(B)-cos(x))*sin(x);
  // f'(x) = -sin(x)*sin(A) + (cos(B)-cos(x))*cos(x) + sin(x)*sin(x);
  double l = B, h = A;
  while(h-l > eps) {
    double m = (h+l)/2.0;
    double r = sin(m)*(sin(m)-sin(A)) + cos(m)*(cos(B)-cos(m));
    if (r < -eps)
      l = m;
    else if(r > eps)
      h = m;
    else
      break;
  }
  return (h+l)/2.0;
}

double E(int N)
{
  assert(!(N&1));
  N >>= 1;
  double *sp = new double[N+2];
  sp[0] = pi/2.0;
  sp[N+1] = 0.0;
  for(int i = 1; i <= N; i++)
    sp[i] = (sp[N+1]*i + sp[0]*(N+1-i))/(N+1);
  bool done = false;
  while(!done) {
    done = true;
    for(int i = 1; i <= N; i++) {
      double x = bestA(sp[i-1], sp[i+1]);
      if(fabs(x-sp[i]) > eps) {
        sp[i] = x;
        done = false;
      }
    }
    if(done) break;
    for(int i = N; i >= 1; i--) {
      double x = bestA(sp[i-1], sp[i+1]);
      if(fabs(x-sp[i]) > eps) {
        sp[i] = x;
        done = false;
      }
    }
  }
  double res = 0;
  for(int i = 0; i <= N; i++)
    res += (cos(sp[i+1])-cos(sp[i]))*sin(sp[i]);
  res *= 4;
  printf("%d: %.10lf\n", 2*N, res);
  delete[] sp;
  return res;
}
int main()
{
  E(10);
  E(400);
  return 0;
}


