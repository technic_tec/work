import sys

def vlr(x, y):
  return not (((x&1)==1 and (y&1)==0) or (x^y)==2)
 
def vud(x, y):
  return not (((x&2)==2 and (y&2)==0) or (x^y)==1)
 
def vtldr(x, y):
  return not ((x==3 and y!=3) or (y==0 and x!=0))
 
def vtrdl(x, y):
  return not ((x==2 and y!=2) or (y==1 and x!=1))

def gen(n, m, b = None, x = 0, y = 0):
  if x >= n:
    yield b
    return
  if y >= m:
    for p in gen(n, m, b, x+1, 0):
      yield p
    return
  if b is None:
    assert x==0 and y == 0
    b = [[-1]*m for _ in range(n)]
  assert b[x][y] == -1
  for v in range(4):
    if y > 0 and not vlr(b[x][y-1], v):
      continue
    if x > 0:
      if not vud(b[x-1][y], v) or (y > 0 and not vtldr(b[x-1][y-1], v)) or (y < m-1 and not vtrdl(b[x-1][y+1], v)):
        continue
    b[x][y] = v
    for p in gen(n, m, b, x, y+1):
      yield p
  b[x][y] = -1
 
def part03(bd, rx, cx, ry, cy):
  n, m = len(bd), len(bd[0])
  assert rx >= ry and cx <= cy
  for i in range(n):
    for j in range(m):
      if i < rx and j < cy:
        bd[i][j] = 0
      elif i < ry:
        bd[i][j] = 1
      elif j < cx:
        bd[i][j] = 2
      else:
        bd[i][j] = 3
  c0 = [cy] * (rx-ry)
  #print c0
  yield bd
  if ry == n or cx == m:
    return

  while bd[ry][cx] != 3:
    i = rx-ry-1
    while c0[i] <= cx:
      i-=1
    c0[i] -= 1
    bd[ry+i][c0[i]] = 3
    for j in range(ry+i+1, rx):
      bd[j][cx:cy] = [0]*(c0[i]-cx)+[3]*(cy-c0[i])
      c0[j-ry] = c0[i]
    #print c0
    yield bd

def part12(b03, b12):
  n, m = len(b03), len(b03[0])
  for i in range(n):
    for j in range(m):
      b12[i][j] = (b03[i][m-1-j]^1)
  return b12

def gen2(n, m):
  b03 = [[-1]*m for _ in range(n)]
  b12 = [[-1]*m for _ in range(n)]
  for rx in range(n+1):
    for cx in range(m+1):
      for b in part03(b03, rx, cx, rx, cx):
        #print (rx, cx)
        yield b
      if (rx == n and cx != 0) or (rx < n and cx == 0) or rx == 0 or cx == m:
        continue
      for ry in range(1, min(rx+1, n)):
        for cy in range(max(1, cx), m):
          if (rx, cx) != (ry, cy):
            for b in part03(b03, rx, cx, ry, cy):
              #print (rx, cx, ry, cy)
              yield b
              part12(b, b12)
              yield b12
      for b in part03(b03, rx, cx, 0, m):
        if (rx, cx) == (n, 0) and b[0][0] == 3 or b[-1][-1] == 0:
          continue
        #print (rx, cx, 0, m)
        yield b
        part12(b, b12)
        yield b12

def pdac(n, p):
  l = []
  while n > 0:
    l.append(n%p)
    n /= p
  return l

P = 10**8+7
cc, inv = [1, 2], [0, 1]
def central_binomial(n):
  nl, ml = pdac(n, P), pdac(2*n, P)
  while len(nl) < len(ml):
    nl.append(0)
  if any(x > y for x, y in zip(nl, ml)):
    return 0
  r = 1
  for x, y in zip(nl, ml):
    assert 2*x == y
    while len(cc) <= x:
      n1 = len(cc)
      inv.append(-(P/n1)*inv[P%n1] % P)
      if inv[-1] < 0:
        inv[-1] += P
      cc.append(cc[-1]*2*(2*n1-1)*inv[n1]%P)
    r *= cc[x]
  return r % P

def cnt(n):
  #sum(i=0, n-1, sum(j=0, n-1, binomial(i+j, i))) = binomial(2*n, n)-1
  #sum(j=0, n-1, (n-1-i)*(n-1-j)*binomial(i+j, i))) = binomial(2*n, n)-1-n^2
  #c(n)=(n+1)**2+2*binomial(2*n, n)-4
  # + 2*sum(i=0, n-1, sum(j=0, n-1, (n-1-i)*(n-1-j)*binomial(i+j, i)))-2*(n-1)**2
  # + 4*sum(i=0, n-1, sum(j=0, n-1, binomial(i+j, i)))-4*(2*n-1)
  # = 8*binomial(2*n, n)-3*n^2-2*n-7
  # = 8*binomial(2*n, n)-12-4*2*(n-1)-3*(n-1)^2
  r = (8*central_binomial(n)-3*n**2-2*n-7)%P
  if(r < 0):
    r += P
  return r
  
print [cnt(x) for x in range(1, 11)]
a, b, s = 1, 1, 0
for n in range(2, 91):
  s += cnt(b)
  a, b = b, a+b
print s%P
