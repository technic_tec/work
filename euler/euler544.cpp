#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <map>
#include <vector>
using namespace std;

#define P 1000000007

class Poly {
  public:
    Poly(int c = 0) : m_v(1,c) {
      init();
    }
    Poly(const vector<int> &coef) : m_v(coef) {
      init();
    }
    virtual ~Poly() {}
    int eval(int x) const
    {
      int s = 0;
      x %= P;
      for(int i = m_v.size()-1; i >= 0; i--)
        s = (int)(((long long)s * (long long)x + (long long)m_v[i]) % P);
      return s;
    }
    Poly operator+(const Poly &o) const {
      Poly r;
      int i;
      r.m_v.clear();
      for(i = 0; i < m_v.size() && i < o.m_v.size(); i++) {
        int sx = (m_v[i] + o.m_v[i]);
        if(sx >= P) sx -= P;
        r.m_v.push_back(sx);
      }
      for(; i < m_v.size(); i++)
        r.m_v.push_back(m_v[i]);
      for(; i < o.m_v.size(); i++)
        r.m_v.push_back(o.m_v[i]);
      return r;
    }
    //Poly operator-() const;
    //Poly operator-(const Poly &o) const;
    Poly operator*(const Poly &o) const {
      Poly r;
      int m = m_v.size(), n = o.m_v.size();
      r.m_v.assign(m+n-1, 0);
      for(int i = 0; i < m; i++)
        for(int j = 0; j < n; j++)
          r.m_v[i+j] = (int)(((long long)r.m_v[i+j] + (long long)m_v[i] * (long long)o.m_v[j]) % P);
      return r;
    }
    // sk = sum(i^k, i, 1, n)
    Poly sum() const {
      // n^(k+1) - (n-1)^(k+1) = sum(C(k+1, i)*(-1)^(k-i)*n^i, i, 0, k)
      // n^(k+1) = sum(C(k+1, i)*(-1)^(k-i)*si, i, 0, k)
      // sk = (n^(k+1) + sum(C(k+1, i)*(-1)^(k+1-i)*si, i, 0, k-1))/(k+1)
      int d = m_v.size()-1;
      Poly res;
      for(int i = 0; i <= d; i++) {
        if(m_sk.size() <= i) {
          Poly sk;
          vector<int> tot;
          tot.push_back(1);
          for(int j = 1; j < m_c.size(); j++)
            tot.push_back(m_c[j-1] + m_c[j] >= P ? (m_c[j-1]+m_c[j]-P) : (m_c[j-1]+m_c[j]));
          tot.push_back(1);
          m_c.swap(tot);
          for(int j = 0; j < i; j++)
            sk = m_sk[j] * m_c[j] + sk*(-1);
          while(sk.m_v.size() <= i+1)
            sk.m_v.push_back(0);
          sk.m_v[i+1]++;
          while(m_inv.size() <= i+1) {
            int j = m_inv.size();
            int vj = -(int)((long long)(P/j)*(long long)m_inv[P%j]%P);
            if(vj < 0) vj += P;
            m_inv.push_back(vj);
          }
          //printf("s%d=", i);sk.print();
          sk = sk * m_inv[i+1];
          m_sk.push_back(sk);
        }
        res = res + m_sk[i] * m_v[i];
      }
      return res;
    }
    void print() const {
      printf("[");
      for(int i = 0; i < m_v.size(); i++)
        printf(" %d", m_v[i]);
      printf(" ]\n");
    }
  private:
    inline void init() {
      for(int i = 0; i < m_v.size(); i++) {
        m_v[i] %= P;
        if(m_v[i] < 0) m_v[i] += P;
      }
    }
    vector<int> m_v;
    static vector<Poly> m_sk;
    static vector<int> m_c;
    static vector<int> m_inv;
};
vector<Poly> Poly::m_sk;
vector<int> Poly::m_c(1, 1);
vector<int> Poly::m_inv(2, 1);

vector<int> normalize(const vector<int> &v) {
  int n = v.size();
  vector<int> o;
  vector<int> mp(max(n, *max_element(v.begin(), v.end()))+1, -1);
  mp[0] = 0;
  int vm = 0;
  for(int i = 0; i < n; i++) {
    if(mp[v[i]] < 0)
      mp[v[i]] = ++vm;
    o.push_back(mp[v[i]]);
  }
  return o;
}

Poly chromatic_poly(int n, int m) {
  if(n > m) swap(n, m);
  map<vector<int>, Poly> c;
  c[vector<int>(n, 0)] = 1;
  for(int i = 0; i < m; i++) {
    for(int j = 0; j < n; j++) {
      map<vector<int>, Poly> cn;
      for(map<vector<int>, Poly>::iterator it = c.begin(); it != c.end(); ++it) {
        vector<int> cx = it->first;
        Poly px = it->second;
        int above = cx[j];
        int left = (j>0 ? cx[j-1] : 0);
        int vm = *max_element(cx.begin(), cx.end());
        assert(vm >= 0 && vm <= n);
        for(int k = 1; k <= vm; k++) {
          if(k == above || k == left)
            continue;
          cx[j] = k;
          vector<int> cy = normalize(cx);
          map<vector<int>, Poly>::iterator ip = cn.find(cy);
          if(ip == cn.end()) {
            cn[cy] = px;
          } else {
            ip->second = ip->second + px;
          }
        }
        const int c1[] = {vm?(P-vm):vm, 1};
        const vector<int> vc1(c1, c1+2);
        const Poly pc1(vc1);
        cx[j] = vm+1;
        vector<int> cy = normalize(cx);
        map<vector<int>, Poly>::iterator ip = cn.find(cy);
        if(ip == cn.end()) {
          cn[cy] = pc1*px;
        } else {
          ip->second = ip->second + pc1*px;
        }
      }
      //printf("(%d, %d) %d\n", i, j, cn.size());
      c.swap(cn);
    }
  }
  Poly ps;
  for(map<vector<int>, Poly>::iterator it = c.begin(); it != c.end(); ++it) {
    ps = ps + it->second;
  }
  //printf("%s(%d, %d) =", __FUNCTION__, n, m);
  //ps.print();
  return ps;
}

int f(int n, int m, int q) {
  Poly cply = chromatic_poly(n, m);
  int ret = cply.eval(q);
  printf("F(%d, %d, %d) = %d\n", n, m, q, ret);
  return ret;
}

int euler544(int n, int m, int q) {
  Poly cply = chromatic_poly(n, m);
  Poly sply = cply.sum();
  int ret = sply.eval(q);
  printf("euler544(%d, %d, %d) = %d\n", n, m, q, ret);
  return ret;
}

int main()
{
  f(2, 2, 3);          // expected: 18
  f(2, 2, 20);         // expected: 130340
  f(3, 4, 6);          // expected: 102923670
  euler544(4, 4, 15);  // expected: 325951319
  euler544(9, 10, 1112131415);
  return 0;
}
