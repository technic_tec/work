model(n) = if(n==1, [], vecsort(factorint(n)~[2,], , 4))
modval(v) = prod(x=1, length(v), prime(x)^v[x])
genmodel(n, v = []) = if(2*modval(v)>n, [v], x=0; while ((length(v)==0 || x < v[length(v)]) && modval(concat(v, x+1)) <= n, x+=1); concat([v], if(x==0, [], concat(vector(x, i, genmodel(n, concat(v, i)))))))
search(N) = {
  ms = genmodel(N);
  print(length(ms));
  vs = List(vector(length(ms), i, [modval(ms[i]), ms[i]]));
  listsort(vs);
  cl = List();
  vl = List();
  for(i=1, length(vs), v=vs[i][1]; listput(vl, v); if(v==1, listput(cl, 1), listput(cl, 0)); listput(cl, sumdiv(v, p, pi=setsearch(vl, modval(model(p)));if(pi==0, print(vl, cl, n, ", ", p, ", ", modval(model(p))));cl[pi]), length(cl)));
  sl = 0;
  for(i=1, length(vl), if(cl[i]<=10^18 && modval(model(cl[i])) == vl[i], print(vl[i], " ", cl[i]);sl+=cl[i]));
  print(sl)
}
m=input()
print(m)
print([m[1], m[2], modval(m[2])])
