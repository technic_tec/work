def gen23(n):
    k, c3 = 1, 0
    while k <= n:
        m, c2 = k, 0
        while m*2 <= n:
            m *= 2
            c2 += 1
        yield (c2, c3)
        k *= 3
        c3 += 1

def euler462(n):
    pts = list(gen23(n))
    x = [u for u, v in pts]
    y = []
    j = 0
    for i in range(len(pts)-1, -1, -1):
        while j <= x[i]:
            y.append(i)
            j += 1
    hooks = []
    for i, u in enumerate(x):
        for j in range(u+1):
            hooks.append((x[i]-j)+(y[j]-i)+1)
    hooks.sort()
    c = 1
    for i, v in enumerate(hooks):
        c *= i+1
    for i, v in enumerate(hooks):
        c /= v
    print '%d(%d)' % (c, len(str(c)))
euler462(6)
euler462(8)
euler462(20)
euler462(1000)
euler462(10**18)
