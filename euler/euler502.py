import sys
import numpy as np

P = 1000000007

def power_mod(m, k):
  k0 = k
  r = np.asmatrix(np.identity(m.shape[0], 'uint64'))
  while k > 0:
    if k & 1:
      r *= m
      r %= P
    m *= m
    m %= P
    k >>= 1
  return r

# 1. from column (w-1) to column w: (h, eo) <- (h+x, eo) | (h-2x, eo) | (h-2x-1, !eo)
def f1(w, h):
  assert(h <= 100)
  m = np.asmatrix([[int((0==((i+j)&1) and (i/2<=j/2 or 0==((i/2-j/2)&1))) or (((i+j)&1) and i/2>j/2 and ((i/2-j/2)&1))) for j in range(2*h)] for i in range(2*h)], dtype='uint64')
  r1 = np.asmatrix([[1, 0]*h], dtype='uint64') * power_mod(m, w-1) * np.asmatrix([[int((i&3)==2 or (i&3)==1)] for i in range(2*h)], dtype='uint64')
  m = np.asmatrix([[int((0==((i+j)&1) and (i/2<=j/2 or 0==((i/2-j/2)&1))) or (((i+j)&1) and i/2>j/2 and ((i/2-j/2)&1))) for j in range(2*h-2)] for i in range(2*h-2)], dtype='uint64')
  r2 = np.asmatrix([[1, 0]*(h-1)], dtype='uint64') * power_mod(m, w-1) * np.asmatrix([[int((i&3)==2 or (i&3)==1)] for i in range(2*h-2)], dtype='uint64')
  res = (r1 - r2) % P
  assert res.shape == (1, 1)
  res = res[0, 0]
  if max(w, h) > 1000:
    print '%s(%d, %d) = %d' % (sys._getframe().f_code.co_name, w, h, res)
  return res

# 2. select w columns from h possible column types: sum(max(h[i]-h[i-1], 0), i=1..w) is even (h[0]=0, 1<=h[i]<=h)
def f2(w, h):
  assert(w <= 100)
  res = 0
  if max(w, h) > 1000:
    print '%s(%d, %d) = %d' % (sys._getframe().f_code.co_name, w, h, res)
  return res

# 3. c[i][j][k] = c[i-1][h][k]+c[i-1][h-1][k]+...+c[i-1][j][k] + c[i-1][j-1][1-k]+c[i-1][j-2][k]+c[i-1][j-3][1-k]+...
#    i=1..w, j=1..h, k=0..1
def f3(w, h):
  assert(w*h <= 10**8)
  tl = [[1, 0]] + [[0, 0] for _ in range(h)]
  sl = [1, 0]
  tl1 = [[1, 0]] + [[0, 0] for _ in range(h-1)]
  sl1 = [1, 0]
  for i in range(1, w+1):
    tc = [[0, 0]]
    sc = [[tl[0][0], tl[0][1]], [0, 0]]
    s = [tc[0][0], tc[0][1]]
    tc1 = [[0, 0]]
    sc1 = [[tl1[0][0], tl1[0][1]], [0, 0]]
    s1 = [tc1[0][0], tc1[0][1]]
    for j in range(1, h):
      tc.append([sl[0] - sc[1^(j&1)][0] + sc[1^(j&1)][1], sl[1] - sc[1^(j&1)][1] + sc[1^(j&1)][0]])
      sc[j&1][0] += tl[j][0]
      sc[j&1][1] += tl[j][1]
      s[0] += tc[j][0]
      s[1] += tc[j][1]
      tc1.append([sl1[0] - sc1[1^(j&1)][0] + sc1[1^(j&1)][1], sl1[1] - sc1[1^(j&1)][1] + sc1[1^(j&1)][0]])
      sc1[j&1][0] += tl1[j][0]
      sc1[j&1][1] += tl1[j][1]
      s1[0] += tc1[j][0]
      s1[1] += tc1[j][1]
    tc.append([sl[0] - sc[1^(h&1)][0] + sc[1^(h&1)][1], sl[1] - sc[1^(h&1)][1] + sc[1^(h&1)][0]])
    sc[h&1][0] += tl[h][0]
    sc[h&1][1] += tl[h][1]
    tl, sl, tl1, sl1 = tc, s, tc1, s1
  res = sl[0] - sl1[0]
  if max(w, h) > 1000:
    print '%s(%d, %d) = %d' % (sys._getframe().f_code.co_name, w, h, res)
  return res % P

def F(w, h):
  r = []
  if h <= 100:
    r.append(f1(w, h))
  if w <= 100:
    r.append(f2(w, h))
  if w*h <= 10**8:
    r.append(f3(w, h))
  return r

print F(4, 2)
print F(13, 10)
print F(10, 13)
print F(100, 100)
print (F(10**12, 100)[0]+F(10000, 10000)[0]+F(100, 10**12)[0])%P

# 0011 0101 1001 0110 1010 1100 1111

