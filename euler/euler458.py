P = 1000000000
def pattern(s):
    if s == '':
        return s
    o = 'a'
    for i in range(1, len(s)):
        if s[i] in s[:i]:
            o += o[s[:i].index(s[i])]
        else:
            o += chr(ord(max(o))+1)
    return o

def trans(s, c):
    if c in s:
        return pattern(s[s.index(c)+1:]+c)
    return pattern(s+c)

def mul(a, b):
    la = map(len, a)
    lb = map(len, b)
    assert(max(la) == min(la) and max(lb) == min(lb))
    assert(la[0] == len(lb))
    return [[sum([a[i][k]*b[k][j] for k in range(la[0])])%P for j in range(lb[0])] for i in range(len(la))]

def pow(a, k):
    la = map(len, a)
    assert(max(la) == min(la) and len(la) == la[0])
    r = [[int(i==j) for j in range(la[0])] for i in range(la[0])]
    while k > 0:
        if k & 1:
            r = mul(r, a)
        a = mul(a, a)
        k >>= 1
    return r

chs = 'abcdefg'
sts = [chs[:i] for i in range(7)]
tr = [[0]*7 for _ in range(7)]
for i, x in enumerate(sts):
    for c in chs:
        y = trans(x, c)
        if y != chs:
            tr[sts.index(y)][i] += 1

def euler(k):
    return sum([x[0] for x in pow(tr, k)])%P

print euler(7)
print euler(10**12)
