/*
 * =====================================================================================
 *
 *       Filename:  euler552.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016年03月21日 09时50分32秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence Hang (tec), Terence.Hang@verisilicon.com
 *   Organization:  Verisilicon,Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;

#define N 300000

int gcdex(int x, int y, int &a, int &b) {
  if(y) {
    int d = gcdex(y, x%y, b, a);
    b -= a*(x/y);
    return d;
  } else {
    a = 1; b = 0;
    return x;
  }
}
int inv(int x, int y) {
  int a, b;
  int d = gcdex(x, y, a, b);
  assert(d == 1);
  assert(abs(a) < y);
  return (a < 0 ? a+y : a);
}
int main()
{
  vector<bool> isp(N+1, true);
  vector<int> prm;
  isp[0] = isp[1] = false;
  for(int i = 2; i <= N; i++)
    if(isp[i]) {
      prm.push_back(i);
      if(i < 32768)
        for(int j = i*i; j <= N; j+=i)
          isp[j] = false;
    }
  int np = prm.size();
  vector<int> d(np+1, 0);
  d[0] = 0;
  long long s = 0;
  for(int i = 1; i <= np; i++) {
    int p = prm[i-1];
    bool valid = false;
    // n = (i-r)*m^(-1)*m+r, m = p1p2...pi-1
    int r = 0, m = 1;
    for(int j = 1; j < i; j++) {
      r = (int)(((long long)d[j]*(long long)m+(long long)r)%p);
      m = (int)((long long)m * (long long)prm[j-1] % p);
      if(r == 0)
        valid = true;
    }
    d[i] = (int)((long long)(i-r)*(long long)inv(m, p)%p);
    if(d[i] < 0) d[i] += p;
    //printf("%d: %d %d\n", prm[i-1], d[i], valid);
    if(valid)
      s += (long long)p;
  }
  printf("%lld\n", s);

  return 0;
}
