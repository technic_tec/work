# H(n)=1+1/2+...+1/n
# H*(n) = sum({1/k | 1<=k<=n, (k,p)=1})
# H(pn) = H*(pn) + H(n)/p = H(n)/p (mod p)
# H(pn+k) = H(pn) + H(k) - sum({1/j-1/(pn+j)|1<=j<=k}) = H(pn)+H(k) (mod p) (Wolstenholme's Theorem)
# H(pn+k) >= 0 (mod p) <=> H(n) = 0 (mod p)
# H(pn+k) = 0 (mod p) <=> H(n) = 0 (mod p) and H(k)+H(n)/p = 0 (mod p)

# [1] BOYD,D.W.: A p-adic study of the partial sums of the harmonic series, Experiment. Math. 3 (1994), 287-302.
# (http://projecteuclid.org/download/pdf_1/euclid.em/1048515811)
# H*(pn) = H(pn) - (1/p)H(n) = sum(c(k)*p^2k*n^2k, k, 1, inf)  (Bernoulli's Formula with r=(p-1)p^s, m=pn)
# partial sum k=1..N gives precision s=2N+2-[log_p (N+1)]

from pprint import pprint
def ilog(n, p):
  c = 0
  while n > 1:
    n /= p
    c += 1
  return c

def gcdex(a, b):
  if b == 0:
    d, x, y = a, 1, 0
  else:
    d, y, x = gcdex(b, a%b)
    y -= a/b*x
  return (d, x, y)

class Harmonic(object):
  def __init__(self, p):
    self.p = p
    self.N = 10 #min(p+1, 10)
    while not self.init():
      self.N += 10 #min(p+1, 10)

  #def inv(self, n):
  #  p, ps = self.p, self.ps
  #  while len(self.iv) <= n:
  #    m = len(self.iv)
  #    if m % p:
  #      ri = -(ps/m)*self.iv[ps%m] % ps
  #      if ri < 0:
  #        ri += ps
  #      if ri*m % ps != 1:
  #        print '%d, %d, %d (%d, %d, %d)' % (ri, m, ps, ps/m, ps%m, self.iv[ps%m])
  #    else:
  #      ri = 0
  #    self.iv.append(ri)
  #  return self.iv[n]

  def inv(self, n):
    assert(n % self.p)
    d, x, y = gcdex(n, self.ps)
    if d != 1:
      #print n, self.ps, d
      raise ValueError
    if x < 0:
      x += self.ps
    assert (x >= 0 and x < self.ps and x*n%self.ps == 1)
    return x

  def linsolve(self, c, b):
    INF = 999999
    n = len(b)
    ps = self.p ** (2*n)
    #print [x % ps for x in b]
    assert len(c) == n and len(c[0]) == n
    ix = range(n)
    for i in range(n):
      j, rj = i, INF
      for k in range(i, n):
        if c[ix[k]][i]:
          cx, vx = 0, c[ix[k]][i]
          while vx % self.p == 0:
            vx /= self.p
            cx += 1
          if cx < rj:
            j, rj = k, cx
      #print(c,b,i, j, rj, n, self.s)
      assert rj < INF
      ix[i], ix[j] = ix[j], ix[i]
      vx = c[ix[i]][i]
      while(vx % self.p == 0):
        vx /= self.p
      r = self.inv(vx)
      for j in range(i, n):
        c[ix[i]][j] *= r
        c[ix[i]][j] %= self.ps
      b[ix[i]] *= r
      b[ix[i]] %= self.ps
      for j in range(i+1, n):
        b[ix[j]] -= b[ix[i]] * c[ix[j]][i] / c[ix[i]][i]
        b[ix[j]] %= self.ps
        if b[ix[j]] < 0:
          b[ix[j]] += self.ps
        for k in range(n-1, i-1, -1):
          c[ix[j]][k] -= c[ix[i]][k] * c[ix[j]][i] / c[ix[i]][i]
          c[ix[j]][k] %= self.ps
          if c[ix[j]][k] < 0:
            c[ix[j]][k] += self.ps
    #print(c,b)
    assert(self.s >= 2*n and self.p**(self.s-2*n) % c[ix[n-1]][n-1] == 0)
    self.s, self.ps = 2*n, self.p**(2*n)
    for i in range(n-1, -1, -1):
      assert(b[ix[i]] % c[ix[i]][i] == 0)
      b[ix[i]] /= c[ix[i]][i]
      c[ix[i]][i] = 1
      for j in range(i-1, -1, -1):
        b[ix[j]] -= b[ix[i]] * c[ix[j]][i]
        b[ix[j]] %= self.ps
        if b[j] < 0:
          b[j] += self.ps
        c[ix[j]][i] = 0
    #print(c,b)
    res = [b[ix[i]] for i in range(n)]
    #print [sum([res[j]*i**(2*j+2) for j in range(n)])%self.ps for i in range(1, n+1)]
    return res

  def init(self):
    N, p = self.N, self.p
    s = self.s = (2*N*p+p-2)/(p-1)
    ps = self.ps = p**s
    # H*(pn) = H(pn) - (1/p)H(n) = sum(c(k)*p^2k*n^2k, k, 1, inf)
    # interpolation c'(k) = c(k)*p^2k
    vs = 0
    coef, b = [], []
    for i in range(N):
      for k in range(i*p+1, i*p+p):
        vs += self.inv(k)
      b.append(vs)
      cx = [(i+1)**2%ps]
      for k in range(1, N):
        cx.append(cx[-1]*cx[0]%ps)
      coef.append(cx)
    c = self.c = self.linsolve(coef, b)
    # H(pn+k) = 0 (mod p) <=> H(n) = 0 (mod p) and H(k)+H(n)/p = 0 (mod p)
    s = self.s = 2*N+2-ilog(N+1, p)
    ps = self.ps = p**s
    Gx = []
    G = [(0, 0)]
    vs = 0
    for i in range(1, p):
      vs += self.inv(i)
      if vs % p == 0:
        Gx.append((i, vs))
        G.append((i, vs))
    for i in range(2, s+1):
      if len(Gx) == 0:
        break
      Gy = []
      s -= 1
      ps /= p
      while N > 0 and c[N-1] % ps == 0:
        N -= 1
      for n, hn in Gx:
        hn /= p
        hn += sum(c[k-1]*n**(2*k) for k in range(1, N+1))
        hn %= ps
        n *= p
        if hn%p == 0:
          Gy.append((n, hn))
        for k in range(n+1, n+p):
          hn += self.inv(k)
          hn %= ps
          if hn%p == 0:
            Gy.append((k, hn))
      Gx = Gy
      G.extend(Gx)
    self.G = G
    return len(Gx) == 0

  def J(self):
    for n, hn in self.G:
      if n > 0:
        yield n

  def I(self):
    for n, hn in self.G:
      for k in range(self.p):
        yield (n*p+k)

for p in [3, 5, 7, 11, 137]:
  H = Harmonic(p)
  #print p, list(H.J()), list(H.I())
  print p, len(list(H.J())), len(list(H.I())), max(H.I())
