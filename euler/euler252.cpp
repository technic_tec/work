/*
 * =====================================================================================
 *
 *       Filename:  euler252.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2016/03/31  1:01:56
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), technic.tec@gmail.com
 *   Organization:  Verisilicon, Inc.
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <vector>
using namespace std;
#define N 100000000
#define P 1000000007

pair<int, int> operator+(const pair<int, int> &x, const pair<int, int> &y) {
  return make_pair(x.first+y.first, x.second+y.second);
}
pair<int, int> operator-(const pair<int, int> &x, const pair<int, int> &y) {
  return make_pair(x.first-y.first, x.second-y.second);
}
inline int cross(const pair<int, int> &x, const pair<int, int> &y){
  return x.first*y.second - x.second*y.first;
}
inline int dot(const pair<int, int> &x, const pair<int, int> &y){
  return x.first*y.first + x.second*y.second;
}
bool cmpY (const pair<int, int> &x, const pair<int, int> &y) {
  return (x.second < y.second || (x.second == y.second && x.first < y.first));
}
bool polaCmp (const pair<int, int> &x, const pair<int, int> &y) {
  int cp = cross(x, y);
  int dp = dot(x, x)-dot(y, y);
  return cp > 0 || (cp == 0 && dp > 0);
}
int convex_hole(vector<pair<int, int> >::iterator _s, vector<pair<int, int> >::iterator _t) {
  if(_t - _s < 3) return 0;
  const pair<int, int> p0 = *_s;
  vector<pair<int, int> > ps;
  for(++_s; _s != _t; ++_s)
    ps.push_back(make_pair(_s->first-p0.first, _s->second-p0.second));
  sort(ps.begin(), ps.end(), polaCmp);

  int sm = 0;
  vector<vector<pair<pair<int, int>, int> > > stk(ps.size());
  for(int i = 1; i < ps.size(); i++) {
    pair<int, int> cand = ps[i];
    for(int j = i-1; j >= 0; j--) {
      if(cross(cand, ps[j]-ps[i]) > 0)
        continue;
      int k = j;
      while(k > 0 && cross(ps[k-1], ps[j]) == 0 && cross(cand, ps[k-1]-ps[i]) <= 0)
        --k;
      while(k < j) {
        int sx = cross(ps[k], ps[i]);
        while(!stk[i].empty() && sx >= stk[i].back().second)
          stk[i].pop_back();
        stk[i].push_back(make_pair(ps[i]-ps[k], sx));
        ++k;
      }
      cand = ps[j] - ps[i];
      while(stk[j].size() > 1 && cross((stk[j].end()-2)->first, ps[i]-ps[j]) >= 0)
        stk[j].pop_back();
      int sx = cross(ps[j], ps[i]);
      if(!stk[j].empty() && cross(stk[j].back().first, ps[i]-ps[j]) >= 0)
        sx += stk[j].back().second;
      while(!stk[i].empty() && sx >= stk[i].back().second)
        stk[i].pop_back();
      stk[i].push_back(make_pair(ps[i]-ps[j], sx));
      sm = max(sx, sm);
    }
  }
  //printf("(%d, %d) %.1lf\n", p0.first, p0.second, sm/2.0);
  return sm;
}
int euler(int n) {
  int s = 290797;
  vector<pair<int, int> > ps;
  for(int i = 0; i < n; i++) {
    s = (int)((long long)s * (long long)s % 50515093);
    int x = s % 2000-1000;
    s = (int)((long long)s * (long long)s % 50515093);
    int y = s % 2000-1000;
    ps.push_back(make_pair(x, y));
  }
  sort(ps.begin(), ps.end(), cmpY);
  int sm = 0;
  for(vector<pair<int, int> >::iterator it = ps.begin(); it != ps.end(); ++it)
    sm = max(sm, convex_hole(it, ps.end()));
  printf("%d: %.1lf\n", n, sm/2.0);
  return sm;
}
int main()
{
  euler(20);
  euler(500);
  return 0;
}
