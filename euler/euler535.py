# S = 1, 1, 2, 1, 3, ...
# N = 1, 2, 3, 4, 5, ...
# Interleaving S and N, inserting sqrt(S[k]) elements of N before each element S[k] in S;
# each k in N is inserted before the first S[i]=k;
# merged sequence S'=S.

# [n^(1/2)]
def isqrt(n):
  if n == 0:
    return 0
  x = n
  y, x = x, (x+n/x)/2
  while x < y:
    y, x = x, (x+n/x)/2
  return y

# [1^(1/2)]+[2^(1/2)]+...+[n^(1/2)]
def sum_of_sqrt(n):
  k = isqrt(n)
  return k*(k-1)*(4*k+1)/6 + (n-k**2+1)*k

# I[k]: position change of S[k] after interleaving with N.
# Sv[k]: sum of S[1..k]
# Sq[k]: sum of [sqrt(S[1..k])]
# I[k]-k = Sq[k]
# Sv[I[k]]-Sv[k] = 1+2+...+Sq[k]
# Sq[I[k]]-Sq[k] = [sqrt(1)]+[sqrt(2)]+...+[Sqrt(Sq[k])]
def evolve(k, sv, sq, c = 1):
  while c > 0:
    k, sv, sq = (k + sq, sv + (1+sq)*sq/2, sq+sum_of_sqrt(sq))
    c -= 1
  return (k, sv, sq)

def euler535(n):
  res = 0
  d, k, sv, sq = 0, 1, 1, 1
  #print (d, k, sv, sq)
  while k < n:
    k, sv, sq = evolve(k, sv, sq)
    d += 1
    #print '->', (d, k, sv, sq)
  if k == n:
    res = sv
    print '%d: %d' % (n, res)
    return res

  l, vl, svl, sql = 0, 0, 0, 0
  h, vh, svh, sqh = 1, 1, 1, 1
  for i in range(1, d+1):
    vmin, vmax = sql+1, sqh
    #print ((l, vl, svl, sql), (h, vh, svh, sqh)), '->',
    l, svl, sql = evolve(l, svl, sql)
    h, svh, sqh = evolve(h, svh, sqh)
    #print ((l, vl, svl, sql), (h, vh, svh, sqh))
    # l,   l+1,            ..., h-1,          h
    # vl,  vmin,           ..., vmax,         vh
    # svl, svl+vmin,       ..., svh-vh,       svh
    # sql, sql+sqrt(vmin), ..., sqh-sqrt(vh), sqh
    assert(vmax == vmin + (h-1-l-1))
    assert(svh-vh == svl - (vmin-1)*vmin/2 + vmax*(vmax+1)/2)
    assert(sqh-isqrt(vh) == sql - sum_of_sqrt(vmin-1) + sum_of_sqrt(vmax))
    l0, svl0, sql0 = l, svl, sql
    while h - l > 1:
      m = (h+l)/2
      vm = vmin + (m-l0-1)
      svm = svl0 - (vmin-1)*vmin/2 + vm*(vm+1)/2
      sqm = sql0 - sum_of_sqrt(vmin-1) + sum_of_sqrt(vm)
      k, sv, sq = evolve(m, svm, sqm, d-i)
      if k == n:
        res = sv
        print '%d: %d' % (n, res)
        return res
      elif k > n:
        h, vh, svh, sqh = m, vm, svm, sqm
      else:
        l, vl, svl, sql = m, vm, svm, sqm
  raise Exception #should return earlier

  print '%d: %d' % (n, res)
  return res

map(euler535, [1, 20, 10**3, 10**9, 10**18])
