from math import ceil

def gen_prime(a, b):
  if a <= 2:
    isp = [True] * (a+b)
    isp[0] = isp[1] = False
    prm = []
    for i in range(2, a+b):
      if not isp[i]:
        continue
      prm.append(i)
      for j in range(i*i, a+b, i):
        isp[j] = False
    return prm
  else:
    prm0 = gen_prime(0, int(ceil((a+b)**0.5)+0.5)+1)
    isp = [True]*b
    for p in prm0:
      for j in range(max((a+p-1)/p, p)*p, a+b, p):
        isp[j-a] = False
    return [a+i for i in range(b) if isp[i]]

# d(p, n, 0) = iv[n] = 1/n (mod p)
# d(p, n, k) = sum(C(k-1+i,k-1)*iv[n-i], i, 0, n-1), k > 0
def d(p, n, k):
  iv = [0, 1]
  for i in range(2, n+1):
    iv.append((-(p/i)*iv[p%i]) % p)
  if k == 0:
    return iv[n]
  s, cx = 0, 1
  for i in range(n):
    s += cx*iv[n-i]
    cx *= (k+i) * iv[i+1]
    cx %= p
  return s % p

# dp(p, k) = d(p, p-1, k)
#          = sum(C(k-1+i,k-1)/(p-1-i), i, 0, p-2)
# dp(p, 0) = 1/(p-1) = p-1
# dp(p, 1) = sum(i/(p-1-i), i, 0, p-2) = sum(1/i, i, 1, p-1) = 0
# dp(p, k) [k>1] = sum(C(k-1+i,k-1)/(p-1-i), i, 0, p-2)
#                = sum(C(k-1+i,k-2)*(i+1)/((k-1)*(p-1-i)), i, 0, p-2)
#                = sum(C(k-1+i,k-2), i, 0, p-2)/(-(k-1))
#                = sum(C(k+i,k-1)-C(k-1+i,k-1), i, 0, p-2)/(-(k-1))
#                = (C(k+p-2, k-1)-C(k-1, k-1))/(-(k-1))
#                = 1/(k-1)
def dp(p, k):
  if k == 1:
    return 0
  else:
    # (k-1)^(-1) = (k-1)^(p-2)
    r, a, t = 1, k-1, p-2
    while t > 0:
      if t & 1:
        r *= a
        r %= p
      a *= a
      a %= p
      t >>= 1
    return r

def D(a, b, k, ans = None):
  res = 0
  for p in gen_prime(a, b):
    #print p
    #res += d(p, p-1, k)
    res += dp(p, k)
  if ans is None:
    print "D(%d, %d, %d) = %d" % (a, b, k, res)
  else:
    print "D(%d, %d, %d) = %d (ans %d)" % (a, b, k, res, ans)
  return res

D(101, 1, 10, 45)
D(10**3, 10**2, 10**2, 8334)
D(10**6, 10**3, 10**3, 38162302)
D(10**9, 10**5, 10**5)
