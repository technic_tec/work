#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <deque>
#include <vector>
#include <set>
using namespace std;

#define INF 99999999
#define N 100000000

class HK_Match {
    public:
        HK_Match(int _nu, int _nv)
        {
            nu = _nu; nv = _nv;
            adj.clear();
            for(int i = 0; i < nu; i++)
                adj.push_back(-1);
            NIL = nu;
            adj.push_back(-1);
            dst.clear();
            nxt.clear();
        }

        int add_edge(int _s, int _t)
        {
            int ne = dst.size();
            dst.push_back(_t);
            nxt.push_back(adj[_s]);
            adj[_s] = ne;
        }

        int hk_match()
        {
            Pair_G1.clear();
            for(int i = 0; i < nu; i++)
                Pair_G1.push_back(NIL);
            Pair_G2.clear();
            for(int i = 0; i < nv; i++)
                Pair_G2.push_back(NIL);
            int matching = 0;
            while (BFS())
                for(int v = 0; v < nu; v++)
                    if(Pair_G1[v] == NIL)
                        if (DFS(v))
                            matching++;
            return matching;
        }

    private:
        bool BFS()
        {
            deque<int> Q;
            Q.clear();
            Dist.clear();
            for(int v = 0; v < nu; v++) {
                if(Pair_G1[v] == NIL) {
                    Dist.push_back(0);
                    Q.push_back(v);
                } else {
                    Dist.push_back(INF);
                }
            }
            Dist.push_back(INF);
            while (!Q.empty()) {
                int v = Q.front();
                Q.pop_front();
                if(Dist[v] < Dist[NIL])
                    for(int e = adj[v]; e >= 0; e = nxt[e]) {
                        int u = dst[e];
                        if(Dist[ Pair_G2[u] ] == INF) {
                            //printf("BFS G1[%d]-->G2[%d]==>G1[%d]\n", v, u, Pair_G2[u]);
                            Dist[ Pair_G2[u] ] = Dist[v] + 1;
                            Q.push_back(Pair_G2[u]);
                        }
                    }
            }
            return (Dist[NIL] != INF);
        }

        bool DFS(int v)
        {
            if(v != NIL) {
                for(int e = adj[v]; e >= 0; e = nxt[e]) {
                    int u = dst[e];
                    if (Dist[ Pair_G2[u] ] == Dist[v] + 1) {
                        if(DFS(Pair_G2[u])) {
                            //printf("DFS G1[%d]==>G2[%d]-->G1[%d]\n", v, u, Pair_G2[u]);
                            Pair_G2[u] = v;
                            Pair_G1[v] = u;
                            return true;
                        }
                    }
                }
                Dist[v] = INF;
                return false;
            }
            return true;
        }

        int nu, nv, NIL;
        vector<int> adj, dst, nxt;
        vector<int> Pair_G1, Pair_G2, Dist;
};

int antichain(const vector<int> &rnk)
{
  int n = rnk.size();
  int nv = 1;
  vector<pair<int, int> > le;
  for(int i = 0; i < n; i++) {
    int k = le.size();
    for(int j = rnk[i]*k; j > 0; j--)
      le.push_back(make_pair(le[le.size()-k].first+nv, le[le.size()-k].second+nv));
    for(int j = nv; j < (rnk[i]+1)*nv; j++)
      le.push_back(make_pair(j-nv, j));
    nv *= rnk[i]+1;
  }
  HK_Match hk(nv, nv);
  for(int j = 0; j < le.size(); j++)
    hk.add_edge(le[j].first, le[j].second);
  return nv-hk.hk_match();
}

vector<int> prm;
vector<int> pc;

void gen_prime(int n)
{
  vector<bool> isp((n-1)/2, true);
  isp[0] = false;
  pc.push_back(0);
  prm.clear();
  prm.push_back(2);
  for(int p = 3; p <= n; p += 2) {
    if(!isp[p/2]) {
      pc.push_back(pc.back());
      continue;
    }
    prm.push_back(p);
    pc.push_back(prm.size());
    if(p < 32768)
      for(int j = p*p/2; 2*j+1 <= n; j += p)
        isp[j] = false;
  }
}

long long power(int n, int k)
{
  long long r = 1;
  long long nn = n;
  while(k) {
    if(k & 1)
      r *= nn;
    nn *= nn;
    k >>= 1;
  }
  //assert(r > 0);
  return r;
}
int next(int m, vector<int> &pi, const vector<int> &rnk)
{
  int nprm = prm.size();
  vector<long long> v(rnk.size(), 0);
  set<int> use(pi.begin(), pi.end()-1);
  for(int i = 0; i < rnk.size(); i++)
    v[i] = (i==0?1:v[i-1]) * power(prm[pi[i]], rnk[i]);
  for(int i = rnk.size()-2; i >= 0; i--) {
     use.erase(pi[i]);
     for(pi[i]++; pi[i] < nprm && use.find(pi[i])!=use.end(); pi[i]++);
     if(pi[i] >= nprm) continue;
     use.insert(pi[i]);
     v[i] = (i==0?1:v[i-1]) * power(prm[pi[i]], rnk[i]);
     bool valid = (v[i] <= m);
     for(int j = i+1, fst=1; j < rnk.size(); j++) {
       pi[j] = pi[j-1]+1; 
       if(fst && rnk[j] != rnk[j-1])
         pi[j] = fst = 0;
       while(pi[j] < nprm && use.find(pi[j])!=use.end())
         pi[j]++;
       if(pi[j] >= nprm)
         valid = false;
       use.insert(pi[j]);
       v[j] = v[j-1] * power(prm[pi[j]], rnk[j]);
       valid = (valid && (v[j] <= m));
     }
     if(valid)
       return (int)v[rnk.size()-2];
     for(int j = i; j < rnk.size(); j++)
       use.erase(pi[j]);
  }
  return m+1;
}

int nth_root(int x, int k)
{
  double r = exp(log((double)x)/k);
  int ri = (int)r;
  if(power(ri+1, k) <= x)
    return ri+1;
  else
    return ri;
}

inline int prime_count(int n) {
  if(n == 2)
    return pc[0]+1;
  else
    return pc[(n-1)/2];
}

int count_pattern(int m, const vector<int>& rnk)
{
  int res = 0;
  //assert(rnk.size() > 1);
  int n = rnk.size();
  vector<int> pi(n, 0);
  int val = 1;
  for(int i = 0; i < n-1; i++) {
    pi[i] = i;
    val *= power(prm[i], rnk[i]);
  }
  pi[n-1] = n-1;
  while(val <= m) {
    int up = prime_count(nth_root(m/val, rnk.back()));
    int c = up-pi.back();
    for(int i  = 0; i < rnk.size()-1; i++)
      if(pi[i] >= pi.back() && pi[i] < up) --c;
    //assert(c > 0);
    res += c;
    val = next(m, pi, rnk);
  }
  return res;
}

long long gen(int n, vector<int> &rnk, int v)
{
  static int cc = 0;
  int w = antichain(rnk);
  long long res = 0;
  int cnt = 0;
  if(w > 1) {
    cnt = count_pattern(n, rnk);
    res = (long long)(w-1)*(long long)cnt;
  }
  //printf("%d %d %d %d %lld\n", cc++, v, w, cnt, res);

  int k = rnk.size();
  int ci = 1;
  v *= prm[k];
  while(v <= n && (k == 0 || ci <= rnk.back())) {
    rnk.push_back(ci);
    res += gen(n, rnk, v);
    rnk.pop_back();
    ci++;
    v *= prm[k];
  }
  return res;
}

int euler386(int n)
{
  vector<int> rnk;
  long long res = gen(n, rnk, 1);
  res += n;
  printf("%d: %lld\n", n, res);
  return res;
}

int main()
{
  gen_prime(N/2);
  euler386(N);
  return 0;
}
