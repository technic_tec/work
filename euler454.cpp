/*
 * =====================================================================================
 *
 *       Filename:  euler454.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年03月26日 16时15分30秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;
vector<pair<pair<long long,long long>,pair<long long,long long> > > pp;
void gen(long long a1, long long b1, long long a2, long long b2, long long N)
{
    pp.clear();
    pp.push_back(make_pair(make_pair(a1,b1),make_pair(a2,b2)));

    long long cc = 0;
    while(pp.size()) {
        a1 = pp.back().first.first; b1 = pp.back().first.second;
        a2 = pp.back().second.first; b2 = pp.back().second.second;
        pp.pop_back();
        if((long long)(a1+a2)*(long long)(b1+b2) <= N) {
            //printf("%lld %d %d\n", ++cc, a1+a2, b1+b2);
            ++cc;
            if((cc & (cc-1))==0)
              printf("%lld: %lld/%lld\n", cc, a1+a2, b1+b2);
            pp.push_back(make_pair(make_pair(a1,b1),make_pair(a1+a2,b1+b2)));
            pp.push_back(make_pair(make_pair(a1+a2,b1+b2),make_pair(a2,b2)));
        }
    }
    printf("%lld\n",  cc);
}

// (x = b*(b-a)*d, y = a*b*d, n = a*(b-a)*d), (a, b) = 1, 1 < b/a < 2, a*b*d <= L
//sum(sum([L/ab], b/2<a<b, (a, b)=1), 1<=b<=L) 
long long euler454(long long N)
{
    long long c = 0;
    gen(1, 1, 1, 2, N);
    return c;
}
int main()
{
    //euler454(15);
    //euler454(1000);
    //euler454(1000000000000LL);
    return 0;
}

