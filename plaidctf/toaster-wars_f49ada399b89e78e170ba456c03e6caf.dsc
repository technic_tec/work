Part I: I found a locked box in Undersea Cavern once... and I think I saw a key in Calm Crystal Reef somewhere? Can you break the lock?

Part II: Based on the site, there's a dungeon called Shallow Sand Bar that's only open to players from their closed alpha. Surely that won't stop you from getting the flag at the end of that dungeon.

Part III: The boss at the end of Treacherous Trench, the Golden Spatula, is a jerk. Show him what you're made of and grab the flag!
