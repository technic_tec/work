import re
from random import shuffle
from itertools import izip
from nltk import NaiveBayesClassifier, classify
from nltk.tokenize.punkt import PunktSentenceTokenizer,  PunktParameters
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
from nltk.chunk import ne_chunk, batch_ne_chunk

def gen():
    punkt_param = PunktParameters()
    punkt_param.abbrev_types = set(['dr',  'vs',  'mr',  'mrs',  'prof',  'inc'])
    sentence_splitter = PunktSentenceTokenizer(punkt_param)
    f = open('corpus.txt')
    txt = list()
    for l in f.readlines():
        l = l.strip()
        if not re.match(r'^[A-Z ]*$', l):
            txt.append(l)
    sentences = sentence_splitter.tokenize(' '.join(txt))
    f.close()
    #print sentences
    for s in sentences:
        if re.search(r'\W(it\'?s|it is|it has)\W', s):
            print ne_chunk(pos_tag(word_tokenize(s.strip())))

def its_feature(sen):
    cls = None
    feat = dict()
    pos = 0
    for i, wt in enumerate(sen):
        #print sen[i]
        w, t = wt
        if w == 'its':
            assert(cls is None)
            cls = w
            pos = (i, i)
            break
        elif w == 'it' and i+1 < len(sen) and sen[i+1][0] in ('\'s', 'is', 'has'):
            assert(cls is None)
            cls = 'it\'s'
            pos = (i, i+1)
            break
    assert(cls is not None)
    #feat['prev_word'] = sen[pos[0]-1][1] if pos[0] > 0 else None
    feat['next_word'] = sen[pos[1]+1][1] if pos[1]+1 < len(sen) else None
    i = pos[1]+1
    while i+1 < len(sen) and sen[i][1] in ('RB', 'RBR', 'RBS', 'JJ', 'JJR', 'JJS', ','):
        i += 1
    feat['next_key'] = sen[i][1]
    return (feat, cls)

def check(classifier, testset, data):
    for (feat,  tag), sen in izip(testset, data):
        guess = classifier.classify(feat)
        if guess != tag:
            print 'correct=%-8s guess=%-8s name=%-30s, %s' % (tag,  guess,  feat, str(sen))

def train():
    f = open('nov13-punctuation-corrector-its.data')
    data = [eval(l) for l in f.readlines()]
    f.close()
    test_set = map(its_feature, data[600:])
    data = data[:600]
    shuffle(data)
    train_set = map(its_feature, data[:200])
    devtest_set = map(its_feature, data[200:600])
    classifier = NaiveBayesClassifier.train(train_set)
    print classify.accuracy(classifier,  devtest_set)
    classifier.show_most_informative_features(10)
    check(classifier, devtest_set, data[200:600])
    print classify.accuracy(classifier,  test_set)

#train()
n = int(raw_input())
for _ in range(n):
    s = raw_input().strip()
    f, t = its_feature(pos_tag(word_tokenize(s.replace('???', 'its'))))
    if f['next_key'] in ['NN', 'NNS', 'NNP']:
        s = s.replace('???', 'its')
    elif f['next_key'] in ['VB', 'VBN', 'VBG', 'CC', 'CD', 'IN']:
        s = s.replace('???', 'it\'s')
    else:
        s = s.replace('???', 'it\'s')
    print s
