from fractions import Fraction

vp = [[0], 
      [1, 0], 
      [0, 2, 0]]
fac = [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800]
for n in range(3, 11):
    vp.append([0]*(n+1))
    vp[n][1] = 2
    for s in range(2, n):
        vp[n][s] = s*vp[n-1][s] + 2*vp[n-1][s-1] + (n-s)*vp[n-1][s-2]
    vp[n][n] = 0

def p(n, s):
    if n <= 0 or s <= 0 or s >= n:
        return 0
    else:
        return vp[n, s]

def f(n, k):
    if n < 0:
        return Fraction(0, 1)
    elif n < 2*k:
        return Fraction(sum([s**k * vp[n][s] for s in range(n)]), fac[n])
    elif k == 0:
        return Fraction(1, 1)
    elif k == 1:
        return Fraction(2*n-1, 3)
    elif k == 2:
        return Fraction(40*n**2-24*n-19, 90);
    elif k == 3:
        return Fraction(560*n**3-168*n**2-1166*n+507, 1890)
    elif k == 4:
        return Fraction(2800*n**4+1120*n**3-13996*n**2+6212*n+4353, 14175)
    elif k == 5:
        return Fraction(12320*n**5+18480*n**4-109912*n**3+7524*n**2+156686*n-60465, 93555)
    else:
        return ValueError

def p(n, k):
    c = [[1], [1, -1], [1, -2, 1], [1, -3, 3, -1], [1, -4, 6, -4, 1], [1, -5, 10, -10, 5, -1]][k]
    return sum([c[i]*f(n, k-i) for i in range(k+1)])

k, n = map(int, raw_input().split())
r = p(n, k)
print '%d/%d' % (r.numerator, r.denominator)
