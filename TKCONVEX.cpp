#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#define N 100000
#define K 20

int nextbmp(int m)
{
    // xx..x011..100..0->xx..x100..01..1
    int i, j;
    for(i = 0; !((m>>i)&1); ++i);
    for(j = i; (m>>j)&1; ++j);
    return ((m+(1<<i)) | ((1<<(j-i-1))-1));
}
struct Stick {
    int v, idx;
} v[N];
int n, k, idx[K];

int comp(const void *a, const void *b)
{
    return ((Stick*)a)->v - ((Stick*)b)->v;
}
bool tkconvex(int n, Stick v[], int k, int idx[])
{
    long long s = 0;
    int c1 = -1, c2 = -1;
    for(int i = 0; i < k; i++)
        s += v[i].v;
    if(s-v[k-1].v > v[k-1].v)
        c1 = c2 = k-1;
    for(int i = k; i < n; i++) {
        s += v[i].v - v[i-k].v;
        if (s-v[i].v > v[i].v) {
            if(c1 < 0)
                c1 = i;
            c2 = i;
        }
    }
    if(c2 - c1 >= k) {
        for(int i = 0; i < k; i++) {
            idx[i] = v[c1-k+1+i].idx;
            idx[k+i] = v[c2-k+1+i].idx;
        }
        return true;
    }
    for(; c2 > c1 && c2 >= 2*k-1; --c2) {
        s = 0;
        for(int i = 0; i < k; i++)
            s += v[c2-i].v;
        if(s-v[c2].v <= v[c2].v)
            continue;
        for(int i = k; i < 2*k; i++)
            s += v[c2-i].v;
        for(int m = (1<<k)-1; !((m >> (2*k-1)) & 1); m = nextbmp(m)) {
            //printf("%d\n", m);
            long long s1 = 0;
            int fst = -1;
            for(int i = 0; i < 2*k-1; i++)
                if((m>>i)&1) {
                    s1 += v[c2-1-i].v;
                    if(fst < 0) fst = c2-1-i;
                }
            if(s1-v[fst].v > v[fst].v && s-s1-v[c2].v > v[c2].v) {
                for(int i = 2*k-2, u = 0, w = k; i >= 0; i--)
                    if((m>>i)&1)
                        idx[u++] = v[c2-1-i].idx;
                    else
                        idx[w++] = v[c2-1-i].idx;
                idx[2*k-1] = v[c2].idx;
                return true;
            }
        }
    }
    return false;
}
int main()
{
    scanf("%d%d", &n, &k);
    for(int i = 0; i < n; i++) {
        scanf("%d", &v[i].v);
        v[i].idx = i+1;
    }
    qsort(v, n, sizeof(Stick), comp);
    if(tkconvex(n, v, k, idx)) {
        printf("Yes\n");
        for(int i = 0; i < 2*k; i++)
            printf("%d%c", idx[i], i<2*k-1?' ':'\n');
    } else {
        printf("No\n");
    }
    return 0;
}

