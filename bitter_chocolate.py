def win(x,y,z,ls):
  for i in range(x):
    if (i,y,z) in ls:
      return True
  for i in range(y):
    if (min(x,i),i,z) in ls:
      return True
  for i in range(z):
    if (min(x,i),min(y,i),i) in ls:
      return True
  return (x+y+z==0)

ls = []
m = 25
for x in range(m):
    for y in range(x, m):
        for z in range(y, m):
            if not win(x, y, z, ls):
                ls.append((x, y, z))
print ls
