/*
 * =====================================================================================
 *
 *       Filename:  nov13-flip.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年11月20日 15时23分05秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 500

int n0, n, d, a[N][N], b[N], row[N], col[N], v[N];
int m, mv[N];

int main()
{
    scanf("%d%d", &n, &d);
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++)
            scanf("%d", &b[i*n+j]);
    for(int i = 0; i < n; i++)
        for(int j = 0; j < n; j++) {
            int x = i*n+j;
            for(int u = 0; u < n; u++)
                for(int v = 0; v < n; v++) {
                    int y = u*n+v;
                    a[x][y] = (abs(i-u)+abs(j-v) <= d);
                }
        }
    n0 = n;
    n *= n;
    for(int i = 0; i < n; i++)
        row[i] = i;
    int i = 0;
    for(int c = 0; c < n; c++) {
        int j = i;
        while(j < n && !a[row[j]][c])
            ++j;
        if(j >= n)
            continue;
        if(j != i) {
            int tmp = row[i]; row[i] = row[j]; row[j] = tmp;
        }
        for(j = i+1; j < n; j++)
            if(a[row[j]][c]) {
                for(int k = c; k < n; k++)
                    a[row[j]][k] ^= a[row[i]][k];
                b[row[j]] ^= b[row[i]];
            }
        col[i++] = c;
    }
    for(int j = i; j < n; j++)
        if(b[row[j]]) {
            printf("Impossible\n");
            return 0;
        }
    int rnk = i;
    for(--i; i >= 0; --i) {
        int c = col[i];
        for(int j = i-1; j >= 0; j--)
            if(a[row[j]][c]) {
                for(int k = c; k < n; k++)
                    a[row[j]][k] ^= a[row[i]][k];
                b[row[j]] ^= b[row[i]];
            }
    }
    for(int i = 0; i < n; i++)
        v[i] = 0;
    m = 0;
    for(int i = 0; i < rnk; i++) {
        v[col[i]] = b[row[i]];
        m += b[row[i]];
    }
#if 0
    for(int i = 0; i < n; i++) {
        printf("%d ", b[i]);
        for(int j = 0; j < n; j++)
            printf(" %d", a[i][j]);
        printf("\n");
    }
#endif
    printf("Possible\n%d\n", m);
    for(int i = 0; i < n; i++)
        if(v[i])
            printf("%d %d\n", i / n0, i % n0);
    return 0;
}

