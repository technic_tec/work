/*
 * =====================================================================================
 *
 *       Filename:  vacationgold.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2013年12月17日 15时07分56秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define N 20100
#define K 210
#define INF 500000000

int n, m, k, q, el[N], dst[N], nxt[N], cst[N], re[N], rdst[N], rnxt[N], h[K], hi[N];
int ds[K][N], dt[K][N];

int heap_pop(int *hp, int *ip, int &tail, int d[])
{
    if(tail <= 0)
        return -1;
    int res = hp[1], pa = 1, ch = 2, x = hp[tail--];
    ip[res] = -1;
    while(ch <= tail) {
        if(ch+1 <= tail && d[hp[ch+1]] < d[hp[ch]])
            ++ch;
        if(d[hp[ch]] >= d[x])
            break;
        hp[pa] = hp[ch]; ip[hp[pa]] = pa;
        pa = ch; ch <<= 1;
    }
    hp[pa] = x; ip[x] = pa;
    return res;
}

void heaplify_up(int x, int *hp, int *ip, int &tail, int d[])
{
    int ch = x, pa = (x>>1), vx = hp[x];
    while(pa > 0 && d[vx] < d[hp[pa]]) {
        hp[ch] = hp[pa]; ip[hp[ch]] = ch;
        ch = pa; pa >>= 1;
    }
    hp[ch] = vx; ip[vx] = ch;
}

int hp[N], ip[N], tail;
void SP(int n, int el[], int dst[], int nxt[], int cst[], int x, int d[])
{
    for(int i = 1; i <= n; i++) {
        d[i] = INF;
        hp[i] = (i-1 <= n-x ? x+i-1 : x+i-1-n);
        ip[hp[i]] = i;
    }
    tail = n;
    d[x] = 0;
    while((x = heap_pop(hp, ip, tail, d)) >= 0) {
        for(int e = el[x]; e >= 0; e = nxt[e]) {
            if(d[dst[e]] > d[x] + cst[e]) {
                d[dst[e]] = d[x] + cst[e];
                heaplify_up(ip[dst[e]], hp, ip, tail, d);
            }
        }
    }
}
int main()
{
    freopen("vacationgold.in", "r", stdin);
    freopen("vacationgold.out", "w", stdout);
    scanf("%d%d%d%d", &n, &m, &k, &q);
    memset(el, -1, sizeof(el));
    memset(re, -1, sizeof(re));
    memset(hi, -1, sizeof(hi));
    for(int i = 0; i < m; i++) {
        int x, y, c;
        scanf("%d%d%d", &x, &y, &c);
        dst[i] = y; cst[i] = c;
        nxt[i] = el[x]; el[x] = i;
        rdst[i] = x; rnxt[i] = re[y]; re[y] = i;
    }
    for(int i = 0; i < k; i++) {
        int x;
        scanf("%d", &x);
        h[i] = x;
        hi[x] = i;
    }
    for(int i = 0; i < k; i++) {
        SP(n, el, dst, nxt, cst, h[i], dt[i]);
        SP(n, re, rdst, rnxt, cst, h[i], ds[i]);
    }
    int cr = 0;
    long long cs = 0;
    for(int i = 0; i < q; i++) {
        int x, y, cx;
        scanf("%d%d", &x, &y);
        if(hi[x] >= 0) {
            cx = dt[hi[x]][y];
        } else if(hi[y] >= 0) {
            cx = ds[hi[y]][x];
        } else {
            cx = INF;
            for(int e = el[x]; e >= 0; e = nxt[e])
                if(cst[e] + dt[hi[dst[e]]][y] < cx)
                    cx = cst[e] + dt[hi[dst[e]]][y];
        }
        if(cx < INF) {
            cr++;
            cs += (long long)cx;
        }
    }
    printf("%d\n%lld\n", cr, cs);
    return 0;
}

