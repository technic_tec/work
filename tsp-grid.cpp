/*
 * =====================================================================================
 *
 *       Filename:  hamilton_grid.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2014年03月15日 12时19分23秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Terence (tec), 
 *   Organization:  Intel
 *
 * =====================================================================================
 */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <vector>
#include <map>
using namespace std;

#define K 10
#define N 59049 // 3^n 
#define INF 9999999

enum EndPoint {
    NONE = 0,
    HEAD,
    TAIL
};
const int pow3[] = {1, 3, 9, 27, 81, 243, 729, 2187, 6561, 19683, 59049};
class State {
    public:
        State(int sz = 0, int *pe = NULL) : m_sz(sz) {
            if(sz) {
                ent = new EndPoint[sz];
                if(pe)
                    memcpy(ent, pe, sz*sizeof(EndPoint));
                else
                    memset(ent, 0, sz*sizeof(EndPoint));
                compute_val();
                validate();
            } else {
                ent = NULL;
                val = 0;
                valid_since = INF;
            }
        }
        State(State &o) : m_sz(o.m_sz), val(o.val), valid_since(o.valid_since) {
            if(m_sz) {
                ent = new EndPoint[m_sz];
                memcpy(ent, o.ent, m_sz*sizeof(EndPoint));
            } else
                ent = NULL;
        }
        ~State() {
            if(ent) {
                delete[] ent;
                ent = NULL;
            }
        }
        inline void inc()
        {
            int i = 0;
            while(i < m_sz && ent[i] == TAIL)
                ent[i++] = NONE;
            assert(i < m_sz);
            ent[i] = (EndPoint)((int)ent[i]+1);
            int old = val;
            compute_val();
            assert(val == old+1);
        }

        inline EndPoint operator[](int k) const {
            assert(k >= 0 && k <= m_sz);
            if(k < m_sz)
                return ent[k];
            else
                return (val == 0 ? NONE : (val == -1 ? HEAD : TAIL));
        }
        inline int state_val() const
        {
            return val;
        }
    private:
        inline void compute_val()
        {
            val = 0;
            for(int i = m_sz-1; i >= 0; i--)
                val = 3 * val + ent[i];
        }
        inline void validate()
        {
            int c = 0, x = -1, x1 = 0;
            for(int i = 0; i < m_sz; i++) {
                if(ent[i] == HEAD)
                    ++c;
                else if(ent[i] == TAIL)
                    --c;
                if(c < -1) {
                    valid_since = INF;
                    return;
                } else if(c == -1) {
                    if(x < 0) x = i;    // first unmatched TAIL
                } else if(c == 1 && ent[i] == HEAD) {
                    x1 = i;             // last HEAD at layer 0
                }
            }
            if(c > 1 || (c > -1 && x >= 0)) {
                valid_since = INF;
                return;
            }
            // (c == -1 && x >= 0) || (c == 0, 1 && x < 0)
            if(c == -1) {
                valid_since = -(x+1);  // need a HEAD before ent[x]
            } else if(c == 1) {
                valid_since = x1+1;    // need a TAIL after ent[x1]
            } else {
                valid_since = 0;
            }
        }
        int m_sz;
        EndPoint *ent;
        int val;
        int valid_since;
};

// [x] [y] [bottom open end ^ n (0--no open end, 1--head open end, 2--tail open end)]
int m, n, dh[K][K], dv[K][K];
int c[K][K][N];
State sts[N];

int main()
{
    scanf("%d%d", &m, &n);
    for(int i = 0; i < m; i++)
        for(int j = 0; j < n-1; j++)
            scanf("%d", &dh[i][j]);
    for(int i = 0; i < m-1; i++)
        for(int j = 0; j < n; j++)
            scanf("%d", &dv[i][j]);
    if(m <= 1 || n <= 1 || (m*n) & 1) {
        printf("0\n");
        return 0;
    }
    sts[0] = State(n);
    for(int i = 1; i < pow3[n]; i++) {
        sts[i] = sts[i-1];
        sts[i].inc();
    }

    for(int j = 0; j < n; j++)
        c[0][0][j] = INF;
    c[0][0][1] = 0;
    for(int i = 0; i < m; i++) {
        for(int j = 0; j < n - (i==m-1); j++) {
            for(int sti = 0; sti < pow3[n]; sti++) {
                State &st = sts[sti];
                if(c[i][j][sti] >= INF || (j == n-1 && st[n] != NONE))
                    continue;
                int *nst = (j == n-1 ? c[i+1][0] : c[i][j+1]);
                int nj = (j == n-1 ? 0 : (j+1));
                // r, J, (-, 7), (L, |)
                if(st[nj] == NONE && st[n] == NONE) { // r
                } else if(st[nj] == NONE || st[n] == NONE) {        // | L - 7
                    if(c[i][j][sti] < nst[sti])
                        nst[sti] = c[i][j][sti];
                    int st1 = st_update(sti, nj, st[n]);
                    if(c[i][j][sti] < nst[st1])
                        nst[st1] = c[i][j][sti];`
                } else {                       // J
                    if(st[n] != st[nj]) {
                        if(st[n] == HEAD && (i != m-1 || nj != n-1))
                            continue;
                        if
                    }
                }
            }
        }
    }
    printf("%d\n", c[m-1][n-1][0]);
    return 0;
}

